#!/usr/bin/env python
# coding: utf-8
# encoding=utf8
#
# Copyright CEP(Kage Park)
# License : GPL
#   It can be sell by CEP Only.
#   Others according to GPL license(Only Freeware software).
#   Since : March 1994
# Kage

### test
# Default 4.7MB
# python 2.6~ for no newline on print()
from __future__ import print_function
# Network
import socket

# ping ( less 10KB )
import random
import struct
import select

import subprocess
from datetime import datetime
# Arguments
#import argparse # 0.1MB

# python command function
import inspect
#import traceback
#Python Code
import code # about 10KB

# fake sys.argv from string
import copy # about 100KB

# Line
import readline

# OS finding executable file like as which command
from distutils.spawn import find_executable

# WEB
import requests,re # 3MB increase binary

#User Network
import getpass # about 60KB

import threading
# hash
import hashlib

import sys,os,stat

try:
    from pygit2 import clone_repository # 3.2MB
except:
    pass
###########################################################
#SELF FILE PATH
self_file=sys.executable
if os.path.basename(self_file) == 'python':
    _k_dest_dir=os.path.dirname(os.path.realpath(__file__)) # Python script file path
else:
    _k_dest_dir=os.path.dirname(self_file)  # Python compiled binary file path
sys.path.append('{}/data/lib/python'.format(os.path.dirname(os.path.dirname(_k_dest_dir))))
import kNet
import kLine as kl
import kFile as kf
import time
import kDict as kd
import kProgress as kpg
from kDaemon import Daemon
import kModule as km
import kCodeRead as kcr

this_mod_name=__name__
_k_version='4.2.156'

k_pipe_file='{}/.k_sh'.format(_k_dest_dir)
k_tmp_db='{}/.k_sh.1'.format(_k_dest_dir)
k_pip_file='{}/.k_sh.pip'.format(_k_dest_dir)
self_k=False

sym_linked=False
sym_list=None
dfname=None


#Return Code
K_ERROR={'ERR'}
K_FAIL={'FAL'}
K_FALSE=K_FAIL
K_TRUE={'TRU'}
K_OK=K_TRUE
K_ALREADY={'ALR'}
K_NOT_FOUND={'NFO'}
K_NO_INPUT={'NIN'}
K_WRONG_FORMAT={'WFO'}
K_UNKNOWN={'UNK'}


#Global Commands
#def import_from(module, name):
#    module = __import__(module, fromlist=[name])
#    return getattr(module, name)

def pipe(k_pipe_file):
    LIBS().pipe(k_pipe_file)

class CMD:
    def _ping(*argv):
        '''
        Emergency ping command
        '''
        ICMP_ECHO_REQUEST = 8 # Seems to be the same on Solaris. From /usr/include/linux/icmp.h;
        ICMP_CODE = socket.getprotobyname('icmp')
        ERROR_DESCR = {
            1: ' - Note that ICMP messages can only be '
               'sent from processes running as root.',
            10013: ' - Note that ICMP messages can only be sent by'
                   ' users or processes with administrator rights.'
            }
 
        def checksum(source_string):
            sum = 0
            count_to = (len(source_string) / 2) * 2
            count = 0
            while count < count_to:
                this_val = ord(source_string[count + 1])*256+ord(source_string[count])
                sum = sum + this_val
                sum = sum & 0xffffffff # Necessary?
                count = count + 2
            if count_to < len(source_string):
                sum = sum + ord(source_string[len(source_string) - 1])
                sum = sum & 0xffffffff # Necessary?
            sum = (sum >> 16) + (sum & 0xffff)
            sum = sum + (sum >> 16)
            answer = ~sum
            answer = answer & 0xffff
            # Swap bytes. Bugger me if I know why.
            answer = answer >> 8 | (answer << 8 & 0xff00)
            return answer
 
        def create_packet(id):
            """Create a new echo request packet based on the given "id"."""
            # Header is type (8), code (8), checksum (16), id (16), sequence (16)
            header = struct.pack('bbHHh', ICMP_ECHO_REQUEST, 0, 0, id, 1)
            data = 192 * 'Q'
            # Calculate the checksum on the data and the dummy header.
            my_checksum = checksum(header + data)
            # Now that we have the right checksum, we put that in. It's just easier
            # to make up a new header than to stuff it into the dummy.
            header = struct.pack('bbHHh', ICMP_ECHO_REQUEST, 0,
                                 socket.htons(my_checksum), id, 1)
            return header + data
 
        def receive_ping(my_socket, packet_id, time_sent, timeout):
            time_left = timeout
            while True:
                started_select = time.time()
                ready = select.select([my_socket], [], [], time_left)
                how_long_in_select = time.time() - started_select
                if ready[0] == []: # Timeout
                    return
                time_received = time.time()
                rec_packet, addr = my_socket.recvfrom(1024)
                icmp_header = rec_packet[20:28]
                type, code, checksum, p_id, sequence = struct.unpack(
                    'bbHHh', icmp_header)
                if p_id == packet_id:
                    return time_received - time_sent
                time_left -= time_received - time_sent
                if time_left <= 0:
                    return
 
        def do_one(dest_addr, timeout=1):
            try:
                my_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, ICMP_CODE)
            except socket.error as e:
                if e.errno in ERROR_DESCR:
                    raise socket.error(''.join((e.args[1], ERROR_DESCR[e.errno])))
                raise # raise the original error
            # Maximum for an unsigned short int c object counts to 65535 so
            packet_id = int((id(timeout) * random.random()) % 65535)
            packet = create_packet(packet_id)
            while packet:
                # The icmp protocol does not use a port, but the function
                # below expects it, so we just give it a dummy port.
                sent = my_socket.sendto(packet, (dest_addr, 1))
                packet = packet[sent:]
            delay = receive_ping(my_socket, packet_id, time.time(), timeout)
            my_socket.close()
            if delay:
                return delay,packet_id
 
        if len(argv) == 0:
            print('ping [-c count] [-t 1] <dest>')
            os._exit(0)
        dest=argv[-1]
        cmd_dict=LIBS().get_cmd_dict(k_tmp_db)
        dest_ip=kNet.host_ip(dest,cmd_dict)
        count=None
        timeout=2
        for oo in range(0,len(argv)):
            if argv[oo] == '-c':
                count=int(argv[oo+1])
            elif argv[oo] == '-t':
                timeout=float(argv[oo+1])
        cc=1
        while True:
            delay=do_one(dest_ip,timeout)
            if delay:
                print('{} bytes from {} : time={} ms'.format(delay[1],dest,round(delay[0]*1000.0,4)))
            else:
                print('{} : timeout ({} second)'.format(dest,timeout))
            if count and int(count) <= cc:
                break
            time.sleep(0.5)
            cc+=1

    def _imports(self,*args):
        '''
        Import python module internal command
        '''
        unload=False
        module_line=LIBS().list2str(args).split(',')
        if '-r' in module_line[0]:
            unload=True
            module_line[0]=module_line[0].replace('-r','')
        for mod in module_line:
            mod_name=None
            mod_list=mod.split()
            if 'as' in mod_list: # support as command
                mod_name=mod_list[-1]
            if mod_list[0][0] == '!':
                LIBS().get_lib(mod_list[0][1:],name=mod_name)
            else:
                #LIBS().imports(name=mod_name,filename=mod_list[0],global_var=globals(),unload=unload)
                km.imports(name=mod_name,filename=mod_list[0],global_var=globals(),unload=unload)

    def _mod_ls(self):
        '''
        Loaded module list
        '''
        for ii in [key for key in globals().keys() if isinstance(globals()[key], type(sys)) and not key.startswith('__')]:
            try:
                m_path=globals()[ii].__file__
            except:
                m_path=''
            print('%15s : %s'%(ii,m_path))


class LIBS:
    def wgit(self,src,dest,branch='origin'):
       try:
           repo=clone_repository(src, dest)
           return repo
       except:
           return False

    def wget(self,src,dest):
       try:
           r=requests.get(src,stream=True)
       except requests.exceptions.RequestException as e:
           return False
       pp=0
       with open(dest,'wb') as f:
          for chunk in r.iter_content(chunk_size=1024):
             f.write(chunk)
             f.flush()
             pp=kpg.progress(pp=pp,msg=dest)
       return True

    def check_login(self,cmd_dict,MyD=None):
        if type(cmd_dict) is not dict:
            return False
        cltime=int(datetime.now().strftime("%s"))
        if 'login' in cmd_dict and (cltime - cmd_dict['login']) < 18000: # 5hours
            cmd_dict['login']=cltime
            kd.save_db(k_tmp_db,cmd_dict)
            return True
        else:
            logout(MyD)
#            if 'login' in cmd_dict:
#                cmd_dict.pop('login',None)
#                kd.save_db(k_tmp_db,cmd_dict)
#            if MyD:
#                MyD.stop()
#            if os.path.exists(k_pip_file):
#                os.remove(k_pip_file)
#            if os.path.exists(k_pipe_file):
#                os.remove(k_pipe_file)
            return False

    def md5(self,string):
        h=hashlib.md5()
        h.update(string)
        return h.hexdigest()

    def get_cmd_dict(self,k_tmp_db,force=False):
        cmd_dict={}
        if force or not os.path.isfile(k_tmp_db):
            soc=kNet.gen_client_socket(ip,port)
            kNet.put_packet(soc,kNet.packet(cmd='ls:cmd_dict'))
            rc=kNet.get_packet(soc)
            if rc['rc']:
                cmd_dict=rc['data']
                kNet.put_packet(soc,kNet.packet(cmd='get:hosts'))
                host=kNet.get_packet(soc)
                cmd_dict['hosts']={}
                if host['rc']:
                    cmd_dict['hosts']=host['data']
            kd.save_db(k_tmp_db,cmd_dict)
        elif os.path.isfile(k_tmp_db):
            cmd_dict=kd.open_db(k_tmp_db)
        return cmd_dict

    def get_ssh_host(self,args,hosts):
        i=0
        for zz in args:
            if '@' in zz:
                zz_a=zz.split('@')
                args[i]='{}@{}'.format(zz_a[0],hosts[zz_a[-1]]['ip'])
                if 'key' in hosts[zz_a[-1]]:
                    return args,hosts[zz_a[-1]]['key']
                else:
                    return args,None
            else:
                if zz in hosts:
                    args[i]=hosts[zz]['ip']
                    if 'key' in hosts[zz]:
                        return args,hosts[zz]['key']
                    else:
                        return args,None
        return args,None

    def code_run(self,cmd_dict,cmd_name):
        if type(cmd_dict) is dict:
            for tt in ['python','shell']:
                if tt in cmd_dict and cmd_name in cmd_dict[tt]:
                    if not cmd_dict[tt][cmd_name]:
                        #Get cmd code
                        soc=kNet.gen_client_socket(ip,port)
                        kNet.put_packet(soc,kNet.packet(cmd='get:run_cmd',dest='{}/{}'.format(tt,cmd_name)))
                        cmd_rc=kNet.get_packet(soc)
                        if cmd_rc['rc']:
                            cmd_dict[tt][cmd_name]=cmd_rc['data']
                        else:
                            os._exit(1)
                        if not self_k:
                            kd.save_db(k_tmp_db,cmd_dict)
                    if tt == 'shell':
                        # run shell code in console
                        self.run_shell('_k_{} '.format(cmd_name) + self.list2str(sys.argv[1:]).replace('.k','').replace('.sh',''),pre_code=cmd_dict[tt][cmd_name])

                        sys.exit()
                    elif tt == 'python':
                         # run python code to function
                        codeObejct = compile(cmd_dict[tt][cmd_name], 'kCode', 'exec')
                        #eval(cmd_dict[tt][cmd_name])
                        #eval(codeObejct,globals(),locals())
                        eval(codeObejct,globals())
#                        exec(cmd_dict[tt][cmd_name])
                        sys.exit()

    def version_number_up(self,s):
        """ look for the last sequence of number(s) in a string and increment """
        lastNum = re.compile(r'(?:[^\d]*(\d+)[^\d]*)+')
        m = lastNum.search(s)
        if m:
            next = str(int(m.group(1))+1)
            start, end = m.span(1)
            s = s[:max(end-len(next), start)] + next + s[end:]
        return s

    def version_up(self,src,version_tag='_k_version='):
        version_str=re.findall("{}.*[0-9]".format(version_tag),src)
        if version_str:
            new_version_str=self.version_number_up(version_str[0])
            src=src.replace("{}".format(version_str[0]),"{}".format(new_version_str))
            return src
        return src

    def symlink(self,linked=False,cmd_list=None,dfname=None):
        if linked and cmd_list:
             if dfname is None:
                 dfname=os.path.basename(sys.argv[0])
             # Clean symlink
             if cmd_list and linked:
                 opwd=os.getcwd()
                 os.environ['PATH']='{}'.format(os.environ['PATH'].replace(_k_dest_dir,''))
                 os.chdir(_k_dest_dir)
                 for slnk in cmd_list:
                     if slnk != dfname and os.path.exists(slnk) and os.path.islink(slnk):
                         os.remove(slnk)  # remove pipe file
                 os.chdir(opwd)
        else:
            # symlink
            cmd_list=LIBS().get_cmd_list(shell=True)
            opwd=os.getcwd()
            for tt in ['python','shell']:
                if tt in cmd_dict:
                    cmd_list+=list(cmd_dict[tt].keys())
            #dest_dir=os.path.dirname(sys.executable)
            # Get current command running path(os.getcwd()) not real file path: os.path.abspath(__file__)
            # running command name : sys.argv[0]
            # source python script file name : __file__
            # running file real path : os.path.dirname(sys.executable)
            # 
            if cmd_list:
                os.chdir(_k_dest_dir)
                dfname=os.path.basename(sys.argv[0])
                if os.path.isfile(dfname):
                    os.environ['PATH']='{}:{}'.format(_k_dest_dir,os.environ['PATH'])
                    linked=True
                    for slnk in cmd_list:
                        if not slnk in [dfname,'shell','python','include'] and slnk[0] != '_' and not os.path.isfile(slnk):
                            try:
                               os.symlink('./'+dfname, slnk)
                            except:
                               pass
                os.chdir(opwd)
            return linked,cmd_list,dfname


    def pipe(self,k_pipe_file):
        while True:
            if not os.path.exists(k_pipe_file):
                os.mkfifo(k_pipe_file)
            pipein = open(k_pipe_file, 'r')
            line = pipein.readline()
            if line:
                line_a=line.split()
                if line_a[0] == 'quit':
                    if os.path.isfile(k_pipe_file):
                        os.remove(k_pipe_file)
                    break
                cmd_analy=line_a[0].split('.')
                if len(cmd_analy) == 1:
                    rc=self.run_function_name_argv(km.get_module_obj(this_mod_name,globals()),line_a[0],*tuple(line_a[1:]))
                elif len(cmd_analy) == 2:
                    if cmd_analy[0] in globals():
                       # get class object from name string at globals() : globals()[<class name>]
                       class_obj=globals()[cmd_analy[0]]() # Change class object to class instance
                       rc=self.run_function_name_argv(class_obj,cmd_analy[1],*tuple(line_a[1:]))
                    else:
                       print('Class({}) not found'.format(cmd_analy[0]))
                else:
                    print('Command Error:',line)

    def run_shell(self,args_list,env='PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin\nexport SHELL=$([ -f /bin/bash ] && echo /bin/bash || /bin/sh)\n',pre_code=''):
        return subprocess.call(['bash','-c',env + pre_code + args_list])

    def exit(self,signal=None,frame=None,msg=None,code=None):
        if code == "no":
          return K_OK
        if msg is not None:
          print(msg)
        if code is None:
          sys.exit(0)
        else:
          sys.exit(CODE().code2shell(code))

    def get_lib(self,filename,name=None):
        args_a=filename.split('/')
        lib_type=args_a[0]
        soc=kNet.gen_client_socket(ip,port)
        kNet.put_packet(soc,kNet.packet(cmd='get:lib',dest=filename))
        rc=kNet.get_packet(soc)
        if rc['rc']: 
            file_name=os.path.basename(filename)
            if name is None:
                name=os.path.splitext(file_name)[0]
            if lib_type == 'python':
                lrc=km.imports(code=rc['data'],name=name,global_var=globals())
                if lrc:
                    print('loaded {} <= {}\r'.format(name, filename))
                else:
                    print('not loaded or not support : {}\n'.format(filename))
                return lrc
            elif lib_type == 'shell':
                print('not support shell yet')
                pass
        else:
            print('not loaded or not support : {}\r'.format(filename))

    def list2str(self,list_data,symbol=' '):
        '''
        convert list to string with symbol between data
        '''
        return symbol.join(list_data)

    def get_function_list_in_module_name(self,module_name=None):
        '''
        get function dict from module name (string text)
        return : {<function name string>:<function object>}
        '''
        aa={}
        if module_name is None:
            module_name='__main__'
        if module_name in sys.modules:
            for name,obj in inspect.getmembers(sys.modules[module_name]):
                if inspect.isfunction(obj): # inspect.ismodule(obj) check the obj is module or not
                    aa.update({name:obj})
        return aa

    def get_function_args(self,func_obj,order=False,data=True):
        """
        Return function's arguments
        order=False : a dictionary of input parameter: {arg_name:default_values, ...}
        order=True  : a list of input parameter : [(arg_name,default_values), ...]
        """
        try:
            args, varargs, keywords, defaults = inspect.getargspec(func_obj)
        except:
            return
        if data:
            if order:
                arg_val=[]
            else:
                arg_val={}
        else:
            arg_val=''
        if args:
            val_t=-1
            if defaults:
                val_t=len(defaults)-1
            arg_t=len(args)
            for ii in range(arg_t,0,-1):
                i=ii-1
                if val_t >= 0:
                    if data:
                        if order:
                            arg_val=[(args[i],defaults[val_t])]+arg_val
                        else:
                            arg_val.update({args[i]:defaults[val_t]})
                    else:
                        if len(arg_val):
                            arg_val='{0}={1},{2}'.format(args[i],defaults[val_t],arg_val)
                        else:
                            arg_val='{0}={1}'.format(args[i],defaults[val_t])
                    val_t=val_t-1
                else:
                    if data:
                        if order:
                            arg_val=[(args[i],)]+arg_val
                        else:
                            arg_val.update({args[i]:''})
                    else:
                        if len(arg_val):
                            arg_val='{0},{1}'.format(args[i],arg_val)
                        else:
                            arg_val='{0}'.format(args[i])
        return arg_val

    def get_func_header(self,func_name,obj=None):
        if inspect.ismodule(obj):
            module_list=LIBS().get_function_list_in_module_name(obj.__name__)
            if func_name in module_list:
                func_obj=module_list[func_name]
        elif hasattr(obj,'__class__') or inspect.isclass(obj): # check class from class instance, : <module>.<classname>()
            func_obj=km.get_method_in_class(obj,func_name)
        elif inspect.isfunction(obj):
            func_obj=obj
        else:
            func_obj=self.get_func_obj_from_str(func_name)
        if func_obj:
            return '{}({})'.format(func_name,LIBS().get_function_args(func_obj,True,False))

    def get_method_name_list_in_class(self,_class_):
        '''
        Get method name in the Class
        _class_ : class object (<module>.<classname>) or class instance (<module>.<classname>())
        '''
        return [i for i in dir(_class_) if not inspect.ismethod(i)]

    def is_method_in_class(self,class_name, func_name):
        '''
        Check method name in the Class
        usage) is_method_in_class(<class name>,'<func name>')
        '''
        if func_name in self.get_method_name_list_in_class(class_name):
            return True
        return False

    def get_func_obj_from_str(self,input_str):
        if type(input_str) is str:
            input_a=input_str.split('.')
            func_name=input_a.pop(-1)
            if len(input_a) > 0:
                mod_name=input_a.pop(0)
                class_name=''
                for i in input_a:
                    if len(class_name):
                        class_name='{0}.{1}'.format(class_name,i)
                    else:
                        class_name='{0}'.format(i)
                module=km.get_module_obj(mod_name,globals())
                if module:
                    class_instance=km.get_class_instance_in_module(module,class_name)
                    if class_instance:
                        return km.get_method_in_class(class_instance,func_name)
                    else:
                        return getattr(module,func_name)
            else:
                if func_name in globals():
                    return globals()[func_name]


    def run_function_name_argv(self,obj,func_name,*arg): # Get variable inputs like as console input(argv)
        '''
        obj : module or class instance (not support class object)
        func_name: function name string
        *args : put argument for the function's parameter (tuple)
        Run function name in object with variable input.
        but the function take fixed variable inputs
        use)run_function_name_arg(<class>,<func_name>[,'v1',...])
        ex)
        run_function_name_arg(abc,test,3,'c') : work
        run_function_name_arg(abc,test,3) : not work

        arg=tuple(sys.argv[2:])
        run_function_name_arg(abc,mm,*arg) : work

        class abc:
           def test(a=None,b=None):
             .......
           def mm(*arg):
             .......
        '''
        if LIBS().is_method_in_class(obj,func_name):
#            print('<<<<',obj,func_name,arg)
            if len(arg) == 0:
                return getattr(obj,func_name)()
            else:
                return getattr(obj,func_name)(*arg) # Extract tuple to variable

    def get_cmd_list(self,shell=False):
        '''
        get defined function list
        '''
        func_list=[]
        if shell:
            for ii in list(LIBS().get_function_list_in_module_name(this_mod_name).keys()):
                if ii[0] != '_' and not ii in ['pipe','import','history','ssh']:
                    func_list.append(ii)
        else:
            func_list=LIBS().get_function_list_in_module_name(this_mod_name).keys()
        if 'cmd' in globals():
            func_list=func_list+list(LIBS().get_function_list_in_module_name('cmd').keys())
        return func_list

    def trash(self,filename,lib_type=None,cmd='del'): # cmd, dk7
        soc=kNet.gen_client_socket(ip,port)
        if lib_type:
            cmd='{}:{}'.format(cmd,lib_type)
        kNet.put_packet(soc,kNet.packet(cmd=cmd,dest=filename))
        rc=kNet.get_packet(soc)
        return rc

    def push(self,filename,dest_file,cmd='save'): #cmd : save, lib(update), sk7: save K
        if os.path.isfile(filename):
            with open(filename,'r') as f:
                fd=f.read()

            soc=kNet.gen_client_socket(ip,port)
            kNet.put_packet(soc,kNet.packet(cmd='save',dest='{0}'.format(dest_file),data=fd))
            rc=kNet.get_packet(soc)
            return rc

    def get(self,filename,save_file=None,cmd='get',progress=False): # cmd: get, k7(k.py)
        file_name=os.path.basename(filename)
        if save_file:
           save_dir=os.path.dirname(save_file)
           if save_dir=='':
               save_dir='.'
           if not os.path.isdir(save_dir):
               print('{} directory not found'.format(save_dir))
               return False
        else:
           save_file='/tmp/{}'.format(file_name)
        soc=kNet.gen_client_socket(ip,port)
        kNet.put_packet(soc,kNet.packet(cmd='get',dest='{0}'.format(filename)))
        if progress is True:
            rc=kNet.get_packet(soc,progress='Downloading : %s => %s'%(file_name,save_file))
        else:
            rc=kNet.get_packet(soc)
        if rc['rc']:
            with open(save_file,'w') as f:
                f.write(rc['data'])
            return True
        else:
            print(rc)
            return False

# Code convert
class CODE:
    def __init__(self,path=None):
       self.require_pkg=[]

    def get_code(self,code=None):
        if code is None:
            return K_NO_INPUT
        elif type(code).__name__ == 'set':
            return next(iter(code))
        else:
            return K_ERROR

    def int2code(self,code):
        if code == -1:
            return K_ERROR
        elif code == 0:
            return K_OK
        else:
            return K_FAIL
    def code2int(self,code):
        if code == K_TRUE:
           return 1
        elif code == K_ERROR:
           return -1
        else:
           return 0

    def shell2code(self,code):
        if code==0 or code is None:
           return K_TRUE
        elif code==-1:
           return K_ERROR
        else:
           return K_FAIL

    def code2shell(self,code):
        if code == K_TRUE:
           return 0
        elif code == K_ERROR:
           return -1
        else:
           return 1

    def bool2code(self,code):
        if code:
           return K_TRUE
        else:
           return K_FAIL

    def code2bool(self,code):
        if code == K_TRUE or code == K_OK:
           return True
        else:
           return False

    def code_convert(self,code,mode='cep'):
        code_type=type(code).__name__
        if mode == 'cep' or mode == 'set':
           if code_type == 'int' or (code_type == 'str' and CEP().is_int(code)):
              return self.int2code(int(code))
           elif code_type == 'bool':
              return self.bool2code(code)
           elif code_type == 'set':
              return code
        elif mode == 'shell':
           if code_type == 'int' or (code_type == 'str' and CEP().is_int(code)):
              return self.code2shell(self.int2code(int(code)))
           elif code_type == 'bool':
              return self.code2shell(self.bool2code(code))
           elif code_type == 'set':
              return self.code2shell(code)
        elif mode == 'bool':
           if code_type == 'int' or (code_type == 'str' and CEP().is_int(code)):
              return self.code2bool(self.int2code(int(code)))
           elif code_type == 'bool':
              return code
           elif code_type == 'set':
              return self.code2bool(code)
        elif mode == 'int':
           if code_type == 'int' or (code_type == 'str' and CEP().is_int(code)):
              return int(code)
           elif code_type == 'bool':
              return self.code2int(self.bool2code(code))
           elif code_type == 'set':
              return self.code2int(code)
        return code

    def code_check(self,code,check):
        '''
        check return code between return-code and user define code.
        single code(True) check: code_check(abc,True)
          (abc same as True? if yes then True, no then False)
        multi code(1,None,False) check: code_check(abc,[1,None,False])
          (abc's return code is in [1,None,False]? if yes then True, no then False)
        string check : code_check(abc,'yes')
          (abc's return same as 'yes'? if yes then True, no then False)
        Checkable : CEP's code, Bool, Int, Shell, String, ....
        '''
        if type(code).__name__ == 'list':
            code=code[0]
        if type(check).__name__ == 'tuple':
            check=check[0]
        check_type=type(check).__name__
        if check_type == 'list':
            for icheck in list(check):
                icheck_type=type(icheck).__name__
                if self.code_convert(code,mode=icheck_type) == icheck:
                    return True
        else:
            if self.code_convert(code,mode=check_type) == check:
                return True
        return False

def funcs(*args):
    '''
    show available function list
    '''
    class_name=''
    module=None
    if len(args):
        args_a=args.split()
        mod_name=args_a.pop(0)
        module=km.get_module_obj(mod_name,globals())
        if module:
            for i in args_a:
                if len(class_name):
                    class_name='{0}.{1}'.format(class_name,i)
                else:
                    class_name='{0}'.format(i)
            if len(class_name):
                class_instance=km.get_class_instance_in_module(module,class_name)
            else:
                module=module.__name__ 
    else:
        module=this_mod_name
    if module is None:
        print('not loaded {0}'.format(args))
        return
    if len(class_name):
        for ii in LIBS().get_method_name_list_in_class(class_instance):
            if ii[0] != '_':
                if hasattr(cep.CMD(),ii):
                    try:
                        print('{0}({1})'.format(ii,LIBS().get_function_args(getattr(CMD,ii),True,False)))
                    except:
                        print('{0}(???)'.format(ii))
    else:
        func_list=LIBS().get_function_list_in_module_name(module)
        for ii in func_list.keys():
            print('{0}({1})'.format(ii,LIBS().get_function_args(func_list[ii],True,False)))

        if 'cep' in globals():
            CMD=cep.CMD()
            for ii in LIBS().get_method_name_list_in_class(CMD):
                if ii[0] != '_':
                    if hasattr(cep.CMD(),ii):
                        try:
                            print('{0}({1})'.format(ii,LIBS().get_function_args(getattr(CMD,ii),True,False)))
                        except:
                            print('{0}(???)'.format(ii))


def p(*args):
    '''
    Internal python
    '''
    if len(args) == 0:
        vars = globals()
        vars.update(locals())
        shell=code.InteractiveConsole(vars)
        shell.interact()
    else:
        if args[0] == '-f':
            with open(args[1],'r') as f:
                data=f.read()
        else:
            data=args[0]

        func_list=kcr.get_func_list(data,mod='k.')
        if func_list:
            soc=kNet.gen_client_socket(ip,port)
            kNet.put_packet(soc,kNet.packet(cmd='get:func',dest=func_list))
            rc=kNet.get_packet(soc)
            if rc['rc']:
                km.imports(name='k',code=rc['data'],global_var=globals(),unload=True)

        try:
            codeObejct = compile(data, 'kCode', 'exec')
            #eval(data)
#            eval(codeObejct,globals(),locals())
            eval(codeObejct,globals())
#            exec(data)
#            exec(data)
        except:
            print('wrong python code')

def pip(*args):
    if os.path.isfile('/usr/bin/pip') or os.path.isfile('/usr/local/bin/pip'):
        LIBS().run_shell('pip '+LIBS().list2str(args))
    elif os.path.isfile('/usr/bin/pip2') or os.path.isfile('/usr/local/bin/pip2'):
        LIBS().run_shell('pip2 '+LIBS().list2str(args))
    else:
        if LIBS().wget('https://bootstrap.pypa.io/get-pip.py','/tmp/get-pip.py'):
            with open('/tmp/get-pip.py','r') as f:
                data=f.read()
            try:
                codeObejct = compile(data, 'kCode', 'exec')
                eval(codeObejct,globals())            
                LIBS().run_shell('pip '+LIBS().list2str(args))
            except:
                print('please install pip')
        else:
            print('please install pip')
#    else:
#        if hasattr(pip, 'main'):
#            print('pip1')
#            pip.main(list(args))
#        else:
#            print('pip2')
#            _main(list(args))
#            print('please install pip')

def cp(*args):
    '''
    copy file
    cp <file> !<category>
    cp <file> <id>@<ip>:<directory>
    cp !<category>/<file> <dir>
    cp http://xxxx/file  <dir>
    cp <id>@<ip>:/path/file  <dir>
    '''
    whttp=re.compile('^http://')
    whttps=re.compile('^https://')
    wftp=re.compile('^ftp://')
    wgit=re.compile('.git$')
    dest=args[-1]
    dest_type=None
    max_num=len(args)-1
    c_num=0
    if len(args) < 2:
        print('cp <src> <dest>')
        return
    # option
    opt=''
    #for src in copy.deepcopy(args):
    for src in args:
        if src[0] == '-':
            opt+=' {}'.format(src)
            del args[src]

    if dest[0] == '!':
         for src in args[:-1]:
             if c_num > max_num:
                break
             c_num+=1
             #if not os.path.isfile(src) or src[0] == '-' or src[0] == '!' or whttp.search(src) or wftp.search(src):
             if not os.path.isfile(src) or src[0] == '!' or whttp.search(src) or wftp.search(src):
                 continue
             rc=LIBS().push(src,'{}/{}'.format(dest[1:],os.path.basename(src)))
             print(rc['data'])
    else:
        if os.path.isdir(dest):
            dest_type='dir'
        elif os.path.dirname(dest) == '' or os.path.isdir(os.path.dirname(dest)):
            dest_type='file'
        else:
            print('cp <src> <dest>')
            return
        for src in args[:-1]:
            if dest_type == 'dir':
                dest='{}/{}'.format(dest,src.split('/')[-1])
            if dest_type == 'file' and c_num > 0:
                print('overwrite {} to {} ?'.format(src,dest))
            if c_num > max_num:
                break
            c_num+=1
            if src[0] == '!': # KAGE DB
                LIBS().get(src[1:],'{}'.format(dest),progress=True)
            elif wgit.search(src): # git
                if LIBS().wgit(src,dest):
                    print('save at {}'.format(dest))
                else:
                    print('''can't git download {}'''.format(src))
                    continue
            elif whttp.search(src) or wftp.search(src) or whttps.search(src): # wget
                if LIBS().wget(src,dest):
                    print('save at {}'.format(dest))
                else:
                    print('''can't web download {}'''.format(src))
                    continue
            else: # Linux
                LIBS().run_shell('cp {} {} {}'.format(opt,src,dest))

def rm(*args):
    '''
    delete file/data
    rm !<category>/<dest>
    rm <files> or <directory>
    '''
    if len(args) > 0 and args[0][0] == '!':
        if do_cmd:
            file_arr=args[0][1:].split('/')
            if file_arr[0] in ['lib','cmd']:
                filename='/'.join(file_arr[1:])
                lib_type=file_arr[0]
            else:
                filename=args[0][1:]
                lib_type=None
            rc=LIBS().trash(filename,lib_type)
        else:
            rc=LIBS().trash(args[0][1:])
        print(rc['data'])
    elif find_executable('rm'):
        LIBS().run_shell('rm '+LIBS().list2str(args))
    else:
        sub_dir=False
        for arg in args:
            if arg[0] == '-':
               if 'r' in arg:
                  sub_dir=True
            else:
                if os.isfile(arg):
                    os.remove(arg)
                elif os.isdir(arg) and sub_dir:
                    shutil.rmtree(arg)
                else:
                    print('''can't delete directory:{}'''.format(arg))

def update(*args):
    rc={}
    rc['rc']=True
    if len(args) and args[0] == '![k7]':
        if do_cmd: # speical person can build
            print('build....')
            src_file='k/k.py'
            soc=kNet.gen_client_socket(ip,port)
            kNet.put_packet(soc,kNet.packet(cmd='build:k',dest=src_file))
            rc=kNet.get_packet(soc)
            if not rc['rc']:
                print(rc['data'])
                return
        else:
            print('not support')
            return
    if rc['rc']: # anybody can update
       print('Download....')
       soc=kNet.gen_client_socket(ip,port)
       kNet.put_packet(soc,kNet.packet(cmd='get:update',dest='k/dist/k'))
       save_file='/tmp/k.up'
       rc=kNet.get_packet(soc,progress='Downloading : k => %s'%(save_file))
       if rc['rc']:
           with open(save_file,'w') as f:
               f.write(rc['data'])
           os.chmod(save_file, stat.S_IRWXU)
    if not rc['rc']:
       print(rc['data'])

def vi(*args):
    '''
    vi command
    '''
    md5=None
    src_file=''
    tmp_file=args[0]
    src_a=None
    kdb=False
    if tmp_file[0] == '!':
        kdb=True
        src_file=tmp_file[1:]
        src_a=src_file.split('/')
        cmdfile=False

        if not do_cmd and src_a[0] in ['[k7]','cmd','lib','[hosts]']:
            print('not support!!')
            os._exit(1)

        if do_cmd:
            if src_a[0] == '[k7]': # special k
                src_a=[]
                src_a.append('k') # src_a[0]
                src_a.append('k') # src_a[1]
                src_file='k/k.py'
            elif src_a[0] == '[hosts]': # special k
                src_a=[]
                src_a.append('khosts') # src_a[0]
                src_file='hosts/hosts'
            elif src_a[0] in ['cmd','lib']: # lib and cmd
                src_file='/'.join(src_a[1:])
                cmdfile=True
        tmp_file='/tmp/.{}'.format(os.path.basename(src_file))
        soc=kNet.gen_client_socket(ip,port)
        if do_cmd and src_a[0] == 'k':
            kNet.put_packet(soc,kNet.packet(cmd='get:k7',dest=src_file))
        elif do_cmd and src_a[0] in ['cmd','lib']:
            kNet.put_packet(soc,kNet.packet(cmd='get:{}'.format(src_a[0]),dest=src_file))
        elif do_cmd and src_a[0] == 'khosts':
            kNet.put_packet(soc,kNet.packet(cmd='get:{}'.format(src_a[0]),dest=src_file))
        else:
            kNet.put_packet(soc,kNet.packet(cmd='get',dest=src_file))
        rc=kNet.get_packet(soc)
        if rc['rc']: # exist in the DB
            with open(tmp_file,'w') as f:
                f.write(rc['data'])
            md5=LIBS().md5(rc['data'])
        else: # not found any file in the DB, so make a template
            file_ext=src_file.split('.')[-1]
            now_time=datetime.now().strftime('%Y-%m-%d %H:%M')
            tmp_vi_data=None
            if (src_a[0] == 'cmd' and src_a[1] == 'shell') or file_ext == 'sh':
                tmp_vi_data='''#!/bin/bash
# Copyright CEP(Kage Park)
# license : GPL
# CEP Library for Python
# Created at {0} by K
_k_version="1.0.1"

# FUNC CODE HERE

if [ "$([ -n "$0" ] && basename "$0")" == "{1}" ]; then
    # RUN CODE HERE
    RC=""
fi
'''.format(now_time,os.path.basename(src_file))
            elif (src_a[0] == 'lib' and src_a[1] == 'shell'):
                tmp_vi_data='''#!/bin/bash
# Copyright CEP(Kage Park)
# license : GPL
# CEP Library for Python
# Created at {0} by K
_k_version="1.0.1"

'''.format(now_time)
            elif (src_a[0] == 'cmd' and src_a[1] == 'python') or file_ext == 'py':
                tmp_vi_data='''#!/usr/bin/env python
# python ~2.9 for no newline on print()
# from __future__ import print_function
# coding: utf-8
# encoding=utf8
#
# Copyright CEP(Kage Park)
# license : GPL
# Created at {0} by K
_k_version="1.0.1"

# FUNC CODE HERE

if __name__ == '__main__': 
    # RUN CODE HERE
    pass
'''.format(now_time)
            elif (src_a[0] == 'lib' and src_a[1] == 'python'):
                tmp_vi_data='''#!/usr/bin/env python
# python ~2.9 for no newline on print()
# from __future__ import print_function
# coding: utf-8
# encoding=utf8
#
# Copyright CEP(Kage Park)
# license : GPL
# Created at {0} by K
_k_version="1.0.1"

'''.format(now_time)
            if tmp_vi_data:
                with open(tmp_file,'w') as f:
                    f.write(tmp_vi_data)
   
    if find_executable('vim'):
        #vimrc=LIBS().vim_rc()
        #os.system('vim -u ' + vimrc + ' ' + tmp_file)
        os.system('vim {}'.format(tmp_file))
        #os.remove(vimrc)
    elif find_executable('vi'):
        LIBS().run_shell('vi ' + tmp_file)
    else: # Symple text edit code here
        print('not yet. please intall vim')
        return
    if kdb:
        soc=kNet.gen_client_socket(ip,port)
        if do_cmd and src_a[0] in ['cmd','lib','k','khosts']: # special case
            with open(tmp_file,'r') as f:
                data=f.read()
                new_md5=LIBS().md5(data)
                if new_md5 != md5:
                    data=LIBS().version_up(data) # update version
                    if src_a[0] == 'k':
                        kNet.put_packet(soc,kNet.packet(cmd='save:k7',dest=src_file,data=data)) # auto trash
                    else:
                        kNet.put_packet(soc,kNet.packet(cmd='save:{}'.format(src_a[0]),dest=src_file,data=data)) #o trash
                    krc=kNet.get_packet(soc)
                    print(krc['data'])
            if do_cmd and src_a[0] == 'khosts': # update hosts
#                soc=kNet.gen_client_socket(ip,port)
                kNet.put_packet(soc,kNet.packet(cmd='get:hosts'))
                host=kNet.get_packet(soc)
                if host['rc']:
                    cmd_dict=LIBS().get_cmd_dict(k_tmp_db)
                    cmd_dict['hosts']=host['data']
                    kd.save_db(k_tmp_db,cmd_dict)
        else: # normal case
           if md5: # get file case
               rc=LIBS().trash(src_file) # then trash for backup
           krc=LIBS().push(tmp_file,src_file) # save to db
        if os.path.isfile(tmp_file):
            os.remove(tmp_file)

def imports(*args):
    '''
    Import python module
    '''
    # if running local daemon then it will work, if not work
    if os.path.exists(k_pipe_file):
        pipeout = os.open(k_pipe_file, os.O_WRONLY)
        os.write(os.open(k_pipe_file, os.O_WRONLY),'CMD._imports '+ LIBS().list2str(args)+'\n')
    else:
        #CMD()._imports(*args)
        print('not support')

def ssh(*args):
    '''
    SSH Command without hostkey check
    '''
    #if find_executable('ssh'):
    if os.path.isfile('/usr/bin/ssh'):
        ssh_opt='-o StrictHostKeychecking=no -o CheckHostIP=no -o ConnectTimeout=5 -o UserKnownHostsFile=/dev/null' 
#        cmd_dict=LIBS().get_cmd_dict(k_tmp_db)
#        if 'hosts' in cmd_dict:
#            host_ip,host_key=LIBS().get_ssh_host(args,cmd_dict['hosts'])
#            print(host_ip,host_key)
        LIBS().run_shell('ssh {0} '.format(ssh_opt) + LIBS().list2str(args))
    else: # Simple ssh code with python only
        pass

def cat(*args):
    if len(args) > 0:
        if args[0][0] == '!':
            for arg in args[0][1:].split(','):
                cmd='get'
                filename=arg
                if do_cmd:
                   dest_arr=arg.split('/')
                   if dest_arr[0] in ['lib','cmd']:
                       cmd='get:{}'.format(dest_arr[0])
                       filename='/'.join(dest_arr[1:])
                soc=kNet.gen_client_socket(ip,port)
                kNet.put_packet(soc,kNet.packet(cmd=cmd,dest='{0}'.format(filename)))
                rc=kNet.get_packet(soc)
                if rc['rc']:
                    print("""%s"""%(rc['data'])) # recover data to readable data
                else:
                    print('''%s'''%(rc))
        else:
            if find_executable('cat'):
                os.system('cat {}'.format(LIBS().list2str(args)))
            else:
                for arg in list(args):
                    if arg[0] != '-' and os.path.isfile(arg):
                        with open(arg,'r') as f:
                            pp=f.read()
                        print(pp)

def ls(*args):
    '''show file list, db list, module list'''
    krun=False
    args_list=list(args)
    del_list=[]
    if len(args) > 0:
        for arg in args_list:
            if arg[0] == '!':
                krun=True
                soc=kNet.gen_client_socket(ip,port)
                arg_list=arg[1:].split('/')

                if do_cmd:
                    if arg_list[0] in ['cmd','lib']:
                        src_file='/'.join(arg_list[1:])
                        kNet.put_packet(soc,kNet.packet(cmd='ls:{}'.format(arg_list[0]),dest=src_file))
                    else:
                        kNet.put_packet(soc,kNet.packet(cmd='ls:[k]',dest='{0}'.format(arg[1:])))
                else:
                    kNet.put_packet(soc,kNet.packet(cmd='ls',dest='{0}'.format(arg[1:])))
                rc=kNet.get_packet(soc)
                if rc['rc']:
                    for ii in rc['data']:
                        print("""%s"""%(ii)) # recover data to readable data
                else:
                    print('''%s'''%(rc['data']))
            elif arg == '[module]':
                krun=True
                if os.path.exists(k_pipe_file):
                    pipeout = os.open(k_pipe_file, os.O_WRONLY)
                    os.write(os.open(k_pipe_file, os.O_WRONLY),'CMD._mod_ls\n')
                else:
                    print(CMD()._mod_ls())
            elif arg == '[bin]':
                krun=True
                cmd_dict=LIBS().get_cmd_dict(k_tmp_db) # it can use global variable from __main__?
                print('[python]')
                func_list=list(LIBS().get_function_list_in_module_name(this_mod_name).keys())
                if 'cmd' in globals():
                    func_list=func_list+list(LIBS().get_function_list_in_module_name('cmd').keys())
                if 'python' in cmd_dict:
                    func_list=func_list+list(cmd_dict['python'].keys())
                print(LIBS().list2str(func_list,'\t'))
                if 'shell' in cmd_dict:
                    print('[shell]')
                    print(LIBS().list2str(list(cmd_dict['shell'].keys()),'\t'))
            elif arg == '[hosts]':
                krun=True
                cmd_dict=LIBS().get_cmd_dict(k_tmp_db)
                if 'hosts' in cmd_dict:
                    for ii in cmd_dict['hosts'].keys():
                        if 'key' in cmd_dict['hosts'][ii]:
                            print('{} : {} *'.format(cmd_dict['hosts'][ii]['ip'],ii))
                        else:
                            print('{} : {}'.format(cmd_dict['hosts'][ii]['ip'],ii))
    if not krun:
        #LIBS().run_shell('ls --color ' + LIBS().list2str(args_list)) # Linux
        LIBS().run_shell('ls ' + LIBS().list2str(args_list)) # Mac

def host_ip(*argv):
    '''
       Get local host's host name and IP
       or
       Get IP from input hostname
    '''
    if len(argv) == 0:
        print(kNet.host_ip())
    else:
        cmd_dict=LIBS().get_cmd_dict(k_tmp_db)
        ip=kNet.host_ip(argv[0],cmd_dict)
        if ip:
            print(ip)
        else:
            print('')
        
def pcc(*args):
    if not find_executable('pyinstaller'):
        if find_executable('pip'):
            os.system('pip install pyinstaller')
        else:
            if hasattr(pip, 'main'):
                pip.main(['install', 'pyinstaller'])
            else:
                pip._internal.main(['install', 'pyinstaller'])
    args=list(args)
    if os.path.isfile(args[-1]):
        lib_path=''
        for ii in range(0,len(args)):
            if args[ii] == '-L':
                if lib_path:
                    lib_path='{} -p {}'.format(lib_path,args[ii+1])
                else:
                    lib_path='-p {}'.format(args[ii+1])
        os.system('pyinstaller {} --onefile {}'.format(lib_path,args[-1]))

def reload(*argv):
    if len(argv) and argv[0] == 'hosts':
        soc=kNet.gen_client_socket(ip,port)
        kNet.put_packet(soc,kNet.packet(cmd='get:hosts'))
        host=kNet.get_packet(soc)
        if host['rc']:
            cmd_dict=LIBS().get_cmd_dict(k_tmp_db)
            cmd_dict['hosts']=host['data']
            kd.save_db(k_tmp_db,cmd_dict)
    else:
        LIBS().get_cmd_dict(k_tmp_db,force=True)

def mkdir(*argv):
    if do_cmd and len(argv) ==1 and argv[0][0] == '!':
        soc=kNet.gen_client_socket(ip,port)
        category=argv[0][1:].split('/')[0]
        if not category:
            category='[k]'
        kNet.put_packet(soc,kNet.packet(cmd='mkdir:{}'.format(category),dest=argv[0][1:]))
        rc=kNet.get_packet(soc)
        if rc['rc']:
            print(rc['data'])
        else:
            print('not support')
    else:
        os.system('mkdir {}'.format(LIBS().list2str(argv)))

def find(*argv):
    if len(argv) >=2 and argv[1][0] == '!':

        soc=kNet.gen_client_socket(ip,port)
        if do_cmd:
            category=argv[1][1:].split('/')[0]
            if not category:
                category='[k]'
            kNet.put_packet(soc,kNet.packet(cmd='find:{}'.format(category),dest=argv[1][
1:],scmd=argv[0]))
        else:
            kNet.put_packet(soc,kNet.packet(cmd='find',dest=argv[1][1:],scmd=argv[0]))
        rc=kNet.get_packet(soc)
        if rc['rc']:
            for ii in rc['data']:
                print(ii)
        else:
            print('not support')
    else:
        os.system('find {}'.format(LIBS().list2str(argv)))

def grep(*argv):
    if len(argv) >=2 and argv[1][0] == '!':
        
        soc=kNet.gen_client_socket(ip,port)
        if do_cmd:
            category=argv[1][1:].split('/')[0]
            if not category:
                category='[k]'
            kNet.put_packet(soc,kNet.packet(cmd='grep:{}'.format(category),dest=argv[1][1:],scmd=argv[0]))
        else:
            kNet.put_packet(soc,kNet.packet(cmd='grep',dest=argv[1][1:],scmd=argv[0]))
        rc=kNet.get_packet(soc)
        if rc['rc']:
            for ii in rc['data']:
                print(ii)
        else:
            print('not support')
    else:
        os.system('grep {}'.format(LIBS().list2str(argv)))

def logout(MyD,*argv):
    os.system('echo quit > {}'.format(k_pipe_file))
    cmd_dict=LIBS().get_cmd_dict(k_tmp_db)
    cmd_dict.pop('login',None)
    kd.save_db(k_tmp_db,cmd_dict)
    if MyD:
       MyD.stop()
    if os.path.exists(k_pip_file):
       os.remove(k_pip_file)
    if os.path.exists(k_pipe_file):
       os.remove(k_pipe_file)

def login(MyD,*argv):
    cmd_dict=LIBS().get_cmd_dict(k_tmp_db)
    if LIBS().check_login(cmd_dict):
        print('Already loged in')
    else:
        user_account=raw_input("User name: ")
        user_password=getpass.getpass('Password: ')
        soc=kNet.gen_client_socket(ip,port)
        kNet.put_packet(soc,kNet.packet(cmd='login:{}'.format(user_account),dest=user_password))
        rc=kNet.get_packet(soc)
        if rc['rc']:
            if os.path.exists(k_pipe_file):
                os.remove(k_pipe_file)
            cmd_dict['login']=int(datetime.now().strftime("%s"))
            kd.save_db(k_tmp_db,cmd_dict)
            MyD.start()
        else:
            print('incorrect user/password')

def ping(*argv):
    if find_executable('ping'):
        os.system('ping {}'.format(LIBS().list2str(argv)))
    else:
        try:
            cmd_dict=LIBS().get_cmd_dict(k_tmp_db)
            LIBS().code_run(cmd_dict,'ping')
        except:
            print(CMD()._ping(*argv))

def vimrc(*args):
    filename='~/.vimrc'
    addition_option=''
    if len(args):
        filename=args[0]
        if len(args) == 2:
            addition_option=args[1]
    vimrcfp=open(filename,"w")
    vimrc_str='''set mouse=a
set title
set encoding=utf-8
set cursorline
syntax on
set expandtab
set shiftwidth=4
set softtabstop=4
autocmd FileType c,cpp setlocal expandtab shiftwidth=2 softtabstop=2 cindent
autocmd FileType python setlocal expandtab shiftwidth=4 softtabstop=4 autoindent
{}'''.format(addition_option)
    vimrcfp.write(vimrc_str)
    vimrcfp.close()

def test(*argv):
    print('++test():',argv)

class MyDaemon(Daemon):
    def run(self):
        pipe_th=threading.Thread(target=pipe,args=(k_pipe_file,))
        pipe_th.start()
        
if __name__ == '__main__':
    ip="kage.cep.kr"
    port=7992
    username=getpass.getuser()
    hostname=socket.gethostname()
    cmd_name='{}'.format(sys.argv[0].split('/')[-1])
    cmd_module=[this_mod_name]
    do_cmd=False
    MyD=MyDaemon(k_pip_file)

    # 1st self command
    if cmd_name == 'k' and len(sys.argv) == 1: # K Shell
        print('command not found')
    else:
        if cmd_name == 'k': # Instance K Command
            if sys.argv[1] == '-v' or sys.argv[1] == '--version':
                print(_k_version)
                sys.exit(0)
            elif sys.argv[1] == 'login':
                login(MyD)
                sys.exit(0)
            elif sys.argv[1] == 'logout':
                logout(MyD)
                sys.exit(0)
            sys.argv.pop(0)
            cmd_name=sys.argv[0]
            self_k=True

        cmd_dict=LIBS().get_cmd_dict(k_tmp_db)
        if LIBS().check_login(cmd_dict,MyD=MyD):
            do_cmd=True
        # 2nd python (self, imported) command
        for mod in cmd_module:
            if cmd_name in LIBS().get_function_list_in_module_name(mod):
                sys.argv.pop(0)
                rc=LIBS().run_function_name_argv(km.get_module_obj(mod,globals()),cmd_name,*tuple(sys.argv))
                if type(rc) == set:
                    LIBS().exit(code=rc)
                else:
                    if rc:
                         print(rc)
                sys.exit()
        # 3rd, code command (shell,python)
        LIBS().code_run(cmd_dict,cmd_name)
        # 4th Linux OS commane
        os.system('PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin; SHELL=$([ -f /bin/bash ] && echo /bin/bash || /bin/sh); ' + LIBS().list2str(sys.argv))

