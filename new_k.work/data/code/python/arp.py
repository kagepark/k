from scapy import *

def mac_getter(self, IP):

        # Sending ARP for take the MAC address
        ans, unans = srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=IP), timeout=2, iface=self.interface, inter=0.2)

        for send, receive in ans:
            return receive.sprintf(r"%Ether.src%") 

def arping(mac, ip, timeout):
    """Arping function takes IP Address or Network, returns nested mac/ip list"""
    for i in range(timeout/2):
        ans = srp1(Ether(dst=mac)/ARP(pdst=ip), timeout=2, verbose=0)
        if ans: break
    return ans 


def update_cache(self):
        try:
            ans, unans = arping(self._router_ip + "/24",
                                iface=self._interface, verbose=False)

            self._arp_cache = []

            if ans:
                for s, r in ans:
                    self._arp_cache.append([r[ARP].psrc, r[Ether].src.lower()])

            _LOGGER.debug("ARP cache: %s", self._arp_cache)
        except Exception as e:
            _LOGGER.error("Error when trying update ARP cache: %s", str(e)) 


update_cache()
