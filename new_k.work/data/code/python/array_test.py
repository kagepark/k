# coding: utf-8

from array import *

#arrayIdentifierName = array(typecode, [Initializer,....])
#'b’ -> Represents signed integer of size 1 byte
#‘B’ -> Represents unsigned integer of size 1 byte
#‘c’ -> Represents character of size 1 byte
#‘u’ -> Represents unicode character of size 2 bytes
#‘h’ -> Represents signed integer of size 2 bytes
#‘H’ -> Represents unsigned integer of size 2 bytes
#‘i’ -> Represents signed integer of size 2 bytes
#‘I’ -> Represents unsigned integer of size 2 bytes
#‘w’ -> Represents unicode character of size 4 bytes
#‘l’ -> Represents signed integer of size 4 bytes
#‘L’ -> Represents unsigned integer of size 4 bytes
#‘f’ -> Represents floating point of size 4 bytes
#‘d’ -> Represents floating point of size 8 bytes



aa=array("i",[10,20,30])
for ii in aa:
	print (ii)
	
aa.append(6)
aa.insert(1,55)

print (aa)
print (aa[3])

# add bb to aa
bb=[1,2,3]
aa.fromlist(bb)
print (aa)

#remove data at the index number(return value is the removed data)
uu=aa.pop(2)
print ("pop(2):",uu)

print ("aa[2]:",aa[2])

print (aa)

# find '2' and remove data '2' in array aa
aa.remove(2)
print (aa)

#how many 2 in aa array?
print (aa.count(2))
#how many 10 in aa array ?
print (aa.count(10))

# reverse array data
aa.reverse()
print (aa)

tolist=aa.tolist()
print (tolist)
	
#aa=array("i",["aa","bb"])
#for ii in aa:
#	print ii
	
	
uu = ['physics', 'chemistry', 1997, 2000];
print ("uu[1]:",uu[1])
print ("uu[1~]:",uu[1:])
print ("uu count:",len(uu))

kk=uu.count('physics')
print (kk)

# find value's index number, if not found then error
kk=uu.index(1997)
print (kk)

kk=uu.index('physics')
print (kk)
