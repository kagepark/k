import requests

def tw_autopxe(tester=None,eth_mac=None,rc_uri=None,django_server=None,djangoId=None,os=None,test_file='sumTester'):

    if eth_mac is None or tester is None or rc_uri is None or django_server is None or djangoId is None:
        return False
    eth_mac_str=eth_mac.replace(':','-')
    tester_str=tester.replace(' ','')
    if os is None:
        os='cburn-r74'

    #cburn_str='{3} DIR=sumtester/{1}/{0} RC=http://{2}/sumTester.cburn.RC.sh test_file={4}'.format(djangoId,django_server,rc_uri,os,test_file)
    cburn_str='cburn-r74 DIR=sumtester/172.16.115.130/1110 RC=http://172.16.115.130/sumTester.cburn.RC.sh test_file=sumTester sum_file=sum.2.2.0_022019 db_file=env-73103xn_/sum_env.cfg log_file=sumTester_v2.2.0-20190220_10.141.173.169_20190225140425.log'
    data={'tester':'{0}'.format(tester),'macaddr':'{0}'.format(eth_mac_str),'command':cburn_str,'submit':'Submit'}
    print(len(cburn_str))
    print(data)

    host_url='http://10.135.0.253/self_register/'
    ss = requests.Session()
    r = ss.post(host_url, data=data)
    return r

#    for i in range(0,30):
#        try:
#            if data is None:
#                r = ss.get(host_url)
#            else:
#                r = ss.post(host_url, data=data)
#            return r
#        except requests.exceptions.RequestException as e:
#            log_format("Django server has no response (wait 10sec: {0}/30)".format(i))
#        time.sleep(10)
#    return False

#def unset_tw_autopxe(eth_mac=None):
#    if eth_mac is None:
#        return False
#    eth_mac_str=eth_mac.replace(':','-')
#    host_url='http://10.135.0.253/self_register/?r_macaddr={0}&action={1}'.format(km.str2url(eth_mac),'Remove')
#    r=requests.get(host_url)
#    print(r.text)

def unset_tw_autopxe(eth_mac=None):

    if eth_mac is None:
        return False
    eth_mac_str=eth_mac.replace(':','-')
    data={'r_macaddr={0}'.format(eth_mac_str):'Remove'}
    print(data)

    host_url='http://10.135.0.253/self_register/'
    ss = requests.Session()
    r = ss.post(host_url, data=data)
    return r



def set_cburn_auto_pxe(mac=None,djangoId=None,sum_file=None,smc_ipmi_file=None,db_file=None,log_file=None,bios_file=None,bmc_file=None,down_bios_file=None,down_bmc_file=None,cmm_file=None,psu_file=None,raid_file=None,django_server=None,rc_uri=None):
    if mac is None or djangoId is None:
        return False

    if django_server is None:
       django_server='172.16.115.130'
    if rc_uri is None:
       rc_uri='172.16.115.130'

    cburn_str='cburn-r74 DIR=sumtester/{1}/{0} RC=http://{2}/sumTester.cburn.RC.sh'.format(djangoId,django_server,rc_uri)

    host_url='http://10.135.0.253/self_register/?command={0}&address={1}&action={2}'.format(km.str2url(cburn_str),km.str2url(mac),'Update')
    data={'tester':'Kage','macaddr':'ac-1f-6b-0c-00-2c','command':cburn_str}

    r=requests.get(host_url)
    if os.path.isfile(sys_log_file):
        km.logging("Set CBURN PXE ({0})({1})".format(mac,cburn_str))


def unset_cburn_auto_pxe(mac=None):
    host_url='http://10.135.0.253/self_register/?r_macaddr={0}&action={1}'.format(km.str2url(mac),'Remove')
    r=requests.get(host_url)
    if os.path.isfile(sys_log_file):
        km.logging("Unset CBURN PXE ({0})".format(mac))

#set_cburn_auto_pxe(mac=None,django_server=None,rc_uri=None)
a=tw_autopxe(tester='Kage',eth_mac='ac:1f:6b:0c:00:2c',rc_uri='172.31.32.180',django_server='172.31.32.180',djangoId=10,test_file='sumTester3')
#a=unset_tw_autopxe(eth_mac='ac:1f:6b:0c:00:2c')
print(a)
