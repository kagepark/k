#!/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
##########################################################################
# sumTester for Supermicro Update Manager
# Kage Park 
#  07/03/2018 : Initial version (Convert from sumTester.sh(v1.0) by Ryan Chu) (v1.2)
#  07/12/2018 : Add auto activate key and enhanced summary and code
#  07/13/2018 : Add options on CLI, reduce typo error code, add pass condition 
#  07/18/2018 : Add send data to Django server (v1.3)
#  07/20/2018 : Add ProductKey function 
#  07/26/2018 : Filter out unknown command (each version support different commands)
#               Support Celery & email to report
#  08/02/2017 : Support multi IPMI ( -l ipmi_table.txt ) option.
#               But, the fancy UI of output result is not correctly display yet.
##########################################################################
version="1.3"
import subprocess,os,sys,signal
from termcolor import colored, cprint
from datetime import datetime
import tempfile
import shutil
import filecmp
import time
import re
import traceback
run_log='\n'
ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')

def error_exit(msg=None,log=True):
    if msg is not None:
       if log:
          log_print(msg)
       else:
          print(msg)
    sys.exit(-1)


def rshell(cmd,timeout=None):
    Popen=subprocess.Popen
    PIPE=subprocess.PIPE
    p = Popen(cmd , shell=True, stdout=PIPE, stderr=PIPE)
    out=None
    err=None
    try:
        if timeout is None:
            out, err = p.communicate()
        else:
            out, err = p.communicate(timeout=timeout)
        if py3:
            return p.returncode, out.rstrip().decode("utf-8"), err.decode("utf-8")
        else:
            return p.returncode, out.rstrip(), err
    except subprocess.TimeoutExpired:
        p.kill()
        return p.returncode, 'Kill process after timeout ({0} sec)'.format(timeout), 'Error: Kill process after Timeout {0}'.format(timeout)




bios_info=rshell("./sum -c GetBIOSInfo --file X11SDV8.528 -i 172.31.36.176 -u ADMIN -p ADMIN")

print(bios_info)

#[sudo] password for kage: 
#Supermicro Update Manager (for UEFI BIOS) 2.1.0 (2018/06/22) (x86_64)
#Copyright(C)2018 Super Micro Computer, Inc. All rights reserved.
#..................................................
#...................
#Managed system...........172.31.36.176
#    Board ID.............0824
#    BIOS build date......2016/8/12
#Local BIOS image file....X11SDV8.528
#    Board ID.............0986
#    BIOS build date......2018/5/28
