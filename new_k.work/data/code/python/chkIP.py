import re

def chkIP2(ip):
    ip_arr=ip.split(".")
    if len(ip_arr) != 4:
        return False
    for ii in ip_arr:
        try:
            dii=int(ii)
        except:
            return False
        if dii < 0 or dii > 255:
            return False
    return True

def chkIP(ip):
    ip_re = re.compile(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
    ip_s = ip_re.search(ip)
    if not ip_s:
         return False
    for ii in ip.split("."):
         if int(ii) < 0 or int(ii) > 255:
             return False
    return True

aa="172.16.0.321"
cc=chkIP(aa)
print(aa,cc)
aa="172.16.0.21"
cc=chkIP(aa)
print(aa,cc)
aa="0.0.0.0"
cc=chkIP(aa)
print(aa,cc)
aa="10.0.1"
cc=chkIP(aa)
print(aa,cc)
aa="1.0.0.1"
cc=chkIP(aa)
print(aa,cc)
aa="1234.52"
cc=chkIP(aa)
print(aa,cc)
aa="123.2..3"
cc=chkIP(aa)
print(aa,cc)
aa="123.-2.3.3"
cc=chkIP(aa)
print(aa,cc)
aa="123.e.3.3"
cc=chkIP(aa)
print(aa,cc)
