import cmd, sys
import io

import os

if os.name=='nt':
    import msvcrt
    def getAnyKey():
        if msvcrt.kbhit():
            return msvcrt.getch()
        return None
else:
    import sys
    import select
    import tty
    import termios
    import atexit        
    def isData():
        #return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])    
        #aa=select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])    
        return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])    
#    old_settings = termios.tcgetattr(sys.stdin)    
#    def restoreSettings():
#        global old_settings
#        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)        
#    atexit.register(restoreSettings)            
    def getAnyKey():
        try:
            if isData():
                return sys.stdin.read(1)
#            return None
        except:
            pass
        return None

class MyShell(cmd.Cmd):
    prompt = '> '
    file = None
    realstdin = sys.stdin
    mocking=False
    breakReadLine=False
    use_rawinput=0
    # ----- basic commands -----

    def do_bye(self, arg):
        'Stop recording, close the terminal, and exit:  BYE'
        print('Exiting.')
        sys.exit(0)
        return True

    def emptyline(self):
        pass

    def myReadLine(self):
        sys.stdout.flush()
        self.breakReadLine=False
        line=''
        while not self.breakReadLine:
#            c=getAnyKey()          
            c=sys.stdin.readline()
            if c in ['\r','\n','\r\n']:
              sys.stdout.flush()
              return line
            else:
              print('  ',end='')
              sys.stdout.flush()
              line+=c
#            if c is None:
#                self.breakReadLine=True
#                return 'No'
#            else:
#            if not c is None:
#            if type(c) is not bool and not c is None:
#                if type(c) is bytes:
#                    c=c.decode("utf-8")              
#                if c=='\x08' and len(line):
#                    line=line[0:-1]
#                if len(line):
#                    line=line+c
#                elif c in ['\r','\n','\r\n']:
#                if c in ['\r','\n','\r\n']:
#                    print('\n')
#                    sys.stdout.flush()
#                    return line
#                else:
#                    line=line+c
#                    print('  ',end='')
#                    sys.stdout.flush()


    def mycmdloop(self, intro=None):
        """Repeatedly issue a prompt, accept input, parse an initial prefix
        off the received input, and dispatch to action methods, passing them
        the remainder of the line as argument.

        """
        self.preloop()
        if self.use_rawinput and self.completekey:
            try:
                import readline
                self.old_completer = readline.get_completer()
                readline.set_completer(self.complete)
                readline.parse_and_bind(self.completekey+": complete")
            except ImportError:
                pass
        try:
            if intro is not None:
                self.intro = intro
            if self.intro:
                self.stdout.write(str(self.intro)+"\n")
            stop = None
            line=''
            while not stop:
                print('******************\ncmdqueue:',self.cmdqueue)
                print('line:',line)
                if self.cmdqueue:
                    line = self.cmdqueue.pop(0)
                else:
#                    if self.use_rawinput:
#                        try:
#                            print(self.prompt,end='')
#                            line = self.myReadLine()#input(self.prompt)
#                        except EOFError:
#                            line = 'EOF'
#                    else:
                        self.stdout.write(self.prompt)
                        self.stdout.flush()
                        line = self.myReadLine()#self.stdin.readline()
#                        print('1:{0}'.format(line))
                #if not line is None:
                if line:
                    print('2:{0}'.format(line))
                    line = line.rstrip('\r\n')
                    line = self.precmd(line)
                    print('3:{0}'.format(line))
                    stop = self.onecmd(line)
                    print('4:{0}'.format(stop))
                    stop = self.postcmd(stop, line)
                    print('5:',line,stop)
            self.postloop()
            print('self.postloop() is end cmd line?')
        finally:
            pass
#            if self.use_rawinput and self.completekey:
#                print('???')
#                try:
#                    import readline
#                    readline.set_completer(self.old_completer)
#                except ImportError:
#                    pass

    def cmdloop_with_keyboard_interrupt(self, intro):
        doQuit = False
        while doQuit != True:
            try:
                if intro!='':
                    cintro=intro
                    intro=''                
                    self.mycmdloop(cintro)
                else:
                    self.intro=''                
                    self.mycmdloop()
                doQuit = True
            except KeyboardInterrupt:
                sys.stdout.write('\n')

def parse(arg):
    'Convert a series of zero or more numbers to an argument tuple'
    return tuple(map(int, arg.split()))

if __name__ == '__main__':
    #MyShell().cmdloop()
    MyShell().cmdloop_with_keyboard_interrupt('*** Terminal ***\nType help or ? to list commands.\n')
