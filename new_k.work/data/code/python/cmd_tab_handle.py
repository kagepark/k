from __future__ import print_function
import sys, os
import termios, fcntl
import select

fd = sys.stdin.fileno()
newattr = termios.tcgetattr(fd)
newattr[3] = newattr[3] & ~termios.ICANON
newattr[3] = newattr[3] & ~termios.ECHO
termios.tcsetattr(fd, termios.TCSANOW, newattr)

oldterm = termios.tcgetattr(fd)
oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)

CMDLISTS=['help','ls','quit','cd','size','import']
print("Type some stuff")
line=''
keep_new_line=False
before_end=''
while True:
    inp, outp, err = select.select([sys.stdin], [], [])
    c = sys.stdin.read()
    if len(line.split()):
        before_end=line.split()[-1]
    if c == '\t' and keep_new_line is False:
        text=line.split('\n')[-1].split()
        last_word=''
        if len(text):
            last_word=text[-1]
        if not last_word:
            print(CMDLISTS[:])
        else:
            if len(last_word.split('/'))>1:
                CMDLISTS = [f for f in os.listdir(os.path.dirname(last_word)) if os.path.isfile(f)]
            else:
                CMDLISTS = [f for f in os.listdir('.') if os.path.isfile(f)]
            completions = [ f for f in CMDLISTS if f.startswith(last_word) ]
            found=len(completions)
            if found==1:
                adding_str=completions[0][len(last_word):]
                line+=adding_str
                print(adding_str,end='')
            elif found>1:
                print(completions[:])
                
    elif c == 'q':
        termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
        break
    elif c in ['\r','\n','\r\n']:
        if before_end in ['{',':','\\']:
            keep_new_line=True
        if keep_new_line and ( before_end == '}' or before_end in ['\r','\n','\r\n']): # return line
            keep_new_line=False
            print(line)
            line=''
        elif keep_new_line is False: # return line
            keep_new_line=False
            print(line)
            line=''
    if c != '\t' and keep_new_line is False:
        print(c,end='') # type prompt 
        sys.stdout.flush()
        line+=c # Make a line
    elif keep_new_line:
        print(c,end='') # type prompt 
        sys.stdout.flush()
        line+=c # Make a line

# Reset the terminal:
termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
#
