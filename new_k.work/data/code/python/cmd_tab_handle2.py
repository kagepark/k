from __future__ import print_function
import sys, os
import termios, fcntl
import select

fd = sys.stdin.fileno()
oldterm = termios.tcgetattr(fd)
oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)

newattr = termios.tcgetattr(fd)
newattr[3] = newattr[3] & ~termios.ICANON
newattr[3] = newattr[3] & ~termios.ECHO
termios.tcsetattr(fd, termios.TCSANOW, newattr)

def list_in_dir(dir_name):
    if dir_name:
        if os.path.isdir(dir_name):
            return os.listdir(dir_name)

def completion(cmd_list,find_word=None,):
    if cmd_list:
        if find_word:
            return [ f for f in cmd_list if f.startswith(find_word) ]
        else:
            return cmd_list[:]
    return []

def line_dbg(line):
    before_end=''
    before_first=''
    line_end=''
    line_first=''
    if len(line):
        line_arr=line.split('\n')
        current_line=line_arr[-1]
        current_line_a=current_line.split()
        if len(current_line_a):
            line_first=current_line_a[0]
            if len(current_line_a) > 1:
                line_end=current_line_a[-1]
        else:
            line_first='\n'
            line_end='\n'
        if len(line_arr) >1:
            before_line=line_arr[-2]
            before_line_a=before_line.split()
            if len(before_line_a):
                before_first=before_line_a[0]
                if len(before_line_a) > 1:
                    before_end=before_line_a[-1]
            else:
                before_first='\n'
                before_end='\n'
    return before_first,before_end,line_first,line_end

CMDLISTS=['help','ls','quit','cd','size','import']
print("Type some stuff")
line=''
keep_new_line=False
before_end=''
prompt='kage: '

print(prompt,end='')
sys.stdout.flush()
# sys.stdout.write(xxx)
#CURSOR_UP_ONE = '\x1b[1A'
#ERASE_LINE='\x1b[2K'
#ERASE_LINE='\033[K'
#BACK_PREVIOUS_LINE='\033[F'

# CURSOR_UP : \033[F
cursor=0
history=[]
history_idx=0
while True:
    inp, outp, err = select.select([sys.stdin], [], [])
    c = sys.stdin.read()
    # analysis line
    before_first,before_end,line_first,line_end=line_dbg(line)
    # code more for Left(^[[D)/right(^[[C)/Up(^[[A)/Down(^[[B)/Delete(^[[3~)/Back space
    # ^[ => \x1b
    # Tab command 
#    for i in c:
#       print('KEY({0})'.format(ord(i)))
    if c == '\x1b[3~': # delete key
        if cursor < len(line):
            front=line[:cursor]
            backend=line[cursor+1:]
            sys.stdout.write(backend+' ')
            sys.stdout.write('\033[{0}D'.format(len(backend)+1))# 1 move left, if 3 move then '\033[3D'
            line=front+backend
    elif c == '\x1b[D': # LEFT
        if cursor >0:
            sys.stdout.write('\b')
            cursor-=1
            #sys.stdout.write('\033[1D')# 1 move left, if 3 move then '\033[3D'
    elif c == '\x1b[C': # Right
        if cursor < len(line):
            sys.stdout.write('\033[1C') # 1 move right, if 3 move then '\033[3C'
            cursor+=1
    elif c == '\x1b[A': # UP
        if history:
            if history_idx < len(history):
                history_idx+=1
                get_history=history[-history_idx]
                sys.stdout.write('\x1b[2K') # erase current line
                sys.stdout.write('\r'+prompt+history[-history_idx]) # \r: cursor start from 0
                sys.stdout.flush()
                line=history[-history_idx]
                cursor=len(line)
    elif c == '\x1b[B': # Down
        if history:
            if history_idx > 0:
                history_idx-=1
                get_history=history[-history_idx]
                sys.stdout.write('\x1b[2K') # erase current line
                sys.stdout.write('\r'+prompt+history[-history_idx])
                line=history[-history_idx]
                cursor=len(line)
    elif len(c) == 1 and ord(c) == 127: # backspace key
        sys.stdout.write('\b \b')
        if cursor == len(line):
            line=line[:len(line)-1]
            cursor-=1
        elif cursor < len(line):
            front=line[:cursor-1]
            backend=line[cursor:]
            sys.stdout.write(backend+' ')
            sys.stdout.write('\033[{0}D'.format(len(backend)+1))# 1 move left, if 3 move then '\033[3D'
            line=front+backend
            cursor-=1
    elif c == '\t' and keep_new_line is False: # Tab key
        found=None
        if line_end and line_end != '\n':
            find_word=line_end
            if len(line_end.split('/'))>1: # search defined directory
                found=completion(list_in_dir(line_end),line_end)
            else: # Search current directory
                found=completion(list_in_dir('.'),line_end)
        elif line_first and not line_end: # search command and complete command
            find_word=line_first
            found=completion(CMDLISTS,line_first)
        else: # Nothing then print command list
            print(CMDLISTS) 
        if found:
                if len(found) == 1:
                    adding_str=found[0][len(find_word):]
                    if len(adding_str): # If found something then complete cursor string
                        print(adding_str,end='')
                        line+=adding_str
                    else: # print currnt file or dir list when already completed command
                        print(list_in_dir('.'))
                else:
                    print(found)
        sys.stdout.flush()
    elif c == 'q': # Exit
        break
    elif c in ['\r','\n','\r\n']: # Enter ; return value
        before_first,before_end,line_first,line_end=line_dbg(line)
        cursor=0
        if keep_new_line is False and line_end in ['{',':','\\']:
            keep_new_line=True
            print(c)
        if keep_new_line is False or (keep_new_line and ( line_first == '}' or (line_end == '\n' and line_first=='\n'))): # Return all data
            keep_new_line=False
            print('return value:{0}'.format(line))
            history.append(line)
            line=''
        else:
            line+=c # Get a line mark
        if keep_new_line:
            print('> ',end='') # prompt after new line
        else:
            print(prompt,end='') # prompt after new line
    else: # get cursor typed charactor
        print(c,end='') # type prompt 
        line+=c         # get type prompt 
        cursor+=1
    sys.stdout.flush()

# Reset the terminal:
termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
#
