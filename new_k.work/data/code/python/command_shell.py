# https://docs.python.org/3/library/cmd.html
# ref2 : https://github.com/python-cmd2/cmd2
from cmd import Cmd
# Shell

import kmisc as km
# Module list
import sys

# Memory size
from pympler import muppy,summary

# Load module
import importlib
gg=globals()

class MyPrompt(Cmd):
    last_output = ''

#    def __init__(self, intro="K",prompt="aa"): # initialization works
#        Cmd.__init__(self)
#        #self.intro=intro
#        self.prompt = '[{0}@{1} {2}]k$ '.format(username,hostname,os.path.basename(os.getcwd()))
        

    def do_shell(self, line): # it works
        "Run a shell command"
        print "running shell command:", line
        output = os.popen(line).read()
        print(output)
        self.last_output = output
    
#    def do_echo(self, line):
#        "Print the input, replacing '$out' with the output of the last shell command"
#        # Obviously not robust
#        print line.replace('$out', self.last_output)


# This is default working without this code
#    CMDLISTS=['help','ls','quit','cd','size','import']
#    def complete_greet(self,text,line,bigidx,endidx):
#        if not text:
#            completions = self.CMDLIST[:]
#        else:
#            completions = [ f for f in self.CMDLIST if f.startswith(text) ]
#        return completions    


# commands module shell handle
    def do_shell2(self,args):
        from commands import *
        status, output = getstatusoutput(args)
        exit_code = status >> 8
        signal_num = status % 256
        print(output)

    def do_hello(self, args):
        """Says hello. If you provide a name, it will greet you with it."""
        if len(args) == 0:
            name = 'stranger'
        else:
            name = args
        print "Hello, %s" % name

    def do_quit(self,args):
        """Quits the program."""
        print "Quitting K."
        raise SystemExit

    def do_ls(self,args):
        rc=km.rshell('ls {0}'.format(args))
        if rc[0] == 0:
            print(rc[1])
        else:
            print(rc[2])

    def default(self,args):
        rc=km.rshell('{0}'.format(args))
        if rc[0] == 0:
            print(rc[1])
        else:
            print(rc[2])

    def do_size(self,args):
#        import tracemalloc
#        snapshot=tracemalloc.take_snapshot()
#        for i, stat in enumerate(snapshot.statistics('filename')[:5], 1):
#             print(i, str(stat))
#        print('')
        # pip install pympler
        all_objects = muppy.get_objects()
        sum1 = summary.summarize(all_objects)
        summary.print_(sum1)
       
        
    def do_cd(self,args):
        os.chdir(args)
        self.prompt = '[{0} {1}]$ '.format(prompt_str,os.path.basename(os.getcwd()))
        
    def do_import(self,args):
        module_list=args.split()
        name=module_list[0]
        if 'as' in module_list: # support as command
            name=module_list[-1]
        gg[name] = importlib.import_module(module_list[0])

    def do_module_list(self,args):
        for ii in [key for key in globals().keys() if isinstance(globals()[key], type(sys)) and not key.startswith('__')]:
            try:
                m_path=globals()[ii].__file__
            except:
                m_path=''
            print('%15s : %s'%(ii,m_path))

    def search(query):
        import requests
        """This search and return results corresponding to the given query from Google Books"""
        url_format = 'https://www.googleapis.com/books/v1/volumes'
        query = "+".join(query.split())
        query_params = {
            'q': query
        }
        response = requests.get(url_format, params=query_params)
        print(response.json()['items']) 

    def do_p(self,args):
        import code
        vars = globals()
        vars.update(locals())
        shell=code.InteractiveConsole(vars)
        shell.interact()

    def do_set(self,args):
        env=os.environ
        nvar=args.split('=')
        tmp=None
        for i in nvar[1:]:
           if tmp is None:
               tmp='{0}'.format(i)
           else:
               tmp='{0}={1}'.format(tmp,i)
        env[nvar[0]]=tmp
       
    do_EOF = do_quit

if __name__ == '__main__':
    import getpass,socket,os
    username=getpass.getuser()
    hostname=socket.gethostname()
    prompt = MyPrompt()
    prompt_str='{0}@{1}'.format(username,hostname)
    prompt.prompt = '[{0} {1}]$ '.format(prompt_str,os.path.basename(os.getcwd())) # it works instad of __init__()
    prompt.cmdloop('Starting K prompt...')

