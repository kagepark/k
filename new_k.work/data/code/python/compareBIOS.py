#!/usr/bin/python2
# -*- coding: utf-8 -*-
# This is the core tool that handles the changing of bios options and testing
# the options.
# Back... to the future.
from __future__ import print_function
import xml
import xml.etree.ElementTree as ElementTree
import json
import pprint
import os
import sys
import time
import shutil
import copy
import glob
import argparse
import string
import random
import multiprocessing
import errno
CRASH_ERR_EXIT_LEVEL=5 #To crash after spitting out the error.
CRASH_ERR_LEVEL=3 #Everything with an error level > 3 stops the test with 'an error occurred'.  This will mostly be borked configs.
WARN_ERR_LEVEL=1 #Warning error level beginning.
LOGFILE="meow"
configDumpTree = None
testDumpTree = None
rootElement = None
# This method writes to the logfile global
def logmsg(message):
	try:
		with open(LOGFILE, "w+") as oFD:
			if isinstance(message, list):
				for line in message:
					oFD.write("%s\n" % line)
			else:
				oFD.write("%s\n" % str(message))
	except:
		return False
	return True
#We give xmlElements Menu entries
class xmlElement:
	def __init__(self, treeTraversal, topTree, bottomTree=None):
		self.idxes = []
		self.settingi = []
		if 'name' in topTree.attrib:
			treeTraversal+="/%s[@name='%s']" % (str(topTree.tag.encode('utf-8')), str(topTree.attrib['name'].encode('utf-8')))
		else:
			treeTraversal+="/%s" % str(topTree.tag.encode('utf-8'))
		self.treeTraversal = treeTraversal
		if topTree.tag.encode('utf-8') == "BiosCfg" or topTree.tag.encode('utf-8') == "Menu" or topTree.tag.encode('utf-8') == "Information" or topTree.tag.encode('utf-8') == "AvailableOptions":
			try:
				self.idxname = treeTraversal
			except:
				pass
			for tt_entry in list(topTree):
				self.idxes.append(xmlElement(treeTraversal, tt_entry))
		elif topTree.tag.encode('utf-8') == "Setting":
			self.settingi.append("%s|%s|%s" % (treeTraversal, topTree.attrib['name'].encode('utf-8'), topTree.attrib['type'].encode('utf-8')))
			for tt_entry in list(topTree):
				self.idxes.append(xmlElement(treeTraversal, tt_entry))
		elif topTree.tag.encode('utf-8') == "Option":
			# print("Option entry:")
			# pprint.pprint(topTree.attrib)
			self.settingi.append("%s|%s|%s" % (str(treeTraversal),str(topTree.attrib), str(topTree.text)))
	def getSettingList(self):
		out = []
		for index in self.idxes:
			try:
				out.append("%s" % self.idxname)
			except:
				pass
			out+=index.getSettingList()
		for setting in self.settingi:
			# out+=setting.getTestOptions()
			# print("setting: %s" % setting)
			# out+="%s\n" % setting
			out.append(setting)
		return out
	def getSettingInstance(self, request):
		out = None
		#Purge request pieces if it's set:
		if '___' in request:
			request = request.split('___')[0]
		for index in self.idxes:
			temp = index.getSettingInstance(request)
			if temp != None:
				return temp
		for setting in self.settingi:
			if setting.treeTraversal == request:
				return setting
		return out
def parseOutGarbageEntries(listIn):
	listOut = []
	for entry in listIn:
		if len(listOut) == 0:
			listOut.append(entry)
			continue
		if entry.endswith("Information"):
			continue
		if entry.endswith("AvailableOptions"):
			continue
		if entry != listOut[-1]:
			listOut.append(entry)
	return listOut
def Diff(li1, li2): 
    li_dif = [i for i in li1 + li2 if i not in li1 or i not in li2] 
    return li_dif 
if __name__=="__main__":
	global tLogPath 
	global errBreak
	cmdAP = argparse.ArgumentParser(description="Compare BIOS options automatically")
	cmdAP.add_argument('-i', dest="origin", help="Original xml config")
	cmdAP.add_argument('-n', dest="new", help="new xml config")
	args = cmdAP.parse_args()
	originalConfig = args.origin
	newConfig = args.new
	configDumpTree = ElementTree.parse(originalConfig)
	newConfigDumpTree = ElementTree.parse(newConfig)
	origRootElement = xmlElement("", configDumpTree.getroot())
	newRootElement = xmlElement("", newConfigDumpTree.getroot())
	diffOutput = Diff(parseOutGarbageEntries(origRootElement.getSettingList()), parseOutGarbageEntries(newRootElement.getSettingList()))
	if len(diffOutput) == 0:
		sys.exit(0)
	else:
		print("Differences between %s and %s:" % (originalConfig, newConfig))
		pprint.pprint(diffOutput)
