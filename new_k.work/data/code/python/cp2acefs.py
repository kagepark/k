#!/usr/bin/env python
import sys, os, shutil, stat
from time import sleep

def mk_rsrc(src,src_full_path=None):
       rc_rsrc=[]
       tdest=''
       start_real_src_point=0
       if src_full_path is None:
           aa=src.split('/')
           if aa[len(aa)-1] == '':
               return False
       else:
           src_full_path_arr=src_full_path.split('/')
           start_real_src_point=len(src.split('/'))-1
           for ii in list(src_full_path_arr[start_real_src_point:]):
               tdest=os.path.join(tdest,ii)
       rc_rsrc.append(tdest)
       rc_rsrc.append(start_real_src_point)
       return rc_rsrc

def mk_dest(src,dest,src_full_path=None):
       rc_dest=[]
       start_real_src_point=0
       tdest=''
       # analy dest
       if os.path.isfile(dest) is True:
           new_dest=dest
       elif os.path.isdir(dest) is True:
           sdest=mk_rsrc(src,src_full_path)
           if sdest is False:
               return False
           start_real_src_point=sdest[1]
           tdest=sdest[0]
           if tdest == '':
               new_dest=os.path.join(dest,os.path.basename(src))
           else:
               new_dest=os.path.join(dest,tdest)
       elif os.path.isdir(os.path.dirname(dest)) is True:
           new_dest=dest
       else:
           return False
       rc_dest.append(new_dest)
       rc_dest.append(start_real_src_point)
       rc_dest.append(tdest)
       return rc_dest

def _mkddir(dest,src=None,new_dest=None):
      if dest is None:
          return False
      if os.path.isdir(dest) is False and os.path.exists(dest) is True:
          print(''' Exising unknown destination ''')
          return False
      if os.path.isdir(os.path.dirname(dest)) is False:
          print(os.path.dirname(dest) + ''' not found1 ''')
          return False

      dest_arr=dest.split('/')
      dest_max=len(dest_arr)
      if dest_arr[dest_max-1] == '':
          dest_max=dest_max-1
      
      if src is not None:
         if new_dest is None:
             os.mkdir(dest)
             return True
         tndest=dest
         tnsrc=src
         new_dest_arr=new_dest.split('/')
         src_arr=src.split('/')
         delay=0
         if src_arr[len(src_arr)-1] != '':
             delay=1
         for ii in list(new_dest_arr[dest_max:]):
             tndest=os.path.join(tndest,ii)
             if delay == 0:
                 tnsrc=os.path.join(tnsrc,ii)
             else:
                 delay=0
             if os.path.isdir(tnsrc) is True and os.path.isdir(tndest) is False:
                 os.mkdir(tndest)
                 for ii in range(0,100):
                     #if os.path.exists(tndest) is True:
                     if os.path.isdir(tndest) is True:
                        break
                     sleep(0.2)
         return True
      else:
         if os.path.isdir(dest) is False:
             return os.system('mkdir -p ' + dest)

def cp_mv(src,dest,mode='cp'):
       if mode == 'cp':
           try:
               shutil.copy2(src,dest)
           except IOError as e:
               print('Unable to copy file. %s'%e)
               return False
       else:
           try:
               shutil.move(src,dest)
           except IOError as e:
               print('Unable to move file. %s'%e)
               return False

        
def _cp_mv(src,dest,mode='cp',fp_dir=None,force=None):
      if os.path.isdir(os.path.dirname(dest)) is False:
         print('''destination's parent ''' + os.path.dirname(dest) + ''' not found''')
         sys.exit(-1)
      if os.path.isdir(src) is True:
         src_arr=src.split('/')
         src_subdir=1
         if src_arr[len(src_arr)-1] != '':
             src_subdir=0
         for dirname, dirnames, filenames in os.walk(src):
            for filename in filenames:
                src_file=os.path.join(dirname,filename)
                new_dest=mk_dest(src,dest,src_file)[0]
                if new_dest == dest and src_subdir == 0:
                    if _mkddir(dest,src) is False:
                        return False
                    src=src+'/'
                    new_dest=mk_dest(src,dest,src_file)[0]

                if force is None:
                    if os.path.exists(new_dest) is True:
                        print( new_dest + ' found ')
                        continue

                if _mkddir(dest,src,new_dest) is False:
                    return False
                if cp_mv(src_file,new_dest,mode) is False:
                    return False
         if mode != 'cp':
             shutil.rmtree(src)
      else:
          new_dest=mk_dest(src,dest)
          if force is None:
              if os.path.exists(new_dest[0]) is True:
                   print( new_dest[0] + ' found ')
                   return False
          if new_dest[1] > 0:
              if _mkddir(dest,src) is False:
                  sys.exit(-1)
          if cp_mv(src,new_dest[0],mode) is False:
              sys.exit(-1)

def _cp_mv2(src,dest,mode='cp',fp_dir=None,force=None):
    for ii in list(src):
       _cp_mv(ii,dest,mode,fp_dir=fp_dir,force=force)

sys.argv.pop(0)
force=None
nargc=len(sys.argv)
if nargc <= 0:
     print('acefs_cp.py [-f or -a or -af] <src> [...] <dest>')
     sys.exit()
if sys.argv[0] == '-h' or sys.argv[0] == '--help':
     print('acefs_cp.py [-f or -a or -af] <src> [...] <dest>')
     sys.exit()
if sys.argv[0] == '-a' or sys.argv[0] == '-f' or sys.argv[0] == '-af':
   force=1
   sys.argv.pop(0)

nargc=len(sys.argv)
if nargc <= 0:
     print('acefs_cp.py [-f or -a or -af] <src> [...] <dest>')
     sys.exit()

_cp_mv2(sys.argv[0:nargc-1],sys.argv[nargc-1],'cp',force=force)
