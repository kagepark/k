import ast

def dict_put(dic=None,path=None,value=None,force=False,safe=True):
    if dic is None or path is None:
        return False
    tmp=dic
    path_arr=path.split('/')
    path_num=len(path_arr)
    if path_arr[0] == '':
        path_num=path_num-1
        path_arr.pop(0)
    for ii in path_arr[:(path_num-1)]:
        if ii in tmp:
           if type(tmp[ii]) == type({}):
               dtmp=tmp[ii]
           else:
               if tmp[ii] == None:
                   tmp[ii]={}
                   dtmp=tmp[ii]
               else:
                   if force:
                      vtmp=tmp[ii]
                      tmp[ii]={vtmp:None}
                      dtmp=tmp[ii]
                   else:
                      return False
        else:
           if force:
               tmp[ii]={}
               dtmp=tmp[ii]
           else:
               return False
        tmp=dtmp
    if value is None or value == '_blank_' or value == '':
       value={}
    if path_arr[path_num-1] in tmp.keys():
        if safe:
            ttype=type(tmp[path_arr[path_num-1]])
            if tmp[path_arr[path_num-1]] is None or ((ttype is str or ttype is list or ttype is dict) and len(tmp[path_arr[path_num-1]]) == 0):
               tmp.update({path_arr[path_num-1]:value})
            else:
               return False
        else:
            tmp.update({path_arr[path_num-1]:value})
    else:
        if force is False:
            return False
        tmp.update({path_arr[path_num-1]:value})


def path2list(path):
    if path is None:
       return
    path_arr=path.split('/')
    if path_arr[0] == '':
       path_arr.pop(0)
    return path_arr

def list2path(path):
    if len(path) < 1:
        return
    new_path='/'
    maxp=len(path)-1
    for ii in range(maxp):
        new_path=new_path+path[ii]+'/'
    new_path=new_path+path[maxp]
    return new_path

def append2list(src,*add):
   org=[]
   if type(src) == str:
      for jj in src.split(','):
         org.append(jj)
   elif type(src) == list:
      org=src
   elif type(src) == int or type(src) == float:
      org.append(src)
   else:
      return False

   if len(add) < 0:
       return False
   for ii in list(add):
      if type(ii) == type([]):
         for jj in list(ii):
            org.append(jj)
      else:
         for jj in ii.split(','):
            org.append(jj)
   return org


def dict2list_path(dic,path='',with_val=False):
    tmp=[]
    if type(dic) is dict:
        for k in dic.keys():
            npath=path+'/'+k
            if type(dic[k]) is dict:
                append2list(tmp,dict2list_path(dic[k],path=npath,with_val=with_val))
            else:
                if with_val:
                    tmp.append(npath+"=['{0}',{1}]".format(type(dic[k]).__name__,dic[k]))
                    #tmp.append(npath+'={0}'.format(dic[k]))
                else:
                    tmp.append(npath)
    return tmp

def dict2symbol_path(dic,path='',with_val=False,symbol=','):
    new_path=None
    if type(dic) is dict:
        for k in dic.keys():
            path=path+'/'+k
            if type(dic[k]) is dict:
                dict2symbol_path(dic[k],path=path,with_val=with_val,symbol=symbol)
            else:
                if with_val:
                    if new_path is None:
                        new_path=path
                    else:
                        new_path=new_path+symbol+path
                    return new_path+'={0}'.format(dic[k])
                else:
                    if new_path is None:
                        new_path=path
                    else:
                        new_path=new_path+symbol+path
    return new_path


def put_data(data=None,form=None,force=False,safe=False):
    if data is None:
        return form
    if form is None:
        return data
    data_type=type(data)
    form_type=type(form)
    if form_type is str:
        return '{0}'.format(data)
    if form_type is int:
        try:
            return int(data)
        except:
            return False
    if form_type is float:
        try:
            return float(data)
        except:
            return False
    if form_type is bool:
        if data_type is int:
            if data == 0:
                return True
            return False
        elif data_type is str:
            if data == 'yes' or data == 'true' or data == 'ok'  or data == 'YES' or data == 'OK' or data == 'TRUE':
                return True
            return False
        elif data_type is tuple:
            if type(data[0]) is bool:
                return data[0]
            elif data[0] == 'yes' or data[0] == 'true' or data == 'ok' or data[0] == 'YES' or data[0] == 'OK' or data[0] == 'TRUE':
                return True
            return False
        elif data_type is dict:
            if 'rc' in data:
                return data['rc']
            elif 'return' in data:
                return data['return']
            elif 'rc_code' in data:
                return data['rc_code']
            elif 'rccode' in data:
                return data['rccode']
            return False
        elif data_type is list:
            if len(data) >= 1:
                if type(data[0]) is bool:
                    return data[0]
                elif data[0] == 'yes' or data[0] == 'true' or data == 'ok' or data[0] == 'YES' or data[0] == 'OK' or data[0] == 'TRUE':
                    return True
                return False
    if data_type is str:
        try:
            new_data=ast.literal_eval(data)
        except:
            new_data=data
    else:
        new_data=data
   
    if form_type == type(new_data):
        if form_type is dict:
            if force:
                return form.update(new_data)
            else:
                for ii in dict2list_path(data,with_val=True):
                   chk=ii.split('=')
                   print('dbg:',chk,len(chk),type(chk[1]))
                   chk_data=ast.literal_eval(chk[1])
                   print(chk_data[1],type(chk_data[1]))
                   if chk_data[0] == 'str':
                       dict_put(dic=form,path=chk[0],value=str(chk_data[1]),safe=safe)
                   else:
                       dict_put(dic=form,path=chk[0],value=chk_data[1],safe=safe)
        elif form_type is list:
            new_data_num=len(new_data)
            form_num=len(form)
            max_num=form_num
            if form_num >= new_data_num:
                max_num=new_data_num
            for i in range(0,max_num):
                if safe:
                   if form[i] is None or form[i] == '':
                       form[i]=new_data[i]
                else:
                   form[i]=new_data[i]
            if force and new_data_num > max_num:
                for i in range(max_num,new_data_num):
                    form.append(new_data[i])
        else:
            if force or safe is False:
                form=data
            else:
                if form is None or form == '':
                    form=data
                    


form={'a':None,'b':None,'c':{'d':None,'e':{'f':None},'g':None},'h':None}
print('form:',form)
a={'a':123,'b':'455','c':{'e':{'f':[1,2,3,888]},'g':999},'h':6}
print('init a',a)
uu=put_data(data=a,form=form,force=False,safe=True)
print('add a to form',form,'rc:',uu)
a={'a':127,'b':'457','c':{'d':557,'g':997},'h':7}
print('update a',a)
uu=put_data(data=a,form=form,force=False,safe=True)
print('add updated a to form',form,'rc:',uu)
