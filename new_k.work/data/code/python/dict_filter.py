def get_Dict_from_Obj(select_fields=['CPU','NVME','DIMM','HDD','CBURN_DIR','BOARD_NAME','SKU','RACK_LOCATION','SLOT_NUMBER','DESC'],filter_name=None,filter_out_value=None,filter_in_value=None,filter_in_like=False,include_del_date=False,model_list=('bmc_mac','item_name','item_info','id')):

    if include_del_date:
        init_obj_query=Inventory.objects # Django DB Qeury OBJ
    else:
        init_obj_query=Inventory.objects.filter(item_del_date='') # Django DB Qeury OBJ
    if model_list is None or model_list == 'all':
        model_list=('id','created','bmc_mac','item_name','item_info','item_desc','item_del_date','item_date')
    else:
        if not 'id' in model_list:
            model_list=tuple(['id']+list(model_list))
    bmc_macs=init_obj_query.values(*model_list).order_by('bmc_mac')
    # convert DB to single line item in a dict
    contacts={}
    for i in bmc_macs:
         if not i['bmc_mac'] in contacts:
             contacts[i['bmc_mac']]={'id':i['id']} # initial contacts information and add link to detail view (id)
         if select_fields and select_fields != 'all': # filter collect field
             if i['item_name'] in select_fields:
                 if i['item_name'] == 'RACK_LOCATION' and len(i['item_info']):
                     contacts[i['bmc_mac']][i['item_name']]='%03d'%(int(i['item_info']))
                 elif i['item_name'] == 'SLOT_NUMBER' and len(i['item_info']):
                     contacts[i['bmc_mac']][i['item_name']]='%02d'%(int(i['item_info']))
                 else:
                     contacts[i['bmc_mac']][i['item_name']]=i['item_info']
         else: # collect all
             contacts[i['bmc_mac']][i['item_name']]=i['item_info']
    if filter_name and (filter_out_value is not None or filter_in_value is not None):
        new_contacts={}
        for i in contacts.keys():
            if filter_out_value is not None:
                if filter_name in contacts[i] and contacts[i][filter_name] != filter_out_value:
                    new_contacts[i]=contacts[i]
            elif filter_in_value is not None:
                if filter_name == 'RACK_LOCATION':
                    filter_in_value = '%03d'%(int(filter_in_value))
                elif filter_name == 'SLOT_NUMBER':
                    filter_in_value = '%02d'%(int(filter_in_value))
                if filter_in_like:
                    if filter_name in contacts[i] and filter_out_value in contacts[i][filter_name]:
                        new_contacts[i]=contacts[i]
                else:
                    if filter_name in contacts[i] and contacts[i][filter_name] == filter_out_value:
                        new_contacts[i]=contacts[i]
        return new_contacts
    else:
        return contacts


#new_contacts=get_Dict_from_Obj()
#new_contacts=get_Dict_from_Obj(filter_out_name='RACK_LOCATION',filter_out_value='')
#new_contacts=get_Dict_from_Obj(filter_out_name='RACK_LOCATION',filter_in_value='4')
#new_contacts=get_Dict_from_Obj(filter_out_name='RACK_LOCATION',filter_in_value='4',filter_in_like=True)
