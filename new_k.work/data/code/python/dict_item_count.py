def item_count(dic):
    #count for ['NVME','HDD','DIMM','CPU']
    if type(dic) is dict:
        total=next(iter(dic))
        if total > 100:
            total=100
        count=-1
        dimm_type=''
        size=''
        model_info1=''
        model_info=''
        for kj in range(0,total):
            if 'PART' in dic[total][kj] and dic[total][kj]['PART'] != 'NO':
                count+=1
        if 'TYPE' in dic[total][0]: # Get dimm type
            dimm_type=dic[total][0]['TYPE']
        if 'SIZE' in dic[total][0]: # Get item size
            size=dic[total][0]['SIZE']
        if 'MANUFACTURER' in dic[total][0]:
            model_info1=dic[total][0]['MANUFACTURER'] # get manufacturer
        if 'MODEL' in dic[total][0]:
            model_info=dic[total][0]['MODEL'] # get model
        elif 'PART' in dic[total][0]: # Get part
            model_info=dic[total][0]['PART']
        model_info='{},{},{},{}'.format(model_info,model_info1,dimm_type,size) # Each Server's Item
        if count < 0:
            return total,model_info
        else:
            return count+1,model_info
    return None,None

