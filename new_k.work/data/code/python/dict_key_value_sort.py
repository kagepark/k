def item_sort(dic,sort_field_name=None,rev=False):
    if rev and ( rev is True or rev == '1'):
        rev=True
    else:
        rev=False
    if sort_field_name is None:
        return sorted(dic.items(),reverse=rev)
    rack_sort={}
    for bmac in dic.keys():
        if sort_field_name=='rack_slot':
            if 'RACK_LOCATION' in dic[bmac]:
                if 'SLOT_NUMBER' in dic[bmac]:
                    rack_sort[bmac]='{}:{}'.format(dic[bmac]['RACK_LOCATION'],dic[bmac]['SLOT_NUMBER'])
                else:
                    rack_sort[bmac]='{}'.format(dic[bmac]['RACK_LOCATION'])
            else:
                rack_sort[bmac]=''
        elif sort_field_name in dic[bmac]:
            rack_sort[bmac]='{}'.format(dic[bmac][sort_field_name])
        else:
            rack_sort[bmac]=''
    contacts=[]
    for i in sorted(rack_sort.items(),key=lambda kv: kv[1], reverse=rev):
        contacts.append((i[0],dic[i[0]]))
    return contacts

