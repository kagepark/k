def mobj2dict(found_objs,items=[]):
    contacts={}
    # convert DB to single line item in a dict
    if type(found_objs).__name__ == 'QuerySet':
        for i in found_objs.values():
            if not i['bmc_mac'] in contacts:
                contacts[i['bmc_mac']]={'id':i['id']} # initial contacts information and add link to detail view (id)
            if not items or i['item_name'] in items:
                if i['item_name'] == 'RACK_LOCATION':
                    if len(i['item_info']) > 0:
                        contacts[i['bmc_mac']][i['item_name']]='%03d'%(int(i['item_info']))
                    else:
                        contacts[i['bmc_mac']][i['item_name']]=i['item_info']
                elif i['item_name'] == 'SLOT_NUMBER' and len(i['item_info']):
                    if len(i['item_info']) > 0:
                        contacts[i['bmc_mac']][i['item_name']]='%02d'%(int(i['item_info']))
                    else:
                        contacts[i['bmc_mac']][i['item_name']]=i['item_info']
                else:
                    contacts[i['bmc_mac']][i['item_name']]=i['item_info']
    return contacts

