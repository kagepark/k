import smtplib

SERVER = "localhost"

FROM = "noreply@supermicro.com"
TO = ["kwanggip@supermicro.com"] # must be a list

SUBJECT = "Email test with python"

TEXT = "This message was sent with Python's smtplib."

# Prepare actual message

message = """\
From: %s
To: %s
Subject: %s

%s
""" % (FROM, ", ".join(TO), SUBJECT, TEXT)

# Send the mail

server = smtplib.SMTP(SERVER)
server.sendmail(FROM, TO, message)
server.quit()
