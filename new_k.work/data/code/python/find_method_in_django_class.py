import request as rq


def get_method_in_class(class_name):
    ret = dir(class_name)
    if hasattr(class_name,'__bases__'):
        for base in class_name.__bases__:
            ret = ret + get_method_in_class(base)
    return ret

def get_val(find,rq,rc=None):
    rq_arr=rq.__class__.__module__.split('.')
    if len(rq_arr) == 2:
       rq_type=rq_arr[1]
    else:
       return rc
    if rq_type == 'models':
        if find in get_method_in_class(rq):
           tmp=getattr(rq,find)
           if tmp != '':
               return tmp
    elif rq_type == 'forms':
        mm=rq.cleaned_data.get(find)
        if mm is not None and mm != '':
           return mm
    elif rq_type == 'request':
        if rq.method == 'GET':
            if find in rq.GET and type(rq.GET[find]) is str and len(rq.GET[find]) > 0:
                return rq.GET[find]
        elif rq.method == 'POST':
            if find in rq.data and type(rq.data[find]) is str and len(rq.data[find]) > 0:
                return rq.data[find]
    return rc

