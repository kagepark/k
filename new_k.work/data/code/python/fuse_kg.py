#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, print_function, absolute_import

import os
import sys

import llfuse
import errno
import stat
from time import time
import sqlite3
import logging
from collections import defaultdict
from llfuse import FUSEError
from argparse import ArgumentParser

sql_inodes="""CREATE TABLE if not exists inodes (
  inode_id        INTEGER PRIMARY KEY,
  uid       INT NOT NULL,
  gid       INT NOT NULL,
  mode      INT NOT NULL,
  mtime_ns  INT NOT NULL,
  atime_ns  INT NOT NULL,
  ctime_ns  INT NOT NULL,
  target    BLOB(1024) ,
  size      INT NOT NULL DEFAULT 0,
  rdev      INT NOT NULL DEFAULT 0,
  data      BLOB
)"""

sql_metadata="""CREATE TABLE if not exists meta_data (
  meta_id     INTEGER PRIMARY KEY AUTOINCREMENT,
  name      BLOB(1024) NOT NULL,
  inode     INT NOT NULL REFERENCES inodes(inode_id),
  parent_inode INT NOT NULL REFERENCES inodes(inode_id),
  UNIQUE (name, parent_inode)
)"""


class KGFS_Operations(llfuse.Operations):
    def __init__(self,db_type='sql',db_file='/tmp/fuse.db'):
        super(Operations, self).__init__()
        if db_type == 'memory':
            self.db = sqlite3.connect(':memory:')
        else: # default sqlite
            self.db = sqlite3.connect(db_file)
        self.db.text_factory = str
        self.db.row_factory = sqlite3.Row
        self.cursor = self.db.cursor()
        self.inode_open_count = defaultdict(int)
        self.init_sql_tables()

    def init_sql_tables(self): 
        self.cursor.execute("SELECT name from sqlite_master where type='table' and name ='inodes'")
        aa=self.cursor.fetchall()
        jump=0
        if len(aa) > 0 :
            if aa[0][0] == 'inodes':
                jump=1
        self.cursor.execute(sql_inodes)
        self.cursor.execute(sql_metadata)
        if jump == 0:
          # Insert root directory
          now_ns = int(time() * 1e9)
          self.cursor.execute("INSERT INTO inodes (inode_id,mode,uid,gid,mtime_ns,atime_ns,ctime_ns) "
                            "VALUES (?,?,?,?,?,?,?)",
                            (llfuse.ROOT_INODE, stat.S_IFDIR | stat.S_IRUSR | stat.S_IWUSR
                              | stat.S_IXUSR | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH
                              | stat.S_IXOTH, os.getuid(), os.getgid(), now_ns, now_ns, now_ns))
          self.cursor.execute("INSERT INTO meta_data (name, parent_inode, inode) VALUES (?,?,?)",
                            (b'..', llfuse.ROOT_INODE, llfuse.ROOT_INODE))
          self.db.commit()

    def getattr(self, inode, ctx=None):
        row = self.get_row('SELECT * FROM inodes WHERE id=?', (inode,))

        entry = llfuse.EntryAttributes()
        entry.st_ino = inode
        entry.generation = 0
        entry.entry_timeout = 300
        entry.attr_timeout = 300
        entry.st_mode = row['mode']
        entry.st_nlink = self.get_row("SELECT COUNT(inode) FROM contents WHERE inode=?",
                                     (inode,))[0]
        entry.st_uid = row['uid']
        entry.st_gid = row['gid']
        entry.st_rdev = row['rdev']
        entry.st_size = row['size']

        entry.st_blksize = 512
        entry.st_blocks = 1
        entry.st_atime_ns = row['atime_ns']
        entry.st_mtime_ns = row['mtime_ns']
        entry.st_ctime_ns = row['ctime_ns']

