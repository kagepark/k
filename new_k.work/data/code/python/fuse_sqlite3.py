#!/usr/bin/env python
# pip2 install fusepy
# pip2 install llfuse

from __future__ import with_statement

import os
import sys
import errno

from fuse import FUSE, FuseOSError, Operations
from argparse import ArgumentParser
import sqlite3
from collections import defaultdict
from time import time
import llfuse
import stat




class Passthrough(Operations):
    def __init__(self):
        super(Operations, self).__init__()

        self.inode_open_count = defaultdict(int)
        self.disk_dict=self.init_tables()

        self.root = ''


    # Helpers
    # =======

    def _full_path(self, partial):
        if partial.startswith("/"):
            partial = partial[1:]
        path = os.path.join(self.root, partial)
        return path

    def init_tables(self):
        '''Initialize file system tables'''
        if os.path.isfile('/tmp/fuse.dat') is False:
            now_ns = int(time() * 1e9)
            idx=llfuse.ROOT_INODE
            rowid=idx
            disk_dict={
               'inodes':{
                  idx:{
                     'uid':os.getuid(),
                     'gid':os.getgid(),
                     'mode':stat.S_IFDIR | stat.S_IRUSR | stat.S_IWUSR| stat.S_IXUSR | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH| stat.S_IXOTH,
                     'mtime_ns':now_ns,
                     'atime_ns':now_ns,
                     'ctime_ns':now_ns,
                     'target':b'',
                     'size':0,
                     'rdev':0,
                     'data':b''
                  }
               },
               'contents':{
                  rowid:{
                     'name':b'..',
                     'inode':llfuse.ROOT_INODE,
                     'parent_inode':llfuse.ROOT_INODE
                  }
              }
            }
        else:

            buf = io.open('/tmp/fuse.dat', 'rb', buffering=(128 * 1024))
            disk_dict = cPickle.load(buf)

        return disk_dict

    def update_db(self, main_db, update_db):
        main_db.update(update_db)

    def change_key(self, main_db, old_key, new_key):
        main_db[new_key]=main_db[old_key]
        main_db.pop(old_key)


    def lookup(self, inode_p, name, ctx=None):
        '''Base function'''
        inode=None
        if name == '.':
            inode = inode_p
        elif name == '..':
            for ii in list(self.disk_dict['contents']):
                if self.disk_dict['contents'][ii]['inode']== inode_p:
                    inode=self.disk_dict['contents'][ii]['parent_inode']
                    break
        else:
            for ii in list(self.disk_dict['contents']):
                if self.disk_dict['contents'][ii]['name']== name and self.disk_dict['contents'][ii]['parent_inode'] == inode_p:
                    inode=self.disk_dict['contents'][ii]['inode']
                    break
            if inode is None:
                raise(llfuse.FUSEError(errno.ENOENT))

        return self.getattr(inode, ctx)

    # Filesystem methods
    # ==================

    def access(self, path, mode):
        full_path = self._full_path(path)
        if not os.access(full_path, mode):
            raise FuseOSError(errno.EACCES)

    def chmod(self, path, mode):
        full_path = self._full_path(path)
        return os.chmod(full_path, mode)

    def chown(self, path, uid, gid):
        full_path = self._full_path(path)
        return os.chown(full_path, uid, gid)

    def getattr(self, inode, ctx=None):
#        full_path = self._full_path(path)
#        st = os.lstat(full_path)
#        return dict((key, getattr(st, key)) for key in ('st_atime', 'st_ctime',
#                     'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_size', 'st_uid'))

        ''' ls command can't access mounted directory.
            But, mount and umount is working '''
        inode=int(inode) # convert long to inte for temporary working
        if inode in self.disk_dict['inodes']:
            row = self.disk_dict['inodes'][inode]
        else:
            now_ns = int(time() * 1e9)
            row = {
                 'uid':os.getuid(),
                 'gid':os.getgid(),
                 'mode':stat.S_IFDIR | stat.S_IRUSR | stat.S_IWUSR| stat.S_IXUSR | stat.S_IRGRP |stat.S_IXGRP | stat.S_IROTH| stat.S_IXOTH,
                 'mtime_ns':now_ns,
                 'atime_ns':now_ns,
                 'ctime_ns':now_ns,
                 'target':b'',
                 'size':0,
                 'rdev':0,
                 'data':b''
           }

        entry = llfuse.EntryAttributes()
        entry.st_ino = inode
        entry.generation = 0
        entry.entry_timeout = 300
        entry.attr_timeout = 300
        entry.st_mode = row['mode']
        entry.st_nlink = 0
        for ii in list(self.disk_dict['contents']):
           if self.disk_dict['contents'][ii]['inode']== inode:
              entry.st_nlink = entry.st_nlink + 1
        entry.st_uid = row['uid']
        entry.st_gid = row['gid']
        entry.st_rdev = row['rdev']
        entry.st_size = row['size']

        entry.st_blksize = 512
        entry.st_blocks = 1
        entry.st_atime_ns = row['atime_ns']
        entry.st_mtime_ns = row['mtime_ns']
        entry.st_ctime_ns = row['ctime_ns']

        return entry


    def readdir(self, path, fh):
        full_path = self._full_path(path)

        dirents = ['.', '..']
        if os.path.isdir(full_path):
            dirents.extend(os.listdir(full_path))
        for r in dirents:
            yield r

    def readlink(self, path):
        pathname = os.readlink(self._full_path(path))
        if pathname.startswith("/"):
            # Path name is absolute, sanitize it.
            return os.path.relpath(pathname, self.root)
        else:
            return pathname

    def mknod(self, path, mode, dev):
        return os.mknod(self._full_path(path), mode, dev)

    def rmdir(self, path):
        full_path = self._full_path(path)
        return os.rmdir(full_path)

    def mkdir(self, path, mode):
        return os.mkdir(self._full_path(path), mode)

    def statfs(self, path):
        full_path = self._full_path(path)
        stv = os.statvfs(full_path)
        return dict((key, getattr(stv, key)) for key in ('f_bavail', 'f_bfree',
            'f_blocks', 'f_bsize', 'f_favail', 'f_ffree', 'f_files', 'f_flag',
            'f_frsize', 'f_namemax'))

    def unlink(self, path):
        return os.unlink(self._full_path(path))

    def symlink(self, name, target):
        return os.symlink(target, self._full_path(name))

    def rename(self, old, new):
        return os.rename(self._full_path(old), self._full_path(new))

    def link(self, target, name):
        return os.link(self._full_path(name), self._full_path(target))

    def utimens(self, path, times=None):
        return os.utime(self._full_path(path), times)

    # File methods
    # ============

    def open(self, path, flags):
        full_path = self._full_path(path)
        return os.open(full_path, flags)

    def create(self, path, mode, fi=None):
        full_path = self._full_path(path)
        return os.open(full_path, os.O_WRONLY | os.O_CREAT, mode)

    def read(self, path, length, offset, fh):
        os.lseek(fh, offset, os.SEEK_SET)
        return os.read(fh, length)

    def write(self, path, buf, offset, fh):
        os.lseek(fh, offset, os.SEEK_SET)
        return os.write(fh, buf)

    def truncate(self, path, length, fh=None):
        full_path = self._full_path(path)
        with open(full_path, 'r+') as f:
            f.truncate(length)

    def flush(self, path, fh):
        return os.fsync(fh)

    def release(self, path, fh):
        return os.close(fh)

    def fsync(self, path, fdatasync, fh):
        return self.flush(path, fh)


def main(mountpoint):
    FUSE(Passthrough(), mountpoint, nothreads=True, foreground=True)

if __name__ == '__main__':
    main(sys.argv[1])
