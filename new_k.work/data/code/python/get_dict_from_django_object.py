def get_Dict_from_Obj(select_fields=['CPU','NVME','DIMM','HDD','CBURN_DIR','BOARD_NAME','SKU','RACK_LOCATION','SLOT_NUMBER','DESC'],filter_out_name=None,filter_out_value=None,filter_in_name=None,filter_in_value=None,filter_in_like=False,include_del_date=False,model_list=('bmc_mac','item_name','item_info','id')):
    if include_del_date:
        init_obj_query=Inventory.objects
    else:
        init_obj_query=Inventory.objects.filter(item_del_date='')
    if model_list is None or model_list == 'all':
        model_list=('id','created','bmc_mac','item_name','item_info','item_desc','item_del_date','item_date')
    else:
        if not 'id' in model_list:
            model_list=tuple(['id']+list(model_list))
    bmc_macs=init_obj_query.values(*model_list).order_by('bmc_mac')
    # convert DB to single line item in a dict
    contacts={}
    filter_out_contacts={}
    filter_in_contacts={}
    for i in bmc_macs:
         if not i['bmc_mac'] in contacts:
             contacts[i['bmc_mac']]={}
         for mm in model_list:
             contacts[i['bmc_mac']][mm]=i[mm] # initial contacts information and add link to detail view (id)
         if select_fields and select_fields != 'all': # filter collect field
             if i['item_name'] in select_fields:
                 if i['item_name'] == 'RACK_LOCATION' and len(i['item_info']):
                     contacts[i['bmc_mac']][i['item_name']]='%03d'%(int(i['item_info']))
                 elif i['item_name'] == 'SLOT_NUMBER' and len(i['item_info']):
                     contacts[i['bmc_mac']][i['item_name']]='%02d'%(int(i['item_info']))
                 else:
                     contacts[i['bmc_mac']][i['item_name']]=i['item_info']
         else: # collect all
             contacts[i['bmc_mac']][i['item_name']]=i['item_info']
    if filter_out_name and filter_out_value is not None:
        for i in contacts.keys():
             if filter_out_name in contacts[i] and contacts[i][filter_out_name] != filter_out_value:
                 filter_out_contacts[i]=contacts[i]

    if filter_in_name and filter_in_value is not None:
        base_contacs={}
        if filter_out_contacts:
            base_contacs=filter_out_contacts
        else:
            base_contacs=contacs
        for i in base_contacs.keys():
            if filter_in_name == 'RACK_LOCATION':
                filter_in_value = '%03d'%(int(filter_in_value))
            elif filter_in_name == 'SLOT_NUMBER':
                filter_in_value = '%02d'%(int(filter_in_value))
            if filter_in_like:
                if filter_in_name in base_contacs[i] and filter_in_value.lower() in base_contacs[i][filter_in_name].lower():
                    filter_in_contacts[i]=base_contacs[i]
            else:
                if filter_in_name in base_contacts[i] and base_contacts[i][filter_in_name].lower() == filter_in_value.lower():
                    filter_in_contacts[i]=base_contacts[i]
        return filter_in_contacts

    if filter_out_contacts:
        return filter_out_contacts
    else:
        return contacts

