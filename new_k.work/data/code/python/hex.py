import base64
import json
import sys
from datetime import datetime
 
import pygit2
import pprint
 
 
def status(repository,objhex=None):
    repo = pygit2.Repository(repository)
 
    if objhex is None:
        for objhex in repo:
            obj = repo[objhex]
            if obj.type == pygit2.GIT_OBJ_COMMIT:
                print(obj.hex, obj.message, datetime.utcfromtimestamp(
                    obj.commit_time).strftime('%Y-%m-%dT%H:%M:%SZ'), obj.author.name, obj.author.email, [c.hex for c in obj.parents])
            elif obj.type == pygit2.GIT_OBJ_TAG:
                print(obj.hex,obj.name,obj.message,obj.target,obj.tagger.name,obj.tagger.email)
            else:
                # ignore blobs and trees
                pass
    else:
        if objhex in repo:
            obj = repo[objhex]
            if obj.type == pygit2.GIT_OBJ_COMMIT:
                print(obj.hex, obj.message, datetime.utcfromtimestamp(
                    obj.commit_time).strftime('%Y-%m-%dT%H:%M:%SZ'), obj.author.name, obj.author.email, [c.hex for c in obj.parents],repo.head.target.tree)
            elif obj.type == pygit2.GIT_OBJ_TAG:
                print(obj.hex,obj.name,obj.message,obj.target,obj.tagger.name,obj.tagger.email)
        else:
            print('{0} not found'.format(objhex))
        
 
 
if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("USAGE {0} <repository>".format(__file__))
        sys.exit(1)
 
    if len(sys.argv) == 3:
        status(sys.argv[1],sys.argv[2])
    elif len(sys.argv) == 2:
        status(sys.argv[1])
