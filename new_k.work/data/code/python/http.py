VERSION = '1.2'
 
import argparse
import BaseHTTPServer
import cgi
import logging
import os
import sys
 
from bottle import Bottle, template
app = Bottle()
@app.route('/')
 
def make_request_handler_class(opts):
    class MyRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
        m_opts = opts
        def do_HEAD(self):
            '''
            Handle a HEAD request.
            '''
            logging.debug('HEADER %s' % (self.path))
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
 
        def info(self):
            pass
 
        def do_GET(self):
            '''
            Handle a GET request.
            '''
            logging.debug('GET %s' % (self.path))
 
            # Parse out the arguments.
            # The arguments follow a '?' in the URL. Here is an example:
            #   http://example.com?arg1=val1
            args = {}
            idx = self.path.find('?')
            if idx >= 0:
                rpath = self.path[:idx]
                args = cgi.parse_qs(self.path[idx+1:])
            else:
                rpath = self.path
 
            # Print out logging information about the path and args.
            if 'content-type' in self.headers:
                ctype, _ = cgi.parse_header(self.headers['content-type'])
                logging.debug('TYPE %s' % (ctype))
 
            logging.debug('PATH %s' % (rpath))
            logging.debug('ARGS %d' % (len(args)))
            if len(args):
                i = 0
                for key in sorted(args):
                    logging.debug('ARG[%d] %s=%s' % (i, key, args[key]))
                    i += 1
 
            if self.path == '/info' or self.path == '/info/': #Find path in URL then call info()
                self.send_response(200)  # OK
                self.send_header('Content-type', 'text/html')
                self.end_headers()
                self.info()
            else:
                # Get the file path.
                path = MyRequestHandler.m_opts.rootdir + rpath
                dirpath = None
                print(path)
                if os.path.exists(path) and os.path.isfile(path): # Find a file

                    self.send_response(200)  # OK
                    self.send_header('Content-type', 'text/html')
                    self.end_headers()
                    info_db={'title':'welcom','content':{'hello con':['a','b','c'],'nnn':1111}} # Template data
                    self.wfile.write(template(path,aa=info_db)) # print template to web browser
                else:
                    self.send_response(500)  # generic server error for now
                    self.send_header('Content-type', 'text/html')
                    self.end_headers()
 
                    self.wfile.write('<html>')
                    self.wfile.write('  <head>')
                    self.wfile.write('    <title>Error</title>')
                    self.wfile.write('  </head>')
                    self.wfile.write('  <body>')
                    self.wfile.write('    <p>%r not found</p>' % (repr(self.path)))
                    self.wfile.write('  </body>')
                    self.wfile.write('</html>')
 
        def do_POST(self):
            logging.debug('POST %s' % (self.path))
 
            # CITATION: http://stackoverflow.com/questions/4233218/python-basehttprequesthandler-post-variables
            ctype, pdict = cgi.parse_header(self.headers['content-type'])
            if ctype == 'multipart/form-data':
                postvars = cgi.parse_multipart(self.rfile, pdict)
            elif ctype == 'application/x-www-form-urlencoded':
                length = int(self.headers['content-length'])
                postvars = cgi.parse_qs(self.rfile.read(length), keep_blank_values=1)
            else:
                postvars = {}
 
            # Get the "Back" link.
            back = self.path if self.path.find('?') < 0 else self.path[:self.path.find('?')]
 
            logging.debug('TYPE %s' % (ctype))
            logging.debug('PATH %s' % (self.path))
            logging.debug('ARGS %d' % (len(postvars)))
            if len(postvars):
                i = 0
                for key in sorted(postvars):
                    logging.debug('ARG[%d] %s=%s' % (i, key, postvars[key]))
                    i += 1
 
            # HTML to display.
            self.send_response(200)  # OK
            self.send_header('Content-type', 'text/html')
            self.end_headers()
 
            # Display the POST variables.
            self.wfile.write('<html>')
            self.wfile.write('  <head>')
            self.wfile.write('    <title>Server POST Response</title>')
            self.wfile.write('  </head>')
            self.wfile.write('  <body>')
            self.wfile.write('    <p>POST variables (%d).</p>' % (len(postvars)))
 
            if len(postvars):
                self.wfile.write('    <table>')
                self.wfile.write('      <tbody>')
                i = 0
                for key in sorted(postvars):
                    i += 1
                    val = postvars[key]
                    self.wfile.write('        <tr>')
                    self.wfile.write('          <td align="right">%d</td>' % (i))
                    self.wfile.write('          <td align="right">%s</td>' % key)
                    self.wfile.write('          <td align="left">%s</td>' % val)
                    self.wfile.write('        </tr>')
                self.wfile.write('      </tbody>')
                self.wfile.write('    </table>')
 
            self.wfile.write('    <p><a href="%s">Back</a></p>' % (back))
            self.wfile.write('  </body>')
            self.wfile.write('</html>')
 
    return MyRequestHandler
 
 
def err(msg):
    '''
    Report an error message and exit.
    '''
    print('ERROR: %s' % (msg))
    sys.exit(1)
 
 
def getopts():
    '''
    Get the command line options.
    '''
 
    # Get the help from the module documentation.
    this = os.path.basename(sys.argv[0])
    description = "desc"
    epilog = ' '
    rawd = argparse.RawDescriptionHelpFormatter
    parser = argparse.ArgumentParser(formatter_class=rawd,
                                     description=description,
                                     epilog=epilog)
 
    parser.add_argument('-d', '--daemonize',
                        action='store',
                        type=str,
                        default='.',
                        metavar='DIR',
                        help='daemonize this process, store the 3 run files (.log, .err, .pid) in DIR (default "%(default)s")')
 
    parser.add_argument('-H', '--host',
                        action='store',
                        type=str,
                        default='localhost',
                        help='hostname, default=%(default)s')
 
    parser.add_argument('-l', '--level',
                        action='store',
                        type=str,
                        default='info',
                        choices=['notset', 'debug', 'info', 'warning', 'error', 'critical',],
                        help='define the logging level, the default is %(default)s')
 
    parser.add_argument('--no-dirlist',
                        action='store_true',
                        help='disable directory listings')
 
    parser.add_argument('-p', '--port',
                        action='store',
                        type=int,
                        default=8080,
                        help='port, default=%(default)s')
 
    parser.add_argument('-r', '--rootdir',
                        action='store',
                        type=str,
                        default=os.path.abspath('.'),
                        help='web directory root that contains the HTML/CSS/JS files %(default)s')
 
    parser.add_argument('-v', '--verbose',
                        action='count',
                        help='level of verbosity')
 
    parser.add_argument('-V', '--version',
                        action='version',
                        version='%(prog)s - v' + VERSION)
 
    opts = parser.parse_args()
    opts.rootdir = os.path.abspath(opts.rootdir)
    if not os.path.isdir(opts.rootdir):
        err('Root directory does not exist: ' + opts.rootdir)
    if opts.port < 1 or opts.port > 65535:
        err('Port is out of range [1..65535]: %d' % (opts.port))
    return opts
 
 
def httpd(opts):
    '''
    HTTP server
    '''
    RequestHandlerClass = make_request_handler_class(opts)
    server = BaseHTTPServer.HTTPServer((opts.host, opts.port), RequestHandlerClass)
    logging.info('Server starting %s:%s (level=%s)' % (opts.host, opts.port, opts.level))
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
    logging.info('Server stopping %s:%s' % (opts.host, opts.port))
 
 
def get_logging_level(opts):
    '''
    Get the logging levels specified on the command line.
    The level can only be set once.
    '''
    if opts.level == 'notset':
        return logging.NOTSET
    elif opts.level == 'debug':
        return logging.DEBUG
    elif opts.level == 'info':
        return logging.INFO
    elif opts.level == 'warning':
        return logging.WARNING
    elif opts.level == 'error':
        return logging.ERROR
    elif opts.level == 'critical':
        return logging.CRITICAL
 
 
def daemonize(opts):
    '''
    Daemonize this process.
 
    CITATION: http://stackoverflow.com/questions/115974/what-would-be-the-simplest-way-to-daemonize-a-python-script-in-linux
    '''
    if os.path.exists(opts.daemonize) is False:
        err('directory does not exist: ' + opts.daemonize)
 
    if os.path.isdir(opts.daemonize) is False:
        err('not a directory: ' + opts.daemonize)
 
    bname = 'webserver-%s-%d' % (opts.host, opts.port)
    outfile = os.path.abspath(os.path.join(opts.daemonize, bname + '.log'))
    errfile = os.path.abspath(os.path.join(opts.daemonize, bname + '.err'))
    pidfile = os.path.abspath(os.path.join(opts.daemonize, bname + '.pid'))
 
    if os.path.exists(pidfile):
        err('pid file exists, cannot continue: ' + pidfile)
    if os.path.exists(outfile):
        os.unlink(outfile)
    if os.path.exists(errfile):
        os.unlink(errfile)
 
    if os.fork():
        sys.exit(0)  # exit the parent
 
    os.umask(0)
    os.setsid()
    if os.fork():
        sys.exit(0)  # exit the parent
 
    print('daemon pid %d' % (os.getpid()))
 
    sys.stdout.flush()
    sys.stderr.flush()
 
    stdin = file('/dev/null', 'r')
    stdout = file(outfile, 'a+')
    stderr = file(errfile, 'a+', 0)
 
    os.dup2(stdin.fileno(), sys.stdin.fileno())
    os.dup2(stdout.fileno(), sys.stdout.fileno())
    os.dup2(stderr.fileno(), sys.stderr.fileno())
 
    with open(pidfile, 'w') as ofp:
        ofp.write('%i' % (os.getpid()))
 
 
def main():
    opts = getopts()
    if opts.daemonize:
        daemonize(opts)
    logging.basicConfig(format='%(asctime)s [%(levelname)s] %(message)s', level=get_logging_level(opts))
    httpd(opts)
 
 
if __name__ == '__main__':
    main()  # this allows library functionality
