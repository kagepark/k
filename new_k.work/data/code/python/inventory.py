#!/bin/python
# -*- coding: utf-8 -*-
# Kage Park
#
# Inventory Agent
# 2019/01/07
#
from __future__ import print_function
import requests
import json
import string
import subprocess
import sys
import os,argparse
import crypt
import time
import threading
import re
import ast


version='0.1.1'
DJ_ADDR='172.16.115.130'
DJ_PORT=8003
log_file=None

def str2url(string):
    return string.replace('/','%2F').replace(':','%3A').replace('=','%3D').replace(' ','+')

def put_data(data=None):
    if data is None:
        return 
    host_url='http://{0}:{1}/'.format(DJ_ADDR,DJ_PORT)
    print(host_url)
    print(data)
    try:
        r = requests.post(host_url,data=data)
    except requests.exceptions.RequestException as e:
        log("Django server({0}:{1}) has no response".format(django_addr,django_port))
        return False


def get_data(bmc_mac=None):
    if bmc_mac is None:
        return
    host_url='http://{0}:{1}/jfind/?bmc_mac={2}'.format(DJ_ADDR,DJ_PORT,str2url(bmc_mac))
    ss = requests.Session()
    try:
        r = ss.get(host_url)
    except requests.exceptions.RequestException as e:
        return False
    json_data=json.loads(r.text)
    return json_data


def rshell(cmd):
    Popen=subprocess.Popen
    PIPE=subprocess.PIPE
    p = Popen(cmd , shell=True, stdout=PIPE, stderr=PIPE)
    out, err = p.communicate()
    return p.returncode, out.decode("ISO-8859-1").rstrip(), err.decode("ISO-8859-1")

class Inventory:
    def __init__(self):
        pass

    def dmidecode():
        import dmidecode
        db={}
        for i in ['bios','system','baseboard','chassis','processor','cache','connector','slot']:
           if not i in db:
               db[i]={}
               tt=dmidecode.QuerySection(i)
               a=tt.keys()
               tt=tt[a[0]]
               for j in tt['data'].keys():
                   db[i].update({j:tt['data'][j]})

        db['memory']={}
        tt=dmidecode.QuerySection('memory')
        for i in tt.keys():
            if 'Maximum Capacity' in tt[i]['data']:
                db['memory'].update({'Maximum Capacity':tt[i]['data']['Maximum Capacity']})
            if 'Number Of Devices' in tt[i]['data']:
                db['memory'].update({'Number Of Devices':tt[i]['data']['Number Of Devices']})
            if 'Locator' in tt[i]['data']:
                db['memory'][tt[i]['data']['Locator']]={}
                db['memory'][tt[i]['data']['Locator']].update({'dmi_handle':'{0}'.format(i)})
                db['memory'][tt[i]['data']['Locator']].update(tt[i]['data'])
        return db

    def bmc():
        db={}
        tmp=rshell("ipmitool lan print")
        if tmp[0] == 0:
            bmc_lan_info=tmp[1]
            for i in bmc_lan_info.split('\n'):
                line=i.split(':')
                if len(line) == 2:
                    key=line[0].strip()
                    val=line[1].strip()
                    if key != 'Cipher Suite Priv Max' and len(key) > 0 and len(val) > 0:
                        db.update({'{0}'.format(key):'{0}'.format(val)})
        tmp=rshell("ipmitool bmc info")
        if tmp[0] == 0:
            bmc_lan_info=tmp[1]
            for i in bmc_lan_info.split('\n'):
                line=i.split(':')
                if len(line) == 2:
                    key=line[0].strip()
                    val=line[1].strip()
                    if len(key) > 0 and len(val) > 0:
                        db.update({'{0}'.format(key):'{0}'.format(val)})
        return db

############################################################
def read_stage_item(name,data):
    tmp={}
    b='{0}="(\w.*)"'.format(name)
    a=re.compile(b).findall(data)
    if len(a) == 0:
        b='{0}=(\w.*)'.format(name)
        a=re.compile(b).findall(data)
    if len(a) == 0:
        d=None
    else:
        d=a[0]
    tmp.update({name:d})
    return tmp

def read_stage_items(name,total_name,items,data):
    tmp={}
    total=int(re.compile('{0}=(\d)'.format(total_name)).findall(data)[0])
    tmp[total]={}
    for i in range(total):
        tmp[total][i]={}
        for n in list(items):
            b='{0}_{1}_{2}="(\w.*)"'.format(name,i,n)
            a=re.compile(b).findall(data)
            if len(a) == 0:
                b='{0}_{1}_{2}=(\w.*)'.format(name,i,n)
                a=re.compile(b).findall(data)
            if len(a) == 0:
                d=None
            else:
                d=a[0]
            if (name == 'DIMM' and n == 'PART' and (d is None or d == 'NO')) or (name == 'POWER' and n=='STATUS' and d == 'NotPresent'):
                break
            tmp[total][i].update({n:d})
    return tmp

def get_inventory():
    inv={}
    if os.path.exists('/root/stage1.conf') and os.path.exists('/root/stage2.conf'):
        with open('/root/stage2.conf','rb') as f:
            stage2=f.read()
        with open('/root/stage1.conf','rb') as f:
            stage1=f.read()

        #stage1
        for i in ['MBOARD_ID']:
            inv.update(read_stage_item(i,stage1))
        #stage2
        for i in ['BOARD_MANUFACTURER','BOARD_NAME','BOARD_SERIAL','BOARD_VER','BMC_MAC','BMC_IP','BMC_MODE','BMC_BOARD_MFR','EXPANDER_TOTAL','TPM_VER']:
            inv.update(read_stage_item(i,stage2))
        #stage2
        for i in [{0:'DIMM',1:'DIMM_SLOTS_TOTAL',2:['TYPE','SIZE','PART','SPEED','MANUFACTURER','SERIAL','RANK']},{0:'POWER',1:'POWER_TOTAL',2:['MODEL','SERIAL','MANUFACTURER','LOCATION','STATUS']},{0:'CPU',1:'CPU_NUM',2:['FAMILY','MODEL','MANUFACTURER','MAXSPEED','CORECOUNT','THREADCOUNT','SIGNATURE']},{0:'NET',1:'NET_TOTAL',2:['NAME','MAC','DRIVER','DRVER','BUS_NUM']}]:
            inv.update({i[0]:read_stage_items(i[0],i[1],i[2],stage2)})
    else:
        # read from system
        pass
    return inv

def inventory_db(find_key=None,find_val=None,inventory=None):
    if inventory is None and type(inventory).__name__ != 'dict':
        return
    bmc_mac=inventory['BMC_MAC']
    inventory.pop('BMC_MAC',None)
    data=get_data(bmc_mac)
    rc=None
    if len(data) == 0:
        inv=[]
# TYPE1
#        for name in list(inventory.keys()):
#            item={'item_name':name,'item_info':inventory[name]}
#            inv.append(item)
# TYPE2
        inv.append(inventory)
        pdata={'bmc_mac':'{0}'.format(bmc_mac),'inv':'{0}'.format(inv)}
#        print(pdata)
        rc=put_data(data=pdata)
        return rc
#        print(rc)
    else:
        if find_key is not None:
            if find_key in data.keys():
                # Get finding key value
                return(data[find_key])
            elif find_val is not None:
                # Put finding data to DB
                tt=[]
                # TYPE2
                tt.append({find_key:find_val})
                pdata={'bmc_mac':'{0}'.format(bmc_mac),'inv':'{0}'.format(tt)}
#                print(pdata)
                rc=put_data(data=pdata)
                return rc
        else:
            # Compare data and update DB (Not found then put data, removed data in local server then delete in DB)
            for key in inventory.keys():
                if key in data.keys():
                    if inventory[key] == data[key]:
                        #print('{0} is same data ({1})'.format(key,data[key]))
                        pass
                    else:
                        invs=ast.literal_eval(data[key])
                        if inventory[key] == invs:
                            #print('{0} is same data ({1})'.format(key,data[key]))
                            pass
                        else:
                            print('{0} is different data ({1} => {2})'.format(key,inventory[key],data[key]))
                    data.pop(key,None)
                else:
                    #print('need add {0}({1}) to DB Server'.format(key,inventory[key]))
                    tt=[]
                    # TYPE2
                    tt.append({key:inventory[key]})
                    pdata={'bmc_mac':'{0}'.format(bmc_mac),'inv':'{0}'.format(tt)}
                    rc=put_data(data=pdata)
            if len(data) > 0:
                for key in data.keys():
                    if key != 'OOB' and key == 'DCMS' and key != 'DESC' key != 'LOCATION' :
                        print('need delete {0} from DB server'.format(key))
            return rc

if __name__ == "__main__":

    find_key='BMC_IP'
    find_val=None
    inv=[]
#    inventory=get_inventory()
#    bmc_mac=inventory['BMC_MAC']
#    inventory.pop('BMC_MAC',None)
#    for name in list(inventory.keys()):
#        item={'item_name':name,'item_info':inventory[name]}
#        inv.append(item)
#    inv.append(inventory)
#    pdata={'bmc_mac':'{0}'.format(bmc_mac),'inv':'{0}'.format(inv)}
#    rc=put_data(data=pdata)

#    inventory=get_inventory()
    a=inventory_db(inventory=get_inventory())
    print(a)
    a=inventory_db(find_key='OOB',inventory=get_inventory())
    print(a)
#    a=inventory_db(find_key='OOB',find_val='EBAD-4ED0-63B3-282F-6858-7901',inventory=get_inventory())
#    print(a)
#    bmc_mac=inventory['BMC_MAC']
#    inventory.pop('BMC_MAC',None)
#    data=get_data(bmc_mac)

