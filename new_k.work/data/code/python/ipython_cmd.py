from IPython.terminal.prompts import Prompts, Token
import os
import getpass,socket
class MyPrompt(Prompts):
    def in_prompt_tokens(self, cli=None):
        #return [(Token,'k'),(Token.Prompt, '[{0}@{1} {2}]$ '.format(getpass.getuser(),socket.gethostname(),os.path.basename(os.getcwd())))]
        idn=getpass.getuser()
        ps="$"
        if idn == 'root':
            ps="#"
        return [(Token,'[{0}@{1}]'.format(idn,socket.gethostname())),(Token.Prompt, ' {0}{1} '.format(os.path.basename(os.getcwd()),ps))]

ip=get_ipython()
ip.prompts=MyPrompt(ip)

#import IPython
#IPython.embed()
#user_ns={}
#IPython.start_ipython(argv=[],user_ns=user_ns)
