#!/usr/bin/env python
# coding: utf-8
# encoding=utf8
#
# Copyright CEP(Kage Park)
# License : GPL
#   It can be sell by CEP Only.
#   Others according to GPL license(Only Freeware software).
#   Since : March 1994
# Kage
### test
# https://docs.python.org/3/library/cmd.html
# ref2 : https://github.com/python-cmd2/cmd2
# Default 4.7MB
# python 2.6~ for no newline on print()
from __future__ import print_function
import socket
import kNet
import kLine as kl
import kFile as kf
import sys,os,stat
import time
import kDict as kd
import kProgress as kpg
import subprocess
from datetime import datetime
# Shell
from cmd import Cmd
# Arguments
import argparse # 0.1MB


# Load python module
import imp,importlib
# Python module install
import pip
#from pip._internal import main as pip_main

# python command function
import inspect
#import traceback
#Python Code
import code

# fake sys.argv from string
import shlex

# custom cursor or console
import termios, fcntl
import select
import pty
# Line
import readline

# OS finding executable file like as which command
from distutils.spawn import find_executable

# WEB
import requests,re # 3MB increase binary

#User Network
import getpass

import threading

this_mod_name=__name__
_k_version='4.2.50'
###########################################################
#SELF FILE PATH
self_file=sys.executable
if os.path.basename(self_file) == 'python':
    _k_dest_dir=os.path.dirname(os.path.realpath(__file__)) # Python script file path
else:
    _k_dest_dir=os.path.dirname(self_file)  # Python compiled binary file path

pipe_name='{}/.k_sh'.format(_k_dest_dir)
tmp_db='{}/.k_sh.1'.format(_k_dest_dir)
self_k=False

sym_linked=False
sym_list=None
dfname=None

#Return Code
K_ERROR={'ERR'}
K_FAIL={'FAL'}
K_FALSE=K_FAIL
K_TRUE={'TRU'}
K_OK=K_TRUE
K_ALREADY={'ALR'}
K_NOT_FOUND={'NFO'}
K_NO_INPUT={'NIN'}
K_WRONG_FORMAT={'WFO'}
K_UNKNOWN={'UNK'}

#Global Commands
#def import_from(module, name):
#    module = __import__(module, fromlist=[name])
#    return getattr(module, name)

def pipe(pipe_name):
    LIBS().pipe(pipe_name)

class CMD:
    def _imports(self,*args):
        '''
        Import python module internal command
        '''
        unload=False
        module_line=LIBS().list2str(args).split(',')
        if '-r' in module_line[0]:
            unload=True
            module_line[0]=module_line[0].replace('-r','')
        for mod in module_line:
            mod_name=None
            mod_list=mod.split()
            if 'as' in mod_list: # support as command
                mod_name=mod_list[-1]
            if mod_list[0][0] == '!':
                LIBS().get_lib(mod_list[0][1:],name=mod_name)
            else:
                LIBS().imports(name=mod_name,filename=mod_list[0],global_var=globals(),unload=unload)

    def _mod_ls(self):
        '''
        Loaded module list
        '''
        for ii in [key for key in globals().keys() if isinstance(globals()[key], type(sys)) and not key.startswith('__')]:
            try:
                m_path=globals()[ii].__file__
            except:
                m_path=''
            print('%15s : %s'%(ii,m_path))

    def ksort(self,string=None,new_line='\n',field_id=None,field_symbol='space',reverse=False,print_field=None,num=False):
        if string is None:
            return []
        sort_arr=[]
        line_arr=string.split(new_line)
        if field_id is None or field_symbol is None:
            if reverse:
                if num:
                   return sorted(line_arr,reverse=True,key=float)
                else:
                   return sorted(line_arr,reverse=True)
            else:
                if num:
                   return sorted(line_arr,key=float)
                else:
                   return sorted(line_arr)
        else:
            sort_dict={}
            for ii in list(line_arr):
                if ii == '':
                    continue
                if field_symbol == 'space':
                    ii_tmp=ii.split()
                else:
                    ii_tmp=ii.split(field_symbol)
                if len(ii_tmp) < field_id-1:
                   print('ignore field_id({0}) at "{1}" string'.format(field_id,ii))
                else:
                   if print_field is None:
                       sort_dict[ii_tmp.pop(field_id)]=ii
                   else:
                       tmp=''
                       for ss in print_field.split(','):
                           if len(ii_tmp) < int(ss)-1:
                               print('out of range print_field({0})'.format(ss))
                           else:
                               if tmp == '':
                                   tmp='{0}'.format(ii_tmp[int(ss)])
                               else:
                                   if field_symbol == 'space':
                                       tmp='{0} {1}'.format(tmp,ii_tmp[int(ss)])
                                   else:
                                       tmp='{0}{1}{2}'.format(tmp,field_symbol,ii_tmp[int(ss)])
                       sort_dict[ii_tmp.pop(field_id)]=tmp
            if reverse:
                if num:
                    sort_dict_keys=sorted(sort_dict.keys(),reverse=True,key=float)
                else:
                    sort_dict_keys=sorted(sort_dict.keys(),reverse=True)
            else:
                if num:
                    sort_dict_keys=sorted(sort_dict.keys(),key=float)
                else:
                    sort_dict_keys=sorted(sort_dict.keys())
            for jj in sort_dict_keys:
                sort_arr.append(sort_dict[jj])
            return sort_arr

class LIBS:
    def symlink(self,linked=False,cmd_list=None,dfname=None):
        if linked and cmd_list:
             if dfname is None:
                 dfname=os.path.basename(sys.argv[0])
             # Clean symlink
             if cmd_list and linked:
                 opwd=os.getcwd()
                 os.environ['PATH']='{}'.format(os.environ['PATH'].replace(_k_dest_dir,''))
                 os.chdir(_k_dest_dir)
                 for slnk in cmd_list:
                     if slnk != dfname and os.path.exists(slnk) and os.path.islink(slnk):
                         os.remove(slnk)  # remove pipe file
                 os.chdir(opwd)
        else:
            # symlink
            cmd_list=LIBS().get_cmd_list(shell=True)
            opwd=os.getcwd()
            for tt in ['python','shell']:
                cmd_list+=list(cmd_dict[tt].keys())
            #dest_dir=os.path.dirname(sys.executable)
            # Get current command running path(os.getcwd()) not real file path: os.path.abspath(__file__)
            # running command name : sys.argv[0]
            # source python script file name : __file__
            # running file real path : os.path.dirname(sys.executable)
            # 
            if cmd_list:
                os.chdir(_k_dest_dir)
                dfname=os.path.basename(sys.argv[0])
                if os.path.isfile(dfname):
                    os.environ['PATH']='{}:{}'.format(_k_dest_dir,os.environ['PATH'])
                    linked=True
                    for slnk in cmd_list:
                        if not slnk in [dfname,'shell','python','include'] and slnk[0] != '_' and not os.path.isfile(slnk):
                            try:
                               os.symlink('./'+dfname, slnk)
                            except:
                               pass
                os.chdir(opwd)
            return linked,cmd_list,dfname


    def pipe(self,pipe_name):
        while True:
            if not os.path.exists(pipe_name):
                os.mkfifo(pipe_name)
            pipein = open(pipe_name, 'r')
            line = pipein.readline()
            if line:
                line_a=line.split()
                if line_a[0] == 'quit':
                    print('bye~ pipe')
                    break
                cmd_analy=line_a[0].split('.')
                if len(cmd_analy) == 1:
                    rc=self.run_function_name_argv(self.get_module_obj(this_mod_name),line_a[0],*tuple(line_a[1:]))
                elif len(cmd_analy) == 2:
                    if cmd_analy[0] in globals():
                       # get class object from name string at globals() : globals()[<class name>]
                       class_obj=globals()[cmd_analy[0]]() # Change class object to class instance
                       rc=self.run_function_name_argv(class_obj,cmd_analy[1],*tuple(line_a[1:]))
                    else:
                       print('Class({}) not found'.format(cmd_analy[0]))
                else:
                    print('Command Error:',line)

    def run_shell(self,args_list,env='PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin\nexport SHELL=$([ -f /bin/bash ] && echo /bin/bash || /bin/sh)\n',pre_code=''):
        return subprocess.call(['bash','-c',env + pre_code + args_list])
#        return os.system(env + args_list)

    def exit(self,signal=None,frame=None,msg=None,code=None):
        if code == "no":
          return K_OK
        if msg is not None:
          print(msg)
        if code is None:
          sys.exit(0)
        else:
          sys.exit(CODE().code2shell(code))

    def get_lib(self,filename,name=None):
        args_a=filename.split('/')
        lib_type=args_a[0]
        soc=kNet.gen_client_socket(ip,port)
        kNet.put_packet(soc,['glib',filename])
        rc=kNet.get_packet(soc)
        if rc: 
            file_name=os.path.basename(filename)
            if name is None:
                name=os.path.splitext(file_name)[0]
            if lib_type == 'python':
                rc=self.imports(code=rc,name=name,global_var=globals())
                if rc:
                    print('load {} => {}\r'.format(filename,name))
                    #print('\nload {} => {}\r'.format(filename,name))
                else:
                    print('not loaded or not support : {}\n'.format(filename))
                return rc
            elif lib_type == 'shell':
                pass
        else:
            print('not loaded or not support : {}\r'.format(filename))

    def imports(self,code=None,name=None,filename=None,global_var=None,unload=False):
        def load_mod(filename): # .pyc or .py or <module name>(string)
            if os.path.isfile(filename):
               file_path=os.path.dirname(os.path.abspath(filename))
               file_name=os.path.basename(filename)
               if not file_path in sys.path:
                   sys.path.append(file_path)
               filename=os.path.splitext(file_name)[0]
               if not os.path.splitext(file_name)[1] in ['.py','.pyc']:
                   print('wrong format file type')
                   return False
            if unload:
               try:
                   imp.reload(self.get_module_obj(filename))
                   #del module correctly work
               except:
                   pass
            try:
               return importlib.import_module(filename)
            except:
               print("\nCan't load {0}\r\n".format(filename))
            return False
        def load_code_module(code,name):
            if unload:
               try:
                   imp.reload(self.get_module_obj(name))
               except:
                   pass
            module=imp.new_module(name)
            try:
                exec code in module.__dict__
            except:
                return False
            return module
        if (code and name) or filename:
            module=False
            if filename:
                if os.path.isfile(filename): # .py .pyc  .txt ....
                    module=load_mod(filename)# load .py and .pyc 
                    if module is False:# Read Python Code from .py or .txt ...
                        with open(filename,'r') as f:
                            code=f.read()
                        if name is None:
                            file_name=os.path.basename(filename)
                            name=os.path.splitext(file_name)[0]
                        module=load_code_module(code,name)
                else:
                    module=load_mod(filename)# load module name
                    if module is False:
                        print("Can't load {0}".format(filename))
                        return False
            else:
                module=load_code_module(code,name) # load code 
            if module:
                if global_var:
                    global_var[module.__name__]=module
#                print('load {} => {}'.format(filename,module.__name__))
                return module
        else:
            print("Can't load {0}".format(name))
            return False

    def list2str(self,list_data,symbol=' '):
        '''
        convert list to string with symbol between data
        '''
        return symbol.join(list_data)

    def get_module_obj(self,module_name=None):
        '''
        get module object from module name(String Text)
        '''
        if module_name:
            if module_name in globals():
                return globals()[module_name]
            elif module_name in sys.modules:
                return sys.modules[module_name]
        return inspect.getmodule(inspect.stack()[1][0]) #self module ???

    def get_class_in_module(self,module,class_name):
        '''
        get class object from module object for class name (string text)
        '''
        try:
            return getattr(module,class_name)
        except:
            return

    def get_class_instance_in_module(self,module,class_name):
        '''
        get class instance from module object for class name (string text)
        '''
        try:
            return getattr(module,class_name)()
        except:
            return

    def get_method_in_class(self,_class_, func_name):
        '''
        Get method(function) object from class (object or instance) for function name(string text)
        _class_ : class object (<module>.<classname>) or class instance (<module>.<classname>())
        '''
        if hasattr(_class_,'__class__'):
            try:
                return getattr(_class_,func_name)
            except:
                return
        #elif inspect.isclass(_class_): # Same function as below. but sometimes 2.7.12 has issue
        elif hasattr(_class_,'__dict__'):
            class_dic=_class_.__dict__
            if func_name in class_dic:
                return class_dic[func_name]

    def get_function_list_in_module_name(self,module_name=None):
        '''
        get function dict from module name (string text)
        return : {<function name string>:<function object>}
        '''
        aa={}
        if module_name is None:
            module_name='__main__'
        if module_name in sys.modules:
            for name,obj in inspect.getmembers(sys.modules[module_name]):
                if inspect.isfunction(obj): # inspect.ismodule(obj) check the obj is module or not
                    aa.update({name:obj})
        return aa

    def get_function_args(self,func_obj,order=False,data=True):
        """
        Return function's arguments
        order=False : a dictionary of input parameter: {arg_name:default_values, ...}
        order=True  : a list of input parameter : [(arg_name,default_values), ...]
        """
        try:
            args, varargs, keywords, defaults = inspect.getargspec(func_obj)
        except:
            return
        if data:
            if order:
                arg_val=[]
            else:
                arg_val={}
        else:
            arg_val=''
        if args:
            val_t=-1
            if defaults:
                val_t=len(defaults)-1
            arg_t=len(args)
            for ii in range(arg_t,0,-1):
                i=ii-1
                if val_t >= 0:
                    if data:
                        if order:
                            arg_val=[(args[i],defaults[val_t])]+arg_val
                        else:
                            arg_val.update({args[i]:defaults[val_t]})
                    else:
                        if len(arg_val):
                            arg_val='{0}={1},{2}'.format(args[i],defaults[val_t],arg_val)
                        else:
                            arg_val='{0}={1}'.format(args[i],defaults[val_t])
                    val_t=val_t-1
                else:
                    if data:
                        if order:
                            arg_val=[(args[i],)]+arg_val
                        else:
                            arg_val.update({args[i]:''})
                    else:
                        if len(arg_val):
                            arg_val='{0},{1}'.format(args[i],arg_val)
                        else:
                            arg_val='{0}'.format(args[i])
        return arg_val

    def get_func_header(self,func_name,obj=None):
        if inspect.ismodule(obj):
            module_list=LIBS().get_function_list_in_module_name(obj.__name__)
            if func_name in module_list:
                func_obj=module_list[func_name]
        elif hasattr(obj,'__class__') or inspect.isclass(obj): # check class from class instance, : <module>.<classname>()
            func_obj=self.get_method_in_class(obj,func_name)
        elif inspect.isfunction(obj):
            func_obj=obj
        else:
            func_obj=self.get_func_obj_from_str(func_name)
        if func_obj:
            return '{}({})'.format(func_name,LIBS().get_function_args(func_obj,True,False))

    def get_method_name_list_in_class(self,_class_):
        '''
        Get method name in the Class
        _class_ : class object (<module>.<classname>) or class instance (<module>.<classname>())
        '''
        return [i for i in dir(_class_) if not inspect.ismethod(i)]

    def is_method_in_class(self,class_name, func_name):
        '''
        Check method name in the Class
        usage) is_method_in_class(<class name>,'<func name>')
        '''
        if func_name in self.get_method_name_list_in_class(class_name):
            return True
        return False

    def get_func_obj_from_str(self,input_str):
        if type(input_str) is str:
            input_a=input_str.split('.')
            func_name=input_a.pop(-1)
            if len(input_a) > 0:
                mod_name=input_a.pop(0)
                class_name=''
                for i in input_a:
                    if len(class_name):
                        class_name='{0}.{1}'.format(class_name,i)
                    else:
                        class_name='{0}'.format(i)
                module=self.get_module_obj(mod_name)
                if module:
                    class_instance=self.get_class_instance_in_module(module,class_name)
                    if class_instance:
                        return self.get_method_in_class(class_instance,func_name)
                    else:
                        return getattr(module,func_name)
            else:
                if func_name in globals():
                    return globals()[func_name]


    def run_function_name_argv(self,obj,func_name,*arg): # Get variable inputs like as console input(argv)
        '''
        obj : module or class instance (not support class object)
        func_name: function name string
        *args : put argument for the function's parameter (tuple)
        Run function name in object with variable input.
        but the function take fixed variable inputs
        use)run_function_name_arg(<class>,<func_name>[,'v1',...])
        ex)
        run_function_name_arg(abc,test,3,'c') : work
        run_function_name_arg(abc,test,3) : not work

        arg=tuple(sys.argv[2:])
        run_function_name_arg(abc,mm,*arg) : work

        class abc:
           def test(a=None,b=None):
             .......
           def mm(*arg):
             .......
        '''
        if LIBS().is_method_in_class(obj,func_name):
#            print(obj,func_name,arg)
            if len(arg) == 0:
                return getattr(obj,func_name)()
            else:
                return getattr(obj,func_name)(*arg) # Extract tuple to variable

    def get_cmd_list(self,shell=False):
        '''
        get defined function list
        '''
        func_list=[]
        if shell:
            for ii in list(LIBS().get_function_list_in_module_name(this_mod_name).keys()):
                if ii[0] != '_' and not ii in ['pipe','import','history','ssh']:
                    func_list.append(ii)
        else:
            func_list=LIBS().get_function_list_in_module_name(this_mod_name).keys()
        if 'cmd' in globals():
            func_list=func_list+list(LIBS().get_function_list_in_module_name('cmd').keys())
        return func_list

    def trash(self,filename,cmd='del'): # cmd, dk7
        soc=kNet.gen_client_socket(ip,port)
        kNet.put_packet(soc,[cmd,filename])
        rc=kNet.get_packet(soc)
        return rc

    #def push(self,filename,dest_dir):
    def push(self,filename,dest_file,cmd='save'): #cmd : save, lib(update), sk7: save K
        if os.path.isfile(filename):
            with open(filename,'r') as f:
                fd=f.read()
            pdata={'filename':'{0}'.format(dest_file),'data':fd}
            soc=kNet.gen_client_socket(ip,port)
            kNet.put_packet(soc,[cmd,pdata])
            rc=kNet.get_packet(soc)
            return rc

    def get(self,filename,save_file=None,cmd='get',progress=False): # cmd: get, k7(k.py)
        file_name=os.path.basename(filename)
        if save_file:
           if not os.path.isdir(os.path.dirname(save_file)):
               print('{} directory not found'.format(os.path.dirname(save_file)))
               return False
        else:
           save_file='/tmp/{}'.format(file_name)
        soc=kNet.gen_client_socket(ip,port)
        kNet.put_packet(soc,[cmd,filename])
        if progress is True:
            rc=kNet.get_packet(soc,progress='Downloading : %s => %s'%(file_name,save_file))
        else:
            rc=kNet.get_packet(soc)
        try:
            with open(save_file,'w') as f:
                f.write(rc)
            return save_file
        except:
            return False

# Code convert
class CODE:
    def __init__(self,path=None):
       self.require_pkg=[]

    def get_code(self,code=None):
        if code is None:
            return K_NO_INPUT
        elif type(code).__name__ == 'set':
            return next(iter(code))
        else:
            return K_ERROR

    def int2code(self,code):
        if code == -1:
            return K_ERROR
        elif code == 0:
            return K_OK
        else:
            return K_FAIL
    def code2int(self,code):
        if code == K_TRUE:
           return 1
        elif code == K_ERROR:
           return -1
        else:
           return 0

    def shell2code(self,code):
        if code==0 or code is None:
           return K_TRUE
        elif code==-1:
           return K_ERROR
        else:
           return K_FAIL

    def code2shell(self,code):
        if code == K_TRUE:
           return 0
        elif code == K_ERROR:
           return -1
        else:
           return 1

    def bool2code(self,code):
        if code:
           return K_TRUE
        else:
           return K_FAIL

    def code2bool(self,code):
        if code == K_TRUE or code == K_OK:
           return True
        else:
           return False

    def code_convert(self,code,mode='cep'):
        code_type=type(code).__name__
        if mode == 'cep' or mode == 'set':
           if code_type == 'int' or (code_type == 'str' and CEP().is_int(code)):
              return self.int2code(int(code))
           elif code_type == 'bool':
              return self.bool2code(code)
           elif code_type == 'set':
              return code
        elif mode == 'shell':
           if code_type == 'int' or (code_type == 'str' and CEP().is_int(code)):
              return self.code2shell(self.int2code(int(code)))
           elif code_type == 'bool':
              return self.code2shell(self.bool2code(code))
           elif code_type == 'set':
              return self.code2shell(code)
        elif mode == 'bool':
           if code_type == 'int' or (code_type == 'str' and CEP().is_int(code)):
              return self.code2bool(self.int2code(int(code)))
           elif code_type == 'bool':
              return code
           elif code_type == 'set':
              return self.code2bool(code)
        elif mode == 'int':
           if code_type == 'int' or (code_type == 'str' and CEP().is_int(code)):
              return int(code)
           elif code_type == 'bool':
              return self.code2int(self.bool2code(code))
           elif code_type == 'set':
              return self.code2int(code)
        return code

    def code_check(self,code,check):
        '''
        check return code between return-code and user define code.
        single code(True) check: code_check(abc,True)
          (abc same as True? if yes then True, no then False)
        multi code(1,None,False) check: code_check(abc,[1,None,False])
          (abc's return code is in [1,None,False]? if yes then True, no then False)
        string check : code_check(abc,'yes')
          (abc's return same as 'yes'? if yes then True, no then False)
        Checkable : CEP's code, Bool, Int, Shell, String, ....
        '''
        if type(code).__name__ == 'list':
            code=code[0]
        if type(check).__name__ == 'tuple':
            check=check[0]
        check_type=type(check).__name__
        if check_type == 'list':
            for icheck in list(check):
                icheck_type=type(icheck).__name__
                if self.code_convert(code,mode=icheck_type) == icheck:
                    return True
        else:
            if self.code_convert(code,mode=check_type) == check:
                return True
        return False

class kPrompt(Cmd):
    last_output = ''
    realstdin = sys.stdin
    mocking=False
    breakReadLine=False
    use_rawinput=0
    shell_end=True
    file = None
    old_dir=os.getcwd()
    history=[]
    CMDLISTS=['help','ls','quit','cd','size','import','include','put','del']

    def KReadLine2(self): #very good
        cursor=0
        history_idx=0
        keep_new_line=False
        line=''

        fd = sys.stdin.fileno()
        oldterm = termios.tcgetattr(fd)
        oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)

        newattr = termios.tcgetattr(fd)
        newattr[3] = newattr[3] & ~termios.ICANON
        newattr[3] = newattr[3] & ~termios.ECHO
        termios.tcsetattr(fd, termios.TCSANOW, newattr)
        sys.stdout.flush()
        while True:
            inp, outp, err = select.select([sys.stdin], [], [])
            c = sys.stdin.read()
            # analysis line
            before_first,before_end,line_first,line_end=kl.line_dbg(line)
            if c == '\x1b[3~': # delete key
                if cursor < len(line):
                    front=line[:cursor]
                    backend=line[cursor+1:]
                    sys.stdout.write(backend+' ')
                    sys.stdout.write('\033[{0}D'.format(len(backend)+1))# 1 move left, if 3 move then '\033[3D'
                    line=front+backend
            elif c == '\x1b[D': # LEFT
                if cursor >0:
                    sys.stdout.write('\b')
                    cursor-=1
                    #sys.stdout.write('\033[1D')# 1 move left, if 3 move then '\033[3D'
            elif c == '\x1b[C': # Right
                if cursor < len(line):
                    sys.stdout.write('\033[1C') # 1 move right, if 3 move then '\033[3C'
                    cursor+=1
            elif c == '\x1b[A': # UP
                if self.history:
                    if history_idx < len(self.history):
                        history_idx+=1
                        get_history=self.history[-history_idx]
                        sys.stdout.write('\x1b[2K') # erase current line
                        sys.stdout.write('\r'+self.prompt+self.history[-history_idx]) # \r: cursor start from 0
                        sys.stdout.flush()
                        line=self.history[-history_idx]
                        cursor=len(line)
            elif c == '\x1b[B': # Down
                if self.history:
                    if history_idx > 0:
                        history_idx-=1
                        get_history=self.history[-history_idx]
                        sys.stdout.write('\x1b[2K') # erase current line
                        sys.stdout.write('\r'+self.prompt+self.history[-history_idx])
                        line=self.history[-history_idx]
                        cursor=len(line)
            elif c == '\x1bOH': # HOME Key
                cursor=0
                sys.stdout.write('\r'+'\033[{0}C'.format(len(self.prompt))) # 1 move right, if 3 move then '\033[3C'
            elif c == '\x1bOF': # END Key
                cursor=len(line)
                sys.stdout.write('\r'+'\033[{0}C'.format(len(self.prompt)+len(line))) # 1 move right, if 3 move then '\033[3C'
            elif c == ' ' and cursor < len(line): # SPACE KEY
                front=line[:cursor]
                backend=line[cursor:]
                sys.stdout.write(' '+backend)
                line=front+' '+backend
                sys.stdout.write('\033[{0}D'.format(len(backend)))# 1 move
                cursor+=1

            elif len(c) == 1 and ord(c) == 4: # Control+d
                # Recover the terminal
                termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
                fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
                self.do_quit()

            elif len(c) == 1 and ord(c) == 127: # backspace key
                if cursor > 0 and cursor == len(line): # delete from backend
                    sys.stdout.write('\b \b')
                    line=line[:len(line)-1]
                    cursor-=1
                elif cursor < len(line): # delete from middle
                    front=line[:cursor-1]
                    backend=line[cursor:]
                    sys.stdout.write('\b \b')
                    sys.stdout.write(backend+' ')
                    sys.stdout.write('\033[{0}D'.format(len(backend)+1))# move left to cursor
                    line=front+backend
                    cursor-=1
            elif c == '\t' and keep_new_line is False: # Tab key
                found=None
                if line_end is None:
                    find_word=''
                else:
                    find_word=line_end
                if line_end and line_end != '\n':
                    line_arr=line_end.split('/')
                    if len(line_arr)>1: # search defined directory
                        if line_end[-1] == '/':
                            find_dir=line_end
                            find_word=''
                        else:
                            find_dir='/'.join(line_arr[:-1])
                            find_word=line_arr[-1]
                            if not find_dir:
                                find_dir='/'
                        found=kl.completion(kf.list_in_dir(find_dir),find_word)
                    else: # Search current directory
                        found=kl.completion(kf.list_in_dir('.'),line_end)
                elif line_first and not line_end: # search command and complete command
                    find_word=line_first
                    found=kl.completion(self.CMDLISTS+LIBS().get_cmd_list(),line_first)
                else: # Nothing then print command list
                    find_word=line_end
                    sys.stdout.write('\n') # erase current line
                    sys.stdout.write('\r'+'\t'.join(self.CMDLISTS+LIBS().get_cmd_list())+'\n')
                    sys.stdout.write('\r'+self.prompt+line)
                if found:
                    if len(found) == 1:
                        adding_str=found[0][len(find_word):]
                        if len(adding_str): # If found something then complete cursor string
                            print(adding_str,end='')
                            line+=adding_str
                            cursor+=len(adding_str)
                        else: # print currnt file or dir list when already completed command
                            sys.stdout.write('\n') # erase current line
                            sys.stdout.write('\r'+'\n '.join(kf.list_in_dir('.'))+'\n')
                            sys.stdout.write('\r'+self.prompt+line)
                    else:
                        sys.stdout.write('\n') # erase current line
                        sys.stdout.write('\r'+'\n'.join(found)+'\n')
                        sys.stdout.write('\r'+self.prompt+line)
                sys.stdout.flush()
            elif c in ['\r','\n','\r\n']: # Enter ; return value
                cursor=0
                before_first,before_end,line_first,line_end=kl.line_dbg(line)
                if keep_new_line is False and line_end in ['{',':','\\']:
                    keep_new_line=True
                    print(c)
                if keep_new_line is False or (keep_new_line and ( line_first == '}' or (line_end == '\n' and line_first=='\n'))): # Return all data
                    keep_new_line=False
                    self.history.append(line)
                    # Recover the terminal
                    termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
                    fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
                    print('')
                    return line
                    line=''
                else:
                    line+=c # Get a line mark
                if keep_new_line:
                    print('> ',end='') # prompt after new line
                else:
                    print(self.prompt,end='') # prompt after new line
            else: # get typed charactor on current cursor
                if cursor < len(line):
                    front=line[:cursor]
                    backend=line[cursor:]
                    sys.stdout.write(c+backend)
                    line=front+c+backend
                    sys.stdout.write('\033[{0}D'.format(len(backend)))# 1 move
                    cursor+=1
                else:
                    print(c,end='') # type prompt
                    line+=c         # get type prompt
                    cursor+=1
            sys.stdout.flush()

    def mycmdloop(self, intro=None):
        self.preloop()
        if self.use_rawinput and self.completekey:
            try:
                self.old_completer = readline.get_completer()
                readline.set_completer(self.complete)
                readline.parse_and_bind(self.completekey+": complete")
            except ImportError:
                pass
        try:
            if intro is not None:
                self.intro = intro
            if self.intro:
                self.stdout.write(str(self.intro)+"\n")
            stop = None
            line=''
            while not stop:
                if self.cmdqueue:
                    line = self.cmdqueue.pop(0)
                else:
                    if self.use_rawinput:
                        try:
                            print(self.prompt,end='')
                            line = self.KReadLine2()
                        except EOFError:
                            line = 'EOF'
                    else:
                        self.stdout.write(self.prompt)
                        self.stdout.flush()
                        line = self.KReadLine2()
                if line:
                    line = line.rstrip('\r\n')

                    line = self.precmd(line)
                    stop = self.onecmd(line)
                    stop = self.postcmd(stop, line)
            self.postloop()
        finally:
            if self.use_rawinput and self.completekey:
                try:
                    readline.set_completer(self.old_completer)
                except ImportError:
                    pass

    def cmdloop_with_mykey(self, intro):
        doQuit = False
        while doQuit != True:
            try:
                if intro!='':
                    cintro=intro
                    intro=''
                    self.mycmdloop(cintro)
                else:
                    self.intro=''
                    self.mycmdloop()
                doQuit = True
            except KeyboardInterrupt:
                sys.stdout.write('\n')

    def do_function_list(self,args):
        '''
        show available function list
        '''
        class_name=''
        module=None
        if len(args):
            args_a=args.split()
            mod_name=args_a.pop(0)
            module=LIBS().get_module_obj(mod_name)
            if module:
                for i in args_a:
                    if len(class_name):
                        class_name='{0}.{1}'.format(class_name,i)
                    else:
                        class_name='{0}'.format(i)
                if len(class_name):
                    class_instance=LIBS().get_class_instance_in_module(module,class_name)
                else:
                    module=module.__name__ 
        else:
            module=this_mod_name
        if module is None:
            print('not loaded {0}'.format(args))
            return
        if len(class_name):
            for ii in LIBS().get_method_name_list_in_class(class_instance):
                if ii[0] != '_':
                    if hasattr(cep.CMD(),ii):
                        try:
                            print('{0}({1})'.format(ii,LIBS().get_function_args(getattr(CMD,ii),True,False)))
                        except:
                            print('{0}(???)'.format(ii))
        else:
            func_list=LIBS().get_function_list_in_module_name(module)
            for ii in func_list.keys():
                print('{0}({1})'.format(ii,LIBS().get_function_args(func_list[ii],True,False)))

            if 'cep' in globals():
                CMD=cep.CMD()
                for ii in LIBS().get_method_name_list_in_class(CMD):
                    if ii[0] != '_':
                        if hasattr(cep.CMD(),ii):
                            try:
                                print('{0}({1})'.format(ii,LIBS().get_function_args(getattr(CMD,ii),True,False)))
                            except:
                                print('{0}(???)'.format(ii))

    def do_put_func(self,args):
        ''' put python function'''
        exec(args,globals())

    def default(self,args):
        args_a=shlex.split(args) # get argument like as shell format
        sub_cmd_args=tuple(args_a[1:])
        func_list=LIBS().get_function_list_in_module_name(this_mod_name)
        if args_a[0] in func_list: # run func in Local 
            print(LIBS().run_function_name_argv(sys.modules[__name__],args_a[0],*sub_cmd_args))
        elif 'cep' in globals() and LIBS().is_method_in_class(cep.CMD(),args_a[0]): # run func in CEP
            print('{0} < {1}'.format(args_a[0],sub_cmd_args))
            print(LIBS().run_function_name_argv(cep.CMD(),args_a[0],*sub_cmd_args))
        else:
            adapt_cmd=LIBS().run_function_name_argv(None,args_a[0],*sub_cmd_args) # run func in any module
            if adapt_cmd is not False:
                if adapt_cmd:
                    print(adapt_cmd)
            else:
                os.system(args) # OS default command

    def do_shell(self, line): # it works for "!<command> in k shell"
        "Run a shell command"
        if len(line) == 0:
            print('k shell')
        else:
            print(line)
            if line.split()[0] == 'vi':
                self.do_vi(LIBS().list2str(line.split()[1:]))
            else:
                print("running shell command:", line)
                os.system(line)
    
    def do_pr(self,args):
        '''Run python code'''
        print(eval(args,globals(),locals()))

    def do_quit(self):
        """Quits the program."""
        print("Bye~ K.")
        raise SystemExit

    def do_version(self,args):
        """Quits the program."""
        print(_k_version)

    def do_compile(self,args):
        if not find_executable('pyinstaller'):
            if find_executable('pip'):
                os.system('pip install pyinstaller')
            else:
                if hasattr(pip, 'main'):
                    pip.main(['install', 'pyinstaller'])
                else:
                    pip._internal.main(['install', 'pyinstaller'])
        if os.path.isfile(args):
            os.system('pyinstaller --onefile {}'.format(args))

    def do_ls(self,args):
        '''show file list'''
        if len(args) > 0 and args[0] == '!':
            soc=kNet.gen_client_socket(ip,port)
            kNet.put_packet(soc,['ls','{0}'.format(args[1:])])
            rc=kNet.get_packet(soc)
            for ii in rc:
                print("""%s"""%(ii)) # recover data to readable data
        else:
            os.system('ls --color {0}'.format(args))

    def do_cp(self,args):
        '''
        copy file
        cp <file> !<category>
        cp <file> <id>@<ip>:<directory>
        cp !<category>/<file> <dir>
        cp http://xxxx/file  <dir>
        cp <id>@<ip>:/path/file  <dir>
        '''
        whttp=re.compile('^http://')
        wftp=re.compile('^ftp://')
        args_a=args.split()
        args_n=len(args_a)
        src=args_a[0]
        dest_dir=args_a[-1]
        if dest_dir[0] == '!' and args_n >1: # KAGE DB
            dest_dir=dest_dir[1:]
            for sf in args_a[:len(args_a)-1]:
                print(LIBS().push(sf,'{}/{}'.format(dest_dir,os.path.basename(sf))))
        elif src[0] == '!' and args_n >=1: # KAGE DB
            if args_n <= 1:
                dest_dir='/tmp'
            file_name=os.path.basename(src[1:])
            print(LIBS().get(src[1:],'/{}/{}'.format(dest_dir,file_name),progress=True))
        elif args_n >= 1 and (whttp.search(src) or wftp.search(src)): # wget
            if args_n > 1:
                filename=dest_dir
            else:
                filename=src.split('/')[-1]
            try:
                print('download: {0}'.format(src))
                r=requests.get(src,stream=True)
            except requests.exceptions.RequestException as e:
                print('''can't download {}'''.format(src))
            pp=0
            with open(filename,'wb') as f:
               for chunk in r.iter_content(chunk_size=1024):
                  f.write(chunk)
                  f.flush()
                  pp=kpg.progress(pp=pp,msg=filename)
            print('save at {}'.format(filename))
        else: # Linux
            os.system('cp '+args)

    def do_rm(self,args):
        '''
        delete file/data
        rm !<category>/<dest>
        rm <files> or <directory>
        '''
        if len(args) > 0 and args[0] == '!':
            args_a=args[1:].split()
            if len(args_a) == 1:
                print(LIBS().trash(args_a[0]))
        elif find_executable('rm'):
            os.system('rm '+args)
        else:
            sub_dir=False
            for arg in args.split():
                if arg[0] == '-':
                   if 'r' in arg:
                      sub_dir=True
                else:
                    if os.isfile(arg):
                        os.remove(arg)
                    elif os.isdir(arg) and sub_dir:
                        shutil.rmtree(arg)
                    else:
                        print('''can't delete directory:{}'''.format(arg))

    def do_ssh(self,args):
        '''
        SSH Command without hostkey check
        '''
        if find_executable('ssh'):
            ssh_opt='-o StrictHostKeychecking=no -o CheckHostIP=no -o ConnectTimeout=5 -o UserKnownHostsFile=/dev/null' 
            os.system('ssh {0} '.format(ssh_opt) + args)
        else: # Simple ssh code with python only
            pass
        #ssh(*tuple([args]))

    def do_vi(self,args):
        '''
        vi command
        '''
        lib_edit=False
        lib_file=''
        dest_dir=''
        tmp_file=''
        if args[0] == '!':
            lib_edit=True
            lib_file=args[1:]
            dest_dir=lib_file.split('/')[0]
            tmp_file='/tmp/{}'.format(os.path.basename(lib_file))
            LIBS().get(lib_file,tmp_file)
            args=tmp_file

        if find_executable('vim'):
            vimrc='/tmp/.vim'
            vimrcfp=open(vimrc,"w")
            vimrcfp.write("set title\n")
            vimrcfp.write("set laststatus=2\n")
            vimrcfp.write("set tabstop=4\n")
            vimrcfp.write("set expandtab\n")
            vimrcfp.write("set shiftwidth=4\n")
            vimrcfp.write("set softtabstop=4\n")
            vimrcfp.write("set visualbell\n")
            vimrcfp.write("set ruler\n")
            vimrcfp.write("syntax on")
            vimrcfp.close()
            os.system('TERM=linux vim -u ' + vimrc + ' ' + args)
            os.remove(vimrc)
        elif find_executable('vi'):
            os.system('vi ' + args)
        else: # Symple text edit code here
            print('not yet. please intall vim')
            return
        if lib_edit:
            print(LIBS().trash(lib_file))
            print(LIBS().push(tmp_file,dest_dir))
            os.remove(tmp_file)
                       
    def do_cd(self,args):
        '''
        Change directory
        '''
        if args:
            if args == '-':
                args=self.old_dir
        else:
            args='~'
        
        dest_dir=os.path.expanduser(args)
        if os.path.isdir(dest_dir):
            os.chdir(dest_dir)
            self.prompt = '[{0} {1}]$ '.format(prompt_str,os.path.basename(os.getcwd()))
            old_dir=dest_dir
        else:
            print('{0} is not a directory'.format(args))
        
    def do_import(self,args):
        '''
        Import python module
        '''
        module_list=args.split()
        unload=False
        if module_list[0] == '-r':
            module_list=module_list[1:]
            unload=True
        print(module_list)
        if 'as' in module_list: # support as command
            if module_list[0][0] == '!':
                LIBS().get_lib(module_list[0][1:])
            else:
                LIBS().imports(name=module_list[-1],filename=module_list[0],global_var=globals(),unload=unload)
        else:
            if module_list[0][0] == '!':
                LIBS().get_lib(module_list[0][1:])
            else:
                LIBS().imports(filename=module_list[0],global_var=globals(),unload=unload)

    def do_module_list(self,args):
        '''
        Loaded module list
        '''
        CMD()._mod_ls()
#        for ii in [key for key in globals().keys() if isinstance(globals()[key], type(sys)) and not key.startswith('__')]:
#            try:
#                m_path=globals()[ii].__file__
#            except:
#                m_path=''
#            print('%15s : %s'%(ii,m_path))

    def do_p(self,args):
        '''
        Internal python
        '''
        if len(args) == 0:
            import code
            vars = globals()
            vars.update(locals())
            shell=code.InteractiveConsole(vars)
            shell.interact()
        else:
            try:
                exec(args)
            except:
                print('wrong python code')

    def do_s(self,args):
        '''
        Full Bash Shell
        '''
        if len(args) == 0:
            if os.path.exists(pipe_name):
                os.remove(pipe_name)
            # create symlink
            sym_linked,sym_list,dfname=LIBS().symlink()
            pipe_th=threading.Thread(target=pipe,args=(pipe_name,)) # Thread  for PIPE with Global
            pipe_th.start() # Thread start
            #pty.spawn("/bin/bash")
            os.system('[ -f /bin/bash ] && /bin/bash || /bin/sh')
            os.system('echo quit > {}'.format(pipe_name))
            for ii in range(0,100):
                if not pipe_th.isAlive(): # Not pipe then remove pipe file
                    # Clean symlink
                    LIBS().symlink(sym_linked,sym_list,dfname)
                    os.remove(pipe_name)  # remove pipe file
                    break
                time.sleep(0.2)
        else:
            try:
                print('not yet')
            except:
                print('wrong python code')

    def do_export(self,args):
        '''
        Set SHELL Environments
        '''
        env=os.environ
        nvar=args.split('=')
        tmp=None
        for i in nvar[1:]:
           if tmp is None:
               tmp='{0}'.format(i)
           else:
               tmp='{0}={1}'.format(tmp,i)
        env[nvar[0]]=tmp

    def do_lib(self,args):
        if args:
            lib_file='/tmp/{}'.format(os.path.basename(args))
            LIBS().get(args,lib_file,cmd='glib')
            vimrc='/tmp/.vim'
            vimrcfp=open(vimrc,"w")
            vimrcfp.write("set title\n")
            vimrcfp.write("set laststatus=2\n")
            vimrcfp.write("set tabstop=4\n")
            vimrcfp.write("set expandtab\n")
            vimrcfp.write("set shiftwidth=4\n")
            vimrcfp.write("set softtabstop=4\n")
            vimrcfp.write("set visualbell\n")
            vimrcfp.write("set ruler\n")
            vimrcfp.write("syntax on")
            vimrcfp.close()
            os.system('TERM=linux vim -u {} {}'.format(vimrc,lib_file))
            os.remove(vimrc)
            LIBS().push(lib_file,args,cmd='lib')
            os.remove(lib_file)

    def do_update(self,args):
        '''
        Update Software
        '''
        if args and args == 'k7' and find_executable('vim'):
            LIBS().get('k/k.py','/tmp/k.py',cmd='k7')
            vimrc='/tmp/.vim'
            vimrcfp=open(vimrc,"w")
            vimrcfp.write("set title\n")
            vimrcfp.write("set laststatus=2\n")
            vimrcfp.write("set tabstop=4\n")
            vimrcfp.write("set expandtab\n")
            vimrcfp.write("set shiftwidth=4\n")
            vimrcfp.write("set softtabstop=4\n")
            vimrcfp.write("set visualbell\n")
            vimrcfp.write("set ruler\n")
            vimrcfp.write("syntax on")
            vimrcfp.close()
            os.system('\cp -f /tmp/k.py /tmp/k.py.1')
            os.system('TERM=linux vim -u ' + vimrc + ' /tmp/k.py')
            rc=os.system('cmp /tmp/k.py /tmp/k.py.1 >& /dev/null')
            os.remove(vimrc)
            if rc != 0:
                LIBS().trash('k/k.py',cmd='dk7')
                print('Update...')
                LIBS().push('/tmp/k.py','k/k.py',cmd='sk7')

                soc=kNet.gen_client_socket(ip,port)
                print('Build...')
                kNet.put_packet(soc,['update'])
                save_file='/tmp/k.up'
                rc=kNet.get_packet(soc,progress='Downloading : k => %s'%(save_file))
                with open(save_file,'w') as f:
                    f.write(rc)
                os.chmod(save_file, stat.S_IRWXU)
                print('save at {}, please overwrite this file to original file'.format(save_file))
            os.remove('/tmp/k.py')
            os.remove('/tmp/k.py.1')
        elif args and args == 'k8':
            soc=kNet.gen_client_socket(ip,port)
            kNet.put_packet(soc,['update'])
            save_file='/tmp/k.up'
            rc=kNet.get_packet(soc,progress='Downloading : k => %s'%(save_file))
            with open(save_file,'w') as f:
                f.write(rc)
            os.chmod(save_file, stat.S_IRWXU)
            print('save at {}, please overwrite this file to original file'.format(save_file))
        
    def do_history(self,args):
        '''
        Show history commands
        '''
        for i in self.history:
            print('%5d %s'%(self.history.index(i),i))

    def emptyline(self):
        pass

    do_EOF = do_quit

    # ----- record and playback -----
#    def do_record(self, arg):
#        'Save future commands to filename:  RECORD rose.cmd'
#        self.file = open(arg, 'w')
#    def do_record_stop(self, arg):
#        self.close()
#    def do_playback(self, arg):
#        'Playback commands from a file:  PLAYBACK rose.cmd'
#        self.close()
#        with open(arg) as f:
#            self.cmdqueue.extend(f.read().splitlines())
#    def precmd(self, line):
#        line = line.lower()
#        if self.file and 'playback' not in line:
#            print(line, file=self.file)
#        return line
#    def close(self):
#        if self.file:
#            self.file.close()
#            self.file = None


############################################
# Own command line read design
#def rpm(*args):
#

def pip(*args):
    #if find_executable('pip'):
    if os.path.isfile('/usr/bin/pip') or os.path.isfile('/usr/local/bin/pip'):
        LIBS().run_shell('pip '+LIBS().list2str(args))
    elif os.path.isfile('/usr/bin/pip2') or os.path.isfile('/usr/local/bin/pip2'):
        LIBS().run_shell('pip2 '+LIBS().list2str(args))
    else:
        if hasattr(pip, 'main'):
            pip.main(list(args))
        else:
#            try:
#                pip_main(list(args))
#            except:
            print('please install pip')

def cp(*args):
    '''
    copy file
    cp <file> !<category>
    cp <file> <id>@<ip>:<directory>
    cp !<category>/<file> <dir>
    cp http://xxxx/file  <dir>
    cp <id>@<ip>:/path/file  <dir>
    '''
    whttp=re.compile('^http://')
    wftp=re.compile('^ftp://')
    dest=args[-1]
    dest_type=None
    max_num=len(args)-1
    c_num=0
    if len(args) < 2:
        print('cp <src> <dest>')
        return
    if dest[0] == '!':
         for src in args[:-1]:
             if c_num > max_num:
                break
             c_num+=1
             if not os.path.isfile(src) or src[0] == '-' or src[0] == '!' or whttp.search(src) or wftp.search(src):
                 continue
             print(LIBS().push(src,'{}/{}'.format(dest[1:],os.path.basename(src))))
    else:
        if os.path.isdir(dest):
            dest_type='dir'
        elif os.path.isdir(os.path.dirname(dest)):
            dest_type='file'
        else:
            print('cp <src> <dest>')
            return
        opt=''
        for src in args[:-1]:
            print('try {}..'.format(src))
            if c_num > max_num:
                break
            c_num+=1
            if src[0] == '-':
                opt+=' {}'.format(src)
                continue 
            if src[0] == '!': # KAGE DB
                if dest_type == 'dir':
                    print(LIBS().get(src[1:],'{}/{}'.format(dest,os.path.basename(src[1:])),progress=True))
                else:
                    print(LIBS().get(src[1:],'{}'.format(dest),progress=True))
            elif whttp.search(src) or wftp.search(src): # wget
                try:
                    print('download: {0}'.format(src))
                    r=requests.get(src,stream=True)
                except requests.exceptions.RequestException as e:
                    print('''can't download {}'''.format(src))
                pp=0
                if dest_type == 'dir':
                    filename='{}/{}'.format(dest,src.split('/')[-1])
                else:
                    filename=dest
                with open(filename,'wb') as f:
                   for chunk in r.iter_content(chunk_size=1024):
                      f.write(chunk)
                      f.flush()
                      pp=kpg.progress(pp=pp,msg=filename)
                print('save at {}'.format(filename))
            else: # Linux
                LIBS().run_shell('cp {} {} {}'.format(opt,src,dest))
#                os.system('cp {} {} {}'.format(opt,src,dest))
    print('done')

def rm(*args):
    '''
    delete file/data
    rm !<category>/<dest>
    rm <files> or <directory>
    '''
    if len(args) > 0 and args[0][0] == '!':
        print(LIBS().trash(args[0][1:]))
    elif find_executable('rm'):
        LIBS().run_shell('rm '+LIBS().list2str(args))
    else:
        sub_dir=False
        for arg in args:
            if arg[0] == '-':
               if 'r' in arg:
                  sub_dir=True
            else:
                if os.isfile(arg):
                    os.remove(arg)
                elif os.isdir(arg) and sub_dir:
                    shutil.rmtree(arg)
                else:
                    print('''can't delete directory:{}'''.format(arg))

def version(*args):
    print(_k_version)

#def view(*args):
#

def vi(*args):
    '''
    vi command
    '''
    lib_edit=False
    src_file=''
    src_dir=''
    filename=args[0]
    print('try {}...'.format(filename))
    if filename[0] == '!':
        lib_edit=True
        src_file=filename[1:]
        src_type=src_file.split('/')[0]
        filename='/tmp/{}'.format(os.path.basename(src_file))
        print(filename,src_file,src_type)
        if src_type in ['python','shell']:
            LIBS().get(src_file,filename,cmd='glib')
        else:
            LIBS().get(src_file,filename)
        LIBS().run_shell('\cp -f {} {}.diff >& /dev/null'.format(filename,filename))

    if find_executable('vim'):
        vimrc='/tmp/.vim'
        vimrcfp=open(vimrc,"w")
        vimrcfp.write("set title\n")
        vimrcfp.write("set laststatus=2\n")
        vimrcfp.write("set tabstop=4\n")
        vimrcfp.write("set expandtab\n")
        vimrcfp.write("set shiftwidth=4\n")
        vimrcfp.write("set softtabstop=4\n")
        vimrcfp.write("set visualbell\n")
        vimrcfp.write("set ruler\n")
        vimrcfp.write("syntax on")
        vimrcfp.close()
        os.system('TERM=linux vim -u ' + vimrc + ' ' + filename)
        os.remove(vimrc)
    elif find_executable('vi'):
        LIBS().run_shell('vi ' + filename)
    else: # Symple text edit code here
        print('not yet. please intall vim')
        return
    if lib_edit:
        rc=os.system('cmp {} {}.diff >& /dev/null'.format(filename,filename))
        if rc != 0:
            print(LIBS().trash(src_file))
            if src_type in ['python','shell']:
                print(LIBS().push(filename,src_file,cmd='lib'))
            else:
                print(LIBS().push(filename,src_file))
        os.remove(filename)
        os.remove('{}.diff'.format(filename))

#def _imports(*args):
#    '''
#    Import python module internal command
#    '''
#    unload=False
#    module_line=LIBS().list2str(args).split(',')
#    if '-r' in module_line[0]:
#        unload=True
#        module_line[0]=module_line[0].replace('-r','')
#    for mod in module_line:
#        mod_name=None
#        module_list=mod.split()
#        if 'as' in module_list: # support as command
#            mod_name=module_list[-1]
#        if module_list[0][0] == '!':
#            LIBS().get_lib(module_list[0][1:],name=mod_name)
#        else:
#            LIBS().imports(name=mod_name,filename=module_list[0],global_var=globals(),unload=unload)

def imports(*args):
    '''
    Import python module
    '''
    if os.path.exists(pipe_name):
        pipeout = os.open(pipe_name, os.O_WRONLY)
        os.write(os.open(pipe_name, os.O_WRONLY),'CMD._imports '+ LIBS().list2str(args)+'\n')
    else:
        CMD()._imports(*args)

def ssh(*args):
    '''
    SSH Command without hostkey check
    '''
    #if find_executable('ssh'):
    if os.path.isfile('/usr/bin/ssh'):
        ssh_opt='-o StrictHostKeychecking=no -o CheckHostIP=no -o ConnectTimeout=5 -o UserKnownHostsFile=/dev/null' 
        LIBS().run_shell('ssh {0} '.format(ssh_opt) + LIBS().list2str(args))
    else: # Simple ssh code with python only
        pass

#def _mod_ls(*args):
#    for ii in [key for key in globals().keys() if isinstance(globals()[key], type(sys)) and not key.startswith('__')]:
#        m_path=''
#        try:
#            m_path=globals()[ii].__file__
#        except:
#            pass
#        sys.stdout.write('\n%15s : %s\r'%(ii,m_path))
#        sys.stdout.flush()
#    sys.stdout.write('\n')
#    sys.stdout.flush()
    

def ls(*args):
    '''show file list, db list, module list'''
    krun=False
    args_list=list(args)
    del_list=[]
    if len(args) > 0:
        for arg in args_list:
            if arg[0] == '!':
                krun=True
                soc=kNet.gen_client_socket(ip,port)
                kNet.put_packet(soc,['ls','{0}'.format(arg[1:])])
                rc=kNet.get_packet(soc)
                for ii in rc:
                    print("""%s"""%(ii)) # recover data to readable data
            elif arg == '[module]':
                krun=True
                if os.path.exists(pipe_name):
                    pipeout = os.open(pipe_name, os.O_WRONLY)
                    os.write(os.open(pipe_name, os.O_WRONLY),'CMD._mod_ls\n')
                else:
                    #print(_mod_ls())
                    print(CMD()._mod_ls())
    if not krun:
        LIBS().run_shell('ls ' + LIBS().list2str(args_list))

def sort(*argv):
    _help='''
sort for sorting string lines or sorting string lines in file
-i : sorting filed id (start from 0), only one
-s : field spliting symbol (default: white space) (ex: " " for only one space, ":" for :)
-p : print field id(start from 0) (ex: 1,3,...), it should need -i
-n : new line symbol (default: Linux (\\n))
-r : reverse sort
-m : numerical sort
-f : Input text file

example)
sorting string:
./sort "1
6
9
3
2
7"

reverse sorting string:
./sort -r "1
6
9
3
2
7"

sorting file size :
./sort -i 4 -s space "$(ls -l)"

sorting file size reverse:
./sort -i 4 -s space -r "$(ls -l)"

sorting file size, and print size and filename:
./sort -i 4 -s space -p 8,4 "$(ls -l)"
          '''
    strings=None
    if select.select([sys.stdin,],[],[],0.0)[0]:
        for line in sys.stdin:
            if strings is None:
                strings='{}'.format(line)
            else:
                strings='{}{}'.format(strings,line)
    else:
        if len(sys.argv) == 0 or '--help' in sys.argv:
            print(_help)
            sys.exit(0)

    new_line='\n'
    if '-n' in sys.argv:
        new_line=sys.argv.pop(sys.argv.index('-n')+1)
        sys.argv.remove('-n')
    field_id=None
    if '-i' in sys.argv:
        field_id=int(sys.argv.pop(sys.argv.index('-i')+1))
        sys.argv.remove('-i')
    field_symbol='space'
    if '-s' in sys.argv:
        field_symbol=sys.argv.pop(sys.argv.index('-s')+1)
        sys.argv.remove('-s')
    print_field=None
    if '-p' in sys.argv:
        print_field=int(sys.argv.pop(sys.argv.index('-p')+1))
        sys.argv.remove('-p')
    filename=None
    if '-f' in sys.argv:
        filename=sys.argv.pop(sys.argv.index('-f')+1)
        sys.argv.remove('-f')
    reverse=False
    if '-r' in sys.argv:
        sys.argv.remove('-r')
        reverse=True
    num=False
    if '-m' in sys.argv:
        sys.argv.remove('-m')
        num=True
    if filename and os.path.isfile(filename): # read file
        with open(filename,'r') as f:
            file_data=f.read()
        if strings:
            strings='{}{}{}'.format(strings,new_line,file_data)
        else:
            strings=file_data
    if len(sys.argv):
        if strings:
            strings='''{}{}{}'''.format(strings,new_line,sys.argv[0])
        else:
            strings=sys.argv[0]
    if strings:
        for ii in CMD().ksort(string=strings,field_symbol=field_symbol,field_id=field_id,reverse=reverse,print_field=print_field,new_line=new_line,num=num):
           print(ii)

def test(*argv):
    print('++test():',argv)

if __name__ == '__main__':
    ip="kage.cep.kr"
    port=7992
    username=getpass.getuser()
    hostname=socket.gethostname()
    cmd_name='{}'.format(sys.argv[0].split('/')[-1])
    cmd_module=[this_mod_name]
    # 1st self command
    if cmd_name == 'k' and len(sys.argv) == 1: # K Shell
        soc=kNet.gen_client_socket(ip,port)
        kNet.put_packet(soc,['ls_cmd'])
        cmd_dict=kNet.get_packet(soc)
        kd.save_db(tmp_db,cmd_dict)
        prompt = kPrompt()
        prompt_str='{0}@{1}'.format(username,hostname)
        prompt.prompt = '[{0} {1}]$ '.format(prompt_str,os.path.basename(os.getcwd())) # it works instad of __init__()
        prompt.cmdloop_with_mykey(intro='-K-')
        #prompt.cmdloop(intro='-K-')
    else:
        if cmd_name == 'k': # Instance K Command
            sys.argv.pop(0)
            cmd_name=sys.argv[0]
            self_k=True
        # 2nd python (self, imported) command
        for mod in cmd_module:
            if cmd_name in LIBS().get_function_list_in_module_name(mod):
                sys.argv.pop(0)
                rc=LIBS().run_function_name_argv(LIBS().get_module_obj(mod),cmd_name,*tuple(sys.argv))
                if type(rc) == set:
                    LIBS().exit(code=rc)
                else:
                    if rc:
                         print(rd)
                sys.exit()
        if self_k:
            soc=kNet.gen_client_socket(ip,port)
            kNet.put_packet(soc,['ls_cmd'])
            cmd_dict=kNet.get_packet(soc)
        else:
            cmd_dict=kd.open_db(tmp_db)
        # 3rd, code command (shell,python)
        if cmd_dict:
            for tt in ['python','shell']:
                if cmd_name in cmd_dict[tt]:
                    if not cmd_dict[tt][cmd_name]:
                        #Get cmd code
                        soc=kNet.gen_client_socket(ip,port)
                        kNet.put_packet(soc,['get_cmd','{}/{}'.format(tt,cmd_name)])
                        cmd_dict[tt][cmd_name]=kNet.get_packet(soc)
                        if not self_k:
                            kd.save_db(tmp_db,cmd_dict)
                    if tt == 'shell':
                        # run shell code in console
                        LIBS().run_shell('\n_k_{} '.format(cmd_name) + LIBS().list2str(sys.argv[1:]),pre_code=cmd_dict[tt][cmd_name])
                    elif tt == 'python':
                         # run python code to function
                        exec(cmd_dict[tt][cmd_name])
#                        eval(cmd_dict[tt][cmd_name],globals(),locals())
                    sys.exit()

        # 4th Linux OS commane
        os.system('PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin; SHELL=$([ -f /bin/bash ] && echo /bin/bash || /bin/sh); ' + LIBS().list2str(sys.argv))

