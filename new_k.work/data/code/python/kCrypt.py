# Kage
# pip install lz4
import lz4.frame
import base64
# Encrypt / Decrypt function
# pip install pycrypto
from Crypto.Cipher import AES
counter="cEPk"*4
key="KeCP" * 8
compress=False
#counter = "H"*16
#counter=os.random(16)
#key = "H"*32
def encrypt(data,lkey=None):
    if lkey is None:
       lkey='{0}'.format(key)
    encrypto = AES.new(lkey, AES.MODE_CTR, counter=lambda: counter)
    if compress:
        return base64.b64encode(lz4.frame.compress(encrypto.encrypt(data)))
    else:
        return encrypto.encrypt(data)

def decrypt(data,lkey=None):
    if lkey is None:
       lkey='{0}'.format(key)
    decrypto = AES.new(lkey, AES.MODE_CTR, counter=lambda: counter)
    if compress:
        return decrypto.decrypt(lz4.frame.decompress(base64.b64decode(data)))
    else:
        return decrypto.decrypt(data)

