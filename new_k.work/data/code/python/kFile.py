import os
import kRandom

def list_in_dir(dir_name):
    if dir_name:
        if os.path.isdir(dir_name):
            return os.listdir(dir_name)

def mkdir(dest,depth=15,cur_dir=None,force=False):
    if cur_dir is None:
        cur_dir=os.getcwd()
    dir_a=dest.split('/')
    if not dir_a[0]:
        cur_dir='/'
        del dir_a[0]
    chk_depth=0
    for ii in dir_a:
        cur_dir+='/{}'.format(ii)
        if not os.path.isdir(cur_dir):
             if force and chk_depth < depth:
                 chk_depth+=1
                 os.mkdir(cur_dir)
             else:
                 return False
     
def mktemp(ddir='/tmp',filename=None,ext=None,length=8,force=False,timeout=30,dryrun=False):
    # directory
    if not dryrun and ddir:
        if not os.path.isdir(ddir):
            if force:
                mkdir(ddir,force=True)
            else:
                return False
    # File name
    if not filename:
        filename='{}'.format(kRandom.random_string(length=length))
    if ext:
        filename+='.{}'.format(ext)
    # Check and re-gen file
    if dryrun:
        if ddir:
            return '{}/{}'.format(ddir,filename)
        else:
            return filename
    else:
        if not ddir:
            ddir='.'
        else:
            chk_file='{}/{}'.format(ddir,filename)
            for i in range(0,timeout):
                if not os.path.isfile(chk_file):
                    return chk_file
                chk_file='{}/{}.{}'.format(ddir,filename,kRandom.random_string(length=length))
