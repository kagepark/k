def line_dbg_bak(line):
    before_end=''
    before_first=''
    line_end=''
    line_first=''
    if len(line):
        line_arr=line.split('\n')
        current_line=line_arr[-1]
        current_line_a=current_line.split()
        if len(current_line_a):
            line_first=current_line_a[0]
            if len(current_line_a) > 1:
                line_end=current_line_a[-1]
        else:
            line_first='\n'
            line_end='\n'
        if len(line_arr) >1:
            before_line=line_arr[-2]
            before_line_a=before_line.split()
            if len(before_line_a):
                before_first=before_line_a[0]
                if len(before_line_a) > 1:
                    before_end=before_line_a[-1]
            else:
                before_first='\n'
                before_end='\n'
    return before_first,before_end,line_first,line_end

def line_dbg(line,h=1):
    before_end=''
    before_first=''
    line_end=''
    line_first=''
    after_end=''
    after_first=''
    if line:
        line_arr=line.split('\n')
        current_line_a=line_arr[-1*h].split()
        if current_line_a:
            if len(current_line_a) == 1:
                line_first=''
                line_end=current_line_a[-1]
            else:
                line_first=current_line_a[0]
                line_end=current_line_a[-1]
        else:
            line_first=''
            line_end='\n'
        if len(line_arr) >h: # before(up) line from current line
            before_line=line_arr[-1*(h+1)]
            before_line_a=before_line.split()
            if before_line_a:
                if len(before_line_a)  == 1:
                    before_first=''
                    before_end=before_line_a[-1]
                else:
                    before_first=before_line_a[0]
                    before_end=before_line_a[-1]
            else:
                before_first=''
                before_end='\n'
        if h > 1: # after(down) line from current line
            after_line=line_arr[-1*(h-1)]
            after_line_a=before_line.split()
            if before_line_a:
                if len(before_line_a) == 1:
                    after_first=''
                    after_end=before_line_a[-1]
                else:
                    after_first=before_line_a[0]
                    after_end=before_line_a[-1]
            else:
                after_first=''
                after_end='\n'
    return before_first,before_end,line_first,line_end,after_first,after_end

def completion(cmd_list,find_word=None,):
    '''Auto find and fill to compiletion word'''
    if cmd_list:
        if find_word:
            return [ f for f in cmd_list if f.startswith(find_word) ]
        else:
            return cmd_list[:]
    return []
