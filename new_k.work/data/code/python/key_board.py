from __future__ import print_function
import sys, termios, tty, os, time
import fcntl
 
def getch():
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
 
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch
 
key_map2={'279168':'left',
  '279167':'right',
  '279165':'up',
  '279166':'down',
  '279151126':'del',
  '277970':'end',
  '277972':'home',
  '279153126':'pageup',
  '279154126':'pagedown',
  '3':'control_c',
  '13':'enter',
  '127':'backspace',
  '9':'tab'}
key_map={'enter':'13',
 'backspace':'127',
 'del':'279151126',
 'tab':'9'}

def get_char():
    key=''
    char=''
    while True:
        char = getch()
        key+='{0}'.format(ord(char))
        print('KEY({0})'.format(key))
        if key in key_map:
          return key_map[key]
        else:
          return char

button_delay = 0.2

#fd = sys.stdin.fileno()
#fl = fcntl.fcntl(fd, fcntl.F_GETFL)
#fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
 
line=''
key=''
while True:
    char = getch()
    key+='{0}'.format(ord(char))
    if (char == "p"):
        print("Stop!")
        exit(0)
    if key in key_map['enter']:
        print('\nENT({0})\n'.format(line))
        line=''
        key=''
    elif key in key_map['backspace']:
        sys.stdout.write('\b \b')
        line=line[:len(line)-1]
        key=''
    elif key in key_map['del']:
        sys.stdout.write('\b \b')
        line=line[:len(line)-1]
        key=''
    elif key in key_map['tab']:
        print('tab')
        key=''
    else:
        print(char,end='')
        line+=char
 

def bak():
  while True:
    char = getch()
    if (char == "p"):
        print("Stop!")
        exit(0)
    if (char == "a"):
        print("Left pressed")
        time.sleep(button_delay)
 
    elif (char == "d"):
        print("Right pressed")
        time.sleep(button_delay)
 
    elif (char == "w"):
        print("Up pressed")
        time.sleep(button_delay)
 
    elif (char == "s"):
        print("Down pressed")
        time.sleep(button_delay)
 
    elif (char == "1"):
        print("Number 1 pressed")
        time.sleep(button_delay)
    elif ord(char) == 13: # enter
        print('enter',chr(ord(char)))
    elif ord(char) == 127: # backspace
        print('backspace')
    else:
        print('!!!')
        print('ord:{0}'.format(ord(char)))
        print('chr:',chr(ord(char)))
        type(chr(ord(char)))


