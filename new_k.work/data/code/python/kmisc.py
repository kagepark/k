#!/bin/python
# -*- coding: utf-8 -*-
# Kage personal stuff
#
from __future__ import print_function
import sys,os,re,subprocess,traceback,copy
import tarfile
import tempfile
import inspect
from datetime import datetime
from os import close, remove
import random
import socket, struct

ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
url_group = re.compile('^(https|http|ftp)://([^/\r\n]+)(/[^\r\n]*)?')
log_file=None
log_intro=3
log_new_line='\n'

def error_exit(msg=None):
    if msg is not None:
       print(msg)
    sys.exit(-1)

def get_caller_fcuntion_name(detail=False):
    try:
        dep=len(inspect.stack())-2
        if detail:
            return sys._getframe(dep).f_code.co_name,sys._getframe(dep).f_lineno,sys._getframe(dep).f_code.co_filename
        else:
            name=sys._getframe(dep).f_code.co_name
            if name == '_bootstrap_inner' or name == '_run_code':
                return sys._getframe(3).f_code.co_name
            return name
    except:
        return False

def timechk(time='0',wait='0',tformat=None):
    if tformat is None:
        now=int(datetime.now().strftime('%s'))
    else:
        if time == '0':
            now=datetime.now().strftime(tformat)
        else:
            now=int(datetime.strptime(str(time),tformat).strftime('%s'))

    if tformat is None and str(time).isdigit() and int(time) > 0 and int(wait) > 0:
        if now - int(time) > int(wait):
            return False
        else:
            return True
    else:
        return now

def log_format(*msg):
    if len(msg) > 0:
        m_str=None
        intro=''
        if log_intro > 0:
            intro='[{0}]'.format(timechk(tformat='%m/%d/%Y %H:%M:%S'))
        if log_intro > 1:
            intro=intro+' {0}()'.format(get_caller_fcuntion_name())
        if intro == '':
           for m in list(msg):
               if m_str is None:
                   m_str=m
               else:
                   m_str='{0} {1}'.format(m_str,m)
        else:
           intro_space=''
           for i in range(0,len(intro)):
               intro_space=intro_space+' '
           for m in list(msg):
               if m_str is None:
                   m_str='{0} {1}'.format(intro,m)
               else:
                   m_str='{0}\n{1} {2}'.format(m_str,intro_space,m)
        return m_str

def logging(*msg):
    if log_new_line == '':
        log_intro=0
    else:
        log_intro=1
    m_str=log_format(*msg)
    def log2file(msg_str,log_file):
        dirname=os.path.dirname(log_file)
        if os.path.isdir(dirname):
            with open(log_file,'a+') as f:
                #f.write(msg_str+'\n')
                f.write(msg_str+log_new_line)
    if m_str is not None:
        if type(log_file) is str:
           log2file(m_str,log_file)
        elif type(log_file) is list:
           for logf in list(log_file):
               log2file(m_str,logf)
        else:
           print(m_str)

def dget(dict=None,keys=None):
    if dict is None or keys is None:
        return False
    tmp=dict.copy()
    for ii in keys.split('/'):
        if ii in tmp:
           dtmp=tmp[ii]
        else:
           return False
        tmp=dtmp
    return tmp

def dput(dic=None,keys=None,val=None,force=False,safe=True):
    if dic is not None and keys:
        tmp=dic
        keys_arr=keys.split('/')
        keys_num=len(keys_arr)
        for ii in keys_arr[:(keys_num-1)]:
            if ii in tmp:
                if type(tmp[ii]) == type({}):
                    dtmp=tmp[ii]
                else:
                    if tmp[ii] == None:
                        tmp[ii]={}
                        dtmp=tmp[ii]
                    else:
                        if force:
                            vtmp=tmp[ii]
                            tmp[ii]={vtmp:None}
                            dtmp=tmp[ii]
                        else:
                            return False
            else:
                if force:
                    tmp[ii]={}
                    dtmp=tmp[ii]
                else:
                    return False
            tmp=dtmp
        if val == '_blank_':
            val={}
        if keys_arr[keys_num-1] in tmp.keys():
            if safe:
                if tmp[keys_arr[keys_num-1]]:
                    return False
            tmp.update({keys_arr[keys_num-1]:val})
            return True
        else:
            if force:
                tmp.update({keys_arr[keys_num-1]:val})
                return True
    return False

def is_py3():
    if sys.version_info[0] >= 3:
        return True
    return False


def rshell(cmd,timeout=None,ansi=True):
    if type(cmd) is not str:
        return 1,'wrong command information :{0}'.format(cmd),''
    Popen=subprocess.Popen
    PIPE=subprocess.PIPE
    start_time=datetime.now().strftime('%s')

    p = Popen(cmd , shell=True, stdout=PIPE, stderr=PIPE)
    out=None
    err=None
    try:
        if timeout is None:
            out, err = p.communicate()
        else:
            out, err = p.communicate(timeout=timeout)
        if is_py3():
            if ansi:
                return p.returncode, out.decode("ISO-8859-1").rstrip(), err.decode("ISO-8859-1"),start_time,datetime.now().strftime('%s')
            else:
                return p.returncode, ansi_escape.sub('',out).decode("ISO-8859-1").rstrip(), ansi_escape.sub('',err).decode("ISO-8859-1"),start_time,datetime.now().strftime('%s')
        else:
            if ansi:
                return p.returncode, out.rstrip(), err,start_time,datetime.now().strftime('%s')
            else:
                return p.returncode, ansi_escape.sub('',out).rstrip(), ansi_escape.sub('',err),start_time,datetime.now().strftime('%s')
    except subprocess.TimeoutExpired:
        p.kill()
        return p.returncode, 'Kill process after timeout ({0} sec)'.format(timeout), 'Error: Kill process after Timeout {0}'.format(timeout),start_time,datetime.now().strftime('%s')


def sendanmail(to,subj,msg,html=True):
    if html:
        email_msg='''To: {0}
Subject: {1}
Content-Type: text/html
<html>
<body>
<pre>
{2}
</pre>
</body>
</html>'''.format(to,subj,msg)
    else:
        email_msg=''
    cmd='''echo "{0}" | sendmail -t'''.format(email_msg)
    return rshell(cmd)

def is_mac4(mac=None,symbol=':'):
    if mac is None or type(mac) is not str:
        return False
    octets = mac.split(symbol)
    if len(octets) != 6:
        return False
    for i in octets:
        try:
           if len(i) != 2 or int(i, 16) > 255:
               return False
        except:
           return False
    return True


def is_ipv4(ipadd=None):
    if ipadd is None or type(ipadd) is not str or len(ipadd) == 0:
        return False
    ipa = ipadd.split(".")
    if len(ipa) != 4:
        return False
    for ip in ipa:
        if not ip.isdigit():
            return False
        if not 0 <= int(ip) <= 255:
            return False
    return True

def ip2num(ip):
    if is_ipv4(ip):
        return struct.unpack("!L", socket.inet_aton(ip))[0]
    return False

def ip_in_range(ip,start,end):
    if type(ip) is str and type(start) is str and type(end) is str:
        ip=ip2num(ip)
        start=ip2num(start)
        end=ip2num(end)
        if start <= ip and ip <= end:
            return True
    return False
def get_function_name():
    return traceback.extract_stack(None, 2)[0][2]

def ipmi_cmd(cmd,ipmi_ip=None,ipmi_user='ADMIN',ipmi_pass='ADMIN'):
    if ipmi_ip is None:
        ipmi_str=""" ipmitool {0} 2>/dev/null""".format(cmd)
    else:
        ipmi_str=""" ipmitool -I lanplus -H {0} -U {1} -P {2} {3} 2>/dev/null""".format(ipmi_ip,ipmi_user,ipmi_pass,cmd)
    return rshell(ipmi_str)

def get_ipmi_mac(ipmi_ip=None,ipmi_user='ADMIN',ipmi_pass='ADMIN'):
    ipmi_mac_str=None
    if ipmi_ip is None:
        ipmi_mac_str=""" ipmitool lan print 2>/dev/null | grep "MAC Address" | awk """.format(ipmi_ip,ipmi_user,ipmi_pass)
    elif is_ipv4(ipmi_ip):
        ipmi_mac_str=""" ipmitool -I lanplus -H {0} -U {1} -P {2} lan print 2>/dev/null | grep "MAC Address" | awk """.format(ipmi_ip,ipmi_user,ipmi_pass)
    if ipmi_mac_str is not None:
        ipmi_mac_str=ipmi_mac_str + """ '{print $4}' """
        return rshell(ipmi_mac_str)


def rm_file(filelist):
    if type(filelist) == type([]):
       filelist_tmp=filelist
    else:
       filelist_tmp=filelist.split(',')
    for ii in list(filelist_tmp):
        if os.path.isfile(ii):
            os.unlink(ii)
        else:
            print('not found {0}'.format(ii))

def make_tar(filename,filelist,ctype='gz',ignore_file=None):
    if ctype == 'bz2':
        tar = tarfile.open(filename,"w:bz2")
    elif ctype == 'stream':
        tar = tarfile.open(filename,"w:")
    else:
        tar = tarfile.open(filename,"w:gz")
    if type(filelist) == type([]):
       filelist_tmp=filelist
    else:
       filelist_tmp=filelist.split(',')
    for ii in list(filelist_tmp):
        if os.path.isfile(ii):
            if ignore_file is not None:
                if ignore_file == os.path.basename(ii):
                    continue
            tar.add(ii)
        elif os.path.isdir(ii):
            for r,d,f in os.walk(ii):
                for ff in f:
                    if ignore_file is not None:
                        if ignore_file == ff:
                            continue
                    tar.add(os.path.join(r,ff))
        else:
            print('not found {0}'.format(ii))
    tar.close()


def cut_string(string,len1=None,len2=None):
    if type(string) != str:
       string='{0}'.format(string)
    str_len=len(string)
    
    if len1 is None or len1 >= str_len:
       return [string]
       
    if len2 is None:
        rc=[string[i:i + len1] for i in range(0, str_len, len1)]
        return rc
    rc=[]
    rc.append(string[0:len1])
    string_tmp=string[len1:]
    string_tmp_len=len(string_tmp)
    for i in range(0, int(string_tmp_len/len2)+1):
        if (i+1)*len2 > string_tmp_len:
           rc.append(string_tmp[len2*i:])
        else:
           rc.append(string_tmp[len2*i:(i+1)*len2])
    return rc

def is_tempfile(filepath,tmp_dir='/tmp'):
   filepath_arr=filepath.split('/')
   if len(filepath_arr) == 1:
      return False
   tmp_dir_arr=tmp_dir.split('/')
   
   for ii in range(0,len(tmp_dir_arr)):
      if filepath_arr[ii] != tmp_dir_arr[ii]:
          return False
   return True


def mktemp(prefix='tmp-',suffix=None,opt='dry'):
   dir=os.path.dirname(prefix)
   if dir == '.':
       dir=os.path.realpath(__file__)
   elif dir == '':
       dir='/tmp'

   pfilename, file_ext = os.path.splitext(prefix)
   filename=os.path.basename(pfilename)
   if suffix is not None:
       if len(file_ext) == 0:
          file_ext='.{0}'.format(suffix)

   dest_file='{0}/{1}{2}'.format(dir,filename,file_ext)
   if opt == 'file':
      if os.path.exists(dest_file):
          return tempfile.TemporaryFile(prefix='{0}-'.format(filename),suffix=file_ext,dir=dir)
      else:
          os.mknod(dest_file)
          return dest_file
   elif opt == 'dir':
      if os.path.exists(dest_file):
          return tempfile.TemporaryDirectory(suffix='{0}-'.format(filename),prefix=prefix,dir=dir)
      else:
          os.mkdir(dest_file)
          return dest_file
   else:
      if os.path.exists(dest_file):
         return tempfile.mktemp(prefix='{0}-'.format(filename),suffix=file_ext,dir=dir)
      else:
         return dest_file

def append2list(src,*add):
   if type(src) == str:
      org=[]
      for jj in src.split(','):
         org.append(jj)
   elif type(src) == list:
      org=copy.deepcopy(src)
   elif type(src) == int or type(src) == float:
      org=[]
      org.append(src)
   else:   
      return False

   if len(add) < 0:
       return False
   for ii in list(add):
      if type(ii) == type([]):
         for jj in list(ii):
            org.append(jj)
      else:
         for jj in ii.split(','):
            org.append(jj)
   return org


def isfile(filename=None):
   if filename is None:
      return False
   if len(filename) == 0:
      return False
   if os.path.isfile(filename):
      return True
   return False


def ping(host,test_num=3,retry=1):
    for i in range(0,retry):
       rc=rshell("ping -c {0} {1}".format(test_num,host))
       if rc[0] == 0:
          return True
    return False


def get_function_args(func):
    args, varargs, keywords, defaults = inspect.getargspec(func)
    if defaults is not None:
        return dict(zip(args[-len(defaults):], defaults))

def get_function_list(objName=None,obj=None):
    aa={}
    if obj is None and objName is not None:
       obj=sys.modules[objName]
    if obj is not None:
        for name,fobj in inspect.getmembers(obj):
            if inspect.isfunction(fobj): # inspect.ismodule(obj) check the obj is module or not
                aa.update({name:fobj})
    return aa

def space(space_num=0,_space_='   '):
    space_str=''
    for ii in range(space_num):
        space_str='{0}{1}'.format(space_str,_space_)
    return space_str

def tap_print(string,bspace='',rc=False,NFLT=False):
    rc_str=None
    if type(string) is str:
        for ii in string.split('\n'):
            if NFLT:
               line='%s'%(ii)
               NFLT=False
            else:
               line='%s%s'%(bspace,ii)
            if rc_str is None:
               rc_str='%s'%(line)
            else:
               rc_str='%s\n%s'%(rc_str,line)
    else:
        rc_str='%s%s'%(bspace,string)

    if rc:
        return rc_str
    else:
        print(rc_str)

def str_format_print(string,rc=False):
    if type(string) is str:
        if len(string.split("'")) > 1:
            rc_str='"%s"'%(string)
        else:
            rc_str="'%s'"%(string)
    else:
        rc_str=string
    if rc:
        return rc_str
    else:
        print(rc_str)

def format_print(string,rc=False,num=0,bstr=None,NFLT=False):
    string_type=type(string)
    rc_str=''
    chk=None
    bspace=space(num)

    # Start Symbol
    if string_type is tuple:
        if bstr is None:
            if NFLT:
                rc_str='%s('%(rc_str)
            else:
                rc_str='%s%s('%(bspace,rc_str)
        else:
            rc_str='%s,\n%s%s('%(bstr,bspace,rc_str)
    elif string_type is list:
        if bstr is None:
            if NFLT:
                rc_str='%s['%(rc_str)
            else:
                rc_str='%s%s['%(bspace,rc_str)
        else:
            rc_str='%s,\n%s%s['%(bstr,bspace,rc_str)
    elif string_type is dict:
        if bstr is None:
            rc_str='%s{'%(rc_str)
        else:
            rc_str='%s,\n%s %s{'%(bstr,bspace,rc_str)
    rc_str='%s\n%s '%(rc_str,bspace)

    # Print string
    if string_type is list or string_type is tuple:
       for ii in list(string):
           ii_type=type(ii)
           if ii_type is tuple or ii_type is list or ii_type is dict:
               if not ii_type is dict:
                  num=num+1
               rc_str=format_print(ii,num=num,bstr=rc_str,rc=True)
           else:
               if chk == None:
                  rc_str='%s%s'%(rc_str,tap_print(str_format_print(ii,rc=True),rc=True))
                  chk='a'
               else:
                  rc_str='%s,\n%s'%(rc_str,tap_print(str_format_print(ii,rc=True),bspace=bspace+' ',rc=True))
    elif string_type is dict:
       for ii in string.keys():
           ii_type=type(string[ii])
           if ii_type is dict or ii_type is tuple or ii_type is list:
               num=num+1
               if ii_type is dict:
                   tmp=format_print(string[ii],num=num,rc=True)
               else:
                   tmp=format_print(string[ii],num=num,rc=True,NFLT=True)
               rc_str="%s,\n%s %s:%s"%(rc_str,bspace,str_format_print(ii,rc=True),tmp)
           else:
               if chk == None:
                  rc_str='%s%s'%(rc_str,tap_print("{0}:{1}".format(str_format_print(ii,rc=True),str_format_print(string[ii],rc=True)),rc=True))
                  chk='a'
               else:
                  rc_str='%s,\n%s'%(rc_str,tap_print("{0}:{1}".format(str_format_print(ii,rc=True),str_format_print(string[ii],rc=True)),bspace=bspace+' ',rc=True))

    # End symbol
    if string_type is tuple:
        rc_str='%s\n%s)'%(rc_str,bspace)
    elif string_type is list:
        rc_str='%s\n%s]'%(rc_str,bspace)
    elif string_type is dict:
        if bstr is None:
            rc_str='%s\n%s}'%(rc_str,bspace)
        else:
            rc_str='%s\n%s }'%(rc_str,bspace)

    else:
       rc_str=string

    # Output
    if rc:
       return rc_str    
    else:
       print(rc_str)


def str2url(string):
    return string.replace('/','%2F').replace(':','%3A').replace('=','%3D').replace(' ','+')

def clear_version(string):
    arr=string.split('.')
    if arr[-1] == '00' or arr[-1] == '0':
        arr.pop(-1)
#    for i in range(0,len(arr)):
#        if int(arr[-1]) == 0:
#            arr.pop(-1)
    new_ver=''
    for i in arr:
        if len(new_ver) > 0:
            new_ver='{0}.{1}'.format(new_ver,i)
        else:
            new_ver='{0}'.format(i)
    return new_ver 
    

def find_key_from_value(dic=None,find=None):
    if type(dic) is dict and find is not None:
        for key,val in dic.items():
            if val == find:
                return key

def random_str(length=8,strs='0aA-1b+2Bc=C3d_D,4.eE?5"fF6g7G!h8H@i9#Ij$JkK%lLmMn^N&oO*p(Pq)Q/r\Rs:St;TuUv{V<wW}x[Xy>Y]z|Z'):
    new=''
    strn=len(strs)-1
    for i in range(0,length):
        new='{0}{1}'.format(new,strs[random.randint(0,strn)])
    return new

def power_handle(ipmi_ip,mode='reset',num=None,interval=None,ipmi_user='ADMIN',ipmi_pass='ADMIN',force=False,eth_ip=None,boot_mode=None,order=False,wait_ready=False):
    rc=True
    if num is None:
        num=2
    if interval is None:
        interval=10

    power_mode={'on':['chassis power on'],'off':['chassis power off'],'reset':['chassis power reset'],'off_on':['chassis power off','chassis power on']}
    if not mode in power_mode:
        return False
    verify_status=''
    sys_status='-'
    for ii in range(0,int(num)):
        rc=True
        test=1
        for rr in list(power_mode[mode]):
            verify_status=rr.split(' ')[2]
            if sys_status == verify_status and len(power_mode[mode]) == test:
                if force:
                    logging("wait {0} seconds after force {1} at {2}".format(interval,rr,ipmi_ip))
                    time.sleep(int(interval))
                else:
                    logging("Command and system status are same. So do not run the command to {0}".format(ipmi_ip))
                    return rc
            else:
                if boot_mode is not False and boot_mode is not None and boot_mode != 'none' and (verify_status == 'on' or verify_status == 'reset'):
                    time.sleep(2)
                    if order:
                        km.ipmi_cmd(cmd='chassis bootdev {0} options=persistent'.format(boot_mode),ipmi_ip=ipmi_ip,ipmi_user=ipmi_user,ipmi_pass=ipmi_pass)
                        logging("Persistent Boot mode set to {0} at {1}".format(boot_mode,ipmi_ip))
                    else:
                        km.ipmi_cmd(cmd='chassis bootdev {0}'.format(boot_mode),ipmi_ip=ipmi_ip,ipmi_user=ipmi_user,ipmi_pass=ipmi_pass)
                        logging("Temporary Boot mode set to {0} at {1}".format(boot_mode,ipmi_ip))
                    time.sleep(5)
                sys_power=km.ipmi_cmd(cmd=rr,ipmi_ip=ipmi_ip,ipmi_user=ipmi_user,ipmi_pass=ipmi_pass)
                logging("{0} to {1} (rc:{2})".format(rr,ipmi_ip,sys_power))
                if sys_power[0] == 0:
                    test=test+1
                    if eth_ip is not None:
                        if verify_status == 'off':
                            for pp in range(0,120):
                                if ping(eth_ip,2) is False:
                                    break
                                logging("ping for off {0} at {1}".format(pp,eth_ip))
                                time.sleep(2)
                        else: # Power on
                            for pp in range(0,600):
                                if ping(eth_ip,2):
                                    break
                                logging("ping for on {0} at {1}".format(pp,eth_ip))
                                time.sleep(2)
                    else:
                        logging("wait 20 seconds for next command at {0}".format(ipmi_ip))
                        time.sleep(20)
                else:
                    rc=False
                    logging("wait {0} seconds for failed power controll at {1}".format(interval,ipmi_ip))
                    time.sleep(int(interval))
                    continue
        last_mode=mode
        sys_status='unknown'
        for ll in mode.split('_'):
            last_mode=ll
        for pp in range(0,30):
            power_status=km.ipmi_cmd(cmd='chassis power status',ipmi_ip=ipmi_ip,ipmi_user=ipmi_user,ipmi_pass=ipmi_pass)
            sys_status='unknown'
            if power_status[0] == 0:
                if power_status[1] == 'Chassis Power is on':
                    sys_status='on'
                elif power_status[1] == 'Chassis Power is off':
                    sys_status='off'
            if sys_status == last_mode:
               break
            time.sleep(5)

        if sys_status == last_mode:
            rc=True
            break
        else:
            time.sleep(20)

    if wait_ready:
        wait_ready_system(ipmi_ip,ipmi_user,ipmi_pass)
    if rc:
        return {'rc':rc,'try':(ii+1),'state':'{0}'.format(sys_status)}
    else:
        return False

def wait_ready_system(ipmi_ip,ipmi_user='ADMIN',ipmi_pass='ADMIN',wait_sec=1200):
    aa="""ipmitool -I lanplus -H {0} -U {1} -P {2} sdr type Temperature 2>/dev/null | grep -e "CPU" -e "System" | grep "Temp" | head -n1 | awk """.format(ipmi_ip,ipmi_user,ipmi_pass)
    aa=aa + """ '{print $10}' | grep "[0-9]" """
    time.sleep(15)
    chk=0
    init_sec=datetime.datetime.now().strftime('%s')
    while True:
        if do_sec - init_sec > wait_sec:
            return False
        if ping(ipmi_ip,2):
            wrc=km.rshell('''{0} '''.format(aa))
            if wrc[0] == 0:
                 if len(wrc[1]) > 1:
                     break
            if chk % 5 == 0:
                logging("wait to read system for {0} ({1})".format(ipmi_ip, wrc))
        else:
            if chk % 5 == 0:
                logging("can't ping to {0}".format(ipmi_ip))
        chk=chk+1
        time.sleep(15)
        do_sec=datetime.datetime.now().strftime('%s')
    time.sleep(20)
    return True

def get_lanmode(smc_ipmi_file,smc_ipmi_opt):
    if smc_ipmi_file is not None and smc_ipmi_opt is not None:
        lanmode_info=km.rshell('''java -jar {0}/{1} {2}'''.format(tool_path,os.path.basename(smc_ipmi_file),smc_ipmi_opt))
        if lanmode_info[0] == 144:
            a=re.compile('Current LAN interface is \[ (\w.*) \]').findall(lanmode_info[1])
            if len(a) == 1:
                return a[0]
    return

def sizeConvert(sz=None,unit='b:g'):
    if sz is None:
        return False
    unit_a=unit.lower().split(':')
    if len(unit_a) != 2:
        return False
    def inc(sz):
        return '%.1f'%(float(sz) / 1024)
    def dec(sz):
        return int(sz) * 1024
    sunit=unit_a[0]
    eunit=unit_a[1]
    unit_m=['b','k','m','g','t','p']
    si=unit_m.index(sunit)
    ei=unit_m.index(eunit)
    h=ei-si
    for i in range(0,abs(h)):
        if h > 0:
            sz=inc(sz)
        else:
            sz=dec(sz)
    return sz

def git_ver(git_dir=None):
    if git_dir is not None and os.path.isdir('{0}/.git'.format(git_dir)):
        gver=rshell('''cd {0} && git describe'''.format(git_dir))
        branch=rshell('''cd {0} && git branch| head -1'''.format(git_dir))
        tag=rshell('''cd {0} && git tag|tail -1'''.format(git_dir))
        if branch[0] == 0:
            if branch[1].split(' ')[1] == 'master':
                return gver[1]
            else:
                return gver[1].replace(tag[1],'v{0}'.format(branch[1].split(' ')[1]))
    return

def load_kmod(modules=[]):
    if type(modules) is list:
        for ii in modules:
            os.system('modprobe --ignore-install {0} && modprobe {0}'.format(ii))
        return
    print('modules is not list type data ({0})'.format(modules))
    return False

def reduce_string(string,symbol=' ',snum=0,enum=None):
    if type(string) is str:
        arr=string.split(symbol)
    strs=None
    if enum is None:
        enum=len(arr)
    for ii in range(snum,enum):
        if strs is None:
            strs='{0}'.format(arr[ii])
        else:
            strs='{0} {1}'.format(strs,arr[ii])
    return strs

def list2str(arr):
    rc=None
    for i in arr:
        if rc:
            rc='{0} {1}'.format(rc,i)
        else:
            rc='{0}'.format(i)
    return rc
