# coding: utf-8

#from __future__ import division
import ui
import clipboard
from console import hud_alert

shows_result = False

def init_action(sender):
	# Get the root view:
	vv = sender.superview
	# Get the Test1’s title
	label= vv['Test1'].text
	label2=vv['label2']
	label2.text=label

def copy_action(sender):
	clipboard.set(sender.superview['label2'].text)
	hud_alert('Copied')
	
def button_tapped(sender):
	'@type sender: ui.Button'
	# Get the button's title for the following logic:
	t = sender.title

	# Get the labels:
	label = sender.superview['Test1']
	label2 = sender.superview['label2']
	
	#check button’s title is in defined array
	if t in '0123456789':
		if label.text == '0':
			# Replace 0 or last result with number:
			label.text = t
		else:
			# put label data to button
			#t=label.text 
			# Append number:
			label.text = label.text +'' +t
			# put label data to label2’s display
			label2.text = label.text
	#check button’s title is 'AC' or not
	elif t == 'AC':
		# Clear All
		label.text = '0'
		label2.text=''

v = ui.load_view('ksch')

# make a initial data from Test1 to label2
init_action(v['label2'])

if min(ui.get_screen_size()) >= 768:
	# iPad
	v.frame = (0, 0, 360, 400)
	v.present('sheet')
else:
	# iPhone
	v.present(orientations=['portrait'])
