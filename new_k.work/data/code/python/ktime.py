from datetime import datetime
from datetime import timedelta

def ptime(**data): 
    '''
    ** Print format time
    rf=xxxx : read format (default:auto detect) (ex: %Y-%m-%d %H:%M:%S, %Y-%m-%d %I:%M:%S%p)
    <add|del>=[<##>N1,<##>N2,...] or <##>N  # Add or del <##>y(year),w(week),d(day),h(hour),m(min),s(sec)
    p=<format> : print format(yy(year),mm(mon),dd(day),hh(hour),m(min),ss(sec),ts(total sec),ww(week number),wd(weekday),wn(weekday name),apm(am/pm), %Y-%m-%d %H:%M:%S(default format))
    default return is TIME
    '''
    _print_format='%Y-%m-%d %H:%M:%S'
    _read_format=None
    _time=datetime.now() 
    if len(data) > 0:
        keys=data.keys()
        if 'pf' in keys: # print form
            _print_format=data['pf']
        elif 'format' in keys: # print form
            _print_format=data['format']

        if 'rf' in keys: # Read form
            _read_format=data['rf']

        if 't' in keys: # input time
            if _read_format is None:
               time_date_format=None
               time_time=None
               time_apm=None
               time_str_arr=data['t'].split(' ')

               # 1 or 2 value
               #date
               s_time_str_arr_1=time_str_arr[0].split('/')
               d_time_str_arr_1=time_str_arr[0].split('-')
               t_time_str_arr_1=time_str_arr[0].split(':')
               if len(s_time_str_arr_1) > 1:
                   if len(s_time_str_arr_1) == 3:
                       if len(s_time_str_arr[0]) == 4:
                           time_date_format='%Y/%m/%d'
                       else:
                           time_date_format='%m/%d/%y'
                   elif len(s_time_str_arr_1) == 2:
                           time_date_format='%m/%d'
               elif len(d_time_str_arr_1) > 1:
                   if len(d_time_str_arr_1) == 3:
                       if len(d_time_str_arr_1[0]) == 4:
                           time_date_format='%Y-%m-%d'
                       else:
                           time_date_format='%m-%d-%y'
                   elif len(d_time_str_arr_1) == 2:
                       time_date_format='%m-%d'
               elif len(t_time_str_arr_1) > 1:
               #time
                   if len(t_time_str_arr_1) == 3:
                       if len(time_str_arr) == 2:
                           _read_format='%I:%M:%S %p'
                       elif len(time_str_arr) == 1:
                           if time_str_arr[0][-1] == 'm' or time_str_arr[0][-1] == 'M':
                               _read_format='%I:%M:%S%p'
                           else:
                               _read_format='%H:%M:%S'
                   elif len(t_time_str_arr_1) == 2:
                       if len(time_str_arr) == 2:
                           _read_format='%I:%M %p'
                       elif len(time_str_arr) == 1:
                           if time_str_arr[0][-1] == 'm' or time_str_arr[0][-1] == 'M':
                               _read_format='%I:%M%p'
                           else:
                               _read_format='%H:%M'

               # 2 or 3 value
               #time
               if time_date_format is not None and _read_format is None and len(time_str_arr) > 1:
                   t_time_str_arr_2=time_str_arr[1].split(':')
                   if len(t_time_str_arr_2) > 1:
                       if len(t_time_str_arr_2) == 3:
                           if len(time_str_arr) == 3:
                               if time_str_arr[2][-1] == 'm' or time_str_arr[2][-1] == 'M':
                                   _read_format='{0} %I:%M:%S %p'.format(time_date_format)
                           else:
                               if time_str_arr[1][-1] == 'm' or time_str_arr[1][-1] == 'M':
                                   _read_format='{0} %I:%M:%S%p'.format(time_date_format)
                               else:
                                   _read_format='{0} %H:%M:%S'.format(time_date_format)
                       elif len(t_time_str_arr_2) == 2:
                           if len(time_str_arr) == 3:
                               if time_str_arr[2][-1] == 'm' or time_str_arr[2][-1] == 'M':
                                   _read_format='{0} %I:%M %p'.format(time_date_format)
                           else:
                               if time_str_arr[1][-1] == 'm' or time_str_arr[1][-1] == 'M':
                                   _read_format='{0} %I:%M%p'.format(time_date_format)
                               else:
                                   _read_format='{0} %H:%M'.format(time_date_format)

               if _read_format is None:
                   print("can't find format for {0}. please give time format".format(data['t']))
                   return False
               _time=datetime.strptime(data['t'],_read_format)

        def get_timedelta(key='add'): 
            _year=0
            _week=0
            _day=0
            _hour=0
            _min=0
            _sec=0
            if not key in data:
                return (_day,_hour,_min,_sec)

            if type(data[key]) is list:
                for _add in list(data[key]):
                    _add_symbol=_add[-1]
                    _new_add=_add[:-1]
                    if _add_symbol == 'y':
                        _year=int(_new_add)
                    elif _add_symbol == 'w':
                        _week=int(_new_add)
                    elif _add_symbol == 'd':
                        _day=int(_new_add)
                    elif _add_symbol == 'h':
                        _hour=int(_new_add)
                    elif _add_symbol == 'm':
                        _min=int(_new_add)
                    elif _add_symbol == 's':
                        _sec=int(_new_add)
            else:
                _add_symbol=data[key][-1]
                _new_add=data[key][:-1]
                if _add_symbol == 'y':
                    _year=int(_new_add)
                elif _add_symbol == 'w':
                    _week=int(_new_add)
                elif _add_symbol == 'd':
                    _day=int(_new_add)
                elif _add_symbol == 'h':
                    _hour=int(_new_add)
                elif _add_symbol == 'm':
                    _min=int(_new_add)
                elif _add_symbol == 's':
                    _sec=int(_new_add)

            if _year > 0:
                _day=_day+(_year * 365)
            if _week > 0:
                _day=_day+(_week * 7)
            return (_day,_hour,_min,_sec)

        if 'add' in keys: # Add time
            if data['add'] == 'today':
                dn=datetime.now()
                _time=_time.replace(year=dn.year,month=dn.month,day=dn.day)
            else:
                _day,_hour,_min,_sec=get_timedelta(key='add')
                _time = _time + timedelta(days=_day,hours=_hour,minutes=_min,seconds=_sec)
        elif 'del' in keys: # delete time
            _day,_hour,_min,_sec=get_timedelta(key='del')
            _time = _time - timedelta(days=_day,hours=_hour,minutes=_min,seconds=_sec)

        if 'p' in keys: # print short value
            _print=data['p']
            if _print == 'day' or _print == 'dd':
                return _time.day
            elif _print == 'year' or _print == 'yy':
                return _time.year
            elif _print == 'month' or _print == 'mon' and _print == 'mm':
                return _time.month
            elif _print == 'hour' or _print == 'hh':
                return _time.hour
            elif _print == 'minute' or _print == 'min' or _print == 'm':
                return _time.minute
            elif _print == 'second' or _print == 'sec' or _print == 'ss':
                return _time.minutes
            elif _print == 'week_number' or _print == 'ww':
                return _time.isocalendar()[1]
            elif _print == 'weekday' or _print == 'wd':
                return _time.weekday()
            elif _print == 'weekday_name' or _print == 'wn':
                return _time.strftime('%a')
            elif _print == 'apm':
                return _time.strftime('%p')
            elif _print == 'ts' or _print == 'total_sec':
                return _time.strftime('%s')
            elif _print == 'now':
                return datetime.now().strftime('%s')
            elif _print == 'today':
                return datetime.now().date()
            elif _print == 'today2':
                return datetime.now().strftime('%Y/%m/%d')
            else:
                if len(data['p'].split('%')) > 1:
                    return _time.strftime(data['p'])# print format
                else:
                    return _time.strftime(_print_format)# print format

    return _time # return to time
        

def chk_time(**data):
    if 'wd' in data.keys():
        if ptime(p='wd') in data['wd']:
            return True
    if 'yy' in data.keys():
        if ptime(p='yy') in data['yy']:
            return True
    if 'mm' in data.keys():
        if ptime(p='mm') in data['mm']:
            return True
    if 'hh' in data.keys():
        if ptime(p='hh') in data['hh']:
            return True
    if 'm' in data.keys():
        if ptime(p='m') in data['m']:
            return True



a=ptime(t='11:30pm',add='today',p='default')
print(a)
b=ptime(t=a,add='31m')
print(b)
#a=ptime(t='12-13 2:30pm',add=['1d','3h'],p='ww')
#print(a)
