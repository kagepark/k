#!/usr/bin/python3

import sys
import os
import argparse

from os import path
from pprint import pprint
from tempfile import TemporaryDirectory

from sh import dd, sudo, ls, losetup, parted, kpartx, mke2fs, grub_install, mount, umount, cp, chmod, ln, chown


def showArgs(args):
    for i,arg in enumerate(args):
        print("{}) {}".format(i, arg))

def ensureNewFileIsCreated(fileName, func):
    if path.isfile(fileName):
        os.remove(fileName)

    func()

    if not path.isfile(fileName):
        raise Exception("Error: could not create raw image file:{}".format(fileName))

def createRawImageFile(fileName, imageSize):
    def func():
        dd("if=/dev/zero", "of={}".format(fileName), "bs=1", "count=0", "seek={}".format(imageSize))
    
    ensureNewFileIsCreated(fileName, func)
    


def main():
    try:
        print("-------CREATE LINUX VIRTUAL DISK WITH ROOTFS--------")
        
        parser = argparse.ArgumentParser()
        parser.add_argument("imageFileName", help="Path to the output image file")
        parser.add_argument("imageFileSize", help="Size of the output image file. You can use suffixes allowed by the dd command.")
        parser.add_argument("--kernel", help="Path to a kernel image. This file will be copied to /boot on the primary partition of the disk image")
        parser.add_argument("--busybox", help="Path to the busybox executable. This file will be copied to /bin on the primary partition of the disk image. Links to several useful commands will also be created.")
        parser.add_argument("--grubcfg", help="Path to a grub.cfg file. A customized version of grub.cfg can be used to boot linux directly and avoid entering commands on the grub prompt.")
        args = parser.parse_args()
                
        createRawImageFile(args.imageFileName, args.imageFileSize)

        chown('sandeepd:sandeepd', args.imageFileName)

        loop0 = losetup('-f', '--show', args.imageFileName).strip()
        print("Disk image file looped at:{}".format(loop0))
        try:
            print("Creating msdos partition table")
            parted(loop0, 'mklabel msdos')
            
            print("Creating primary partition")
            parted(loop0, 'mkpart primary 1 100%')
            try:
                print("Looping primary partition to /dev/mapper")
                kpartx('-va', loop0)
                try:
                    loop1 = losetup('-f','--show', "/dev/mapper/{}p1".format(path.basename(loop0))).strip()
                    print("Primary partition looped at:{}".format(loop1))
                    
                    print("Formatting primary partition")
                    mke2fs(loop1)

                    with TemporaryDirectory(dir='/mnt') as dirName:
                        print("Created temporary mount point for primary partion")
                        
                        print("Mounting partion 1 at:", dirName)
                        mount(loop1, dirName)
                        
                        bootDir = path.join(dirName, 'boot')
                        
                        print("Installing GRUB2")
                        grub_install('--boot-directory={}'.format(bootDir), '--modules=ext2 part_msdos', loop0)
                        
                        print(ls('-lh', bootDir))

                        if args.kernel:
                            print("Copying kernel to /boot on primary partition")
                            cp(args.kernel, bootDir)

                        print(ls('-lh', bootDir))
                        
                        if args.busybox:
                            print("Installing busybox to /bin")
                            binDir = path.join(dirName, 'bin')
                            os.makedirs(binDir)
                            busboxTargetPath = path.join(binDir,'busybox')
                            cp(args.busybox, busboxTargetPath)
                            chmod('+x', busboxTargetPath)
                            cwd=os.getcwd()
                            os.chdir(binDir)
                            for cmd in ['ash', 'sh', 'ls', 'rm', 'mkdir', 'rmdir', 'grep', 'find', 'mount', 'umount']:
                                #Use relative paths here.
                                ln('-s', 'busybox', cmd)
                            os.chdir(cwd)
                            print(ls('-lh', binDir))

                        if args.grubcfg:
                            print("Copying '{}' to /boot/grub".format(args.grubcfg))
                            cp(args.grubcfg, path.join(bootDir, 'grub'))
                        
                        umount(loop1)
                except Exception as e:
                    print(e)
                finally:
                    losetup('-d', loop1)
            except Exception as e:
                print(e)
            finally:
                kpartx('-vd', loop0)
        except Exception as e:
            print(e)
        finally:
            losetup('-d', loop0)
    except Exception as e:
        print(e)

if __name__ == "__main__":
    main()
