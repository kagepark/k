members={}
# member : name, tel, eat

init_total_member=len(members)

#new member
cur_total_member=len(members)

# Update changed member 
def update_eat_count(old_members,cur_members):
    init_total_member=len(old_members)
    cur_total_member=len(cur_members)
    if init_total_member != cur_total_member:
        for n in cur_members.keys():
            cur_members[n]['eat'] = cur_members[n]['eat'] + (init_total_member - cur_total_member)
    return cur_members


# find host
def find_host(members):
    for n in members.keys():
        if members[n]['eat'] == 0:
            return n
