def dict_del(dd,keys):
   d=dd.copy()
   keysa=keys.split('/')
   if len(keysa) == 1:
      d.pop(keysa[0])
      return d

   ii=0
   m=[]
   m.append(d[keysa[0]])
   for ii in range(1,len(keysa)):
      if keysa[ii] in m[ii-1]:
         m.append(m[ii-1][keysa[ii]])
      elif ii < len(keysa)-1:
         return 
   m[ii-1].pop(keysa[ii])
   return d


def dict_get(dd,keys=None):
   if keys is None:
      return dd
   else:
      t=None
      d=dd.copy()
      for key in list(keys.split('/')):
         if key in d:
            t=d[key]
            d=t
      return t

def dict_put(dic,path,value):
      path_arr=path.split('/')
      mx=len(path_arr)-1
      new_dic=dic
      for ii in range(0,mx):
         if not path_arr[ii] in new_dic:
            new_dic[path_arr[ii]]={}
         dic=new_dic[path_arr[ii]]
         new_dic=dic
      if len(path_arr[mx]) > 0:
         if not path_arr[mx] in new_dic:
            new_dic[path_arr[mx]]={}
         new_dic[path_arr[mx]]=value
         dic=new_dic



a={'a1':{'aa1':{'aaa1':{'aaaa1':1111111,'aaaa2':2222222},'aaa2':{'aaaa22':1111}}},'b1':{'bb1':1111}}
print(a)
print("\n")

oo=dict_del(a,'a1/aa1/aaa2')
uu=dict_get(oo)
print(uu)
oo=dict_put(a,'a1/aa1/aaa2',{'aaaa22':77777,'aaaa33':00000})
#oo=dict_add(a,'a1/aa1',{'aaaa22':77777,'aaaa33':00000})
uu=dict_get(oo)
print(uu)
uu=dict_get(a)
print(uu)
