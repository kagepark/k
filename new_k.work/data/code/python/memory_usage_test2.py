import resource
class Foo(object):
   def __init__(self, val):
      self.val1 = val+1
      self.val2 = val+2
      self.val3 = val+3
      self.val4 = val+4
      self.val5 = val+5
      self.val6 = val+6
def f(count):
   l = []
 
   for i in range(count):
      foo = Foo(i)
      l.append(foo)
   return l
def main():
   count = 10000
   l = f(count)
   mem = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
   print "Memory usage is: {0} KB".format(mem)
   print "Size per foo obj: {0} KB".format(float(mem)/count)
if __name__ == "__main__":
   main()
