import ui

class MyView(ui.View):
    def __init__(self):
        self.present()

    def did_load(self):
        self['button1'].title = 'It works ;-)'
#        self['button1'].action = self.button_tapped

    def button_tapped(self, sender):
        sender.title = 'Got tapped'

the_view = ui.load_view()
