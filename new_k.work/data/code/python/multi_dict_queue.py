import Queue
import threading 
import time
exitFlag = 0
class myThread (threading.Thread):
   def __init__(self, threadID, name, counter,q,b):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.counter = counter
      self.q = q
      self.b = b
   def run(self):
      print "Starting " + self.name
      print_time(self.name, 20, self.counter,self.q, self.b)
      print "Exiting " + self.name

def print_time(threadName, counter, delay, q, b):
   while counter:
      if exitFlag:
         threadName.exit()
      time.sleep(delay)
      if counter%4 == 0:
          n="%s" % (time.ctime(time.time()))
          a={'nd':{'uu':2,'ndbar':{'3':n}}}
          q_update(q,a)
      print "%s: %s : %s <> %s" % (threadName, time.ctime(time.time()), q_get(q), q_get(b))
      counter -= 1


def dict_del(dd,keys):
   d=dd.copy()
   keysa=keys.split('/')
   if len(keysa) == 1:
      if keysa[0] in d:
         d.pop(keysa[0])
      return d

   ii=0
   m=[]
   m.append(d[keysa[0]])
   for ii in range(1,len(keysa)):
      if keysa[ii] in m[ii-1]:
         m.append(m[ii-1][keysa[ii]])
      elif ii < len(keysa)-1:
         return
   m[ii-1].pop(keysa[ii])
   return d


def dict_get(dd,keys=None):
   if keys is None:
      return dd
   else:
      t=None
      d=dd.copy()
      for key in list(keys.split('/')):
         if key in d:
            t=d[key]
            d=t
      return t

def dict_put(dic,path,value):
      path_arr=path.split('/')
      mx=len(path_arr)-1
      new_dic=dic
      for ii in range(0,mx):
         if not path_arr[ii] in new_dic:
            new_dic[path_arr[ii]]={}
         dic=new_dic[path_arr[ii]]
         new_dic=dic
      if len(path_arr[mx]) > 0:
         if not path_arr[mx] in new_dic:
            new_dic[path_arr[mx]]={}
         new_dic[path_arr[mx]]=value
         dic=new_dic


def q_init(d,qnum=1):
   q=Queue.Queue(qnum)
   q.put(d)
   return q

def q_get(q,keys=None):
   gd = q.get()
   d = gd.copy()
   q.put(gd)
   if keys is not None:
       return dict_get(d,keys=keys)
   else:
       return d

def q_update(q,d,keys=None):
   gd = q.get()
   try:
      gd.update(d)
   finally:
      q.put(gd)
      return True
   return False

def q_pop2(q,keys):
   gd = q.get()
   if gd:
      d = gd.copy()
      t=None
      c=0
      keysa=keys.split('/')
      kt=len(keysa)
      u={}
      for key_idx in (range(0,kt)):
         print(key_idx,'d:',d)
         if keysa[key_idx] in d:
            t=d[keysa[key_idx]]
            d.pop(keysa[key_idx])
            if key_idx == 0:
               u=d
            u[key]=d
            u[key][key]=d
            u[key][key][key]=d
            d=t
            c=c+1
         else:
            q.put(gd)
            return False
         print(c,kt)
      print(u)
      u.pop[m]
      q.put(u)
      return True
   q.put(gd)
   return False

def q_pop(q,keys):
   gd = q.get()
   if gd:
      gd=dict_del(gd,keys)
      q.put(gd)
      return True
   q.put(gd)
   return False

def q_clean(q):
   gd = q.get()
   q.put({})
   return True

print("initial global dict")
#q=Queue.Queue(1)
a={'foo':1,'bar':'aaa'}
q=q_init(a)
b={'boo':1,'bar':'bbb'}
qb=q_init(b,qnum=2)

print("get the initialized dict")
ggg=q_get(q)
print(ggg)
bbb=q_get(qb)
print(bbb)

print("update dict")
a={'foo':1,'bar':'up'}
q_update(q,a)
ggg=q_get(q)
print(ggg)

# Create new threads
thread1 = myThread(1, "Thread-1", 3,q,qb)
thread2 = myThread(2, "Thread-2", 5,q,qb)
thread3 = myThread(3, "Thread-3", 2,q,qb)

# Start new Threads
thread1.start()
thread2.start()
thread3.start()

time.sleep(5)
print("Add new dict")
a={'new':2,'newbar':'new'}
q_update(q,a)
ggg=q_get(q)
print(ggg)

print("queue size")
print(q.qsize())

print("queue full")
print(q.full())

print("queue Emplty")
print(q.empty())


time.sleep(5)
print("del new dict")
print(q_pop(q,'new'))
print(q_get(q))
print(q_pop(q,'new2'))
print(q_get(q))

print("Add nb dict")
a={'nd':{'uu':2,'ndbar':{'3':'ndnew'}}}
q_update(q,a)
ggg=q_get(q)
print(ggg)

time.sleep(5)
print("Get the dict 3 in nd/ndbar/3")
ggg=q_get(q,keys='nd/ndbar/3')
print(ggg)

print("del the dict 3 in nd/ndbar/3")
ggg=q_pop(q,keys='nd/ndbar/3')
print(ggg)
ggg=q_get(q)
print(ggg)

print("clean all")
q_clean(q)
print(q_get(q))



print "Exiting Main Thread"
