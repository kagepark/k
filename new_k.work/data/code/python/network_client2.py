import socket
import lz4.frame
import base64

# Encrypt / Decrypt Function
from Crypto.Cipher import AES
counter = "H"*16
key = "H"*32
compress=True
#compress=False

def encrypt(data):
    encrypto = AES.new(key, AES.MODE_CTR, counter=lambda: counter)
    if compress:
        return base64.b64encode(lz4.frame.compress(encrypto.encrypt(data)))
    else:
        return encrypto.encrypt(data)

def decrypt(data):
    decrypto = AES.new(key, AES.MODE_CTR, counter=lambda: counter)
    if compress:
        return decrypto.decrypt(lz4.frame.decompress(base64.b64decode(data)))
    else:
        return decrypto.decrypt(data)


#def encrypt(message):
#    encrypto = AES.new(key, AES.MODE_CTR, counter=lambda: counter)
#    return encrypto.encrypt(message)
#
#def decrypt(message):
#    decrypto = AES.new(key, AES.MODE_CTR, counter=lambda: counter)
#    return decrypto.decrypt(message)

# Network connectioin
soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  
soc.connect(("127.0.0.1", 623))

# Generate Data
import random  
data = zip([random.randint(1,10000) for i in range(10000)],  
 [random.randint(1,10000) for i in range(10000)])

for x, y in data:  
    # send x and y separated by tab 
    data = "{}\t{}".format(x,y)
    #soc.sendall(data.encode("utf8"))
    # Send encrypt data
    soc.sendall(encrypt(data).encode("utf8"))

    # wait for response from server, so we know
    # that server has enough time to process the
    # data. Without this it can make problems

    # Receive Data and decrypt the data
#    if soc.recv(4096).decode("utf8") == "-":
    if decrypt(soc.recv(4096)).decode("utf8") == "-":
        pass

# end connection by sending this string
#soc.send(b'--ENDOFDATA--')
soc.send(encrypt(b'--ENDOFDATA--'))  
