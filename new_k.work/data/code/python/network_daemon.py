#!/usr/bin/python
# /etc/systemd/system/foo.socket
# [Unit]
#Description=Foo Socket
#PartOf=foo.service
#
#[Socket]
#ListenStream=127.0.0.1:9999
#
#[Install]
#WantedBy=sockets.target

# Test
# echo "Hello" | netcat 127.0.0.1 9999

from SocketServer import TCPServer, StreamRequestHandler
import socket
import logging

class Handler(StreamRequestHandler):
    def handle(self):
        self.data = self.rfile.readline().strip()
        logging.info("From <%s>: %s" % (self.client_address, self.data))
        self.wfile.write(self.data.upper() + "\r\n")

class Server(TCPServer):
    # The constant would be better initialized by a systemd module
    SYSTEMD_FIRST_SOCKET_FD = 3

    def __init__(self, server_address, handler_cls):
        # Invoke base but omit bind/listen steps (performed by systemd activation!)
        TCPServer.__init__(
            self, server_address, handler_cls, bind_and_activate=False)
        # Override socket
        self.socket = socket.fromfd(
            self.SYSTEMD_FIRST_SOCKET_FD, self.address_family, self.socket_type)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    HOST, PORT = "10.5.1.201", 623 # not really needed here
    server = Server((HOST, PORT), Handler)
    server.serve_forever()
