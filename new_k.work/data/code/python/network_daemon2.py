import socket
import lz4.frame
import base64

# Encrypt / Decrypt function
#pycrypto
from Crypto.Cipher import AES
counter = "H"*16
#counter=os.random(16)
key = "H"*32
#key=os.random(32)
compress=True
#compress=False

def encrypt(data):
    encrypto = AES.new(key, AES.MODE_CTR, counter=lambda: counter)
    if compress:
        return base64.b64encode(lz4.frame.compress(encrypto.encrypt(data)))
    else:
        return encrypto.encrypt(data)

def decrypt(data):
    decrypto = AES.new(key, AES.MODE_CTR, counter=lambda: counter)
    if compress:
        return decrypto.decrypt(lz4.frame.decompress(base64.b64decode(data)))
    else:
        return decrypto.decrypt(data)

def rec_data(conn, MAX_BUFFER_SIZE):  # Receive data and decrypt the data, return readable data
    #input_from_client_bytes = decrypt(conn.recv(MAX_BUFFER_SIZE),key,counter)
    input_from_client_bytes = conn.recv(MAX_BUFFER_SIZE)

    import sys
    siz = sys.getsizeof(input_from_client_bytes)
    if  siz >= MAX_BUFFER_SIZE:
        print("The length of input is probably too long: {}".format(siz))

    input_from_client = decrypt(input_from_client_bytes).decode("utf8").rstrip()

    return input_from_client

def client_thread(conn, ip, port, MAX_BUFFER_SIZE = 4096): # thread per each connection
    # read lines periodically without ending connection
    still_listen = True
    while still_listen:
        input_from_client = rec_data(conn, MAX_BUFFER_SIZE) # receive data

        # if you receive this, end the connection
        if "--ENDOFDATA--" in input_from_client: # End data
            print('--ENDOFDATA--')
            conn.close()
            print('Connection ' + ip + ':' + port + " ended")
            still_listen = False
        else:# Handle data
            splin = input_from_client.split('\t')

            print("{}, {}".format(splin[0], splin[1]))

            # tell client that we can accept another data processing
            conn.sendall(encrypt("-").encode("utf8")) # reply data
            #conn.sendall("-".encode("utf8"))

def start_server(ip,port): # Start daemon
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # this is for easy starting/killing the app
    soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    print('Socket created for {0}:{1}'.format(ip,port))

    try:
        soc.bind((ip, port))
        print('Socket bind complete')
    except socket.error as msg:
        import sys
        print('Bind failed. Error : ' + str(sys.exc_info()))
        sys.exit()

    #Start listening on socket
    soc.listen(10)
    print('Socket now listening')

    # for handling task in separate jobs we need threading
    from threading import Thread

    # this will make an infinite loop needed for 
    # not reseting server for every client
    while True:
        conn, addr = soc.accept()
        ip, port = str(addr[0]), str(addr[1])
        print('Accepting connection from ' + ip + ':' + port)
        try:
            Thread(target=client_thread, args=(conn, ip, port)).start()
        except:
            print("Terible error!")
            import traceback
            traceback.print_exc()
    soc.close()

start_server("127.0.0.1",623)  
