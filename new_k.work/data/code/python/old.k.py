#!/usr/bin/env python
# coding: utf-8
# encoding=utf8
#
# Copyright CEP(Kage Park)
# License : GPL
#   It can be sell by CEP Only.
#   Others according to GPL license(Only Freeware software).
#   Since : March 1994
#########
# requires
# https://bitbucket.org/kagepark/cep_py.git

# Version :<type>.<main>.<bug fix/addition in the main>.<testing in the bug/addition>
# <type> - 0:shell, 1: python, 2: C, 3:OSX, 4:iOS, 5:Android, 6:Java, 7:Fortran, 8:Unknown
#_k_v_version="1.1.5.46"
version="1.2.1.3"

# common
import cep
#cep.imports(['platform','os','sys','time','getpass','datetime','termcolor','subprocess'],globals())
import platform,os,sys,time,getpass,datetime,termcolor,subprocess,argparse
import sqlite3
import re
datetime=datetime.datetime
colored=termcolor.colored
k = cep
k.dbg_file='k.dbg'
k._kv_dbg=3

def _mkp(filename=None):
    init_py_temp="""#!/usr/bin/env python
# coding: utf-8
# encoding=utf8
#
# Copyright CEP(Kage Park)
# License : GPL
#   It can be sell by CEP Only.
#   Others according to GPL license(Only Freeware software).
#   Since : 1994
# {0}  {1}  Initial create
# Version :<type>.<main>.<bug fix/addition in the main>.<testing in the bug/addition>
# <type> - 0:shell, 1: python, 2: C, 3:OSX, 4:iOS, 5:Android, 6:Java, 7:Fortran, 8:Unknown
_k_v_version="1.0.0.1" 

###############
### Code here
""".format(datetime.now().strftime("%m/%d/%Y"),getpass.getuser())
    f = open(filename,'w')
    f.write(init_py_temp)
    f.close()

def _help(msg=None, cmd=None, opt=1):
     nargc=len(sys.argv)
     cmd_name=os.path.basename(sys.argv[0])
     if ( cmd_name == 'k' or cmd_name == 'k.py' ) and ( nargc == 1 or sys.argv[1] == '--help'):
         lib_ver=k.version()
         print("""CEP(Kage) personal command and library
  for helping CEP
  version: {0}
  lib ver: {1}\n\nUsage)""".format(version,lib_var))

         print(os.path.basename(sys.argv[0]) + ' <option>')
         option_str="""
 --help [<module>] : help [ for <module>]
 -v(--version) : version
 -b(--build) <file>    : build to binary file
 -m(--module) -b(--build) <file> : build to library file
 -t <file>     : convert character code to utf-8
 -h <hostname> : Make a hostname list
"""
         print(option_str)
         print(os.path.basename(sys.argv[0]) + ' <command> [<option of command>]')
         command_str = """
 show/look/what            : show DB
   ( Make a done a task in show command )
 save/add                  : save item to DB
 update/change/replace     : update item in DB
 recover                   : recover from trash to original DB (*** Not support yet, just scheduled)
 memory/remember           : save to memo to memory DB
 rememory/reremember       : update memo in memory DB
 forget/del/delete/trash   : move item to trash DB
 search  <string>          : find the <string> in all DB
 search_all <string>       : find the <string> in all DB even trash
 cleanup                   : Cleanup garbage data in DB file
 cleanup trash [all]       : Cleanup trash DB (all: make a empty trash) (default: keep 30days trash from now on)

 fp add                    : Make a finger print DB
 fp info                   : Get finger print information of file or MD5 string
 fp dup                    : Find duplicated finger print file
 fp cleanup                : Cleanup finger print DB
 fp update                 : Cleanup and update finger print DB
 fp flush                  : Flush DB

 pkg                       : Package handle

 cp                        : cp command
 mv                        : mv command
 rm                        : rm command
 vi [<option of command>] <file>
 md5                       : md5 command
 mkp                       : Make a python initial file
 sort                      : sort string lines

 ping [<option of command>]
 ssh [<option of command>] -h [<id>@]<host> [<command>]
 scp [<option of command>] <file> [<id>@]<host>:[<path>]
 rsync [<option of command>] <file> [<id>@]<host>:[<path>]
"""
         print(command_str)
         k.exit()
     elif cmd is not None and nargc == 1:
        if opt != 0:
           if cmd == sys.argv[nargc-1] and nargc == 1:
              print(os.path.basename(sys.argv[nargc-1]) + ' --help')
              k.exit()
     elif cmd is not None and nargc > 1:
        if opt == 0:
           if cmd == sys.argv[nargc-2] and sys.argv[nargc-1] == '--help':
              print(os.path.basename(sys.argv[nargc-2]) + ' ' + msg)
              k.exit()
        else:
           if cmd == sys.argv[nargc-1] and nargc == 2:
              print(os.path.basename(sys.argv[nargc-1]) + ' --help')
              k.exit()
           elif cmd == sys.argv[nargc-2] and sys.argv[nargc-1] == '--help':
              print(os.path.basename(sys.argv[nargc-2]) + ' ' + msg)
              k.exit()

def task_chk(db):
     chk_reminder_list=sql.get(db,'task','',field='idx,title,due', order=' ORDER BY pri, due', condition=" done==0 and due <= {0}".format(int(k.now() + (3600 * 24)) ))
     if len(chk_reminder_list) > 0:
        print('------------------------------------------------------------------- ')
        print(colored('** Reminder **','red'))
        for ii in list(chk_reminder_list):
           due_date=0
           due_time=0
           if ii[2] > 0:
               due_date=time.strftime('%Y-%m-%d', time.localtime(int(ii[2])))
               due_time=time.strftime('%H:%M:%S', time.localtime(int(ii[2])))
           print(colored('  - "{0}" due {1} {2} at {3}'.format(ii[1],due_date,due_time,ii[0]),'cyan'))
        print('------------------------------------------------------------------- ')

def pkg(*argv):
        _help('[ -repo <yum repo file> | -iso <iso file>] \tlist : package list(rpm|brew|pip)\n\t\t\t\t\t\tfile <pkg name> : file list of the package\n\t\t\t\t\t\tinstall <pkg> : install pkg\n\t\t\t\t\t\tinstall pip : install pip\n\t\t\t\t\t\tinstall pip3 : install pip3',cmd=cmd,opt=0)
        sys.argv.pop(0)
        sys.argv.pop(0)
        os_class.pkg()

def lsmod(*argv):
        _help(': display module list',cmd=cmd,opt=0)
        sys.argv.pop(0)
        sys.argv.pop(0)
        rc=os_class.lsmod(find=sys.argv)
        return rc

def insmod(*argv):
        _help('<module name>',cmd=cmd)
        sys.argv.pop(0)
        sys.argv.pop(0)
        aa=os_class.insmod(sys.argv)
        return aa

def rmmod(*argv):
        _help('<module name>',cmd=cmd)
        sys.argv.pop(0)
        sys.argv.pop(0)
        aa=os_class.rmmod(sys.argv)
        return aa


def mount(*argv):
        _help('<device>|<img>:<snap> <mount dir>',cmd=cmd)
        sys.argv.pop(0)
        sys.argv.pop(0)
        os_class.mount(sys.argv[0],sys.argv[1])

def umount(*argv):
        _help('<device> [<mount dir>]',cmd=cmd)
        sys.argv.pop(0)
        sys.argv.pop(0)
        os_class.umount(sys.argv[0],sys.argv[1])

def md5(*argv):
        _help('<file>',cmd=cmd)

        ffname=sys.argv[len(sys.argv)-1]
        md5_str=k.CMD().md5(ffname)
        print(md5_str)

def vimrc(*argv):
        sys.argv.pop(1)
        home_vimrc="{0}/.vimrc".format(os.getenv("HOME"))
        vimrcfp=open(home_vimrc,"w")
        vimrcfp.write("set title\n")
        vimrcfp.write("set laststatus=2\n")
        vimrcfp.write("set tabstop=4\n")
        vimrcfp.write("set expandtab\n")
        vimrcfp.write("set shiftwidth=4\n")
        vimrcfp.write("set softtabstop=4\n")
        vimrcfp.write("set visualbell\n")
        vimrcfp.write("set ruler\n")
        vimrcfp.write("syntax on")
        vimrcfp.close()


def search_all(*argv):
        _help('<string>',cmd=cmd)
        sys.argv.pop(1)
        title=sys.argv[1]
        for tbname in list(sql.get_table_name(db)):
            fields=sql.get_field_name(db,tbname)
            for fd in list(fields[1:len(fields)-2]):
                 num=sql.find(db,tbname,condition=fd+' like "%'+title+'%"')
                 for nn in list(num):
                     aa=sql.get(db,tbname,fields[1],condition='idx='+k.var(nn,'str'))
                     if len(aa) > 0:
                        print('Found "'+aa[0][0]+'" in "' + tbname + '" at idx='+k.var(nn,'str'))


def search(*argv):
        _help('<string>',cmd=cmd)
        sys.argv.pop(1)
        title=sys.argv[1]
        for tbname in list(sql.get_table_name(db)):
            if tbname == 'trash':
                continue
            fields=sql.get_field_name(db,tbname)
            for fd in list(fields[1:len(fields)-2]):
                 num=sql.find(db,tbname,condition=fd+' like "%'+title+'%"')
                 for nn in list(num):
                     aa=sql.get(db,tbname,fields[1],condition='idx='+k.var(nn,'str'))
                     if len(aa) > 0:
                        print('Found "'+aa[0][0]+'" in "' + tbname + '" at idx='+k.var(nn,'str'))


def forget(*argv):
        _help('<table name> <title> or idx=<idx>',cmd=cmd)
        sys.argv.pop(1)
        # Delete in SQL
        #if len(sys.argv) == 3:
        if input_mx == 3:
            table_name=sys.argv[1]
            title=sys.argv[2]

            idx_find=title.split('=')
            if idx_find[0] == 'idx':
                num=float(idx_find[1])
            else:
                num=sql.find(db,table_name,'/'+title)[0]
            if num > 0 :
                sql_msg = 'select * from ' + table_name + ' where idx='+k.var(num,'str')
                cur = db.cursor()
                cur.execute(sql_msg)
                got_db=cur.fetchall()
                if len(got_db) == 0:
                    print(title + " not found")
                    k.exit()

                #get_data=cur.fetchall()[0]
                get_data=got_db[0]
                #del_db = sql.init_table(home_path+'/cep.db','trash',msg='name text not null, value text, msg text, cnt int default 0')
                del_db = db
                rc=sql.put(del_db,'trash','/'+table_name,sql.convert_put(k.var(get_data,'str')),new=1)
                if rc is None:
                    rc=sql.delete(db,table_name,None,condition="idx={0}".format(num))
                    if rc is None:
                        print('Moved to Trash')
                    else:
                        print('Move failed')
                else:
                    print('Not found')
                del_db.close()
            else:
                print('{0} not found in {1}'.format(title,table_name))
            db.close()
        else:
            print('forget <table name> <title>')


def sort(*argv):
        sys.argv.pop(1)
        parser = argparse.ArgumentParser(
          prog='sort',
          description='sort for sorting string lines or sorting string lines in file',

          epilog='''
example)
sorting string:
./sort "1
6
9
3
2
7"

reverse sorting string:
./sort -r "1
6
9
3
2
7"

sorting file size :
./sort -i 4 -s space "$(ls -l)"

sorting file size reverse:
./sort -i 4 -s space -r "$(ls -l)"

sorting file size, and print size and filename:
./sort -i 4 -s space -p 8,4 "$(ls -l)"
          ''',
          formatter_class=argparse.RawDescriptionHelpFormatter
        )
        parser.add_argument('otherthings',nargs='*')
        parser.add_argument('-v','--version', action='version', version='%(prog)s '+version)
        parser.add_argument('-i','--field_id', action='store', dest='id', help='sorting filed id (start from 0), only one',type=int)
        parser.add_argument('-s','--field_symbol', default='space', action='store', dest='symbol', help='field spliting symbol (default: white space) (ex: " " for only one space, ":" for :)')
        parser.add_argument('-p','--print_field', action='store', dest='printid', help="print field id(start from 0) (ex: 1,3,...), it should need -i")
        parser.add_argument('-n','--newline', action='store', dest='newline', help="new line symbol (default: Linux (\\n))")
        parser.add_argument('-r','--reverse', action='store_true', dest='reverse', help='reverse sort')
        parser.add_argument('-m','--num', action='store_true', dest='num', help='numerical sort')
        parser.add_argument('-f','--file', action='store', dest='filename', help='Input text file',metavar="FILE")
        strings=None
        new_line='\n'
        if cep.is_stream_input():
            for line in sys.stdin:
                if strings is None:
                    strings='{0}'.format(line)
                else:
                    strings='{0}{1}{2}'.format(strings,new_line,line)
        else:
            if len(sys.argv) == 1:
                parser.print_help(sys.stderr)
                sys.exit(1)
        args = parser.parse_args()
        if args.newline:
          new_line=args.newline

        if args.otherthings:
            if strings is None:
               strings=args.otherthings[0]
            else:
               strings='{0}{1}{2}'.format(strings,new_line,args.otherthings[0])
        if args.filename:
            f=open(args.shell_file,'r')
            if strings is None:
                strings=f.read()
            else:
                strings='{0}{1}{2}'.format(strings,new_line,f.read())
            f.close()
        for ii in cep.ksort(string=strings,field_symbol=args.symbol,field_id=args.id,reverse=args.reverse,print_field=args.printid,new_line=new_line,num=args.num):
            print(ii)

def ssh(*argv):
        _help('[<id>@]<hostname>',cmd=cmd)
        # SSH
        mx=len(sys.argv)
        idx=0
        hostname=None
        for ii in range(1,mx):
           if sys.argv[ii][0] == '-':
              idx=0
           else:
              idx=idx+1
           if idx==2:
              hostname=sys.argv[ii]
              hostname_idx=ii

        if hostname is not None:
            tmp=k.var(hostname,'str').split('@')
            host_mx=len(tmp)
            host_name=tmp[host_mx-1]
            found_host=net.host(host_name,type='db',db=db)
            ssh_key_file_opt=''
            ssh_key_file=''
            if found_host is not None:
                found_host_key=sql.get(db,'host','/'+k.var(host_name,'str'),field='key')
                if found_host_key is not None and len(found_host_key) > 0:
                    if found_host_key[0][0] is not None and found_host_key[0][0] != '':
                        found_host_key_str=sql.convert_get(found_host_key[0][0])
                        ssh_key_file=k.CMD().mktemp('/tmp/'+k.var(host_name,'str'))
                        fd = os.open(ssh_key_file, os.O_RDWR|os.O_CREAT , int("0600", 8))
                        if k.is_ver3() == 1:
                            found_host_key_strb=str.encode(found_host_key_str)
                            os.write(fd,found_host_key_strb)
                        else:
                            os.write(fd,found_host_key_str)
                        os.close(fd)
                        ssh_key_file_opt='-i ' + ssh_key_file
            else:
                found_host=net.host(host_name,type='no_db',db=db)
                if found_host is None:
                    print(host_name + ' not found')
                    k.exit(code=-1)

            if host_mx == 1:
                new_host=found_host
            else:
                new_host=tmp[0]+'@'+found_host
            sys.argv.insert(hostname_idx,new_host)
            sys.argv.pop(hostname_idx+1)
            os.system('ssh '+ ssh_key_file_opt + ' -q -o StrictHostKeychecking=no -o CheckHostIP=no -o ConnectTimeout=5 -o UserKnownHostsFile=/dev/null ' + k.var(sys.argv[2:],'str'))
            if len(ssh_key_file) > 0 :
                os.system('[ -f ' + ssh_key_file + ' ] && rm -f ' + ssh_key_file)
        else:
            new_opts=sys.argv[1:]
            os.system('ssh -q -o StrictHostKeychecking=no -o CheckHostIP=no -o ConnectTimeout=5 -o UserKnownHostsFile=/dev/null ' + k.var(new_opts,'str'))

def ping(*argv):
        _help('<hostname>',cmd=cmd)
        new_opts=[]
        sys.argv.pop(1)
        hostname = sys.argv[len(sys.argv)-1]
        found_host=net.host(hostname,db=db)
        if found_host is None:
            print(hostname + ' not found')
            k.exit(code=-1)
        new_opts=sys.argv[1:len(sys.argv)-2]
        new_opts.insert(len(sys.argv)-1,found_host)
        os.system('ping ' + k.var(new_opts,'str'))

def rsync(*argv):
        _help('-a --progress <file|dir> [<id>@]<hostname>:[<dir>]\nrsync -a --progress [<id>@]<hostname>:<file|dir> <file|dir>',cmd=cmd)
        sys.argv.pop(1)
        for ii in range(1,len(sys.argv)):
            if sys.argv[ii].find(':') > 0 :
                host_idx=ii
                break
        hostname_arr=sys.argv[host_idx].split(':')
        hostname_src=hostname_arr[0].split('@')
        host_mx=len(hostname_src)
        host_name=hostname_src[host_mx-1]
        found_host=net.host(host_name,db=db)

        ssh_key_file_opt=''
        ssh_key_file=''
        if found_host is not None:
            found_host_key=sql.get(db,'host','/'+k.var(host_name,'str'),field='key')
            if found_host_key is not None and len(found_host_key) > 0:
                if found_host_key[0][0] is not None:
                    found_host_key_str=sql.convert_get(found_host_key[0][0])
                    ssh_key_file=k.CMD().mktemp('/tmp/'+k.var(host_name,'str'))
                    fd = os.open(ssh_key_file, os.O_RDWR|os.O_CREAT , int("0600", 8))
                    if k.is_ver3() == 1:
                        found_host_key_strb=str.encode(found_host_key_str)
                        os.write(fd,found_host_key_strb)
                    else:
                        os.write(fd,found_host_key_str)
                    os.close(fd)
                    name=k.var(platform.system(),'str')
                    if 'Linux' == name:
                        ssh_key_file_opt=' -e ssh ' + ssh_key_file + ' -p 22 '
                    elif 'Darwin' == name:
                        ssh_key_file_opt=' -e "ssh -i ' + ssh_key_file + ' " '
        else:
            found_host=net.host(host_name,type='no_db',db=db)
            if found_host is None:
                print(host_name + ' not found')
                k.exit(code=-1)

        if host_mx == 1:
            new_host=found_host+':'+hostname_arr[1]
        else:
            new_host=hostname_src[0]+'@'+found_host+':'+hostname_arr[1]
        sys.argv.insert(host_idx,new_host)
        sys.argv.pop(host_idx+1)
        new_argv=sys.argv[1:]

        os.system('rsync ' + ssh_key_file_opt + k.var(new_argv,'str'))
        if len(ssh_key_file) > 0 :
            os.system('[ -f ' + ssh_key_file + ' ] && rm -f ' + ssh_key_file)


#################################
# Main Run
#################################
if __name__ == "__main__":
    sql = cep.SQLite()
    k.control_c()
    k.encoding('utf8')
    home_path=os.path.dirname(os.path.realpath(__file__))
#    db = sql.init_table(home_path+'/cep.db')
    kfpdb = sql.init_table(home_path+'/.kfp.db','fp',msg='md5 text, os_name text, hostname text, dev_id int, inode int, n_hlnk int, f_size int, f_ctime int, f_type text, f_path text')

    net = cep.NET()

    #try:
    #    assert sys.version_info >= (3,0)
    #except AssertionError:
    #    msg='It need python3 version'
    #    k.exit(msg)

    self_cmd_name=os.path.basename(sys.argv[0])
    if len(sys.argv) == 1:
        print(self_cmd_name + ' <command> [ <option of command> ] or <option>')
        k.exit()

    cmd = sys.argv[1]
    os_class=cep.CMD()
    cmd_arr=['update','change','add','replace','save','show','look','waht','remember','memory','rememory','reremember','del','delete','forget','trash']
    table_arr=['host','memory','trash','task','shell']
    table_define_arr=['name text not null, value text, msg text, cnt int default 0, key text','name text not null, value text, msg text,binfile BLOB,cnt int default 0','name text not null, value text, msg text, cnt int default 0','title text not null, pri int, msg text, due int, done int default 0','name text not null, value text, msg text, cnt int default 0']

    if cmd in cmd_arr:
#        sys.argv.pop(1)
        input_mx=len(sys.argv) - 1
        if input_mx < 2:
            print('missing something')
            k.exit()
        table_name=sys.argv[2]
        tdi=[i for i, j in enumerate(table_arr) if j == table_name]
        if len(tdi) > 0:
            db = sql.init_table(home_path+'/cep.db',table_name,msg=table_define_arr[tdi[0]])
        else:
            db = sql.init_table(home_path+'/cep.db')
    else:
        db = sql.init_table(home_path+'/cep.db')
        

    #task_chk(db)

    if cmd == 'scp':
        task_chk(db)
        _help('[-d] <file|dir> [<id>@]<hostname>:[<dir>]\nscp [-d] [<id>@]<hostname>:<file|dir> <file|dir>',cmd=cmd)
        # SCP
        sys.argv.pop(1)

        for ii in range(1,len(sys.argv)):
            if sys.argv[ii].find(':') > 0 :
                host_idx=ii
                break
        hostname_arr=sys.argv[host_idx].split(':')
        hostname_src=hostname_arr[0].split('@')
        host_mx=len(hostname_src)
        host_name=hostname_src[host_mx-1]
        found_host=net.host(host_name,type='db',db=db)
        ssh_key_file_opt=''
        ssh_key_file=''
        if found_host is not None:
            found_host_key=sql.get(db,'host','/'+k.var(host_name,'str'),field='key')
            if found_host_key is not None and len(found_host_key) > 0:
                if found_host_key[0][0] is not None and found_host_key[0][0] != '':
                    found_host_key_str=sql.convert_get(found_host_key[0][0])
                    ssh_key_file=k.CMD().mktemp('/tmp/'+k.var(host_name,'str'))
                    fd = os.open(ssh_key_file, os.O_RDWR|os.O_CREAT , int("0600", 8))
                    if k.is_ver3() == 1:
                        found_host_key_strb=str.encode(found_host_key_str)
                        os.write(fd,found_host_key_strb)
                    else:
                        os.write(fd,found_host_key_str)
                    os.close(fd)
                    ssh_key_file_opt='-i ' + ssh_key_file
        else:
            found_host=net.host(host_name,type='no_db',db=db)
            if found_host is None:
                print(host_name + ' not found')
                k.exit(code=-1)

        if host_mx == 1:
            new_host=found_host+':'+hostname_arr[1]
        else:
            new_host=hostname_src[0]+'@'+found_host+':'+hostname_arr[1]
        sys.argv.insert(host_idx,new_host)
        sys.argv.pop(host_idx+1)
        new_argv=sys.argv[1:]
        os.system('scp ' + ssh_key_file_opt + ' -o StrictHostKeychecking=no -o CheckHostIP=no -o ConnectTimeout=5 -o UserKnownHostsFile=/dev/null ' + k.var(new_argv,'str'))
        if len(ssh_key_file) > 0 :
            os.system('[ -f ' + ssh_key_file + ' ] && rm -f ' + ssh_key_file)
        k.exit()    
    elif cmd == 'remember' or cmd == 'memory' or cmd == 'rememory' or cmd == 'reremember':
        task_chk(db)
        _help('<title> <--msg <message>> or <--file <text file>> or <--save_file>  [--arch <binary file>]',cmd=cmd)
        sys.argv.pop(1)
#        input_mx=len(sys.argv)
        table_name='memory'
        title=sys.argv[1]
        #db = sql.init_table(home_path+'/cep.db',table_name,msg='name text not null, value text, msg text,binfile BLOB,cnt int default 0')

        body_file=k.opt('--file',1)
        bin_file=k.opt('--arch',1)
        body_msg=k.opt('--msg',1)
        desc=k.opt('--desc',1)

        if k.opt('--save_file') is not None:
            sql_msg = "select msg,binfile from memory where name='{0}'".format(sql.convert_put(k.var(title,'str')))
            cur = db.cursor()
            cur.execute(sql_msg)
            get_bin_body=cur.fetchone()
            if get_bin_body[0] is None:
                print('"{0}" has no attached file'.format(title))
            else:
                with open('/tmp/{0}'.format(get_bin_body[0]),'wb') as f:
                    f.write(get_bin_body[1])
                print('Save binary file to /tmp/{0}'.format(get_bin_body[0]))
            k.exit()

        if title is None or ( body_file is None and bin_file is None and body_msg is None):
            print(self_cmd_name + ' remember <title> <--msg <message>> or <--file <text file>> or <--arch <binary file>>')
            k.exit()

        body_msg_str=''
        if body_msg is not None:
            body_msg_str=body_msg[0]

        if body_file is not None:
            if k.isfile(body_file[0]) is True:
                file = open(body_file[0],'r')
                if body_msg_str != '':
                    body_msg_str = body_msg_str + "-------------" +  file.read()
                else:
                    body_msg_str = file.read()

        if bin_file is not None:
            if k.isfile(bin_file[0]) is True:
                file = open(bin_file[0],'rb')
                bin_body = file.read()

        num=sql.find(db,table_name,'/'+k.var(title,'str'))
        if cmd == 'rememory' or cmd == 'reremember' and len(num) == 1:
            update_str=''
            if body_msg_str != '':
                update_str="value='{0}'".format(sql.convert_put(body_msg_str))
            if desc is not None:
                if update_str == '':
                    update_str="msg='{0}'".format(sql.convert_put(desc[0]))
                else:
                    update_str=update_str+", msg='{0}'".format(sql.convert_put(desc[0]))
            if bin_file is not None:
                if update_str == '':
                    update_str=" msg='{0}', binfile=?".format(bin_file[0])
                else:
                    update_str=update_str+", msg='{0}', binfile=?".format(bin_file[0])
                sql.update2(db,table_name,num,update_str=update_str,bin_body=[buffer(bin_body)])
            else:
                sql.update2(db,table_name,num,update_str=update_str)
            print('Updated {0}'.format(title))
        else:
            rc=sql.put(db,'memory','/'+sql.convert_put(title),sql.convert_put(body_msg_str))
            if rc is None:
                print('Saved {0}'.format(title))
            else:
                print('Found same samething')

        db.close()
        k.exit()

    elif cmd == 'update' or cmd == 'change' or cmd == 'replace' or cmd == 'save' or cmd == 'add':
        _help('<task> or <shell> or <host>',cmd=cmd)
        sys.argv.pop(1)
#        input_mx=len(sys.argv)
        table_name=sys.argv[1]

        if table_name == 'host':
            hostname=None
            if input_mx > 2:
                hostname=sys.argv[2]
            if input_mx < 2 or hostname is None or sys.argv[2] == '--help':
                print('{0} {1} {2} <hostname> --ip <ipaddr> [--desc <desc>] [ --keyfile <ssh keyfile>]'.format(self_cmd_name,cmd,table_name))
                k.exit()

            ipaddr=k.opt('--ip',1)
            desc=k.opt('--desc',1)
            keyfile=k.opt('--keyfile',1)
            if keyfile is not None:
                key_body=''
                if k.isfile(keyfile[0]) is True:
                    file = open(keyfile[0],'r')
                    key_body = file.read()
                    key_body = sql.convert_put(k.var(key_body,'str'))
                else:
                    print("{0} file not found".format(k.var(keyfile,'str')))
                    k.exit()
            else:
                key_body=''

            num=sql.find(db,'host','/'+k.var(hostname,'str'))
            if ( cmd == 'update' or cmd == 'change' or cmd == 'replace' ) and len(num) == 1:
                update_str=''
                if ipaddr is not None:
                    if update_str == '':
                        update_str="value='{0}'".format(ipaddr[0])
                    else:
                        update_str=update_str + ", value='{0}'".format(ipaddr[0])
                if desc is not None:
                    if update_str == '':
                        update_str="msg='{0}'".format(sql.convert_put(k.var(desc,'str')))
                    else:
                        update_str=update_str + ", msg='{0}'".format(sql.convert_put(k.var(desc,'str')))
                if keyfile is not None:
                    if update_str == '':
                        update_str="key='{0}'".format(sql.convert_put(k.var(key_body,'str')))
                    else:
                        update_str=update_str + ", key='{0}'".format(sql.convert_put(k.var(key_body,'str')))

                if input_mx < 2 or hostname is None or sys.argv[2] == '--help' or update_str == '':
                    print('{0} {1} {2} <hostname> <--ip <ipaddr> or --desc <desc> or --keyfile <ssh keyfile>>'.format(self_cmd_name,cmd,table_name))
                    k.exit()

                sql.update2(db,table_name,num,update_str=update_str)
                print('Updated {0}'.format(k.var(hostname,'str')))
            else:
                if input_mx < 3 or ipaddr is None or sys.argv[2] == '--help':
                    print('{0} {1} {2} <hostname> --ip <ipaddr> [--desc <desc>] [ --keyfile <ssh keyfile>]'.format(self_cmd_name,cmd,table_name))
                    k.exit()

                if len(num) > 0 :
                    print('Found samething')
                    k.exit()
                else:
                    idx = k.now(1)
                    if desc is None:
                        desc=''

                    sql_msg = "insert into host (idx,name,value,msg,cnt,key) values({0},'{1}','{2}','{3}',{4},'{5}')".format(idx,k.var(hostname,'str'),k.var(ipaddr,'str'),sql.convert_put(k.var(desc,'str')),'0',sql.convert_put(k.var(key_body,'str')))
                    cur = db.cursor()
                    cur.execute(sql_msg)
                    db.commit()
                    db.close()
                    print('Saved {0}'.format(k.var(hostname,'str')))
        elif table_name == 'shell':
            shell_name=None
            if input_mx > 2:
                shell_name=sys.argv[2]
            if input_mx < 3 or sys.argv[2] == '--help' or shell_name is None:
                print('{0} {1} {2} <shell name> <--msg <shell body>> or <--file <shell file>>'.format(self_cmd_name,cmd,table_name))
                k.exit()

            #db = sql.init_table(home_path+'/cep.db',table_name,msg='name text not null, value text, msg text, cnt int default 0')
            num=sql.find(db,table_name,'/'+k.var(shell_name,'str'))

            shell_body_file=k.opt('--file',1)
            shell_body=k.opt('--msg',1)
            desc=k.opt('--desc',1)

            if shell_body is None and shell_body_file is None:
                print(self_cmd_name + ' save shell <shell name> <--msg <shell body>> or <--file <shell file>>')
                k.exit()

            if shell_body_file is not None:
                if k.isfile(shell_body_file[0]) is True:
                    file = open(shell_body_file[0],'r')
                    shell_body = file.read()

            if ( cmd == 'update' or cmd == 'change' or cmd == 'replace' ) and len(num) == 1:
                sql.update2(db,table_name,num,update_str="value='{0}'".format(sql.convert_put(k.var(shell_body,'str'))))
            else:
                if len(num) > 0:
                    print('Found same samething')
                    k.exit()
                else:
                    rc=sql.put(db,table_name,'/'+k.var(shell_name,'str'),sql.convert_put(k.var(shell_body,'str')))
                    if rc is None:
                        print('Saved {0}'.format(k.var(shell_name,'str')))
                    else:
                        print('Found same samething')
                    db.close()
        elif table_name == 'task' or table_name == 'remind':
            table_name='task'
            title=None
            if input_mx > 2:
                title=sys.argv[2]
            if input_mx < 3 or title is None or sys.argv[2] == '--help':
                print('{0} {1} {2} <title> [-p 1] [--msg <msg> or --file <filename>] [ -d <due date:MM/DD/YYYY or MM/DD/YYYY HH:MIN>]'.format(self_cmd_name,cmd,table_name))
                k.exit()

            pri=k.opt('-p',1)
            if pri is None: pri=0
            msg=k.opt('--msg',1)
            if msg is None: 
                msg='' 
            else: 
                msg=msg[0]
            msg_file=k.opt('--file',1)
            if msg_file is not None:
                if k.isfile(msg_file[0]) is True:
                    file = open(msg_file[0],'r')
                    msg = file.read()
            due=k.opt('-d',1)
            if due is None:
                due=0
            else:
                #due=k.convert_date2sec(due[0])
                if len(due[0].split(' '))==2:
                   due=k.convert_datetime2sec(due[0],format="%m/%d/%Y %H:%M")
                else:
                   due=k.convert_datetime2sec(due[0],format="%m/%d/%Y")
            done=0

            #db = sql.init_table(home_path+'/cep.db',table_name,msg='title text not null, pri int, msg text, due int, done int default 0')

            num=sql.find(db,table_name,'/'+k.var(title,'str'))
            if ( cmd == 'update' or cmd == 'change' or cmd == 'replace' ) and len(num) == 1:
                update_str=''
                if pri != 0:
                   if update_str == '':
                       update_str=' pri={0}'.format(pri)
                   else:
                       update_str=update_str + ', pri={0}'.format(pri)
                if msg == '':
                   print('update should be need message(or file) for update message')
                   k.exit()
                else:
                   if update_str == '':
                       update_str=" msg='{0}'".format(sql.convert_put(k.var(msg,'str')))
                   else:
                       update_str=update_str + ", msg='{0}'".format(sql.convert_put(k.var(msg,'str')))
                if due != '':
                   if update_str == '':
                       update_str=" due={0}".format(due)
                   else:
                       update_str=update_str + ", due={0}".format(due)

                sql.update2(db,table_name,num,update_str=update_str)
            else:
                if len(num) > 0:
                    print('Found samething')
                    k.exit()
                else:
                    idx = k.now(1)
                    sql_msg = "insert into task (idx,title,pri,msg,due,done) values({0},'{1}',{2},'{3}',{4},{5})".format(idx,k.var(title,'str'),pri,sql.convert_put(k.var(msg,'str')),due,done)
                    cur = db.cursor()
                    cur.execute(sql_msg)
                    db.commit()
        else:
            print('Unknown ' + table_name  + ' name')
            print(self_cmd_name + ' ' + cmd + ' <task> or <shell> or <host> ')
        k.exit()

    elif cmd == 'del' or cmd == 'delete' or cmd == 'forget' or cmd == 'trash':
        arg=tuple(sys.argv[2:])
        forget(*arg)
        k.exit()

    elif cmd == 'show' or cmd == 'look' or cmd == 'what':
        _help('tables\n{0} <table name>\n{1} <table name> <title>\n{2} <table name> idx=<idx>\n{3} task all            : show all task with done\n{4} task idx=<idx> done : set the task to done'.format(cmd,cmd,cmd,cmd,cmd),cmd=cmd)

        sys.argv.pop(1)
        # Save to SQL
        #if len(sys.argv) >= 2:
        if input_mx >= 2:
            table_name=sys.argv[1]
            if table_name != 'task':
                task_chk(db)
            if table_name == 'tables':
                for ii in list(sql.get_table_name(db)):
                    print(ii)
            elif table_name == 'task':
                if k.opt('--help',1) is not None:
                    print(self_cmd_name + ' show task                : show all task without done')
                    print(self_cmd_name + ' show task all            : show all task with done')
                    print(self_cmd_name + ' show task <title>        : show task info')
                    print(self_cmd_name + ' show task idx=<idx>      : show task info')
                    print(self_cmd_name + ' show task idx=<idx> done : set the task to done')
                    k.exit()

                #if len(sys.argv) > 2:
                if input_mx > 2:
                    task_title=sys.argv[2]
                    task_idx_arr=sys.argv[2].split('=')
                    if task_idx_arr[0] == 'idx':
                        task_idx=task_idx_arr[1]
                    else:
                        task_idx=0
                else:
                    task_title=None
                if task_title == 'all':
                    for ii in list(sql.get(db,table_name,'/',field='title,due,done', order=' ORDER BY pri, due')):
                        if ii[1] > 0:
                           due_date=time.strftime('%Y-%m-%d', time.localtime(int(ii[1])))
                           due_time=time.strftime('%H:%M:%S', time.localtime(int(ii[1])))
                        else:
                           due_date=''
                           due_time=''
                        if ii[2] > 0:
                           done_date=time.strftime('%Y-%m-%d', time.localtime(int(ii[2])))
                           done_time=time.strftime('%H:%M:%S', time.localtime(int(ii[2])))
                        else:
                           done_date=''
                           done_time=''
                        
                        if due_time == '00:00:00': due_time=''
                        if done_time == '00:00:00': done_time=''
                        shell_body_idx=sql.get(db,table_name,'value',condition="title='"+k.var(ii[0],'str')+"'",field='idx')
                        print(k.var("%f"%(shell_body_idx[0][0]),'str') + ' : '+k.var(ii[0],'str') + ' : ' + due_date + ' ' + due_time +' : '+ done_date + ' '+done_time)
                elif task_title is not None:
                    if task_idx != 0:
                        #if len(sys.argv) > 3:
                        if input_mx > 3:
                            task_done=sys.argv[3]
                            if task_done == 'done':
                                sql_msg = 'update task set done='+k.var(k.now(),'str') + ' where idx='+task_idx
                                cur = db.cursor()
                                cur.execute(sql_msg)
                                db.commit()
                                k.exit()
                        
                        sql_msg = 'select * from task where idx="'+task_idx+'"'
                    else:
                        sql_msg = 'select * from task where title="'+sql.convert_put(k.var(task_title,'str'))+'"'
                    cur = db.cursor()
                    cur.execute(sql_msg)
                    get_task=cur.fetchall()
                    if len(get_task) == 0 or len(get_task[0]) != 6:
                        print(task_title + ' not found')
                        k.exit()
                    print('------------------------------------------------------')
                    print('Task  : ' + sql.convert_get(get_task[0][1]))
                    print('------------------------------------------------------')
                    print('Added : ' + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(get_task[0][0])))
                    if get_task[0][4] > 0:
                        print('Due   : ' + time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(get_task[0][4]))))
                    else:
                        print('Due   : -')
                    if get_task[0][5] > 0:
                        done_date=time.strftime('%Y-%m-%d', time.localtime(int(get_task[0][5])))
                        done_time=time.strftime('%H:%M:%S', time.localtime(int(get_task[0][5])))
                        print('Done  : ' + done_date + ' ' + done_time)
                    else:
                        print('Done  : waiting...')
                    print('Pri   : ' + k.var(get_task[0][2],'str'))
                    print('idx   : ' + k.var("%f"%(get_task[0][0]),'str'))
                    print('------------------------------------------------------')
                    print(sql.convert_get(get_task[0][3]))
                    k.exit()
                else:
                    for ii in list(sql.get(db,table_name,'',field='title,due', order=' ORDER BY pri, due', condition=" done==0")):
                        if ii[1] > 0:
                           due_date=time.strftime('%Y-%m-%d', time.localtime(int(ii[1])))
                           due_time=time.strftime('%H:%M:%S', time.localtime(int(ii[1])))
                        else:
                           due_date=''
                           due_time=''
                        
                        if due_time == '00:00:00': due_time=''
                        shell_body_idx=sql.get(db,table_name,'value',condition="title='"+k.var(ii[0],'str')+"'",field='idx')
                        print(k.var("%f"%(shell_body_idx[0][0]),'str') + ' : '+k.var(ii[0],'str') + ' : ' + due_date + ' ' + due_time)
            elif table_name == 'host':
                for ii in list(sql.get(db,table_name,'/',field='name,value,msg',order=' ORDER by CAST(cnt as integer) ASC')):
                    print(ii[1] + ' '+sql.convert_get(ii[0]) + ' : ' + sql.convert_get(ii[2]))
            #elif table_name == 'memory' and len(sys.argv) == 2:
            elif table_name == 'memory' and input_mx == 2:
                for ii in list(sql.get(db,table_name,'/',field='idx,name,msg',order=' ORDER by CAST(cnt as integer) ASC')):
                    if ii[2] == '':
                        print('{0} : {1}'.format(k.var("%f"%(ii[0]),'str'),ii[1]))
                    else:
                        print('{0} : {1} (attached: {2})'.format(k.var("%f"%(ii[0]),'str'),ii[1],ii[2]))
            else:
                if not table_name in sql.get_table_name(db):
                    print('"{0}" is wrong table name'.format(table_name))
                    k.exit()

                #if len(sys.argv) == 3:
                if input_mx == 3:
                    shell_title=sys.argv[2]
                    if shell_title == '--help':
                        print(self_cmd_name + ' show ' + table_name + ' <title> or <idx=<idx>>')
                    else:
                        idx_find=shell_title.split('=')
                        if idx_find[0] == 'idx':
                            print(isinstance(idx_find[1],int),isinstance(idx_find[1],float))
                            #if idx_find[1].isdigit():
                            if float(idx_find[1]) < 0 and int(idx_find[1]) < 0:
                                print('idx({0}) is not digital number'.format(idx_find[1]))
                                k.exit()
                            else:
                                shell_body=sql.get(db,table_name,'value',condition='idx='+k.var(idx_find[1],'str'),field='value,cnt,msg,name')
                            shell_body_idx_2=k.var(idx_find[1],'str')
                            shell_title=shell_body[0][3]
                        else:
                            shell_body=sql.get(db,table_name,'/'+k.var(shell_title,'str'),field='value,cnt,msg')
                            shell_body_idx=sql.get(db,table_name,'/'+k.var(shell_title,'str'),field='idx')
                            if len(shell_body_idx) == 0:
                               print("'"+k.var(shell_title,'str') + "' not found")
                               k.exit()
                            shell_body_idx_2=k.var("%f"%(shell_body_idx[0][0]),'str')
                        if shell_body is None or len(shell_body) == 0:
                            print('"'+k.var(shell_title,'str')+'"' + ' not found in ' + table_name)
                        else:
                            print("---------------------------------------------")
                            print("title: {0}".format(sql.convert_get(shell_title)))
                            print("---------------------------------------------")
                            print("index: {0}".format(shell_body_idx_2))
                            print("count: {0}".format(shell_body[0][1]))
                            #print("Note : {0}".format(shell_body[0][2]))
                            print("Note : {0}".format(sql.convert_get(shell_body[0][2])))
                            print("---------------------------------------------")
                            print(sql.convert_get(shell_body[0][0]))
                            sql.counting(db,table_name,shell_body_idx_2)

                else:
                    get_data=sql.get(db,table_name,'/',field='name',order=' ORDER by CAST(cnt as integer) ASC')
                    if get_data is None:
                        print(table_name + ' not found')
                        k.exit()
                    for ii in list(get_data):
                        shell_body_idx=sql.get(db,table_name,'/'+k.var(ii[0],'str'),field='idx')
                        print(k.var("%f"%(shell_body_idx[0][0]),'str') + ' '+ii[0])
        else:
            print(self_cmd_name + ' show tables              : show table list')
            print(self_cmd_name + ' show <table name>        : show all list in the <table name>')
            print(self_cmd_name + ' show <table name> <title>: show detail info')
            print(self_cmd_name + ' show <table name> idx=<idx>')
            print(self_cmd_name + ' show task all            : show all task with done')
            print(self_cmd_name + ' show task idx=<idx> done : set the task to done')
        k.exit()
    elif cmd == 'cleanup' or cmd == 'clean':
        _help(' : cleanup garbage',cmd=cmd,opt=0)
        sys.argv.pop(1)
        cur = db.cursor()
        #if len(sys.argv) >= 2:
        if input_mx >= 2:
            if sys.argv[1] == 'trash':
                del_idx = k.now(1) - ( 3600 * 24 * 30)
                del_condition = ' where idx < ' + k.var(del_idx,'str')
                sql_msg = 'delete from trash'
                if input_mx > 2 and sys.argv[2] == 'all':
                    cur.execute(sql_msg)
                else:
                    cur.execute(sql_msg + del_condition)
                db.commit()
        cur.execute("VACUUM")
        cur.close()
        k.exit()

    elif cmd == 'fp':
        _help('add|cleanup|update|flush|info|dup',cmd=cmd)
        sys.argv.pop(0)
        sys.argv.pop(0)
        if sys.argv[0] == 'add':
           _help('[-vv] [-md5] <file|dir> [...]\n\t-vv : verbos mode\n\t-md5 : check md5 (not found: add, found: ignore)\n\t <default> : same file then ignore, others then add', cmd=sys.argv[0])
#           if len(sys.argv) == 1:
#              k.exit()
           sys.argv.pop(0)
           vv_mode=0
           md5_mode=None
           if sys.argv[0] == '-vv':
              sys.argv.pop(0)
              vv_mode=1
           if sys.argv[0] == '-md5':
              sys.argv.pop(0)
              md5_mode='md5'
           for put_item in list(sys.argv):
              if os.path.isdir(os.path.abspath(put_item)) is True:
                  for dirname, dirnames, filenames in os.walk(put_item):
                      for filename in filenames:
                         file_name=os.path.join(dirname,filename)
                         rc=k.fp(cmd='put',file_name=file_name,kfpdb=kfpdb,chk=md5_mode)
                         if vv_mode == 1:
                            if rc is False:
                               print('{0} is alread got finger print'.format(file_name))
              else:
                 rc=k.fp(cmd='put',file_name=put_item,kfpdb=kfpdb,chk=md5_mode)
                 if vv_mode == 1:
                    if rc is False:
                       print('{0} is alread got finger print'.format(put_item))


        elif sys.argv[0] == 'info':
           _help('''[--all] --size <filesize> | --md5 <md5 str>[...] | [--find] <dest> [...]\n\t --find : find a string in file field\n\t<default> : find correct file's infomation''',cmd=sys.argv[0])
           #if len(sys.argv) == 1:
           #   k.exit()
           sys.argv.pop(0)
           all_find='find'
           find_string=None
           if sys.argv[0] == '--all':
              all_find='find_all'
              sys.argv.pop(0)

           if len(sys.argv) > 0 and sys.argv[0] == '--size':
              #k.fp(kfpdb=kfpdb,cmd='find',fsize=sys.argv[1])
              k.fp(kfpdb=kfpdb,cmd=all_find,fsize=sys.argv[1])
           elif len(sys.argv) > 0 and sys.argv[0] == '--md5':
              for fp_str in list(sys.argv[1:]):
                 k.fp(kfpdb=kfpdb,cmd=all_find,md5=fp_str)
                 #k.fp(kfpdb=kfpdb,cmd='md5',md5=fp_str)
           elif len(sys.argv) > 0 and sys.argv[0] == '--find':
              for fp_str in list(sys.argv[1:]):
                 k.fp(kfpdb=kfpdb,cmd=all_find,find_str=fp_str)
           else:
              for ii in list(sys.argv[0:]):
                 if os.path.isdir(ii) is True:
                    for dirname, dirnames, filenames in os.walk(ii):
                       for filename in filenames:
                          #k.fp(kfpdb=kfpdb,cmd='find',file_name=os.path.join(dirname,filename))
                          k.fp(kfpdb=kfpdb,cmd=all_find,file_name=os.path.join(dirname,filename))
             
                 elif os.path.isfile(ii) is True:
                     #k.fp(kfpdb=kfpdb,cmd='find',file_name=ii)
                     k.fp(kfpdb=kfpdb,cmd=all_find,file_name=ii)
                 else:
                    print(ii + ' not found in DB')
    
        elif sys.argv[0] == 'dup':
           _help('[--type <file type> | --size <filesize>]',cmd=sys.argv[0],opt=0)
           sys.argv.pop(0)
           if len(sys.argv) > 1 and sys.argv[1] == '--type':
              fp_type=sys.argv[2]
              k.fp(kfpdb=kfpdb,cmd='fdfp',ftype=fp_type)
           elif len(sys.argv) > 1 and sys.argv[1] == '--size':
              fp_size=sys.argv[2]
              k.fp(kfpdb=kfpdb,cmd='fdfp',fsize=fp_size)
           else:
              k.fp(kfpdb=kfpdb,cmd='fdfp')

        elif sys.argv[0] == 'cleanup':
           _help('[--size <filesize> | --md5 <md5 str>[...] | <dest path> [...]]',cmd=sys.argv[0],opt=0)
           sys.argv.pop(0)
           if len(sys.argv) == 0:
              k.fp(kfpdb=kfpdb,cmd='clean')
           elif len(sys.argv) > 0 and sys.argv[0] == '--size':
              k.fp(kfpdb=kfpdb,cmd='clean',fsize=sys.argv[1])
           elif len(sys.argv) > 0 and sys.argv[0] == '--md5':
              for fp_str in list(sys.argv[0:]):
                 k.fp(kfpdb=kfpdb,cmd='clean',md5=fp_str)
           else:
              for ii in list(sys.argv[0:]):
                 if os.path.isdir(ii) is True:
                    for dirname, dirnames, filenames in os.walk(ii):
                       for filename in filenames:
                          k.fp(kfpdb=kfpdb,cmd='clean',file_name=os.path.join(dirname,filename))

                 elif os.path.isfile(ii) is True:
                    k.fp(kfpdb=kfpdb,cmd='clean',file_name=ii)
                 else:
                    print(ii + ' not found')
    
        elif sys.argv[0] == 'update':
           _help('[--size <filesize> | --md5 <md5 str>[...] | <dest path> [...]]',cmd=sys.argv[0],opt=0)
           sys.argv.pop(0)
           if len(sys.argv) == 0:
              k.fp(kfpdb=kfpdb,cmd='update')
           elif len(sys.argv) > 0 and sys.argv[0] == '--size':
              k.fp(kfpdb=kfpdb,cmd='update',fsize=sys.argv[1])
           elif len(sys.argv) > 0 and sys.argv[0] == '--md5':
              for fp_str in list(sys.argv[0:]):
                 k.fp(kfpdb=kfpdb,cmd='update',md5=fp_str)
           else:
              for ii in list(sys.argv[0:]):
                 if os.path.isdir(ii) is True:
                    for dirname, dirnames, filenames in os.walk(ii):
                       for filename in filenames:
                          k.fp(kfpdb=kfpdb,cmd='update',file_name=os.path.join(dirname,filename))

                 elif os.path.isfile(ii) is True:
                    k.fp(kfpdb=kfpdb,cmd='update',file_name=ii)
                 else:
                    print(ii + ' not found')
        elif sys.argv[0] == 'flush':
           _help(' : Flush DB',cmd=sys.argv[0],opt=0)
           k.fp(kfpdb=kfpdb,cmd='flush')

    elif cmd == 'cp':
        task_chk(db)
        _help('<src> [...] <dest>',cmd=cmd)
        sys.argv.pop(0)
        sys.argv.pop(0)
        nargc=len(sys.argv)
        k.cp(sys.argv[0:nargc-1],sys.argv[nargc-1],kfpdb=kfpdb)

    elif cmd == 'mv':
        task_chk(db)
        _help('<src> [...] <dest>',cmd=cmd)
        sys.argv.pop(0)
        sys.argv.pop(0)
        nargc=len(sys.argv)
        k.mv(sys.argv[0:nargc-1],sys.argv[nargc-1],kfpdb=kfpdb)

    elif cmd == 'rm':
        task_chk(db)
        _help('[-fr] <file> [...]',cmd=cmd)
        force=0
        sys.argv.pop(0)
        sys.argv.pop(0)
        if len(sys.argv) > 0 and sys.argv[0] == '-fr':
            force=3
            sys.argv.pop(0)

        k.rm(sys.argv,force=force,kfpdb=kfpdb)

    elif cmd == 'mkp':
        _help('<file>',cmd=cmd)
        ffname=sys.argv[len(sys.argv)-1]
        _mkp(ffname)

    elif cmd == 'vi' or cmd == 'vim':
        _help('<file>',cmd=cmd)
        sys.argv.pop(1)
        vimrc=k.CMD().mktemp(filename='/tmp/vim.',uniq=1)
        vimrcfp=open(vimrc,"w")
        vimrcfp.write("set title\n")
        vimrcfp.write("set laststatus=2\n")
        vimrcfp.write("set tabstop=4\n")
        vimrcfp.write("set expandtab\n")
        vimrcfp.write("set shiftwidth=4\n")
        vimrcfp.write("set softtabstop=4\n")
        vimrcfp.write("set visualbell\n")
        vimrcfp.write("set ruler\n")
        vimrcfp.write("syntax on")
        vimrcfp.close()
        ffname=sys.argv[len(sys.argv)-1]
        fname=os.path.basename(ffname)
        fpath=os.path.dirname(ffname)
        st = os.stat(ffname)
        if fpath == '':
           fpath='.'
        bvi_md5=md5(ffname)

        f = open(ffname, 'r')
        fdata=f.read()
        match = re.search(r'''_k_v_version="\d*.\d*.\d*.\d*"''', fdata)
        f.close()
        # if found version info then backup old version
        if match:
           match_old_ver=match.group().split('"')[1]
           match_bak_ignore=0
           if os.path.isdir(fpath+'/.bak') is False:
              try:
                 os.mkdir(fpath+'/.bak')
              except:
                 match_bak_ignore=1
           if match_bak_ignore == 0:
              k.CMD().cp_file(ffname,fpath+'/.bak/'+fname+'.'+match_old_ver,mode='copy')

        os.system('TERM=linux vim -u ' + vimrc + ' ' + k.var(sys.argv[1:],'str'))
        os.remove(vimrc)
        avi_md5=md5(ffname)
        if bvi_md5 != avi_md5:
           if match:
              f = open(ffname, 'r')
              fdata=f.read()
              f.close()

              #new_version=k.version_up(match.group().split('"')[1])
              new_version=k.version_up(match_old_ver)
              newdata = fdata.replace(match.group(),'''_k_v_version="{0}"'''.format(new_version))
              f = open(fpath+'/.'+fname+'.uv','w') 
              f.write(newdata)
              f.close()
              k.CMD().cp_file(fpath+'/.'+fname+'.uv',ffname,mode='copy')
              k._copystat(dest=ffname,st=st)
        k.exit()
    else:
        task_chk(db)
        # module type command
        if k.isfile(home_path+'/'+sys.argv[1]+'.py') or k.isfile(home_path+'/'+sys.argv[1]+'.pyc'):
            cep.imports(sys.argv[1])
            aa=sys.argv[1]+'.'+sys.argv[1]+'()'
            sys.argv.pop(0)
            eval(aa)
            k.exit()

        # Call library function
        self_mod=cep.self()
        if cep.CODE().code_check(cep.METHOD().is_method_in_class(self_mod,sys.argv[1]),True):
            arg=tuple(sys.argv[2:])
            rc=cep.METHOD().run_function_name_argv(self_mod,sys.argv[1],*arg)
            if type(rc) == set:
                k.exit(code=rc)
            else:
                if rc is not None:
                   print(rc)
            k.exit()
        
        num=sql.find(db,'shell','/'+k.var(sys.argv[1],'str'))
        if len(num) > 0:
            shell_lib=sql.get(db,'shell','/k')[0]
            shell_body=sql.get(db,'shell','/'+k.var(sys.argv[1],'str'),field='value')
            aa=sql.convert_get(shell_body[0][0])
            shell_cmd = 'export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin \n'
            shell_cmd = shell_cmd + 'export SHELL=/bin/bash \n'
            # Base library (k.k)
            shell_cmd=shell_cmd + sql.convert_get(shell_lib[0]) + '\n'
            # Load required shell library
            lib_body=k.load_shell_lib(db,aa)
            for ii in range(0,20):
                if len(lib_body) == 0:
                    break
                else:
                    shell_cmd=shell_cmd + lib_body[0]
                    lib_body=k.load_shell_lib(db,lib_body[0],lib_body[1])

            shell_cmd=shell_cmd + aa + '\n'
            if len(sys.argv[2:]) == 0:
                shell_cmd=shell_cmd + '_k_'+sys.argv[1]
            else:
                shell_cmd=shell_cmd + '_k_'+sys.argv[1] + ' ' + cep.var(sys.argv[2:],'str')
            subprocess.call(['bash','-c',shell_cmd])
            k.exit()

        # Help
        vhelp=k.opt('--help',1)
        if vhelp is not None:
            if len(vhelp) > 0:
                print(k.var(vhelp,'str')+':')
                try:
                   print(k.help(eval(cep.var(vhelp,'str'))))
                except:
                   print(' Not found')
            else:
                _help()
        # Version
        if k.opt('-v') is not None or k.opt('--version') is not None:
            print(version)
            k.exit()

        # Text character view
        vopt=k.opt('-t',1)
        if vopt is not None:
           #a=k.convert_char(vopt[0].encode('utf-8'))
           a=k.convert_char(vopt[0])
           print(a)
           k.exit()

        # Build
        vopt=k.opt('-b',1)
        if vopt is None:
            vopt=k.opt('--build',1)
           
        if vopt is not None:
            mopt=None
            if k.opt('-m') is not None:
                mopt='-m'
            elif k.opt('--module') is not None:
                mopt='-m'
            k.CMD().compile(k.var(vopt,'str'),mopt)
            k.exit()

        # Make a hostname
        make_host=k.opt('-h',1)
        if make_host is not None:
            for ii in list(k.braket(cep.var(make_host,'str'))):
                print(ii)
            k.exit()

        # Other commands of current system
        #print('oth cmd')
        sys.argv.pop(0)
        subprocess.call(['bash','-c',cep.var(sys.argv,'str')])
