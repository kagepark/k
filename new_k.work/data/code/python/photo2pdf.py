#This is a simple Pythonista script to convert camera images into 1 PDF Doc.

from PIL import Image, ImageEnhance
import photos
from PyPDF2 import PdfFileWriter, PdfFileReader

pdfname = 'Page0.pdf'
pagename = []
pagecnt = 0
while True:
	print('Use Last Image?')
	qu = input('Use last img(l);Use camera(c);Save(s);Quit(q)')
	if(qu=='l'):
		all_assets = photos.get_assets()
		last_asset = all_assets[-1]
		img = last_asset.get_image()
		img.show()
		img.save(pdfname,'PDF')
		pagename.append(pdfname)
		pagecnt=pagecnt+1
		pdfname = 'Page'+str(pagecnt)+'.pdf'
	if(qu=='c'):
		img = photos.capture_image()
		img4 = ImageEnhance.Contrast(img).enhance(4)
		img4 = ImageEnhance.Color(img4).enhance(0)
		img4.show()
		img4.convert('RGB').save(pdfname,'PDF')
		pagename.append(pdfname)
		pagecnt=pagecnt+1
		pdfname = 'Page'+str(pagecnt)+'.pdf'
		#img.rotate(45).show()
	if(qu=='s'):
		output = PdfFileWriter()
		for pname in pagename:
			theinput = PdfFileReader(open(pname, "rb"))
			output.addPage(theinput.getPage(0))
		outputStream = open("output.pdf", "wb")
		output.write(outputStream)
		print ('done')
	if(qu=='q'):
		break
