import os,sys
import requests, requests.utils, pickle

def post_url(url,user=None,password=None,options=None,token='csrftoken',session_file='/tmp/abc'):
    if user is not None and password is not None and os.path.isfile(session_file):
        os.unlink(session_file)
    if user is None and password is None and os.path.isfile(session_file):
        with open(session_file) as f:
            ss = pickle.load(f)
        csrf_token=ss.cookies[token]
        data={'csrfmiddlewaretoken':csrf_token}
        if options is not None and len(options) > 0:
            data.update(options)
        r = ss.post(url, data=data, verify=False)
    else:
        # Login
        ss = requests.Session()
        ss.stream = False
        print('11111')
#        try:
        ss.get(url,verify=False)
#        ss.get(url)
        print('22222',url)
#        except:
#           print('aaa')
#           return 

        csrf_token=ss.cookies[token]
        data={'csrfmiddlewaretoken':csrf_token,'username': user, 'password':password}
        if options is not None and len(options) > 0:
            data.update(options)
        headers={"X-CSRFToken": csrf_token}
        try:
           r = ss.post(url, data=data, headers=headers, verify=False)
           print(r.status_code)
           with open(session_file,'wb') as f:
               session = pickle.dump(ss,f)
        except:
           print('bbbb')
           return
    return r

#Login
a=post_url('http://kage.cep.kr:7990/accounts/login/',user='kage',password='K@ge9ark!')
print(a)
print('{0}'.format(a.text))
# get data
#print(sys.argv)
#print(post_url('http://kage.cep.kr:7990/list/').text)
