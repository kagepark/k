def split_page(list_item,page,num):
    page=int(page)
    num=int(num)
    last_page=int(len(list_item)/num)
    first_page=1
    if page <= 1:
        previous_page=''
        next_page=page+1
        now_page=1
        page_data=list_item[:num]
    elif page == last_page:
        previous_page=page-1
        next_page=''
        now_page=page
        page_data=list_item[(last_page-1)*num:]
    else:
        previous_page=page-1
        next_page=page+1
        now_page=page
        page_data=list_item[(page-1)*num:(page*num)]

    return {'data':page_data,'firs_page':first_page,'last_page':last_page,'previous_page':previous_page,'next_page':next_page,'now_page':now_page}

