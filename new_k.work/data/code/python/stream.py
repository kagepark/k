import sys

import select

def list2str(ll):
    tt=None
    for t in ll:
       if tt is None:
           tt='{0}'.format(t)
       else:
           tt='{0} {1}'.format(tt,t)
    return tt

def rsc(line):
    ll=line.split(' ')
    return {'NAME': ll[0], 'SLOT': ll[1].replace('SLOT',''), 'PCI-E': list2str(ll[3:])}

if select.select([sys.stdin,],[],[],0.0)[0]:
    #lines=sys.stdin.readlines()
    #for line in lines:
    #    aa=line.split('\n')[0]
    #    if len(aa) > 0:
    #        print(aa)
    data=[]
    for line in sys.stdin:
        ll=line.strip().split('\n')[0]
        if len(ll) > 0:
            data.append(ll)
#            print(ll)
    new_dic={}
    if len(data) > 0:
        name=data.pop(0)
        total=len(data)
        new_dic[name]={total:{}}
        for ii in range(0,total):
            new_dic[name][total].update({ii:rsc(data[ii])})
    import pprint
    pprint.pprint(new_dic)
else:
    print "No data"
