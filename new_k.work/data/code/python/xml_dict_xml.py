import xmltodict

xml = open('/tmp/aa1.cfg', "r")
org_xml = xml.read()
dict_xml = xmltodict.parse(org_xml, process_namespaces=True)

out = xmltodict.unparse(dict_xml, pretty=True)
with open("/tmp/bb1.cfg", 'w') as file:
    #file.write(out.encode('utf-8'))
    #file.write(out.decode('utf-8'))
    file.write(out)
