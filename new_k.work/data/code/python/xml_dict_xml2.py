from lxml import etree

def generateKey(element):
    if element.attrib:
        key = (element.tag, element.attrib)
    else:
	key = element.tag
    return key	

class parseXML(object):
    def __init__(self, xmlFile = '/tmp/aa1.cfg'):
        self.xmlFile = xmlFile
	
    def parse(self):
        doc = etree.parse(self.xmlFile)
	root = doc.getroot()
	key = generateKey(root)
	dictA = {}
	for r in root.getchildren():
	    keyR = generateKey(r)
	    if r.text:
	        dictA[keyR] = r.text
	    if r.getchildren():
	        dictA[keyR] = r.getchildren()
		
	newDict = {}
	newDict[key] = dictA
	return newDict
	        	
if __name__ == "__main__":
    px = parseXML(xmlFile='/tmp/aa1.cfg')
    newDict = px.parse()
    print newDict
