#!/bin/sh
# 2006       KGP Initial version
# 2010       KGP Tested more many switch and update few code
# 2011.07    KGP Convert to ace style format
# 2011.10    KGP Tested in APP and Ethernet only ACE
# 2015.10.16 KGP Tested in Ethernet Only ACE ( ACE 3.2 - 3328 ) (Add "ace put" command of Ethernet only ACE style format)
   #             Change name from snmp_get_mac.sh to ace_ethstatd
# 2015.10.20 KGP Add right connection check and working on sub-mgmt node.
#                 Add log rotate & example db format


interval=60
log_size=$((1024*1024)) # 1Mb 



# Example DB
#global {
#    ms_net1_device 'eth4'
#    identify_servers 2
#}
#
#ethswitches {
#    ethswitch-0001 {
#        id 1
#        subnet 1
#        ipaddr '10.10.0.200'
#        rack 0
#        slot 0
#        num_ports 68
#        ports {
#            1 { id=1 type='server' rate=1 dest_id=5 dest_port=1 }
#            2 { id=2 type='server' rate=1 dest_id=6 dest_port=1 }
#            67 { id=67 type='server' rate=1 dest_id=1 dest_port=1 }
#            68 { id=68 type='server' rate=1 dest_id=2 dest_port=1 }
#        }
#    }
#}
#servers {
#  server-0001 {
#      id 1
#      type management
#      group_id 1
#      peer_id 1
#      rack 1
#      nics 1
#      slot 14
#      nicports {
#          1 { id=1 nic=1 nic_port=5 dest_id=1 dest_port=67 }
#      }
#  }
#
# The port number is rail number
# 1 { : rail 1
# 2 { : rail 2
#
# The nic= just 1 in all system
# The nic_port='s number is NIC number (ms_net1_device's number)
#   eth0: 1
#   eth2: 3
#   eth4: 5



dbg=1
dbg() {
  [ "$dbg" ==  "1" ] && echo $*
}

if [ -d /acefs/ethswitches ]; then
    acefs=/acefs
    ace_bin=ace
    log_dir=/tmp
elif [ -d /.ace/acefs/ethswitches ]; then
    acefs=/.ace/acefs
    ace_bin=/.ace/bin/ace
    log_dir=/.ace/tmp
fi
eth_db=$acefs/ethswitches

[ -f $log_dir/ace_ethstatd.log ] && mv $log_dir/ace_ethstatd.log $log_dir/ace_ethstatd.log.1
[ -f $log_dir/ace_ethstatd.dbg ] && mv $log_dir/ace_ethstatd.dbg $log_dir/ace_ethstatd.dbg.1
exec >$log_dir/ace_ethstatd.log 2>&1
echo "Start $(basename $0) at $(date)" 

mac2int() {
    printf "%d" $1
}

log_rotate() {
    for ii in $log_dir/ace_ethstatd.dbg; do
        if [ -f $ii ]; then
            if (( $(stat -c "%s" $ii) > $log_size )); then
            (($dbg >= 3)) && echo "$ii: $(stat -c "%s" $ii) > $log_size"
               mv $ii ${ii}.1
               touch $ii
            fi
        fi
    done
}

# func get_mac
get_mac() {
  # mac : .1.3.6.1.2.1.17.4.3.1.1.<identify>
  local identify
  identify=$1
  if [ -n "$identify" ]; then
     mac=( $(snmpwalk -O n -v2c -c public $target .1.3.6.1.2.1.17.4.3.1.1.$identify) )
     if [ "${mac[2]}" == "Hex-STRING:" ]; then
        printf "%s:%s:%s:%s:%s:%s\n" ${mac[3]} ${mac[4]} ${mac[5]} ${mac[6]} ${mac[7]} ${mac[8]}
     else
        printf "%s\n" "unknown"
     fi
  else
     printf "%s\n" "unknown"
  fi
}

mac_format() {
   local mac_addr
   mac_addr=$1
   [ -n "$mac_addr" ] || return 1
   echo $(for ii in $(echo $mac_addr | sed "s/:/ /g"); do
      [ "$(echo $ii | wc -c)" == "2" ] && echo 0$ii || echo $ii
   done) | sed "s/ /:/g"
}

#Get switch's mac address
get_sw_mac_addr() {
  local target sw_mac_addr
  target=$1
  # snmpwalk -O n -v2c -c public 10.10.0.200 .1.3.6.1.2.1.2.2.1.6 : switch port's mac address
  sw_mac_addr=$(snmpwalk -O n -v2c -c public $target .1.3.6.1.2.1.2.2.1.6.1 | awk '{print $4}')
  [ -n "$sw_mac_addr" ] || return 1
  mac_format $sw_mac_addr
}

# func get_sw_port
get_sw_port_name() {
  # port name : .1.3.6.1.2.1.31.1.1.1.1.<port>
  local port
  port=$1
  [ -n "$port" ] || return 1
  snmpwalk -O n -v2c -c public $target .1.3.6.1.2.1.31.1.1.1.1.$port | awk '{print $4}'
}

get_sw_ports_status() {
   local target
   target=$1
   [ -n "$target" ] || return 1
   # port status : .1.3.6.1.2.1.2.2.1.8.<port>
   #snmpwalk -O n -v2c -c public $target .1.3.6.1.2.1.2.2.1.8 | grep up | awk '{print $1}' | cut -c 22-
   snmpwalk -O n -v2c -c public $target .1.3.6.1.2.1.2.2.1.8 | while read line; do
        if echo $line | grep down >& /dev/null ; then
            echo $line | awk '{print $1}' | cut -c 22- | awk '{printf "d.%s ",$1}'
        else
            echo $line | awk '{print $1}' | cut -c 22- | awk '{printf "u.%s ",$1}'
        fi
   done
}

get_sw_port_status() {
   # port status : .1.3.6.1.2.1.2.2.1.8.<port>
   local port
   port=$1
   [ -n "$port" ] || return 1
   snmpwalk -O n -v2c -c public $target .1.3.6.1.2.1.2.2.1.8.${port} | grep "up" >& /dev/null  && echo up || echo down
}

get_identify_from_sw_port() {
   # get port num from identify : .1.3.6.1.2.1.17.4.3.1.2.<identify>
   local port
   port=$1
   [ -n "$port" ] || return 1
   snmpwalk -O n -v2c -c public $target .1.3.6.1.2.1.17.4.3.1.2 | while read line; do
       echo $line | awk -v port=$port '{if($4==port) print $1}'
   done
}

put_state() {
   local object value
   object=$1
   value=$2
   [ ! -n "$object" -o ! -n "$value" ] && return 1
   if [ -f $acefs/${object} ]; then
       [ "$(cat $acefs/${object})" == "$value" ] && return 0
   fi
   [ -d "$(dirname $acefs/${object})" ] || return 0
   $ace_bin put ${object}=$value 
}

get_local_eth_mac() {
   local eth_dev
   eth_dev=$(cat $acefs/global/ms_net1_device)
   [ -f /sys/class/net/$eth_dev/address ] || return 1
   cat /sys/class/net/$eth_dev/address
}

get_mgmt_sw() {
   local mgmt_sw dest_id conn_id dest_port mgmt_sw_arr
   mgmt_sw=$(for ii in $(ls $acefs/self/server/nicports); do
       dest_port=$(cat $acefs/self/server/nicports/$ii/dest_port)
       dest_id=$(cat $acefs/self/server/nicports/$ii/dest_id)
       conn_id=$(cat $acefs/ethswitches/ethswitch-$(printf "%04d" $dest_id)/ports/$dest_port/server/id)
       if [ "$(cat $acefs/self/server/id)" == "$conn_id" ]; then
           sw_mgmt_mac=$(cat $acefs/ethswitches/ethswitch-$(printf "%04d" $dest_id)/ports/$dest_port/conn_guid0)
           if [ "$sw_mgmt_mac" != "0x0000000000000000" ]; then
               if [ "$(get_hexmac $sw_mgmt_mac)" == "$(hexmac $(get_local_eth_mac))" ]; then 
                   echo "$dest_id $dest_port"
                   break
               fi
           fi
       fi
   done)
   mgmt_sw_arr=($mgmt_sw)
   if [ "${#mgmt_sw_arr[*]}" == "2" ]; then
       #cat $acefs/ethswitches/ethswitch-$(printf "%04d" ${mgmt_sw_arr[0]})/ipaddr
       echo -n "ethswitch-$(printf "%04d" ${mgmt_sw_arr[0]}) "
   else
       echo "Not found right connected switch from mgmt"
       return 1
   fi
}

get_group_sw() {
    local grp_sw grp_id
    grp_id=$(cat $acefs/self/server/group_id)
    if [ "$grp_id" == "$(cat $(awk '{if($1=="13") print FILENAME}' $acefs/servers/server-000[1-2]/state | sed "s/\/state$/\/group_id/g"))" ]; then
        echo "This group is same as mgmt group. So, not running this group server"
        exit 2
    fi
    if [ "$(cat $acefs/self/server/peer_id)" == "2" ]; then
        srv_id=$(cat /.ace/acefs/self/server/id)
        [ -n "$srv_id" ] || return 1
        [ "$(cat $acefs/servers/server-$(printf "%04d" $(( $srv_id - 1)))/state)" == "13" ] && return 3
    fi
    for jj in $(for ii in $(awk -v grp=$grp_id '{if($1==grp) print FILENAME}' $acefs/servers/*/group_id | awk -F/ '{print $5}'); do cat $acefs/servers/$ii/nicports/1/switch/id; done | sort | uniq); do
         echo -n "ethswitch-$(printf "%04d" ${jj}) "
    done
}

get_value() {
    local arg find
    find=$1
    for arg in $(cat /proc/cmdline); do
        if echo $arg | grep "^${find}=" >/dev/null 2>&1; then
            echo $arg | sed "s/^${find}=//g"
             return
         fi
    done
}

hexmac() {
  [ -n "$1" ] || return 1
  echo 0x$(echo $1 | sed 's/://g' | tr '[:upper:]' '[:lower:]')
}

get_hexmac() {
  [ -n "$1" ] || return 1
  echo $1 | sed "s/^0x0000/0x/g"
}

#[ "$(cat $port_path/server/name)" = "" ] && continue
#[ "$(cat $port_path/server/type)" != "compute" ] && continue
#port_hexmac=$(ace get $port_path/conn_guid0)
#port_mac=$(mac2int $port_hexmac)
#port_hexmac=0x$(echo $mac_addr | sed 's/://g')
#ace put ethswitches/$sw/ports/$port/conn_guid0=$port_hexmac



if [ "$(cat $acefs/global/identify_servers)" != "2" ]; then
    echo "This ace is not Ethernet Only ACE"
    exit
fi


if [ "$(cat $acefs/self/server/type)" == "group" ]; then
    ip=$(get_value ip)
    ip_server=$(echo $ip | awk -F : '{print $2;}')
    export ACE_HOST=$ip_server
fi

while [ 1 ]; do
  log_rotate
  echo "$(date +"%m/%d %H:%M") "  >> $log_dir/ace_ethstatd.dbg
  if [ ! -d $eth_db ]; then
     echo -n "Not ready /acefs/ethswitches information, check database file or wait..."  >> $log_dir/ace_ethstatd.dbg
     sleep 10 
     continue
  fi
  if [ "$(cat $acefs/self/server/type)" == "management" ]; then
      if ! switches=$(get_mgmt_sw); then
         echo -n "$switches"  >> $log_dir/ace_ethstatd.dbg
         sleep 10
         continue
      fi
  elif [ "$(cat $acefs/self/server/type)" == "group" ]; then
      switches=$(get_group_sw)
      rc=$?
      
      if [ "$rc" == "3" ]; then
             echo -n "Running ace_ethstatd.sh in peer_id=1"  >> $log_dir/ace_ethstatd.dbg
             sleep 120
             continue
      elif [ "$rc" == "2" ]; then
             echo -n "$switches"  >> $log_dir/ace_ethstatd.dbg
             exit 0
      elif [ "$rc" == "1" ]; then
             echo -n "$switches"  >> $log_dir/ace_ethstatd.dbg
             sleep 10
             continue
      fi
  else
      echo "This server is not group or management node"
      exit 1
  fi

  for sw in $switches; do
    echo -n "$sw: "  >> $log_dir/ace_ethstatd.dbg
    target=$(cat $eth_db/$sw/ipaddr)
    [ ! -n "$target"  -o "$target" == "0" ] && continue
    sw_mac_addr=$(get_sw_mac_addr $target) || sw_mac_addr=""
    if [ ! -n "$sw_mac_addr" ]; then
       put_state ethswitches/$sw/state 1
       continue
    fi

    #sw_hexmac=0x$(echo $sw_mac_addr | sed 's/://g' | tr '[:upper:]' '[:lower:]')
    sw_hexmac=$(hexmac $sw_mac_addr) 
    put_state ethswitches/$sw/guid $sw_hexmac

    for ports in $(get_sw_ports_status $target); do
      port=$(echo $ports | awk -F. '{print $2}')

      #echo -n "$(get_sw_port_name $port)(SNMP ID:$port)<==> "
      echo -n "$port:"  >> $log_dir/ace_ethstatd.dbg

      if [ "$(echo $ports | awk -F. '{if($1=="d") printf "down"}')" == "down" ]; then
        #echo "down"
        echo -n "0 "  >> $log_dir/ace_ethstatd.dbg
        put_state ethswitches/$sw/ports/$port/conn_guid0 0x0000000000000000
      else
        identify=$(get_identify_from_sw_port $port | cut -c 25-)
        mac_addr=$(get_mac $identify)
        if [ "$mac_addr" == "unknown" ]; then
            #echo -n "$mac_addr"
            echo -n "2 " >> $log_dir/ace_ethstatd.dbg
        else
            (( $dbg >= 3 )) && echo -n "($mac_addr) " >> $log_dir/ace_ethstatd.dbg || echo -n "1 " >> $log_dir/ace_ethstatd.dbg
            #port_hexmac=0x$(echo $mac_addr | sed 's/://g' | tr '[:upper:]' '[:lower:]')
            port_hexmac=$(hexmac $mac_addr)
            put_state ethswitches/$sw/ports/$port/conn_guid0 $port_hexmac
        fi
      fi
    done
    put_state ethswitches/$sw/state 2
  done
  echo  >> $log_dir/ace_ethstatd.dbg
  sleep $interval
done 
