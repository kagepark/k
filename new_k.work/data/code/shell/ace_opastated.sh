#!/bin/sh

# 09/15/2015  KGP Initial opa status script
# 09/16/2015  KGP make a put_port_info() 
# 09/17/2015  KGP make a get_node_hca() for local hca's information, 
#                 update put_port_info()
#
#


get_node_hca() {
    hca_info=($(opainfo | head -n4 | while read line; do
          echo $line | sed "s/ /:/g" | sed "s/::/:/g"
    done))

    echo ${hca_info[0]} | awk -F: '{printf "%s:%s:0x%s", $1, $2,$5}'
    echo ${hca_info[1]} | awk -F: '{printf ":%s", $2}'
    echo ${hca_info[2]} | awk -F: '{printf ":%s", $3}' | sed "s/Gb//g"
    echo ${hca_info[3]} | awk -F: '{printf ":%s", $3}'
    opaportinfo | grep NeighborNodeGuid | awk '{printf ":%s", $2}'
    # HCA_NAME:PORT:GUID:STATE:RATE:NUM:SW_GUID
}

find_switch() {
   local sw_guid
   sw_guid=$1
   [ -n "$sw_guid" ] || return 1
   awk -v guid=$sw_guid '{if($1==guid) print FILENAME}' /acefs/switches/*/guid | awk -F/ '{print $4}'
}

find_my_id() {
   local guid sw_guid port
   guid=$1
   port=$2
   sw_guid=$3
   [ -n "$guid" ] || return 1
   sw_name=$(find_switch $sw_guid)
   node_guid_path=$(awk -v guid=$guid '{if($1==guid) print FILENAME}' /acefs/switches/$sw_name/ports/*/server/ports/$port/guid)
   [ -n "$node_guid_path" ] && cat $(dirname $node_guid_path)/../../id
}
# ex: find_my_id  $(get_node_hca| awk -F: '{printf "%s %s %s",$3,$2,$7}')

put_sw_guid() {
    local sw_name sw_guid
    sw_name=switch-0001
    sw_guid=$(opasaquery -t sw | awk '{if($1=="SystemImageGuid:") print $2}')
    if [ -n "$sw_guid" ]; then
        ace put switches/$sw_name/guid=$sw_guid
        ace put switches/$sw_name/state=2
    else
        ace put switches/$sw_name/state=1
    fi
}


put_port_info() {
 unset sw_good_port

 x=0
 for line in $(opareport -o nodes -x -F nodetype:SW 2>/dev/null | opaxmlextract \
  -e Switches.Node.NodeGUID \
  -e PortInfo.PortNum \
  -e PortInfo.LinkWidthActive \
  -e PortInfo.LinkSpeedActive \
  -e Neighbor.Port.NodeGUID \
  -e Neighbor.Port.PortNum); do
  if (( x == 0 )); then
      x=$(($x+1))
      continue
  fi
  
#echo "> $x: $line"
  if (( $x % 2 == 0 )); then
#echo "$x >> $line"
      sw_guid=$(echo $line| awk -F\; '{print $1}')
      sw_port=$(echo $line| awk -F\; '{print $2}')
      node_guid=$(echo $line| awk -F\; '{print $5}')
      node_port=$(echo $line| awk -F\; '{print $6}')
      sw_name=$(find_switch "$sw_guid") || return 1
      if [ ! -n "$sw_name" ]; then
          echo "ERR: $sw_guid can't find switch name"
          continue
      fi

      # switch port state: 0=?, 1=O, 3=1
      if [ -f /acefs/switches/$sw_name/ports/$sw_port/server/ports/$node_port/guid ]; then
          server_guid_path=$(readlink -f /acefs/switches/$sw_name/ports/$sw_port/server/ports/$node_port/guid | sed "s#^/acefs/##g")
          if [ -n "$node_guid" ]; then
             ace put ${server_guid_path}=$node_guid
             ace put $(echo ${server_guid_path} | sed "s/guid/mrate/g")=$sw_port_rate_speed
             ace put switches/$sw_name/ports/$sw_port/state=3
             sw_id=$(cat /acefs/switches/$sw_name/id)
             sw_good_port[$sw_id]="${sw_good_port[$sw_id]} $sw_port"
          else
             # Unknown switch port state : 0
             ace put switches/$sw_name/ports/$sw_port/state=0
          fi
      else
          [ -f /acefs/switches/$sw_name/ports/$sw_port/state ] && ace put switches/$sw_name/ports/$sw_port/state=1
      fi

      echo "$sw_name($sw_guid):$sw_port:$node_guid:$node_port:$sw_port_rate_speed"
  else
#echo "$x >> $line"
      sw_rate_n=$(echo $line| awk -F\; '{print $3}')
      sw_rate_sp=$(echo $line| awk -F\; '{print $4}' | sed "s/Gb//g")
#echo "> $sw_rate_n : $sw_rate_sp"
      [ -n "$sw_rate_sp" ] || sw_rate_sp=0
      sw_port_rate_speed=$(($sw_rate_n * $sw_rate_sp))
  fi
  x=$(($x+1))
 done

 export sw_good_port

 # MGMT state
 my_sw_port=$(cat /acefs/self/server/ports/1/dest_port)
 my_sw_state_path=$(readlink -f /acefs/self/server/ports/1/switch/ports/$my_sw_port/state | sed "s#/acefs/##g")

 [ "$(cat /acefs/self/server/ports/1/guid 2>/dev/null)" != "$(get_node_hca|awk -F: '{print $3}')" ] && ace put ${my_sw_state_path}=0

 my_sw_guid_path=$(readlink -f /acefs/self/server/ports/1/switch/guid)
 [ "$([ -f $my_sw_guid_path ] && cat $my_sw_guid_path)" != "$(get_node_hca|awk -F: '{print $7}')" ] && ace put ${my_sw_state_path}=0
}


clean_sw_port() {
  local sw_n
  for ((ii=1;ii<=$(cat /acefs/global/num_switches); ii++)); do
      sw_nm=switch-$(printf "%04d" $ii)
      [ -n "${sw_good_port[$ii]}" ] && sw_good_port_arr=(${sw_good_port[$ii]}) || sw_good_port_arr=()
      for sw_nm_pn in $(ls /acefs/switches/$sw_nm/ports 2>/dev/null); do
          brk=0
          for ((chk=0;chk<${#sw_good_port_arr[*]};chk++)); do
               if [ "$sw_nm_pn" == "${sw_good_port_arr[$chk]}" ]; then 
                  brk=1
                  break
               fi
          done
          [ "$brk" == "1" ] && continue
          [ -f /acefs/switches/$sw_nm/ports/$sw_nm_pn/state ] && ace put switches/$sw_nm/ports/$sw_nm_pn/state=1
      done
  done
}

while [ 1 ]; do
    put_sw_guid
    put_port_info
    clean_sw_port
    sleep 5
done
