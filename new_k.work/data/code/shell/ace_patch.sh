#!/bin/sh
# 04/24/2014 make a patch script
# 04/25/2014 Fix some bugs

src_file=$1
[ -n "$src_file" ] || error_exit "$(basename $0) <src_file>"
[ -f "$src_file" ] || error_exit "$src_file not found"

patch_date=$(date "+%Y%m%d-%H%M%S")
work_dir=$(mktemp -d /tmp/ace_patch.XXXXXXXXXXXXXXX)
ace_patch[0]="pre_work.sh"                                  # Pre-work script
ace_patch[1]="ace-3.1_r3148_patch-3252-20150424142208.tgz"  # ACE Patch file
ace_patch[2]="post_work.sh"                                 # Post-work script
patch_history=/opt/ace/etc/patch.history
file_list=/opt/ace/etc/patch.$patch_date.files
log_file=/opt/ace/etc/patch.$patch_date.log
backup_dir=/backup/patch/$patch_date
remain_patch=/opt/ace/etc/patch.$patch_date.remain

_cleanup() {
    [ -n "$work_dir" ] || return 0
    [ -d "$work_dir" ] && rm -fr "$work_dir"
}

error_exit() {
    echo $*
    exit 1
}

trap _cleanup EXIT

echo "$(date) : $(hostname) : $(pwd) : $src_file : $ace_patch[0] : $ace_patch[1] : $ace_patch[2] : $file_list : $log_file : $backup_dir" >> $patch_history

help() {
  echo
}

_base_backup() {
    local patch_backup
    patch_backup=$backup_dir/backup
    [ -d "$patch_backup" ] || mkdir -p "$patch_backup"
    echo "$(date) : Backup list : $*"
    for bf in $*; do
        [ ! -d $bf -a ! -f $bf -a ! -L $bf ] && continue
        echo "Backup $bf"
        if [ -L $bf -a -f $bf ]; then
            [ -d $patch_backup/$(dirname $bf) ] || mkdir -p $patch_backup/$(dirname $bf)
            cp $bf  $patch_backup/$bf
        elif [ ! -L $bf -a -f $bf ]; then
            [ -d $patch_backup/$(dirname $bf) ] || mkdir -p $patch_backup/$(dirname $bf)
            cp -a $bf $patch_backup/$bf
        elif [ -L $bf -a -d $bf ]; then
            [ -d $patch_backup/$bf ] || mkdir -p $patch_backup/$bf
            cp -a $bf/  $patch_backup/$bf/
        elif [ ! -L $bf -a -d $bf ]; then
            [ -d $patch_backup/$bf ] || mkdir -p $patch_backup/$bf
            cp -a $bf $patch_backup/$(dirname $bf)
        fi
    done
}

_pre_work() {
  [ -d $work_dir ] && rm -fr $work_dir

  mkdir -p $work_dir/src
  mkdir -p $work_dir/root

  echo "Work file is $src_file" | tee -a $log_file
  tar zxvf $src_file  -C $work_dir/src | tee -a $log_file
  [ -n "${ace_patch[1]}" ] && _extract $work_dir/src/${ace_patch[1]} $work_dir/root | tee -a $log_file

  if [ -n "${ace_patch[0]}" ]; then
      if [ -f $work_dir/src/${ace_patch[0]} ]; then
          echo "Run pre-work script" | tee -a $log_file
          . $work_dir/src/${ace_patch[0]} | tee -a $log_file
      fi
  fi

}

_cp() {
     local src_file dest_file
     src_file=$1
     dest_file=$2
     [ -n "$src_file" ] || return 1
     [ -n "$dest_file" ] || return 1
#     [ ! -f "$src_file" -a ! -d "$src_file" -a ! -L "$src_file" ] || return 1
     [ -s "$src_file" ] || return 1

     dest_dir=$(dirname $dest_file)
     if [ -L $dest_file ]; then
         if [ "/opt/ace" == "$dest_file" -o "/opt/ace/tools" == "$dest_file" -o "/opt/ace/plugins" == "$dest_file" ]; then
             continue
         else
             [ -d $backup_dir/$dest_dir ] || mkdir -p $backup_dir/$dest_dir
             mv $dest_file $backup_dir/$dest_dir
             if cp -a $src_file $dest_file; then
                   echo "(update) $(ls -ld $dest_file)" >> $file_list 
                   echo "Patched $dest_file file"
             else
                   echo "(fail) $dest_file" >> $file_list
                   echo "cp $(echo $src_file | sed "s#^$work_dir/root##g") $dest_file" >> $remain_patch
                   echo "Patch fail $dest_file file"
                   return 3
             fi
         fi
     elif [ ! -s $dest_file ]; then
         [ -d $dest_dir ] || mkdir -p $dest_dir
         if cp -a $src_file $dest_file; then
             echo "(new) $(ls -ld $dest_file)" >> $file_list 
             echo "Created $dest_file file"
         else
             echo "(fail) $dest_file" >> $file_list
             echo "cp $(echo $src_file | sed "s#^$work_dir/root##g") $dest_file" >> $remain_patch
             echo "Patch fail $dest_file file"
             return 3
         fi
     else
         if cmp -s $src_file $dest_file ; then
           echo "Skip patch $dest_file (same file)"
           echo "(same) $(ls -ld $dest_file)" >> $file_list
           return 2
         else
           [ -d $backup_dir/$dest_dir ] || mkdir -p $backup_dir/$dest_dir
           cp -a $dest_file $backup_dir/$dest_file # Backup
           if cp -a $src_file $dest_file ; then
               echo "(update) $(ls -ld $dest_file)" >> $file_list 
               echo "Patched $dest_file file"
           else
               echo "(fail) $dest_file" >> $file_list
               echo "cp $(echo $src_file | sed "s#^$work_dir/root##g") $dest_file" >> $remain_patch
               echo "Patch fail $dest_file file"
               return 3
           fi
         fi
     fi
}

_ace_patch() {
  local src dest

  sync
  cd $work_dir/root 
  echo "*** work dir $(pwd)"
  echo
  echo "Stop ace" | tee -a $log_file
  /etc/init.d/ace stop
  sync
  sleep 5
  find | while read line; do

     [ "$line" == "." ] && continue
     dest_file=$(echo $line | sed "s/^.//g")
     [ -d $dest_file ] && continue
     _cp "$work_dir/root/$dest_file" "$dest_file"

  done | tee -a $log_file

  sleep 5
  echo
  echo "Start ace" | tee -a $log_file
  /etc/init.d/ace start
  sleep 5
}

_post_work() {
  echo "Run Post work script" | tee -a $log_file
  if [ -n "${ace_patch[2]}" ]; then
     [ -f $work_dir/src/${ace_patch[2]} ] && . $work_dir/src/${ace_patch[2]} | tee -a $log_file
  fi
}


_extract() {
   local file
   file=$1
   dest=$2

   [ -n "$dest" ] || error_exit "dest directory not found"
   [ -n "$file" ] || error_exit "input file not found"
   [ -f "$file" ] || error_exit "$file not found"
   [ -d "$dest" ] || error_exit "$dest not found"

   echo "Extract $file to $dest"
   if file $file 2>/dev/null | grep "gzip compressed data" >& /dev/null; then
        tar zxvf $file -C $dest
   elif file $file 2>/dev/null | grep  "bzip2 compressed data" >& /dev/null; then
        tar jxvf $file -C $dest
   elif file $file 2>/dev/null | grep  "xz compressed data" >& /dev/null; then
        tar Jxvf $file -C $dest
   elif file $file 2>/dev/null | grep  "shell script text" >& /dev/null; then
        return 2
   fi
}

_do_backup() {
  echo
  echo "aced version" | tee -a $log_file
  ace version | tee -a $log_file
  ace version > $backup_dir/backup/version.txt

  echo
  echo "aced md5" | tee -a $log_file
  md5sum /opt/ace/bin/aced >> $log_file
  md5sum /opt/ace/bin/aced > $backup_dir/backup/aced.md5

  echo
  echo "ace_version's information" | tee -a $log_file
  /opt/ace/bin/ace_version | tee -a $log_file
  /opt/ace/bin/ace_version > $backup_dir/backup/ace_version.txt

  for ii in servers revisions clusters switches ports hosts ;do
      echo "Backup \"ace $ii\" information"
      ace $ii > $backup_dir/backup/$ii.txt
  done | tee -a $log_file

  echo
  _base_backup /ha_cluster/ace /ha_cluster/tools /ha_cluster/plugins /acefs /ha_cluster/tftpboot /var/lib/pacemaker /etc | tee -a $log_file


  echo "stop ace" | tee -a $log_file
  /etc/init.d/ace stop
  sleep 5
  _base_backup /ha_cluster/data | tee -a $log_file
  sleep 3
  echo "Start ace" | tee -a $log_file
  /etc/init.d/ace start
  sleep 5
}

echo
echo
echo "** Check ace"
echo
/etc/init.d/ace status >/dev/null || error_exit "ACE not found"
[ -d $backup_dir/backup ] || mkdir -p $backup_dir/backup

echo "Check ACE Environment" | tee -a $log_file
[ -f /opt/ace/tools/bin/chk_ace_env.sh ] && /opt/ace/tools/bin/chk_ace_env.sh > $backup_dir/backup/chk_ace_env.txt

echo "Check MGMT Environment" | tee -a $log_file
[ -f /opt/ace/tools/chk_hw_info.sh ] && /opt/ace/tools/chk_hw_info.sh -h $(hostname) > $backup_dir/backup/chk_hw_info.txt
if [ -f /usr/local/mon_disk/mon_disk.sh ]; then
  /usr/local/mon_disk/mon_disk.sh -v > $backup_dir/backup/mon_disk.txt
  if grep "NetAPP outbound IP" $backup_dir/backup/mon_disk.txt >& /dev/null; then
     lvscan > $backup_dir/backup/lvscan.txt
     lvdisplay > $backup_dir/backup/lvdisplay.txt
     vgdisplay > $backup_dir/backup/vgdisplay.txt
     pvdisplay > $backup_dir/backup/pvdisplay.txt
  fi
fi

echo "Check pacemaker"
if rpm -qa |grep pacemaker >& /dev/null; then
    pcs status > $backup_dir/backup/pcs_status.txt
    pcs config show > $backup_dir/backup/pcs_config.txt
    cat /etc/cluster/cluster.conf > $backup_dir/backup/pcs.cluster.conf
fi

echo
echo
echo "** Backup base files"
echo
_do_backup

echo
echo
echo "** Pre-work"
echo
_pre_work 

echo
echo
echo "** Patch ace files"
echo
_ace_patch

echo
echo
echo "** Post-work"
echo
_post_work 
