find_location() {
	local find str_arr
    find=$1
	shift 1
	str_arr=($*)
	for ((ii=0;ii<${#str_arr[*]};ii++)); do
		if [ "$find" == "${str_arr[ii]}" ]; then
			echo $ii
			break
	    fi
    done
}

print_h2d() {
	printf "%d" "0x$*"
}

print_d2h() {
	printf "%x" $*
}

print_s2c() {
	echo $* | tr "[:lower:]" "[:upper:]"
}

print_c2s() {
	echo $* | tr "[:upper:]" "[:lower:]"
}

remove_special_character() {
	echo $* | tr -cd "[:print:]"
}

print_mac() {
	local arr
	arr=($*)
	for ((ii=0;ii<$((${#arr[*]}-1));ii++)); do
		printf "%02x:" "${arr[$ii]}"
	done
	printf "%02x" "${arr[$((${#arr[*]}-1))]}"
}

mac_convert() {
    local mac num opt mac_arr mac_num chk gnum
	mac=$1
	num=$2
	opt=$3 # increse(+): i, decrese(-): d 
	[ -n "$opt" ] || opt=i
    mac_arr=( $(echo $mac| sed -e "s/:/ /g" -e "s/-/ /g") )
    chk=0
	for ((ii=$((${#mac_arr[*]}-1));ii>=0;ii--)); do
	    mac_num=$(print_h2d ${mac_arr[$ii]})
		if (($chk>1)) ; then
  		    gnum=$(($mac_num + 1)) 
		    chk=1
		elif (($chk==0)); then
  		    [ "$opt" == "d" ] && gnum=$(($mac_num - $num)) || gnum=$(($mac_num + $num))
		elif (($chk==1)); then
		    gnum=$mac_num
		fi
		if (($gnum > 255)); then
			chk=2
			mac_arr[$ii]=$(($gnum-255))
		else
			chk=1
			mac_arr[$ii]=$gnum
		fi
	done
	print_mac ${mac_arr[*]}
}

chk=0
eth0=2
eth1=1

iscb_ip=$1
if [ ! -n "$iscb_ip" ]; then
    echo "$(basename $0) <iscb_ip>"
	exit
fi

ssh root@$iscb_ip iscb bmc | while read line; do
    if (($chk==0)); then
        lnum=$(find_location MACaddr $line)
        chk=1
	else
        line_arr=($line)
    	echo ${line_arr[0]} $(mac_convert ${line_arr[$lnum]} $eth0 d)
	fi
done
