target_dir=$(pwd)
kernel_version=2.6.32-358.6.2.el6.x86_64
sourcefile=/global/kernel-358/OFED-1.5.3-3.0.0-ACE.MLX.rhel6.2.tgz
OS_NAME=rhel
OS_RELEASE=6.2

ls $target_dir/RPMS/mlnx*.rpm >/dev/null 2>&1 && ofed_type=mlx || ofed_type=open
rpm_version=$(echo $kernel_version | sed 's/-/_/g')
ofed_version=$(echo $(basename $sourcefile) | awk -F- '{print $2}')
if [ "$ofed_type" == "mlx" ]; then
      ofed_os_ver=$OS_NAME$(echo $OS_RELEASE | sed "s/\./u/g")
      ofed_rpms="$(cd $target_dir/RPMS && ls mlnx-ofa_kernel-${ofed_version}-*.$ofed_os_ver.x86_64.rpm mlnx-ofa_kernel-devel-${ofed_version}-*.$ofed_os_ver.x86_64.rpm 2>/dev/null)"
else
      ofed_rpms="kernel-ib-${ofed_version}-${rpm_version}.x86_64.rpm kernel-ib-devel-${ofed_version}-${rpm_version}.x86_64.rpm"
fi

ofed_minor_version=$(echo $(basename $sourcefile) | awk -F- '{print $3}')
source_rpm=$target_dir/SRPMS/ofa_kernel-${ofed_version}-OFED.${ofed_version}.${ofed_minor_version}.src.rpm

echo "
file: $sourcefile
ofed type : $ofed_type
kernel: $kernel_version
work_dir: $target_dir
rpm version: $rpm_version
ofed_version :  $ofed_version
ofed rpms : $ofed_rpms
ofed minor : $ofed_minor_version
source_rpm : $source_rpm
"
sleep 5


rpmbuild --rebuild --define "_topdir $work" --define 'configure_options --with-mlx4_vnic-mod --with-core-mod --with-user_mad-mod --with-user_access-mod --with-addr_trans-mod --with-mthca-mod --with-srpt-mod --with-mlx4-mod --with-mlx4_en-mod --with-cxgb3-mod --with-nes-mod --with-ipoib-mod --with-sdp-mod --with-srp-mod --with-srp-target-mod --with-rds-mod --with-umad-mod ' --define 'build_kernel_ib 1' --define 'build_kernel_ib_devel 1' --define "KVERSION $kernel_version" --define "K_SRC $kernel_src" --define 'network_dir /etc/sysconfig/network-scripts' --define "_prefix $prefix" $source_rpm
