#!/bin/sh

error_exit() {
   echo $*
   exit
}

rpm -qa |grep "^mstflint" >&/dev/null || error_exit "Can not found mstflint util. Can you install OFED or mstflint rpm package?"
rpm -qa |grep "^pciutils" >& /dev/null || error_exit "Can not found lspci util. Can you install pciutils rpm package?"

force=0
found=0

firmware=$1
[ -n "$firmware" ] || error_exit "$(basename $0) <firmware file name> [<force install: 1 >]"
[ -f $fimware ] || error_exit "Firmware file not found"
echo ""
echo "Verifying firmware file"
echo ""
mstflint -i $firmware v || error_exit "Firmware failed verification test."

x=$(mstflint -i $firmware q)
firmware_board_id=$(echo "$x" | grep PSID | awk '{print $2;}')
devices=$(lspci -v | egrep -i "infiniband|ethernet" | grep -i mellanox | awk '{print $1;}')
for dev in $devices; do
    board_id=$(mstflint -d $dev q | grep PSID | awk '{print $2;}')

    echo ""
    echo "Finding board id for $dev"
    echo ""

    if [ "$firmware_board_id" == "$board_id" ]; then
        found=1
        echo
        echo "Found matched HCA ID($board_id) with Firmware ID ($firmware_board_id)"
        echo

        echo ""
        echo "Saving a copy of the existing firmware in $save_firmware ..."
        echo ""
        mstflint -d $dev ri $(hostname)_fw_${board_id}_${dev}.bak.bin  || error_exit "Unable to save old firmware image"

        echo ""
        echo "Burning firmware image to $dev (do not interrupt) ..."
        echo ""

        if [ $force -eq 1 ]; then
            echo n | mstflint -d $dev -y -skip_is -i $firmware burn
        else
            echo n | mstflint -d $dev -i $firmware burn
        fi
        rc=$?
        if [ $rc -ne 0 ]; then
            echo
            echo "WARNING: unable to burn new firmware image to $dev"
            echo
            exit
        fi
    fi
done

if [ $found -eq 1 ]; then
    echo ""
    echo "Burn completed. Restart Infiniband drivers or reboot."
    echo ""
else
    echo ""
    echo "Can't found matched HCA device with $firmware"
    echo ""
fi
