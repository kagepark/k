#!/bin/bash

grep '^SYS_DIR' /root/stage2.conf > /root/provision.bvar
source /root/provision.bvar

LOGFILE="${SYS_DIR}/provisioning.log"

echo | tee /dev/tty0
echo | tee /dev/tty0
echo "This is an input test from tty0." |tee /dev/tty0
echo "Logging to '${LOGFILE}'" | tee /dev/tty0
while [ 1 ]
 do
	echo -n "Please enter a number: " | tee /dev/tty0
	sleep 1
	read NUMBERY < /dev/tty0
	if [ $NUMBERY -eq 666 ]
	 do
		echo "Exiting after mark of the beast" | tee /dev/tty0 $LOGFILE
		break
	fi
	echo "Logging number ${NUMBERY}" | tee /dev/tty0 $LOGFILE
done


