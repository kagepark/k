#!/bin/sh

error_exit() {
   echo "$*"
   exit 
}

k_print() {
   local a b c
   a="$1"
   b="$2"
   c="$3"
   printf "%-30s : %-20s %-20s\n" "$a" "$b" "$c"
}

k_ssh() {
   local ip msg
   ip=$1
   shift 1
   msg=$*
   ssh $ip "$msg"
}

lib_file=$(dirname $0)/kdrbd.so
[ -f $lib_file ] && . $(dirname $0)/kdrbd.so || error_exit "kdrbd.so file nof tound"

_hostname=$(hostname)

drbd_conf=/etc/drbd.conf
drbd_device=$(find_conf_value $_hostname device $drbd_conf)
drbd_disk=$(find_conf_value $_hostname disk $drbd_conf)
drbd_syncer=$(find_conf_value syncer rate $drbd_conf)

pri_drbd_ip=$(find_conf_value $_hostname address $drbd_conf | awk -F: '{print $1}')
pri_drbd_port=$(find_conf_value $_hostname address $drbd_conf | awk -F: '{print $2}')

s_hostname=$(convert_hostname $_hostname)
sec_drbd_ip=$(find_conf_value $s_hostname address $drbd_conf | awk -F: '{print $1}')
sec_drbd_port=$(find_conf_value $s_hostname address $drbd_conf | awk -F: '{print $2}')


k_print "Comment" "Primary" "Secondary"
k_print "Hostname(Pri.)"  "$_hostname"
k_print "Hostname(Sec.)"  "$s_hostname" "$(ssh $sec_drbd_ip hostname)"
k_print "DRBD IP" "$pri_drbd_ip" "$sec_drbd_ip"
ping=$(ping -c 3 $sec_drbd_ip >/dev/null && echo OK || echo Fail)
k_print "DRBD ping (both)" "$ping" 

if [ "$ping" == "OK" ]; then
   scp $lib_file $sec_drbd_ip:/tmp >& /dev/null
   scp $sec_drbd_ip:/etc/drbd.conf /tmp/drbd.conf >&/dev/null
   sdrbd_conf=/tmp/drbd.conf
fi

k_print "DRBD connection (Pri.)" "$(find_drbd_value connect)" "$([ "$ping" == "OK" ] && ssh $sec_drbd_ip ". /tmp/kdrbd.so; find_drbd_value connect")"


k_print "DRBD state (Pri.)" "$(find_drbd_value state)" "$([ "$ping" == "OK" ] && ssh $sec_drbd_ip ". /tmp/kdrbd.so; find_drbd_value state")"
k_print "DRBD update (Pri.)" "$(find_drbd_value update)" "$([ "$ping" == "OK" ] && ssh $sec_drbd_ip ". /tmp/kdrbd.so; find_drbd_value update")"


k_print "DRBD.conf (device)" "$drbd_device" "$(find_conf_value $_hostname device $sdrbd_conf)"
k_print "DRBD.conf (disk)"  "$drbd_disk" "$(find_conf_value $_hostname disk $sdrbd_conf)"
k_print "DRBD.conf (Pri. IP)" "$pri_drbd_ip" "$(find_conf_value $_hostname address $sdrbd_conf | awk -F: '{print $1}')"
k_print "DRBD.conf (Pri. Port)" "$pri_drbd_port" "$(find_conf_value $_hostname address $sdrbd_conf | awk -F: '{print $2}')"

k_print "DRBD.conf (Sec. IP)" "$sec_drbd_ip" "$(find_conf_value $s_hostname address $sdrbd_conf | awk -F: '{print $1}')"
k_print "DRBD.conf (Sec. Port)" "$sec_drbd_port" "$(find_conf_value $s_hostname address $sdrbd_conf | awk -F: '{print $2}')"
k_print "DRBD.conf (Rate)" "$drbd_syncer" "$(find_conf_value syncer rate $sdrbd_conf)"

rm -f /tmp/drbd.conf

pri_size_arr=($(fdisk -l $drbd_disk | grep "^Disk ${drbd_disk}:"))
sec_size_arr=($(ssh $sec_drbd_ip "fdisk -l $drbd_disk | grep \"^Disk ${drbd_disk}:\"" 2>/dev/null))
k_print "Physical disk(size)"  "${pri_size_arr[$((${#pri_size_arr[*]}-2))]}" "${sec_size_arr[$((${#sec_size_arr[*]}-2))]}"

k_print "DRBD version" "$(find_drbd_value version)" "$(ssh $sec_drbd_ip ". /tmp/kdrbd.so; find_drbd_value version")"

k_print "DRBD filesystem" "$(k_disk_check_dev $drbd_disk)"
