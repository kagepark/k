#!/bin/sh
# Version : 1.2.5-120
# Kage  2008-06-09   initial vrersion
# Kage  2011-05-17   upgrade few functions
# Kage  2012-02-14   fix OS version

[ -f /opt/ace/sbin/libfunc.sh ] && . /opt/ace/sbin/libfunc.sh
check_os

error_exit() {
  echo "$*"
  exit
}

kprint() {
   if [ "$1" == "-n" ]; then
	  skip=on
      shift 1
   fi
   printf " %-30s " "$1"
   if [ -n "$2" ]; then
     printf ":"
     printf "\t %-s" "$2"
   fi
   [ "$kip" == "on" ] || printf "\n" 
}


PATH=/opt/ace/sbin:/opt/ace/bin:$PATH
_SUB_MGMT_FLAG=0
if [ -d /opt/ace -a -d /acefs/global ]; then
   . /opt/ace/sbin/util-scripts.sh
   _ACE_DAEMON_TYPE=Master
   _STANDALONE_FLAG=$(/opt/ace/sbin/ace_read_config /global/standalone_ms 2>/dev/null)
   _ETHONLY_FLAG=$([ -f /acefs/global/identify_servers ] && cat /acefs/global/identify_servers)
   _SUB_MGMT_FLAG=$(/opt/ace/sbin/ace_read_config /global/sub_mgmt 2>/dev/null)
   _CLUSTER_NAME=$(/opt/ace/sbin/ace_read_config /global/mgmt_name 2>/dev/null)
   _ETH10_DEV=$(/opt/ace/sbin/ace_read_config /global/ms_net1_device 2>/dev/null)
elif [ -d /.ace/acefs/global ]; then
   _ACE_DAEMON_TYPE=Client
   _STANDALONE_FLAG=$(cat /.ace/acefs/global/standalone_ms)
   _ETHONLY_FLAG=$([ -f /.ace/acefs/global/identify_servers ] && cat /.ace/acefs/global/identify_servers)
   _SUB_MGMT_FLAG=$(cat /.ace/acefs/global/sub_mgmt)
   _CLUSTER_NAME=$(cat /.ace/acefs/global/mgmt_name)
   _ETH10_DEV=$(cat /.ace/acefs/global/ms_net1_device)
else
  error_exit "ACE directory not found"
fi

_RUN_LEVEL=$(cat /etc/inittab | grep "^id:" | awk -F: '{print $2}')

if [ "$_ACE_DAEMON_TYPE" == "Client" ]; then
   _ACE_VERSION=$(/.ace/bin/ace version)
else
   _ACE_VERSION=$(echo $(/opt/ace/bin/ace_version)) 

   if [ "$_ETHONLY_FLAG" == "2" ]; then
     _RAIL_FLAG=$(/opt/ace/sbin/ace_read_config /global/num_ethrails 2>/dev/null)
   else
     _RAIL_FLAG=$(/opt/ace/sbin/ace_read_config /global/num_rails 2>/dev/null)
   fi

   if [ "$_RAIL_FLAG" == "2" ]; then
     _ETH10_DEV2=$(/opt/ace/sbin/ace_read_config /global/ms_net2_device 2>/dev/null)
   fi

   _WHO=$(hostname | sed -e "s/$_CLUSTER_NAME//g")

   #Ethernet
   _METH0=( $([ -f /opt/ace/sbin/ace_read_config ] && /opt/ace/sbin/ace_read_config /global/mgmt_net1  2>/dev/null | sed -e "s/\./ /g") )
   [ -n "$_METH0" ] && _MGMT_ETH0=$(printf "%d.%d.%d.%d" ${_METH0[0]} ${_METH0[1]} ${_METH0[2]} $_WHO)
   [ -n "$_METH0" ] && _HA_MGMT_ETH0=$(printf "%d.%d.%d.3" ${_METH0[0]} ${_METH0[1]} ${_METH0[2]})
   _METH1=( $([ -f /opt/ace/sbin/ace_read_config ] && /opt/ace/sbin/ace_read_config /global/mgmt_net2  2>/dev/null | sed -e "s/\./ /g") )
   [ -n "$_METH1" ] && _MGMT_ETH1=$(printf "%d.%d.%d.%d" ${_METH1[0]} ${_METH1[1]} ${_METH1[2]} $_WHO)
   [ -n "$_METH1" ] && _HA_MGMT_ETH1=$(printf "%d.%d.%d.3" ${_METH1[0]} ${_METH1[1]} ${_METH1[2]})

   # HA
   if [ "$_STANDALONE_FLAG" == "0" ]; then
     _RWHO=$( [ $_WHO -eq 1 ] && echo 2 || echo 1 )
     _REMOTE_ETH0=$(printf "%d.%d.%d.%d" ${_METH0[0]} ${_METH0[1]} ${_METH0[2]} $_RWHO)
     remote_hostname=$(ssh -o ConnectTimeout=5 $_REMOTE_ETH0 hostname 2>/dev/null)
     [ "${_CLUSTER_NAME}$_RWHO" == "$remote_hostname" ] && _REMOTE_ALIVE=1 || _REMOTE_ALIVE=0
   fi

   #10G
   _MNET1=( $([ -f /opt/ace/sbin/ace_read_config ] && /opt/ace/sbin/ace_read_config /global/network1 2>/dev/null | sed -e "s/\./ /g") ) 
   [ -n "$_MNET1" ] && _MGMT_E10G1=$(printf "%d.%d.%d.%d" ${_MNET1[0]} ${_MNET1[1]} ${_MNET1[2]} $_WHO)
   [ -n "$_MNET1" ] && _MGMT_HA1=$(printf "%d.%d.%d.3" ${_MNET1[0]} ${_MNET1[1]} ${_MNET1[2]})


   if [ "$_ETHONLY_FLAG" != "2" ]; then
     #IB
     _MNET3=( $([ -f /opt/ace/sbin/ace_read_config ] && /opt/ace/sbin/ace_read_config /global/network3 2>/dev/null | sed -e "s/\./ /g") ) 
     [ -n "$_MNET3" ] && _IB0=$(printf "%d.%d.%d.%d" ${_MNET3[0]} ${_MNET3[1]}  ${_MNET3[2]}  $_WHO)
   fi
 

   if [ "$_RAIL_FLAG" == "2" ]; then
     #10G
     _MNET2=( $([ -f /opt/ace/sbin/ace_read_config ] && /opt/ace/sbin/ace_read_config /global/network2 2>/dev/null | sed -e "s/\./ /g") ) 
     [ -n "$_MNET2" ] && _MGMT_E10G2=$(printf "%d.%d.%d.%d" ${_MNET2[0]} ${_MNET2[1]} ${_MNET2[2]} $_WHO)
     [ -n "$_MNET2" ] && _MGMT_HA2=$(printf "%d.%d.%d.3" ${_MNET2[0]} ${_MNET2[1]} ${_MNET2[2]})
     if [ "$_ETHONLY_FLAG" != "2" ]; then
       #IB
       _MNET4=( $([ -f /opt/ace/sbin/ace_read_config ] && /opt/ace/sbin/ace_read_config /global/network4 2>/dev/null | sed -e "s/\./ /g") ) 
       [ -n "$_MNET4" ] && _IB1=$(printf "%d.%d.%d.%d" ${_MNET4[0]} ${_MNET4[1]}  ${_MNET4[2]}  $_WHO)
     fi
   fi
fi

#if [ -f /etc/redhat-release ]; then
#   if cat /etc/redhat-release  |grep "^CentOS" > /dev/null 2>&1 ; then
#      OS_NAME=centos
#      OS_VERNAME=CentOS
#      OS_R=( $(cat /etc/redhat-release) )
#      OS_RELEASE=${OS_R[2]}
#   else
#      OS_NAME=rhel
#      OS_VERNAME="Red Hat "
#      OS_R=( $(cat /etc/redhat-release) )
#      OS_RELEASE=${OS_R[6]}
#   fi
#fi
_KERNEL=$(uname -r)
[ -f /boot/grub/menu.lst ] && _GRUB="( default:$( grep "^default" /boot/grub/menu.lst | awk -F= '{print $2}')/Total:$( grep "^title" /boot/grub/menu.lst|wc -l) )"
_HOST=$(hostname)

#################################
echo
echo " Date : $(date)"
echo

echo
echo "<< System Environment >>"
kprint "Hostname" "$_HOST"
kprint "OS Name" "$OS_VERNAME $OS_RELEASE"
kprint "Kernel" "$_KERNEL $_GRUB"
kprint "Run Level" "$_RUN_LEVEL"
kprint "uptime" "$(uptime | cut -c 13-)"


echo
echo "<< ACE Environment >>"
kprint "ACE Version" "$_ACE_VERSION"
kprint "ACE Daemon" "$_ACE_DAEMON_TYPE"
kprint "ACE Type" "$([ "$_ETHONLY_FLAG" == "2" ] && echo "Ethernet Only" || echo IB)/$([ "$_SUB_MGMT_FLAG" == "0" ] && echo Lite || echo Full)"
kprint "ACE Network" "$([ "$_RAIL_FLAG" == "2" ] && echo Dual rail || echo Single rail)"
kprint "ACE Function" "$([ "$_STANDALONE_FLAG" == "0" ] && echo "HA (${_CLUSTER_NAME}$_RWHO [ $( [ "$_REMOTE_ALIVE" == "1" ] && echo ready || echo not ready) ])" || echo Standalone)"
[ -f /opt/ace/tools/version ] && kprint "ACE Tools Version" "$(cat /opt/ace/tools/version)"

if (( $_SUB_MGMT_FLAG > 0 )); then
    kprint "Sysgrp HA" "$( if [ "$_SUB_MGMT_FLAG" == "2" ]; then echo HA; elif [ "$_SUB_MGMT_FLAG" == "1" ]; then echo Standalone; else echo "??"; fi)"
fi
kprint "Power manage" "$( if [ "$(cat /acefs/global/commands/server/* | grep iscb | wc -l)" == "4" ]; then echo iSCB; 
elif [ "$(cat /acefs/global/commands/server/* | grep iscb | wc -l)" == "0" ]; then echo IPMI ; else echo Mixed; fi)"
kprint "Subrack" "$(db_subrack_num=$(ls /acefs/subracks 2>/dev/null | wc -l); if [ "$db_subrack_num" == "$(cat /acefs/global/num_subracks 2>/dev/null)" ]; then  (($db_subrack_num > 0 )) && echo used || echo none; else  echo "X"; fi)"
if [ -d /acefs/appfs/ace_apps ]; then kprint "ACE AppFS" "Default"; elif [ -d /acefs/appfs ]; then kprint "ACE AppFS" "Useable"; fi

#################################
# Network
echo
echo "<< network environment >>"

if [ "$_ACE_DAEMON_TYPE" == "Client" ]; then
   if [ -d /.ace/acefs/self/host ]; then
      net[1]=eth0
      net[2]=eth1
      net[3]=ib0
      net[4]=ib1
      for dev in $(seq 1 4); do
         [ "$(cat /.ace/acefs/self/host/ip$dev)" == "0.0.0.0" ] || kprint "DEV(${net[$dev]})" "$(ifconfig ${net[$dev]} 2>/dev/null |grep "$(cat /.ace/acefs/self/host/ip$dev)" >& /dev/null && cat /.ace/acefs/self/host/ip$dev || echo Fail)"
      done
   else
      kprint "Network has problem"
   fi
else
   [ -n "$_SUB_MGMT_FLAG" ] || _SUB_MGMT_FLAG=0
   # eth
   mgmt_net1_device=$([ -f /opt/ace/sbin/ace_read_config ] && /opt/ace/sbin/ace_read_config /global/mgmt_net1_device  2>/dev/null)
   mgmt_net2_device=$([ -f /opt/ace/sbin/ace_read_config ] && /opt/ace/sbin/ace_read_config /global/mgmt_net2_device  2>/dev/null)
   if [ -n "$mgmt_net1_device" ]; then
       kprint "$mgmt_net1_device : $_MGMT_ETH0" "$([ -n "$_MGMT_ETH0" ] && echo $(ifconfig $mgmt_net1_device 2>/dev/null | grep $_MGMT_ETH0 > /dev/null 2>&1 && echo "OK" || echo "X" ) || echo Not assigned)"

       if (( $_SUB_MGMT_FLAG > 0 )); then
           HA_MGMT_ETH0_RUN=$(if [ -n "$_HA_MGMT_ETH0" ]; then eth0_is=X; ip addr | grep "$_HA_MGMT_ETH0" | grep "${mgmt_net1_device}" >& /dev/null && eth0_is="OK"; echo $eth0_is ;  else  echo Not assigned; fi)
           kprint "HA-$mgmt_net1_device : $_HA_MGMT_ETH0" "$HA_MGMT_ETH0_RUN" 
       fi
       mgmt_net1_switch=$(database_config /global/mgmt_net1_switch 2>/dev/null)
       kprint "$mgmt_net1_device switch : $mgmt_net1_switch" "$(if [ -n "$mgmt_net1_switch" ]; then  ping -c 3 $mgmt_net1_switch >/dev/null &&  echo OK || echo Fail; else echo Not assigned; fi)"
   fi
   if [ -n "$mgmt_net2_device" ]; then
       kprint "$mgmt_net2_device : $_MGMT_ETH1" "$([ -n "$_MGMT_ETH1" ] && echo $(ifconfig $mgmt_net2_device 2>/dev/null | grep $_MGMT_ETH1 > /dev/null 2>&1 && echo "OK" || echo "X" ) || echo Not assigned)"
       if (( $_SUB_MGMT_FLAG > 0 )); then
           HA_MGMT_ETH1_RUN=$(if [ -n "$_HA_MGMT_ETH1" ]; then eth1_is=X; ip addr | grep "$_HA_MGMT_ETH1" | grep "${mgmt_net2_device}" >& /dev/null && eth1_is="OK"; echo $eth1_is ;  else  echo Not assigned; fi)
           kprint "HA-$mgmt_net2_device : $_HA_MGMT_ETH1" "$HA_MGMT_ETH1_RUN"
       fi
       mgmt_net2_switch=$(database_config /global/mgmt_net2_switch 2>/dev/null)
       kprint "$mgmt_net2_device switch : $mgmt_net2_switch" "$(if [ -n "$mgmt_net2_switch" ]; then  ping -c 3 $mgmt_net2_switch >/dev/null &&  echo OK || echo Fail; else echo Not assigned; fi)"
   fi

   #10G
   E10G_RUN=$([ -n "$_MGMT_E10G1" ] && echo $(ifconfig $_ETH10_DEV > /dev/null 2>&1 && ifconfig $_ETH10_DEV 2>/dev/null | grep $_MGMT_E10G1 > /dev/null 2>&1 && echo "OK" || echo "X") || echo Not assigned)
   kprint "Net1-10G($_ETH10_DEV) : $_MGMT_E10G1" "$E10G_RUN"
#   if [ "$_RAIL_FLAG" == "2" ]; then
#   if [ -n "$_MNET2" ]; then
     E10G_RUN2=$([ -n "$_MGMT_E10G2" ] && echo $(ifconfig $_ETH10_DEV2 > /dev/null 2>&1 && ifconfig $_ETH10_DEV2 2>/dev/null | grep $_MGMT_E10G2 > /dev/null 2>&1 && echo "OK" || echo "X") || echo Not assigned)
     kprint "Net2-10G($_ETH10_DEV2) : $_MGMT_E10G2" "$E10G_RUN2"
#   fi

   #HA
   #MGMT_HA1_RUN=$([ -n "$_MGMT_HA1" ] && echo $(ifconfig ${_ETH10_DEV}:0 > /dev/null 2>&1 && ifconfig ${_ETH10_DEV}:0  2>/dev/null | grep $_MGMT_HA1 > /dev/null 2>&1 && echo "OK" || echo "X") || echo Not assigned)

   MGMT_HA1_RUN=$(if [ -n "$_MGMT_HA1" ]; then eth10_is=X; ip addr | grep "$_MGMT_HA1" | grep "${_ETH10_DEV}" >& /dev/null && eth10_is="OK"; echo $eth10_is ;  else  echo Not assigned; fi)
   kprint "HA-Net1 : $_MGMT_HA1" "$MGMT_HA1_RUN"

#   if [ -n "$_MNET2" ]; then
     MGMT_HA2_RUN=$(if [ -n "$_MGMT_HA2" ]; then eth102_is=X; ip addr | grep "$_MGMT_HA2" | grep "${_ETH10_DEV2}" >& /dev/null && eth102_is="OK"; echo $eth102_is ;  else  echo Not assigned; fi)
     kprint "HA-Net2 : $_MGMT_HA2" "$MGMT_HA2_RUN"
#   fi

   #switch
   net1_switch=$(database_config /global/net1_switch 2>/dev/null)
   net2_switch=$(database_config /global/net2_switch 2>/dev/null)
   kprint "Net1 switch : $net1_switch" "$(if [ -n "$net1_switch" ]; then  ping -c 3 $net1_switch >/dev/null &&  echo OK || echo Fail; else echo Not assigned; fi)"
   kprint "Net2 switch : $net2_switch" "$(if [ -n "$net2_switch" ]; then  ping -c 3 $net2_switch >/dev/null &&  echo OK || echo Fail; else echo Not assigned; fi)"
   
   #IB
   if [ "$_ETHONLY_FLAG" != "2" ]; then
     kprint "ib0 : $_IB0" "$([ -n "$_IB0" ] && echo $(ifconfig ib0 > /dev/null 2>&1 && ifconfig ib0 2>/dev/null | grep $_IB0 > /dev/null 2>&1 && echo "OK" || echo "X") || echo Not assigned)"
     if [ "$_RAIL_FLAG" == "2" ]; then
       kprint "ib1 : $_IB1" "$([ -n "$_IB1" ] && echo $(ifconfig ib1 > /dev/null 2>&1 && ifconfig ib1 2>/dev/null | grep $_IB1 > /dev/null 2>&1 && echo "OK" || echo "X") || echo Not assigned)"
     fi
   fi

fi


#################################
echo
echo "<< check Daemons >>"

kprint "[ SGE ]"
if [ -f /acefs/global/sge ]; then
    sge_d=$(cat /acefs/global/sge)
elif [ -f /acefs/global/job_scheduler ]; then
    sge_d=$(cat /acefs/global/job_scheduler)
fi
[ "$sge_d" == "1" ] || kprint "  ACE not using SGE"

if [ -f /opt/sge/default/common/settings.sh ]; then
  . /opt/sge/default/common/settings.sh
  if ps -ef |grep sge_qmaster | grep -v grep>& /dev/null ; then
    kprint "  version" "$(qhost -help | head -n 1)"
    kprint "  Queues" "$(qconf -sql | wc -l)"
    kprint "  Hosts" "$(qhost | grep -v " \- " | grep -v HOSTNAME | grep -v "^\-\-" | wc -l)"
    kprint "  Submited Jobs" "$(qstat | grep -v "job-ID" | grep -v "^\-\-\-\-"| wc -l)"
    kprint "  Running Jobs" "$(qstat -s r | grep -v "job-ID" | grep -v "^\-\-\-\-"| wc -l)"
    kprint "  Pendding Jobs" "$(qstat -s p | grep -v "job-ID" | grep -v "^\-\-\-\-"| wc -l)"
  elif ps -ef | grep sgeexecd | grep -v grep >& /dev/null; then
    kprint "SGE Client" "Running"
  else
    kprint "  not found SGE daemon"
  fi
else
  [ "$sge_d" == "1" ] && kprint "  SGE configuration file not found"
fi

if [ "$OS_NAME" == "rhel" -a "$(higher_version 5.9 $OS_RELEASE)" == "1" ];then
    portmap_name=rpcbind
    portmap=$(ps -ef |grep rpcbind | grep -v grep > /dev/null 2>&1 && echo "Run" || echo "Down")
else
    portmap_name=portmap
    portmap=$(ps -ef |grep portmap | grep -v grep > /dev/null 2>&1 && echo "Run" || echo "Down")
fi

kprint "$portmap_name" "$portmap"

_NFSd=$(ps -ef |grep nfsd |grep -v grep |wc -l)
kprint "NFS Daemons" "$_NFSd"
kprint "NFS_export" "$( [ $_NFSd -ne 0 ] && showmount -e localhost |grep -w ha_cluster > /dev/null 2>&1 && echo "OK" || echo "X")"
kprint "Xinetd daemon" "$(ps -ef |grep -w xinetd | grep -v grep > /dev/null 2>&1 && echo "Run" || echo "Down")"
kprint "TFTPd conf" "$(grep "/ha_cluster/tftpboot/" /etc/xinetd.d/tftp >& /dev/null && echo OK || echo Fail)"
kprint "DHCPD daemon" "$(ps -ef |grep -w dhcpd | grep -v grep > /dev/null 2>&1 && echo "Run" || echo "Down")"
kprint "DHCPD conf" "$( [ "$(readlink -f /etc/dhcpd.conf)" == "/acefs/global/dhcpd.conf" ] && echo OK || echo Fail)"



if [ "$_ACE_DAEMON_TYPE" == "Client" ]; then
   kprint "ACE Client Daemon" "$(ps -ef |grep ace_server | grep -v grep >& /dev/null && echo Running || echo "Not found")"
   _GNBD=$(ps -ef |grep gnbd_recvd |grep -v grep > /dev/null 2>&1 && echo "OK" || echo "X" )
   _GNBD_DAEMONS=$(gnbd_import -n |grep "^Device" | wc -l)
   ps -ef |grep kipmi0 | grep -v grep >& /dev/null && kprint "IPMI" "Running" || kprint "IPMI" "Not found"
   kprint "BMC" "$([ -f /sys/class/ipmi/ipmi0/device/bmc/firmware_revision ] && cat /sys/class/ipmi/ipmi0/device/bmc/firmware_revision || echo "Not founc")"
else
   kprint "TFTP daemon" "$([ "$(cat /etc/xinetd.d/tftp |grep disable | awk -F= '{print $2}' | sed "s/ //g")" == "no" ] && echo "enabled" || echo "disable")/$(cat /etc/xinetd.d/tftp |grep server_args | grep " tftp " > /dev/null 2>&1 && echo "OK" || echo "OLD")"
   if [ "$_STANDALONE_FLAG" == "0" ]; then
     ha_type=$([ -f /acefs/global/ha_type ] && cat /acefs/global/ha_type || database_config /global/ha_type 2>/dev/null)
     ha_storage_type=$([ -f /acefs/global/ha_storage_type ] && cat /acefs/global/ha_storage_type || database_config /global/ha_storage_type 2>/dev/null)
     pacemaker_chk=$(ps -ef |grep pacemakerd |grep -v grep >& /dev/null && echo "OK" || echo "X")
     heartbeat_chk=$(ps -ef |grep heartbeat |grep -v grep > /dev/null 2>&1 && echo "OK" || echo "X")
     [ -n "$ha_type" ] &&  kprint "ha_type in DB" "$ha_type"
     if [ "$ha_type" == "pacemaker" ]; then
         kprint "Pacemaker Daemon" "$pacemaker_chk"
     elif [ "$ha_type" == "heartbeat" ]; then
         kprint "Hearbeat Daemon" "$heartbeat_chk"
     else
         if [ "$pacemaker_chk" == "OK" -a "$heartbeat_chk" == "X" ]; then
             kprint "Pacemaker Daemon" "OK"
         elif [ "$heartbeat_chk" == "OK" -a "$pacemaker_chk" == "X" ]; then
             kprint "Hearbeat Daemon" "OK"
         elif [ "$heartbeat_chk" == "X" -a "$pacemaker_chk" == "X" ]; then
             kprint "HA Daemon" "Not found"
         else
             kprint "HA Daemon" "$([ "$pacemaker_chk" == "OK" ] && echo " pacemaker"; [ "$heartbeat_chk" == "OK" ] && echo " heartbeat")"
         fi
     fi
   fi

   drbd_device=$([ -f /acefs/global/drbd_device ] && cat /acefs/global/drbd_device || database_config /global/drbd_device 2>/dev/null)
   if mountpoint /ha_cluster >& /dev/null; then
         ha_amount=$(echo $(df -h /ha_cluster 2>/dev/null | grep -v "^Filesystem" ) | grep "/dev/drbd0")
         if [ -n "$ha_amount" ]; then
             chk_storage_type=DRBD
         else
             if [ "/dev/mapper/$(lvscan 2>/dev/null | awk '{print $2}' |sed -e "s/'//g"  -e "s#/dev/##g" | sed "s/\//-/g" | grep "^ace_")" == "$drbd_device" ]; then
                  chk_storage_type=LVM
             elif [ "$drbd_device" == "/dev/$([ -f /proc/mdstat ] && grep "^md" /proc/mdstat | awk '{print $1}')" ]; then
                  chk_storage_type=MD
             else
                  chk_storage_type=HDD
             fi
             ha_amount=$(echo $(df -h /ha_cluster 2>/dev/null | grep -v "^Filesystem" ) | grep "$drbd_device")
         fi
   fi
   kprint "HA Device Type" "$chk_storage_type $(if [ "$chk_storage_type" == "DRBD" ]; then lsmod |grep drbd > /dev/null 2>&1 && echo "(OK)" || echo "(X)"; elif [ "$chk_storage_type" == "LVM" -a "$ha_storage_type" == "netapp" ]; then lspci -vv |grep LSI | grep Fusion-MPT >& /dev/null && echo "(OK)" || echo "(X)"; fi)"
   [ -n "$ha_storage_type" ] &&  kprint "ha_storage_type in DB" "$ha_storage_type"
   HA_C=( $ha_amount )
   kprint "/ha_cluster dir" "$([ -n "$HA_C" ] && echo "OK" || echo "X")"

   if [ -f /opt/ace/sbin/cluster_functions ]; then
       [ "$(grep "^cowloop=" /opt/ace/sbin/cluster_functions | awk -F= '{print $2}')" == "1" ] && VDEV_NAME=COW || VDEV_NAME=LOOP
   else 
       VDEV_NAME=COW
   fi
   if [ "$VDEV_NAME" == "COW" ]; then
       VDEV="$(lsmod |grep cowloop > /dev/null 2>&1 && echo "OK/$(cowdev -l | grep "/dev/cow" | wc -l)")"
   else
       used_loop=$(losetup -a 2>/dev/null | wc -l)
       total_loop=$(ls /dev/loop* 2>/dev/null | wc -l)
       free_loop=$(( $total_loop - $used_loop ))
       VDEV="$free_loop / $total_loop"
   fi

   if [ ! -n "$VDEV" ]; then
      VDEV="X/0"
      VDEV_NAME="COW/LOOP"
   fi
   kprint "$VDEV_NAME device" "$VDEV"
   _GNBD=$(ps -ef |grep gnbd_serv |grep -v grep > /dev/null 2>&1 && echo "OK" || echo "X" )
   _GNBD_DAEMONS=$(gnbd_export -l |grep Server | wc -l)
fi

kprint "GNBD daemon" "$( [ "$_GNBD" == "OK" ] && echo "$_GNBD/$_GNBD_DAEMONS" || echo "X/0" )"

_NTPD="$(chkconfig --list ntpd | grep ${_RUN_LEVEL}:on >& /dev/null && echo ON || echo OFF)"
_NTPD_RUN="$( ps -ef |grep -w ntpd |grep -v grep >&/dev/null && echo Run || echo No-Run)"
[ "$_NTPD_RUN" == "Run" ] && _NTPD_MASTER=$( ntpq -p | grep "^*" | awk '{print $1}' | sed 's/*//g')
[ -n "$_NTPD_MASTER" ] && _NTPD_SYNC="$( echo sync with $_NTPD_MASTER)"  || _NTPD_SYNC="not-sync"
kprint "NTP daemon" "$_NTPD/$_NTPD_RUN/$_NTPD_SYNC"
if [ "$_ACE_DAEMON_TYPE" == "Client" ]; then
   DIFF=( $(tail -n 100 /.ace/tmp/ace_debug.log | grep diff= | tail -n 1) )
   kprint " Time gap(-5 <= good <= 5)" "${DIFF[$((${#DIFF[*]}-1))]}"
fi

if [ "$_ETHONLY_FLAG" != "2" -a "$_ACE_DAEMON_TYPE" != "Client" ]; then
  if ps -ef |grep opensm | grep ufm | grep -v grep >/dev/null; then
    kprint "opensm" "Run of UFM"
  elif ps -ef |grep opensm | grep -v grep >/dev/null; then
    opensm_ports=$( aa=$(for ii in $(ps -ef | grep opensm | grep -v grep); do echo $ii | grep "\.log$" ; done); [ -n "$aa" ] && basename $aa | awk -F. '{print $1}' | sed "s/opensm//g" | sort -n)
    kprint "opensm" "$([ "$opensm_ports" == "$(subnet_ports)" ] && echo "OK (used port: $opensm_ports)" || echo Fail)"
  elif ps -ef |grep ifs_fm | grep -v grep >/dev/null; then
    kprint "opensm" "Run of Intel OFED+ SM"
  else
    kprint "opensm" "not found"
  fi

  kprint "IB State"
  port=0
  for gport in $(subnet_ports); do
    kprint "   Port $gport" "$(ibstat | while read line; do  echo $line |  grep Port | grep ":$" && port=$(( $port + 1 )); if [ $port -eq $gport ]; then  echo $line;  fi; done  | grep "State: Active" > /dev/null 2>&1 && echo -n "OK " || echo -n "X ")"
  done
fi

kprint "zombi process" "$(ps -ef |grep defunct |grep -v grep|wc -l)" 

kprint "Crontab daemon" "$(service crond status >& /dev/null && echo OK || echo X)"
kprint "Backup Setting" "$(crontab -l 2>/dev/null | grep ace_backup.sh >& /dev/null && echo OK || echo X)"
kprint "selinux Setting" "$(selinuxenabled  && echo "X (Please check /etc/sysconfig/selinux file and disable it)" || echo OK)"




if [ "$_ACE_DAEMON_TYPE" == "Client" ]; then
   echo
   kprint "<< Check requirement DEVICE >>"
   for dev in \
       mcelog \
       zero   \
       null   \
       pts    \
       shm    \
   ; do
       kprint "/dev/$dev" "$(ls /dev/$dev >& /dev/null && echo "Found" || echo "Not found")"
   done

   echo
   kprint "<< Check ACE output stuff >>"
   kprint "cluter name" "$( [ -d /.ace/acefs/self ] && cat /.ace/acefs/self/cluster_name)"
   _REVISION=$( [ -d /.ace/acefs/self ] && cat /.ace/acefs/self/cluster/revision)
   kprint "cluster revision" "$( [ -n "$_REVISION" ] && echo $_REVISION || echo check)"
   [ -n "$_REVISION" ] && kprint "check out" "$( [ -d /.ace/acefs/self ] && cat /.ace/acefs/self/cluster/revisions/$_REVISION/checkout)"
   kprint "kernel arg" "$( [ -d /.ace/acefs/self ] && cat /.ace/acefs/self/cluster/kernel_args)"
   kprint "server name" "$( [ -d /.ace/acefs/self ] && cat /.ace/acefs/self/server_name)"

   echo 
   kprint "<< Check imported GNBD >>"
   [ "$_GNBD" == "OK" ] && kprint "$(gnbd_import -n| grep "^Device name" | awk -F: '{print $2}')" || kprint "Not found"
else
   ################################
   # disk
   echo
   kprint "<< Disk Environment>>"
   kprint "Amount used of backup dir" "$([ -d /backup ] && du -sh /backup | awk '{print $1}' || echo "Not found /backup dir")"
   kprint "Amount used of system disk" "$(SDA=( $(echo $(df -h / 2>/dev/null | grep -v "^Filesystem")) ); echo ${SDA[4]})"
   kprint "Amount used of /ha_cluster" "${HA_C[4]}"
   [ -f /ha_cluster/storage/${OS_NAME}$(echo ${OS_RELEASE}|sed "s/\./\-/g").img ] && kprint "Create cluster image file" "$(IMG=( $(ls -l /ha_cluster/storage/${OS_NAME}$(echo ${OS_RELEASE}|sed "s/\./\-/g").img) ); echo $(( $(( ${IMG[4]}/17179869184)) *100))%)"


   _RAID_CARD=$(lspci | grep -i raid | grep -w Areca > /dev/null && echo Areca)
   [ -n "$_RAID_CARD" ] || _RAID_CARD=$(lspci | grep -i raid | grep -w 3ware > /dev/null && echo 3ware)
   [ -n "$_RAID_CARD" ] || _RAID_CARD=$(lspci | grep -i raid | grep -w MegaRAID > /dev/null && echo MegaRaid)
   [ -n "$_RAID_CARD" ] || _RAID_CARD=$(database_config /global/drbd_device | grep "/dev/md" > /dev/null && echo MD)
   [ -n "$_RAID_CARD" ] || _RAID_CARD=$(lspci -vv |grep LSI | grep Fusion-MPT >& /dev/null && echo NetAPP)
   [ -n "$_RAID_CARD" ] || _RAID_CARD=None
   kprint "Raid Info"  "$_RAID_CARD"
   kprint "  detail info : /usr/local/mon_disk/mon_disk.sh -v"
   if [ "$_RAID_CARD" == "MegaRaid" ]; then
      _CMD_RAID=/opt/ace/sbin/CmdTool2
      if [ -f $_CMD_RAID ]; then
        _ADP=$($_CMD_RAID -PDGetNum -aALL | grep "Number of Physical" | awk '{print $7}' | sed "s/://g")
        for adp_num in $_ADP; do
          _RAID_VD=$($_CMD_RAID -LDGetNum -a${adp_num} |grep "Number of Virtual" | awk -F: '{print $2}' | sed "s/ //g")
          _RAID_PD=$($_CMD_RAID -PDGetNum -a${adp_num} |grep "Number of Physical" | awk -F: '{print $2}' | sed "s/ //g")
          _RAID_TAPD=0
   	      for N in $(seq 1 $_RAID_VD); do
	        _RAID_APD=$( $_CMD_RAID -LDInfo -L$(( $_RAID_VD - $N )) -a${adp_num} | grep "Number Of Drives" | awk -F: '{print $2}' )
	        _RAID_TAPD=$(( $_RAID_APD + $_RAID_TAPD ))
	 	    _RAID_VD_LEVEL[$N]="$($_CMD_RAID  -LDInfo -L$(($N-1)) -a${adp_num} |grep "RAID Level" | awk '{print $3}' | awk -F- '{print $2}'| sed 's/,//g')"
		    _RAID_VD_SIZE[$N]="$($_CMD_RAID  -LDInfo -L$(($N-1)) -a${adp_num} |grep "^Size" | awk -F: '{print $2}')"
		    _RAID_VD_ND[$N]="$($_CMD_RAID  -LDInfo -L$(($N-1)) -a${adp_num} |grep "^Number Of Drives" | awk -F: '{print $2}')"
		    _RAID_VD_CACHE[$N]="$($_CMD_RAID  -LDInfo -L$(($N-1)) -a${adp_num} |grep "^Disk Cache Policy" | awk -F: '{print $2}' | sed "s/ //g")"
		    _RAID_VD_CONSIS[$N]="$($_CMD_RAID  -LDInfo -L$(($N-1)) -a${adp_num} |grep -w "Consistency")"
		    _RAID_LD_STATE[$N]="$($_CMD_RAID  -LDInfo -L$(($N-1)) -a${adp_num} |grep -w "^State:" | awk -F: '{print $2}' | sed "s/ //g")"
	      done
          _FAIL_DISKS="$( echo $($_CMD_RAID -PDList -a${adp_num} | while read line; do  if echo $line |grep "^Enclosure Device ID:" >& /dev/null ; then  e=$(echo $line | awk -F: '{print $2}' | sed "s/ //g"); else if echo $line |grep "^Slot Number:" >& /dev/null ; then  s=$(echo $line | awk -F: '{print $2}' | sed "s/ //g"); fi; if echo $line | grep "^Firmware state:" >& /dev/null; then state=$(echo $line | awk -F: '{print $2}' | awk -F, '{print $1}' | sed "s/ //g");  if [ "$state" != "Online" -a "$state" != "Hotspare" ]; then echo "E$e:S$s($state)"; fi ; fi; fi; done ) )"
          [ -n "$_FAIL_DISKS" ] || _FAIL_DISKS=None
	      _RAID_NFD=$(( $_RAID_PD - $_RAID_TAPD ))
	      kprint "   Physical Drives[$adp_num]" "$_RAID_PD"
	      kprint "   Free Physical Drives[$adp_num]" "$_RAID_NFD"
	      kprint "   Virtual Drives[$adp_num]" "$_RAID_VD"
	      for N in $(seq 1 $_RAID_VD); do
	        kprint "    - L$(($N - 1)) Raid Level" "${_RAID_VD_LEVEL[$N]}"
	        kprint "    - L$(($N - 1)) Disk Size" "${_RAID_VD_SIZE[$N]}"
	        kprint "    - L$(($N - 1)) # of Disks" "${_RAID_VD_ND[$N]}"
	        kprint "    - L$(($N - 1)) Cache" "${_RAID_VD_CACHE[$N]}"
	        kprint "    - L$(($N - 1)) State" "$( [ "${_RAID_LD_STATE[$N]}" == "Optimal" ] && echo OK || echo "Fail" )"
	        [ -n "${_RAID_VD_CONSIS[$N]}" ] && kprint "    -L$(($N - 1)) Consistency" "Yes (Check please for Disk speed)"
	      done
          kprint "    - Fail Disks" "$_FAIL_DISKS"
          echo
        done
      fi
   fi

   #################################
   # ACE
   echo
   echo "<< check syncable files >>"
   if [ "$_STANDALONE_FLAG" == "0" -a "$_REMOTE_ALIVE" == "1"  ]; then
     tmp_dir=/tmp/ace_chk
     [ -d $tmp_dir ] && rm -fr $tmp_dir
     mkdir -p $tmp_dir
     [ "$sge_d" == "1" ] && sge_chk="/var/spool/sge /etc/profile.d/sge.sh"

     if [ "$ha_type" == "pacemaker" ]; then
         ha_file="/etc/cluster/cluster.conf
/etc/corosync/service.d/pcmk
/etc/corosync/authkey"
     else
         ha_file="/etc/ha.d/ha.cf
/etc/ha.d/haresources"
     fi

     CHK_ENV="
         /etc/hosts
         /etc/drbd.conf
         /etc/fstab
         $sge_chk
         /etc/exports
         /etc/sysctl.conf
         /etc/modprobe.conf
         /etc/modprobe.d/loop.conf
         /etc/modprobe.d/igb.conf
         /boot/grub/grub.conf
         /etc/ld.so.conf
         /etc/securetty
         /etc/inittab
         /etc/resolv.conf
         /etc/login.defs
         /etc/localtime
         /etc/sudoers
         /etc/issue
         $ha_file
         /etc/passwd
         /etc/group
         /etc/shadow
         /etc/security/limits.conf
         /usr/bin/ace
         /usr/bin/ace_console
         /etc/bashrc
     "

     for cfile in $CHK_ENV; do
       if [ -f $cfile ]; then
          diff_cfile="$( rsync -aupl ${_REMOTE_ETH0}:$cfile $tmp_dir >&/dev/null && ( diff -up $cfile $tmp_dir/$(basename $cfile) && echo Same ) )"
          if [ "$diff_cfile" == "Same" ]; then
             echo "$cfile : Same"
          elif [ -n "$diff_cfile" ]; then
             echo "$cfile :"
             echo "---------------------------------------------------"
             echo "$diff_cfile"
             echo "---------------------------------------------------"
          else
             echo "$cfile : $cfile not found in ${_REMOTE_ETH0} node"
          fi
       elif [ -d $cfile ]; then
          RCID=( $( ssh ${_REMOTE_ETH0} ls -ld $cfile 2>/dev/null) )
          LCID=( $(ls -ld $cfile ) )
          echo "$cfile : $( [ "${RCID[2]}" == "${LCID[2]}" -a "${RCID[3]}" == "${LCID[3]}" ] && echo Same || echo Different)"
       else
          echo "$cfile not found"
       fi
     done

     rm -fr $tmp_dir
   fi

   if [ "$_STANDALONE_FLAG" == "0" ]; then
     echo
     if [ "$ha_storage_type" == "netapp" ]; then
         echo "<< Local NetAPP device (/ha_cluster) >>"
         ls $drbd_device >& /dev/null && echo Ready || echo "NetAPP device($drbd_device) is not ready"
         echo "<< Remote NetAPP Filesystem (/ha_cluster) >>"
         [ "$_REMOTE_ALIVE" == "1" ] && ssh $_REMOTE_ETH0 "ls $drbd_device >& /dev/null && echo Ready || echo \"NetAPP device($drbd_device) is not ready\" 2>/dev/null" || echo "Not ready server"
     else
         echo "<< Local DRBD device (/ha_cluster) >>"
         ls /proc/drbd > /dev/null 2>&1  && cat /proc/drbd  || echo "DRBD not ready"
         echo "<< Remote DRBD Filesystem (/ha_cluster) >>"
         [ "$_REMOTE_ALIVE" == "1" ] && ssh $_REMOTE_ETH0 '(ls /proc/drbd >&/dev/null && cat /proc/drbd || echo "DRBD not ready")' 2>/dev/null || echo "Not ready server"
     fi
   fi

   
   echo
   echo
   echo "<< check runlevel service >>"
   echo "    local runlevel  : $(who -r | awk '{print $2}')"
   if [ -n "$_REMOTE_ETH0" ]; then
       echo "    remote runlevel : $(ssh -o ConnectTimeout=5 $_REMOTE_ETH0 who -r 2>/dev/null || echo null Not-ready | awk '{print $2}')"
       chkconfig --list |awk '{if($2=="0:on" || $3=="1:on" || $4=="2:on" || $5=="3:on" || $6=="4:on" && $7=="5:on" || $8=="6:on" || $2== "on" ) print}' > /tmp/local.txt
       ssh -o ConnectTimeout=5 $_REMOTE_ETH0 chkconfig --list 2>/dev/null |awk '{if($2=="0:on" || $3=="1:on" || $4=="2:on" || $5=="3:on" || $6=="4:on" && $7=="5:on" || $8=="6:on" || $2== "on" ) print}' > /tmp/remote.txt
       if [ -s /tmp/remote.txt ]; then
          if diff -up /tmp/local.txt /tmp/remote.txt; then
             echo "    compare runlevel  : same"
             echo
          fi
       fi
       cat /tmp/local.txt
       rm -f /tmp/local.txt /tmp/remote.txt
   else
       chkconfig --list |awk '{if($2=="0:on" || $3=="1:on" || $4=="2:on" || $5=="3:on" || $6=="4:on" && $7=="5:on" || $8=="6:on" || $2== "on" ) print}'
   fi


   #################################
   echo
   echo "<< Check ACE output stuff >>"
   echo "[ clusters of ACE DB ]:" $( [ -d /acefs/clusters ] && ls /acefs/clusters)
   echo "[ tftpboot           ]:" $( [ -d /ha_cluster/tftpboot/ace ] && ls /ha_cluster/tftpboot/ace | grep -v pxelinux | grep -v kernels )
   #echo "[ storage/cluster    ]:" $( [ -d /ha_cluster/storage ] && ls -sh /ha_cluster/storage/  | grep -v total | grep -v "DELETED$" | grep -v ".img$" )
   #echo "[ storage/base       ]:" $( [ -d /ha_cluster/storage ] && ls -sh /ha_cluster/storage/  | grep -v total | grep -v "DELETED$" | grep ".img$" )
   storages="
$(echo "Base image file => Cluster image files
----------------------------------------- "
for cls in $(ls /acefs/clusters/ ); do 
    base=$(cat /acefs/clusters/$cls/install) 
    echo "$(cd /ha_cluster/storage && ls -sh $base | awk '{printf "%s(%s) ",$2,$1}' ) =>" $(cd /ha_cluster/storage && ls -sh ${cls}.[1-9]*.[0-9]* | grep -e "[0-9]$" -e ".co$" -e ".DELETED" | grep -v "0.kernel" | awk '{printf "%s(%s) ",$2,$1}' )
    echo
done)"
   echo "[ storage            ]:" "$storages"

   #################################
   echo 
   echo "<< Check exported GNBD >>"
   [ "$_GNBD" == "OK" ] && gnbd_export -l| grep Server | awk -F: '{print $2}'

   echo 
   echo "<< Check server for imported GNBD >>"
   [ "$_GNBD" == "OK" ] && gnbd_export -L 

   echo
   echo "<< Check Process >>"
   ps -ef |grep "/opt/ace" | grep -v ace_env_monitor.sh | grep -v grep 

   if [ "$ha_type" != "pacemaker" ]; then
     if [ "$(/opt/ace/sbin/ace_read_config /global/standalone_ms 2>/dev/null )" == "0" ]; then
       echo
       echo "<< Check Heartbeat Error >>"
       echo "after $(date "+%b/%d %Hh")"
       grep ResourceManager /var/log/messages | grep  "ERROR:" | grep "$(date "+%b %d %H")" | tail -n 5
       #grep ResourceManager /var/log/messages | grep  "ERROR:" 
     fi
   fi

fi



[ "$MGMT_HA1_RUN" == "X" ] && echo "ACE has a big issue for HA Ethernet Network"
if [ "$_RAIL_FLAG" == "2" ]; then
    [ "$MGMT_HA2_RUN" == "X" ] && echo "ACE has a big issue for secondary HA Ethernet Network"
fi


if (( $_SUB_MGMT_FLAG > 0 )); then
#####################################################
echo
echo
echo "<< checking overlap IP between ipmi network and dhcp range of group server >>"
echo
#####################################################
nodes="$(cd /acefs/clusters/sysgrp/hosts && ls ) "

ipmi_net1=$(cat /acefs/global/ipmi_net1)
echo "ipmi_net1: $ipmi_net1"
echo
for node in $nodes; do
   if [ -n "$node" ]; then
       echo -n "$node : "
       ping -c 2 $node >&/dev/null && ( aa=$(ssh $node '( 

[ -f /.ace/acefs/global/dhcpd.conf ] && dhcp_file=/.ace/acefs/global/dhcpd.conf || dhcp_file=/acefs/global/dhcpd.conf

[ "$(cat /.ace/acefs/self/host/server/failover_ip1)" == "$(grep routers $dhcp_file | sed -e "s/option//g" -e "s/routers//g" -e "s/ //g" -e "s/\;//g"|uniq)" ] || echo "router problem"

echo $(grep range $dhcp_file | sed -e "s/range//g" -e "s/\;//g")
       )' 2>/dev/null || echo "Connection fail"); [ "$aa" == "Connection fail" ] && echo "Connection fail" || echo $aa | grep $( echo $ipmi_net1 | awk -F. '{printf "%s.%s.%s",$1,$2,$3}' ) && echo "IP overlaped(same range)" || echo ok ) || echo "-"
   fi
done
fi


#####################################################
echo
echo
echo "<< checking DHCPd in group server >>"
echo
#####################################################
nodes="$(cd /acefs/clusters/sysgrp/hosts && ls ) "
for node in $nodes; do
   if [ -n "$node" ]; then
       echo -n "$node : "
       ping -c 2 $node >&/dev/null && ( ssh $node '(
[ -f /.ace/acefs/global/dhcpd.conf ] && dhcp_file=/.ace/acefs/global/dhcpd.conf || dhcp_file=error
ps -ef | grep dhcpd | grep "$dhcp_file" |grep -v grep >& /dev/null && echo -n "DHCP OK " || echo -n "DHCP Fail "
ps -ef | grep xinetd | grep -v grep >& /dev/null && echo -n ", xinetd ok" || echo -n ", xinetd fail"
grep "/.ace/ha_cluster/tftpboot" /etc/xinetd.d/tftp >& /dev/null && echo -n ", tftp ok" || echo -n ", tftp fail"
[ -f /.ace/ha_cluster/tftpboot/ace/pxelinux.0 ] && echo ", pxelinux ok" || echo ", pxelinux fail"
       )' 2>/dev/null || echo "connection fail" ) || echo "-"
   fi
done

# $Id: chk_ace_env.sh 3215 2015-03-26 21:31:43Z kage $
