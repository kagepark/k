#!/bin/sh
#      Date        Num   Comment
# KGP 07/25/2014 :    : add global, switch, server & server-sw ib connection.
# KGP 08/06/2014 :    : add IB connection between sw-sw & sw-server, rail num, switch num, max_server
# KGP 08/07/2014 :    : add subrack check, peer_id check (group/mgmt), server groups, support mac osx
# KGP 08/12/2014 :    : add checking server's port issue, fix a id checking bug in server's port
# KGP 08/14/2014 : 03 : Patch issue on MAC OSX
# KGP 08/15/2014 : 01 : ignore comment out line ( start "#" line )
# KGP 09/17/2014 : 01 : Add checking subnet_ports issue for opensm#d
# KGP 09/17/2014 : 02 : Add checking OFED (openibd) on the mgmt node, if not mgmt node then skip this
# KGP 01/06/2015 : 03 : Add checking typo in global section and add server_data in server section.
# KGP 06/21/2015 : 01 : Fix a bug of subnet_port number

tmp_dir=$(mktemp -d /tmp/ACE_DB_CHK_XXXXXXXXX)

_k_temp_close() {
    [ -d $tmp_dir ] && rm -fr $tmp_dir
}

error_exit() {
    echo "
$*
    "
    echo "Please look at $tmp_dir directory and remove the $tmp_dir after debugging"
    exit  1
}

ERR=0
export rotate_num=0
export group_peer=0
rotate() {
    if [ "$rotate_num" == "0" ]; then
       printf "\b%s" "|"
    elif [ "$rotate_num" == "1" ]; then
       printf "\b%s" "/"
    elif [ "$rotate_num" == "2" ]; then
       printf "\b%s" "-"
    elif [ "$rotate_num" == "3" ]; then
       printf "\b%s" "\\"
    fi
    (( $rotate_num >= 3 )) && export rotate_num=0 || export rotate_num=$(($rotate_num+1))
}

srv_hca() {
    local hcas hca ports port srv_path d_id d_port p_id p_hca p_hca_port d_id_t d_port_t
    srv_path=$tmp_dir/servers/$1
    hcas=$(cat $srv_path/hcas)
    for port in $(ls $srv_path/ports); do
        p_id=$(cat $srv_path/ports/$port/id)
        p_hca[$(cat $srv_path/ports/$port/hca 2>/dev/null)]=y

        p_hca_port[$port]=$(cat $srv_path/ports/$port/hca_port)

        d_id_t=$(cat $srv_path/ports/$port/dest_id 2>/dev/null)
        d_id[$d_id_t]=$d_id_t
        d_port_t=$(cat $srv_path/ports/$port/dest_port 2>/dev/null)
        d_port[$d_port_t]=$d_port_t
        [ "$p_id" == "$port" ] || error_exit "ERR: $1/ports/$port/id=$p_id"
    done
    p_hca_num=$(printf "%d" $(echo ${p_hca[*]} | wc -w))
    [ "$hcas" == "$p_hca_num" ] || error_exit "ERR: $1 has wrong hca number (hcas has $hcas, but hca has ${p_hca_num} (DBG:${p_hca[*]}))"
    (( $(( $(printf "%d" $(ls $srv_path/ports | wc -l)) + 1 )) <= $((${#d_port[*]} + ${#d_id[*]})) )) || error_exit "ERR: $1 has wrong dest_id and dest_port"
}

chk_val() {
    local find values
    find=$1
    shift 1
    values=$*
    val=$(database_config $find)
    [ -n "$val" ] || return 0
    for ii in $values; do
        if [ "$ii" == "$val" ]; then
            echo "  + $find : $ii"
            return 0
        fi
    done
    echo "  + $find : incorrect value ($val) <== Typo !!"
    return 1
}

__array_index() {
    # $1 = value to look for
    # $* (after shift) where to look for it

    local value="$1"
    shift

    local n
    for (( n = 0; $#; n += 1 ))
    do
        if [[ "$value" == "$1" ]]
        then
            echo $n
            return
        else
            shift
        fi
    done

    echo -1
}

database_config() {
    local db_path dest
    db_path=$tmp_dir
    for dest in $* ; do
        [ -f $db_path/$dest ] && cat $db_path/$dest || return 1
    done
}

subnet_ports() {
    local globals=($(database_config \
                        /global/num_rails \
                        /global/server_ports))

    local num_subnets=${globals[0]}
    local num_server_ports=${globals[1]}

    local all_hcas=($(/bin/ls -d /sys/class/infiniband/*))
    #local all_ports=($(/bin/ls -d /sys/class/infiniband/*/ports/*))
    local all_ports=($(for hca in $(ls /sys/class/infiniband/*/ports/*/link_layer); do [ "$(cat $hca)" == "IB" -o "$(cat $hca)" == "InfiniBand" ] && echo $(dirname $hca) ; done))

    local subnets=()
    local subnet_ports=()

    local p
    for (( p = 1; p <= num_server_ports; p += 1 ))
    do
        local server_props=($(database_config \
                                /servers/server-0001/ports/$p/hca \
                                /servers/server-0001/ports/$p/hca_port \
                                /servers/server-0001/ports/$p/dest_id \
                                    2>/dev/null))

        if (( ${#server_props[*]} == 3 ))
        then
            local hca=${server_props[0]}
            local hca_port=${server_props[1]}
            local switch=$(printf "switch-%04d" ${server_props[2]})

            local subnet=$(database_config /switches/$switch/subnet)

            local n=$(__array_index $subnet "${subnets[@]}")

            if (( n < 0 ))
            then
                local hca_path=${all_hcas[hca - 1]}
                local subnet_port=$(__array_index \
                                        $hca_path/ports/$hca_port \
                                        ${all_ports[*]})

                subnets+=($subnet)
                subnet_ports+=($(($subnet_port + 1)))
            fi
        fi
    done

    echo ${subnet_ports[*]}
}

chk_connection() {
    local type port dest_id dest_port server switch dev
    type=$1
    id=$2
    port=$3

    dev=${type}-$(printf "%04d" $id)
    [ "$type" == "switch" ] && dev_path=$tmp_dir/${type}es/$dev/ports/$port || dev_path=$tmp_dir/${type}s/$dev/ports/$port
         
    dest_id=$(cat $dev_path/dest_id)
    dest_port=$(cat $dev_path/dest_port)
    [ "$type" == "switch" ] && dest_type=$(cat $dev_path/type) || dest_type=switch

    dest_dev=${dest_type}-$(printf "%04d" $dest_id)
    [ "$dest_type" == "switch" ] && dest_path=$tmp_dir/switches/$dest_dev/ports/$dest_port  || dest_path=$tmp_dir/servers/$dest_dev/ports/$dest_port

    if [ ! -d $dest_path ]; then
         printf "\b%s\n" "$dev port($port)'s destination ($dest_dev:$dest_port) not found ($dest_path)"
         return 1
    else
         dest_dest_id=$(cat $dest_path/dest_id)
         dest_dest_port=$(cat $dest_path/dest_port)
         [ "$dest_type" == "switch" ] && dest_dest_type=$(cat $dest_path/type) || dest_dest_type=switch
         dest_dest_dev=${dest_dest_type}-$(printf "%04d" $dest_dest_id)

         if [ "${dest_dest_dev}" != "$dev" ]; then
              printf "\b%s\n" "$dev($port) connected to $dest_dev($dest_port), but the $dest_dev($dest_port) connected to ${dest_dest_dev}"
              return 1
         fi
         if [ "$dest_dest_port" != "$port" ]; then
               printf "\b%s\n" "$dev($port) connected to $dest_dev($dest_port), but the $dest_dev($dest_port) connected to $dest_dest_dev port $dest_dest_port"
               return 1
         fi
    fi
}



database=$1
[ -n "$database" ] || error_exit "$(basename $0) <database file>"
[ -f $database ] || error_exit "$database file not found"

chk_open=0
line_num=0


cat $database | while read line; do
   line_num=$(($line_num+1))
   rotate

   echo $line | grep "^#" >& /dev/null && continue
   [ -n "$(echo $line |sed "s/ //g")" ] || continue

   # finding quotation marks issue
   if echo $line | awk -F\# '{print $1}' | grep "'" >& /dev/null; then
        aa=$(printf "%d" $(echo $line | awk -F\# '{print $1}' |tr -cd "'" | wc -c))
        (( $(($aa%2)) != 0 )) && error_exit "ERR: single quotation marks error at line $line_num ($line)"
   elif echo $line | awk -F\# '{print $1}' | grep "\`" >& /dev/null; then
        error_exit "ERR: found \` marks at line $line_num ($line)"
   elif echo $line | awk -F\# '{print $1}' | grep "\"" >& /dev/null; then
        error_exit "ERR: found double quotaion marks  at line $line_num ($line)"
   fi

   # finding bracket issue
   if echo $line | grep -e "switches {" -e "servers {" >& /dev/null; then
      if [ "$chk_open" != "0" ]; then
          error_exit "ERR: mismatched close(}) or open({) near by line $line_num ($line)"
      fi
   elif echo $line | grep -e "switch-" -e "server-" >& /dev/null; then
      if [ "$chk_open" != "1" ]; then
          error_exit "ERR: mismatched close(}) or open({) near by line $line_num ($line)"
      fi
   elif echo $line | grep "ports {" >& /dev/null; then
      if [ "$chk_open" != "2" ]; then
          error_exit "ERR: mismatched close(}) or open({) near by line $line_num ($line)"
      fi
   fi

   # Check IB connection  & ID issue
   if [ "$chk_open" == "0" ]; then
        root_obj=$(echo $line | awk '{print $1}')
        if [ "$root_obj" == "global" -o "$root_obj" == "servers" -o "$root_obj" == "switches" -o "$root_obj" == "subracks" ]; then
            if [ "$root_obj" == "servers" ]; then
                printf "\b%s\n" "Checking $root_obj and IB connection"
            else
                printf "\b%s\n" "Checking $root_obj"
            fi
            [ "$root_obj" == "}" ]  || ( [ -d $tmp_dir/$root_obj ] || mkdir -p $tmp_dir/$root_obj)
        fi
   elif [ "$chk_open" == "1" ]; then   # switch / server obj
        if echo $line | grep -e "switch-" -e "server-" -e "subrack-" >& /dev/null; then
            obj1=$(echo $line | awk '{print $1}')
            if [ "$obj1" != "}" ]; then
                 [ -d $tmp_dir/$root_obj/$obj1 ] && error_exit "duplicated $root_obj/$obj1 at line $line_num" || mkdir -p $tmp_dir/$root_obj/$obj1
            fi
        elif [ "$root_obj" == "global" ]; then
            gobj=$(echo $line | awk '{print $1}')
            val=$(echo $line | awk '{print $2}' | sed "s/'//g")
            [ "$gobj" == "}" ] || echo $val > $tmp_dir/$root_obj/$gobj
        fi
   elif [ "$chk_open" == "2" ]; then   # switch / server's item
        obj2=$(echo $line | awk '{print $1}' | sed "s/ //g")
        val=$(echo $line | awk '{print $2}' | sed "s/'//g")
        if [ "$obj2" == "ports" -o "$obj2" == "server_data" ]; then
            mkdir -p $tmp_dir/$root_obj/$obj1/$obj2
        elif [ "$obj2" == "type" ]; then
            echo $val > $tmp_dir/$root_obj/$obj1/$obj2
            if [ "$val" == "group" -o "$val" == "management" ]; then
                 [ "$old_group" == "$val" ] || group_peer=0
                 group_peer=$(($group_peer+1)) 
                 old_group=$val
            else
                 group_peer=0
            fi
        elif [ "$obj2" == "peer_id" ]; then
            echo $val > $tmp_dir/$root_obj/$obj1/$obj2
            if (( $group_peer > 0 )); then
                 [ "$val" == "$group_peer" ] || error_exit "$root_obj/$obj1/$obj2 has wrong peer_id ($val) at line $line_num"
            fi
        elif [ "$obj2" == "id" ]; then
            dev_name=$(echo $obj1 | awk -F- '{print $1}')
            [ -d $tmp_dir/$root_obj/$(printf "%s-%04d" $dev_name $val) ] || error_exit "$root_obj/$obj1/$obj2 has wrong id ($val) at line $line_num"
            echo $val > $tmp_dir/$root_obj/$obj1/$obj2
        elif [ "$obj2" != "}" ]; then
            echo $val > $tmp_dir/$root_obj/$obj1/$obj2
        fi
   elif [ "$chk_open" == "3" ]; then   # switch / server's ports
        obj3=$(echo $line | awk '{print $1}')
        if [ "$obj3" != "}" ]; then
             mkdir -p $tmp_dir/$root_obj/$obj1/$obj2/$obj3
             ports_arr=($line)
             for ((zz=2; zz<$((${#ports_arr[*]}-1)); zz++)); do
                 obj4=$(echo ${ports_arr[$zz]} | awk -F= '{print $1}' | sed "s/'//g")
                 o4val=$(echo ${ports_arr[$zz]} | awk -F= '{print $2}' | sed "s/'//g")
                 echo $o4val > $tmp_dir/$root_obj/$obj1/$obj2/$obj3/$obj4
                 if [ "$obj4" == "id" ]; then
                      [ "$o4val" == "$obj3" ] || error_exit "$root_obj/$obj1/$obj2/$obj3/id=$o4val is wrong at line $line_num"
#                      echo "$o4val" > $tmp_dir/$root_obj/$obj1/$obj2/$obj3/$obj4
                 elif [ "$obj4" == "dest_port" -a "$root_obj" == "servers" ]; then
#                      echo $o4val > $tmp_dir/$root_obj/$obj1/$obj2/$obj3/$obj4  
                      if ! chk_connection server $(echo $obj1 | awk -F- '{printf "%d",$2}') $obj3 ; then
                         echo " at line $line_num"
                         exit 1
                      fi
#                 else
#                      echo $o4val > $tmp_dir/$root_obj/$obj1/$obj2/$obj3/$obj4
                 fi
             done
        fi
   fi


   if echo $line | grep "{"  | grep "}" >& /dev/null; then
       continue
   elif echo $line | grep "{" >& /dev/null; then
       chk_open=$(($chk_open+1))
   elif echo $line | grep "}" >& /dev/null; then
       chk_open=$(($chk_open-1))
   fi
   
done

if [ "$?" == "0" ]; then
    printf "\b%s\n" "Checking global section's items"
    echo " - Check num_servers, max_servers numbers"
    num_servers=$(cat $tmp_dir/global/num_servers)
    max_servers=$(cat $tmp_dir/global/max_servers)
    (( $max_servers < $num_servers )) && error_exit "/global/max_servers ($max_servers) < /global/num_servers ($num_servers)"

    echo " - Check rail numbers"
    num_rails=$(cat $tmp_dir/global/num_rails)
    (( $(printf "%d" $(ls $tmp_dir/servers/server-0001/ports 2>/dev/null| wc -l)) < $num_rails )) && error_exit "This system has $num_rails rail(s). but server-0001 has not much port numbers"
    (( $(printf "%d" $(ls $tmp_dir/servers/server-0002/ports 2>/dev/null| wc -l)) < $num_rails )) && error_exit "This system has $num_rails rail(s). but server-0002 has not much port numbers"

    echo " - Check switche numbers"
    num_switches=$(cat $tmp_dir/global/num_switches)
    db_switches=$(printf "%d" $(ls $tmp_dir/switches 2>/dev/null| wc -l))
    [ "$db_switches" == "$num_switches" ] || error_exit "/global/num_switches($num_switches) and switche-database's switch number($db_switches) is different"

    echo " - Check server group numbers"
    num_groups=$(cat $tmp_dir/global/num_groups)
    db_groups=$(printf "%d" $(cat $tmp_dir/servers/*/group_id 2>/dev/null| sort | uniq | wc -l))
    [ "$db_groups" == "$num_groups" ] || error_exit "/global/num_groups($num_groups) and server-database's group number($db_groups) is different"

    if [ -f $tmp_dir/global/num_subracks ]; then
        echo " - Check subrack numbers"
        num_subracks=$(cat $tmp_dir/global/num_subracks)
        db_subracks=$(printf "%d" $(ls $tmp_dir/subracks 2>/dev/null| wc -l))
        [ "$db_subracks" == "$num_subracks" ] || error_exit "/global/num_subracks($num_subracks) and subrack-database's subrack number($db_subracks) is different"

        echo " - Check subrack_ip1"
        subrack_ip1=$(cat $tmp_dir/global/subrack_ip1)
        if [ -n "$subrack_ip1" ]; then
            sb_end_ip=$(echo $subrack_ip1 | awk -F. '{print $4}')
            [ "$sb_end_ip" == "0" ] && error_exit "end of /global/subrack_ip1 number should be start 1, not 0"
        else
            error_exit "subrack_ip1 not found"
        fi
    fi


    echo " - Check typo for global items"
    chk_val /global/conserve_ip 0 1
    chk_val /global/depth 1 
    chk_val /global/cs_net1_device eth0 eth1 eth2 eth3 eth4
    chk_val /global/ha_type pacemaker heartbeat
    chk_val /global/ha_storage_type drbd netapp
    chk_val /global/drbd_device /dev/sdb /dev/sdc /dev/mapper/ace_vg-ha_cluster
    chk_val /global/drbd_filesystem ext3 ext4 xfs
    chk_val /global/mgmt_net1_device eth0 eth1 eth2 eth3 eth4 eth5 eth6 bond0 bond1
    chk_val /global/ms_net1_device eth1 eth2 eth3 eth4 eth5 eth6 bond0 bond1
    chk_val /global/ms_net1_driver mlx4_en
    chk_val /global/ms_net2_device eth1 eth2 eth3 eth4 eth5 eth6 bond0 bond1
    chk_val /global/ms_net2_driver mlx4_en
    chk_val /global/ms_net3_device ib0 ib1 ib2 ib3
    chk_val /global/ms_net3_driver ib_ipoib
    chk_val /global/ms_net4_device ib0 ib1 ib2 ib3
    chk_val /global/ms_net4_driver ib_ipoib
    chk_val /global/topology fat-tree mesh torus
    chk_val /global/hostname_id_length 2 3 4 5
    chk_val /global/hostname_dash 0 1
    chk_val /global/job_scheduler 0 1 2 3 4
    chk_val /global/manage_mics 0 1
    chk_val /global/manage_gpus 0 1


    printf "\b%s\n" "Checking switch to switch/server connection(IB)"
    for sw in $(ls $tmp_dir/switches); do
        sw_path=$tmp_dir/switches/$sw
        for pp in $(ls $sw_path/ports); do
            rotate
            sw_port=$sw_path/ports/$pp
            sw_dest_id=$(cat $sw_port/dest_id) || error_exit "ERR: $sw_port/dest_id"
            sw_dest_port=$(cat $sw_port/dest_port) || error_exit "ERR: $sw_port/dest_id"
            sw_dest_type=$(cat $sw_port/type) || error_exit "ERR: $sw_port/dest_id"
            chk_connection $sw_dest_type $sw_dest_id $sw_dest_port
        done
    done
    for srv in $(ls $tmp_dir/servers); do
        rotate
        srv_hca $srv
    done

    printf "\b%s\n" "Checking mgmt hca port for opensm port number (subnet_ports)"
    if [ -d /sys/class/infiniband ]; then
        subnet_port=$(subnet_ports)
        (( ${#subnet_port[*]} > 0 && ${#subnet_port[*]} < 3 )) || error_exit "ERR: subnet_ports has problem ($subnet_port), please check /global/num_rails | server-0001: hcas, hca, hca_port | ibstat command"
    else
        echo "  - Not stated openibd yet. This function checking hca port on the installed ofed mgmt node. So, skip this"
    fi

    printf "\b%s\n" "Check done"
    if (( $ERR == 0 )); then
        echo Looks fine for fundermental of the DB
    fi
fi

trap _k_temp_close EXIT
