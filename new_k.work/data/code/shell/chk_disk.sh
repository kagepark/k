#!/bin/sh
# Version : 0.3.17
# Kage  2012-02-22   initial version from ace_check_env.sh script.
# Kage  2012-02-24   add replace function, add other options
# Kage  2012-02-24   add md & 3ware

MegaRaid_CMD=/opt/ace/sbin/CmdTool2
_3Ware_CMD=/usr/bin/tw_cli

error_exit() {
  echo "$*"
  exit
}

help() {
  echo "
  $(basename $0) [ <option>  ]
  
  none                     : show simple information
  -v ( --verbose)          : show detail information
  --version                : show version
  -h ( --help )            : help 
  -r ( --replace )         : replace problem HDD
  -rp                      : show rebuild progress (stop : control + c)
  -s                       : make a alarm silence
  --threshold   <value>    : checking disk partition usage ( default 85 %)
  -t ( --time ) <value>    : checking period time ( default 30 sec )
  -l ( --loop ) <value>    : loop (default 1)
  -d ( --daemon )          : daemon mode
    -e ( --email ) <userid1@user.domain> [<userid2@user.domain> ...]
                           : if it found any issue then send email to userid@user.domain
                             it need email environment ( sendmail / postfix / etc )
  
  "
  exit
}


mode=""
argv=( $* )
i=0
while [  $(( ${#argv[*]} -1 )) -ge $i ]; do
    if [ "${argv[$i]}" == "-d" -o "${argv[$i]}" == "--daemon" ]; then
       mode=d
    elif [ "${argv[$i]}" == "-v" -o "${argv[$i]}" == "--verbose" ]; then
       [ "$mode" == "d" ] || mode=v
    elif [ "${argv[$i]}" == "-r" -o "${argv[$i]}" == "--replace" ]; then
       if [ "$mode" != "d" ]; then
         replace=r
         mode=v
       fi
    elif [ "${argv[$i]}" == "-h" -o "${argv[$i]}" == "--help" ]; then
       help
    elif [ "${argv[$i]}" == "-rp" ]; then
       progress=p
    elif [ "${argv[$i]}" == "-s" ]; then
       $MegaRaid_CMD -AdpSetProp -AlarmSilence -aALL
    elif [ "${argv[$i]}" == "--version" ]; then
       grep "^# Version :" $0 | awk -F: '{print $2}'
       exit
    elif [ "${argv[$i]}" == "-t" -o "${argv[$i]}" == "--time" ]; then
       sleep=${argv[$(( $i + 1 ))]}
       i=$(($i+1))
    elif [ "${argv[$i]}" == "-e" -o "${argv[$i]}" == "--email" ]; then
       for (( i=$(($i+1)) ; $i <= $(( ${#argv[*]} -1 )) ; i=$(($i+1)) )); do
           if echo ${argv[$(( $i ))]} | grep "^-" ; then
               break
           else
               email="$email ${argv[$i]}"
           fi
       done
    elif [ "${argv[$i]}" == "-l" -o "${argv[$i]}" == "--loop" ]; then
       loop=${argv[$(( $i + 1 ))]}
       i=$(($i+1))
    elif [ "${argv[$i]}" == "--threshold" ]; then
       threshold=${argv[$(( $i + 1 ))]}
       i=$(($i+1))
    fi
    i=$(($i+1))
done

[ -n "$sleep" ] || sleep=30
[ -n "$loop" ] || loop=1
[ -n "$threshold" ] || threshold=85

kprint() {
   if [ "$1" == "-n" ]; then
	  skip=on
      shift 1
   fi
   printf " %-30s " "$1"
   if [ -n "$2" ]; then
     printf ":"
     printf "\t %-s" "$2"
   fi
   [ "$kip" == "on" ] || printf "\n" 
}


_megaraid(){
   local mode _FAIL_DISKS _CMD_RAID _RAID_VD _RAID_PD _RAID_TAPD chk VD_NUM 
   mode=$1
   _CMD_RAID=$MegaRaid_CMD
   if [ -f $_CMD_RAID ]; then
      _RAID_VD=$($_CMD_RAID -LDGetNum -aALL |grep "Number of Virtual" | awk -F: '{print $2}' | sed "s/ //g")
      _RAID_PD=$($_CMD_RAID -PDGetNum -aALL |grep "Number of Physical" | awk -F: '{print $2}' | sed "s/ //g")
      _RAID_TAPD=0
      for N in $(seq 1 $_RAID_VD); do
           _RAID_LD_STATE[$N]=$($_CMD_RAID  -LDInfo -L$(($N-1)) -aALL |grep -w "^State:" | awk -F: '{print $2}' | sed "s/ //g")
           if [ "$mode" == "d" ]; then
               if [ "${_RAID_LD_STATE[$N]}" != "Optimal" ]; then
                  logger "$(basename $0): MagaRaid: L$(($N - 1)) State : ${_RAID_LD_STATE[$N]}"
                  echo "$(basename $0): MagaRaid: L$(($N - 1)) State : ${_RAID_LD_STATE[$N]}" >> /tmp/raid.email
               fi
           else
               _RAID_APD=$( $_CMD_RAID -LDInfo -L$(( $_RAID_VD - $N )) -aALL | grep "Number Of Drives" | awk -F: '{print $2}' )
               _RAID_TAPD=$(( $_RAID_APD + $_RAID_TAPD ))
               _RAID_VD_LEVEL[$N]="$($_CMD_RAID  -LDInfo -L$(($N-1)) -aALL |grep "RAID Level" | awk '{print $3}' | awk -F- '{print $2}'| sed 's/,//g')"
               _RAID_VD_SIZE[$N]="$($_CMD_RAID  -LDInfo -L$(($N-1)) -aALL |grep "^Size" | awk -F: '{print $2}')"
               _RAID_VD_ND[$N]="$($_CMD_RAID  -LDInfo -L$(($N-1)) -aALL |grep "^Number Of Drives" | awk -F: '{print $2}')"
               _RAID_VD_CACHE[$N]="$($_CMD_RAID  -LDInfo -L$(($N-1)) -aALL |grep "^Disk Cache Policy" | awk -F: '{print $2}' | sed "s/ //g")"
               _RAID_VD_CONSIS[$N]="$($_CMD_RAID  -LDInfo -L$(($N-1)) -aALL |grep -w "Consistency")"
               _RAW_SIZE[$N]=$($_CMD_RAID -LDPDInfo  -aALL | while read line ; do
  if echo $line | grep "^Virtual Disk:" > /dev/null ; then
    VD_NUM=$(echo $line | awk -F: '{print $2}' | awk '{print $1}')
  fi

  if [ "$VD_NUM" == "$(($N-1))" ]; then
     if echo $line | grep "^PD:" | grep "Information$" | grep 0 >& /dev/null; then
        chk=1
     fi

     if [ "$chk" == "1" ]; then
        if echo $line | grep "^Raw Size:">& /dev/null; then
           echo $(echo $line | grep "^Raw Size:" | awk -F: '{print $2}' | awk '{printf "%s %s",$1,$2}')
           break
        fi
     fi
  fi
done)
           fi
      done
      _FAIL_DISKS="$( echo $($_CMD_RAID -PDList -aALL | while read line; do  if echo $line |grep "^Enclosure Device ID:" >& /dev/null ; then  e=$(echo $line | awk -F: '{print $2}' | sed "s/ //g"); else if echo $line |grep "^Slot Number:" >& /dev/null ; then  s=$(echo $line | awk -F: '{print $2}' | sed "s/ //g"); fi; if echo $line | grep "^Firmware state:" >& /dev/null; then state=$(echo $line | awk -F: '{print $2}' | awk -F, '{print $1}' | sed "s/ //g");  if [ "$state" != "Online" ]; then echo "E$e:S$s($state)"; fi ; fi; fi; done ) )"

      if [ "$mode" == "d" ]; then
          if [ -n "$_FAIL_DISKS" ]; then
             logger "$(basename $0): MegaRaid: Issued Disk's Enc & Slot numbers : $_FAIL_DISKS"
             echo "$(basename $0): MegaRaid: Issued Disk's Enc & Slot numbers : $_FAIL_DISKS" >> /tmp/raid.email
          fi
          if [ -f /tmp/raid.email ]; then
             echo  >> /tmp/raid.email
             echo "Checking time : $(date)" >> /tmp/raid.email
             echo "monitoring path : $0" >> /tmp/raid.email
             echo "help  : $(basename $0) -h" >> /tmp/raid.email
             for mail in $email ; do
                mail -s $(hostname)_disk_information $mail < /tmp/raid.email
             done
             rm -f /tmp/raid.email
          fi
      else
          [ -n "$_FAIL_DISKS" ] || _FAIL_DISKS=None
          _RAID_NFD=$(( $_RAID_PD - $_RAID_TAPD ))
          if [ "$mode" == "v" ]; then
              kprint "   Physical Drives" "$_RAID_PD"
              kprint "   Free Physical Drives" "$_RAID_NFD"
              kprint "   Virtual Drives" "$_RAID_VD"
          fi
          for N in $(seq 1 $_RAID_VD); do
              if [ "$mode" == "v" ]; then
                  kprint "    - L$(($N - 1)) Raid Level" "${_RAID_VD_LEVEL[$N]}"
                  kprint "    - L$(($N - 1)) Volume Size" "${_RAID_VD_SIZE[$N]}"
                  kprint "    - L$(($N - 1)) # of Physical Disk" "${_RAID_VD_ND[$N]}"
                  kprint "    - L$(($N - 1)) Physical Disk Size" "${_RAW_SIZE[$N]}"
                  kprint "    - L$(($N - 1)) Cache" "${_RAID_VD_CACHE[$N]}"
              fi
              kprint "    - L$(($N - 1)) State" "${_RAID_LD_STATE[$N]} $([ "${_RAID_LD_STATE[$N]}" == "Optimal" ] && echo "(OK)" )"
              if [ "$mode" == "v" ]; then
                  [ -n "${_RAID_VD_CONSIS[$N]}" ] && kprint "    -L$(($N - 1)) Consistency" "Yes (Check please for Disk speed)"
              fi
          done
          if [ "$mode" == "v" ]; then
              kprint "    - Issued Disk's [E#:S#]s" "$_FAIL_DISKS"
          fi
      fi
   fi
}

_megaraid_replace() {
  local FAIL_DISKS _CMD_RAID
   _CMD_RAID=$MegaRaid_CMD
  FAIL_DISKS="$( echo $($_CMD_RAID -PDList -aALL | while read line; do  if echo $line | grep "^Adapter" >/dev/null; then a=$(echo $line | awk '{print $2}' | sed "s/\#//g"); fi; if echo $line |grep "^Enclosure Device ID:" >& /dev/null ; then  e=$(echo $line | awk -F: '{print $2}' | sed "s/ //g"); else if echo $line |grep "^Slot Number:" >& /dev/null ; then  s=$(echo $line | awk -F: '{print $2}' | sed "s/ //g"); fi; if echo $line | grep "^Firmware state:" >& /dev/null; then state=$(echo $line | awk -F: '{print $2}' | awk -F, '{print $1}' | sed "s/ //g");  if [ "$state" != "Online" ]; then echo "$a:$e:$s"; fi ; fi; fi; done) )"


  for pdoff in $FAIL_DISKS; do
    raid=( $(echo $pdoff | sed "s/:/ /g" ) )
    adp=${raid[0]}
    phy="${raid[1]}:${raid[2]}"

    echo
    echo
    echo -n "Disk of Adapter:$adp,  Enclosure:${raid[1]}, Slot:${raid[2]} is correct [Y/n]? "
    read y
    [ -n "$y" ] || y=y
    if [ "$y" != "y" -a "$y" != "Y" ]; then
        echo "Bye~"
        exit
    fi

    $_CMD_RAID -PDOffline -PhysDrv [$phy] -a$adp
    sleep 2
    $_CMD_RAID -PDMarkMissing -PhysDrv [$phy] -a$adp
    sleep 2
    $_CMD_RAID -PDPrpRmv -PhysDrv [$phy] -a$adp

    echo
    echo
    echo "Replace the physical disk (Adapter:$adp,  Enclosure:${raid[1]}, Slot:${raid[2]})"
    echo  "and anykey to continue"
    echo
    read x

    ar=( $($_CMD_RAID -PDGetMissing -a$adp | awk '{if ($1 == 0) printf "%s %s",$2,$3 }') )
    $_CMD_RAID -PdReplaceMissing -PhysDrv [$phy] -Array${ar[0]} -row${ar[1]} -a$adp
    $_CMD_RAID -PDRbld -Start -PhysDrv [$phy] -a$adp

    echo
    echo
    echo "Start rebuild the physical disk (Adapter:$adp,  Enclosure:${raid[1]}, Slot:${raid[2]})"
    echo
    echo
  done

}

_megaraid_progress() {
  local FAIL_DISKS _CMD_RAID
   _CMD_RAID=$MegaRaid_CMD
  FAIL_DISKS="$( echo $($_CMD_RAID -PDList -aALL | while read line; do  if echo $line | grep "^Adapter" >/dev/null; then a=$(echo $line | awk '{print $2}' | sed "s/\#//g"); fi; if echo $line |grep "^Enclosure Device ID:" >& /dev/null ; then  e=$(echo $line | awk -F: '{print $2}' | sed "s/ //g"); else if echo $line |grep "^Slot Number:" >& /dev/null ; then  s=$(echo $line | awk -F: '{print $2}' | sed "s/ //g"); fi; if echo $line | grep "^Firmware state:" >& /dev/null; then state=$(echo $line | awk -F: '{print $2}' | awk -F, '{print $1}' | sed "s/ //g");  if [ "$state" != "Online" ]; then echo "$a:$e:$s"; fi ; fi; fi; done) )"


  while [ 1 ]; do
    for pdoff in $FAIL_DISKS; do
       raid=( $(echo $pdoff | sed "s/:/ /g" ) )
       adp=${raid[0]}
       phy="${raid[1]}:${raid[2]}"

       $_CMD_RAID -PDRbld -ShowProg -PhysDrv [$phy] -a$adp | grep "Rebuild Progress"
    done
    sleep $sleep
  done
}


_md() {
   echo not ready
}

_3ware() {
      disks=( $($_3Ware_CMD info | grep "^c[0-9]" | awk '{printf "%s:%s ",$1,$4}') )

      if [ "$mode" == "d" ]; then
          echo "daemon"
      else
          for i in ${disks[*]}; do
              _RAID_PD=$(( $_RAID_PD + $(echo $i | awk -F: '{print $2}') ))
          done
          _RAID_VD=${#disks[*]}
          if [ "$mode" == "v" ]; then
              kprint "   Physical Drives" "$_RAID_PD"
              kprint "   Virtual Drives" "$_RAID_VD"
          fi
          for N in $(seq 1 $_RAID_VD); do
              _RAID_INFO=( $( $_3Ware_CMD /c$(($N - 1)) show all | grep RAID ) )
              _RAID_LD_STATE[$N]=${_RAID_INFO[2]}
              _FAIL_DISKS="$_FAIL_DISKS $($_3Ware_CMD /c$(($N - 1)) show all | grep "^p[0-9]" | awk '{if($2 != "OK" && $2 != "NOT-PRESENT") print $1 }')"
              if [ "$mode" == "v" ]; then
                  _RAID_VD_CACHE[$N]=${_RAID_INFO[7]}
                  _RAID_VD_LEVEL[$N]=$(echo ${_RAID_INFO[1]} | sed "s/RAID-//g")
                  _RAID_VD_SIZE[$N]=${_RAID_INFO[6]}
                  _RAID_VD_ND[$N]=$(echo ${disks[$(($N - 1))]} | awk -F: '{print $2}')
                  _RAW_SIZE[$N]=$( $_3Ware_CMD /c$(($N - 1)) show all | awk '{if ($2=="OK") printf "%s %s\n",$4,$5}' | head -n 1 )
                  kprint "    - c$(($N - 1)) Raid Level" "${_RAID_VD_LEVEL[$N]}"
                  kprint "    - c$(($N - 1)) Volume Size" "${_RAID_VD_SIZE[$N]}"
                  kprint "    - c$(($N - 1)) # of Physical Disk" "${_RAID_VD_ND[$N]}"
                  kprint "    - c$(($N - 1)) Physical Disk Size" "${_RAW_SIZE[$N]}"
                  kprint "    - c$(($N - 1)) Cache" "${_RAID_VD_CACHE[$N]}"
                  kprint "    - c$(($N - 1)) State" "${_RAID_LD_STATE[$N]}"
              fi
              if [ "$_RAID_LD_STATE[$N]" != "OK" ]; then
                  logger "$(basename $0): c$(($N - 1)) State" "${_RAID_LD_STATE[$N]}"
                  echo "$(basename $0): c$(($N - 1)) State" "${_RAID_LD_STATE[$N]}" >> /tmp/raid.email
              fi
          done
          if [ "$mode" == "v" ]; then
              kprint "    - Issued Disks" "$_FAIL_DISKS"
          fi
      fi

   
}

PATH=/opt/ace/sbin:/opt/ace/bin:$PATH
echo
kprint "<< Check Disk/Raid >>"

lloop=1
while [ 1 ] ; do
   df -h | grep "^/dev/" | while read line ; do 
        disk=( $line )
        if [ "$mode" == "d" ]; then
           [ -f /tmp/raid.email ] && rm -f /tmp/raid.email
           if (( $(echo ${disk[4]} | sed "s/%//g") >= $threshold )); then
              logger "$(basename $0): ${disk[5]} used over ${disk[4]}" 
              echo "$(basename $0): ${disk[5]} used over ${disk[4]}" > /tmp/raid.email
           fi
        else
           if [ "$mode" == "v" ]; then
               kprint "${disk[5]}"  "${disk[4]}"
           fi
        fi
   done

   _RAID_CARD=$(lspci | grep -i raid | grep -w Areca > /dev/null && echo Areca)
   [ -n "$_RAID_CARD" ] || _RAID_CARD=$(lspci | grep -i raid | grep -w 3ware > /dev/null && echo 3ware)
   [ -n "$_RAID_CARD" ] || _RAID_CARD=$(lspci | grep -i raid | grep -w MegaRAID > /dev/null && echo MegaRaid)
   [ -n "$_RAID_CARD" ] || _RAID_CARD=$(ls /dev/md* 2> /dev/null | grep "/dev/md" > /dev/null && echo MD)

   if [ "$mode" == "v" ]; then
       [ "$mode" == "d" ]   || kprint "Raid Info"  "$_RAID_CARD"
   fi
   if [ "$_RAID_CARD" == "MegaRaid" ]; then
       _megaraid $mode
   elif [ "$_RAID_CARD" == "3ware" ]; then
       _3ware $mode
   fi
   #[ "$mode" == "d" ] && sleep $sleep || break 
   if [ "$mode" != "d" ]; then
      if (( $lloop  >= $loop )) ; then
         break
      fi
      lloop=$(($lloop+1))
   fi
   sleep $sleep
done

[ "$replace" == "r" ] && _megaraid_replace
[ "$progress" == "p" ] && _megaraid_progress
