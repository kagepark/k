bin_path=/opt/CUDA/CUDA/SDK/C/bin/linux/release
echo y | $bin_path/deviceQuery | while read line; do
  if  echo $line | grep "^Device" |grep "Tesla" >& /dev/null; then
      echo -n "$(echo $line | awk '{print $4}' | sed "s/\"//g") ["
  fi
  echo $line | grep "^CUDA Driver" >& /dev/null && echo -n "$(echo $line | awk '{printf " %s %s",$7,$9}')"
  echo $line | grep "^Total amount of global memory" >& /dev/null && echo -n "$(echo $line | awk '{printf " %s%s",$6,$7}')"
  echo $line | grep "Multiprocessors" | grep "CUDA Cores" >& /dev/null && echo -n "$(echo $line | awk '{printf " %s",$7}')"
  echo $line | grep "^GPU Clock Speed" >& /dev/null && echo -n "$(echo $line | awk '{printf " %s%s",$4,$5}') ] "
done
