_K_LIB=/opt/ace/tools/lib
. $_K_LIB/libhost.so

total_hosts=( $_hosts )
printf "\nSubmit to %4s nodes\n\n" ${#total_hosts[*]}
[ ${#total_hosts[*]} == 0 ] && exit

printf "%-14s" Servername
printf " %-14s" Hostname
printf " %3s" GRP
printf " %16s" "GNBD name"
printf " %16s" "GNBD Server"
printf " %8s" Compare
printf " %16s" "GRP ServerIP"
printf " %16s" "DHCP IP"
echo

hostname_dash=$([ -f /acefs/global/hostname_dash ] && cat /acefs/global/hostname_dash || echo 1)
hostname_id_length=$([ -f /acefs/global/hostname_id_length ] && cat /acefs/global/hostname_id_length || echo 4)

for i in $_hosts; do
  if [ -n "$_cluster" ]; then
     cluster=$_cluster
  else
     cluster_info=($(get_cluster $i $hostname_dash $hostname_id_length))
     cluster=${cluster_info[0]}
  fi
  printf "%-14s" $(cat /acefs/clusters/$cluster/hosts/$i/server/name)
  printf " %-14s" "$i"
  ssh $i '(
gnbd_info=( $(/.ace/sbin/gnbd_import -n | grep -e Device -e Server) )
group_ip_id=($(echo ${gnbd_info[6]} | sed "s/\./ /g"))
group_id=$(cat /.ace/acefs/self/group_id)
server_type=$(cat /.ace/acefs/self/server/type)
dhcp_ip=($(cat /proc/cmdline | sed "s/ /\n/g" | grep "^ip="| sed -e "s/^ip=//g" -e "s/:/ /g"))

printf " %3d" $group_id
printf " %16s" ${gnbd_info[3]}
printf " %16s" ${gnbd_info[6]}
if [ "$server_type" == "group" ]; then
  printf " %8s" $([ "${group_ip_id[2]}" == "0" ] && echo ok|| echo mismatch)
else
  printf " %8s" $([ "${group_ip_id[2]}" == "$group_id" ] && echo ok|| echo mismatch)
fi
printf " %16s" ${dhcp_ip[1]}
printf " %16s" ${dhcp_ip[0]}
echo
)'

done
