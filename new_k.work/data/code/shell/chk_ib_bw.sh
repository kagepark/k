#!/bin/sh
_app_help="
--hca  <hca dev>    : default mlx4_0 \n
\t --port <IB port>  : default 1 \n
\t --size <value>    : packet size (default: 65536) \n
"

_K_LIB=/opt/ace/tools/lib
. $_K_LIB/libhost.so

error_exit() {
   echo $*
   exit
}

psize=$(_k_opt_opt "$*" --size)
[ -n "$psize" ] || psize=65536
ib_dev=$(_k_opt_opt "$*" --hca)
[ -n "$ib_dev" ] || ib_dev=mlx4_0
ib_port=$(_k_opt_opt "$*" --port)
[ -n "$ib_port" ] || ib_port=1

total_hosts=( $_hosts )
printf "\nSubmit to %4s nodes\n\n" ${#total_hosts[*]}
[ ${#total_hosts[*]} == 0 ] && exit

echo
echo "IB speed test with package size $psize for ${#total_hosts[*]} hosts"
echo
for hostname in $_hosts; do
   if ssh -f $hostname  ib_write_bw -F -s $psize -d $ib_dev -i $ib_port >&/dev/null ; then
      sleep 1
      avg=$(ib_write_bw -F -s $psize -d $ib_dev -i $ib_port $hostname 2>/dev/null |grep $psize | awk '{print $4}')
      [ -n "$avg" ] && echo "$(hostname) <=> $hostname : average $avg MB/s" || echo "$(hostname) <=> $hostname : No data"
   else
      echo "$hostname connection fail"
   fi
done
