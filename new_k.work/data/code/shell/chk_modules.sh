#!/bin/sh

module_path=$(cat $(cat /etc/bashrc | grep Modules | tail -n 1 | awk '{print $3}' | sed "s/\/sh/\/.modulespath/g") | grep -v "^#")

for path in $module_path; do
    cd $path
    for mfile in $(find . -type f  | grep -v ".version$" | sed "s#\./##g"); do
        echo "Checking $mfile"
        module display $mfile 2> /tmp/check_module.log
        _PATH=$(grep -w "PATH" /tmp/check_module.log | awk '{print $3}')
        _MANPATH=$(grep -w "MANPATH" /tmp/check_module.log | awk '{print $3}')
        _LD_LIBRARY_PATH=$(grep -w "LD_LIBRARY_PATH" /tmp/check_module.log | awk '{print $3}')
        _DYLD_LIBRARY_PATH=$(grep -w "DYLD_LIBRARY_PATH" /tmp/check_module.log | awk '{print $3}')
        _INTEL_LICENCE_FILE=$(grep -w "INTEL_LICENCE_FILE" /tmp/check_module.log | awk '{print $3}')
        _LM_LICENCE_FILE=$(grep -w "LM_LICENCE_FILE" /tmp/check_module.log | awk '{print $3}')
        [ -d $_PATH ] || echo " - $_PATH directory not found for PATH"
        [ -n "$_MANPATH" ] && ( [ -d $_MANPATH ] || echo " - $_MANPATH directory not found for MANPATH" )
        for i_LD_LIBRARY_PATH in $(echo $_LD_LIBRARY_PATH | sed "s/:/ /g"); do [ -d $i_LD_LIBRARY_PATH ] || echo " - $i_LD_LIBRARY_PATH directory not found for LD_LIBRARY_PATH" ; done
        [ -n "$_DYLD_LIBRARY_PATH" ] && ( for i_DYLD_LIBRARY_PATH in $(echo $_DYLD_LIBRARY_PATH | sed "s/:/ /g"); do [ -d $i_DYLD_LIBRARY_PATH ] || echo " - $i_DYLD_LIBRARY_PATH directory not found for DYLD_LIBRARY_PATH" ; done )
        [ -n "$_INTEL_LICENCE_FILE" ] && ( [ -f $_INTEL_LICENCE_FILE ] || echo " - $_INTEL_LICENCE_FILE file not found for INTEL_LICENCE_FILE" )
        [ -n "$_LM_LICENCE_FILE" ] && ( [ -f $_LM_LICENCE_FILE ] || echo " - $_LM_LICENCE_FILE file not found for LM_LICENCE_FILE")
    done
done
