_K_LIB=/opt/ace/tools/lib
. $_K_LIB/libhost.so

total_hosts=( $_hosts )
printf "\nSubmit to %4s nodes\n\n" ${#total_hosts[*]}
[ ${#total_hosts[*]} == 0 ] && exit

for i in $_hosts; do
  echo "===================[ $i ]====================="
  ssh $i '( 
print_k() {
    printf "%-20s : %s\n" "$1" "$2"
}

print_kb() {
    printf "%20s   %s\n" "$1" "$2"
}

print_k "Date" "$(date)"
print_k "Host Name" "$(hostname)"

#OS
print_k "OS Version" "$(rpm -qf /etc/issue)"
print_k "Kernel" "$(uname -r)"

# M/B
APPRO_SN=( $(dmidecode -t chassis |grep "Serial Number:") )
print_k "Chassis S/N" "$( [ "${APPRO_SN[2]}" == "0123456789" ] && echo "0" || echo ${APPRO_SN[2]} )"
print_k "M/B S/N" "$(dmidecode -s baseboard-serial-number)"

#CPU
CPU_INFO=( $(cat /proc/cpuinfo | grep "model name" | tail -n 1 | sed "s/model name//g" | sed "s/\://g" ) )
CPU_HZ=( $(cat /proc/cpuinfo | grep "cpu MHz" | tail -n 1) )
CPU_CORE=( $(cat /proc/cpuinfo | grep "cpu cores" | tail -n 1) )
CPU_TCORE=$( [ -d /sys/class/cpuid ] && echo $(ls /sys/class/cpuid | wc -l) || echo $(cat /proc/cpuinfo | grep processor | wc -l) )
print_k "CPU" "${CPU_INFO[*]} "
print_k "  Cores x CPUs" "${CPU_CORE[3]} cores x $(($CPU_TCORE / ${CPU_CORE[3]})) ea"
print_k "  MHz" "${CPU_HZ[3]}MHz"

#MEM
print_k "MEM"  "$(printf "%3.1f" $( echo "$(cat /proc/meminfo | grep MemTotal | sed -e s/MemTotal:// -e s/kB//)/(1024*1024)" | bc -l) )G"

#BIOS
print_k "BIOS Version" "$(dmidecode -s bios-version)"

print_k "HDD"  "$([ -f /proc/diskstats ] && (cat /proc/diskstats | grep -w "[h|s]d[a-z]" | wc -l) || echo 0)ea"
if [ -d /sys/class/scsi_disk ]; then
for hd_l in $(ls /sys/class/scsi_disk); do
  cd /sys/class/scsi_disk/${hd_l}/device/
  HDD_DEV=/dev/$([ -d block ] && (ls block/) ||  (ls -d block\:s* | sed -e 's/block://g'))
  HDD_SN=( $(smartctl -a -d ata ${HDD_DEV} | grep "Serial Number:") )
  HDD_FM=( $(smartctl -a -d ata ${HDD_DEV} | grep "Firmware Version:") )
  [ -n "${HDD_SN[2]}" ] && hdd_sf_info="Serial(${HDD_SN[2]})"
  [ -n "${HDD_FM[2]}" ] && hdd_sf_info="$hdd_sf_info Firmware(${HDD_FM[2]})"

  print_k "  $HDD_DEV" "Chennel($hd_l) Model($(cat /sys/class/scsi_disk/${hd_l}/device/model | sed "s/ //g")/$(cat /sys/class/scsi_disk/${hd_l}/device/vendor | sed "s/ //g"))"
  [ -n "$hdd_sf_info" ] && print_kb " " "$hdd_sf_info"
done
fi

print_k "BMC" ""
print_k "  Version"  "$( [ -f /sys/class/ipmi/ipmi0/device/bmc/ipmi_version ] && (cat /sys/class/ipmi/ipmi0/device/bmc/ipmi_version) || echo "-")"
print_k "  Firmware" "$( [ -f /sys/class/ipmi/ipmi0/device/bmc/firmware_revision ] && (cat /sys/class/ipmi/ipmi0/device/bmc/firmware_revision) || echo "-")"
mac_ipmi=( $(ls /dev/ipmi* > /dev/null 2>&1 && (ipmitool lan print | grep "MAC Address") || echo "- - - - -" ) )
print_k "  MAC Address"   "${mac_ipmi[3]}"

echo "--------------- ETH Info ------------------"
for _net in $(ls /sys/class/net |grep "^[e|i]"); do
   ETH_DEV=( $(ls -l /sys/class/net/$_net/device/driver) )
   print_k "$_net ($(cat /sys/class/net/$_net/operstate)/$(basename ${ETH_DEV[$((${#ETH_DEV[*]}-1))]}))" "$(cat /sys/class/net/$_net/address)"
done

#IB
echo "--------------- IB Info ------------------"
[ -d /sys/class/infiniband ] && (
for ibn in $(ls /sys/class/infiniband | grep -v "^scif"); do
  print_k "IB Module"  "$ibn"
  print_k "  IB Type" "$(cat /sys/class/infiniband/$ibn/hca_type)"
  print_k "  IB Ports" "$(ls /sys/class/infiniband/$ibn/ports|wc -l)"
  print_k "  IB Firmware" "$([ -f /sys/class/infiniband/$ibn/fw_ver ] && (cat /sys/class/infiniband/$ibn/fw_ver) || echo "-" )"
  print_k "  IB Rate" "$([ -f /sys/class/infiniband/$ibn/ports/1/rate ] && (PGB=( $(cat /sys/class/infiniband/$ibn/ports/1/rate) ); echo "${PGB[0]}Gbps (${PGB[3]}" ) || echo "-" )"
done 
) || echo "                - NONE -"

#Memory
echo "--------------- Memory Info ------------------"
dmidecodeFile=/tmp/mem.dmidecode
dmidecode -t memory > $dmidecodeFile
grep -w "Locator" $dmidecodeFile | grep -v Bank | sed "s/ * / /g" | cut -d: -f 2 > /tmp/mem.locator
grep -w "Bank Locator" $dmidecodeFile | sed "s/ * / /g" | cut -d: -f 2 | sed -e "s/NODE /N/g" -e "s/ CHANNEL /C/g" -e "s/ DIMM /D/g" | awk '{if($3=="Unknown") printf "%s %s -Blank-\n", $1,$2)'> /tmp/mem.bank_locator
grep -w "Type:" $dmidecodeFile | grep -v Error | sed "s/ * / /g" | cut -d: -f 2 > /tmp/mem.type
grep -w "Manufacturer" $dmidecodeFile | sed "s/ * / /g" | cut -d: -f 2 > /tmp/mem.manufacturer
grep -w "Speed" $dmidecodeFile | sed "s/ * / /g" | cut -d: -f 2 > /tmp/mem.speed
grep -w "Serial" $dmidecodeFile | sed "s/ * / /g" | cut -d: -f 2 > /tmp/mem.serial
grep -w "Part Number" $dmidecodeFile | sed "s/ * / /g" | cut -d: -f 2 > /tmp/mem.part

echo  "Locator   Bank Locator             Speed     Type  ManuF.    Part Number        Serial Number"
paste /tmp/mem.locator /tmp/mem.bank_locator /tmp/mem.speed /tmp/mem.type  /tmp/mem.manufacturer  /tmp/mem.part  /tmp/mem.serial  | cat -A | sed "s/\^I/ /g" | sed "s/\$$//g"
  )' 
  echo "==================================================="
  echo ""
done 
