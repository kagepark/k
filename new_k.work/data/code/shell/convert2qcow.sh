src=$1
error_exit() {
    echo "$*"
    exit 1
}

[ ! -n "$src"  -o "$src" == "-h" -o "$src" == "--help" ] && error_exit "$(basename $0) <source image file>"
[ -f "$src" ] || error_exit "$src not found"
tmp_raw=$(mktemp -d /tmp/VDI2QCOW2-XXXXXXXX).raw
src_name=$(basename "$src")

if file "$src" | grep "VDI Image" >& /dev/null; then
    VBoxManage clonehd --format RAW "$src" $tmp_raw
elif file "$src" | grep "VMware4 disk image" >& /dev/null ;then
    qemu-img convert -f vmdk -O raw "$src" $tmp_raw
elif file "$src" | grep "x86 boot sector" >& /dev/null; then
    tmp_raw=$src
else
    error_exit "Unknown format"
fi
qemu-img convert -f raw $tmp_raw -O qcow2 ${src_name}.qcow2 && rm -f $tmp_raw

