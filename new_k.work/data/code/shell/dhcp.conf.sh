#dhcpd_conf=/etc/dhcpd.conf
dhcpd_conf=/home/kage/dhcpd.conf
tftp_dir=/ha_cluster/tftpboot
pxe_dir=ace
pxe_cfg_dir=$tftp_dir/$pxe_dir/pxelinux.cfg
host_conf_file=$pxe_cfg_dir/abc

host_conf_ace() {
	local cluster console kernel initrd init_file
	cluster=$1
	kernel=$2
	initrd=$3
	console=$4
	init_file="init=ace_init.sh"
#console=ttyS0,115200
	echo "default $cluster
	prompt 0
	label $cluster
    kernel $kernel
    append console=tty0 console=$console initrd=$initrd rw $init_file
    ipappend 3
	" > $host_conf_file
}

host_conf_localboot() {
}

host_conf_pxeinstall() {
    local server cluster console kernel initrd init_file
	server=$1
    cluster=$1
    kernel=$2
    initrd=$3
    console=$4
    init_file="init=linuxrc"
	ramdisk_size=$5
	os_type=$6
	net_dev=$7
	ks_file="/images/ks.cfg/ks.suse11sp1.cfg"
	os_dir="/images/suse11sp1"
	if [ "$os_type" == "suse" ]; then
	    ks_path="autoyast=nfs://$server$ks_file"
	    os_path="install=nfs://$server$os_dir"
	else
		ks_path="ks=nfs:$server:$ks_file"
	fi

	[ -n "$ramdisk_size" ] || ramdisk_size=8192
	
	echo "label $cluster
  kernel $kernel
    append load_ramdisk=1 initrd=$initrd splash=silent showopts ramdisk_size=$ramdisk_size $init_file $ks_path netdevice=$net_dev $os_path
	" > $host_conf_file

}


base_dhcp_conf() {
	local gw subnet netmask range1 range2
	gw=10.10.0.3
	subnet=10.10.0.0
	netmask=255.255.0.0
	range1=10.10.255.1
	range2=10.10.255.254

    echo "# DHCP for pxe boot server
ddns-update-style interim;
allow booting;
allow bootp;

class "pxeclients" {
    match if substring(option vendor-class-identifier, 0, 9) = "PXEClient";
    filename "$pxe_dir/pxelinux.0";
    option routers $gw;
    option domain-name-servers $gw;
}

subnet $subnet netmask $netmask {
    range $range1 $range2;
    next-server $gw;
    option routers $gw;
    option domain-name-servers $gw;
    filename "$pxe_dir/pxelinux.0";
}
    " > $dhcpd_conf
}

pxe_cfg() {
	local mac host
	mac=$1
	host=$2
	cd $pxe_cfg_dir && ln -s $host 01-$(echo $mac | sed "s/:/-/g" | tr "[:upper:]" "[:lower:]") || return 1
}

add_dhcp_conf() {
	local hostname ip mac
	hostname=$1
	ip=$2
	mac=$3

	[ -f $dhcpd_conf ] || return 1
	pxe_cfg $mac $host_conf_file

	echo "
host $hostname {
	hardware ethernet $mac;
    fixed-address $ip;
}
    " >> $dhcpd_conf

}

