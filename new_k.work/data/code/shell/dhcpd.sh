source /ha_cluster/dhcpd/dhcpd_conf.sh

error_exit() {
   echo $*
   exit 1
}

acefs=/acefs/global
[ -d $acefs ] || error_exit "ACEFS not found"


netmask=$(cat $acefs/netmask)
network1=$(cat $acefs/network1)

gw=$(_k_add_ip $(cat $acefs/network1) 3)

dhcpd_arg=""

for ii in network1 network2 mgmt_net1 mgmt_net2; do
    ace_network=$(cat $acefs/$ii)
    [ -n "$ace_network" ] || continue
    network=$(_k_dhcpd_network $ace_network $netmask)
    next_server=$(_k_add_ip $ace_network $(cat /acefs/self/server_id))
    end_ip_num=$(( $(_k_ip2num $network ) + 65534))
    end_ip=$(_k_num2ip $end_ip_num)
    start_ip=$(_k_num2ip $(( $end_ip_num - $(cat $acefs/max_servers) +1)) )

    dhcpd_arg="$dhcpd_arg -s $network:$netmask:$start_ip:$end_ip:$gw:$next_server"
done

#<network>:<netmask>:<start ip>:<end ip>:<gateway>
dhcpd_conf=/etc/dhcpd.conf
dhcpd_ha_conf=/etc/dhcpd.conf

umount $dhcpd_conf >& /dev/null
_k_dhcpd_conf -g $gw $dhcpd_arg -h /opt/ace/etc/dhcpd.hosts  > $dhcpd_ha_conf
mount -o bind $dhcpd_ha_conf $dhcpd_conf
/etc/init.d/dhcpd start
