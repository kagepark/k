#!/bin/sh
_K_DHCP_TAG=ace

#_k_dhcpd_network() {
#    local netmask ip
#    ip=$1
#    netmask=$2
#
#    bit_count=$(echo $netmask  | sed -e "s/\./ /g" -e "s/0//g" | wc -w)
#    network_arr=( $(echo $ip| sed "s/\./ /g") )
#    network=$( for ((ii=0; ii<4; ii++)); do
#        (($ii<$bit_count)) && echo -n "${network_arr[$ii]}" || echo -n 0
#        (($ii<3)) && echo -n "."
#    done)
#    echo $network
#}

_k_dhcpd_subnet() {
    local network netmask endip
    network=$1
    netmask=$2
    startip=$3
    endip=$4
    gateway=$5
    nextserver=$6

    echo "
subnet $network netmask $netmask {
   range $startip $endip;
   next-server $nextserver;
   option routers $gateway;
   option domain-name-servers $gateway;
   filename \"$_K_DHCP_TAG/pxelinux.0\";
}
"

}

_k_dhcpd_conf() {
   #-g <gateway>
   #-s <network>:<netmask>:<start ip>:<end ip>:<gateway>
   #-h <host file>

   local opt dhcpd_conf gateway subnet hostfile ss hh
   dhcpd_conf=/etc/dhcpd.conf
   ss=0
   hh=0
   opt=($*)
   if (( ${#opt[*]} == 0 )); then
       echo "$(basename $0) -g <gateway> -s <network>:<netmask>:<start ip>:<end ip>:<gateway> -h <hostfile>"
       exit 1
   fi
   for ((ii=0;ii<${#opt[*]};ii++)); do
       if [ "${opt[$ii]}" == "-g" ]; then
          ii=$(($ii+1))
          gateway=${opt[$ii]}
       elif [ "${opt[$ii]}" == "-s" ]; then
          ii=$(($ii+1))
          subnet[$ss]=${opt[$ii]}
          ss=$(($ss+1))
       elif [ "${opt[$ii]}" == "-h" ]; then
          ii=$(($ii+1))
          hostfile[$hh]=${opt[$ii]}
          hh=$(($hh+1))
       fi
   done
   [ ! -n "$gateway" -o ! -n "$subnet" ] && return 1
   echo "# Advanced Cluster Engine DHCP configuration file

ddns-update-style interim;
allow booting;
allow bootp;

class \"pxeclients\" {
   match if substring(option vendor-class-identifier, 0, 9) = \"PXEClient\";
   filename \"$_K_DHCP_TAG/pxelinux.0\";
   option routers $gateway;
   option domain-name-servers $gateway;
}
"

    for ((jj=0; jj<${#subnet[*]}; jj++)); do
        _k_dhcpd_subnet $(echo ${subnet[$jj]} | sed "s/:/ /g")
    done

    for ((jj=0; jj<${#hostfile[*]}; jj++)); do
        echo "include \"${hostfile[$jj]}\";"
    done
}

_k_ip2num() {
    local ip
    ip=( $(echo $1 | sed 's/\./ /g') )
    echo $(( $((256*256*256*${ip[0]})) + $((256*256*${ip[1]})) + $((256*${ip[2]})) +${ip[3]} ))
}

_k_num2ip() {
    local num
    num=$1
    echo $(( $(( $num/(256*256*256) ))% 256)).$(( $(($num/(256*256) ))%256)).$(( $(($num/256)) % 256)).$(($num%256))
}

_k_add_ip() {
    local ip num
    ip=$1
    num=$2
    _k_num2ip $(( $(_k_ip2num $ip) + $num ))
}

_k_dhcpd_conf $*
