#!/bin/bash
# 2006       KGP Initial version
# 2010       KGP Tested more many switch and update few code
# 2011.07    KGP Convert to ace style format
# 2011.10    KGP Tested in APP and Ethernet only ACE
# 2015.10.16 KGP Tested in Ethernet Only ACE ( ACE 3.2 - 3328 ) (Add "ace put" command of Ethernet only ACE style format)
   #             Change name from snmp_get_mac.sh to ace_ethstatd
# 2015.10.20 KGP Add right connection check and working on sub-mgmt node.
#                 Add log rotate & example db format
# 2016.01.14 KGP Add test function and upgrade to two more groups cluster
# 2016.01.16 KGP Readable defined switch ports, fix bugs
# 2016.01.18 KGP Available multi switches group, fix minor bugs
# 2016.02.17 KGP Update missing switch issue, fix port number sort issue.
# 2016.03.10 KGP Enhance speed SNMP (re-write code)
#      SNMP gathering speed up from 45~70sec/switch to 3~5sec/switch
# 2016.03.11 KGP Add state update code for GUI
# 2016.03.16 KGP Add mrate value update code for GUI
# 2016.03.28 KGP Fix a bug of switch state update in initial ACE cluster
# 2016.03.29 KGP show mac address's number in the port when detect switch.
# 2016.07.28 KGP Fixed down-state monitoring of a ethernet switch
# 2016.08.01 KGP Add mac filtering function when detected several mac address in a port

#base_interval=300
base_interval=60
boot_mon_interval=30
log_size=$((1024*1024*10)) # 10Mb 

## Example DB
#global {
#    cs_net1_device 'eth0' # must use 10.10.x.x device name
#    gs_net1_device 'eth2' # must use 10.10.x.x device name
#    manufacturing 0
#    num_switches 2
#    ms_net1_device 'eth4' # ethX or enXXXX
#    identify_servers 2
#}
#
#ethswitches {
#    ethswitch-0001 {
#        id 1
#        subnet 1
#        ipaddr '10.10.0.200'
#        group_id 1
#        rack 0
#        slot 0
#        num_ports 68
#        ports {
#            1 { id=1 type='server' rate=1 dest_id=5 dest_port=1 }
#            2 { id=2 type='server' rate=1 dest_id=6 dest_port=1 }
#            67 { id=67 type='server' rate=1 dest_id=1 dest_port=1 }
#            68 { id=68 type='server' rate=1 dest_id=2 dest_port=1 }
#        }
#    }
#}
#servers {
#  server-0001 {
#      id 1
#      type management
#      group_id 1
#      peer_id 1
#      rack 1
#      nics 1
#      slot 14
#      nicports {
#          1 { id=1 nic=1 nic_port=5 dest_id=1 dest_port=67 }
#      }
#  }
#
# The port number is rail number
# 1 { : rail 1
# 2 { : rail 2
#
# The nic= just 1 in all system
# The nic_port='s number is NIC number (ms_net1_device's number)
#   eth0: 1
#   eth2: 3
#   eth4: 5
#
#
# If you need mac filtering function then
# Add a mac filter file to /acefs/clusters/sysgrp/mount/ethstatd/<group id>.filter_out
# 1. mkdir /acefs/clusters/sysgrp/mount/ethstatd
# 2. echo <BMC mac or some of filtering mac address> >> /acefs/clusters/sysgrp/mount/ethstatd/<group id>.filter_out
# ** it should be add lower case Alphabet mac address(xx:xx:xx:xx:xx:xx) to the <group id>.filter_out file 
# the <group id> is the compute node's <group id>

dbg=2
## dbg : Debug level
# 1 : no make dbg file 
# 2 : debug mode (default)
# 3 : detail debug (log file size, show mac address )
# 4 : print to screen(console)
# 5 : print to screen(console) with "set -x"

## Port number
# 0 : down
# 1 : up
# 2 : unknown (connected something but not found mac)
# 3 : connected switch
# '*' before port number : this port defined in ACEFS.

## Format version
ver=2
#ver=1 : Old version : more heavy
#ver=2 : New format (using group_id in ethswitch DB) : more lite

if [ "$1" == "--help" -o "$1" == "-h" -o "$1" == "help" ]; then
    echo "
Detail parameter and sample DB are in head side in $(basename $0) file.

test snmp to switch
$(basename $0) --test <switch IP> or <switch name>
    "
    exit
fi

#####################################################
if [ -f /.ace/ace_init.sh ]; then
    acefs=/.ace/acefs
    ace_bin=/.ace/bin/ace
    log_dir=/.ace/tmp
elif [ -d /acefs/ethswitches ]; then
    acefs=/acefs
    ace_bin=ace
    log_dir=/tmp
fi
eth_db=$acefs/ethswitches
my_grp_id=$(cat $acefs/self/group_id)
filter_out_file=$acefs/clusters/sysgrp/mount/ethstatd/${my_grp_id}.filter_out

if [ "$1" == "--sim" ]; then
    sim=1
    dbg=4
    shift 1
    sim_val=$1
    if [ -n "$sim_val" ]; then
        base_interval=$sim_val
        boot_mon_interval=$sim_val
        shift 1
    fi
fi

dbg_log() {
   local mode
   [ -n "$dbg" ] || dbg=1
   mode=$1
   shift 1
   if [ $dbg -lt 4 ]; then
       if [ "$mode" == "-b" ]; then
           echo >> $log_dir/ace_ethstatd.dbg
           mode=""
       elif [ "$mode" == "-bn" ]; then
           echo >> $log_dir/ace_ethstatd.dbg
           mode="-n"
       fi
       echo $mode "$*" >> $log_dir/ace_ethstatd.dbg
   else
       if [ "$mode" == "-b" ]; then
           echo
           mode=""
       elif [ "$mode" == "-bn" ]; then
           echo
           mode="-n"
       fi
       echo $mode "$*"
   fi
}

if [ "$dbg" == "5" ]; then
    set -x
else
    if [ "$1" != "--test" ]; then
        if (( "$dbg" < 4 )); then
            [ -f $log_dir/ace_ethstatd.log ] && mv $log_dir/ace_ethstatd.log $log_dir/ace_ethstatd.log.1
            [ -f $log_dir/ace_ethstatd.dbg ] && mv $log_dir/ace_ethstatd.dbg $log_dir/ace_ethstatd.dbg.1
            exec >$log_dir/ace_ethstatd.log 2>&1
        fi
    fi
fi
echo "Start $(basename $0) at $(date)" 

_k_mac_hex2int() {
    if [[ "$1" == "UNKNOWN" || "$1" == "sw" ]] ; then
        return 1
    fi
    printf "%d " "0x"${1//:/}
}

id2num() {
    local ip
    if [[ -z "$1" ]] ; then
        return 1
    fi
    ip=(${1//./ })
    echo $(( $((255*255*255*255*255*${ip[0]})) + $((255*255*255*255*${ip[1]})) + $((255*255*255*${ip[2]})) + $((255*255*${ip[3]})) + $((255*${ip[4]})) + ${ip[5]} ))
}

log_rotate() {
    for ii in $log_dir/ace_ethstatd.dbg $log_dir/ace_ethstatd.log; do
        if [ -f $ii ]; then
            if (( $(stat -c "%s" $ii) > $log_size )); then
            (($dbg >= 3)) && echo "$ii: $(stat -c "%s" $ii) > $log_size"
               mv $ii ${ii}.1
               touch $ii
            fi
        fi
    done
}

# func get_mac
get_mac() {
  # mac : .1.3.6.1.2.1.17.4.3.1.1.<identify>
  local identify target
  target=$1
  identify=$2
  if [ -n "$identify" -a -n "$target" ]; then
     get_mac_str=$(snmpwalk -O n -v2c -c public $target .1.3.6.1.2.1.17.4.3.1.1.$identify 2> /dev/null) || return 1 
     mac=( $get_mac_str )
     if [ "${mac[2]}" == "Hex-STRING:" ]; then
        printf "%s:%s:%s:%s:%s:%s\n" ${mac[3]} ${mac[4]} ${mac[5]} ${mac[6]} ${mac[7]} ${mac[8]}
     else
        printf "%s\n" "unknown"
     fi
  else
     printf "%s\n" "unknown"
  fi
}

mac_format() {
   local mac_addr
   mac_addr=$1
   [ -n "$mac_addr" ] || return 1
   echo $(for ii in $(echo $mac_addr | sed "s/:/ /g"); do
      [ "$(echo $ii | wc -c)" == "2" ] && echo 0$ii || echo $ii
   done) | sed "s/ /:/g"
}

#Get switch's mac address
get_sw_mac_addr() {
  local target sw_mac_addr mac_state
  target=$1
  mac_state=$(snmpwalk -O n -v2c -c public $target .1.3.6.1.2.1.2.2.1.6.1 2>/dev/null) || return 1
  sw_mac_addr=$(echo $mac_state | awk '{print $4}')
  [ -n "$sw_mac_addr" ] || return 1
  mac_format $sw_mac_addr
}

# func get_sw_port
get_sw_port_name() {
  # port name : .1.3.6.1.2.1.31.1.1.1.1.<port>
  local port target str
  target=$1
  port=$2
  [ ! -n "$target" -o ! -n "$port" ] && return 1
  str=$(snmpwalk -O n -v2c -c public $target .1.3.6.1.2.1.31.1.1.1.1.$port 2>/dev/null) || return 1
  echo $str | awk '{print $4}'
}

get_sw_ports_status() {
   local target snmp_id
   target=$1
   [ -n "$target" ] || return 1
   snmp_id=".1.3.6.1.2.1.2.2.1.8"
   #snmpwalk -O n -v2c -c public $target $snmp_id 2>/dev/null | while read line; do
   #    echo $line | awk '{print $1}' | awk -v state=$(echo $line | grep down >& /dev/null && echo d || echo u) -F. '{printf "%s.%s ",$12,state}'
   #done | sort -n

   while read line; do
       echo $line | awk '{print $1}' | awk -v state=$(echo $line | grep down >& /dev/null && echo d || echo u) -F. '{printf "%s.%s ",$12,state}'
   done < <(snmpwalk -O n -v2c -c public $target $snmp_id 2>/dev/null || return 1)
   return $?
}

get_sw_port_status() {
   # port status : .1.3.6.1.2.1.2.2.1.8.<port>
   local port target port_state
   target=$1
   port=$2
   [ ! -n "$target" -o ! -n "$port" ] && return 1
   snmpwalk -O n -v2c -c public $target .1.3.6.1.2.1.2.2.1.8.${port} | grep "up" >& /dev/null  && echo "${port}.u" || echo "${port}.d"
}

get_identify_from_sw_port() {
   # get port num from identify : .1.3.6.1.2.1.17.4.3.1.2.<identify>
   local port target
   target=$1
   port=$2
   [ -n "$port" ] || return 1
   snmpwalk -O n -v2c -c public $target .1.3.6.1.2.1.17.4.3.1.2 | awk -v port=$port '{if($4==port) print $1}' | cut -c 25-
}

put_state() {
   local object value
   object=$1
   value=$2
   mode=$3
   [ ! -n "$object" -o ! -n "$value" ] && return 1
   [ "$value" == "sw" -o "$value" == "UNKNOWN" ] && return 1

   if [ -f $acefs/${object} ]; then
       if echo $value | grep "^0x" >& /dev/null; then
           [ "$(get_hexmac $(cat $acefs/${object}))" == "$value" ] && return 0
       else
           [ "$(cat $acefs/${object})" == "$value" ] && return 0
       fi
   fi
   [ -d "$(dirname $acefs/${object})" ] || return 1
#   [ "$mode" == "direct" ] &&  $ace_bin put ${object}=${value} || \
   putargs[${object}]=${value}
}

get_local_eth_mac() {
   local eth_dev
   eth_dev=$(cat $acefs/global/ms_net1_device)
   [ -f /sys/class/net/$eth_dev/address ] || return 1
   cat /sys/class/net/$eth_dev/address
}

get_mgmt_sw() {
   local mgmt_sw dest_id conn_id dest_port mgmt_sw_arr my_group_sw
   #grp_id=$(cat $acefs/self/server/group_id)
   mgmt_sw=$(for ii in $(ls $acefs/self/server/nicports 2>/dev/null | sort -n); do
       dest_port=$(cat $acefs/self/server/nicports/$ii/dest_port)
       dest_id=$(cat $acefs/self/server/nicports/$ii/dest_id)
       conn_id=$(cat $acefs/ethswitches/ethswitch-$(printf "%04d" $dest_id)/ports/$dest_port/server/id)
       if [ "$(cat $acefs/self/server/id)" == "$conn_id" ]; then
           sw_mgmt_mac=$(cat $acefs/ethswitches/ethswitch-$(printf "%04d" $dest_id)/ports/$dest_port/conn_guid0)
           if [ "$sw_mgmt_mac" != "0x0000000000000000" ]; then
               if [ "$(get_hexmac $sw_mgmt_mac)" == "$(hexmac $(get_local_eth_mac))" ]; then 
                   echo "$dest_id $dest_port"
                   break
               fi
           fi
       fi
   done)

   if [ "$ver" == "2" ]; then
       my_group_sw=$(awk -v grp=$my_grp_id '{if($1==grp) print FILENAME}' $acefs/ethswitches/*/group_id | awk -F/ '{print $4}')
   else
       my_group_sw=$(for jj in $(for ii in $(awk -v grp=$my_grp_id '{if($1==grp) print FILENAME}' $acefs/servers/*/group_id 2>/dev/null | awk -F/ '{print $4}'); do cat $acefs/servers/$ii/nicports/1/switch/id; done | sort -n | uniq); do
         echo -n "ethswitch-$(printf "%04d" ${jj}) "
       done)
   fi
   mgmt_sw_arr=($mgmt_sw)
   if [ "${#mgmt_sw_arr[*]}" == "2" ]; then
       echo $(echo "ethswitch-$(printf "%04d" ${mgmt_sw_arr[0]}) $my_group_sw" | sed "s/ /\n/g" | sort | uniq)
   else
       echo
       echo "Not found right connected switch from mgmt"
       return 1
   fi
}

get_group_sw() {
    local grp_sw 
    #grp_id=$(cat $acefs/self/server/group_id)
    if [ "$my_grp_id" == "$(cat $(awk '{if($1=="13") print FILENAME}' $acefs/servers/server-000[1-2]/state | sed "s/\/state$/\/group_id/g"))" ]; then
        echo "This group is same as mgmt group. So, not running on this group server"
        exit 2
    fi
    if [ "$(cat $acefs/self/server/peer_id)" == "2" ]; then
        srv_id=$(cat $acefs/self/server/id)
        [ -n "$srv_id" ] || return 1
        [ "$(cat $acefs/servers/server-$(printf "%04d" $(( $srv_id - 1)))/state)" == "13" ] && return 3
    fi
    if [ "$ver" == "2" ]; then
        awk -v grp=$my_grp_id '{if($1==grp) print FILENAME}' $acefs/ethswitches/*/group_id | awk -F/ '{print $5}'
    else
        for jj in $(for ii in $(awk -v grp=$my_grp_id '{if($1==grp) print FILENAME}' $acefs/servers/*/group_id 2>/dev/null | awk -F/ '{print $5}'); do cat $acefs/servers/$ii/nicports/1/switch/id; done | sort -n | uniq); do
             echo -n "ethswitch-$(printf "%04d" ${jj}) "
        done
    fi
}

get_value() {
    local arg find
    find=$1
    for arg in $(cat /proc/cmdline); do
        if echo $arg | grep "^${find}=" >/dev/null 2>&1; then
            echo $arg | sed "s/^${find}=//g"
             return
         fi
    done
}

hexmac() {
  [ -n "$1" ] || return 1
  echo 0x$(echo $1 | sed 's/://g' | tr '[:upper:]' '[:lower:]')
}

get_hexmac() {
  [ -n "$1" ] || return 1
  echo $1 | sed "s/^0x0000/0x/g"
}

id_sort() {
    local target ids tmp found
    target=$1
    shift 1
    ids=($*)
    found=0
    if [ -f $filter_out_file ]; then
	new_ids=""
        for ((ii=0;ii<${#ids[*]};ii++)); do
            if ! grep "^$(echo ${ids[$ii]} | tr '[:upper:]' '[:lower:]')$" $filter_out_file >& /dev/null; then
                new_ids="$new_ids ${ids[$ii]}"
            fi
        done
        ids=($new_ids)
    fi

    if [ -n "$target" ]; then
        tmp=$(_k_mac_hex2int $(get_mac "$target" "${ids[0]}"))
    else
        tmp=$(_k_mac_hex2int "${ids[0]}")
    fi
    for ((ii=1;ii<${#ids[*]};ii++)); do
         if [ -n "$target" ]; then
            (( $(_k_mac_hex2int $(get_mac "$target" "${ids[$ii]}")) < $tmp )) && found=$ii
         else
            (( $(_k_mac_hex2int "${ids[$ii]}") < $tmp )) && found=$ii
         fi
    done
    echo ${ids[$found]} 

}

put_sw_state() {
  local switch
  switch=$1
  [ -n "$switch" ] || return 1
  target=$(cat $eth_db/$switch/ipaddr)
  [ ! -n "$target"  -o "$target" == "0" ] && continue
  if ! sw_mac_addr=$(get_sw_mac_addr $target); then
#     dbg_log "" "$switch have no response from SNMP"
     put_state ethswitches/$switch/state 1 
     return 1
  fi
  if [ ! -n "$sw_mac_addr" ]; then
#      put_state ethswitches/$switch/state 1 "direct"
      put_state ethswitches/$switch/state 1 
      return 1
#      echo "D"
  else
      sw_hexmac=$(hexmac $sw_mac_addr) || return 1
      put_state ethswitches/$switch/guid $sw_hexmac || return 1
      put_state ethswitches/$switch/state 2 || return 1
#      put_state ethswitches/$switch/guid $sw_hexmac "direct" || return 1
#      put_state ethswitches/$switch/state 2 "direct" || return 1
#      echo "U"
  fi
}

put_group_db() {
   local sw port target identify mac_addr 
   dbg_log "-bn" "Other switches (connected sub-mgmt(sysgrp)) status :"
   dbg_log "-bn" "  SU's SW log (tail -f /.ace/tmp/ace_ethstatd.dbg) in each sub-mgmt"
   #my_group_id=$(cat $acefs/self/group_id)
   for sw in $(for ii in $(grep -v "^$my_grp_id$" $acefs/clusters/sysgrp/hosts/*/server/group_id | awk -F/ '{print $6}'); do cat $acefs/clusters/sysgrp/hosts/$ii/server/nicports/1/switch/name; done | sort | uniq); do
#        dbg_log -bn "$sw ($(put_sw_state $sw)): "
        put_sw_state_str=$(put_sw_state $sw) && pgstat=U || pgstat=D
        #[ -n "$put_sw_state_str" ] && pgstat="$put_sw_state_str"
        dbg_log -bn "$sw ($pgstat): "
        if [ "$pgstat" == "D" ]; then
            dbg_log "" "Down or no response from SNMP"
            put_state ethswitches/$sw/state 1
            continue
        fi
        target=$(cat $eth_db/$sw/ipaddr)
        [ ! -n "$target"  -o "$target" == "0" ] && continue
        ports=$(for ii in $(grep group $acefs/ethswitches/$sw/ports/*/server/type | awk -F: '{print $1}' | sed "s/type/nicports\/1\/dest_port/g"); do cat $ii; done | sort -n)
        for port in $ports; do
           if [ "$(get_sw_port_status $target $port)" == "down" ]; then
              dbg_log -n "$port:0 "
#              put_state ethswitches/$sw/ports/$port/conn_guid0 0x0000000000000000
              continue
           fi

           identify=($(get_identify_from_sw_port "$target" "$port"))
           if (( ${#identify[*]} > 3 )); then
               (( $dbg >= 3 )) && dbg_log -n "$port:(Looks connected other switch) " || dbg_log -n "$port:3 "
               continue
           elif (( ${#identify[*]} > 1 && ${#identify[*]} <= 3 )); then
               identify=($(id_sort "$target" ${identify[*]}))
           fi
           mac_addr=$(get_mac "$target" "${identify[0]}")
           if [ "$mac_addr" == "unknown" ]; then
              dbg_log -n "$port:2 "
           else
              (( $dbg >= 3 )) && dbg_log -n "$port:($mac_addr) " || dbg_log -n "$port:1 "
              port_hexmac=$(hexmac $mac_addr)
              put_state ethswitches/$sw/ports/$port/conn_guid0 $port_hexmac
           fi
        done
   done
   dbg_log ""
}

get_snmp_sw() {
    local target port_identify idv_mac port_mac port_speed port_id max_port
    target=$1
    [ -n "$target" ] || return 1

    if [ -d $eth_db/$target ]; then
        max_port=$(cat $eth_db/$target/num_ports)
        target=$(cat $eth_db/$target/ipaddr)
        [ ! -n "$target"  -o "$target" == "0" ] && return 1
    fi


    ip_format="${target//[^.]}"
    [ "${#ip_format}" == "3" ] || return 1

    # Speed port
    snmp_id=".1.3.6.1.2.1.31.1.1.1.15"
    for ii in $(snmpwalk -O n -v2c -c public $target ${snmp_id} | awk '{printf "%s:%s\n",$1,$4}' | sed "s/${snmp_id}.//g"); do
       port_id=$(echo $ii| awk -F: '{print $1}')
       port_speed[$port_id]=$(echo $ii|awk -F: '{print $2}')
    done

    # Identify-mac
    snmp_id=".1.3.6.1.2.1.17.4.3.1.1"
    for ii in $(snmpwalk -O n -v2c -c public $target ${snmp_id} | sed -e "s/${snmp_id}.//g" -e "s/Hex-STRING: //g" -e "s/ = /=/g" -e "s/ $//g" -e "s/ /:/g"); do
        idv=$(id2num $(echo $ii | awk -F= '{print $1}'))
        idv_mac[$idv]=$(echo $ii | awk -F= '{print $2}')
    done

    port_id=""
    # port identify
    snmp_id=".1.3.6.1.2.1.17.4.3.1.2"
    idv_arr=($(snmpwalk -O n -v2c -c public $target $snmp_id | \
        sed -e "s/${snmp_id}.//g" -e "s/= INTEGER://g" | \
        awk '{printf "%s %s\n",$2,$1}'))
    for ((ii=0; ii<${#idv_arr[*]}; ii=ii+2)); do
        port_id=${idv_arr[ii]}
        idv_id=${idv_arr[$(($ii+1))]}
        port_mac[$port_id]="${port_mac[$port_id]} ${idv_mac[$(id2num $idv_id)]}"
    done
    port_mac[0]=$port_id

    if ! get_sw_ports_status_str=$(get_sw_ports_status "$target"); then
         dbg_log "" "$target have no response from SNMP(2)"
         return 1
    fi
    for ports in $(echo "$get_sw_ports_status_str" | sort -n); do
        id=$(echo $ports | awk -F. '{print $1}')
        if [ -n "$max_port" ]; then
           (( $id > $max_port )) && break
        fi
        if [ "$(echo $ports | awk -F. '{print $2}')" == "d" ];then
            echo "$id|down|0"
        else
            mac=(${port_mac[$id]})
            if (( ${#mac[*]} > 3 )); then
                [ "$dbg" == "4" -o "$dbg" == "3" ] && echo "$id|sw|$(echo ${mac[*]}| sed "s/ /,/g")" || echo "$id|sw|${#mac[*]}"
                continue
            elif (( ${#mac[*]} > 1 && ${#mac[*]} <= 3 )); then
                mac=($(id_sort "" "${mac[*]}"))
            fi
            [ -n "$mac" ] && echo "$id|$mac|${port_speed[$id]}" || echo "$id|down|${port_speed[$id]}"
        fi
    done
}

put_sw_db() {
  local switches group_mode idle ace_port
  idle=0
  ace_port=0
  group_mode=$1
  shift 1
  switches=$*
  [ -n "$switches" ] || return 1

  dbg_log "-b" "My Switches :"
  for sw in $switches; do
    put_sw_state $sw && psstate=U || psstate=D
#    dbg_log -n "$sw ($(put_sw_state $sw)): "
    dbg_log -n "$sw ($psstate): "
#    target=$(cat $eth_db/$sw/ipaddr)
#    [ ! -n "$target"  -o "$target" == "0" ] && continue

    if [ "$psstate" == "D" ]; then
    if [ "$(cat $eth_db/$sw/state)" == "1" ]; then
#       dbg_log "No response of SNMP"
       dbg_log "Currnetly down state switch in DB"
       idle=1
       continue
    fi
    fi

    port_info=($(get_snmp_sw "$sw"))
    for ((ii=0;ii<${#port_info[*]};ii++)); do
            port=$(echo ${port_info[$ii]}| awk -F\| '{print $1}')
            if [ -d $acefs/ethswitches/$sw/ports/$port ]; then
               dbg_log -n "*"
               ace_port=1
            else
               ace_port=0
            fi
            dbg_log -n "$port:"
            mac=$(echo ${port_info[$ii]}| awk -F\| '{print $2}')
            speed=$(echo ${port_info[$ii]}| awk -F\| '{print $3}')
            [ "$ace_port" == "1" -a "$sim" == "1" ] && server_name=$([ -f $acefs/ethswitches/$sw/ports/$port/server/name ] && cat $acefs/ethswitches/$sw/ports/$port/server/name || echo "-") || server_name=""

            if [ "$mac" == "down" ]; then
               [ "$ace_port" == "1" ] && idle=1
               dbg_log -n "0$([ -n "$server_name" ] && echo ":$server_name") "
#               put_state ethswitches/$sw/ports/$port/conn_guid0 0x0000000000000000
               # For ACEGUI
               put_state ethswitches/$sw/ports/$port/state 1
            elif [ "$mac" == "unknown" ]; then
               [ "$ace_port" == "1" ] && idle=1
               dbg_log -n "2$([ -n "$server_name" ] && echo ":$server_name") "
               # For ACEGUI
               put_state ethswitches/$sw/ports/$port/state 4
               put_state ethswitches/$sw/ports/$port/mrate $speed
            elif [ "$mac" == "sw" ]; then
               dbg_log -n "3($speed) "
            else
               (( $dbg >= 3 )) && dbg_log -n "($mac$([ -n "$server_name" ] && echo ":$server_name")) " || dbg_log -n "1 "
               port_hexmac=$(hexmac $mac)
               put_state ethswitches/$sw/ports/$port/conn_guid0 $port_hexmac
               # For ACEGUI
               put_state ethswitches/$sw/ports/$port/state 3
               put_state ethswitches/$sw/ports/$port/mrate $speed
            fi
    done
    dbg_log "" ""
  done
  return $idle
}

if [ "$1" == "--test" ]; then
   if [ ! -n "$2" ]; then
        echo "$(basename $0) --test <SW IP>"
        exit
   fi
   target=$2
   if [ -d $eth_db/$target ]; then
       sw_path=$eth_db/$target
       sw_ip=$(cat $sw_path/ipaddr)
   else
       sw_path=$(awk -v ip=$target '{if($1==ip) print FILENAME}' $acefs/ethswitches/ethswitch-*/ipaddr)
       if [ ! -n "$sw_path" ]; then
          echo "$target not found in DB"
          exit 1
       fi
       sw_path=$(dirname $sw_path)
       sw_ip=$target
   fi
   port_to_group_server=$(cd $sw_path/ports && grep group */server/type | awk -F/ '{print $1}')
   echo "My ($(hostname)) group ID"
   echo "  Group ID : $my_grp_id"
   echo
   echo "Switch DB Info"
   __sw_name=$(cat $sw_path/name)
   echo "  Name          : $__sw_name"
   echo "  IP            : $sw_ip"
   echo "  Rack          : $(cat $sw_path/rack)"
   echo "  GUID          : $(cat $sw_path/guid)" 
   if [ -n "$port_to_group_server" ]; then
     for gsp in $port_to_group_server; do
        echo "  Group SRV     : $(cat $sw_path/ports/$gsp/server/name)"
        echo "   Hostname     : $(cat $sw_path/ports/$gsp/server/host/name)"
        echo "   IP Addr      : $(cat $sw_path/ports/$gsp/server/ip1)"
        echo "   IPMI IP      : $(cat $sw_path/ports/$gsp/server/ipmi1)"
        echo "   Group ID     : $(cat $sw_path/ports/$gsp/server/group_id)"
        echo "   SNMP ID of SW: $gsp"
     done
   fi
   echo
   echo "SNMP output"
   port_info=($(get_snmp_sw "$target"))

   for ((ii=0;ii<${#port_info[*]};ii++)); do
       port=$(echo ${port_info[$ii]}| awk -F\| '{print $1}')
       mac=$(echo ${port_info[$ii]}| awk -F\| '{print $2}')
       speed=$(echo ${port_info[$ii]}| awk -F\| '{print $3}')
       echo -n "$(get_sw_port_name $sw_ip $port)(SNMP ID:$port)<==> "
       if [ -d $acefs/ethswitches/$__sw_name/ports/$port ]; then
           echo "$mac ($speed) ($(cat $acefs/ethswitches/$__sw_name/ports/$port/server/name)$([ -f $acefs/ethswitches/$__sw_name/ports/$port/server/host/name ] && echo "($(cat $acefs/ethswitches/$__sw_name/ports/$port/server/host/name))"))"
       else
           echo "$mac ($speed)"
       fi
   done   
   exit 
fi

while [ ! -f $acefs/global/identify_servers ]; do
    echo "Wait identify_servers parameter...."
    sleep 2
done
if [ "$(cat $acefs/global/identify_servers)" != "2" ]; then
    echo "This ace is not Ethernet Only ACE"
    exit
fi


if [ "$(cat $acefs/self/server/type)" == "group" ]; then
    ip=$(get_value ip)
    ip_server=$(echo $ip | awk -F : '{print $2;}')
    export ACE_HOST=$ip_server
fi

unset putargs
declare -A putargs
sleep_time=0

#Initial to down
if [ "$(cat $acefs/self/server/type)" == "management" ]; then
   dest_id=$(cat $acefs/self/server/nicports/1/dest_id)
   my_sw_name=ethswitch-$(printf "%04d" $dest_id)
   init_state_path=$acefs/ethswitches/$my_sw_name/state
   for sw in $(ls $acefs/ethswitches); do
       if [ "$sw" == "$my_sw_name" ]; then
          put_sw_db "" $my_sw_name && continue
       fi
       put_state ethswitches/$sw/state 1
   done
fi

while [ 1 ]; do
  cmdargs=""
  for arg in ${!putargs[@]} ; do
    cmdargs="${cmdargs} ${arg}=${putargs[${arg}]}"
  done
  if [ "$sim" == "1" ]; then
     echo "+ $ace_bin put ${cmdargs}"
  else
     if [[ -n "${cmdargs}" ]] ; then
        $ace_bin put ${cmdargs}
     fi
  fi
  unset putargs
  declare -A putargs
  if (( ${sleep_time} > 0 )) ; then
    sleep ${sleep_time}
  fi

  log_rotate
  dbg_log "" "$(date +"%m/%d %H:%M") "

  if [ ! -d $eth_db ]; then
     dbg_log -n "Not ready $acefs/ethswitches information, check database file or wait..."
     sleep_time=10
     continue
  fi

  if [ "$(cat $acefs/self/server/type)" == "management" ]; then
      #unknown switch initialization
      if [ "$(cat $init_state_path)" == "2" ]; then
        for ii in $(ls $eth_db); do
          if [ -n "$(grep "0x0000000000000000" $eth_db/$ii/guid)" -o "$(cat $eth_db/$ii/state)" != "2" ]; then
             dbg_log "-n" "Initial $ii state : "
             put_sw_state $ii && dbg_log "" "Up" || dbg_log "" "Down or no response from SNMP"
          fi
        done
        (( $(cat /acefs/global/sub_mgmt) > 0 )) && put_group_db
      fi

      if ! switches=$(get_mgmt_sw); then
         dbg_log "" "$switches"  
         sleep 10

         dest_id=$(cat $acefs/self/server/nicports/1/dest_id)
         my_sw_name=ethswitch-$(printf "%04d" $dest_id)
         put_sw_db "" $my_sw_name
         sleep_time=5
         continue
      fi
  elif [ "$(cat $acefs/self/server/type)" == "group" ]; then
      switches=$(get_group_sw)
      rc=$?
      group_mode=1
      
      if [ "$rc" == "3" ]; then
             dbg_log -n "Running ace_ethstatd.sh in peer_id=1"
             sleep_time=$base_interval
             continue
      elif [ "$rc" == "2" ]; then
             dbg_log "" "$switches"
             exit 0
      elif [ "$rc" == "1" ]; then
             dbg_log "$switches"
             sleep_time=$base_interval
             continue
      fi
  else
      echo "This server is not sub-mgmt(sysgrp) or management node"
      exit 1
  fi

  put_sw_db "$group_mode" $switches && \
      interval=$base_interval || \
      interval=$boot_mon_interval

  dbg_log "-b" ""
  dbg_log "-b" "Interval $interval"
  sleep_time=$interval
done 
