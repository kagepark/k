#!/bin/sh
#
# export_cluster_image.sh
#
#       Export a cluster image to a file
#
# Usage:        export_cluster_image.sh cluster revision output-file.img
#
# Change log:
#
# Date          Name                    Change Description
#
# 2010-04-30    Shannon Davidson        Initial release
# 2010-07-29    Kage Park        Initial release
#

set +u
export PATH=/usr/sbin:/sbin:/usr/bin/:/bin

acefs=/acefs
[ -d /.ace/acefs ] && acefs=/.ace/acefs

error_exit() {
    echo "ERROR: $*" 1>&2
    exit 1
}

checkout_storage() {
    echo $1 | grep "\.co\$" >/dev/null 2>&1
    return $?
}

[ ${#*} -ne 3 ] && error_exit "usage: $(basename $0) cluster revision outputfile.img"

#
# find_cow_device <basefile> <cowfile>
#
#       find the cow device for the specified basefile and cowfile
#

find_cow_device() {
    local basefile cowfile dev base cow
    basefile=$1
    cowfile=$2
    set -- $(cowdev -l | egrep -v '^cowdevice|^---------|^$')
    while [ -n "$1" ]; do
        dev=$1
        base=$2
        cow=$3
        if [ "$basefile" = "$base" -a "$cowfile" = "$cow" ]; then
            echo $dev
            return
        fi
        shift 3
    done
}

cluster=$1
revision=$2
output_file=$3

#
# check for ACEFS
#

[ ! -d $acefs/global ] && error_exit "This script requires ACE to be up and running"
storage=$(cat $acefs/global/storage_dir)
[ -z "$storage" ] && error_exit could not determine cluster storage directory
[ -d /.ace/tmp ] && storage=/.ace/$storage
[ ! -d $storage ] && error_exit cluster storage directory $storage does not exist
[ ! -d $acefs/clusters/$cluster ] && error_exit "Cluster $cluster not found in ACEFS"
[ ! -d $acefs/clusters/$cluster/revisions/$revision ] && error_exit "Cluster $cluster does not have revision $revision"

oneup=$(cat $acefs/clusters/$cluster/revisions/$revision/oneup)

# handle kickstart clusters
kickstart=$(cat $acefs/clusters/$cluster/kickstart)
[ $kickstart -ne 0 ] && error_exit "cluster $cluster is a kickstart cluster"

install=$(cat $acefs/clusters/$cluster/install)
[ -z "$install" ] && error_exit "No install file for cluster $cluster"
base_file=$storage/$install
gnbd_name=${cluster}.${revision}.${oneup}
cow_file=$storage/$gnbd_name
cow_device=$(find_cow_device $base_file $cow_file)

if [ -z "$cow_device" ]; then

    # start the COW device
    echo
    echo "COW device not found - starting COW device"
    echo
    cow_device=$(cowdev -a $base_file $cow_file)
    [ -z "$cow_device" ] && error_exit Unable to start COW device for $base_file $cow_file
    sleep 2
fi

echo
echo "Exporting cluster $cluster revision $revision to $output_file"
echo
echo "    This could take a while..."
echo

curr_dir=$(pwd)
ace_image_tmp=$(mktemp -d /tmp/${cluster}.${revision}.${oneup}.XXXXXXX)
acefs=/acefs/clusters/$cluster
tmp_acefs=${ace_image_tmp}${acefs}

mkdir -p $tmp_acefs/mount
cp -fra $acefs/mount/* $tmp_acefs/mount
cp -fra /acefs/global/mount/* $tmp_acefs/mount
cat $acefs/kernel > $tmp_acefs/kernel
cat $acefs/kernel_args > $tmp_acefs/kernel_args
cat $acefs/hostfs_files > $tmp_acefs/hostfs_files
cat $acefs/tmpfs_files > $tmp_acefs/tmpfs_files
cat $acefs/desc > $tmp_acefs/desc
cat /acefs/global/mgmt_name > $tmp_acefs/mgmt_name
cat /acefs/global/system_name > $tmp_acefs/system_name
cat /acefs/global/sync_files > $tmp_acefs/sync_files
/opt/ace/bin/ace_version > $ace_image_tmp/version
echo $cluster > $ace_image_tmp/cluster_name
echo ${revision} > $ace_image_tmp/revision
echo ${oneup} > $ace_image_tmp/oneup
echo $(date) > $ace_image_tmp/date
mkdir -p $ace_image_tmp/kernels
cp -fa /boot/vmlinuz-$(cat $acefs/kernel) $ace_image_tmp/kernels
cp -fa /boot/config-$(cat $acefs/kernel) $ace_image_tmp/kernels
tar zcf $ace_image_tmp/kernels/modules.tar.gz /lib/modules/$(cat $acefs/kernel)
cp -fa /boot/initrd-$(cat $acefs/kernel).img $ace_image_tmp/kernels
cp -fra /ha_cluster/tftpboot/ace/${cluster} $ace_image_tmp/pxe_boot

cp --sparse=always $cow_device $ace_image_tmp/${cluster}.${revision}.img || error_exit Could not copy cluster $cluster revision $revision to ${cluster}.{$revision}.img

(cd $ace_image_tmp && tar zcSf ${curr_dir}/$output_file . ) || error_exit Could not pack $ace_image_tmp
rm -fr $ace_image_tmp

echo "Completed export $cluster image to $output_file"
exit 0

