#!/bin/sh
# Kage  08/21/2014 : 04 : Initial version

error_exit() {
    echo "$*"
    exit 1
}

read_db() {
    local find db_file
    find=$1
    db_file=$2
    [ -n "$find" ] || error_exit "read_db <find> [<db file>]"
    [ -f "$db_file" ] || db_file=/opt/ace/etc/database.cfg
    [ -f "$db_file" ] || error_exit "$db_file not found"

    awk -v f1=$find '{if($1==f1) print $2}' $db_file | sed "s/'//g"
}

ip2str() {
    local num
    num=$1
    [ -n "$num" ] || error_exit "input not found"
    echo $(( $(( $num/$((255*255*255)) ))% 255)).$(( $(($num/$((255*255)) ))%255)).$(( $(($num/255)) % 255)).$(($num%255))
}

str2ip() {
    [ -n "$1" ] || error_exit "input not found"
    ip=( $(echo $1 | sed 's/\./ /g') )
    echo $(( $((255*255*255*${ip[0]})) + $((255*255*${ip[1]})) + $((255*${ip[2]})) +${ip[3]} ))
}

add_ip() {
    [ -n "$1" ] || error_exit "IP not found"
    [ -n "$2" ] || error_exit "add value not found"
    echo $(ip2str $(( $(str2ip $1) + $2 )) )
}

db_file=$1
[ -n "$db_file" ] || error_exit "$(basename $0) <db_file> [<cluster name> or <cluster id>]"
[ -f "$db_file" ] || error_exit "$db_file not found"

num_groups=$(read_db num_groups $db_file)
max_servers=$(read_db max_servers $db_file)
network1_base=$(read_db network1 $db_file)
sw_ip=$(read_db net1_switch $db_file)
netmask=$(read_db netmask $db_file)
max_compute_servers=$max_servers
max_clusters_ip_num=$(($(str2ip $(ipcalc -b -n $network1_base -m $netmask  | awk -F= '{if($1=="BROADCAST") print $2}')) -1 ))
net_reserved=256

if [ -d /acefs/clusters ]; then
    ace=1
    cluster=$2
    if [ -n "$cluster" ]; then
        [ -d /acefs/clusters/$cluster ] || error_exit "$(basename $0) <db_file> [<cluster name>]"
        cluster_num=1
    else
        cluster_num=$(ls /acefs/clusters | wc -l)
    fi
    max_rev=$(cat /acefs/global/max_revisions)
    if [ "$([ -f /acefs/global/conserve_ip ] && cat /acefs/global/conserve_ip)" == "1" ]; then
        net_reserved=$(( $max_servers + $max_rev + 2 ))
        base_cluster_ip=$net_reserved
    else
        base_cluster_ip=$(( $(($num_groups + 1)) * $net_reserved))
    fi
else
    cluster_num=$2
    [ -n "$cluster_num" ] || error_exit "$(basename $0) <db_file> <cluster num>"
    max_rev=10
    if [ "$(read_db conserve_ip $db_file)" == "1" ]; then
        net_reserved=$(( $max_servers + $max_rev + 2 ))
        base_cluster_ip=$net_reserved
    else
        base_cluster_ip=$(( $(($num_groups + 1)) * $net_reserved))
    fi
fi

ip_s=$(add_ip $network1_base 4)
ip_e=$(add_ip $network1_base $(($base_cluster_ip + $(( $(($max_compute_servers + $max_rev)) * $((1 - 1)) )) + $net_reserved - 1)) )

cluster_id=1
cluster_start_ip=$(add_ip $network1_base $(($base_cluster_ip + $(( $(($max_compute_servers + $max_rev)) * $(($cluster_id - 1 )) )) + $net_reserved )) )
cluster_end_ip=$(ip2str $max_clusters_ip_num)
group_num=$(( $(str2ip $(add_ip $network1_base $(($base_cluster_ip + $max_compute_servers + $max_rev + $net_reserved)) ) ) - $(str2ip $cluster_start_ip) ))

echo "Network: $network1_base / $netmask"
echo "Switch IP: $sw_ip"
echo "Not ace ip : $ip_s ~ ??"
echo "Max cluster number : $(( $(($max_clusters_ip_num - $(str2ip $cluster_start_ip) )) / $group_num  )) (temporary. Not sure)"
echo "Cluster IP Range : $cluster_start_ip ~ $cluster_end_ip (temporary. Not sure)"

#net1 + $group_id * $net_reserved + 4 + compute_servers_per_group[group_id-1]
echo "Server IP Range : Not yet"
echo "iSCB IP Range : Not yet"
echo "IPMI IP Range : Not yet"

for ((ii=1;ii<= $cluster_num; ii++)); do 
    if [ -d /acefs/clusters -a -n "$cluster" ]; then
        [ -d /acefs/clusters/$cluster ] && cluster_id=$(cat /acefs/clusters/$cluster/id) || error_exit "$cluster not found"
        cluster_name=$cluster
    elif [ -d /acefs/clusters -a ! -n "$cluster" ]; then
        cluster_name=$(cat $(awk -v id=$ii '{if($1==id) print FILENAME}'  /acefs/clusters/*/id | sed "s/id$/name/g") )
        cluster_id=$ii
    else
        cluster_id=$ii
    fi
    ip1=$(add_ip $network1_base $(($base_cluster_ip + $(( $(($max_compute_servers + $max_rev)) * $(($cluster_id - 1 )) )) + $net_reserved )) )
    ip1_e=$(add_ip $ip1  $max_servers )
    if [ -n "$cluster_name" ]; then
        printf "%20s : %s\n" "$cluster_name ($cluster_id)" "servers #: $max_servers, ip1: $ip1 ~ $ip1_e"
    else
        printf "%20s : %s\n" "cluster_id ($cluster_id)"   "servers #: $max_servers, ip1: $ip1 ~ $ip1_e"
    fi
    ip1_e_num=$(str2ip $ip1_e)
    (( $max_clusters_ip_num < $ip1_e_num )) && error_exit "Out of range ACE cluster's IP"
done
