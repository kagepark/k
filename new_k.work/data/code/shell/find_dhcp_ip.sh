#!/bin/sh

host=$1
if [ ! -n "$host" ]; then
  echo "$(basename $0) <hostname>"
  exit
fi

host_mac=$(cat /acefs/clusters/$(echo $host | awk -F- '{print $1}')/hosts/$host/server/mac1)
dhcp_tmp=/var/lib/dhcpd/dhcpd.leases


cat $dhcp_tmp | while read line; do
   if echo $line | grep "^lease" >& /dev/null; then
      tmp_ip=$(echo $line | awk '{print $2}')
   else
      echo $line | grep "hardware" >& /dev/null && tmp_mac=$(echo $line | awk '{print $3}' | sed 's/;//g')
      if [ "$tmp_mac" == "$host_mac" ]; then
           echo $tmp_ip
           break
      fi
   fi
done
