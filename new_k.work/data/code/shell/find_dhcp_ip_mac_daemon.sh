#!/bin/sh

dhcp_dir=/var/lib/dhcpd
dhcp_file=dhcpd.leases
dhcp_bak=dhcpd.leases.k
find_ip="10.4"

cp -a $dhcp_dir/$dhcp_file $dhcp_dir/$dhcp_bak
while [ true ]; do
    if ! cmp $dhcp_dir/$dhcp_file $dhcp_dir/$dhcp_bak >& /dev/null; then
        mv $dhcp_dir/$dhcp_bak $dhcp_dir/${dhcp_bak}~
        cp -a $dhcp_dir/$dhcp_file $dhcp_dir/$dhcp_bak
        diff -up $dhcp_dir/$dhcp_file $dhcp_dir/${dhcp_bak}~ | grep "^-" | grep -v "^---" | sed "s/^-//g" | while read line ; do
            if echo $line | grep "^lease" >& /dev/null; then
               dhcp_ip=$(echo $line | awk '{printf "%s",$2}')
            else
               dhcp_mac=$(echo $line | grep "hardware" >& /dev/null && echo $line | awk '{printf "%s",$3}' | sed 's/;//g')
            fi
            if [ -n "$dhcp_ip" -a -n "$dhcp_mac" ]; then
               if [ -n "$find_ip" ]; then
                   echo $dhcp_ip | grep "^$find_ip" >& /dev/null && echo "$dhcp_ip $dhcp_mac"
               else
                   echo "$dhcp_ip $dhcp_mac"
               fi
               dhcp_ip=""
               dhcp_mac=""
            fi
        done
    fi
    sleep 2
done

