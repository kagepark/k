#!/bin/sh
error_exit() {
    echo $*
    exit
}

[ "$1" == "-h" ] && host=$2 || server=$1
[ -n "$1" ] || error_exit "$(basename $0) <server name>"
[ -d /acefs/servers/$server ] || error_exit "$server not found"
ipmi1=$(cat /acefs/servers/$server/ipmi1)
hostname=$(cat /acefs/servers/$server/host/name)
arp_mac=$(arp -a | grep "$ipmi1" |awk '{print $4}')
host_mac=$(ssh $hostname ipmitool lan print | grep "MAC Address" | awk '{print $4}')

if [ "$arp_mac" == "$host_mac" ]; then
    echo "mac address matched"
    ping -c 3 $ipmi1 >/dev/null &&  ipmitool -I lan -H $ipmi1 -U ace -P ace -L operator chassis power status || echo "ping to
$ipmi1 fail"

else
    error_exit "IPMI ${ipmi1}'s mac address is different (mgmt:$arp_mac <==> $hostname:$host_mac)"
fi
