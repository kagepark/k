find_file() {
   local old_release release tmp
   old_release=0
   for line in $(ls $* 2>/dev/null); do
      release=$(rpm -qp --queryformat "%{release}" $line | awk -F. '{print $1}');
      (( $old_release < $release )) && tmp=$line
      old_release=$release
   done
   [ -n "$tmp" ] && echo $tmp || return 1
   return 0
}

find_file2() {
  local rpm_info rpm_num skip name rpm_ver rpm_rel chk_ver chk_rel
#echo "$*"
#ls $* 2>/dev/null
  rpm_info=($(ls $* 2>/dev/null | while read line; do
    echo "${line}#$(rpm -qp --queryformat '%{version}#%{release}\n' $line)"
  done))

  rpm_num=$((${#rpm_info[*]}-1))
  for (( ii=0; ii<=$rpm_num; ii++)); do
    skip=$rpm_num
    name=$( echo ${rpm_info[$ii]} | awk -F# '{print $1}')
    rpm_ver=$( echo ${rpm_info[$ii]} | awk -F# '{print $2}')
    rpm_rel=$( echo ${rpm_info[$ii]} | awk -F# '{print $3}')
    for ((jj=0; jj<=$rpm_num; jj++)); do
        chk_ver=$( echo ${rpm_info[$jj]} | awk -F# '{print $2}')
        chk_rel=$( echo ${rpm_info[$jj]} | awk -F# '{print $3}')
        [ "$ii" == "$jj" ] && continue
        if [ "$rpm_ver" == "$chk_ver" ]; then
            if (($rpm_rel == $chk_rel )); then
                skip=$jj
            elif (($rpm_rel < $chk_rel )); then 
                name=$(echo ${rpm_info[$jj]} | awk -F# '{print $1}')
            elif (($rpm_rel > $chk_rel )); then 
                skip=0
            fi
        fi
    done
    if (( $skip >= $ii)); then
      echo $name
    fi
  done | sort | uniq
}

find_file2 packages/ace/rhel6.3/ace-common-2.0r2556*.x86_64.rpm
find_file2 packages/ace/rhel6.3/ace_ofed*.x86_64.rpm
find_file2 packages/ace/rhel6.3/ace-2.0r2556*_OFED*1.5.4.1.rhel6.3.x86_64.rpm
