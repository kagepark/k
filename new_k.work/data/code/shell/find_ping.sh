#!/bin/sh
MPIRUN=/usr/mpi/gcc/mvapich-1.2.0/bin/mpirun_rsh
_path=$1
force=$2

cluster=$(cat /.ace/acefs/self/cluster_name)
mpi=$(rpm -qa |grep "^mvapich_" | grep -v -e mpitest)
[ -n "$mpi" ] || mpi=$(rpm -qa |grep "^mvapich2_" | grep -v -e mpitest)
[ -n "$mpi" ] || mpi=$(rpm -qa |grep "^openmpi_" | grep -v -e mpitest)
if [ ! -n "$mpi" ]; then
     echo "MPI not found in $cluster"
     exit 1
fi

[ -n "$MPIRUN" ] || MPIRUN=$(rpm -ql $mpi | grep "bin/mpirun$")
if [ -n "$MPIRUN" ]; then
     if [ ! -f $MPIRUN ]; then
         echo "MPIRUN not found in $cluster"
         exit 1
     fi
else
     echo "MPIRUN not found in $cluster"
     exit 1
fi

if [ ! -f $_path/pingpong.${cluster} -o -n "$force" ]; then
        cd $_path
        rm -f *.o *.${cluster}
        CC=$(rpm -ql $mpi | grep "bin/mpicc$")

        if ! ${CC} ${CFLAGS} -o pingpong.${cluster} pingpong.c ${LIBFLAGS}; then
            #cd $_path && ./pingtest.sh -i $_hca_ping -p $_port_ping -r $MPIRUN -c $cluster
            #echo "$MPIRUN $cluster"
#        else
            echo "Compile error to $_path with $CC"
            exit 1
        fi
fi
echo "$MPIRUN $cluster"
