server_name=$1
if [ ! -n "$server_name" ]; then
    echo "$(basename $0) <server name>"
    exit
fi
if [ ! -d /acefs/servers/$server_name ]; then
    echo "$server_name not found"
    exit
fi

find_mac=$(cat /acefs/servers/$server_name/mac1)

sub_mgmt=$(cat /acefs/global/sub_mgmt)
if [ "$sub_mgmt" == "1" ]; then
elif [ "$sub_mgmt" == "2" ]; then
else
fi

cat /var/lib/dhcpd/dhcpd.leases | while read line ; do
   if [ "$(echo $line | grep "^lease" >& /dev/null && echo ok)" == "ok" -a "$open" != "1" ]; then
        open=1
        ip=$(echo $line | awk '{print $2}')
   fi
   if [ "$open" == "1" ]; then
       if echo $line | grep "^hardware ethernet" >& /dev/null; then
           if [ "$find_mac" == "$(echo $line | awk '{print $3}' | sed "s/;//g")" ]; then
               echo $ip
               break
           fi
           open=0
       fi
   fi
done
