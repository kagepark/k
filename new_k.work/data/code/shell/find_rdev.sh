#!/bin/bash
error_exit() {
    echo "$*"
    exit 1
}
device_name=$1
[ -n "$device_name" ] || error_exit "$(basename $0) <device name>|<filename>|<dir name>"
if [ ! -b $device_name ]; then
    device_name=$(findmnt -n -o SOURCE --target $device_name)
fi

find_rdev() {
   local device_name
   device_name=$1
   [ -b $device_name ] || error_exit "Not a block device"
   dev_type=$(lsblk $device_name | grep -v "^NAME" | awk -v dn=$(basename $device_name) '{if($1==dn) print $6}')
   if [ "$dev_type" == "lvm" ]; then
      group_name=$(lvdisplay $device_name | grep "VG Name" | awk '{print $3}')
      for ii in $(pvdisplay | grep -e "PV Name" -e "VG Name" | awk '{if($1 == "PV") printf "%s:",$3; else printf "%s\n",$3}' | grep ":${group_name}" | awk -F: '{print $1}'); do
         find_rdev $ii
      done
   elif echo $dev_type | grep "raid" >& /dev/null; then
      mdadm --detail $device_name | grep "active sync" | awk '{print $7}'
   else
      echo $device_name
   fi
}

find_rdev $device_name
