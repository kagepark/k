if [ "$1" == "-h" -o "$1" == "--help" ]; then
  echo "$(basename $0) all : Find all SM"
  echo "$(basename $0)     : Find master SM"
  exit
elif [ "$1" == "all" ]; then
  lids=$(ibnetdiscover | grep lid |awk '{print $5}'| sort -n | uniq)
  for ii in $lids; do
    if sminfo=$(sminfo -L $ii 2>/dev/null); then
        sminfo_arr=($sminfo)
        lid=${sminfo_arr[3]}
        quid=${sminfo_arr[6]}
        prio=${sminfo_arr[$((${#sminfo_arr[*]}-4))]}
        stat=${sminfo_arr[$((${#sminfo_arr[*]}-1))]}
        host=$(smpquery nd $lid | sed "s/\./ /g" | awk '{printf "%s(%s)", $3, $4}')
        echo "$host : lid $lid quid $quid priority $prio state $stat"
    fi
  done
else
  if sminfo=$(sminfo 2>/dev/null); then
      sminfo_arr=($sminfo)
      lid=${sminfo_arr[3]}
      quid=${sminfo_arr[6]}
      prio=${sminfo_arr[$((${#sminfo_arr[*]}-4))]}
      stat=${sminfo_arr[$((${#sminfo_arr[*]}-1))]}
      host=$(smpquery nd $lid | sed "s/\./ /g" | awk '{printf "%s(%s)", $3, $4}')
      echo "$host : lid $lid quid $quid priority $prio state $stat"
  fi
fi
