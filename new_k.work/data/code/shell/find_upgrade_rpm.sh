src_file=$1
dest_file=$2
if [ ! -f "$src_file" -o ! -f "$dest_file" ]; then
    echo "Please get rpm list file using \"rpm -qa --qf '%{name} %{version} %{arch}\n'> <rpm list file>.txt\" command"
    echo "$(basename $0) <src rpm list file> <dest rpm list file>"
    exit
fi

#rpm -qa --qf '%{name} %{version} %{arch}\n'> rhel6.2_ace_rpm.txt

cat $src_file | while read line ; do
    name=$(echo $line | awk '{print $1}')
    ver=$(echo $line | awk '{print $2}')
    same_file=$(awk -v fn=$name -v ver=$ver '{if($1==fn && $2 == ver) print }' $dest_file)
    upgrade_file=$(awk -v fn=$name -v ver=$ver '{if($1==fn && $2 != ver) print }' $dest_file)
    if [ -n "$same_file" ]; then
        echo "same: $same_file"
    elif [ -n "$upgrade_file" ]; then
        echo "update: $upgrade_file"
    else
        echo "not found: $line"
    fi
done
