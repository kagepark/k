#!/bin/sh
#
# get extra rpm package list
#
error_exit() {
   echo $*
   exit
}

nodename=$(hostname)
[ -f /root/install.log ] || error_exit "install.log file not found"

cat /root/install.log |grep "^Installing " | awk '{print $2}' > ${nodename}-base-rpm.txt
for rpm in $(cat ${nodename}-base-rpm.txt); do
   echo $rpm | grep ":" >&/dev/null && echo $rpm | awk -F: '{print $2}' || echo $rpm
done > ${nodename}-base-rpm.txt~

mv ${nodename}-base-rpm.txt~ ${nodename}-base-rpm.txt
BASE=( $(cat ${nodename}-base-rpm.txt) )

rpm -qa --queryformat '%{NAME}-%{VERSION}-%{RELEASE}.%{ARCH}\n' | sort > ${nodename}-rpm.txt
RHEL=( $(cat ${nodename}-rpm.txt) )

chk=0
for j in $(seq 0 $((${#RHEL[*]}-1))); do
   for i in $(seq 0 $((${#BASE[*]}-1))); do
      if [ "${RHEL[$j]}" == "${BASE[$i]}" ]; then
          chk=1 
      fi
   done
   if [ $chk -eq 0 ]; then
      echo ${RHEL[$j]}
   else
      chk=0
   fi
done

