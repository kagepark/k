#!/bin/sh
rpm -qa |grep "^mstflint" >&/dev/null || error_exit "Can not found mstflint util. Can you install OFED or mstflint rpm package?"
rpm -qa |grep "^pciutils" >& /dev/null || error_exit "Can not found lspci util. Can you install pciutils rpm package?"

devices=$(lspci -v | egrep -i "infiniband|ethernet" | grep -i mellanox | awk '{print $1;}')
for dev in $devices; do
   board_id=$(mstflint -d $dev q | grep PSID | awk '{print $2;}')
   if [ -n "$board_id" ]; then
      mstflint -d $dev ri $(hostname)_fw_${board_id}_${dev}.bak.bin
      echo "firmware of $dev saved at $(hostname)_fw_${board_id}_${dev}.bak.bin"
   else
      echo "board_id not found at $dev"
   fi
done
