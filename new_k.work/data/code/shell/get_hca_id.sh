#!/bin/sh

FW_DIR=$1
[ -n "$FW_DIR" ] || FW_DIR=/opt/ace/share/mellanox
FW_EXT=".bin"

error_exit() {
   echo $*
   exit
}
opwd=$(pwd)

rpm -qa |grep "^mstflint" >&/dev/null || error_exit "Can not found mstflint util. Can you install OFED or mstflint rpm package?"
rpm -qa |grep "^pciutils" >& /dev/null || error_exit "Can not found lspci util. Can you install pciutils rpm package?"

chk=0
devices=$(lspci -v | egrep -i "infiniband|ethernet" | grep -i mellanox | awk '{print $1}')
for dev in $devices; do
   board_id=$(mstflint -d $dev q | grep PSID | awk '{print $2}')
   board_fw_ver=$(mstflint -d $dev q | grep "FW Version:" | awk '{print $3}')
   dev_path=$(find /sys/devices -name "0000:$dev")
   module=( $( ls -l $dev_path/driver/module) ) 
   module_ver=$( cat $dev_path/driver/module/version)

   echo "Device ($dev) : BoardID($board_id)  F/W($board_fw_ver) MV($module_ver)"

   if [ ! -n "$board_id" ]; then
       error_exit "HCA device not found"
   fi
done
cd $opwd
echo



