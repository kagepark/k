#!/bin/sh

FW_DIR=$1
[ -n "$FW_DIR" ] || FW_DIR=/opt/ace/share/mellanox
FW_EXT=".bin"

error_exit() {
   echo $*
   exit
}
opwd=$(pwd)

rpm -qa |grep "^mstflint" >&/dev/null || error_exit "Can not found mstflint util. Can you install OFED or mstflint rpm package?"
rpm -qa |grep "^pciutils" >& /dev/null || error_exit "Can not found lspci util. Can you install pciutils rpm package?"

chk=0
devices=$(lspci -v | egrep -i "infiniband|ethernet" | grep -i mellanox | awk '{print $1}')
for dev in $devices; do
   board_id=$(mstflint -d $dev q | grep PSID | awk '{print $2}')
   board_fw_ver=$(mstflint -d $dev q | grep "FW Version:" | awk '{print $3}')
   dev_path=$(find /sys/devices -name "0000:$dev")
   module=( $( ls -l $dev_path/driver/module) ) 
   module_name=$(basename ${module[$((${#module[*]} - 1))]})
   module_ver=$( cat $dev_path/driver/module/version)

   for net in $(ls -l $dev_path/net* | awk '{ print $11}'); do
      net_name="$(basename $net) $net_name"
   done

   echo
   echo "Device ($dev) : "
   echo "     BoardID($board_id) "
   echo "     F/W($board_fw_ver) "
   echo "     Module($module_name)"
   echo "     Module Version($module_ver)"
   echo "     Network($net_name)"

   if [ -d $dev_path/mlx4_? ]; then
      for mlx in $(ls -d $dev_path/mlx4_?); do
          mlx_name=$(basename $mlx)
          echo "     $mlx_name: "
          echo "         FV($(cat /sys/class/infiniband/$mlx_name/fw_ver)) "
          echo "         Type($(cat /sys/class/infiniband/$mlx_name/hca_type)) "
          for port in $(ls /sys/class/infiniband/$mlx_name/ports ); do
             echo -n "         Port${port}: state($(cat /sys/class/infiniband/$mlx_name/ports/$port/state) /"
             echo -n " $(cat /sys/class/infiniband/$mlx_name/ports/$port/phys_state))"
             echo -n " Layer($(cat /sys/class/infiniband/$mlx_name/ports/$port/link_layer))"
             echo -n " Rate($(cat /sys/class/infiniband/$mlx_name/ports/$port/rate))"
             echo 
          done
          echo 
      done
   fi

   if [ ! -n "$board_id" ]; then
       error_exit "HCA device not found"
   fi
done
cd $opwd
echo



