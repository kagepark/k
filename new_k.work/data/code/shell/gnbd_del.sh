#!/bin/sh
#
# cluster group export script
#
#       Import and export network block devices
#

set +u
export PATH=/.ace/sbin:/usr/sbin:/sbin:/usr/bin/:/bin

if [ -d /.ace/tmp ]; then
    exec >>/.ace/tmp/cluster_group_export.out 2>&1
else
    exec >>/tmp/cluster_group_export.out 2>&1
fi

set -x

acefs=/acefs
[ -d /.ace/acefs ] && acefs=/.ace/acefs

export GNBD_PROGRAM_DIR=/.ace/gnbd
mkdir -p $GNBD_PROGRAM_DIR

error_exit() {
    echo "ERROR: $*" 1>&2
    exit 1
}

find_non_gnbd() {
    cluster_path=/.ace/acefs/clusters
    for cluster in $(ls $cluster_path); do 
        if [ "$(cat $cluster_path/$cluster/num_hosts)" == "0" ]; then
            revision=$(cat $cluster_path/$cluster/revision)
            if [ -f $cluster_path/$cluster/revisions/$revision/oneup ]; then
                echo ${cluster}.${revision}.$(cat $cluster_path/$cluster/revisions/$revision/oneup)
            else
                echo ${cluster}.${revision}
            fi
        fi
    done
}

get_cluster() {
    echo $1 | sed 's/\./ /g' | awk '{print $1;}'
}

get_revision() {
    echo $1 | sed 's/\./ /g' | awk '{print $2;}'
}

get_gnbd_device() {
    local name=$1
    local a
    cd /sys/class/gnbd && \
    for a in *; do
        if [ "$name" = "$(cat $a/name)" ]; then
            echo $a
            break
        fi
    done
    echo ""
    cd - >/dev/null 2>&1
}

get_imported_gnbd_names() {
    local name a
    cd /sys/class/gnbd && \
    for a in *; do
        name=$(cat $a/name)
        #connected=$(cat $a/connected)
        #pid=$(cat $a/pid)
        usage=$(cat $a/usage)
        #[ -n "$name" -a "$connected" == "1" ] && echo $name
        #[ -n "$name" -a "$pid" != "-1" ] && echo $name
        [ -n "$name" -a "$usage" -gt "0" ] && echo $name
    done
    cd - >/dev/null 2>&1
}

get_exported_gnbd_names() {
    gnbd_export -l | grep "^Server" | awk '{print $3;}'
}

#
# start GNBD server
#

if ! ps -e | grep gnbd_serv >/dev/null 2>&1; then
    gnbd_serv -n
    sleep 1
fi

echo "$(date) export cluster block devices - $*"

#
# wait for ACEFS
#

count=600
while [ ! -d $acefs/global -a $count -gt 0 ]; do
    echo "waiting for ACE file system"
    sleep 1
    count=$(expr $count - 1)
done

#
# unexport GNBDs which are no longer needed
#

imported_gnbds=$(get_imported_gnbd_names)
gnbds_to_remove=
for gnbd in $imported_gnbds; do
    found=0
    for gnbd_name in $*; do
        if [ "$gnbd" = "$gnbd_name" ]; then
            found=1
            break
        fi
    done
    if [ $found -eq 0 ]; then
        gnbds_to_remove="$gnbds_to_remove $gnbd"
    fi
done

echo "remove gnbd: $gnbds_to_remove"
#kage
#if [ -n "$gnbds_to_remove" ]; then
#    for gnbd in $gnbds_to_remove; do
#        echo "Un-exporting $gnbd"
#        gnbd_export -O -r $gnbd
#    done
#    echo "Un-importing $gnbds_to_remove"
#    gnbd_import -n -r $gnbds_to_remove
#fi

#
# check to see if we have any new GNBDs to import/export
#

exported_gnbds=$(get_exported_gnbd_names)
gnbds_to_add=
for gnbd_name in $*; do
    found=0
    for gnbd in $exported_gnbds; do
        if [ "$gnbd" = "$gnbd_name" ]; then
            found=1
            break
        fi
    done
    if [ $found -eq 0 ]; then
        gnbds_to_add="$gnbds_to_add $gnbd_name"
    fi
done

echo GNBDs to remove $gnbds_to_remove
echo GNBDs to add $gnbds_to_add

#
# Exit if there are no new GNBDs to add
#

[ -z "$gnbds_to_add" ] && exit 0

#
# import and export cluster storage
#

#ip_ha_server=$(cat $acefs/servers/server-0001/ha_ip)
ip_ha_server=$(cat $acefs/servers/server-0001/failover_ip1)
[ -z "$ip_ha_server" ] && error_exit "Unable to get management server HA IP address"
gnbd_import -n -i $ip_ha_server

#
# import/export new GNBDs
#

#for gnbd_name in $*; do
for gnbd_name in $gnbds_to_add; do
    
    cluster=$(get_cluster $gnbd_name)
    revision=$(get_revision $gnbd_name)

    # hack to wait for cluster to show up
    for i in $(seq 1 30); do
        [ -d $acefs/clusters/$cluster ] && break
        sleep 1
    done

    gnbd_device=$(get_gnbd_device $gnbd_name)
    if [ "$gnbd_device" = "" ]; then
        echo Unable to get GNBD device for $gnbd_name
        continue
    fi
    if ! gnbd_export -l | grep "^Server" | grep " $gnbd_name " >/dev/null 2>&1; then
        gnbd_export -o -c -d /dev/$gnbd_device -e $gnbd_name || echo unable to export $gnbd_device as $gnbd_name
    fi

done

gnbd_export -l

exit 0

