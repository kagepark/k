#!/bin/sh
# Kage 08/17/2014

for ii in $(cl_status listnodes | sort -n ); do
    echo -n "$ii :"
    status=$(cl_status nodestatus $ii)
    if [ "$status" == "active" ]; then
        [ "$(ssh $ii cl_status rscstatus)" == "all" ] && echo -n " PRI" || echo -n " SEC"
    elif [ "$status" == "ping" ]; then
        echo -n " PING"
    elif [ "$status" == "dead" ]; then
        echo -n " DEAD"
    else
        echo -n " Unknown"
    fi
    if [ "$status" == "ping" -o "$status" == "active" ]; then
        for dev in $(cl_status listhblinks $ii 2>/dev/null); do
            echo -n " $dev $(cl_status hblinkstatus $ii $dev)"
        done
    fi
    echo
    chk=1
done
if [ "$chk" != "1" ]; then
    if ps -ef |grep heartbeat | grep -v grep >& /dev/null; then
        echo "ERROR: This node is running heartbeat daemon, but can't talk with heartbeat daemon"
        echo " Please stop heartbeat daemon and waiting over $([ -f /etc/ha.d/ha.cf ] && awk '{if($1=="initdead") print $2}' /etc/ha.d/ha.cf || echo "** ERR: not found /etc/ha.d/ha.cf ** ") sec and start up heartbeat daemon"
    else
        echo "This node is not running heartbeat daemon"
    fi
fi
