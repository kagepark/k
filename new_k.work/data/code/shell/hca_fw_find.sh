#!/bin/sh

FW_DIR=$1
[ -n "$FW_DIR" ] || FW_DIR=/opt/ace/share/mellanox
FW_EXT=".bin"

error_exit() {
   echo $*
   exit
}
opwd=$(pwd)

rpm -qa |grep "^mstflint" >&/dev/null || error_exit "Can not found mstflint util. Can you install OFED or mstflint rpm package?"
rpm -qa |grep "^pciutils" >& /dev/null || error_exit "Can not found lspci util. Can you install pciutils rpm package?"

chk=0
devices=$(lspci -v | egrep -i "infiniband|ethernet" | grep -i mellanox | awk '{print $1}')
for dev in $devices; do
   board_id=$(mstflint -d $dev q | grep PSID | awk '{print $2}')
   board_fw_ver=$(mstflint -d $dev q | grep "FW Version:" | awk '{print $3}')
   if [ ! -n "$board_id" ]; then
       error_exit "HCA device not found"
   fi
   echo
   echo
   echo "Board_ID of device ($dev) is $board_id"
   echo "Device ($dev) firmware version is $board_fw_ver"

   echo 
   echo "  Searching firmware file for this device($dev)...."
   cd $FW_DIR
   for fw in $(ls *${FW_EXT}); do
      echo -n "    >> $fw ..."
      for i in $(seq 1 $(( ${#fw}+11)) ); do
         echo -n -e "\b"
      done
      fw_board_id=$(mstflint -i $fw q | grep PSID | awk '{print $2}')
      if [ "$fw_board_id" == "$board_id" ]; then
         fw_ver=$(mstflint -i $fw q | grep "FW Version:" | awk '{print $3}')
         echo "                                                                               "
         echo "                                                                               "
         echo "    >> Matched firmware file is \"$fw\" for device ($dev : $board_id)"
         echo "      - firmware version : $fw_ver"
         echo "      - Board ID         : $fw_board_id"
         echo
         continue
      else
         chk=1
      fi
   done
   if [ $chk -eq 1 ]; then
      echo "    Matched file not found...                                                    "
      chk=0
   fi
done
cd $opwd
echo
