#!/bin/bash

if ((${#1} == 0)); then
   echo "$(basename $0) <hex> [<bit#> or <end bit#>:<start bit#>] "
   echo "<bit#> range : 0~63"
   exit
fi
range=$2

hex=$(echo $1 | tr '[:lower:]' '[:upper:]')
dec=$(echo "ibase=16;$hex" | bc)
bin=$(echo "obase=2;$dec" | bc)
if (( ${#bin} < 64 )); then
    for ((ii=0;ii<$((64 - ${#bin} )); ii++)) do
	bin="0"$bin
    done
fi

dbin=$(
aa=0
printf "%s"  "${bin:0:1}"
for ((ii=1; ii<64; ii++)); do
   if (($ii%4 == 0)); then
      aa=$(($aa+1))
      if (( $aa != 0 && $aa % 4 == 0)); then
         echo 
      else
         printf " "
      fi
   fi
   printf "%s" "${bin:$ii:1}"
done)


if (( ${#range} == 0 )); then
   echo "$dbin"
else
   range_arr=($(echo $range | sed "s/:/ /g"))
   bitstr=$(echo $dbin | sed "s/ //g")
   bitstrn=${#bitstr}
   
   if ((${range_arr[0]} > 63 )); then
       echo "Out of range"
       exit
   fi
   crange=$((${range_arr[0]}+1)) #convert range to calculate range
#   echo "$bitstr:$bitstrn:$crange"
   if ((${#range_arr[*]} == 1 )); then
      echo ${bitstr:$(($bitstrn-$crange)):1}
   else
      endp=$(($bitstrn-${range_arr[1]}))
      #endp=$(($bitstrn-${range_arr[0]}))
      startp=$(($bitstrn-${crange}))
#      echo "$startp($bitstrn - ${crange}):$endp"
      if (($startp >= $endp )); then
          echo "$(basename $0) <Hex String> [<bit#> or <end bit#>:<start bit#>]"
          exit
      fi
      echo ${bitstr:$startp:$endp}
      #echo ${bitstr:$startp:$(($endp-$startp+1))}
      #echo ${bitstr:$endp:$(($endp+$startp-1))}
   fi
fi
exit

#dec=$((16#$1))
#echo $dec
#octal=$(echo  'obase=8;$dec' | bc)
#echo $octal
#
#bit=$(echo $((16#$1)) | sed "s/^-//g" | gawk ' {
#   for(i=lshift(1,63);i;i=rshift(i,1))
#   printf "%d%s", (and($1,i) > 0), ++c%4==0?" ":"";
#   printf"\n"; }')


#range=$2
#if (( ${#range} == 0 )); then
#   echo $bit
#else
#   range_arr=($(echo $range | sed "s/:/ /g"))
#   bitstr=$(echo $bit | sed "s/ //g")
#   bitstrn=${#bitstr}
#   if ((${#range_arr[*]} == 1 )); then
#      echo ${bitstr:$(($bitstrn-$range)):1}
#   else
#      startp=$(($bitstrn-${range_arr[0]}))
#      endp=$(($bitstrn-${range_arr[1]}))
#      if (($startp >= $endp )); then
#          echo "$(basename $0) <Hex String> [<bit#> or <start bit#>:<end bit#>]"
#          exit
#      fi
#      echo ${bitstr:$startp:$(($endp-$startp+1))}
#   fi
#fi
