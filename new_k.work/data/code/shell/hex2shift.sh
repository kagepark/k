#!/bin/bash

ints=$((16#$1))
if (($# == 2 )); then
   shiftnum=$2
   if (($shiftnum > 0 )); then
      shiftint=$(($ints >> $shiftnum))
   else
      shiftnum=$(echo $shiftnum | sed "s/-//g")
      shiftint=$(($ints << $shiftnum))
   fi
   printf "%x\n" $shiftint
else
   echo "$(basename $0) <hex str> <shift number>"
   echo "    -<shift number> : shift left to the number"
   echo "    <shift number>  : shift right to the number"
fi

