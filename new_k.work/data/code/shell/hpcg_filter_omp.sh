
for mpi in 1 2 4; do
old=0
old_file=$yaml
for nz in 16 32 64 104 128; do
#for nz in 104; do
   for yaml in $(grep "nz: $nz" *.yaml | grep -v "Global" | awk -F: '{print $1}'); do
      if grep "nx: $nz" $yaml | grep -v "Global" >& /dev/null; then
           if grep "Distributed Processes: $mpi" $yaml >& /dev/null; then
            if grep "Threads per processes: 32" $yaml >& /dev/null ;then
               gflops=$(grep "HPCG result is VALID with a GFLOP/s rating of:" $yaml | awk '{print $10}')
               echo "$yaml (MPI:$mpi => $(echo $(grep "  n[x|y|z]: " $yaml | awk -F: '{print $2}'))) : $gflops"

               igflops=$(echo "$gflops * 100000" |bc | awk -F\. '{print $1}')
               if (( $old < $igflops )); then
                  old=$igflops
                  old_gf=$gflops
                  old_file=$yaml
               fi
            fi
           fi
      fi
   done
done
echo
echo "Found best: $old_file : $old_gf"
echo
done


