hybrid() {
  module load mpi
  host_list=(hpcgib1 hpcgib2 hpcgib3 hpcgib4)
  for ((ii=1; ii<=5; ii++)); do
    echo
    echo " == LOOP $ii/5 =="
    for jj in 1 2 4; do
      host_str=""
      for ((qq=0;qq<$jj;qq++)); do
         [ -n "$host_str" ] && host_str="${host_str},${host_list[$qq]}" || host_str=${host_list[$qq]}
      done
      for omp in 1 2 4 8 16 32; do
         if [ "$jj" == "1" -a "$omp" == "32" ]; then
            omp_run="OMP_NUM_THREADS=$omp ./xhpcg"
            echo -n " ** Run Total Task($omp) : $omp_run : "
            eval $omp_run
         else
            hy_run="OMP_NUM_THREADS=$omp mpirun -np $(($((32/$omp)) * $jj)) -iface=ib0 -hosts $host_str ./xhpcg"
            echo -n " ** Run Total Task($((32 * $jj))) : $hy_run : "
            eval $hy_run
         fi
  
         file=$(ls -tr HPCG-Benchmark*.yaml| tail -n1)
         echo "$file : $(grep "HPCG result is VALID with a GFLOP/s rating of" $file | awk -F: '{print $2}')"
         sleep 30
      done
    done
  done
}

dat() {
echo "HPCG benchmark input file
Sandia National Laboratories; University of Tennessee, Knoxville
$1 $2 $3
30" > hpcg.dat
}


#dat 16 8 8
#hybrid | tee run_mpi_hybriad_ib.16x8x8.30_auto3.log
dat 16 16 16
hybrid | tee run_mpi_hybriad_ib.16x16x8.30_auto3.log
dat 32 16 16
hybrid | tee run_mpi_hybriad_ib.32x16x16.30_auto3.log
dat 32 32 16
hybrid | tee run_mpi_hybriad_ib.32x32x16.30_auto3.log
dat 64 32 32
hybrid | tee run_mpi_hybriad_ib.64x32x32.30_auto3.log
dat 64 64 32
hybrid | tee run_mpi_hybriad_ib.64x64x32.30_auto3.log
dat 64 64 64
hybrid | tee run_mpi_hybriad_ib.64x64x64.30_auto3.log
dat 96 64 64
hybrid | tee run_mpi_hybriad_ib.96x64x64.30_auto3.log
dat 96 96 64
hybrid | tee run_mpi_hybriad_ib.96x96x64.30_auto3.log
dat 96 96 96
hybrid | tee run_mpi_hybriad_ib.96x96x96.30_auto3.log
dat 128 96 96
hybrid | tee run_mpi_hybriad_ib.128x96x96.30_auto3.log
dat 128 128 96
hybrid | tee run_mpi_hybriad_ib.128x128x96_auto3.log
