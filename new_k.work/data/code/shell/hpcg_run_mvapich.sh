hybrid() {
  max_loop=4
  module load mvapich
  host_list=(hpcgib1 hpcgib2 hpcgib3 hpcgib4)
  for ((ii=1; ii<=$max_loop; ii++)); do
    echo
    echo " == LOOP $ii/$max_loop =="
    for jj in 1 2 4; do
      host_str=""
      rm -f hostfile
      for ((qq=0;qq<$jj;qq++)); do
         echo ${host_list[$qq]} >> hostfile
      done
      #for omp in 1 4 16 32; do
      for omp in 1 32; do
         if [ "$jj" == "1" -a "$omp" == "32" ]; then
            omp_run="OMP_NUM_THREADS=$omp MV2_ENABLE_AFFINITY=0 ./xhpcg"
            echo -n " ** Run Total Task($omp) : $omp_run : "
            eval $omp_run
         else
            echo -n " ** Run Total Task($((32 * $jj))) : OMP_NUM_THREADS=$omp MV2_IBA_HCA=mlx4_0 MV2_DEFAULT_PORT=1 mpirun_rsh -ssh -np $(($((32/$omp)) * $jj)) -hostfile $(echo $(cat ./hostfile)) ./xhpcg : "
            #OMP_NUM_THREADS=$omp MV2_IBA_HCA=mlx4_0 MV2_DEFAULT_PORT=1 MV2_ENABLE_AFFINITY=0 mpirun_rsh -ssh -np $(($((32/$omp)) * $jj)) -hostfile ./hostfile ./xhpcg
            mpirun_rsh -ssh -np $(($((32/$omp)) * $jj)) -hostfile ./hostfile ./xhpcg.sh $omp
         fi
  
         file=$(ls -tr HPCG-Benchmark*.yaml| tail -n1)
         echo "$file : $(grep "HPCG result is VALID with a GFLOP/s rating of" $file | awk -F: '{print $2}')"
         sleep 25
      done
    done
  done
}

dat() {
echo "HPCG benchmark input file
Sandia National Laboratories; University of Tennessee, Knoxville
$1 $2 $3
60" > hpcg.dat
}


#dat 4 4 4
#hybrid | tee run_mpi_hybriad_ib.4x4x4._mvapich.log
#dat 8 4 4
#hybrid | tee run_mpi_hybriad_ib.8x4x4._mvapich.log
#dat 8 8 4
#hybrid | tee run_mpi_hybriad_ib.8x8x4._mvapich.log
#dat 8 8 8
#hybrid | tee run_mpi_hybriad_ib.8x8x8._mvapich.log
#dat 16 8 8
#hybrid | tee run_mpi_hybriad_ib.16x8x8._mvapich.log
#dat 16 16 8
#hybrid | tee run_mpi_hybriad_ib.16x16x8._mvapich.log
dat 16 16 16
hybrid | tee run_mpi_hybriad_ib.16x16x16._mvapich.log
dat 32 16 16
hybrid | tee run_mpi_hybriad_ib.32x16x16._mvapich.log
dat 32 32 16
hybrid | tee run_mpi_hybriad_ib.32x32x16._mvapich.log
dat 32 32 32
hybrid | tee run_mpi_hybriad_ib.32x32x32._mvapich.log
dat 64 32 32
hybrid | tee run_mpi_hybriad_ib.64x32x32._mvapich.log
dat 64 64 32
hybrid | tee run_mpi_hybriad_ib.64x64x32._mvapich.log
dat 64 64 64
hybrid | tee run_mpi_hybriad_ib.64x64x64._mvapich.log
dat 96 64 64
hybrid | tee run_mpi_hybriad_ib.96x64x64._mvapich.log
dat 96 96 64
hybrid | tee run_mpi_hybriad_ib.96x96x64._mvapich.log
dat 96 96 96
hybrid | tee run_mpi_hybriad_ib.96x96x96._mvapich.log
dat 104 104 104
hybrid | tee run_mpi_hybriad_ib.104x104x104._mvapich.log
