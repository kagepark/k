hybrid() {
  max_loop=4
  module load openmpi
  host_list=(hpcgib1 hpcgib2 hpcgib3 hpcgib4)
  for ((ii=1; ii<=$max_loop; ii++)); do
    echo
    echo " == LOOP $ii/$max_loop ($(cat hpcg.dat | tail -n2 | head -n1)) =="
    for jj in 1 2 4; do
      rm -f hostfile
      host_str=""
      for ((qq=0;qq<$jj;qq++)); do
         echo "${host_list[$qq]} slots=32" >> hostfile
         [ -n "$host_str" ] && host_str="${host_str},${host_list[$qq]}" || host_str=${host_list[$qq]}
      done
      #for omp in 1 2 4 8 16 32; do
      #for omp in 1 32; do
      for omp in 1; do
         if [ "$jj" == "1" -a "$omp" == "32" ]; then
            omp_run="OMP_NUM_THREADS=$omp ./xhpcg"
            echo -n " ** Run Total Task($omp) : $omp_run : "
            eval $omp_run
         else
            if [ "$jj" == "1" ]; then
               hy_run="OMP_NUM_THREADS=$omp mpirun --allow-run-as-root -np $(($((32/$omp)) * $jj)) --mca btl ^tcp,self ./xhpcg"
            elif [ "$omp" == "1" ]; then
               hy_run="OMP_NUM_THREADS=$omp mpirun --allow-run-as-root --mca btl ^tcp --hostfile ./hostfile ./xhpcg"
            else
               hy_run="OMP_NUM_THREADS=$omp mpirun --allow-run-as-root -np $jj --mca btl ^tcp --host $host_str ./xhpcg"
            fi
            echo -n " ** Run Total Task($((32 * $jj))) : $hy_run : "
            eval $hy_run
         fi
  
         file=$(ls -tr HPCG-Benchmark*.yaml| tail -n1)
         echo "$file : $(grep "HPCG result is VALID with a GFLOP/s rating of" $file | awk -F: '{print $2}')"
         sleep 25
      done
    done
  done
}

dat() {
echo "HPCG benchmark input file
Sandia National Laboratories; University of Tennessee, Knoxville
$1 $2 $3
60" > hpcg.dat
}


for mt in 16x16x16 32x16x16 32x32x16 32x32x32 64x32x32 64x64x32 64x64x64 96x64x64 96x96x64 96x96x96 104x96x96 104x104x96 104x104x104; do
  dat $(echo $mt | sed "s/x/ /g")
  hybrid | tee run_mpi_hybriad_ib.${mt}_$(basename $0).log
done
exit
dat 32 16 16
hybrid | tee run_mpi_hybriad_ib.32x16x16._mpich3_3rd.log
dat 32 32 16
hybrid | tee run_mpi_hybriad_ib.32x32x16._mpich3_3rd.log
dat 32 32 32
hybrid | tee run_mpi_hybriad_ib.32x32x32._mpich3_3rd.log
dat 64 32 32
hybrid | tee run_mpi_hybriad_ib.64x32x32._mpich3_3rd.log
dat 64 64 32
hybrid | tee run_mpi_hybriad_ib.64x64x32._mpich3_3rd.log
dat 64 64 64
hybrid | tee run_mpi_hybriad_ib.64x64x64._mpich3_3rd.log
dat 96 64 64
hybrid | tee run_mpi_hybriad_ib.96x64x64._mpich3_3rd.log
dat 96 96 64
hybrid | tee run_mpi_hybriad_ib.96x96x64._mpich3_3rd.log
dat 96 96 96
hybrid | tee run_mpi_hybriad_ib.96x96x96._mpich3_3rd.log
dat 104 104 104
hybrid | tee run_mpi_hybriad_ib.104x104x104._mpich3_3rd.log
