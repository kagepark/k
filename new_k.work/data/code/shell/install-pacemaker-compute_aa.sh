#!/bin/sh
#
# 2015/03/28  Kage Park Initial version for Gateway pacemaker
# 2015/04/02  Kage Park Adding stonth
# 2015/04/03  Kage Park update parameter and fix few issue. add change order
#

error_exit() {
   echo "$*"
   exit 1
}

###########################
# Global values
###########################
auto_failback=1        # 0: off , 1: on
ha_type=aa             # as: Active/Standby , aa : Active/Active

# Pingd
multiplier=100         # default : 1000
takeover=1             # 0: lost whole ping, 1: lost anyone ping
interval=30s           # Monitoring interval (default: 30s)
timeout=10s            # monitoring timeout  (default: 10s)

# Stonith
ipmi_id=ace
ipmi_pass=ace
stonith=1              # 0: off , 1: on
stonith_interval=60s   # default 60s

#############################
# Auto set for global value
#############################
[ "$ha_type" == "aa" ] && auto_failback=1


#############################
# Functions
#############################

pcm_db_cleanup() {
  # Pacemaker clean up
  [ -f /etc/init.d/pacemaker ] || exit
  /etc/init.d/pacemaker status >& /dev/null && pcs cluster stop
  
  # Common node clean up
  for ii in /var/lib/cluster /var/lib/corosync /var/lib/pacemaker; do
      if [ -d $ii ]; then
        mounted_dev=$(awk -v mdir=$ii '{if($2==mdir) print $1}' /proc/mounts)
        if [ -n "$mounted_dev" ]; then
          umount $ii || umount -l $ii
        fi
      fi
  done

  # Compute node clean up
  if [ -f /.ace/ace_server ]; then
    ha_cluster_ip=$(cat /.ace/acefs/servers/server-0001/failover_ip1)
    checkout=$(< /.ace/acefs/self/host/checkout)
    for hh in /var/lib/pacemaker /var/lib/cluster /var/lib/corosync; do
      for uu in $(cat /proc/mounts | grep "^$ha_cluster_ip" | grep "$hh"| awk '{print $2}'); do
        umount $uu || umount -l $uu
      done
    done
    server_name=$(cat /.ace/acefs/self/server_name)
    [ -d /.ace/ha_cluster/hostfs/$server_name ] && rm -fr /.ace/ha_cluster/hostfs/$server_name
    [ "$checkout" == "0" ] && exit 0
  fi

  # checkout or standalone server then initial db directory
  # Set directory to standalone node 
  for ii in /var/lib/cluster /var/lib/corosync /var/lib/pacemaker; do
      [ -d $ii ] ||  mkdir -p $ii
  done

  [ -d /var/lib/pacemaker/pengine ] && rm -f /var/lib/pacemaker/pengine/* || mkdir -p /var/lib/pacemaker/pengine
  [ -d /var/lib/pacemaker/cib ] && rm -f /var/lib/pacemaker/cib/* || mkdir -p /var/lib/pacemaker/cib
  [ -d /etc/corosync ] && rm -f /etc/corosync/pacemaker.xml || mkdir -p /etc/corosync

  chown hacluster:haclient /var/lib/pacemaker
  chown hacluster:haclient /var/lib/pacemaker/*
  (( $(ls /var/lib/pacemaker/pengine | wc -l) > 0 )) && chown hacluster:root /var/lib/pacemaker/pengine/*
  (( $(ls /var/lib/pacemaker/cib | wc -l) > 0 )) && chown hacluster:root /var/lib/pacemaker/cib/*
}

check_ipmi() {
    local ha_nodes ha_nodes_arr ipmi_type
    ha_nodes=$*
    ha_nodes_arr=($ha_nodes)
    for ((ii=0; ii<${#ha_nodes_arr[*]}; ii++)); do
        echo "Check IPMI to ${ipmis_arr[$ii]}"
        if ipmitool -I lan -L operator -U $ipmi_id -P $ipmi_pass -H ${ipmis_arr[$ii]} chassis power status  >& /dev/null; then
           ipmi_type="with password"
        elif ipmitool -I lan -A NONE -H ${ipmis_arr[$ii]} chassis power status >& /dev/null; then
           ipmi_type="none"
        else
           error_exit "Please check IPMI first !! IPMI is not correctly working now"
        fi
    done
    [ "$ipmi_type" == "none" ] && return 2 || return 0
}

stonith_on() {
    local ipmis ha_nodes ipmis_arr ha_nodes_arr ipmi_type
    ipmi_type=$1
    ipmis=$2
    ha_nodes=$3
    echo "Configure fencing to use IPMI(Stonith) at $ipmis"

    # Stonith : Configure fencing to use IPMI
    ipmis_arr=($ipmis)
    ha_nodes_arr=($ha_nodes)

    for ((ii=0; ii<${#ha_nodes_arr[*]}; ii++)); do
        if [ "$ipmi_type" == "none" ]; then
            pcs stonith create fence_${ha_nodes_arr[$ii]}_ipmi fence_ipmilan pcmk_host_list=${node_arr[$ii]} ipaddr=${ipmis_arr[$ii]} action=reboot method=cycle  delay=15s privlvl=operator op monitor interval=$stonith_interval
        else
            pcs stonith create fence_${ha_nodes_arr[$ii]}_ipmi fence_ipmilan pcmk_host_list=${node_arr[$ii]} ipaddr=${ipmis_arr[$ii]} action=reboot method=cycle  delay=15s login=$ipmi_id passwd=$ipmi_pass privlvl=operator op monitor interval=$stonith_interval
        fi
        pcs constraint location fence_${ha_nodes_arr[$ii]}_ipmi avoids ${node_arr[$ii]}
    done

    pcs resource create do_stonith lsb:do_stonith --clone 
#    pcs resource clone do_stonith  do_stonith-clone globally-unique=false
    
    # Enable stonith/fencing
    pcs property set stonith-enabled=true
}

stonith_off() {
    pcs config stonith-enabled=false
    pcs property set stonith-enabled=false
}

pingd() {
    local ping_list ha_group_name
    ping_list=$1
    ha_group_name=$2
    
    pcs resource create ping_test ocf:pacemaker:ping host_list="$ping_list" multiplier="$multiplier" dampen=10s op monitor interval=$interval timeout=$timeout --clone

#    pcs resource create ping_test ocf:pacemaker:ping host_list="$ping_list" multiplier="$multiplier" dampen=10s op monitor interval=$interval timeout=$timeout
#    pcs resource clone ping_test  ping_test_clone globally-unique=false

    if [ "$ha_type" == "aa" ]; then
        #Colocate VIP resources and ping resources
        ha_group_name_arr=($ha_group_name)
        for ii in ${ha_group_name_arr[*]}; do
            pcs constraint colocation add $ii with ping_test score=INFINITY 2>/dev/null || error_exit "ERROR: pcs constraint colocation add $ii with ping_test score=INFINITY"
        done
    fi
}

pingd_rule() {
    local ping_list ping_list_arr pingd_act
    pingd_act=$1
    shift 1
    ping_list=$*
    [ "$stonith" == "1" ] && pingd_act="do_stonith"
    
    if [ "$takeover" == "0" ]; then
        pingd_rule="lte 0"
    elif [ "$takeover" == "1" ]; then
        ping_list_arr=($ping_list)
        pingd_rule="lt $(( ${#ping_list_arr[*]} * $multiplier))"
    fi
    pcs constraint location $pingd_act rule score="-INFINITY" not_defined pingd or pingd $pingd_rule
}

do_stonith_init_d() {
    echo "#!/bin/sh
#
# chkconfig: 2345 42 80
# description: Stonith action script
#

# source function library
. /etc/rc.d/init.d/functions

RETVAL=0

start() {
  return 0
}

stop() {
   if [ ! -f /.ace/tmpfs/var/run/do_stonith ]; then
       echo \$(date) >> /.ace/tmpfs/var/run/do_stonith
       return 0
   else
       echo \$(date) >> /.ace/tmpfs/var/run/do_stonith
       return 1
   fi
}

status_chk() {
   return 0
}

case \"\$1\" in
    start)      start; RETVAL=\$?;;
    stop)       stop; RETVAL=\$?;;
    status)     status_chk; RETVAL=\$?;;
    *)          echo \"Usage: \$0 {start|stop|restart|status}\"; exit 1;;
esac

exit \$RETVAL " > /etc/init.d/do_stonith
    chmod +x /etc/init.d/do_stonith

}

pcm_install() {
######################
# compute node
######################
if [ -f /.ace/ace_server ]; then
  #
  # checkout node
  #
  if [ "$(cat /.ace/acefs/self/server/host/checkout)" == "1" ]; then
    cluster_name=$1
    nodes=$2
    pcm_port=$3
    iso_file=$4
    iso_mnt=$5

    # Checkup
    if [ ! -n "$cluster_name" -o ! -n "$nodes" -o ! -n "$pcm_port" -o ! -n "$iso_file" -o ! -n "iso_mnt" ]; then
       echo 
       echo "Install pacemaker package and setup base cluster configuration"
       echo
       error_exit "$(basename $0) install <cluster name> \"node1:alt_node1 node2:alt_node2 ...\" <pacemaker port#> <OS iso file> <OS iso mount dir>"
    fi

    # cleanup existing pacemaker db
    pcm_db_cleanup

    for ii in $nodes; do
      pri_node=$(echo $ii | awk -F: '{print $1}')
      alt_node=$(echo $ii | awk -F: '{print $2}')
      if ! awk -v host=$pri_node '{if($1 == host || $2 == host) printf "ok"}' /etc/hosts | grep "^ok" >& /dev/null; then
         error_exit "Not found $ii in /etc/hosts"
      fi

      if ! awk -v host=$alt_node '{if($1 == host || $2 == host) printf "ok"}' /etc/hosts | grep "^ok" >& /dev/null ; then
        error_exit "Not found $ii in /etc/hosts"
      fi
    done

    [ -f "$iso_file" ] || error_exit "$iso_file not found"
    if [ ! -d "$iso_mnt" ]; then
      mkdir -p "$iso_mnt" || error_exit "$iso_mnt can't create"
    fi

    pacemaker_repo=/etc/yum.repos.d/pacemaker.repo

    # heartbeat down
    chkconfig --list | grep heartbeat| grep ":on" >& /dev/null && chkconfig --level 12345 heartbeat off

    cat /etc/yum.conf | grep pcs >& /dev/null && error_exit "Please fix /etc/yum.conf. currently blocked deal with pacemaker"

    mount -o loop $iso_file $iso_mnt

    echo "[rhel6-os]
name=RHEL 6
baseurl=file://////$iso_mnt
enable=1
gpgcheck=1
gpgkey=file://////etc//pki//rpm-gpg//RPM-GPG-KEY-redhat-release

[rhel6-HA]
name=rhel 6 HA
baseurl=file://////$iso_mnt//HighAvailability
enabled=1
gpgcheck=1
gpgkey=file://////etc//pki//rpm-gpg//RPM-GPG-KEY-redhat-release
    " > $pacemaker_repo

    yum list pcs >& /dev/null || error_exit "pacemaker not found in $iso_mnt/HighAvailability"

    yum -y install pacemaker cman corosync pcs

    rm -f $pacemaker_repo

    [ -d /etc/corosync ] || mkdir -p /etc/corosync
    dd if=/dev/urandom of=/etc/corosync/authkey bs=1 count=128
    chmod 0400 /etc/corosync/authkey

    #Create a CMAN cluster cluster
    [ -d /var/lib/corosync ] || mkdir -p /var/lib/corosync
    ccs -f /etc/cluster/cluster.conf -i --createcluster $cluster_name

    # Add the Gateway nodes to the cluster &
    # Add the second communication ring for redundancy
    for ii in $nodes; do
      pri_node=$(echo $ii | awk -F: '{print $1}')
      alt_node=$(echo $ii | awk -F: '{print $2}')
      ccs -f /etc/cluster/cluster.conf --addnode $pri_node
      sleep 1
      ccs -f /etc/cluster/cluster.conf --addalt $pri_node $alt_node
      sleep 1
    done

    #The following tells CMAN how to send it’s fencing requests to Pacemaker. 
    for ii in $nodes; do
      pri_node=$(echo $ii | awk -F: '{print $1}')
      ccs -f /etc/cluster/cluster.conf --addmethod pcmk-redirect $pri_node
      sleep 1
    done
    ccs -f /etc/cluster/cluster.conf --addfencedev pcmk agent=fence_pcmk

    for ii in $nodes; do
      pri_node=$(echo $ii | awk -F: '{print $1}')
      ccs -f /etc/cluster/cluster.conf --addfenceinst pcmk $pri_node pcmk-redirect port=$pri_node
      sleep 1
    done

    # Set port & secure (It should different port number as HA-mgmt, HA-nfs, ... Others pacemaker in same network)
    ccs -f /etc/cluster/cluster.conf --setcman keyfile="/etc/corosync/authkey" transport="udpu" port="$pcm_port"
    cat /etc/cluster/cluster.conf | grep port
    sleep 5

    # Make a active for redundant 
    ccs -f /etc/cluster/cluster.conf --settotem rrp_mode="active"

    # Add quorum timeout to cman config      ==> quorum requires minimum 3 nodes
    grep "^CMAN_QUORUM_TIMEOUT=0" /etc/sysconfig/cman >& /dev/null || echo "CMAN_QUORUM_TIMEOUT=0" >> /etc/sysconfig/cman


    #Setup service file
    echo "service {
      ver:  1
      name: pacemaker
}" > /etc/corosync/service.d/pcmk

    chkconfig --level 35 cman on
    chkconfig --level 35 pacemaker on

    # Make a stonith manage script
    [ "$stonith" == "1" ] && do_stonith_init_d

  #
  # Checkin node
  #
  elif [ "$(cat /.ace/acefs/self/server/host/checkout)" == "0" ]; then
    ping_list=$1
    ha_ips=$2
    ha_nodes=$3
    ipmis=$4

    if [ ! -n "$ping_list" -o ! -n "$ha_ips" -o ! -n "$ha_nodes" -o ! -n "$ipmis" ]; then
       echo 
       echo "Setup pacemaker DB"
       echo
       echo "$(basename $0) install <ping IP list> <HA IPs> <NODEs> <IPMI IPs>"
       echo 
       echo "<ping IP list> : \"10.4.0.200  10.10.0.200\""
       echo "<HA IPs>       : \"<HA IPs Group in Server-1>             <HA IPs Group in Server-2>\""
       echo "   example)      \"eth0:10.10.3.101:16|ib0:10.12.3.101:16 eth0:10.10.3.102:16|ib0:10.12.3.102:16\""
       echo "<NODEs>        : \"<Server-1 name> <Server-2 name>\""
       echo "   example)      \"gw1             gw2\""
       echo "<IPMI IPs>     : \"<Server-1 IPMI IP> <Server-2 IPMI IP>\""
       echo "   example)      \"10.10.128.50       10.10.128.51\""
       exit 1
    fi

    for ii in $ping_list; do
        echo "Checking pingable to $ii"
        ping -c 2 $ii >& /dev/null || error_exit "Can't ping to $ii"
    done
    check_ipmi $ipmis
    [ "$?" == "2" ] && ipmi_type="none" || ipmi_type="with password"
    echo " - Find IPMI Type : $ipmi_type"

    if [ -f /etc/init.d/pacemaker ]; then
        /etc/init.d/pacemaker status >& /dev/null || /etc/init.d/pacemaker start
        echo "Check pacemaker status :"
        while [ 1 ]; do
           echo -n "."
           chk_node_up=0
           for ii in $(pcs status nodes | grep "Online" | awk -F: '{print $2}'); do
               if ip addr | grep "$ii" >& /dev/null; then
                  chk_node_up=1
                  break
               fi
           done
           [ "$chk_node_up" == "1" ] && break || sleep 2
        done
        sleep 2
        echo
    else
        error_exit "Pacemaker not found in init.d"
    fi

    # Global setting and Initially stonith/fencing is disabled.
    pcs property set stonith-enabled=false
    pcs property set no-quorum-policy=ignore
    pcs resource op defaults timeout=240s
    pcs resource defaults migration-threshold=1

    #Set HA IPs 
    ha_nodes_arr=($ha_nodes)
    ha_ips_arr=($ha_ips)
    for ((zz=0; zz<${#ha_nodes_arr[*]}; zz++)); do
      for ii in $(echo ${ha_ips_arr[$zz]} | sed "s/|/ /g"); do
        dev=$(echo $ii | awk -F: '{print $1}')
        ha_ip=$(echo $ii | awk -F: '{print $2}')
        cidr=$(echo $ii | awk -F: '{print $3}')
        pcs resource create ha_${ha_nodes_arr[$zz]}_${dev} IPaddr2 ip=$ha_ip cidr_netmask=$cidr nic=$dev op monitor interval=20s timeout=5s
        pcs_group[$zz]="${pcs_group[$zz]}ha_${ha_nodes_arr[$zz]}_${dev} "
        sleep 1
      done
    done
   
    # Create resource group for each node’s Ethernet and IB VIP
    for ((zz=0; zz<${#ha_nodes_arr[*]}; zz++)); do
        pcs resource group add HA_${ha_nodes_arr[$zz]} ${pcs_group[$zz]}
        HA_GROUP_NAME="${HA_GROUP_NAME}HA_${ha_nodes_arr[$zz]} "
    done

    # Set Active / Active to VIP
    if [ "$ha_type" == "aa" ]; then
        #Set a preferred location for each VIP resource group
        node_arr=($(crm_node -l))
        ha_group_name_arr=($HA_GROUP_NAME)
        for ((ii=0; ii<${#ha_group_name_arr[*]}; ii++)); do
            pcs constraint location ${ha_group_name_arr[$ii]} prefers ${node_arr[$ii]}=50 2>/dev/null || error_exit "ERROR: pcs constraint location ${ha_group_name_arr[$ii]} prefers ${node_arr[$ii]}=50"
        done
    fi

    # Turn off fail back function.
    if [ "$auto_failback" != "1" ]; then
      echo "Set auto failback disable"
      for jj in $(crm_node -l); do
        for ((ii=0;ii<${#pcs_group[*]};ii++)); do
          for zz in ${pcs_group[$ii]}; do
            pcs constraint location $zz  prefers  ${jj}=INFINITY
            sleep 1
          done
        done
      done
      pcs resource defaults resource-stickiness=100
    fi

    #PINGd
    pingd "$ping_list" "${ha_group_name_arr[*]}"

    # Configure stonith/fencing to use IPMI
    if [ "$stonith" == "1" ]; then
        # Turn on stonith : Configure fencing to use IPMI
        stonith_on "$ipmi_type" "$ipmis" "${ha_nodes_arr[*]}"
        pcs property set stonith-enabled=true
    else
        # Turn off stonith
        stonith_off 
    fi

    # Setup ping resource rule
    pingd_rule "$pingd_act" $ping_list
  fi

elif [ -d /acefs/global/mount ]; then

####################
# MGMT node
####################

  gw_start=$1
  gw_end=$2

  if [ ! -n "$gw_start" -o ! -n "$gw_end" ]; then
    echo 
    echo "Setup boot_commands.d script for compute node pacemaker DB"
    echo
    error_exit "$(basename $0) install <start server id> <end server id>"
  fi

  echo "
gw_start=$gw_start
gw_end=$gw_end
server_id=\$(cat /.ace/acefs/self/server_id)
if (( \$server_id >= \$gw_start && \$server_id <= \$gw_end )); then
    if [ ! -d /.ace/ha_cluster/hostfs/\$(cat /.ace/acefs/self/server_name)/var/lib/cluster ]; then
         mkdir -p /.ace/ha_cluster/hostfs/\$(cat /.ace/acefs/self/server_name)/var/lib/cluster
         cp -a /var/lib/cluster/* /.ace/ha_cluster/hostfs/\$(cat /.ace/acefs/self/server_name)/var/lib/cluster
    fi
    mount -o bind /.ace/ha_cluster/hostfs/\$(cat /.ace/acefs/self/server_name)/var/lib/cluster /var/lib/cluster

    if [ ! -d /.ace/ha_cluster/hostfs/\$(cat /.ace/acefs/self/server_name)/var/lib/pacemaker ]; then
         mkdir -p /.ace/ha_cluster/hostfs/\$(cat /.ace/acefs/self/server_name)/var/lib/pacemaker
         cp -a /var/lib/pacemaker/* /.ace/ha_cluster/hostfs/\$(cat /.ace/acefs/self/server_name)/var/lib/pacemaker
    fi
    mount -o bind /.ace/ha_cluster/hostfs/\$(cat /.ace/acefs/self/server_name)/var/lib/pacemaker /var/lib/pacemaker

    if [ ! -d /.ace/ha_cluster/hostfs/\$(cat /.ace/acefs/self/server_name)/var/lib/corosync ]; then
         mkdir -p /.ace/ha_cluster/hostfs/\$(cat /.ace/acefs/self/server_name)/var/lib/corosync
         cp -a /var/lib/corosync/* /.ace/ha_cluster/hostfs/\$(cat /.ace/acefs/self/server_name)/var/lib/corosync
    fi
    mount -o bind /.ace/ha_cluster/hostfs/\$(cat /.ace/acefs/self/server_name)/var/lib/corosync /var/lib/corosync
  fi " > /acefs/global/mount/boot_commands.d/0999-pacemaker

fi
}

cmd=$1
shift 1

if [ "$cmd" == "--help" -o "$cmd" == "-h" -o "$cmd" == "help" ]; then
  echo "
  Pacemaker install/setup script for IP gateway on compute node
    - support stonith
    - support ACTIVE/ACTIVE
    - support ACTIVE/STANDBY

  $(basename $0) <cmd> [<options>]

  <cmd> 
   --help            : this screen
   install           : help for the install
   install <options> : install pacemaker with the <options>
   cleanup           : cleanup database
   change_order      : change resource order in group, help for the change_order
  "
elif [ "$cmd" == "install" ]; then
  pcm_install $*
elif [ "$cmd" == "cleanup" ]; then
  if [ -d /.ace/tmpfs ]; then
    echo "Are you sure cleanup DB of pacemaker ? (y/[n])"
    read xy
    [ "$xy" == "y" ] || exit 0
    # clean up db
    pcm_db_cleanup
    if [ "$(< /.ace/acefs/self/host/checkout)" == "0" ]; then
        #make a writable db on compute node
        if [ -f /.ace/acefs/global/mount/boot_commands.d/0999-pacemaker ]; then
           sh /.ace/acefs/global/mount/boot_commands.d/0999-pacemaker
        else
           error_exit "/.ace/acefs/global/mount/boot_commands.d/0999-pacemaker not found"
        fi
    fi
    echo
    echo " ** Please must remove pacemaker DB in secondary node before setup(install) pacemaker on this server !!"
    echo
  else
    echo "Looks this server is standalone or MGMT node. not support yet"
#    echo -n "Are you sure remove pacemaker DB ? (y/[n]) "
#    read xy
#    [ "$xy" == "y" ] && pcm_db_cleanup
  fi
elif [ "$cmd" == "change_order" ]; then
  group_name=$1
  shift 1
  res_list=$*
  [ -n "$group_name" ] && old_res_list=$(pcs resource group list | awk -v grp_name=$group_name -F: '{if($1 == grp_name) print $2}')
  if [ -n "$old_res_list" ]; then
      pcs resource group delete $group_name $old_res_list
  else
      echo
      echo "Change resource order in the group with resource list"
      echo
      error_exit "$(basename $0) change_order <group name> <resource1> [<resource2>....]"
  fi
  pcs resource group add $group_name $res_list
fi
