#!/bin/bash
#
# Install RAID controller tools and crontab script for LSI MegaRAID SAS (megaraid_sas).
# 
# Author: Anders Lövgren
# Date:   2013-07-05
# 

function setup_gentoo()
{
    emerge sys-block/megacli
}

function setup_debian()
{
    wget http://hwraid.le-vert.net/debian/pool-wheezy/megacli_8.04.07-1_amd64.deb
    dpkg -i megacli_8.04.07-1_amd64.deb
}

function setup_redhat()
{
    wget http://it.bmc.uu.se/andlov/docs/linux/raid/files/megacli-8.04.07-2.x86_64.rpm
    rpm -ihv megacli-8.04.07-2.x86_64.rpm
}

function setup_slackware()
{
    http://hwraid.le-vert.net/debian/pool-wheezy/megacli_8.04.07-1_amd64.deb
    alien -t megacli_8.04.07-1_amd64.deb
    installpkg megacli-8.04.07.txz
}

function setup_crontab()
{
    if ! [ -e /etc/cron.hourly/megacli ]; then
        mv /etc/cron.hourly/megacli /etc/cron.hourly/megacli.old
    fi

    wget http://it.bmc.uu.se/andlov/docs/linux/raid/files/megacli.cron
    mv megacli.cron /etc/cron.hourly/megacli
}

case "$1" in
    gentoo)
        setup_gentoo
        setup_crontab
        ;;
    debian)
        setup_debian
        setup_crontab
        ;;
    redhat)
        setup_redhat
        setup_crontab
        ;;
    slackware)
        setup_slackware
        setup_crontab
        ;;
    *)
        echo "usage: $0 {gentoo|debian|redhat|slackware}"
        exit 1
esac
