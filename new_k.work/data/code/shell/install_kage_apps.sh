opwd=$(pwd)
yum install -y mc screen iptables wget openssl-devel lz4-devel lzo-devel pam-devel
yum install -y epel-release
yum install -y terminator
wget https://go.skype.com/skypeforlinux-64.rpm -O /tmp/skypeforlinux-64.rpm
yum install -y /tmp/skypeforlinux-64.rpm
wget https://download.virtualbox.org/virtualbox/5.2.8/VirtualBox-5.2-5.2.8_121009_el7-1.x86_64.rpm -O /tmp/virtualbox.rpm
yum install -y /tmp/virtualbox.rpm
wget https://download.teamviewer.com/download/linux/teamviewer.x86_64.rpm -O /tmp/teamviewer.rpm
yum install -y /tmp/teamviewer.rpm
wget https://swupdate.openvpn.org/community/releases/openvpn-2.4.5.tar.gz -O /tmp/openvpn.tgz
mkdir -p /tmp/openvpn
tar zxvf /tmp/openvpn.tgz -C /tmp/openvpn
cd /tmp/openvpn/*
./configure
make
cp -a src/openvpn/openvpn $(opwd)
