#!/bin/sh
#
# Kage 2012-09-03
#$version$:0.1.10-20120903
#
# xCAT installation

os=1 #0: rhel, 1:suse
mgmt_name=xcatm
mgmt_dev=eth0
mgmt_ip=10.60.0.1
mgmt_netmask=255.255.0.0

ext_ip=192.168.1.140
ext_dev=eth1
mgmt_resov="192.168.1.6 68.87.85.98 68.87.69.146 205.196.176.90 72.20.133.20"

serial_port=1
serial_speed=19200

node_num=84
node_bmc_ip=10.70.10.0
node_ip=10.60.10.0
switch_ip=10.60.3.1
node_obj_name=nodeperrack
bmc_obj_name=bmcperrack
domainname=clusters
tftp_dir=/tftpboot

node_name=n
bmc_name=bmc
switch_name=switch
switch_num=4


error_exit() {
    echo $*
    exit
}

echo1() {
    echo
    echo $*
    echo
    sleep 5
}

echo1 check defaut gw
netstat -nr | grep "^0.0.0.0" >/dev/null || error_exit "default gw not found"

echo1 check resolv.conf
if ! grep "^nameserver" /etc/resolv.conf; then
    for ip in $(mgmt_resolv); do
        echo1 adding $ip
        echo "nameserver $ip" >> /etc/resolv.conf
    done
fi

echo1 check outside network
ping -c 5 sourceforge.net >/dev/null || error_exit "can't not access to outside"

if [ "$os" != "1" ]; then
    echo1 check SELinux
    selinuxenabled && error_exit "SELinux must be disabled! Please update /etc/selinux/config and reboot."
fi

echo1 check Firewall
if [ "$os" == "1" ]; then
   for fwn in $(chkconfig --list |grep -i firewall|awk '{print $1}'); do
      chkconfig --level 012345 $fwn off
      service $fwn stop
   done
else
   for fwn in $(chkconfig --list |grep -i ip | grep table |awk '{print $1}'); do
      chkconfig --level 012345 $fwn off
      service $fwn stop
   done
fi

#echo1 disable service
#if [ "$os" == "1" ]; then
#   rpm -e --nodeps "$(rpm -qa |grep "^tftp")"
#else
#   rpm -e --nodeps "$(rpm -qa |grep "^tftp-server")"
#fi

echo1 check $mgmt_dev for xcat
if [ -d /sys/class/net/$mgmt_dev ]; then
    get_mgmt_ip=$(ifconfig $mgmt_dev | grep "inet addr:" | awk '{print $2}'| awk -F: '{print $2}')
    get_mgmt_netmask=$(ifconfig $mgmt_dev | grep "inet addr:" | awk '{print $4}'| awk -F: '{print $2}')
    if [ "$get_mgmt_ip" != "$mgmt_ip" -o "$get_mgmt_netmask" != "$mgmt_netmask" ]; then
        echo1 "mgmt device ($mgmt_dev) has different configuration"
        echo "auto set (y|n)?"
        read x
        if [ "$x" == "y" ]; then
           if [ "$os" == "1" ]; then
               echo "BOOTPROTO='static'
BROADCAST=''
ETHTOOL_OPTIONS=''
IPADDR='$mgmt_ip'
MTU=''
NAME=''
NETMASK='$mgmt_netmask'
NETWORK=''
REMOTE_IPADDR=''
STARTMODE='auto'
USERCONTROL='no'" > /etc/sysconfig/network/ifcfg-$mgmt_dev
           else
               echo "DEVICE=$mgmt_dev
BOOTPROTO=none
ONBOOT=yes
NETMASK=$mgmt_netmask
IPADDR=$mgmt_ip
TYPE=Ethernet
USERCTL=no
IPV6INIT=no
PEERDNS=yes " > /etc/sysconfig/network-scripts/ifcfg-$mgmt_dev
               
           fi
           ifdown $mgmt_dev
           ifup $mgmt_dev
        else
           exit
        fi
    fi
else
    error_exit "$mgmt_dev not found"
fi

echo1 check /etc/hosts
if ! grep "^$mgmt_ip" /etc/hosts >/dev/null; then
   echo "$mgmt_ip      $mgmt_name $mgmt_name.$domainname" >> /etc/hosts
fi
[ -n "$(hostname)" ] ||  hostname $mgmt_name
[ -n "$(domainname)" ] ||  domainname $domainname
if [ "$os" == "1" ]; then
  echo "${mgmt_name}.$domainname" > /etc/HOSTNAME
else
  grep -e "^HOSTNAME" /etc/sysconfig/network > /etc/sysconfig/network~
  mv /etc/sysconfig/network~ /etc/sysconfig/network
  echo "${mgmt_name}.$domainname" >> /etc/sysconfig/network
fi
  


echo1 check ntp
[ -f /etc/ntp.conf ] && cp -a /etc/ntp.conf /etc/ntp.conf.xcat
echo "driftfile /var/lib/ntp/drift
disable auth
restrict 127.0.0.1
server  127.127.1.0     # local clock
fudge   127.127.1.0 stratum 10" > /etc/ntp.conf
if [ "$os" == "1" ]; then
   service ntp restart
else
   service ntpd restart
fi
sleep 2
ntpq -p

echo1 Install xCAT
if [ "$os" == "1" ]; then
  zypper lr | grep "xCAT-core" && zypper rr xCAT-core
  zypper ar -t rpm-md http://sourceforge.net/projects/xcat/files/yum/stable/xcat-core xCAT-core
  zypper lr | grep "xCAT-dep" && zypper rr xCAT-dep
  zypper ar -t rpm-md http://sourceforge.net/projects/xcat/files/yum/xcat-dep/sles11/x86_64 xCAT-dep 
  zypper install xCAT  
else
  cd /etc/yum.repos.d
  wget http://sourceforge.net/projects/xcat/files/yum/stable/xcat-core/xCAT-core.repo || error_exit "can't get repo"
  wget http://sourceforge.net/projects/xcat/files/yum/xcat-dep/rh6/x86_64/xCAT-dep.repo || error_exit "can't get repo"
  yum clean metadata
  yum install xCAT
fi

echo1 requirement packages
if [ "$os" == "1" ]; then
  zypper install apache2-mod_php5 php5 php5-openssl
else
  yum install apache2-mod_php5 php5 php5-openssl net-snmp?net-snmp-utils.x86_64
fi


echo1 load xcat environment
[ -f /etc/profile.d/xcat.sh ] || error_exit "xcat.sh file not found"
source /etc/profile.d/xcat.sh

tabdump site

echo1 check domain name 
chtab key=domain site.value=$domainname
tabdump domain | grep -v "^#" >/dev/null || tabedit domain

echo1 change tftp dir
[ -d $tftp_dir ] || mkdir -p $tftp_dir
chdef  -t site -o clustersite tftpdir="$tftp_dir"
chtab key=tftpdir site.value=$tftp_dir

echo1 Update xCAT
if [ "$os" == "1" ];then
  zypper refresh
  zypper update -t package '*xCAT*'
else
  #yum update '*xCAT*'
  yum update
fi

echo1 Configure xCAT
cd /opt/xcat/share/xcat/templates/e1350/
cp -a hosts.csv hosts.csv.xcat
node_bmc_ip_arr=($(echo $node_bmc_ip|sed "s/\./ /g"))
node_ip_arr=($(echo $node_ip|sed "s/\./ /g"))
switch_ip_arr=($(echo $switch_ip|sed "s/\./ /g"))
echo "#node,ip,hostnames,comments,disable
${node_num}${bmc_obj_name},\"|\D+(\d+).*\$|${node_bmc_ip_arr[0]}.${node_bmc_ip_arr[1]}.(${node_bmc_ip_arr[2]}+((\$1-1)/${node_num})).((\$1-1)%${node_num}+1)|\"
#storagebmc,\"|\D+(\d+).*\$|172.29.1.(\$1+0)|\"
#userbmc,\"|\D+(\d+).*\$|172.29.4.(\$1+0)|\"
${node_num}${node_obj_name},\"|\D+(\d+).*\$|${node_ip_arr[0]}.${node_ip_arr[1]}.(${node_ip_arr[2]}+((\$1-1)/${node_num})).((\$1-1)%${node_num}+1)|\"
switch,\"|\D+(\d+).*\$|${switch_ip_arr[0]}.${switch_ip_arr[1]}.${switch_ip_arr[2]}.(\$1+0)|\"
#bigswitch,\"|\D+(\d+).*\$|172.30.80.(\$1+0)|\"" > hosts.csv

for i in *csv; do 
  echo resotre $i
  tabrestore $i
done

echo1 Add Nodes to the nodelist
#nodeadd n1-n167 groups=ipmi,idataplex,42perswitch,compute,all
nodeadd ${node_name}1-${node_name}${node_num} groups=${node_num}${node_obj_name}
nodeadd ${bmc_name}1-${bmc_name}${node_num} groups=${node_num}${bmc_obj_name} 
(($switch_num>1)) && switches=${switch_name}1-${switch_name}${switch_num} || switches=${switch_name}1
nodeadd ${switchs} groups=switch
nodels
sleep 5
lsdef n1,bmc1,switch1
sleep 5

echo1 Declare a dynamic range of addresses for discovery
num=1
chdef_ip=$(echo $mgmt_ip | awk -F. '{print $1}')
chdef_dy=$chdef_ip
for n in $(echo $mgmt_netmask | awk -F. '{printf "%s %s %s",$2,$3,$4}'); do
   if [ "$n" == "255" ]; then
      num=$(($num+1))
      chdef_ip="${chdef_ip}_$(echo $mgmt_ip | awk -F. -v i=$num '{print $i}')"
      chdef_dy="${chdef_dy}.$(echo $mgmt_ip | awk -F. -v i=$num '{print $i}')"
   else
      chdef_ip="${chdef_ip}_0"
   fi
done
chdef_ip="${chdef_ip}-$(echo $mgmt_netmask|sed "s/\./_/g")"
chdef -t network $chdef_ip dynamicrange=${chdef_dy}.128.1-${chdef_dy}.255.254
chdef -t network $(echo $ext_ip|sed "s/\./_/g")-255_255_255_255 dynamicrange=$ext_ip-$ext_ip
tabdump networks
#if networks has some problem then manually work
#tabedit networks

echo1 Declare use of SOL
chdef -t group -o compute serialport=$serial_port serialspeed=$serial_speed serialflow=hard

echo1 passwd Table
tabedit passwd

echo1 Setup /etc/hosts file
makehosts switch,${node_num}${node_obj_name},${node_num}${bmc_obj_name}
cat /etc/hosts

echo1 Setup DNS
chdef -t site forwarders=1.2.3.4,1.2.5.6
grep "^$domainname" /etc/resolv.conf || echo "search $domainname" >> /etc/resolv.conf
grep "^$mgmt_ip" /etc/resolv.conf || echo "nameserver $mgmt_ip" >> /etc/resolv.conf
makedns && service named start

echo1 Setup DHCP
makedhcp -n

echo1 Setup xCAT UI 
zypper install xCAT-UI

service apache2 restart
service xinetd restart
service xcatd restart

echo "at web browser"
echo "username : root"
echo "password : $(tabdump passwd | grep "\"xcat\"" | awk -F, '{print $3}')"


###########################
#common tables reference
###########################
#site
#nodelist
#nodehm
#ipmi
#mp
#mpa
#networks
#noderes
#passwd
#chain
#switch
#nodetype
#mac
