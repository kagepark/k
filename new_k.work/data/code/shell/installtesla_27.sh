#! /bin/sh
#
# This file becomes the install section of the generated spec file.
#

usage() {
echo "installtesla.sh v1.0rc1"
echo "usage: $0 [tesla package path]"
echo "ex. $0"
echo "ex. INSTALL_ROOT=/mnt/sysimage $0 /mnt/source/bladedome/tesla/5.3"
}

install_latest() {
LATEST=$(ls -t1 ${2}*.rpm|head -1)
echo "installing $LATEST"
[ -f $LATEST ] && rpm -ihv --root $1 --force --nodeps $LATEST
}

install_wholepkgs() {
echo "installing $2 pkgs..."
for PKG in $2/*.rpm
do
	echo "installing $PKG"
	rpm -ivh --force --nodeps --root $1 $PKG
done
}

if [ -z $INSTALL_ROOT ] ; then
	INSTALL_ROOT=/
fi

if [ -z $1 ] ; then
	TESLAPATH=`pwd`
else
	TESLAPATH=$1
fi

if [ ! -d $TESLAPATH ] ; then
	echo "$TESLAPATH does not exist."
	usage
	exit 1
fi

MODULEPATH=$INSTALL_ROOT/lib/modules

echo Install TESLA packages...

sh $TESLAPATH/cudatoolkit_2.2_linux_64_rhel5.3.run
sh $TESLAPATH/cudasdk_2.2_linux.run

rpm -q freeglut-2.4.0-7.1
if [ $? -ne 0 ]; then
	install_latest $INSTALL_ROOT $TESLAPATH/freeglut-2.4.0-7.1*i
	install_latest $INSTALL_ROOT $TESLAPATH/freeglut-2.4.0-7.1*x
fi
rpm -q freeglut-devel-2.4.0-7.1
if [ $? -ne 0 ]; then
	install_latest $INSTALL_ROOT $TESLAPATH/freeglut-devel-2.4.0-7.1*i
	install_latest $INSTALL_ROOT $TESLAPATH/freeglut-devel-2.4.0-7.1*x
fi

#sh $TESLAPATH/NVIDIA-Linux-x86_64-180.22-pkg2.run -a -q -s
#sh $TESLAPATH/NVIDIA-Linux-x86_64-185.18.19-pkg2.run -a -q -s
sh $TESLAPATH/NVIDIA-Linux-x86_64-185.18.27-pkg2.run -a -q -s

nvidia-xconfig -a

TESLAPROFILE=/etc/profile.d/aitutesla.sh
if [ ! -f $TESLAPROFILE ] ; then
	echo 'export PATH=/usr/local/cuda/bin:$PATH' >> $TESLAPROFILE
	echo 'export LD_LIBRARY_PATH=/usr/local/cuda/lib:$LD_LIBRARY_PATH' >> $TESLAPROFILE
	source $TESLAPROFILE
fi
#grep PATH /root/.bashrc 2> /dev/null | grep '/usr/local/cuda/bin'
#[ $? -ne 0 ] && echo 'export PATH=$PATH:/usr/local/cuda/bin' >> /root/.bashrc && export PATH=$PATH:/usr/local/cuda/bin
#grep LD_LIBRARY_PATH /root/.bashrc 2> /dev/null | grep '/usr/local/cuda/lib'
#[ $? -ne 0 ] && echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH/usr/local/cuda/lib' >> /root/.bashrc && export LD_LIBRARY_PATH=$LD_LIBRARY_PATH/usr/local/cuda/lib
#source /root/.bashrc

PWD=$(pwd)
cd /root/NVIDIA_CUDA_SDK/common
make
make dbg=1
make -f Makefile_paramgl
make -f Makefile_paramgl dbg=1

cd /root/NVIDIA_CUDA_SDK/projects/nbody
make
make dbg=1
make emu=1
make emu=1 dbg=1

cd /root/NVIDIA_CUDA_SDK/projects/deviceQuery
make
cd /root/NVIDIA_CUDA_SDK/projects/bandwidthTest
make

echo "Install tesla package done."

