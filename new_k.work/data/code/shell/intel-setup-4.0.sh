export INTEL_LICENSE_FILE=/global/opt/intel/licenses/:${INTEL_LICENSE_FILE}
INTELBASE=/global/opt/intel
ICTVER=3.2.0.020
COMPVERSION=11.0.074
MKLVERSION=10.1.0.015
FORTRANENV=${INTELBASE}/fc/${COMPVERSION}/bin/intel64/ifortvars_intel64.sh
CCENV=${INTELBASE}/cc/${COMPVERSION}/bin/intel64/iccvars_intel64.sh
MKLENV=${INTELBASE}/mkl/${MKLVERSION}/tools/environment/mklvarsem64t.sh
MPIENV=${INTELBASE}/impi/4.0/tp.gen2.20090416/bin64/mpivars.sh
#
#
#
source ${FORTRANENV}
source ${CCENV}
source ${MKLENV}
source ${MPIENV}
