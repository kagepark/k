INTELBASE=/global/opt/intel
INTELVER=10.1.018
MKLVER=10.0.4.023
FORTRANENV=${INTELBASE}/fce/${INTELVER}/bin/ifortvars.sh
CCENV=${INTELBASE}/cce/${INTELVER}/bin/iccvars.sh
MKLENV=${INTELBASE}/mkl/${MKLVER}/tools/environment/mklvars64.sh
#
#
#
source ${FORTRANENV}
source ${CCENV}
source ${MKLENV}
