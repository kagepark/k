#!/bin/sh
#sol_speed : 9.6 | 19.2 | 38.4 | 57.6 | 115.2

export PATH=/usr/bin:/bin:/usr/sbin:/sbin:/global/tools/bin

error_exit() {
  echo $*
  exit 1
}
ipmi_ip=$1
netmask=$2
sol_speed=$3
[ -n "$ipmi_ip" ] || error_exit "$(basename $0) <ipmi ip> <netmask> <sol speed>"
[ -n "$netmask" ] || error_exit "$(basename $0) <ipmi ip> <netmask> <sol speed>"
[ -n "$sol_speed" ] || error_exit "$(basename $0) <ipmi ip> <netmask> <sol speed>"

for rpms in OpenIPMI ipmitool; do
    rpm -qa --queryformat '%{NAME}' | grep -i $rpms >& /dev/null || error_exit "ipmi package not found"
done

if [ ! -c /dev/ipmi0 ]; then
   /etc/init.d/ipmi start
   chkconfig --level 235 ipmi on

   ipmi_dev=$(grep ipmidev /proc/devices | awk '{ print $1; }')
   mknod -m 0666 /dev/ipmi0 c $ipmi_dev 0
   #ls -la /dev/ipmi0
fi

# set IP address
ipmitool lan set 1 ipsrc static
ipmitool lan set 1 ipaddr $ip
ipmitool lan set 1 netmask $netmask
# set authentication levels
auth_list=$(ipmitool lan  print 1 | grep "^Auth Type Support" | sed 's/^.*://')
auth_list=$(echo $auth_list | sed 's/ /,/g')
if [ -n "$auth_list" ]; then
    ipmitool lan set 1 auth user $auth_list
    ipmitool lan set 1 auth operator $auth_list
    ipmitool lan set 1 auth admin $auth_list
else
    ipmitool lan set 1 auth user none,md5,password
    ipmitool lan set 1 auth operator none,md5,password
    ipmitool lan set 1 auth admin none,md5,password
fi
#add user 'ace' for controll
if [ "$(ipmitool user list 1 | awk '{ if( $2 == "ace" ) printf "exist" }' )" != "exist" ]; then
  id_num=$(( $(ipmitool user list 1 | tail -n 1 | awk '{print $1}') + 1 ))
  [ -n "$id_num" ] || id_num=2
  if ! ipmitool channel setaccess 1 $id_num ipmi=on privilege=3 >& /dev/null; then
     if [ $id_num -gt 5 ]; then
       id_num=4
       ipmitool channel setaccess 1 $id_num ipmi=on privilege=3
     fi
  fi
  ipmitool user set name $id_num ace
  ipmitool user set password $id_num ace
  ipmitool user priv 3 $id_num OPERATOR
  ipmitool user enable $id_num
fi

# allow None authentication
ipmitool user enable 1
ipmitool user priv 1 4 1

# set up SOL
ipmitool sol set force-encryption true 1
ipmitool sol set force-authentication true 1
ipmitool sol set privilege-level operator 3
ipmitool sol set non-volatile-bit-rate $sol_speed 1
ipmitool sol set volatile-bit-rate $sol_speed 1

