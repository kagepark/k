_lvm_info() {
vgs  |awk '{if($1!="VG") printf "%s (%s/%s)\n",$1,$7,$6}' | while read vg; do
    echo "$vg"
    vg_name=$(echo $vg | awk '{print $1}')
    pvs | awk -v vg=$vg_name '{if($2 == vg) printf "%s (%s/%s)\n",$1,$6,$5}' | while read pv; do
        echo "   PV NAME: $pv"
    done
    lvs $vg_name | awk '{if($1!="LV") printf "%s (%s)\n",$1,$4}' | while read lv; do
           lv_name=$(echo $lv | awk '{print $1}')
           dev_path=$([ -L /dev/mapper/${vg_name}-${lv_name} ] && echo /dev/mapper/${vg_name}-${lv_name} || echo /dev/${vg_name}/${lv_name})
           echo "   LV NAME: $lv"
           echo "      DEV NAME : $dev_path"
           echo "      MOUNT DIR: $(df -h $dev_path | grep -v "^Filesystem" | awk '{print $6}')"
    done
done
}


aa() {
echo
echo
for part_dir in $(cat /etc/fstab  | grep -v -e "^#" -e " swap "| grep -e "^/dev" -e "^UUID=" | awk '{print $2}'); do
    blk_dev=$(df -h $part_dir | grep -v "^Filesystem" | awk '{print $1}')
    if [ "$(lsblk $blk_dev | awk '{if($6=="lvm") printf "lvm"}')" == "lvm" ]; then
         lv_info=$(lvs $blk_dev | grep -v "LV   VG")
         lv_gname=$(echo "$lv_info" | awk '{print $2}')
         lv_name=$(echo "$lv_info" | awk '{print $1}')
         lv_size=$(echo "$lv_info" | awk '{print $4}')
         echo "  Device : $blk_dev (size: $lv_size)"
         echo "  VG Name: $lv_gname (size: )"
         while read line; do
             echo "   $line"
         done < <(pvs | awk -v vg=$lv_gname '{if($2 == vg) printf "%s %s %s\n",$1,$5,$6}')
    fi
done
}

#_lvm_info
for part_dir in $(cat /etc/fstab  | grep -v -e "^#" -e " swap "| grep -e "^/dev" -e "^UUID=" | awk '{print $2}'); do
    blk_dev=$(df -h $part_dir | grep -v "^Filesystem" | awk '{print $1}')
    if [ "$(lsblk $blk_dev | awk '{if($6=="lvm") printf "lvm"}')" == "lvm" ]; then
         lv_info=$(lvs $blk_dev | grep -v "LV   VG")
         lv_gname=$(echo "$lv_info" | awk '{print $2}')
         lv_name=$(echo "$lv_info" | awk '{print $1}')
         lv_size=$(echo "$lv_info" | awk '{print $4}')
         echo "  Device : $blk_dev (size: $lv_size)"
         echo "  VG Name: $lv_gname (size: )"
         while read line; do
             echo "   $line"
         done < <(pvs | awk -v vg=$lv_gname '{if($2 == vg) printf "%s %s %s\n",$1,$5,$6}')
    fi
done

