#!/bin/bash

source /root/pghandler.sh

#Consistency data directive.
CONST_DIR="${SYS_DIR}_const"

function write_json_log {
	echo "{ \"state\":\"${1}\", \"error\":\"${2}\", \"comment\":\"${3}\" }" > "${4}"
}

function mergeConst {
	if [ ! -d "${SYS_DIR}/consistency" ]; then
		mkdir -p "${SYS_DIR}/consistency"
	fi

	if [ ! -d "${CONST_DIR}" ]; then
		echo "No consistency data to copy back." | tee -a /dev/tty0
	fi

	/bin/cp -a ${CONST_DIR}/* ${SYS_DIR}/consistency/
	if [ $? -ne 0 ]; then
		echo "ERROR: issue copying back consistency data." | tee -a /dev/tty0
	fi
}

function sleepForever {
	mergeConst
	while [ 1 ]; do
		sleep 1
	done
}


#Well; I wanted it duplicated so that everything was in one place (ish)
TARGET_SITE="${NAPXE}"
TARGET_PATH="/cburn_staging"

M2_PATH="${TARGET_PATH}/m2/latest"

RDIR="${SYS_DIR}"
M2LOGTARGET="${RDIR}/m2.log"
M2_LOG_CONSISTENCY="${CONST_DIR}/m2.log"


#Radian data
RAD_LOG_JSON="${CONST_DIR}/radian.json"
RAD_LOG="${CONST_DIR}/radian.log"
RAD_LOG_JSON="${CONST_DIR}/radian.json"
RAD_LOG_CONSISTENCY="${CONST_DIR}/radian_const.log"
RADIAN_PATH="http://${NAPXE}/cburn_staging/radian_deploy/latest"


echo "M.2 and Radian Firmware update is executing." | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG} ${M2LOGTARGET} ${M2_LOG_CONSISTENCY} /dev/tty0
date | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG} ${M2LOGTARGET} ${M2_LOG_CONSISTENCY} /dev/tty0

#OK, so we want to grab and source this bash library for additional function blocks
# wget "http://${TARGET_SITE}${M2_PATH}/issdcm.x86_64.rpm" -O /root/issdcm.x86_64.rpm &> /dev/null
# if [ $? -ne 0 ]; then
# 	if [ "${NAFW}" != "0" ]; then
# 		echo "ERROR: Unable to acquire issdcm" | tee -a /dev/tty0 $M2LOGTARGET
# 		exit -1
# 	fi
# 	echo "WARNING: Unable to acquire issdcm" | tee -a /dev/tty0 $M2LOGTARGET
# fi

# wget "http://${TARGET_SITE}${M2_PATH}/msecli" -O /root/msecli &> /dev/null
# if [ $? -ne 0 ]; then
# 	if [ "${NAFW}" != "0" ]; then
# 		echo "ERROR: Unable to acquire msecli" | tee -a /dev/tty0 $M2LOGTARGET
# 		exit -1
# 	fi

# 	echo "WARNING: Unable to acquire msecli" | tee -a /dev/tty0 $M2LOGTARGET
# fi

# chmod +x /root/msecli

#Grab updated m2 firmware post scripts
# wget "http://${TARGET_SITE}${M2_PATH}/intel_m2_fw.sh" -O /root/intel_m2_fw.sh &> /dev/null
# if [ $? -ne 0 ]; then
# 	echo "ERROR: Unable to acquire intel fw dir" | tee -a /dev/tty0 $M2LOGTARGET
# 	exit -1
# fi

# wget "http://${TARGET_SITE}${M2_PATH}/micron_m2_fw.sh" -O /root/micron_m2_fw.sh &> /dev/null
# if [ $? -ne 0 ]; then
# 	echo "ERROR: Unable to acquire micron fw dir" | tee -a /dev/tty0 $M2LOGTARGET
# 	exit -1
# fi

wget "http://${TARGET_SITE}${M2_PATH}/tools.sh" -O /root/tools.sh &> /dev/null
if [ $? -ne 0 ]; then
	echo "ERROR: Unable to acquire tools script" | tee -a /dev/tty0 $M2LOGTARGET
	exit -1
fi


source /root/tools.sh

# RETC=`get_all`
get_all
if [ $? -ne 0 ]; then
	echo "ERROR: Unable to call get_all from tools" | tee -a /dev/tty0 $M2LOGTARGET
	exit -2
fi

source /root/micron_m2_fw.sh
source /root/intel_m2_fw.sh

FWUPDATE=0

##
## Validation block for locking down M.2 firmware version information.
##
# Intel first
for m2 in `ls /dev/disk/by-id/*INTEL_SSDSCK* | grep -v 'part[0-9]$'`
 do
	echo "Validating Intel M.2 disk: ${m2}" | tee -a /dev/tty0 ${M2LOGTARGET} ${M2_LOG_CONSISTENCY}
#	udevadm info -p $(udevadm info -q path -n ${m2}) |grep '^E:' | cut -d' ' -f 2 | grep 'ID_REVISION' | cut -d'=' -f 2 | grep 'N2010112' &> /dev/null
	udevadm info -p $(udevadm info -q path -n ${m2}) |grep '^E:' | cut -d' ' -f 2 | grep 'ID_REVISION' | cut -d'=' -f 2 | grep "${INTEL_FIRMWARE_VER}" &> /dev/null
	if [ $? -ne 0 ]
	 then
		echo "Intel M.2 disk firmware does not match approved; time to begin flashing" | tee -a /dev/tty0 ${M2LOGTARGET} ${M2_LOG_CONSISTENCY}
		SERIALTARGET=`udevadm info -p $(udevadm info -q path -n ${m2}) |grep '^E:' | cut -d' ' -f 2 | grep 'ID_SERIAL_SHORT' | cut -d'=' -f 2`

		#We can probably just do this once... but whatever; what do I care?  Intel shouldn't be referencing disks by some random ordering anyways.
		for i in `seq 0 100`; do
			issdcm -drive_index "${i}" > "/tmp/issdcm_${i}"
			if [ "$?" -ne "0" ]; then
				break
			fi

			#OK, so now we gotta find our cupcake.
			grep "$SERIALTARGET" "/tmp/issdcm_${i}" &> /dev/null
			if [ $? -ne 0 ]; then
				continue
			fi

			#OK; so we've got the right drive.  Lets update the mf'er
			echo "FW Update on ${m2}"  | tee -a /dev/tty0 ${M2LOGTARGET} ${M2_LOG_CONSISTENCY}
			issdcm -drive_index "${i}" -firmware_update "/root/${INTEL_FIRMWARE}" -force | tee -a /dev/tty0 ${M2LOGTARGET} ${M2_LOG_CONSISTENCY}
			if [ $? -ne 0 ]; then
				echo "ERROR: Failure when attempting to handle firmware update on ${SERIALTARGET}" | tee -a /dev/tty0 ${M2LOGTARGET} ${M2_LOG_CONSISTENCY}
				sleepForever
				exit 1
			fi
			FWUPDATE=1
		done
	else
		echo "Intel M.2 ${m2} was the appropriate firmware." | tee -a /dev/tty0 ${M2LOGTARGET} ${M2_LOG_CONSISTENCY}
	fi
done

##
## then Micron.
##CONSISTENCY}
for m2 in `ls /dev/disk/by-id/*Micron_5100_MTF* | grep -v 'part[0-9]$'`
 do
	echo "Validating Micron M.2 disk: ${m2}" | tee -a /dev/tty0 ${M2LOGTARGET} ${M2_LOG_CONSISTENCY}
#	udevadm info -p $(udevadm info -q path -n ${m2}) |grep '^E:' | cut -d' ' -f 2 | grep 'ID_REVISION' | cut -d'=' -f 2 | grep 'D0MU027\|D0MU037' &> /dev/null
	udevadm info -p $(udevadm info -q path -n ${m2}) |grep '^E:' | cut -d' ' -f 2 | grep 'ID_REVISION' | cut -d'=' -f 2 | grep "${MICRON_FIRMWARE_VER}" &> /dev/null
	if [ $? -ne 0 ]
	 then
		echo "Micron M.2 disk firmware does not match approved; time to begin flashing" | tee -a /dev/tty0 ${M2LOGTARGET} ${M2_LOG_CONSISTENCY}
		DTARGET=`readlink -f "${m2}"`

		#This next cmd is dead quiet
		echo "FW Update on ${m2}"  | tee -a /dev/tty0 ${M2LOGTARGET} ${M2_LOG_CONSISTENCY}

		/root/msecli -F -U "${MICRON_FIRMWARE}" -n "${DTARGET}" -r  | tee -a /dev/tty0 ${M2LOGTARGET} ${M2_LOG_CONSISTENCY}
		if [ $? -ne 0 ]; then
			echo "ERROR: Failure when attempting to handle firmware update on ${m2}" | tee -a /dev/tty0 ${M2LOGTARGET} ${M2_LOG_CONSISTENCY}
			sleepForever
			exit 2
		fi
		FWUPDATE=1
	else
		echo "Micron M.2 ${m2} was the appropriate firmware." | tee -a /dev/tty0 ${M2LOGTARGET} ${M2_LOG_CONSISTENCY}
	fi
done


if [ -b /dev/nvme0n1 ]; then
	#Main.sh should handle /everything/ so that we don't have to.
	wget http://${NAPXE}/cburn_staging/radian_deploy/latest/main.sh -O /root/main.sh &> /dev/null
	if [ $? -ne 0 ]
	 then
	 	if [ "${NAFW}" != "0" ]; then
			echo "ERROR: Unable to acquire latest radian firmware handler tool" | tee -a /dev/tty0 ${M2LOGTARGET} ${M2_LOG_CONSISTENCY}
			sleepForever
			exit -1
		fi
		echo "WARNING: Could not find latest radian tools.  Flashing with currently bundled scripts." | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG}
		# /usr/local/sbin/netapp_preprov.sh
		# exit $?
	fi

	chmod +x /root/main.sh
	if [ $? -ne 0 ]
	 then
		echo "ERROR: Could not set main.sh to +x" | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG}
		echo "" | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG}
		echo "TEST FAILED" | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG}
		sleepForever
		exit 1
	fi

	#It seems odd to tee something already running in-utero.
	/root/main.sh | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG}
	if [ $? -ne 0 ]
	 then
		echo "ERROR: Bad status reported from main.sh" | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG}
		sleepForever
		exit 2
	fi

	FWVER=`/usr/sbin/dump_log_page_cur -d 0 | grep 'Firmware Ver'`
	#We need actual read implimentation of fwver reading to populate this grep req.
	echo $FWVER | grep ae34b8cc > /dev/null
	if [ $? -ne 0 ]
	 then
	 	echo "###############################################################################" | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG} /dev/tty0
		echo "#                      Radian Firmware Flash Begin                            #" | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG} /dev/tty0
		echo "###############################################################################" | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG} /dev/tty0
		#We need to perform the update process and reboot the node.
		echo "Firmware needs to be updated..." | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG} /dev/tty0
		/usr/sbin/firmware_update /dev/nvme0 -u /root/firmware_ae34b8cc.tar &> /tmp/fwupdate.log
		cat /tmp/fwupdate.log | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG} /dev/tty0
		echo ""
		grep 'status = 0' /tmp/fwupdate.log > /dev/null
		if [ $? -ne 0 ]
		 then
			echo "ERROR: Bad status on firmware upgrade" | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG} /dev/tty0
			echo "TEST FAILED"
			write_json_log "FAIL" "11" "Bad status on firmware upgrade" $TESTRESULTS_JSON
			sleepForever
			exit 11
		fi

		echo "Rebooting system..." | tee -a ${RAD_LOG_CONSISTENCY} ${RAD_LOG} /dev/tty0
		write_json_log "NONE" "NA" "Rebooting node after firmware update" $TESTRESULTS_JSON
		sleep 3
		# reboot
		FWUPDATE=1
		# exit 1
	fi
fi


if [ "${FWUPDATE}" -eq "1" ]; then
	#Lets just do the super generic complete shutdown for sync.
	echo 0 > /sys/class/rtc/rtc0/wakealarm
	echo `date '+%s' -d '+ 3 minutes'` > /sys/class/rtc/rtc0/wakealarm
	shutdown -h now
fi

/usr/bin/ipmitool raw 0x30 0x0e

mergeConst

exit 0