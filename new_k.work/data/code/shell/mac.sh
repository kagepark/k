h2d(){  
  mac=$(echo $1 |  tr '[:lower:]' '[:upper:]')  
  echo "ibase=16; ${mac}"|bc  
}  

d2h(){  
  mac=$(echo $1 |  tr '[:lower:]' '[:upper:]')  
  echo "obase=16; ${mac}"|bc  
}  
  
dhall(){  
    for ii in $*  
    do  
        printf "%012X " $ii  
    done  
    echo  
}  
  
hdall(){  
    for ii in $*  
    do  
        h='0x'$(echo $ii | tr '[:lower:]' '[:upper:]')  
        printf "%d "  $h  
    done  
    echo  
}  

##dhall $*
#printf "%012X " $*

h='0x'$(echo $* | tr '[:lower:]' '[:upper:]')
printf "%d "  $h
