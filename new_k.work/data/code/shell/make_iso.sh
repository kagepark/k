#anaconda 10.x
# 20060709  cep
# version 2


export BASE=/usr/src
export VERSION=1.0
export PRODUCT_NAME=booyo
export ARCH=i386
export DIST_DIR=${BASE}/${VERSION}
export BUILD_DIR=${BASE}/${PRODUCT_NAME}
export ISO_DIR=$BASE/iso
export ISO_FILE=$ISO_DIR/${PRODUCT_NAME}-${ARCH}.iso
export MAKE_INFO="CEP co."
export COMPNAME=${DIST_DIR}/${ARCH}/${PRODUCT_NAME}/base/comps.xml
export PKGORDER_FILE=${DIST_DIR}/${ARCH}/${PRODUCT_NAME}/base/pkgorder
export DISC_NUM=1
export SDISC_NUM=0
export PRODUCTSTR="test"
export RELEASE="test-i386"
export NOW=`pwd`

export LANG="C"
export PYTHONPATH=/usr/lib/anaconda 
export PATH=$PATH:/usr/lib/anaconda-runtime



#change BASE_DIR
#echo "copy RPM file to destination directory"
#cd ${BUILD_DIR}/RPMS/${ARCH}
#rm -fr ${DIST_DIR}/${ARCH}/${PRODUCT_NAME}/RPMS
#mkdir -p ${DIST_DIR}/${ARCH}/${PRODUCT_NAME}/RPMS
#mkdir -p ${DIST_DIR}/${ARCH}/SRPMS
#mkdir -p ${DIST_DIR}/${ARCH}/${PRODUCT_NAME}/base
#tar cf - *.rpm | tar xf - -C ${DIST_DIR}/${ARCH}/${PRODUCT_NAME}/RPMS

#make a new comps.xml
#$NOW/getfullcomps.py comps.xml $BASE $ARCH

#create repodata
#echo "Creates a common metadata repository"
#createrepo -g ${COMPNAME} ${DIST_DIR}/${ARCH}

#add .discinfo file
#echo "Create Disk Infomation File"
#echo "1104951135.38 
#final 
#$ARCH
#$DISC_NUM 
#$PRODUCT_PATH/base 
#$PRODUCT_PATH/RPMS 
#$PRODUCT_PATH/pixmaps" > ${DIST_DIR}/${ARCH}/.discinfo


#check sample comps.xml

#delete previous TRAN.TBL file 
#echo "Delete prefious TRAN.TBL file"
#find ${DIST_DIR}/${ARCH} -name TRANS.TBL -exec rm -f {} \;

#hdlist
#echo "create hdlist file"
#rm -f ${DIST_DIR}/${ARCH}/$PRODUCT_NAME/base/hdlist*
#genhdlist --withnumbers --productpath=${PRODUCT_NAME} ${DIST_DIR}/${ARCH}


#pkgorder
#echo "create package file"
#rm -f $PKGORDER_FILE
#pkgorder ${DIST_DIR}/${ARCH} $ARCH --product ${PRODUCT_NAME} > $PKGORDER_FILE
#or
#ls -l $SOURCE_ROOT_DIR/PRODUCT_PATH/RPMS > $PKGORDER_FILE

#renewal for pkgorder
#echo "Renewal for pkgorder"
#genhdlist --withnumbers --productpath=${PRODUCT_NAME} --fileorder=$PKGORDER_FILE ${DIST_DIR}/${ARCH}

#make cd-boot-base-iso
#echo "Make a install base image"
#echo "buildinstall --comp $COMPNAME --pkgorder $PKGORDER_FILE --version "$VERSION" --product "$PRODUCTSTR" --release "$RELEASE" --prodpath ${PRODUCT_NAME} --discs ${DISC_NUM}  ${DIST_DIR}/${ARCH}"
#sleep 5
#buildinstall --comp $COMPNAME --pkgorder $PKGORDER_FILE --version "$VERSION" --product "$PRODUCTSTR" --release "$RELEASE" --prodpath ${PRODUCT_NAME} --discs ${DISC_NUM}  ${DIST_DIR}/${ARCH}


#iso file size split
echo "split for ISO File Size"
rm -fr ${DIST_DIR}/${ARCH}-disc[1-9]
splittree.py --arch=${ARCH} \
 --total-discs=${DISC_NUM} \
 --bin-discs=${DISC_NUM} \
 --src-discs=${SDISC_NUM} \
 --release-string="${RELEASE}" \
 --pkgorderfile=${PKGORDER_FILE} \
 --distdir=${DIST_DIR}/${ARCH} \
 --srcdir=${DIST_DIR}/${ARCH}/SRPMS \
 --productpath=${PRODUCT_NAME}
#Error ???
exit

#renewal hdlist for iso file size split
# rm -f $BASE/i386-[1-9]/$PRODUCT_PATH/base/hdlist*
#genhdlist --withnumbers --productpath=$PRODUCT_NAME --fileorder=$PKGORDER_FILE ${DIST_DIR}/${ARCH}-disc[1-9]


#make iso
echo "create a product iso file"
#if [ ! -d ${ISO_DIR} ]; then
#	mkdir -p ${ISO_DIR}
#elif
	rm -fr ${ISO_DIR}
	mkdir -p ${ISO_DIR}
#fi

cd ${DIST_DIR}

echo "create install iso CD"
LOP=1
rm -f /tmp/i-cd-tmp
while [ $LOP -le $DISC_NUM ]
do
  echo $LOP  >> /tmp/i-cd-tmp
  LOP=`expr $LOP + 1`
done

for n in `cat /tmp/i-cd-tmp`
do
 if [ $n -eq 1 ]; then
   export BOOTCD="-b isolinux/isolinux.bin  -c isolinux/boot.cat  -no-emul-boot -boot-load-size 4 -boot-info-table "
 elif
   export BOOTCD=""
 fi

mkisofs \
 $BOOTCD \
 -input-charset=UTF-8 -output-charset=UTF-8 \
 -R -J -T -v \
 -V "$VERSION" \
 -A "$MAKE_INFO" \
 -x "lost+found" \
 -o $ISO_DIR/${PRODUCT_NAME}-${VERSION}-${ARCH}-disc${n}.iso \
 ${DIST_DIR}/${ARCH}-disc${n}
done

#SOURCE RPM ISO
echo "create Source iso CD"
LOP=1
  rm -f /tmp/s-cd-tmp
while [ $LOP -lt $SDISC_NUM ]
do
  echo $LOP  >> /tmp/s-cd-tmp
  LOP=`expr $LOP + 1`
done

for n in `cat /tmp/s-cd-tmp`
do
mkisofs \
 -input-charset=UTF-8 -output-charset=UTF-8 \
 -R -J -T -v \
 -V "$VERSION" \
 -A "$MAKE_INFO" \
 -x "lost+found" \
 -o $ISO_DIR/${PRODUCT_NAME}-${VERSION}-${ARCH}-SRPM-disc${n}.iso \
 ${DIST_DIR}/${ARCH}-SRPM-disc${n}
done
  

#Next iso cd
#mkisofs -R -J -V "$VERSION" -T -A "$MAKE_INFO" -o $ISO_FILE_2 $SOURCE_ROOT_DIR_N2
