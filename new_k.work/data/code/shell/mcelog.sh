#!/bin/bash

hex2bin() {
    if ((${#1} == 0)); then
       echo "$(basename $0) <hex> [<bit#> or <start bit#>:<end bit#>] "
       echo "<bit#> range : 0~63"
       exit 1
    fi
    hex=$(echo $1 | tr '[:lower:]' '[:upper:]')
    dec=$(echo "ibase=16;$hex" | bc)
    bin=$(echo "obase=2;$dec" | bc)
    if (( ${#bin} < 64 )); then
        for ((ii=0;ii<$((64 - ${#bin} )); ii++)) do
	        bin="0"$bin
        done
    fi
    echo $bin
}

displaybin() {
    bin=$1
    aa=0
    printf "%s"  "${bin:0:1}"
    for ((ii=1; ii<64; ii++)); do
       if (($ii%4 == 0)); then
          aa=$(($aa+1))
          if (( $aa != 0 && $aa % 4 == 0)); then
             echo 
          else
             printf " "
          fi
       fi
       printf "%s" "${bin:$ii:1}"
    done
}

getrange() {
    bin=$1
    range=$2
    if (( ${#range} == 0 )); then
       displaybin "$1"
    else
       range_arr=($(echo $range | sed "s/:/ /g"))
       bitstr=$(echo $bin | sed "s/ //g")
       bitstrn=${#bitstr}
       if ((${#range_arr[*]} == 1 )); then
          echo ${bitstr:$(($bitstrn-$range)):1}
       else
          startp=$(($bitstrn-${range_arr[0]}))
          endp=$(($bitstrn-${range_arr[1]}))
          if (($startp >= $endp )); then
              echo "$(basename $0) <Hex String> [<bit#> or <start bit#>:<end bit#>]"
              exit 1
          fi
          echo ${bitstr:$startp:$(($endp-$startp+1))}
       fi
    fi
}

bin2dec() {
    bit=$1
    echo "ibase=2;$bit" | bc
}

#1. get CPU #C BANK #B log
#2. get #B and make a "IA32_MC<#B>_STATUS" and find the string in Skylake_Server_v2_546832_Skylake_vol2_Register_partA_54682_rev0p8.pdf
#3. get STATUS #S and convert #S to bin
if [ "$1" == "bin" ]; then
    bin2dec $2
else
    bin=$(hex2bin $1)
    getrange $bin $2
fi
#4. debugging bin according to PDF file.
# - check : OVERFLOW (62)
# - check : Uncorected Error (61)
# - check : CORR_ERR_CNT(52:38)
#   bin2dec <bin of 52:38> : How many errors
#5. M2M 1 then IMC 1
#6. Looking [15:0] for channel and something
#7. between [3:0], that should be channel number.
#    010 => decimal : 2 , So, the channel 2 will be issued.
# some of case, it display at IA32_MC#_STATUS : IMC and Channel.
exit
while read line; do
   echo "$line" | grep " mcelog:" >& /dev/null || continue
   echo "$line" | grep CPU | grep BANK  
done < $*
