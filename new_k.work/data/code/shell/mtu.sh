#ifcfg=/etc/sysconfig/network-scripts/ifcfg-eth0
ifcfg=eth0
MTU_SIZE=1500
if [ -f $ifcfg ]; then
    if grep "^MTU=" $ifcfg >/dev/null ; then
        grep -v "^MTU=" $ifcfg > ${ifcfg}~
        cp -a ${ifcfg}~ $ifcfg
    fi
    echo "MTU=$MTU_SIZE" >> $ifcfg
else
    echo "$ifcfg file not found"
fi



