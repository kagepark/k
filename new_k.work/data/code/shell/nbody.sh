error_exit() {
    echo $*
    exit
}

nbody=$1
precision=$2
hostmem=$3
[ -n "$nbody" ] || error_exit "$(basename $0) <nbody> [ <precision:1> <hostmem:1> ]"
[ $nbody -lt 511 ] && error_exit make a over 512
[ -d /proc/driver/nvidia ] || error_exit "Not ready NVIDIA device"

numdevices=$(nvidia-smi -L |wc -l)
devices=$(chk=0; for i in $(nvidia-smi -L | awk '{print $2}' | sed "s/://g"); do [ "$chk" == "1" ] && echo -n ",";  echo -n $i; chk=1; done) 

[ -n "$precision" ] && precision="-fp64"
[ -n "$hostmem" ] && hostmem="-hostmem"

[ -d /opt/ace/tools/nvidia/log/ ] || mkdir -p /opt/ace/tools/nvidia/log/
cd /opt/ace/tools/nvidia/CUDA_SDK/C/bin/linux/release/

echo "nbody: $nbody  devices: $devices  precision: $precision  hostmem : $hostmem" > /opt/ace/tools/nvidia/log/$(hostname).log
echo $(date) >> /opt/ace/tools/nvidia/log/$(hostname).log
./nbody -benchmark -n=$nbody $precision -numdevices=$numdevices -device=$devices $hostmem >> /opt/ace/tools/nvidia/log/$(hostname).log 2>&1
echo $(date) >> /opt/ace/tools/nvidia/log/$(hostname).log
