#!/bin/bash

source /root/stage2.conf

function UErr {
	echo "An uncorrectable error occurred.  CBURN Will not continue any additional work." | tee /dev/tty0
	exit 1
}


wget http://172.31.1.185/ipmicfg
chmod +x ipmicfg
/bin/cp ipmicfg /usr/local/bin/ipmicfg


CMDLINE=`dmesg | grep '] Command line: '`
IFS=' ' read -r -a CMDVARS <<< $CMDLINE

CHASSIS_SER=""
NODE_SER=""

for ELE in "${CMDVARS[@]}"
 do
	echo $ELE | grep "=" > /dev/null
	if [ "$?" -eq 0 ]
	 then
		echo $ELE | grep 'NA_CHASSIS_SER' > /dev/null
		if [ "$?" -eq 0 ]
		 then
			CHASSIS_SER=`echo $ELE | cut -d'=' -f 2`
			continue
		fi

		echo $ELE | grep 'NA_NODE_SER' > /dev/null
		if [ "$?" -eq 0 ]
		 then
			NODE_SER=`echo $ELE | cut -d'=' -f 2`
			continue
		fi
	fi
done

#Ensure that serials are populated.  Die if they are not.
if [[ -z "${NODE_SER// }" ]]
 then
	echo "Empty Node serial.  Will not continue."
	UErr
fi

if [[ -z "${CHASSIS_SER// }" ]]
 then
	echo "Empty Node serial.  Will not continue."
	UErr
fi


echo "Chassis Serial: ${CHASSIS_SER}" | tee /dev/tty0
echo "Node Serial: ${NODE_SER}" | tee /dev/tty0

if [ -d "${SYS_DIR}/inventory" ]
 then
 	CDATE=$(date +%s)
 	#We don't want to  overwrite everything
 	mv "${SYS_DIR}/inventory" "${SYS_DIR}/inventory${CDATE}"
 	if [ $? -ne 0 ]
 	 then
 		echo "Error moving original inventory directory out of the way.  There may be issues if you continue (Lost inventory data)."
 		read -p "Press enter to continue."
 	fi
fi

#Establist original inventory
mkdir -p "${SYS_DIR}/inventory"
ipmicfg -fru list > "${SYS_DIR}/inventory/ipmicfg_fru_list.orig"
if [ $? -ne 0 ]
 then
 	echo "Error when dumping the initial FRU List, bailing."
 	UErr

fi

ipmicfg -fru tbackup "${SYS_DIR}/inventory/ipmicfg_fru_tbackup.orig"
if [ $? -ne 0 ]
 then
 	echo "Error when dumping the initial FRU tbackup, bailing."
 	UErr

fi

ipmicfg -fru backup "${SYS_DIR}/inventory/ipmicfg_fru_backup.orig"
if [ $? -ne 0 ]
 then
 	echo "Error when dumping the initial FRU backup, bailing."
 	UErr

fi

ipmicfg -tp info 1 > "${SYS_DIR}/inventory/ipmicfg_tp_info_1.orig"
if [ $? -ne 0 ]
 then
 	echo "Error when dumping the initial TP type 1 data, bailing."
 	UErr
fi

ipmicfg -tp info 2 > "${SYS_DIR}/inventory/ipmicfg_tp_info_2.orig"
if [ $? -ne 0 ]
 then
 	echo "Error when dumping the initial TP type 2 data, bailing."
 	UErr
fi

ipmicfg -tp info 3 > "${SYS_DIR}/inventory/ipmicfg_tp_info_3.orig"
if [ $? -ne 0 ]
 then
 	echo "Error when dumping the initial TP type 3 data, bailing."
 	UErr
fi

dmidecode > "${SYS_DIR}/inventory/dmidecode.orig"
if [ $? -ne 0 ]
 then
 	echo "Error when dumping the initial dmi data, bailing."
 	UErr
fi

dmidecode --dump-bin "${SYS_DIR}/inventory/dmidecode.bin.orig"
if [ $? -ne 0 ]
 then
 	echo "Error when dumping the initial dmi data (Binary), bailing."
 	UErr
fi

#Start updating at the top, the MCU information:
ipmicfg -tp chassissn "${CHASSIS_SER}"
if [ $? -ne 0 ]
 then
 	echo "Error when writing chassis serial to the MCU, bailing."
 	UErr
fi

ipmicfg -tp nodesn "${NODE_SER}"
if [ $? -ne 0 ]
 then
 	echo "Error when writing node serial to the MCU, bailing."
 	UErr
fi

#Move to DMI Data adjustment phase
cd /root/ami-util/
#insmod amifldrv_mod.o

./adelnx /SS "${NODE_SER}"
if [ $? -ne 0 ]
 then
 	echo "Error when writing system serial to DMI, bailing."
 	UErr
fi

./adelnx /BS "${NODE_SER}"
if [ $? -ne 0 ]
 then
 	echo "Error when writing board serial to DMI, bailing."
 	UErr
fi

CUR_NODE_ID=`ipmicfg -tp info | grep 'Current Node ID' | cut -d':' -f 2 | awk '{ print $1 }' | tr '\n'`
#Assign the chassis serial number based on current node ID.
./adelnx /CS "${CHASSIS_SER}(${CUR_NODE_ID})"
if [ $? -ne 0 ]
 then
 	echo "Error when writing modded chassis serial to DMI (With NodeID), bailing."
 	UErr
fi

cd /root

ipmicfg -fru 1s
if [ $? -ne 0 ]
 then
 	echo "Error when writing system serial to FRU, bailing."
 	UErr
fi

ipmicfg -fru 2s
if [ $? -ne 0 ]
 then
 	echo "Error when writing board serial to FRU, bailing."
 	UErr
fi

ipmicfg -fru 3s
if [ $? -ne 0 ]
 then
 	echo "Error when writing modded chassis serial to FRU (With NodeID), bailing."
 	UErr
fi

#Write everything back
ipmicfg -fru list > "${SYS_DIR}/inventory/ipmicfg_fru_list"
if [ $? -ne 0 ]
 then
 	echo "Error when writing end inventory fru list, bailing."
 	UErr
fi

ipmicfg -fru tbackup "${SYS_DIR}/inventory/ipmicfg_fru_tbackup"
if [ $? -ne 0 ]
 then
 	echo "Error when writing end inventory fru tbackup, bailing."
 	UErr
fi

ipmicfg -fru backup "${SYS_DIR}/inventory/ipmicfg_fru_backup"
if [ $? -ne 0 ]
 then
 	echo "Error when writing end inventory fru backup, bailing."
 	UErr
fi

ipmicfg -tp info 1 > "${SYS_DIR}/inventory/ipmicfg_tp_info_1"
if [ $? -ne 0 ]
 then
 	echo "Error when writing end inventory tp type 1 info, bailing."
 	UErr
fi

ipmicfg -tp info 2 > "${SYS_DIR}/inventory/ipmicfg_tp_info_2"
if [ $? -ne 0 ]
 then
 	echo "Error when writing end inventory tp type 1 info, bailing."
 	UErr
fi

ipmicfg -tp info 3 > "${SYS_DIR}/inventory/ipmicfg_tp_info_3"
if [ $? -ne 0 ]
 then
 	echo "Error when writing end inventory tp type 1 info, bailing."
 	UErr
fi

dmidecode > "${SYS_DIR}/inventory/dmidecode"
if [ $? -ne 0 ]
 then
 	echo "Error when writing end inventory DMI, bailing."
 	UErr
fi

dmidecode --dump-bin "${SYS_DIR}/inventory/dmidecode.bin"
if [ $? -ne 0 ]
 then
 	echo "Error when writing end inventory DMI Binary, bailing."
 	UErr
fi

return 0
