#!/bin/bash

#
# This script dumps smart data
#
cat /root/stage2.conf | grep "SYS_DIR" > /root/nvme_smart.sh
source /root/nvme_smart.sh

REBOOTCOUNT="-1"

if [ -f "${SYS_DIR}/onoff.log" ]; then
	REBOOTCOUNT=`tail -n 1 "${SYS_DIR}/onoff.log" | cut -d'>' -f 2 | sed 's/^ *//;s/ *$//'`
fi

RDIR="${SYS_DIR}"

CBFILETARGET="${RDIR}/nvme_smart_counter"

if [ "${REBOOTCOUNT}" != "-1" ]; then
	CBFILETARGET="${RDIR}/nvme_smart_counter_reboot_${REBOOTCOUNT}"
fi

mkdir "${CBFILETARGET}"
if [ $? -ne 0 ]; then
	echo "WARNING: Could not create directory" | tee -a /dev/tty0
fi

for NVMED in `ls /dev/nvme* | grep -v 'n1$' | grep -v 'p[0-9]'`; do
	NVMNAME=`echo ${NVMED} | awk -F/ '{print $NF}'`
	NVMP="${CBFILETARGET}/${NVMNAME}.log"
	echo "Dumping $NVMED smart-log:" >> ${NVMP}
	nvme smart-log "${NVMED}" >> ${NVMP}
	echo "Dumping $NVMED intel smart-log-add:" >> ${NVMP}
	nvme intel smart-log-add "${NVMED}" >> ${NVMP}
	echo "Dumping $NVMED error-log:" >> ${NVMP}
	nvme error-log "${NVMED}" >> ${NVMP}
done

#return 0
