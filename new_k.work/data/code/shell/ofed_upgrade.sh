#!/bin/sh

error_exit() {
    echo $*
    exit
}

if [ -d /acefs/global/commands ]; then
    OFED=$1
    server_name=$2
    manual=$3
    [ -n "$server_name" ] || error_exit "$(basename $0) <OFED file> <server name> [<manual>]"
    [ -f $OFED ] || error_exit "$OFED file not found"

    GLOBAL_TEMP=$(mktemp -d /global/OFED-upgrade-XXXXXXXXXXXX)
    tar zxvf $OFED -C $GLOBAL_TEMP
    [ -d /acefs/servers/$server_name ] || error_exit "$server_name not found"
    host_name=$(cat /acefs/servers/$server_name/host/name)
    ssh $host_name /.ace/ha_cluster/ace/sbin/ofed_upgrade.sh $GLOBAL_TEMP $manual
    exit
else
    GLOBAL_TEMP=$1 
    manual=$2
fi
 
[ "$(cat /.ace/acefs/self/cluster/revisions/$(cat /.ace/acefs/self/host/revision)/checkout)" == "1" ] || error_exit "This node it not checked out"

ace_tmp_dir=/.ace/tmpfs
for ace in /etc/passwd /etc/shadow /etc/group /etc/ssh ; do
         mkdir -p $ace_tmp_dir/$(dirname $ace)
         \cp -a $ace $ace_tmp_dir/$ace
         umount $ace
         mount -o bind $ace_tmp_dir/$ace $ace
done

for ace in $(ls /.ace/acefs/global/mount/root/.ssh/); do
         umount /root/.ssh/$ace
         \cp -af /.ace/acefs/global/mount/root/.ssh/$ace /root/.ssh
done
/etc/init.d/sshd restart

kill -9 $(ps -ef |awk '{if($8=="/.ace/ace_server") print $2}')

/etc/init.d/openibd stop

if [ "$manual" != "1" ]; then
    echo y | ofed_uninstall.sh

    cd $GLOBAL_TEMP/*

    ./install.pl -c ofed.conf
fi
