#!/bin/bash

MOUNTPATH="/test/"

#Unmount everything just in case.
for diskp in `ls /dev/sd* | grep '[0-9]$'`
 do
	SKIP=0
	for var in "$@"
	 do
		if [ "$diskp" == "$var" ]
		 then
			SKIP=1
		fi
	done

	if [ $SKIP -eq 1 ]
	 then
		echo "Skipping ${diskp}"
		continue
	fi

	umount $diskp
done

#Destroy all previous partitions and mount
for disk in `ls /dev/sd* | grep -v '[0-9]$'`
 do
	SKIP=0
	for var in "$@"
	 do
		if [ "$diskp" == "$var" ]
		 then
			SKIP=1
		fi
	done

	if [ $SKIP -eq 1 ]
	 then
		echo "Skipping ${diskp}"
		continue
	fi

	parted -s $disk mklabel gpt
	#Creates 50G partition
	parted -s $disk mkpart "" ext4 0% 50G
	mkfs.ext4 ${disk}1
	TARGETMNT=`echo $disk | awk -F "/" '{ print $NF}'`
	echo "Creating ${MOUNTPATH}${TARGETMNT}"
	mkdir "${MOUNTPATH}${TARGETMNT}"
	mount ${disk}1 "${MOUNTPATH}${TARGETMNT}"
done

