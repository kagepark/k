file=database.cfg
argv=/global/server-0002/ports/2/rate

a_argv=( $(echo $argv| sed 's/\//\ /g') )
deep=$(( ${#a_argv[*]} - 1 ))
argc=$(( ${#a_argv[*]} - 1 ))
[ $deep -ge 3 ] && deep=3


_skip_blank(){
   local in
   in="$*"
   echo $(echo $in | sed 's/\ //g')
}

_naked() {
  local inp
  inp="$*"

  if echo $inp | grep \' >/dev/null ; then
    echo $inp | awk -F\' '{print $2}' && return 0
  elif echo $inp | grep \" >/dev/null ; then
    echo $inp | awk -F\" '{print $2}' && return 0
  elif echo $inp | grep \= > /dev/null ; then
#    echo $( echo $inp | awk -F\{ '{print $2}' | awk -F\} '{print $1}')
    for fn in $( echo $inp | awk -F\{ '{print $2}' | awk -F\} '{print $1}'); do
#       echo $fn ${a_argv[$argc]}
#       echo $fn | awk -F\= -v fv=${a_argv[$argc]} '{ if ( $1 == fv ) print $2}'
       _f_v=( $(echo $fn | sed 's/\=/ /g') )
       [ "${_f_v[0]}" == "${a_argv[$argc]}" ] && echo ${_f_v[1]} && return 0
    done
  fi
  return 1
}


_open=0
cat $file | grep -v "^#" | while read line; do
#   echo "= $deep | $_open | ${a_argv[$_open]} | $( echo $line | awk '{print $1}') | $line"
#   if echo $line| grep -w "^${a_argv[$_open]} " | grep " {$" >/dev/null ; then
#   if echo $line| grep -w "^${a_argv[$_open]} " | grep " {" >/dev/null ; then
   if [ $_open -ne $deep -a "$(echo $line| grep -w "^${a_argv[$_open]} " | grep " {" >/dev/null  && echo 1 || echo 0)" == "1" ]; then
      _open=$(( $_open + 1)) 
   elif [ $_open -eq $deep -a "${a_argv[$_open]}" == "$( echo $line | awk '{print $1}')" ]; then
#      echo "> $line"
      _naked $line || echo $line |awk '{print $2}'
      break
   fi
done
