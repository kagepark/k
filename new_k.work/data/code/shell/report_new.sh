#!/bin/sh

_KG_HOST=hoa

function help() {
  echo "
  $0 <option> <val>
    -a                             : whole node of /etc/hosts
    -h <hostname> [ <hostname2> ..]: hostnames
    -n <start number> <end number> : from start number to end number
    -r <rack number>               : whole node of rack number

  ex)
  $0        : help
  $0 -n 2 5 : hoa0002 ~ hoa0005
  $0 -h hoa0001 hoa0002 : hoa0001 and hoa0002
  $0 -a     : whole node of /etc/hosts
  $0 -r 2   : whole node of rack number 2
  "
  exit
}

function _k_host() {
  if [ "$1" == "-n" ]; then
    if [ $# -ne 3 ]; then
       echo help
       exit
    fi
    _start=$2
    _end=$3
    for i in $(seq $2 $3); do
      echo ${_KG_HOST}$(printf "%04d" $i)
    done
  elif [ "$1" == "-r" ]; then
    _start=$(expr $(expr $(expr $2 - 1) \* 38) + 1)
    _end=$(expr $_start + 37)
    for i in $(seq $_start $_end); do
      echo ${_KG_HOST}$(printf "%04d" $i)
    done
  elif [ "$1" == "-a" ]; then
    _hosts=$(cat /etc/hosts |grep ${_KG_HOST} |grep -v "\-ipmi"| grep -v "\-cntl" | awk '{print $2}')
    echo $_hosts
  elif [ "$1" == "-h" ]; then
    _in_hosts=( $* )
    for i in $(seq 1 $#); do
      _hosts="$_hosts ${_in_hosts[$i]}"
    done
    [ "$#" -ne 1 ] && echo $_hosts || echo help

  else
       echo help
       exit
  fi
}

_hosts=$(_k_host $*)

[ "$_hosts" == "help" ] && help


for i in $_hosts; do
  echo "===================[ $i ]====================="
#  scp bin/report.sh $i: > /dev/null 2>&1
#  ssh $i /root/report.sh $(./find_node.sh $i)
  ssh $i '( 
echo "Date              : $(date)"

#APPRO S/N
APPRO_SN=( $(dmidecode -t chassis |grep "Serial Number:") )
echo "Appro Chassis S/N : $( [ "${APPRO_SN[2]}" == "0123456789" ] && echo "Need Appro Chassis S/N" || echo ${APPRO_SN[2]} )"
#echo "Position          : $( [ ! -n "$2" ] && echo "-" || echo $*  )"
#echo "M/B S/N           : $(dmidecode -s baseboard-serial-number)"

echo ""
echo "--------------- Node Info ------------------"
echo "Host Name         : $(hostname)"
#MAC
mac_ipmi=( $(ls /dev/ipmi* > /dev/null 2>&1 && (ipmitool lan print | grep "MAC Address") || echo "- - - - -" ) )
mac_eth0=( $(ifconfig eth0 | grep HWaddr ) )
mac_eth1=( $(ifconfig eth1 | grep HWaddr ) )
echo "eth0 MAC Address  : ${mac_eth0[4]}"
echo "eth1 MAC Address  : ${mac_eth1[4]}"
echo "BMC MAC Address   : ${mac_ipmi[3]}"
#OS
echo "OS                : $(cat /etc/issue | head -n 1 )"
echo "Kernel            : $(uname -r)"
#HDD
echo "HDD               : $(cat /proc/scsi/sg/device_strs| wc -l)ea"
for hd_n in $(ls /dev/sd[a-d]) ; do
  HDD_MODEL=( $(smartctl -a -d ata ${hd_n}| grep "Device Model") )
  HDD_SN=( $(smartctl -a -d ata ${hd_n} | grep "Serial Number:") )
  HDD_FM=( $(smartctl -a -d ata ${hd_n} | grep "Firmware Version:") )
  echo "  $hd_n        : Model(${HDD_MODEL[2]}) Serial(${HDD_SN[2]}) Firmware(${HDD_FM[2]})"
done
for hd_l in $(ls /sys/class/scsi_disk); do
  cd /sys/class/scsi_disk/${hd_l}/device/
  echo "  Location        : $hd_l ( $( ls -d block\:s* 2>/dev/null | sed -e 's/block://g') )"
done

#CPU
CPU_INFO=( $(cat /proc/cpuinfo | grep "model name" | tail -n 1) )
echo "CPU               : ${CPU_INFO[6]}@${CPU_INFO[8]} $(ls /sys/class/cpuid | wc -l) cores"
#MEM
echo "MEM               : $(printf "%3.1f" $( echo "$(cat /proc/meminfo | grep MemTotal | sed -e s/MemTotal:// -e s/kB//)/(1024*1024)" | bc -l) )G"
#BIOS
echo "BIOS Version      :$(dmidecode -s bios-version)"
#BMC
echo "BMC Version       : $( [ -f /sys/class/ipmi/ipmi0/device/bmc/ipmi_version ] && (cat /sys/class/ipmi/ipmi0/device/bmc/ipmi_version) || echo "-")"
echo "BMC Firmware      : $( [ -f /sys/class/ipmi/ipmi0/device/bmc/firmware_revision ] && (cat /sys/class/ipmi/ipmi0/device/bmc/firmware_revision) || echo "-")"
echo "BMC Users         : $(ls /dev/ipmi* >& /dev/null && echo $(ipmitool user list ADMINISTRATOR | while read line ; do echo $line | grep [3,4] >/dev/null && (A=( $line ); if [ "${A[0]}" == "3" ]; then if [ "${A[6]}" == "OPERATOR" ]; then  echo -n " OPERATOR "; else echo -n " oper not "; fi elif [ "${A[0]}" == "4" ]; then if [ "${A[6]}" == "ADMINISTRATOR" ]; then echo -n " ADMINISTRATOR "; else echo -n " USERID not "; fi fi) ; done  ) || echo -)"

echo ""
#NVIDIA
echo "--------------- NVIDIA Info ------------------"
V=( $([ -f /proc/driver/nvidia/version ] && (cat /proc/driver/nvidia/version | head -n 1) || echo "- - - - - - - -" ) )
echo "NVIDIA Version    : ${V[7]}"
 
[ -f /usr/bin/nvidia-smi ] && (
   CK=0
   nvidia-smi | while read line ; do
     TMP=( $line );
     if [ "${TMP[0]}" == "Product" -a "${TMP[1]}" == "Name" -a "$CK" == "0" ]; then
       printf "NVIDIA Product    : %s\n" ${TMP[5]};
       CK=1
     elif [ "${TMP[0]}" == "Product" -a "${TMP[1]}" == "ID" ]; then
       printf "NVIDIA Product ID : %s\n" ${TMP[3]};
     elif [ "${TMP[0]}" == "Serial" ]; then
       printf "NVIDIA S/N        : %s\n" ${TMP[3]};
     elif [ "${TMP[0]}" == "Firmware" ]; then
       printf "NVIDIA Firmware   : %s\n" ${TMP[3]};
     fi ;
   done
) || echo "NVIDIA Product, S/N, Firmware:  command not found"

echo ""
#IB
echo "--------------- IB Info ------------------"
echo "IB Type           : $([ -f /sys/class/infiniband/mthca0/hca_type ] && (cat /sys/class/infiniband/mthca0/hca_type) || echo "-" )"
echo "IB Ports          : $([ -d /sys/class/infiniband/mthca0/ports ] && (ls /sys/class/infiniband/mthca0/ports|wc -l) || echo "-" )"
echo "IB Firmware       : $([ -f /sys/class/infiniband/mthca0/fw_ver ] && (cat /sys/class/infiniband/mthca0/fw_ver) || echo "-" )"
echo "IB Rate           : $([ -f /sys/class/infiniband/mthca0/ports/1/rate ] && (cat /sys/class/infiniband/mthca0/ports/1/rate | grep 20 > /dev/null && echo 20 Gbps|| echo ?? Gbps) || echo "-" )"
  )' 
  echo "==================================================="
  echo ""
done | tee /tmp/$(hostname).report.$(date +%Y%m%d-%H%M).log

echo "$(date +%Y%m%d-%H%M) report" >> /tmp/$(hostname).log


