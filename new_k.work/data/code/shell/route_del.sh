netstat -nr | while read line; do
    if echo $line | grep "255.255.255.255" >/dev/null; then
        ip=$(echo $line | awk '{print $1}')
        gw=$(echo $line | awk '{print $2}')
        route del -host $ip/32 gw $gw
    fi
done
