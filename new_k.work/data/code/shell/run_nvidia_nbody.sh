_app_help="
-nbody <num> : nbody number (over 512) \n
\t -d       : double precision \n
\t -m       : using host memory
"

_K_LIB=/opt/ace/tools
. $_K_LIB/lib/libhost.so

_n=$(_k_opt_opt "$*" -nbody)
_k_opt_opt "$*" -d > /dev/null 2>&1 &&  _d=1
_k_opt_opt "$*" -m > /dev/null 2>&1 &&  _m=1
[ -n "$_n" ] || _n=512

for i in $_hosts; do
  echo "run nbody($_n) at $i"
  ssh -f $i /opt/ace/tools/bin/nbody.sh $_n $_d $_m
done

