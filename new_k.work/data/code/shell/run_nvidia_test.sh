
_app_help="
"
. $(dirname $0)/lib/libhost.so
 
printf "%17s   %10s %10s %10s\n" hostname H2D D2H D2D
for i in $_hosts; do
  printf "%17s : " $i
  ssh $i '( [ -d /proc/driver/nvidia ] && printf "%10s %10s %10s\n" $(echo y | numactl --physcpubind=10,11  /opt/ace/tools/nvidia/CUDA_SDK/C/bin/linux/release/bandwidthTest --memory=pinned | grep 33554432 | sed -e "s/33554432//g" ) || printf "%10s %10s %10s\n" - - - )'
done

