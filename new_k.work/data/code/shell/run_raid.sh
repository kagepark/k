#!/bin/sh
PATH=/usr/bin:/bin:/usr/sbin:/sbin:/global/tools:/global/tools/bin
help() {
echo "
  $(basename $0) <option>
  
    none : checking raid
    -c <0/1> : 0: create, 1: check
      default(skip) : check
      -f : force delete and create

    -d : delete
    -r <num> :  Raid Level number <0|1|5>
        default :5

   * support ARECA & 3WARE Card
"
exit
}

. $_K_LIB/klib.so
_k_load opt

_force=0
_k_opt_opt "$*" -f > /dev/null && _force=1

_raid=5
_raid=$(_k_opt_opt "$*" -r)
[ -n "$_raid" ] || _raid=5 

_check=1
_check=$(_k_opt_opt "$*" -c)
[ -n "$_check" ] || _check=1 # 1: check, 0 : create

_k_opt_opt "$*" -h > /dev/null && help

_del=0
_k_opt_opt "$*" -d > /dev/null && _del=1

raidcard=$(lspci | grep -i raid | grep Areca > /dev/null && echo areca)
[ -n "$raidcard" ] || raidcard=$(lspci | grep -i raid | grep 3ware > /dev/null && echo 3ware)

if [ "$raidcard" == "areca" ]; then
  if [ $_check -eq 1 ]; then
    cli64 disk info
    cli64 rsf info raid=$_raid
    cli64 vsf info vol=1
  elif [ $_check -eq 0 ]; then
    #checking 
    cli64 disk info
    if [ -n "$C_RAID" -a $_force -eq 0 ]; then
      echo "Exist raid"
      sleep 5
      echo "Stop : control + c"
    elif [ $_force -eq 1 ]; then
      #Delete raid6 with drive 9~24 (16 drives)
      cli64 set curctrl=1 password=0000
      cli64 rsf delete raid=$_raid
      sleep 2
    fi
    #Create raid6 with drive 9~24 (16 drives)
    cli64 set curctrl=1 password=0000
    cli64 rsf create drv=9~24 name=RaidSet1
    cli64 vsf create raid=$_raid level=6 stripe=64 fginit=N    
  elif [ $_del -eq  1 ]; then
    #Delete raid6 with drive 9~24 (16 drives)
    cli64 set curctrl=1 password=0000
    cli64 rsf delete raid=$_raid
  fi
elif [ "$raidcard" == "3ware" ]; then
  if [ $_check -eq 1 ]; then
    C_PORT=$(/opt/ace/sbin/tw_cli info |grep "^c[0-9]"| awk '{print $1}')
    /opt/ace/sbin/tw_cli /${C_PORT} show all 
  elif [ $_check -eq 0 ]; then
    C_PORT=$(/opt/ace/sbin/tw_cli info |grep "^c[0-9]"| awk '{print $1}')
    C_RAID=$(/opt/ace/sbin/tw_cli /${C_PORT} show all | grep RAID-5 | awk '{print $1}')
    if [ -n "$C_RAID" -a $_force -eq 0 ]; then
       echo "Exist raid $C_RAID"
       sleep 5
       echo "Stop : control + c"
    elif [ $_force -eq 1 ]; then
       echo "Delete exist raid"
       echo Y | /opt/ace/sbin/tw_cli /${C_PORT}/${C_RAID} del
       sleep 5
    fi
    echo "Create new raid"
    /opt/ace/sbin/tw_cli /${C_PORT} add type=raid$_raid disk=1-3 storsave=balance noqpolicy
  elif [ $_del -eq 1 ];then
       echo "Delete exist raid"
       echo Y | /opt/ace/sbin/tw_cli /${C_PORT}/${C_RAID} del
       sleep 2
  fi
else
  echo "doesn't support"
  exit
fi
