perm() {
file=$1
shift 1
iperm=( $* )
perms=""
chk=0
i=1
for ii in 0 1 2; do
  perm=$(ls -l $file | awk '{print $1}' | cut -c 2- | cut -c ${i}-$(( $i + 2 )) )
  kk=0
  for j in 1 2 3; do
     tt=0
     [ "$(echo $perm | cut -c $j )" == "r" ] && tt=4
     [ "$(echo $perm | cut -c $j )" == "w" ] && tt=2
     [ "$(echo $perm | cut -c $j )" == "x" ] && tt=1
     kk=$(( $kk + $tt ))
  done
  perms="$perms$kk"
  i=$(( $i + $j ))
  [ ${iperm[$ii]} -lt $kk ] && chk=1
done
if [ $chk -eq 0 ]; then
  echo $perms
else
  echo "File is not mode $(echo ${iperm[*]} | sed -e 's/\ //g')"
fi
}


file_owner() {
   uid=$(ls -l --time-style=long-iso $1 | awk '{ print $3}' | grep [0-9])
   gid=$(ls -l --time-style=long-iso $1 | awk '{ print $4}' | grep [0-9])
   if [ -n "$uid" -o -n "$gid" ]; then
     echo "$1 :"
     echo "Owner: Undocumented User ( $uid )"
     echo "Group: Undocumented User ( $gid )"
   fi
}


echo "** account"
pwck -r

echo "** umask"
um_files="/etc/profile /etc/login /etc/csh.cshrc /etc/csh.login /etc/bashrc /etc/bash.bashrc /etc/ksh.kshrc /etc/.login"
for um in $um_files; do
  if [ -f $um ]; then
     echo "    ${um}:"
     um_num=$(grep umask $um | grep -v "^#" | awk '{print $2}')
     if [ -n "$um_num" ]; then
       [ "$(echo $um_num | cut -c2)" == "3" ] || echo "FAIL: Umask for group must be 3"
       [ "$(echo $um_num | cut -c3)" == "7" ] || echo "FAIL: Umask for world must be 7"
     fi
  else
     echo "INFO: Does not exist."
  fi
done


echo "** password"
#400
pwd_files="/etc/shadow* /etc/gshadow*"
for pass in $pwd_files; do
  echo -n "$pass : "
  perm $pass "4 0 0"
done

#644
pwd_files="/etc/passwd* /etc/group*"
for pass in $pwd_files; do
  echo -n "$pass : "
  perm $pass "6 4 4"
done


#find_dir="/usr /etc"
find_dir="/etc"
for dir in $(find $find_dir  -type d); do 
  for file in $(find $dir -maxdepth 1 -type f); do
     file_owner $file
  done
done
