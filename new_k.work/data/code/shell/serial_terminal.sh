#!/bin/sh

port="ttyUSB0"

[ -n "$1" ] && port=$1

trap "{ /sbin/fuser -k /dev/$port; exit 0; }" EXIT

baud=115200
term=ansi

while true
do
   echo -n "`date`: Hosting $baud baud $term serial process on $port ... "
   /sbin/agetty -hL $baud $port $term
   /sbin/fuser -k /dev/$port
   echo -e "\n`date`: Closed serial session."
done
