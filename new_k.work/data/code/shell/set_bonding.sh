#!/bin/sh
# Kage 08/21/2014
# Setup bondding to host

error_exit() {
    echo "$*"
    exit 1
}

_k_bond() {
    bond_name=$1
    bond_eth=$2
    bond_ip=$3
    bond_netmask=$4
    bond_dev=$5

    bond_mod=$6
    bond_miimon=$7
    bond_ctl=$8
    [ -n "$bond_mod" ] || bond_mod=0
    [ -n "$bond_miimon" ] || bond_miimon=100
    [ -n "$bond_ctl" ] || bond_ctl=no

    [ -n "$bond_dev" ] || bond_dev=netdev-bond0

    if [ -n "$bond_dev" ]; then
        [ "$([ -f /etc/modprobe.d/bonding.conf ] && awk '{if($2=="$bond_dev") printf "exist"}' /etc/modprobe.d/bonding.conf)" == "exist" ] || echo "alias $ii bonding" >> /etc/modprobe.d/bonding.conf
    fi

    if [ ! -n "$bond_ip" ]; then
        for ii in $bond_eth; do
            bond_ip=$(awk -F= '{if($1=="IPADDR") print $2}' /etc/sysconfig/network-scripts/ifcfg-$ii)
            if [ -n "$bond_ip" ]; then
                bond_netmask=$(awk -F= '{if($1=="NETMASK") print $2}' /etc/sysconfig/network-scripts/ifcfg-$ii)
                break
            fi
        done
    fi
    [ -n "$bond_ip" ] || error_exit "bond_ip and bond_netmask not found from $bond_eth devices"

    [ -f /etc/sysconfig/network-scripts/ifcfg-$bond_name ] && sed "s/ONBOOT=yes/ONBOOT=no/g" < /etc/sysconfig/network-scripts/ifcfg-$bond_name > /etc/sysconfig/network-scripts/ifcfg-${bond_name}_save
    echo "DEVICE=$bond_name
IPADDR=$bond_ip
NETMASK=$bond_netmask
ONBOOT=yes
BOOTPROTO=none
USERCTL=no
NM_CONTROLLED=$bond_ctl
BONDING_OPTS=\"mode=$bond_mod miimon=$bond_miimon\"" > /etc/sysconfig/network-scripts/ifcfg-$bond_name

    for ii in $bond_eth; do
         sed "s/ONBOOT=yes/ONBOOT=no/g" < /etc/sysconfig/network-scripts/ifcfg-$ii > /etc/sysconfig/network-scripts/ifcfg-${ii}_save
         sed -i -e "/^NETMASK=/d"  -e "/^IPADDR=/d" -e "/^GATEWAY=/d" -e "s/ONBOOT=no/ONBOOT=yes/g" /etc/sysconfig/network-scripts/ifcfg-$ii
         echo "MASTER=$bond_name
SLAVE=yes
NM_CONTROLLED=$bond_ctl" >> /etc/sysconfig/network-scripts/ifcfg-$ii
    done
    echo "bonded $bond_dev to $bond_name with $bond_ip $bond_netmask"
}

bond_name=$1
bond_eth=$2
bond_ip=$3
bond_netmask=$4
bond_dev=$5
bond_mod=$6
bond_miimon=$7
bond_ctl=$8

(($# == 8 )) || error_exit "$(basename $0) <bond name> \"<eth1> <eth2>...\" \"[<bond_ip:if null then find ip from eth#>]\" \"[<bond netmask:if null then find netmask from eth#>]\" \"[<bond raw device:default netdev-bond0>]\" \"[<bond mode:default 1>]\" \"[<bond miimon:default 100>]\" \"[<bond ctl:default no>]\""

_k_bond "$bond_name" "$bond_eth" "$bond_ip" "$bond_netmask" "$bond_dev" "$bond_mod" "$bond_miimon" "$bond_ctl"
