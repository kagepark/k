#!/bin/sh
# kage 2012-07-20
# set console parameter to mgmt node or standalone server.
#
grub=/boot/grub/grub.conf
console_arg="$@"

if [ ! -n "$console_arg" ]; then
   echo "console_arg not found"
   echo "
$(basename $0) <console args>
   "
   exit
fi
if [ -d /.ace ]; then
   echo "This node is compute node"
   exit
fi


if [ ! -f $grub ]; then
   echo "grub.conf file not found"
   exit
fi


num=$(grep -n "$(uname -r)" $grub | grep kernel | awk -F: '{print $1}')

if [ ! -n "$num" ]; then
   echo "Current kernel not found in grub.conf"
   exit
fi

cp -a $grub $grub.org
sed "$num d" $grub > ${grub}~
sed "$num i\
\ $(grep -n "$(uname -r)" $grub | grep kernel | awk -F: '{print $2}') $console_arg" ${grub}~ > $grub


for tty in $console_arg; do
       echo $tty | grep "console=ttyS" >& /dev/null && break
done
tty_val=( $(echo $tty | awk -F= '{print $2}' | awk -F, '{printf "%s %s",$1,$2}' ) )
if ! grep "^${tty_val[0]}" /etc/securetty >/dev/null 2>&1 ; then
       echo "${tty_val[0]} " >> /etc/securetty
fi

if [ -d /etc/init ]; then
   if [ -f /etc/init/SOL.conf ]; then
      grep "^exec /sbin/agetty" /etc/init/SOL.conf | grep ${tty_val[0]} | grep ${tty_val[1]} >& /dev/null || (
      grep -v "^exec /sbin/agetty" /etc/init/SOL.conf > /tmp/sol.conf~
      echo "exec /sbin/agetty /dev/${tty_val[0]} ${tty_val[1]} vt100-nav " >> /tmp/sol.conf~
      cp -a /tmp/sol.conf~ /etc/init/SOL.conf )
   else
      echo "
start on stopped rc RUNLEVEL=[2345]
stop on starting runlevel [016]

respawn
exec /sbin/agetty /dev/${tty_val[0]} ${tty_val[1]} 115200 vt100-nav  
exec /sbin/agetty /dev/$(echo ${tty_val[0]}| awk -FS '{if($2%2==0) printf "ttyS1"; else printf "ttyS0"}') ${tty_val[1]} 115200 vt100-nav  
" > /etc/init/SOL.conf

   fi
elif [ -f /etc/inittab ]; then
   grep -v "^co:2345:respawn" /etc/inittab > /tmp/inittab~
   echo "co:2345:respawn:/sbin/agetty ${tty_val[0]} ${tty_val[1]}" >> /tmp/inittab~
   cp -a /tmp/inittab~ /etc/inittab
else
   echo "Can you check system?"
   exit
fi

