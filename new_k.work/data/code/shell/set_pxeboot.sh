#!/bin/sh
version=1.7.15-20120403
##################################################
# 1.0.x - 20100320 kage : initial
# 1.3.x - 20120313 kage : upgrade 
# 1.6.x - 20120319 kage : upgrade
# 1.6.x - 20120329 kage : upgrade
# 1.7.x - 20120403 kage : it working with ACE mgmt server
# infromation
# ------------------------------------------------
# $PXE_HOME/iso_images : ISO files
# $PXE_HOME/ks.cfg : ks files
# $PXE_HOME/bin : current directory
# $PXE_HOME/tftpboot/pxelinux.cfg : tftpboot configuration 
# $PXE_HOME/tftpboot/msgs : screen messages
# $PXE_HOME/tftpboot/pxelinux.0 : PXE boot image
##################################################

##################################################
#    make a default.conf file like as below.
check_os() {
  redhat=0
  if [ -f /etc/redhat-release ]; then
    redhat=1
    if cat /etc/redhat-release  |grep "^CentOS" > /dev/null 2>&1 ; then
      OS_NAME=centos
      OS_VERNAME=CentOS
      OS_RELEASE=$(cat /etc/redhat-release | awk '{print $3}')
    else
      OS_NAME=rhel
      OS_VERNAME="Red Hat "
      OS_RELEASE=$(cat /etc/redhat-release | awk '{print $7}')
    fi
  elif [ -f /etc/SuSE-release ]; then
    redhat=0
    OS_NAME=sles
    OS_VERNAME="SuSE Linux "
    OS_PATCH=$(cat /etc/SuSE-release | grep PATCHLEVEL | awk '{print $3}')
    [ -n "$OS_PATCH" ] && OS_RELEASE=$(cat /etc/SuSE-release | grep VERSION | awk '{print $3}').$OS_PATCH || OS_RELEASE=$(cat /etc/SuSE-release | grep VERSION | awk '{print $3}')
  else
    error_exit "This is not support OS"
  fi
}

run_exe() {
  echo "
# --dest : client node's dhcp request device name
# --dev  : pxe server's dhcp answer device name
# if you have a kickstart file then you should copy it to --ks_dir directory and use ex1
# default is working with ACE (ex3)

sh set_pxeboot.sh --mode unset
#ex1)
#sh set_pxeboot.sh --dest eth0 --mode start --dev eth1 --netmask 255.255.255.0 --pxe_home \$(pwd) --iso_exp_dir \$(pwd)  --ks_dir \$(pwd)/ks.cfg  
#ex2)
#sh set_pxeboot.sh --dest eth0 --mode start --dev eth1 --netmask 255.255.255.0 --pxe_home \$(pwd) --iso_exp_dir \$(pwd)  --ks_dir \$(pwd)/ks.cfg  --ks_cfg Default
#ex3)
sh set_pxeboot.sh --dest eth0 --mode start --dev eth1 --netmask 255.255.255.0 --pxe_home \$(pwd) --iso_exp_dir \$(pwd)  --ks_dir \$(pwd)/ks.cfg  --ks_cfg sync
  " > run_eth1_eth0.sh
  chmod +x run_eth1_eth0.sh
}

create_default() {
  if [ -d /acefs/global ]; then
    mkdir -p /images/pxe_boot_server
    cd /images/pxe_boot_server
    cp $0 /images/pxe_boot_server/$(basename $0)
    chmod +x /images/pxe_boot_server/$(basename $0)
    run_exe
    check_os
    iso_file=$(ls /images/$OS_NAME$OS_RELEASE/*.iso | head -n 1)
    echo "
OS_LIST[1]=\"mgmt1 $iso_file ACE mgmt1 of $OS_NAME$OS_RELEASE\"
OS_LIST[2]=\"mgmt2 $iso_file ACE mgmt2 of $OS_NAME$OS_RELEASE\"
OS_LIST[3]=\"$OS_NAME$OS_RELEASE $iso_file $OS_NAME $OS_RELEASE Server x86_64\"
    " > default.cfg
    echo "
      ** Can you check /images/pxe_boot_server directory ?
    "
  else
    run_exe
    echo "OS_LIST[1]=\"rhel6.2 /your/path/rhel-server-6.2-x86_64-dvd.iso RHEL 6.2 Server x86_64\"
OS_LIST[2]=\"mgmt1 /your/path/ace-1.3.3-20120312-2271-rhel6.2-x86_64-dvd.iso ACE mgmt1 of RHEL6.2\"
OS_LIST[3]=\"mgmt2 /your/path/ace-1.3.3-20120312-2271-rhel6.2-x86_64-dvd.iso ACE mgmt2 of RHEL6.2\"
#OS_LIST[4]=\"suse11sp1 /your/path/SLES-11-SP1-DVD-x86_64-GM-DVD1.iso SuSE11 SP1 x86_64\"
    " > default.cfg
    echo "
     ** check default.cfg file and modify it to your iso file. 
    "
  fi

  echo "
  ** start pxeboot server
     Can you check default.cfg file for iso file path ?
     if default.cfg file has no problem then you can modify run_eth1_eth0.sh script.
     and run this (run_eth1_eth0.sh) script 

  ** stop pxeboot server
  ./set_pxeboot.sh --mode unset
  "
}

error_exit() {
   echo $*
   exit
}

help() {
  echo "
$(basename $0)
version: $(echo $version)

$(basename $0) --mode <set|unset> [ <option> <value> ] ... [ <option#> <value#> ]

  --pxe_home /your/path : default /images/pxe_boot_server ( current server's root path )
  --dev eth#            : default eth0 ( pxe server ethernet device for export & tftp )
  --ip 10.100.0.1       : default NULL. if you want change IP then set IP of \"--dev\" device
                          ( pxe server eth#'s IP )
  --netmask 255.255.0.0 : default 255.255.255.0 ( netmask of pxe server eth#'s IP )
  --dest eth#           : default eth0 ( pxe client server ethernet device for using tftp )
  --iso_exp_dir /your_path         : default null ( <pxe_home>/iso_images )
                        - export base directory of iso mount point directory
  --ks_dir /your_path   : default null ( <pxe_home>/ks.cfg ), kickstart directory
  --tag <name>          : default default ( ks source file )


  --ks_cfg /your_path/ks.cfg   : 
                        - default null 
                             1. <tag>   :find ks source file with <tag> in the <ks_dir>
                             2. no <tag>:using ks file in the <ks_dir> with out convert from <tag>
                        - create ks file with this file.
                             sync : /root/anaconda-ks.cfg
                             </your/path/ks.cfg> : use this file.
    --os_name <os name> : save iso file to /images/<os_name> directory
    --ace_name <ace name>: save iso file to /images/<os_name>/<ace_name> name


  --tb_base your_path   : default null ( null is auto ) ex) /tftpboot
  --tb_ext your_path    : default pxebs
  --default             : create default.cfg & sample run script file. 
                          you can modify this file.
  --version / -v        : current version
  --help / -h           : current help
"
  exit
}


opt=( $* )
for (( i=0; i < ${#opt[*]} ; i++ )); do
   if [ "${opt[$i]}" == "--mode" ]; then
       MODE=${opt[$(($i+1))]}
       i=$(($i+1))
   elif [ "${opt[$i]}" == "--pxe_home" ]; then
       PXE_HOME=${opt[$(($i+1))]}
       i=$(($i+1))
   elif [ "${opt[$i]}" == "--dev" ]; then
       DEV=${opt[$(($i+1))]}
       i=$(($i+1))
   elif [ "${opt[$i]}" == "--dest" ]; then
       DEST=${opt[$(($i+1))]}
       i=$(($i+1))
   elif [ "${opt[$i]}" == "--netmask" ]; then
       NETMASK=${opt[$(($i+1))]}
       i=$(($i+1))
   elif [ "${opt[$i]}" == "--tag" ]; then
       TAG=${opt[$(($i+1))]}
       i=$(($i+1))
   elif [ "${opt[$i]}" == "--iso_exp_dir" ]; then
       ISO_EXP_DIR=${opt[$(($i+1))]}
       i=$(($i+1))
   elif [ "${opt[$i]}" == "--ks_dir" ]; then
       KS_DIR=${opt[$(($i+1))]}
       i=$(($i+1))
   elif [ "${opt[$i]}" == "--ks_cfg" ]; then
       KS_CFG=${opt[$(($i+1))]}
       i=$(($i+1))
   elif [ "${opt[$i]}" == "--os_name" ]; then
       OS_NAME=${opt[$(($i+1))]}
       i=$(($i+1))
   elif [ "${opt[$i]}" == "--ace_name" ]; then
       ACE_ISO_NAME=${opt[$(($i+1))]}
       i=$(($i+1))
   elif [ "${opt[$i]}" == "--tb_base" ]; then
       TB_BASE=${opt[$(($i+1))]}
       i=$(($i+1))
   elif [ "${opt[$i]}" == "--tb_ext" ]; then
       TB_EXT=${opt[$(($i+1))]}
       i=$(($i+1))
   elif [ "${opt[$i]}" == "--ip" ]; then
       NEW_IP=${opt[$(($i+1))]}
       i=$(($i+1))
   elif [ "${opt[$i]}" == "--default" ]; then
       create_default
       exit
   elif [ "${opt[$i]}" == "--version" -o "${opt[$i]}" == "-v" ]; then
       #grep "^#\$version" $0 | awk -F: '{print $2}'    
       echo $version
       exit
   elif [ "${opt[$i]}" == "--help" -o "${opt[$i]}" == "-h" ]; then
       help
   fi
done

[ -n "$PXE_HOME" ] || PXE_HOME=$(pwd)
config=$PXE_HOME/default.cfg

###########################################################
###########################################################
ks_ace() {
   local src dest SERVER_IP OS_DIR ACE OS_VER ACE_ISO_NAME ETH1_IP ETH2_IP KS_NETMASK DEV_DEST ACE ETH_IP
   src=$1
   dest=$2
   SERVER_IP=$3
   OS_DIR=$4
   ACE=$5
   OS_VER=$6
   ACE_ISO_NAME=$7
   ETH_IP=$8
   DEV_DEST=$9

   ETH1_IP=$(echo $ETH_IP | awk -F: '{print $1}')
   ETH2_IP=$(echo $ETH_IP | awk -F: '{print $2}')
   KS_NETMASK=$(echo $ETH_IP | awk -F: '{print $3}')

[ -f $src ] || error_exit "$src not found"
[ -d $(dirname $dest) ] || mkdir -p $(dirname $dest)

cat << EOF > $dest
#Installation Number for RHEL 5
key --skip
#System  language
lang en_US.UTF-8
#System keyboard
keyboard us
#System mouse

#Sytem timezone
timezone --utc Etc/UTC
#Root password
rootpw --iscrypted \$1\$yK7daMQL\$mvQYdszcbG6sIqojo6W0h1
#Reboot after installation
#reboot
#Use text mode install
text
#Install Red Hat Linux instead of upgrade
install
#Installation Media Part
nfs --server=$SERVER_IP --dir=$OS_DIR
#System bootloader configuration
bootloader --location=mbr  --driveorder=sda
#Clear the Master Boot Record
zerombr yes
#Partition clearing information
clearpart --initlabel --all
#Disk partitioning information
part swap --fstype swap --size 24576 --asprimary --ondisk=sda
part /boot --fstype ext3 --size 300 --asprimary  --ondisk=sda
part / --fstype ext3 --size 30000 --grow --asprimary --ondisk=sda
#System authorization infomation
auth  --useshadow  --passalgo=md5
#Network information
network --bootproto=dhcp --device=$DEV_DEST --onboot=on
#Firewall configuration
firewall --disabled
# Run the Setup Agent on first boot
firstboot --disable
# Installation logging level
logging --level=info
# SELinux configuration
selinux --disabled
#Do not configure XWindows
xconfig --startxonboot

%packages
$(cat $src | while read line ; do if [ "$chk" == "1" ]; then  if echo $line | grep "^%" >& /dev/null ; then break; else echo $line; fi; fi ;  echo $line | grep "^%packages" >/dev/null && chk=1; done )

%pre
echo This is the pre script

%post
export PATH=/sbin:/usr/sbin:\$PATH

exec >& /root/post-install.log
set -x

#fix network
echo "alias eth0 igb" > /etc/modprobe.d/igb.conf
echo "alias eth1 igb" >> /etc/modprobe.d/igb.conf
#  echo "alias eth2 mlx4_en" >> /etc/modprobe.d/mlx4_en.conf
#  echo "alias eth3 mlx4_en" >> /etc/modprobe.d/mlx4_en.conf
[ -f /etc/udev/rules.d/70-persistent-net.rules ] && rm -f /etc/udev/rules.d/70-persistent-net.rules
[ -f /etc/modprobe.d/mlx4_en.conf ] && rm -f /etc/modprobe.d/mlx4_en.conf
[ -f /etc/modprobe.d/libmlx4.conf ] && rm -f /etc/modprobe.d/libmlx4.conf
EOF

if [ "$ACE" == "1" ]; then
cat << EOF >> $dest

src_dir=/mnt/source
dest_dir=/
[ -d \$src_dir ] || mkdir -p \$src_dir
mount $SERVER_IP:$OS_DIR \$src_dir
[ -d \$dest_dir/images ] || mkdir -p \$dest_dir/images
[ -d \$src_dir/icr ] && cp -fra \$src_dir/icr \$dest_dir/images
[ -d \$src_dir/ace ] && cp -fra \$src_dir/ace \$dest_dir/images
# Make ISO file
dvd_mount=\$src_dir
output_iso_file=\$dest_dir/images/$OS_VER/$ACE_ISO_NAME
[ -d \$(dirname \$output_iso_file) ] || mkdir -p \$(dirname \$output_iso_file)
tmp_isolinux=\$(mktemp -d /tmp/isolinux.XXXXXX)
cp -fra \$dvd_mount/isolinux \$tmp_isolinux
mount -o bind \$tmp_isolinux/isolinux \$dvd_mount/isolinux
mkisofs -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -input-charset=UTF-8 -output-charset=UTF-8 -R -J -T -v -x "lost+found" -o \$output_iso_file \$dvd_mount
umount /mnt

# turn off some broke stuff
for chkl in \$(chkconfig --list | grep ":on" | awk '{print \$1}'); do
     [ -f /etc/init.d/\$chkl ] && chkconfig --level 123456 \$chkl off
done

for chkl in atd acpid crond haldaemon ipmi irqbalance mcelogd messagebus netfs network ntpd nfs rpcbind rsyslog syslog udev-post xinetd cpuspeed microcode_ctl sshd; do
     [ -f /etc/init.d/\$chkl ] && chkconfig --level 2345 \$chkl on
done

#change runlevel from 5 to 3
sed -e 's/^id:5:initdefault:/id:3:initdefault:/g' < /etc/inittab > /etc/inittab~
mv /etc/inittab~ /etc/inittab
rm -f /etc/sysconfig/network-scripts/ifcfg-eth?
# configure the NICs
ks_eth0="$ETH1_IP"
ks_eth1="$ETH2_IP"
ks_netmask="$KS_NETMASK"

if [ -n "\$ks_eth0" -a -n "\$ks_netmask" ]; then
    #mac=\$(ifconfig eth0 | grep "HWaddr" | awk '{print \$5;}')
    echo "# ACE Xtreme-X installation
DEVICE=eth0
BOOTPROTO=none
HWADDR=\$mac
ONBOOT=yes
NETMASK=\$ks_netmask
IPADDR=\$ks_eth0
TYPE=Ethernet
USERCTL=no
IPV6INIT=no
PEERDNS=yes" > /etc/sysconfig/network-scripts/ifcfg-eth0
fi

if [ -n "\$ks_eth1" -a -n "\$ks_netmask" ]; then
    #mac=\$(ifconfig eth1 | grep "HWaddr" | awk '{print \$5;}')
    echo "# ACE Xtreme-X installation
DEVICE=eth1
BOOTPROTO=none
HWADDR=\$mac
ONBOOT=yes
NETMASK=\$ks_netmask
IPADDR=\$ks_eth1
TYPE=Ethernet
USERCTL=no
IPV6INIT=no
PEERDNS=yes" > /etc/sysconfig/network-scripts/ifcfg-eth1
fi

if ! grep "^ttyS1" /etc/securetty >/dev/null 2>&1 ; then
   echo "ttyS1 " >> /etc/securetty
fi
if ! grep "^ttyS2" /etc/securetty >/dev/null 2>&1 ; then
   echo "ttyS2 " >> /etc/securetty
fi


    echo "\$(sed '17d' /boot/grub/grub.conf) max_loop=256
\$(tail -n 1 /boot/grub/grub.conf)" > /boot/grub/grub.conf.ace
    mv /boot/grub/grub.conf.ace /boot/grub/grub.conf
EOF
fi

}

###########################################################
###########################################################
pxe_set() {
echo
echo "################################################"
echo "#  Start pxeboot server"
echo "################################################"

if [ -f $config ]; then
  . $config
else
  echo "

  make a default config file (default.cfg)

  OS_LIST[1]=\"<title> <full_path/iso_file> <description>\"
  OS_LIST[2]=\"<title2> <full_path/iso_file2> <description2>\"
  ....

"
  exit
fi

grep "^MODE=" $config |grep set >/dev/null && error_exit "it is working now. if you want restart then you can use \"reset\" mode"
[ -n "$KS_DIR" ] || KS_DIR=$PXE_HOME/ks.cfg
[ -d $KS_DIR ] || mkdir -p $KS_DIR
[ -n "$ISO_EXP_DIR" ] || ISO_EXP_DIR=$PXE_HOME/iso_images
[ -d $ISO_EXP_DIR ] || mkdir -p $ISO_EXP_DIR
TB_BASE_PXE_HOME=$PXE_HOME/tftpboot
[ -d $TB_BASE_PXE_HOME ] || mkdir -p $TB_BASE_PXE_HOME
if [ ! -n "$TB_BASE" ]; then
   [ -f /etc/xinetd.d/tftp ] || error_exit "/etc/xinetd.d/tftp file not found"
   tftp_base_dir=( $(grep server_args /etc/xinetd.d/tftp) )
   TB_BASE=${tftp_base_dir[$(( ${#tftp_base_dir[*]} - 1 ))]}
fi
[ -n "$TB_EXT" ] ||  TB_EXT=pxebs
[ -n "$NETMASK" ] ||  NETMASK=255.255.255.0
[ -n "$DEV" ] ||      DEV=eth0
[ -n "$DEST" ] ||     DEST=eth0
[ -n "$TAG" ] ||      TAG=default
[ "$KS_CFG" == "sync" ] && KS_CFG=/root/anaconda-ks.cfg
if [ ${#OS_LIST[*]} -eq 0 ]; then
  echo "I can not find PXEBOOT OS."
  exit
else
  echo "Start setting ${#OS_LIST[*]} configuration"
fi

echo "DEST=$DEST" >> $config
echo "PXE_HOME=$PXE_HOME" >> $config
echo "TB_BASE=$TB_BASE" >> $config
echo "TB_EXT=$TB_EXT" >> $config
echo "ISO_EXP_DIR=$ISO_EXP_DIR" >> $config
echo "MODE=set" >> $config

DAEMONS=""
RDAEMONS=""
IP=$(ifconfig $DEV | grep "inet addr" | awk '{print $2}' | awk -F: '{print $2}')
NET_FLAG="$([ -f /sys/class/net/$DEV/flags ] && cat /sys/class/net/$DEV/flags)"
if [ -n "$NEW_IP" ]; then
   if [ "$IP" != "$NEW_IP" -o "0x1002" == "$NET_FLAG" ]; then
      echo "Found $IP at $DEV, Can I set $NEW_IP to $DEV ?"
      echo "enter key to continue"
      echo "if you want stop then \"control+c\""
      read x
      ifconfig $DEV $NEW_IP netmask $NETMASK up
      DAEMONS="IP_$DEV"
   fi
   IP=$NEW_IP
fi
ping -c 2 $IP >& /dev/null || error_exit "I can't ping to $IP of $DEV"

if [ -d /acefs/global ]; then
   [ "$DEST" == "eth2" ] && error_exit "ACE is working, So you can not use eth2 for destination device( device of client node )"
   grep -w "$IP" /etc/dhcpd.conf >& /dev/null  && error_exit "ACE is using $IP, So you can not use $IP for destination device( device of client node )"
fi


############################################################################
### check environment
#rhel5x
#package_list="tftp-server tftp dhcp OpenIPMI nfs-utils"
#rhel6x
package_list="tftp-server tftp dhcp OpenIPMI rpcbind nfs-utils"
#suse
#package_list="tftp dhcp-server nfs-kernel-server"
for pl in $package_list; do
   rpm -qa --queryformat '%{NAME}\n' | grep -w "^${pl}$" >& /dev/null || error_exit "requirement package \"$pl\""
done
############################################################################
#OS and exprot set
[ -d $PXE_HOME/etc ] || mkdir -p $PXE_HOME/etc
[ -f /etc/exports ] && cat /etc/exports > $PXE_HOME/etc/exports || echo > $PXE_HOME/etc/exports
echo "$KS_DIR *(rw,sync,no_root_squash)" >> $PXE_HOME/etc/exports

[ -d $TB_BASE_PXE_HOME/msgs ] || mkdir -p $TB_BASE_PXE_HOME/msgs

#                09Welcome to 0cPXE BOOT Server09 -$(grep "^#\$version:" $0 | awk -F: '{print $2}' | sed "s/ //g") !07
echo "

                09Welcome to 0cPXE BOOT Server09 -$(echo $version) !07
0a
07

Enter number of the Operation System you wish to install:

0.  Local Machine
" > $TB_BASE_PXE_HOME/msgs/boot.msg


[ -d $TB_BASE_PXE_HOME/pxelinux.cfg ] || mkdir -p $TB_BASE_PXE_HOME/pxelinux.cfg
if [ -f $TB_BASE_PXE_HOME/pxelinux.0 ]; then
    pxelinux_file=$TB_BASE_PXE_HOME/pxelinux.0
elif [ -f /opt/ace/share/pxelinux.0 ]; then
    pxelinux_file=/opt/ace/share/pxelinux.0
elif locate pxelinux.0 | grep "pxelinux\.0" >& /dev/null ; then
    pxelinux_file=$(locate pxelinux.0 | grep "pxelinux\.0"|head -n 1)
else
    error_exit "\"pxelinux.0\" not found "
fi
cp -af $pxelinux_file $TB_BASE_PXE_HOME

echo "default local
timeout 100
prompt 1
display msgs/boot.msg
F1 msgs/boot.msg

serial  1,19200

label local
  localboot 1

label 0
  localboot 1
" > $TB_BASE_PXE_HOME/pxelinux.cfg/default

for i in $(seq 1 ${#OS_LIST[*]}); do
   os=( ${OS_LIST[$i]} )
   [ -f ${os[1]} ] || error_exit "${os[1]} not found"
   echo "set ${os[0]} with ${os[1]}"

   desc=$(echo $(for z in $(seq 2 $(( ${#os[*]}-1 ))); do echo ${os[$z]}; done))
   echo "$i. ${os[0]}  - $desc" >> $TB_BASE_PXE_HOME/msgs/boot.msg
   ks_name="ks.${os[0]}.cfg"

   if [ -d $ISO_EXP_DIR/${os[0]} ]; then
       grep iso9660 | grep "$ISO_EXP_DIR/${os[0]}" >& /dev/null || mount -o loop ${os[1]} $ISO_EXP_DIR/${os[0]} 2> /dev/null
   else
       mkdir -p $ISO_EXP_DIR/${os[0]}
       mount -o loop ${os[1]} $ISO_EXP_DIR/${os[0]} 2> /dev/null
   fi
   echo "$ISO_EXP_DIR/${os[0]}  *(rw,sync,no_root_squash)" >> $PXE_HOME/etc/exports
   [ -d $TB_BASE_PXE_HOME/${os[0]} ] ||  mkdir -p  $TB_BASE_PXE_HOME/${os[0]}
   if echo "${os[*]}" | grep -e suse -e SUSE -e SLES -e sles >& /dev/null ; then
      cp -fa $ISO_EXP_DIR/${os[0]}/boot/x86_64/loader/initrd $TB_BASE_PXE_HOME/${os[0]}
      cp -fa $ISO_EXP_DIR/${os[0]}/boot/x86_64/loader/linux $TB_BASE_PXE_HOME/${os[0]}
      kernel=linux
      initrd=initrd
      echo "
label $i 
  kernel ${os[0]}/$kernel
  append load_ramdisk=1 initrd=${os[0]}/$initrd splash=silent showopts ramdisk_size=8192 init=linuxrc autoyast=nfs://$IP$KS_DIR/$ks_name netdevice=$DEST install=nfs://$IP/$ISO_EXP_DIR/${os[0]}
       " >> $TB_BASE_PXE_HOME/pxelinux.cfg/default
   else
      cp -fa $ISO_EXP_DIR/${os[0]}/isolinux/initrd.img $TB_BASE_PXE_HOME/${os[0]}
      cp -fa $ISO_EXP_DIR/${os[0]}/isolinux/vmlinuz $TB_BASE_PXE_HOME/${os[0]}
      kernel=vmlinuz
      initrd=initrd.img
      echo "
label $i 
  kernel ${os[0]}/$kernel
  append initrd=${os[0]}/$initrd ksdevice=$DEST ramdisk_size=8192 ks=nfs:$IP:$KS_DIR/$ks_name
       " >> $TB_BASE_PXE_HOME/pxelinux.cfg/default
   fi


   if [ -n "$KS_CFG" ]; then
      if echo $ks_name | grep mgmt1 >& /dev/null ; then
         ETH_IP='10.4.0.1:10.5.0.1:255.255.0.0'
         ACE=1
      elif echo $ks_name | grep mgmt2 >& /dev/null ; then
         ETH_IP='10.4.0.2:10.5.0.2:255.255.0.0'
         ACE=1
      else
         ETH_IP=0:0:0
         ACE=0
      fi
      [ -n "$OS_NAME" ] && OS_NAME_SRC=$OS_NAME || OS_NAME_SRC=${os[0]}
      [ -n "$ACE_ISO_NAME" ] && ACE_ISO_NAME_SRC=$ACE_ISO_NAME || ACE_ISO_NAME_SRC=$(basename ${os[1]})
      [ -f $KS_CFG ] && ks_ace $KS_CFG $KS_DIR/$ks_name $IP  $ISO_EXP_DIR/${os[0]} $ACE $OS_NAME_SRC $ACE_ISO_NAME_SRC $ETH_IP $DEST || error_exit "$KS_CFG not found"
   else
      [ -f $KS_DIR/$ks_name.$TAG ] &&  sed -e "s#SERVER_IP#$IP#g" -e "s#OS_DIR#$ISO_EXP_DIR/${os[0]}#g" -e "s#DEV_DEST#$DEST#g" < $KS_DIR/$ks_name.$TAG > $KS_DIR/$ks_name
   fi
done

echo "


05[F1-Main] 07" >> $TB_BASE_PXE_HOME/msgs/boot.msg

### Calculate IP
if [ ! -n "$IP" ]; then
  echo "I can't find IP address. Can you check device $DEV ? "
  exit
fi
NETMASK_CLASS=0
for i in $(echo $NETMASK | sed -e 's/\./ /g'); do 
   [ "$i" == "255" ] && NETMASK_CLASS=$(( $NETMASK_CLASS + 1 ))
done
jc=0
for j in $(echo $IP| sed -e 's/\./ /g'); do
   jc=$(($jc+1))
   [ -n "$NETWORK_SRC" ] && NETWORK_SRC="$NETWORK_SRC.$j" || NETWORK_SRC="$j"
   [ $jc -eq $NETMASK_CLASS ] && break
done

BROADCASET="$NETWORK_SRC"
NETWORK="$NETWORK_SRC"
for (( c=$(($jc+1)) ; $c <= 4; c++ )); do
   NETWORK="$NETWORK.0"
   BROADCASET="$BROADCASET.255"
done


### DHCP Set
[ -d $PXE_HOME/etc ] || mkdir -p $PXE_HOME/etc
[ -n "$TB_EXT" ] && pxelinux=/$TB_EXT/pxelinux.0 || pxelinux=/pxelinux.0
#if [ -f /etc/dhcpd.conf ]; then
#    grep "^subnet" /etc/dhcpd.conf >& /dev/null  &&  cat /etc/dhcpd.conf > $PXE_HOME/etc/dhcpd.conf ||  rm -f $PXE_HOME/etc/dhcpd.conf
#else
#   touch /etc/dhcpd.conf
#   rm -f $PXE_HOME/etc/dhcpd.conf
#fi

#echo "ddns-update-style interim;
#ignore client-updates;
#
#subnet $NETWORK netmask $NETMASK {
#  range $(echo $NETWORK | awk -F. '{printf "%s.%s.%s.200",$1,$2,$3}') $(echo $NETWORK | awk -F. '{printf "%s.%s.%s.250",$1,$2,$3}');
#  option routers $IP;
#  option broadcast-address $BROADCASET;
#  allow booting;
#  allow bootp;
#  class \"pxeclients\" {
#        match if substring (option vendor-class-identifier, 0, 9) = \"PXEClient\";
#        next-server $IP;
#        filename \"$pxelinux\";
#  }
#} 
#" >> $PXE_HOME/etc/dhcpd.conf 

echo "ddns-update-style interim;
allow booting;
allow bootp;

class \"pxeclients\" {
   match if substring(option vendor-class-identifier, 0, 9) = \"PXEClient\";
#   option routers $IP;
#   option domain-name-servers $IP;
}

subnet $NETWORK netmask $NETMASK {
   range $(echo $NETWORK | awk -F. '{printf "%s.%s.%s.200",$1,$2,$3}') $(echo $NETWORK | awk -F. '{printf "%s.%s.%s.250",$1,$2,$3}');
   next-server $IP;
   option routers $IP;
   option domain-name-servers $IP;
   filename \"$pxelinux\";
} 

" > $PXE_HOME/etc/dhcpd.conf

if [ -f /etc/dhcpd.conf ]; then
   cat /etc/dhcpd.conf | while read line ; do echo $line |grep "^subnet" >& /dev/null && chk=1; if [ "$chk" == "1" ]; then echo "$line"; fi ; done >> $PXE_HOME/etc/dhcpd.conf
fi


if cmp $PXE_HOME/etc/dhcpd.conf /etc/dhcpd.conf >& /dev/null; then
   cat /proc/mounts | grep dhcpd.conf >& /dev/null  || mount -o bind $PXE_HOME/etc/dhcpd.conf /etc/dhcpd.conf
else
   mount -o bind $PXE_HOME/etc/dhcpd.conf /etc/dhcpd.conf
fi
if [ -d /etc/dhcp ]; then
  if ! cmp /etc/dhcp/dhcpd.conf /etc/dhcpd.conf >& /dev/null; then
     mv /etc/dhcp/dhcpd.conf /etc/dhcp/dhcpd.conf.nfs-ha
     ln -sf /etc/dhcpd.conf /etc/dhcp/dhcpd.conf 
  fi
fi

### mount ks, export, etc environemnt
[ -f /etc/exports ] || touch /etc/exports
if cmp $PXE_HOME/etc/exports /etc/exports >& /dev/null; then
   cat /proc/mounts | grep exports >& /dev/null  || mount -o bind $PXE_HOME/etc/exports /etc/exports
else
   mount -o bind $PXE_HOME/etc/exports /etc/exports
fi

### tftpboot
[ -d $TB_BASE/$TB_EXT ] || mkdir -p $TB_BASE/$TB_EXT
if cat /proc/mounts | grep "$TB_BASE/$TB_EXT" >& /dev/null; then
   if ! cmp $TB_BASE_PXE_HOME/pxelinux.cfg/default $TB_BASE/$TB_EXT/pxelinux.cfg/default >& /dev/null; then
      mount -o bind $TB_BASE_PXE_HOME $TB_BASE/$TB_EXT
   fi
else
   mount -o bind $TB_BASE_PXE_HOME $TB_BASE/$TB_EXT
fi

### Start Deaemon
if ! chkconfig --list |grep tftp | grep on |grep -v grep>& /dev/null; then
    chkconfig tftp on
    DAEMONS="tftp $DAEMONS"
fi
if ps -ef |grep "in\.tftpd" |grep -v grep > /dev/null  2>&1 ; then
   kill -9 $(echo $(ps -ef |grep in.tftpd |grep -v grep | awk '{print $2}'))
fi

rm -f /var/lib/dhcpd/*
if ps -ef |grep dhcpd |grep -v grep > /dev/null  2>&1 ; then
    /etc/init.d/dhcpd force-reload
    RDAEMONS="dhcpd $RDAEMONS"
else
    /etc/init.d/dhcpd start
    DAEMONS="dhcpd $DAEMONS"
fi

if ps -ef |grep xinetd |grep -v grep > /dev/null  2>&1 ; then
    service xinetd restart
    RDAEMONS="xinetd $RDAEMONS"
else
    service xinetd start
    DAEMONS="xinetd $DAEMONS"
fi

if [ -f /etc/init.d/rpcbind ]; then
    if ! ps -ef |grep "rpcbind" |grep -v grep >& /dev/null; then
        DAEMONS="rpcbind $DAEMONS"
        service rpcbind start
    fi
fi
if [ -f /etc/init.d/rpcidmap ]; then
    if ! ps -ef |grep "rpc\.idmapd" |grep -v grep >& /dev/null; then
        DAEMONS="rpcidmapd $DAEMONS"
        service rpcidmapd start
    fi
fi
if ps -ef |grep nfsd |grep -v grep > /dev/null  2>&1 ; then
    exportfs -ra
else
    DAEMONS="nfs $DAEMONS"
    [ -f /etc/init.d/nfsserver ] && /etc/init.d/nfsserver start
    [ -f /etc/init.d/nfs ] && /etc/init.d/nfs start
fi
echo "DAEMONS=\"$DAEMONS\"" >> $config
echo "RDAEMONS=\"$RDAEMONS\"" >> $config
}




###########################################################
###########################################################
pxe_unset() {
echo
echo "################################################"
echo "# Stop pxeboot server"
echo "################################################"
if [ -f $config ]; then
    . $config
else
    echo "default.cfg file not found"
    exit
fi
if ! grep "^MODE=" $config >/dev/null ; then
   error_exit "it is stopped"
fi


if [ ${#OS_LIST[*]} -eq 0 ]; then
  echo "I can't find PXEBOOT OS."
  exit
fi


umount /etc/exports 2>/dev/null
sleep 2
exportfs -ra

sleep 10

# Restart daemons
for rdm in $RDAEMONS; do
    if [ "$rdm" == "xinetd" ]; then
        service $rdm restart
    elif [ "$rdm" == "dhcpd" ]; then
        /etc/init.d/dhcpd force-reload
    fi
done

#stop daemons
for dm in $DAEMONS; do
    if [ "$dm" == "tftp" ]; then
        chkconfig tftp off
    elif echo $dm | grep "^IP_" >& /dev/null ;then
        dev=$(echo "$dm" | awk -F_ '{print $2}' )
        ifconfig $dev down
    elif [ "$dm" == "nfs" ]; then
        [ -f /etc/init.d/nfs ] && /etc/init.d/nfs stop
        [ -f /etc/init.d/nfsserver ] && /etc/init.d/nfsserver stop
    else
        service $dm stop
    fi
done

for i in $(seq 1 ${#OS_LIST[*]}); do
   os=( ${OS_LIST[$i]} )
   if grep iso9660 /proc/mounts | grep "$ISO_EXP_DIR/${os[0]}" >& /dev/null; then
      umount $ISO_EXP_DIR/${os[0]} 2>/dev/null || umount -l $ISO_EXP_DIR/${os[0]}
   fi
   [ -d $ISO_EXP_DIR/${os[0]} ] && rmdir $ISO_EXP_DIR/${os[0]}
done

sleep 2
umount /etc/dhcpd.conf 2> /dev/null
if umount $TB_BASE/$TB_EXT 2>/dev/null ; then
  if rmdir $TB_BASE/$TB_EXT >/dev/null; then
      grep -v -e "^TB_EXT=" -e "^ISO_EXP_DIR=" -e "^DEST=" -e "^PXE_HOME=" -e "^TB_BASE="  -e "^DAEMONS=" -e "^RDAEMONS=" -e "^MODE=" $config  > ${config}~
      \mv ${config}~ $config
  fi
fi
}



###########################################################
###########################################################
if [ "$MODE" == "set" -o "$MODE" == "start" ]; then
    pxe_set
elif [ "$MODE" == "unset" -o "$MODE" == "stop" ]; then
    pxe_unset
else
    help
fi
