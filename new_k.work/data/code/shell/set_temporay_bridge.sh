#!/bin/sh
# Kage  03/19/2013
# Set Network bridge
error_exit() {
    echo "$*"
    exit 1
}

_k_br() {
    local br eth ip netmask
    br=$1
    eth=$2
    ip=$3
    netmask=$4

    [ "$(cat /proc/sys/net/ipv4/ip_forward)" == "1" ] || echo 1 > /proc/sys/net/ipv4/ip_forward
    if [ -d /sys/class/net/$br ]; then
        vir_eth=$(echo "$eth" | awk -F: '{print $1}')
        vir_id=$(echo "$eth" | awk -F: '{print $2}')
        if [ -d /sys/class/net/$br/brif/$vir_eth ]; then
            if [ -n "$vir_id" ]; then
                ifconfig $eth down
                ifconfig ${br}:${vir_id} $ip netmask $netmask up
            else
                ifconfig $eth 0.0.0.0 up
                ifconfig $br $ip netmask $netmask up
            fi
        else
            ifconfig $vir_eth 0.0.0.0 up
            brctl addif $br $vir_eth
        fi
    else
        brctl addbr $br && brctl stp $br off
        brctl addif $br $eth
        ifconfig $eth 0.0.0.0
        ifconfig $br $ip netmask $netmask up
    fi
}

br=$1
net_dev=($2)
ip=$3 
netmask=$4
(( $# == 4 )) || error_exit "$(basename $0) <br name> \"<net1> <net2> ...\" <ip> <netmask>"

_k_br $br ${net_dev[0]} $ip $netmask
for ((ii=1;ii<${#net_dev[*]}; ii++));do
   _k_br $br ${net_dev[$ii]}
done

echo "made tempoary bridge $net_dev to $br with $ip $netmask"
