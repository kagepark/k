freeqlut_src=freeglut-2.8.1.tar.gz
cuda_sdk=cuda_5.5.22_linux_64.run

if ! grep "nouveau" /etc/modprobe.d/blacklist.conf >/dev/null; then
   echo "blacklist nouveau
options nouveau modeset=0" >> /etc/modprobe.d/blacklist.conf
fi

if [ -d /acefs/global ]; then
    cluster=$1
    if ! grep "nouveau" /acefs/clusters/$cluster/kernel_args >/dev/null; then
        ace put "/clusters/$cluster/kernel_args='$(cat /acefs/clusters/$cluster/kernel_args) rdblacklist=nouveau nouveau.modeset=0'"
    fi
fi

for ii in libXdamage \
libXdamage-devel \
libdrm-devel \
libXxf86vm \
libXxf86vm-devel \
mesa-libGL-devel \
mesa-libGLU-devel \
libICE-devel \
libSM-devel \
libXt \
libXt-devel \
libXmu-devel \
freegult \
freegult-devel ; do
    if ! rpm -qa --queryformat '%{name}\n' | grep "^${ii}$" >/dev/null; then
        if [ "$ii" == "freegult" ]; then
            freeqlut=1
        elif [ "$ii" == "freegult-devel" ]; then
            freeqlut_devel=1
        else
            echo "Requirement package is : $ii"
            chk=1
        fi
    fi
done 

[ "$chk" == "1" ] && exit

if [ "$freeglut" == "1" -a "$freeglut_devel" == "1" ]; then
   if [ -f $freeqlut_src ]; then
      tar zxvf $freeqlut_src 
      cd freeglut-2.8.1 && ./configure --prefix=/usr --libdir=/usr/lib64 && make -j 10 && make install && ldconfig
   fi
fi

sh $cuda_sdk

echo "/usr/local/cuda/lib64
/usr/local/cuda/lib
/usr/local/lib64
/usr/local/lib
" >  /etc/ld.so.conf.d/cuda.conf
ldconfig

echo "export PATH=\${PATH}:/usr/local/cuda/bin" > /etc/profile.d/cuda.sh
