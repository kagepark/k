cd /proc
for pfile in $(ls | grep "^[1-9]"); do
    if [ -f $pfile/status ]; then
       name=$(cat $pfile/status | awk -F: -v iv=Name '{ if ($1 == iv ) print $2}' | awk '{print $1}' )
       pid=$pfile
       vmpeak=$(cat $pfile/status | awk -F: -v iv=VmPeak '{ if ( $1 == iv ) print $2}' | awk '{print $1}' )
       vmdata=$(cat $pfile/status | awk -F: -v iv=VmData '{ if ( $1 == iv ) print $2}' | awk '{print $1}' )
       [ -n "$vmpeak" ] && printf "%30s vmpeak: %10s vmdata: %10s\n" "$name ($pid)" "$vmpeak" "$vmdata"
    fi
done
