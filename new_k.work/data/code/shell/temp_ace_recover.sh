#!/bin/sh
#
# Recover ACE from existing /ha_cluster with OS iso and ACE iso.
# 01/04/2017  Kage Park


error_exit() {
    echo $*
    exit 1
}

ip2str() {
    local num
    num=$1
    [ -n "$num" ] || error_exit "input not found"
    echo $(( $(( $num/$((255*255*255)) ))% 255)).$(( $(($num/$((255*255)) ))%255)).$(( $(($num/255)) % 255)).$(($num%255))
}

str2ip() {
    [ -n "$1" ] || error_exit "input not found"
    ip=( $(echo $1 | sed 's/\./ /g') )
    echo $(( $((255*255*255*${ip[0]})) + $((255*255*${ip[1]})) + $((255*${ip[2]})) +${ip[3]} ))
}

add_ip() {
    [ -n "$1" ] || error_exit "IP not found"
    [ -n "$2" ] || error_exit "add value not found"
    echo $(ip2str $(( $(str2ip $1) + $2 )) )
}

read_db() {
   local find val db_file
   db_file=$1
   [ -f $db_file ] || error_exit "$db_file not found"
   shift 1
   for find in $*; do
      val=$(awk -v find=$find '{if($1==find) print}' $db_file | awk -F# '{print $1}')
      if [ -n "$val" ]; then
          val=$(echo "$val" | grep "'" >& /dev/null && echo $val | awk -F\' '{print $2}' || echo $val | awk '{print $2}')
          [ -n "$val" ] && echo $val || return 1
      else
          logger -s "read_db: $find not found"
          return 1
      fi
   done
}

netmask2bit() {
    local netmask
    netmask=$1

    bit_num=$(echo $1 | sed "s/\./\n/g" | grep 255 | wc -l)
    echo $(($bit_num * 8))
}

get_device_mac() {
    [ -f /sys/class/net/$1/address ] || return 1
    cat /sys/class/net/$1/address
}

net_dev_ip() {
    local dev
    dev=$1
    [ -d /sys/class/net/$dev ] || return 1
    ifconfig $dev | awk '{if($1=="inet") print $2}'
}

set_hostname() {
    local _hostname=$1
    [ -f /etc/hostname ] && echo "$_hostname" > /etc/hostname
    mv /etc/sysconfig/network /etc/sysconfig/network.ace
    sed "s/^HOSTNAME=.*\$/HOSTNAME=$_hostname/g" < /etc/sysconfig/network.ace > /etc/sysconfig/network
    hostname $_hostname
}

config_dir() {
    echo /etc/sysconfig/network-scripts
}

[ -d $(config_dir)/../old-network-scripts ] || mkdir -p $(config_dir)/../old-network-scripts >& /dev/null
saved_date=$(date +'%Y%m%d%H%M%S')

config_file() {
    local _device _ipaddr _netmask _ext
    (( "${#*}" < "3" )) && return 1
    _device=$1
    _ipaddr=$2
    _netmask=$3
    _ext=$4
    echo $(config_dir)/ifcfg-${_device}${_ext}
}

write_config() {
    local _device _ipaddr _netmask _ext _config _hwaddr _uuid
    (( "${#*}" < "3" )) && return 1
    _device=$1
    _ipaddr=$2
    _netmask=$3
    _ext=$4
    _config=$(config_file $*)
    _hwaddr=$(get_device_mac $_device) || error_exit No MAC address found for $_device
    saved_date=1111
    if [ -f $_config ]; then
        _uuid=$(awk -F= '{if($1=="UUID") print $2}' $_config)
        [ -d $(config_dir)/../old-network-scripts ] || mkdir -p $(config_dir)/../old-network-scripts
        mv $_config $(config_dir)/../old-network-scripts/$(basename ${_config}).$saved_date
        echo "Existing network configuration file($_config) moved to $(config_dir)/../old-network-scripts/$(basename $_config).$saved_date"
    fi
    echo "# Ethernet NIC - Created during ACE installation on $(date)
$( [ -n "$_uuid" ] && echo "UUID=$_uuid")
DEVICE=$_device
HWADDR=$_hwaddr
ONBOOT=yes
NETMASK=$_netmask
IPADDR=$_ipaddr
TYPE=Ethernet" > $_config
    echo "Created $_config file by ACE"
}

req_rpms() {
    local media_dir dvd_iso prpms ace emsd utils ib_firmware compile devel ofed_req net_snmp
    dvd_iso=$1
    [ -f "$dvd_iso" ] || return 1

    emsd="expect tk tcl tcl-devel bison readline-devel gcc-gfortran smartmontools pciutils flex libXpm-devel lsof wget libaio-devel bc libgcrypt-devel pam-devel rcs sysstat tcsh libcurl-devel sysstat mcelog"
    ace="dhcp dhcp-common openmotif openssh-server openssh-clients openssl-devel nfs-utils nfs-utils-lib rpcbind tftp tftp-server pciutils smartmontools ntp ntpdate fuse fuse-devel fuse-libs expect rsync screen mcelog rsyslog dmidecode sysstat net-snmp net-snmp-devel net-snmp-libs net-snmp-utils crash kexec-tools ipmitool xinetd OpenIPMI OpenIPMI-libs readline zlib zlib-devel libgcc xinetd"
    utils="psmisc createrepo telnet OpenIPMI screen minicom iputils iotop ed vim-enhanced vim-common hdparm bzip2 zip unzip cpio gzip tcsh zsh chrpath createrepo deltarpm device-mapper-multipath device-mapper-multipath-libs dracut-kernel edac-utils  gd giflib glib2 keyutils libedit libevent libgssglue libnl libtirpc libtool-ltdl libuuid libXp lockdev lrzsz lsof minicom module-init-tools mysql mysql-libs mysql-server newt perl-TimeDate portreserve python-deltarpm PyXML rhino  slang tcsh texinfo tigervnc tigervnc-server tzdata-java lsscsi numactl-devel libnl-devel"
    ib_firmware="libstdc++.i686 ncurses-libs.i686"
    compile="gcc gcc-gfortran make cmake autoconf rpm-build"
    devel="libtool-ltdl-devel libXpm openssl-devel glib2-devel libXft-devel krb5-devel pcre-devel xz-devel libxml2-devel glibc-devel zlib-devel tk-devel tcl-devel readline-devel keyutils-libs krb5-libs libuuid-devel ncurses-libs libXp-devel"
    ofed_req="pciutils gtk2 atk cairo gcc-gfortran tcsh lsof tcl numactl tk"
    net_snmp="bzip2-devel elfutils-devel rpm-devel perl-devel mysql-devel net-tools lm_sensors-devel perl-ExtUtils-Embed"
    prpms="$ace $emsd $utils $ib_firmware $compile $devel $ofed_req $net_snmp"

    media_dir=/media

  if [ -n "$dvd_iso" ]; then
     [ -f $dvd_iso ] || error_exit "$dvd_iso not found"
     [ -d $media_dir ] || mkdir -p $media_dir
     mount -o loop $dvd_iso $media_dir
  else
     [ "$(awk -v mp=$media_dir '{if($2==mp) printf "ok"}' /proc/mounts)" == "ok" ] || error_exit "$media_dir not found for OS DVD mounted"
  fi

  if [ -d /lib/systemd/system ]; then
     addons="/addons"
  fi

    echo "[PACEMAKER_Y]
name=PACEMAKER_Y
baseurl=file://$media_dir
enable=1
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release

[PACEMAKER_Y-HA]
name=PACEMAKER_Y-HA
baseurl=file://${media_dir}${addons}/HighAvailability
enabled=1
gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
    " > /etc/yum.repos.d/ace_pacemaker.repo

    yum --disablerepo="*" --enablerepo="PACEMAKER_*" --disableexcludes=all -y install $prpms
    wait
    sleep 1
    umount $media_dir
    rm -f /etc/yum.repos.d/ace_pacemaker.repo
    return 0
}

ace_rpm() {
    local media_dir dvd_iso prpms ace emsd utils ib_firmware compile devel ofed_req net_snmp ha_type
    dvd_iso=$1
    ha_type=$2
    [ -f "$dvd_iso" ] || return 1
    media_dir=/media

  if [ -n "$dvd_iso" ]; then
     [ -f $dvd_iso ] || error_exit "$dvd_iso not found"
     [ -d $media_dir ] || mkdir -p $media_dir
     mount -o loop $dvd_iso $media_dir
     . $media_dir/libfunc.sh
     check_os
     if [ ! -d $media_dir/packages/ace/$OS_NAME$OS_RELEASE ]; then
          echo "This ace iso file not support $OS_NAME$OS_RELEASE on this server"
          exit 1
     fi
     rpm -ihv $media_dir/packages/ace/$OS_NAME$OS_RELEASE/inotify-*.${OS_NAME}${OS_RELEASE}.x86_64.rpm
     rpm -ihv $media_dir/packages/ace/$OS_NAME$OS_RELEASE/cman-libs-*.${OS_NAME}${OS_RELEASE}.x86_64.rpm
     rpm -ihv $media_dir/packages/ace/$OS_NAME$OS_RELEASE/gnbd-kmod-*_$(uname -r).${OS_NAME}${OS_RELEASE}.x86_64.rpm
     rpm -ihv $media_dir/packages/ace/$OS_NAME$OS_RELEASE/gnbd-*.${OS_NAME}${OS_RELEASE}.x86_64.rpm
     rpm -ihv $media_dir/packages/ace/$OS_NAME$OS_RELEASE/module-*.${OS_NAME}${OS_RELEASE}.x86_64.rpm
     rpm -ihv $media_dir/packages/ace/$OS_NAME$OS_RELEASE/ace-authd-*.${OS_NAME}${OS_RELEASE}.x86_64.rpm
     rpm -ihv $media_dir/packages/ace/$OS_NAME$OS_RELEASE/ace-ipmitool-*.${OS_NAME}${OS_RELEASE}.x86_64.rpm
     rpm -ihv $media_dir/packages/ace/$OS_NAME$OS_RELEASE/pdsh-*.rpm
     rpm -ihv $media_dir/package/os/$OS_NAME$OS_RELEASE/net-snmp-static-*.x86_64.rpm
     rpm -ihv $media_dir/package/os/$OS_NAME$OS_RELEASE/fuse-static-*.x86_64.rpm
     rpm -ihv $media_dir/package/os/$OS_NAME$OS_RELEASE/openssl-static-*.x86_64.rpm
     rpm -ihv $media_dir/package/os/$OS_NAME$OS_RELEASE/libxml2-static-*.x86_64.rpm
     if [ "$ha_type" == "drbd" ]; then
         rpm -ihv $media_dir/packages/ace/$OS_NAME$OS_RELEASE/drbd-km-$(uname -r | sed "s/-/_/g")*.x86_64.rpm
         rpm -ihv $media_dir/packages/ace/$OS_NAME$OS_RELEASE/drbd-utils-*.${OS_NAME}${OS_RELEASE}.x86_64.rpm
     fi

    wait
    sleep 1
    umount $media_dir
  else
     echo "ACE iso file not found"
     exit 1
  fi
}

db_file=$1
server_type=$2
os_iso_file=$3
ace_iso_file=$4

if [ ! -n "$db_file" -o ! -f "$db_file" -o ! -n "$os_iso_file" -o ! -f "$os_iso_file" -o ! -n "$ace_iso_file" -o ! -f "$ace_iso_file" ]; then
    echo "$(basename $0) <db file> <pri|sec> <os iso file> <ace iso file>"
    exit
fi

#############################
# Setup ACE path
ha_device=$(read_db $db_file drbd_device)
ha_type=$(read_db $db_file ha_storage_type)
if [ "$ha_type" == "drbd" ]; then
    echo "Not yet coding done"
    exit 1
    if [ -d /lib/systemd/system ]; then
        systemctl start drbd
    else
        /etc/init.d/drbd start
    fi
else
    [ -d /ha_cluster ] || mkdir -p /ha_cluster
    mountpoint /ha_cluster >& /dev/null || mount $ha_device /ha_cluster
fi
if [ ! -d /ha_cluster/ace ]; then
    echo "Can not found ACE directory in /ha_cluster"
    exit 1
fi
[ -L /opt/ace ] || ln -s /ha_cluster/ace /opt/ace

#############################
# Install minimal requred ace rpm
ace_rpm $ace_iso_file $ha_type || exit 1

#############################
# Install minimal required rpm files
req_rpms $os_iso_file || exit 1

echo "Setup IP"
#############################
# Read Network Information
if [ "$server_type" == "sec" ]; then
    server_eip=2
else
    server_type="pri"
    server_eip=1
fi

mgmt_net1_dev=$(read_db $db_file mgmt_net1_device) || exit 1
mgmt_net1_ip=$(read_db $db_file mgmt_net1) || exit 1
mgmt_netmask=$(read_db $db_file mgmt_netmask) || exit 1
[ -n "$mgmt_net1_ip" ] || error_exit "I can't find management network device's IP address on this server"
ms_net1_dev=$(read_db $db_file ms_net1_device) || exit 1
ms_net1_ip=$(read_db $db_file network1) || exit 1
ms_netmask=$(read_db $db_file netmask) || exit 1
srv_mgmt_net1_ip=$(net_dev_ip $mgmt_net1_dev) || exit 1
mgmt_net1_ip_addr=$(add_ip $mgmt_net1_ip $server_eip)
mgmt_net1_ha_ip_addr=$(add_ip $mgmt_net1_ip 3)

if [ "$srv_mgmt_net1_ip" != "$mgmt_net1_ip_addr" ]; then
   echo "Looks wrong Management network1($mgmt_net1_dev) IP address($srv_mgmt_net1_ip) from database file's mgmt_net1_ip($mgmt_net1_ip_addr) with input \"$server_type\""
   exit 1
fi

#############################
# Setup Network infromation
echo "Setup Network"
ms_net1_ip_addr=$(add_ip $ms_net1_ip $server_eip)
ms_net1_ha_ip_addr=$(add_ip $ms_net1_ip 3)
srv_ms_net1_ip=$(net_dev_ip $ms_net1_dev) || exit 1
if [ ! -n "$srv_ms_net1_ip" ]; then
    #write_config $ms_net1_dev $ms_net1_ip_addr $ms_netmask .ace
    write_config $ms_net1_dev $ms_net1_ip_addr $ms_netmask 
    
    ifup $ms_net1_dev
else
    if [ "$srv_ms_net1_ip" != "$ms_net1_ip_addr" ]; then
        echo "Looks wrong Primary ethernet network($ms_net1_dev) IP address($srv_ms_net1_ip) from database file's network1($ms_net1_ip_addr) with input \"$server_type\""
        exit 1
    fi
fi

if [ "$(read_db $db_file sub_mgmt)" == "1" -o "$(read_db $db_file sub_mgmt)" == "2" ]; then
    [ "$(ip add list $mgmt_net1_dev | grep secondary | awk '{if($1=="inet") print $2}' | sed "s/\/16//g")" == "$mgmt_net1_ha_ip_addr" ] || \
    ip addr add ${mgmt_net1_ha_ip_addr}/$mgmt_netmask dev $mgmt_net1_dev
fi
[ "$(ip add list $ms_net1_dev | grep secondary | awk '{if($1=="inet") print $2}' | sed "s/\/16//g")" == "$ms_net1_ha_ip_addr" ] || \
ip addr add ${ms_net1_ha_ip_addr}/$ms_netmask dev $ms_net1_dev

#############################
# Start ace
cat << EOF > /etc/init.d/ace
#!/bin/sh
#
# chkconfig: 2345 35 85
# description: ace manages a high performance computing cluster
# config: /etc/sysconfig/ace
#

# source function library
. /etc/rc.d/init.d/functions

# source configuration
#. /etc/sysconfig/ace
. /opt/ace/sbin/libfunc.sh

ACE_CLUSTER_FS="/ha_cluster"
ACE_CLUSTER_STORAGE="/ha_cluster/storage/"
ACE_DEBUG_LOG_FILE=/tmp/ace_debug.log
ACE_INIT_LOG_FILE=/tmp/ace_init.log

RETVAL=0
aced_debug=1

error_exit() {
   echo
   echo "Error: \$*"
   echo
   logger "\$(basename \$0): Error: \$*"
   exit 1
}

_killall() {
   local app chk
   app=\$1
   [ -n "\$app" ] || return 0
   for i in \$(seq 1 10); do
       if [ "\$(awk -v name=\$app '{if(\$2==name) printf "run"}' /proc/*/status 2>/dev/null)" == "run" ]; then
          echo "kill(\$i) \$app"
          killall -9 \$app
       else
           return 0
       fi
       sleep 2
   done
   if [ "\$(awk -v name=\$app '{if(\$2==name) printf "run"}' /proc/*/status 2>/dev/null)" == "run" ]; then
      echo "Can not kill \$app"
      return 0
   fi
}

_umount() {
   local dest
   dest=\$1
   [ -n "\$(awk -v dir=\$dest '{if(\$2==dir) print \$1}' /proc/mounts)" ] && ( umount \$dest >& /dev/null || umount -l \$dest >& /dev/null )
}

_read_config(){
  local func num
  func=\$1
  [ -f /opt/ace/etc/database.cfg ] || return 1
  awk -v find=\$func '{ if (find == \$1) print \$2}' /opt/ace/etc/database.cfg | sed -e "s/'//g"
}

_wait_net(){
   local i lip network
   network=\$1
   lip=\$2
   echo "Check network \$network"
   for i in \$(seq 1 30); do
     ip addr | grep \$( echo \$(_read_config \$network) | awk -v lip=\$lip -F. '{printf "%d.%d.%d.%d",\$1,\$2,\$3,lip}') > /dev/null 2>&1 && return 0
     echo -n "."
     sleep 2
   done
   return 1
}

_wait_nfs(){
   local i lip network nfs_server_ip
#   [ "\$(_read_config standalone_ms)" = "1" ] && return 0
   network=\$1
   lip=\$2

   ps -ef |grep nfsd >& /dev/null || _daemonctl start nfs

   nfs_server_ip=\$(echo \$(_read_config \$network) | awk -v lip=\$lip -F. '{printf "%d.%d.%d.%d",\$1,\$2,\$3,lip}')
   echo "Check NFS export"
   for i in \$(seq 1 10); do
     showmount -e \$nfs_server_ip |grep ha_cluster > /dev/null 2>&1  && return 0
     echo -n "."
     sleep 2
   done
   killall -9 nfsd
   sleep 2
   _daemonctl start nfs
   for i in \$(seq 1 10); do
     showmount -e \$nfs_server_ip |grep ha_cluster > /dev/null 2>&1  && return 0
     echo -n "."
     sleep 2
   done
   return 1
}

_wait_ib(){
   echo -n "Check IB device :"
   for i in \$(seq 1 30); do
     #ibstat | grep "State: Active" > /dev/null 2>&1 && return 0
     cat \$(awk '{if(\$1=="IB" || \$1=="InfiniBand") print FILENAME}' /sys/class/infiniband/*/ports/*/link_layer | sed "s/link_layer/state/g") | grep -i "ACTIVE\$" >& /dev/null  && return 0
     echo -n "."
     sleep 2
   done
   return 1
}

start() {
    logger "ACE Service Start"
    if [ -d /acefs/self ]; then
       echo "working"
       return 0
    fi

    [ -f /opt/ace/etc/database.cfg ] || error_exit "ACE database not found"

    ETH_ONLY=\$(_read_config identify_servers)
    [ -n "\$ETH_ONLY" ] || ETH_ONLY=1

    if [ "\$(_read_config standalone_ms)" == "0" ]; then
        _wait_net network1 3 ||  error_exit "HA-network1(\$(_read_config ms_net1_device)) not found"
        if [ -n "\$(_read_config network2)" ]; then
           _wait_net network2 3 ||  error_exit "HA-network2(\$(_read_config ms_net2_device)) not found"
         fi
        _wait_nfs network1 3 || error_exit "NFS export not found"
    else
        _wait_net network1 1 ||  error_exit "Network1(\$(_read_config ms_net1_device)) not found"
        if [ -n "\$(_read_config network2)" ]; then
          _wait_net network2 1 ||  error_exit "Network2(\$(_read_config ms_net2_device)) not found"
        fi
        _wait_nfs network1 1 || error_exit "NFS export not found"
    fi
    if [ "\$ETH_ONLY" == "1" ]; then
         _wait_ib || error_exit "IB is not ready"
    fi

    # make sure cluster file system is available
    if [ ! -d "\$ACE_CLUSTER_STORAGE" ]; then
        echo "Error: Cluster storage \$ACE_CLUSTER_STORAGE is not available"
        logger "\$(basename \$0): Error: Cluster storage \$ACE_CLUSTER_STORAGE is not available"
        return 1
    fi

    # create tftp remapping directory
    if [ ! -f /etc/tftp.remap ]; then
        echo "
#
# tftpd remap file for ACE
#

rg     ^[^/]   /ha_cluster/tftpboot/\0     # convert non-absolute filenames

" > /etc/tftp.remap
    fi

    #create dhcpd.hosts for "add include file for DHCP customizations" of ace_fuse
    [ -f /opt/ace/etc/dhcpd.hosts ] || touch /opt/ace/etc/dhcpd.hosts

    # stop below daemons, if still running...
    job_sch=\$(_read_config job_scheduler)
    [ -n "\$job_sch" ] || job_sch=1
    if [ "\$job_sch" == "1" ]; then
        job_sch_daemon="
            ace_sged.sh
            ace_sged
        "
    fi
    for kill_daemon in \
        ace_syncd.sh    \
        ace_syncd       \
        \$job_sch_daemon \
        ace_ethstatd.sh \
        ace_cluster_mon.sh \
        ace_ethstatd    \
        ace_ibstatd.sh  \
        ace_ibstatd     \
        ace-authd      \
    ; do
       _killall \$kill_daemon
    done

    # stop dhcpd
    _daemonctl stop dhcpd

    # Start ace-authd
    if [ -f /etc/rc.d/init.d/ace-authd ]; then
        /etc/rc.d/init.d/ace-authd start
    elif [ -f /opt/ace-authd/etc/rc.d/init.d/ace-authd ]; then
        /opt/ace-authd/etc/rc.d/init.d/ace-authd start
    fi

    # starting COW block devices
    #makecowdevs
    #modprobe cowloop maxcows=254
    if [ -d /lib/systemd/system ]; then
       loop_dev=\$(lsmod |awk '{if(\$1=="loop") print \$3}')
       if [ ! -n "\$loop_dev" ]; then
          modprobe loop max_loop=1024
       elif [ "\$loop_dev" == "0" ]; then
          if (( "\$(cat /sys/module/loop/parameters/max_loop)" < "1024" )); then
            modprobe -r loop
            modprobe loop max_loop=1024
          fi
       fi
    fi
    loop_dev_num=\$(ls /dev/loop* | wc -l)
    if (( \$loop_dev_num < 256 )); then
        logger -s "Please update /boot/grub/grub.conf file for increase Loop device number to max. This mgmt node have \$loop_dev_num devices."
    fi

    # start GNBD
    gnbd_serv -n

    #unmount pxelinux.cfg
    _umount /ha_cluster/tftpboot/ace/pxelinux.cfg

    # start ACE daemon
    cd /root
    export ACE_DEBUG=1
    ulimit -c unlimited         # allow core files
#    ulimit -s 65536             # 64 MB stack size
    ulimit -s unlimited         # unlimited stack size
    ulimit -n 20000             # 20,000 open files
    ulimit -a > /tmp/ace-limits.log
    export ACE_LOG_KEEP_VERSIONS=20
    export ACE_LOG_WRAP_COUNT=100000
    #export ACE_DEBUG_LOG_WRAP_COUNT=10000
    #ACE_DEBUG_LOG_FILE=/tmp/ace_debug.log /opt/ace/bin/aced --update &
    #/opt/ace/bin/aced --update >& /tmp/ace_debug.log &

#
#   aced Debug code
#
    echo "\$(date +"%Y-%m-%d %H:%M:%S") : aced start" > \$ACE_INIT_LOG_FILE
    if [ "\$aced_debug" == "1" ]; then
      if [ "\$(nmap localhost -p 9777 2> /dev/null | grep 9777 | awk '{print \$2}')" == "open" ]; then
        echo "warning : 9777 " >> \$ACE_INIT_LOG_FILE
        netstat | grep 9777 >> \$ACE_INIT_LOG_FILE
        nmap localhost -p 9777 >> \$ACE_INIT_LOG_FILE
      fi
    fi

    #/opt/ace/bin/aced --update >& /tmp/ace_output.log &
    if (( \$([ -d /acefs ] && ls /acefs | wc -l || echo 0) > 0)); then
        tmp_acefs=\$(mktemp -u /tmp/acefs.XXXXX)
        mkdir -p \$tmp_acefs
        mv /acefs/* \$tmp_acefs
        echo "Moved /acefs directory to \$tmp_acefs directory"
    fi
    /opt/ace/bin/aced --update >> \$ACE_INIT_LOG_FILE 2>&1 &

#
#   aced Debug code
#
    if [ "\$aced_debug" == "1" ]; then
      for i in \$(seq 1 60); do
        echo "/acefs(\$i) is \$(ls /acefs)" >> \$ACE_INIT_LOG_FILE
        [ -d /acefs/self ] && break
        sleep 1
      done
      if [ ! -d /acefs/self ]; then
        echo "Not found /acefs/self" >> \$ACE_INIT_LOG_FILE
        ls /acefs >> \$ACE_INIT_LOG_FILE
        echo "restart aced daemon" >> \$ACE_INIT_LOG_FILE
        killall -9 aced
        sleep 5
        /opt/ace/bin/aced --update >> \$ACE_INIT_LOG_FILE 2>&1 &
        sleep 7
        ls /acefs >> \$ACE_INIT_LOG_FILE
      fi
    fi

    unset ACE_DEBUG_LOG_FILE

    # (re)start ace_ibstatd/ace_ethstatd
    if [ "\$ETH_ONLY" == "2" ]; then
       MANUFACTURING=\$(_read_config manufacturing)
       if [ "\$MANUFACTURING" == "1" ]; then
          /opt/ace/bin/ace_ethstatd_manufacturing.sh >& /tmp/ace_ethstatd.log &
       else
          /opt/ace/bin/ace_ethstatd.sh >& /tmp/ace_ethstatd.log &
       fi
    elif [ "\$ETH_ONLY" == "1" ]; then
       /opt/ace/bin/ace_ibstatd.sh >& /tmp/ace_ibstatd.log &
    fi

    # (re)start ace_sged
    job_sch=\$(_read_config job_scheduler)
    [ -n "\$job_sch" ] || job_sch=1
    if [ "\$job_sch" == "1" ]; then
        /opt/ace/bin/ace_sged.sh >& /tmp/ace_sged.log &
    #elif [ "\$job_sch" == "2" ]; then
    #   slurm
    #elif [ "\$job_sch" == "3" ]; then
    #   torque/maui
    #elif [ "\$job_sch" == "4" ]; then
    #   pbs
    fi

    # (re)start ace_syncd
    /opt/ace/bin/ace_syncd.sh >& /tmp/ace_syncd.log &

    if [ -f /etc/init.d/ace_emsd ]; then
       # Create symlink if it doesn't exist
       ln -nfs / /.ace

        # Create pipe for ace_emsd syslog facilty
	syslog_pipe=/tmp/ace_syslog
	if [ ! -p \$syslog_pipe ]; then
	    rm -f \$syslog_pipe
	    mkfifo -m 600 \$syslog_pipe
	fi

        # Restart ace_emsd
	_daemonctl restart ace_emsd
    fi

    #patch for over rhel5.3
    [ -d /var/lock/subsys ] && touch /var/lock/subsys/ace

    # For RHEL 7.1
    if [ -d /etc/systemd/system ]; then
        _daemonctl start dhcpd
        _daemonctl start xinetd
    fi

    # (re)start ace_cluster_monitor daemon
    /opt/ace/bin/ace_cluster_mon.sh &

    start_apps
    return 0
}

stop() {
    logger "ACE Service Stop"
    stop_apps

    if [ -f /proc/drbd ]; then
        [ "\$(drbdadm state all 2>/dev/null | awk -F\/ '{print \$1}')" == "Secondary" ] && return 0
    fi

    #umount pxelinux.cfg
    _umount /ha_cluster/tftpboot/ace/pxelinux.cfg

    # stop ace_emsd
    [ -f /etc/init.d/ace_emsd ] && /etc/init.d/ace_emsd stop

    # stop ace-authd
    [ -f /etc/rc.d/init.d/ace-authd ] && /etc/rc.d/init.d/ace-authd stop

    # stop below daemonsif it's still running...
    job_sch=\$(_read_config job_scheduler)
    [ -n "\$job_sch" ] || job_sch=1
    if [ "\$job_sch" == "1" ]; then
        job_sch_daemon="
            ace_sged.sh
            ace_sged
        "
    fi
    for kill_daemon in \
        ace_syncd.sh    \
        ace_syncd       \
        \$job_sch_daemon \
        ace_ethstatd.sh \
        ace_ethstatd_manufacturing.sh \
        ace_ethstatd    \
        ace_ibstatd.sh  \
        ace_ibstatd     \
        ace-authd      \
    ; do
       _killall \$kill_daemon
    done

    # stop dhcpd
    _daemonctl stop dhcpd
    # Fix RHEL7.1
    _daemonctl stop xinetd

    if ps -e | grep ' aced\$' >& /dev/null; then
        /opt/ace/bin/ace sync
        sleep 2
    fi

    # shutdown ACE daemon
    killall -9 aced

    sleep 2

    # unexport network block device daemons
    sync
    gnbd_export -O -R

    # kill gnbd_serv
    gnbd_serv -k

    # stopping COW devices
    . /opt/ace/sbin/cluster_functions
    cow_close_all

    # unmount ace file system
    _umount /acefs

    #patch for rhel5.3
    [ -d /var/lock/subsys ] && /bin/rm -f /var/lock/subsys/ace

    return 0
}

status_chk() {
    if status aced > /dev/null ; then
       echo "OK"
       return 0
    else
       echo "DOWN"
       return 3
    fi
}

start_apps() {
  [ -d /acefs/appfs/ace_apps ] || return 0
  [ -n "\$(awk '{if(\$2=="/appfs") printf "ok"}' /proc/mounts)" ] && return 0
  lsmod |grep gnbd >& /dev/null || modprobe gnbd
  [ -d /appfs ] || mkdir -p /appfs
  for ((ii=0;ii<30;ii++)); do
      ls /dev/mapper/ace_apps* >& /dev/null && break
      echo "Wait ace_apps device..."
      sleep 1
  done
  if ! ls /dev/mapper/ace_apps* >& /dev/null; then
     logger "ace_apps device not found. Plase check \"ace appfs\""
     return 1
  fi
  mount -t appfs -o ro ace_apps /appfs || logger "ace_apps mount fail"
}

stop_apps() {
  if [ -n "\$(awk '{if(\$2=="/appfs") printf "ok"}' /proc/mounts)" ]; then
     if [ -f /sbin/umount.appfs ]; then
         umount.appfs /appfs
     else
         umount /appfs || umount -l /appfs
     fi
  fi
}

case "\$1" in
    start)      start; RETVAL=\$?;;
    stop)       stop; RETVAL=\$?;;
    status)     status_chk; RETVAL=\$?;;
    restart)    stop; sleep 5; start; RETVAL=\$?;;
    *)          echo "Usage: \$0 {start|stop|restart|status}"; exit 1;;
esac

exit \$RETVAL

# \$Id: ace.init.d 3531 2016-12-02 20:05:37Z kage \$
EOF

chmod +x /etc/init.d/ace
cp -a /opt/ace/share/ace_emsd.init.d /etc/init.d/ace_emsd
echo "/ha_cluster *(rw,sync,no_root_squash,fsid=15531)" > /etc/exports
echo "# default: off
# Advanced Cluster Engine tftp configuration file
#
#     Do not edit. This file is created by ACE during startup.
#
# description: The tftp server serves files using the trivial file transfer
#       protocol.  The tftp protocol is often used to boot diskless
#       workstations, download configuration files to network-aware printers,
#       and to start the installation process for some operating systems.
service tftp
{
        disable = no
        socket_type             = dgram
        protocol                = udp
        wait                    = yes
        user                    = root
        server                  = /usr/sbin/in.tftpd
        server_args             = -v -v -v -v -v -v -v -u tftp -s /ha_cluster/tftpboot/
        per_source              = 11
        cps                     = 1000 2
        flags                   = IPv4
}" > /etc/xinetd.d/tftp

if ! id tftp >& /dev/null; then
    echo "tftp:x:500:500::/home/tftp:/bin/bash" >> /etc/passwd
    echo "tftp:x:500:" >> /etc/group
    echo "tftp:!!:17156:0:99999:7:::" >> /etc/shadow
fi
if ! id dhcpd >& /dev/null; then
    echo "dhcpd:x:177:177:DHCP server:/:/sbin/nologin" >> /etc/passwd
    echo "dhcpd:x:501:" >> /etc/group
    echo "dhcpd:!!:17156:0:99999:7:::" >> /etc/shadow
fi
if ! id hacluster >& /dev/null; then
    echo "hacluster:x:189:189:cluster user:/home/hacluster:/sbin/nologin" >> /etc/passwd
    echo "haclient:x:189:" >> /etc/group
    echo "hacluster:$6$dJt8pj0d$BGsJZHJD7leaiLZ4HUvRlo.8moeOUqRC01OH5y55ecyfeIgJxLplhvkSkve5zLMUIoNKTi2vjOa50GvKak6HY0:17156::::::" >> /etc/shadow
fi
if ! id sshd >& /dev/null; then
    echo "sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin" >> /etc/passwd
    echo "sshd:x:74:" >> /etc/group
    echo "sshd:!!:17142::::::" >> /etc/shadow
fi

if [ -d /lib/systemd/system ]; then
    systemctl stop xinetd
    systemctl start xinetd
    systemctl stop nfs
    systemctl start nfs
else
    /etc/init.d/xinetd stop
    /etc/init.d/xinetd start
    /etc/init.d/nfs stop
    /etc/init.d/nfs start
fi
ln -s /opt/ace/bin/ace /usr/bin/ace
ln -s /opt/ace/sbin/iscb_cmd.sh /usr/bin/ace_console
if [ -d /etc/dhcp ]; then
    ln -sf /etc/dhcpd.conf /etc/dhcp/dhcpd.conf
fi 

systemctl daemon-reload
/etc/init.d/ace start

while [ 1 ]; do
    sleep 5
    [ -d /acefs/global/mount/root/.ssh ] && break
done
sleep 5
[ -d /root/.ssh ] && mv /root/.ssh /root/dot.ssh
cp -a /acefs/global/mount/root/.ssh /root/.ssh
chmod 700 /root/.ssh
cp -a /acefs/global/mount/etc/ssh/ssh_known_hosts /etc/ssh
