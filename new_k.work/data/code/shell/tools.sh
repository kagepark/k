# This is "tools.sh", it contains a function what gets called in order to
# download any required software packages.  Most initial versions will be
# simply returning 0, but in the future the tools package might be expanded.

source /root/pghandler.sh

TARGET_SITE="${NAPXE}"
TARGET_PATH="/cburn_staging"
M2_PATH="${TARGET_PATH}/m2/latest"
RDIR="${SYS_DIR}"
M2LOGTARGET="${RDIR}/m2.log"

source /root/micron_m2_fw.sh
source /root/intel_m2_fw.sh

function get_all()
{
	#Here we would have wget operations.... But we don't actually need to do anything.

	
	# wget "http://${TARGET_SITE}${M2_PATH}/issdcm.x86_64.rpm" -O /root/issdcm.x86_64.rpm &> /dev/null
	# if [ $? -ne 0 ]; then
	# 	echo "ERROR: Unable to acquire issdcm" | tee -a /dev/tty0 $M2LOGTARGET
	# 	return -1
	# fi

	# rpm -i /root/issdcm.x86_64.rpm
	# if [ $? -ne 0 ]; then
	# 	echo "ERROR: Unable to install issdcm" | tee -a /dev/tty0 $M2LOGTARGET
	# 	return -2
	# fi

	# wget "http://${TARGET_SITE}${M2_PATH}/msecli" -O /root/msecli &> /dev/null
	# if [ $? -ne 0 ]; then
	# 	echo "ERROR: Unable to acquire msecli" | tee -a /dev/tty0 $M2LOGTARGET
	# 	return -1
	# fi

	wget "http://${TARGET_SITE}${M2_PATH}/${INTEL_FIRMWARE}" -O /root/${INTEL_FIRMWARE} &> /dev/null
	if [ $? -ne 0 ]; then
		echo "ERROR: Unable to acquire intel firmware" | tee -a /dev/tty0 $M2LOGTARGET
		return -1
	fi

	wget "http://${TARGET_SITE}${M2_PATH}/${MICRON_FIRMWARE}" -O /root/${MICRON_FIRMWARE} &> /dev/null
	if [ $? -ne 0 ]; then
		echo "ERROR: Unable to acquire micron firmware" | tee -a /dev/tty0 $M2LOGTARGET
		return -1
	fi

	chmod +x /root/msecli

	return 0
}