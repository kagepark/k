<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
// http URL can support /ajax/check/
// If call /ajax/check/ URL then run and response
// def check(request):
//     code here
//    rc['ipmi_ip']='aaaa'
//    rc['bbb']='ccc'
//    return JsonResponse(rc)
function CheckEnv(tag_name) {
   //<input type="text" name="ipmi_ip" size="15" placeholder="IPMI/BMC IP Address" required id="id_ipmi_ip">
   //<button type="button" onclick="CheckEnv("id_ipmi_ip");">Check</button>
   const xhttp = new XMLHttpRequest();
   var ipmi_ip = document.getElementById(tag_name).value;
   if (ipmi_ip == '') {
       return
   }
   $.ajax({
     url: "/ajax/check/",
     type: "POST",
     data: {"ipmi_ip": ipmi_ip, 'csrfmiddlewaretoken': '{{ csrf_token }}' },
     success: function (json) {
         if ( json['error'] != null ) { // if found important data then display that data
            alert(json['error']);
            return
         };

         alert(' ** Found data **\n'+json); // display all data
         if ( 'mb_name' in json ) { // got new data then overwrite to element
             document.getElementById('id_mb_name').value=json['mb_name'];
         };
         if ( 'ipmi_pass' in json ) { // got new data then overwrite to element
             document.getElementById('id_ipmi_pass').value=json['ipmi_pass'];
         };
     }
   });
}

</script>
