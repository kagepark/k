#!/usr/bin/python3
# -*- coding: utf-8 -*-
##########################################################################
# Kage Park 
# AutoTester function collection
# ISO Verify function
##########################################################################
version="2.1"
import os
import time
import re

def find_cdrom_dev():
    if os.path.isdir('/sys/block') is False:
        return
    for r, d, f in os.walk('/sys/block'):
        for dd in d:
            for rrr,ddd,fff in os.walk(os.path.join(r,dd)):
                if 'removable' in fff:
                    with open('{0}/removable'.format(rrr),'r') as fp:
                        removable=fp.read()
                    if '1' in removable:
                        if os.path.isfile('{0}/device/model'.format(rrr)):
                            with open('{0}/device/model'.format(rrr),'r') as fpp:
                                model=fpp.read()
                            if 'CDROM' in model:
                                return '/dev/{0}'.format(dd)

def get_cdrom_uid(dev):
    rc=k.rshell('''blkid {0}'''.format(dev))
    if rc[0] == 0:
        rc_a=rc[1].split(' ')
        return '{0}_{1}'.format(re.compile('UUID="(\w.*)"').findall(rc_a[1])[0],re.compile('LABEL="(\w.*)"').findall(rc_a[2])[0])
    return

def iso_verify(cdrom_dev=None):
    print('Load kernel module for CDROM')
    with open('iso_verify.log','a') as f:
        f.write('Load kernel module for CDROM\n')
    rc=k.load_kmod(['sr_mod','usb_storage','libata','ata_piix','ata_generic','cdrom'])
    if rc is False:
        print('Fail for Load CDROM kernel module!!!')
        with open('iso_verify.log','a') as f:
            f.write('Fail for Load CDROM kernel module!!!\n')
        return False
        
    time.sleep(5)
    for i in range(0,30):
        cdrom_dev=find_cdrom_dev()
        print('finding cdrom_dev:',cdrom_dev)
        with open('iso_verify.log','a') as f:
            f.write('finding cdrom_dev: {0}\n'.format(cdrom_dev))
        if cdrom_dev is not None:
            break
        time.sleep(10)
    if cdrom_dev is not None:
        cdrom_uid=get_cdrom_uid(cdrom_dev)
        print('cdrom_uid:',cdrom_uid)
        with open('iso_verify.log','a') as f:
            f.write('cdrom_uid: {0}\n'.format(cdrom_uid))
        data={'tmp':'{0}'.format(cdrom_uid)}
    else:
        data={'tmp':'Not Found Device'}
        with open('iso_verify.log','a') as f:
            f.write('Not Found Device\n')
    return data
