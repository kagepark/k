"""
Interfaces with portal.adtpulse.com.
For more details about this platform, please refer to the documentation at
https://home-assistant.io/components/alarm_control_panel.adtpulse/
"""

import json
import os
import pickle
# python3 case, it need install beautifulsoup4 package
from bs4 import BeautifulSoup
from dateutil.parser import parse
import requests
from requests.auth import AuthBase
import voluptuous as vol
import time

#import logging
#_LOGGER = logging.getLogger(__name__)

class Adtpulse(object):
    """
    Access to ADT Pulse partners and accounts.
    This class is used to interface with the options available through
    portal.adtpulse.com. The basic functions of checking system status and arming
    and disarming the system are possible.
    """

    """
    ADT Pulse constants
    """
    def __init__(self,username=None,password=None):
       self.COOKIE_PATH = '/tmp/adtpulse_cookies.pickle'
       self.ADTPULSE_JSON_PREAMBLE_SIZE = 18
       self.DEFAULT_LOCALE = 'en_US'
       self.HTML_PARSER = 'html.parser'
       self.ERROR_FIND_TAG = 'div'
       self.ERROR_FIND_ATTR = {'id': 'warnMsgContents'}
       self.VALUE_ATTR = 'value'
       self.ATTRIBUTION = 'Information provided by portal.adtpulse.com'
       self.ID=username
       self.PASS=password
       # ADT Pulse baseURL
       self.ADTPULSE_DOMAIN = 'https://portal.adtpulse.com'

       self.BTN_DISARM = ('id', 'ctl00_phBody_butDisarm')
       self.BTN_ARM_STAY = ('id', 'ctl00_phBody_butArmStay', 'ctl00_phBody_ArmingStateWidget_btnArmOptionStay')
       self.BTN_ARM_AWAY = ('id', 'ctl00_phBody_butArmAway', 'ctl00_phBody_ArmingStateWidget_btnArmOptionAway')

    """
    Determine the current code version used on portal.adtpulse.com
    """
    def adtpulse_version(self):
        """Determine current ADT Pulse version"""
        resp = requests.get(self.ADTPULSE_DOMAIN)
        parsed = BeautifulSoup(resp.content, self.HTML_PARSER)
        adtpulse_script = parsed.find_all('script', type='text/javascript')[4].string
        if "=" in adtpulse_script:
            param, value = adtpulse_script.split("=",1)
            adtpulse_version = value
            adtpulse_version = adtpulse_version[1:-2]
            return(adtpulse_version)


    def _save_cookies(self,requests_cookiejar, filename):
        """Save cookies to a file."""
        with open(filename, 'wb') as handle:
            pickle.dump(requests_cookiejar, handle)

    def _load_cookies(self,filename):
        """Load cookies from a file."""
        with open(filename, 'rb') as handle:
            return pickle.load(handle)

    def _parsed_date(self, date):
        """Parse a date."""
        return str(parse(date).date()) if date else ''

    def get_session(self, username=None, password=None, force=None):
        """Get ADTPULSE HTTP session."""
        session = requests.session()
        if os.path.exists(self.COOKIE_PATH) and force is None:
            session.cookies = self._load_cookies(self.COOKIE_PATH)
        else:
            if username is not None and password is not None:
               self._login(session, username, password)
            else:
               self._login(session, self.ID, self.PASS)
        return session

    def _login(self, session, username, password):
        """Login to ADTPULSE."""
        if os.path.exists(self.COOKIE_PATH):
            os.remove(self.COOKIE_PATH)
            print('adt delete {0}'.format(self.COOKIE_PATH))

        adt_ver=self.adtpulse_version()
        resp = session.post(self.ADTPULSE_DOMAIN +str(adt_ver) + '/access/signin.jsp', {
            'usernameForm': username,
            'passwordForm': password,
        })
#        parsed = BeautifulSoup(resp.text, self.HTML_PARSER)
#        print(parsed)
#        error = parsed.find(self.ERROR_FIND_TAG, self.ERROR_FIND_ATTR).string
#        if error:
#            raise LoginException(error.strip())
        self._save_cookies(session.cookies, self.COOKIE_PATH)

    #status is working good
    def status(self,session):
       resp = session.get(self.ADTPULSE_DOMAIN + self.adtpulse_version() + '/summary/summary.jsp')
       parsed = BeautifulSoup(resp.content, self.HTML_PARSER)
       alarm_status = parsed.find('span', 'p_boldNormalTextLarge')
       if alarm_status is None:
           return
       for string in alarm_status.strings:
           if "." in string:
               param, value = string.split(".",1)
           adtpulse_alarmstatus = param
           state = adtpulse_alarmstatus
           return state
    
   # stay_arm is not working
   # Need how to make away alarm and disalarm
    def stay_arm(self,session):
       #payload={'security_button_1':'Arm Away','U2VjdXJpdHkgUGFuZWw=':'Away','networkid':'00603503fd70','partner':'adt'}
       #resp = session.post(self.ADTPULSE_DOMAIN + self.adtpulse_version() + '/myaccount/myaccountaction.jsp',data=payload)
       #resp = session.post(self.ADTPULSE_DOMAIN + self.adtpulse_version() + '/summary/summary.jsp',data=payload)
       #../quickcontrol/serv/ChangeVariableServ?rtn=xml&fi='+encodeURIComponent(instance)+'&vn=arm-state&u='+encodeURIComponent(units)+'&ft=security-panel&sat=7f6a47b4-d3ec-4a2e-b4d6-e3faeef726cd&value=' + encodeURIComponent(newstate), 'accelerate'
#if(!confirm(decodeURIComponent(icg.string('STR.ORB.SENSORS_OPEN_OR_MOTION', '####').replace("####", encodeURIComponent("" + x.value))).replace(/<br\s*\/>/g, '\n')))
       resp = session.post(self.ADTPULSE_DOMAIN + self.adtpulse_version() + '''/quickcontrol/serv/ChangeVariableServ?rtn=xml&fi=U2VjdXJpdHkgUGFuZWw=&vn=arm-state&u=Disarmed||Stay||Away||Disarmed with Alarm||Night Stay&ft=security-panel&sat=7f6a47b4-d3ec-4a2e-b4d6-e3faeef726cd&value=Away''')
       print(resp.text)

if __name__ == "__main__":
     adtp=Adtpulse()
     session=adtp.get_session('<id>','<pass>')
     # get status
     aa=adtp.status(session)
     if aa is None:
        session=adtp.get_session('<id>','<pass>')
        aa=adtp.status(session)
     print("Alarm status: {0}".format(aa))
#     adtp.stay_arm(session)
