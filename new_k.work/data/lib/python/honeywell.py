#!/usr/bin/python

# By Brad Goodman
# http://www.bradgoodman.com/
# brad@bradgoodman.com

############################ End settings ############################
#python2
import urllib2
#python3
#import urllib.request
import urllib
import json
import datetime
import re
import time
import math
import base64
import time
#python2.x
import httplib
#python3.x
#import http.client
import sys
import getopt
import os
import stat
import subprocess
import string

class Honeywell:
   def __init__(self,Id=None,Pass=None,Device_id=None,k_class=None,k_net=None):
       self.AUTH="https://mytotalconnectcomfort.com/portal"
       self.cookiere=re.compile('\s*([^=]+)\s*=\s*([^;]*)\s*')
       self.Id=Id
       self.Pass=Pass
       self.Device_id=Device_id
       self.k_class=k_class
       self.k_net=k_net
       if self.k_class is not None:
           self.k_class.log('Module load : Thermostat(Honeywell)')

   def client_cookies(self,cookiestr,container):
     if not container: container={}
     toks=re.split(';|,',cookiestr)
     for t in toks:
       k=None
       v=None
       m=self.cookiere.search(t)
       if m:
         k=m.group(1)
         v=m.group(2)
         if (k in ['path','Path','HttpOnly']):
           k=None
           v=None
       if k: 
         #print k,v
         container[k]=v
     return container

   def export_cookiejar(self, jar):
     s=""
     for x in jar:
       s+='%s=%s;' % (x,jar[x])
     return s

   def get_login(self):
       if self.k_net.check_network() is False:
          return

       cookiejar=None
       #print "Run at ",datetime.datetime.now()
       headers={"Content-Type":"application/x-www-form-urlencoded",
            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding":"sdch",
            "Host":"mytotalconnectcomfort.com",
            "DNT":"1",
            "Origin":"https://mytotalconnectcomfort.com/portal",
            "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36"
                }
       try:
          conn = httplib.HTTPSConnection("mytotalconnectcomfort.com")
       except:
          return
       try:
          conn.request("GET", "/portal/",None,headers)
       except:
          return
       try:
          r0 = conn.getresponse()
       except:
          return
       #print r0.status, r0.reason
    
       for x in r0.getheaders():
         (n,v) = x
         #print "R0 HEADER",n,v
         if (n.lower() == "set-cookie"): 
           cookiejar=self.client_cookies(v,cookiejar)
       #cookiejar = r0.getheader("Set-Cookie")
       location = r0.getheader("Location")

       retries=5
       params=urllib.urlencode({"timeOffset":"240",
        "UserName":self.Id,
        "Password":self.Pass,
        "RememberMe":"false"})
       #print params
       newcookie=self.export_cookiejar(cookiejar)
       #print "Cookiejar now",newcookie
       headers={"Content-Type":"application/x-www-form-urlencoded",
            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Accept-Encoding":"sdch",
            "Host":"mytotalconnectcomfort.com",
            "DNT":"1",
            "Origin":"https://mytotalconnectcomfort.com/portal/",
            "Cookie":newcookie,
            "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36"
        }
       conn = httplib.HTTPSConnection("mytotalconnectcomfort.com")
       conn.request("POST", "/portal/",params,headers)
       try:
          r1 = conn.getresponse()
          #print r1.status, r1.reason
          return r1
       except:
          return
    
   # hold_time : min, value : F , r1 : session, action : heat, cool, status
   def do_set(self, r1, action,value=None, hold_time=0):
       if self.k_net.check_network() is False or r1 is None:
          return

       cookiejar=None
       for x in r1.getheaders():
         (n,v) = x
         #print "GOT2 HEADER",n,v
         if (n.lower() == "set-cookie"): 
           cookiejar=self.client_cookies(v,cookiejar)
       cookie=self.export_cookiejar(cookiejar)
       #print "Cookiejar now",cookie
       location = r1.getheader("Location")

       if ((location == None) or (r1.status != 302)):
           #raise BaseException("Login fail" )
           if self.k_class is not None:
               self.k_class.dbg(3,"ErrorNever got redirect on initial login  status={0} {1}".format(r1.status,r1.reason))
           else:
               print("ErrorNever got redirect on initial login  status={0} {1}".format(r1.status,r1.reason))
           return


      # Skip second query - just go directly to our device_id, rather than letting it
       # redirect us to it. 

       code=str(self.Device_id)

       t = datetime.datetime.now()
       utc_seconds = (time.mktime(t.timetuple()))
       utc_seconds = int(utc_seconds*1000)
       #print "Code ",code

       location="/portal/Device/CheckDataSession/"+code+"?_="+str(utc_seconds)
       #print "THIRD"
       headers={
            "Accept":"*/*",
            "DNT":"1",
            #"Accept-Encoding":"gzip,deflate,sdch",
            "Accept-Encoding":"plain",
            "Cache-Control":"max-age=0",
            "Accept-Language":"en-US,en,q=0.8",
            "Connection":"keep-alive",
            "Host":"mytotalconnectcomfort.com",
            "Referer":"https://mytotalconnectcomfort.com/portal/",
            "X-Requested-With":"XMLHttpRequest",
            "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36",
            "Cookie":cookie
        }
       try:
          conn = httplib.HTTPSConnection("mytotalconnectcomfort.com")
          #conn.set_debuglevel(999);
          #print "LOCATION R3 is",location
          conn.request("GET", location,None,headers)
          r3 = conn.getresponse()
       except:
          return

       if (r3.status != 200):
         if self.k_class is not None:
             self.k_class.dbg(3,"Error Didn't get 200 status on R3 status={0} {1}".format(r3.status,r3.reason))
         else:
             print("Error Didn't get 200 status on R3 status={0} {1}".format(r3.status,r3.reason))
         return

       # Print thermostat information returned
       if (action == "status"):
           rawdata=r3.read()
           j = json.loads(rawdata)
           #print "R3 Dump"
           #print json.dumps(j,indent=2)
           #print json.dumps(j,sort_keys=True,indent=4, separators=(',', ': '))
           #print "Success:",j['success']
           #print "Live",j['deviceLive']
           if j['latestData']['uiData']["SystemSwitchPosition"] == 1:
               system='heat'
           elif j['latestData']['uiData']["SystemSwitchPosition"] == 3:
               system='cool'
           else:
               system='unknown'
           hwth={'in_temp':j['latestData']['uiData']["DispTemperature"],
                 'in_hum':j['latestData']['uiData']["IndoorHumidity"],
                 'cool':j['latestData']['uiData']["CoolSetpoint"],
                 'heat':j['latestData']['uiData']["HeatSetpoint"],
                 'hold':j['latestData']['uiData']["TemporaryHoldUntilTime"],
                 'stat_cool':j['latestData']['uiData']["StatusCool"],
                 'stat_heat':j['latestData']['uiData']["StatusHeat"],
                 'unit':j['latestData']['uiData']['DisplayUnits'],
                 'system':system,
                 'running':j['latestData']['uiData']['EquipmentOutputStatus'],
                 'stat_fan':j['latestData']['fanData']["fanMode"],
                 'fan_running':j['latestData']['fanData']['fanIsRunning']
                }
           # running: 2(running), 0(stopped) 
           # fan_running: True(running), False(stopping(run about 5min more)/stopped)
           #print "\n"
           #print j
           return hwth
    
       headers={
            "Accept":'application/json; q=0.01',
            "DNT":"1",
            "Accept-Encoding":"gzip,deflate,sdch",
            'Content-Type':'application/json; charset=UTF-8',
            "Cache-Control":"max-age=0",
            "Accept-Language":"en-US,en,q=0.8",
            "Connection":"keep-alive",
            "Host":"mytotalconnectcomfort.com",
            "Referer":"https://mytotalconnectcomfort.com/portal/",
            "X-Requested-With":"XMLHttpRequest",
            "User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36",
            'Referer':"/TotalConnectComfort/Device/CheckDataSession/"+code,
            "Cookie":cookie
        }


       # Data structure with data we will send back
       payload = {
        "CoolNextPeriod": None,
        "CoolSetpoint": None,
        "DeviceID": self.Device_id,
        "FanMode": None,
        "HeatNextPeriod": None,
        "HeatSetpoint": None,
        "StatusCool": 0,
        "StatusHeat": 0,
        "SystemSwitch": None,
        "TemporaryHoldUntilTime": 0
       }


       # Calculate the hold time for cooling/heating
       t = datetime.datetime.now();
#          stop_time = ((t.hour+int(hold_time))%24) * 60 + t.minute
#          stop_time = stop_time/15
#      36 is Honeywell default value
       stop_time=36

       # Modify payload based on user input
       if (action == "cool"):
         payload["SystemSwitch"] = 3
         payload["CoolSetpoint"] = value
         payload["StatusCool"] = 1
         payload["StatusHeat"] = 1
         payload["CoolNextPeriod"] = stop_time
         if hold_time > 0:
            payload["TemporaryHoldUntilTime"] = int(hold_time * 60)
    
       if (action == "heat"):
         payload["SystemSwitch"] = 1
         payload["HeatSetpoint"] = value
         payload["StatusCool"] = 1
         payload["StatusHeat"] = 1
         payload["HeatNextPeriod"] = stop_time
         if hold_time > 0:
            payload["TemporaryHoldUntilTime"] = int(hold_time * 60)

       if (action == "cancel"):
         payload["StatusCool"] = 0
         payload["StatusHeat"] = 0

       if (action == "fan"):
         payload["FanMode"] = value


       # Prep and send payload
       location="/portal/Device/SubmitControlScreenChanges"
       rawj=json.dumps(payload)
        
       conn = httplib.HTTPSConnection("mytotalconnectcomfort.com");
       #conn.set_debuglevel(999);
       #print "R4 will send"
       #print rawj
       #self.k_class.log(rawj)
       conn.request("POST", location,rawj,headers)
       r4 = conn.getresponse()
       #self.k_class.log(r4.status)
       if (r4.status != 200): 
         if self.k_class is not None:
            self.k_class.dbg(3,"Error Didn't get 200 status on R4 status={0} {1}".format(r4.status,r4.reason))
            self.k_class.log("Set {0}:{1} : Fail".format(action,value))
         else:
            self.k_class.dbg(3,"Error Didn't get 200 status on R4 status={0} {1}".format(r4.status,r4.reason))
         return
       else:
           time.sleep(10)
           thstat=self.do_set(r1=r1,action="status")
           if thstat is None:
              self.k_class.log("Set {0}:{1} : Fail".format(action,value))
              return

           if action == 'fan' and int(thstat['stat_fan']) == int(value):
               self.k_class.log("Set {0}:{1} : Success".format(action,value))
               return True
           elif thstat['system'] == action and int(thstat[thstat['system']]) == int(value):
               self.k_class.log("Set {0}:{1} : Success".format(action,value))
               return True
           else:
               self.k_class.log("Set {0}:{1} : Not completed".format(action,value))
               return

   def printUsage(self):
       print("")
       print("Cooling: -c temperature -t hold_time")
       print("Heating: -h temperature -t hold_time")
       print("Status: -s")
       print("Cancel: -x")
       print("Fan: -f [0=auto|1=on]")
       print("")
       print("Example: Set temperature to cool to 80f for 1 hour: \n\t therm.py -c 80 -t 1")
       print("")
       print("If no -t hold_time is provided, it will default to one hour from command time.")
       print("")
    
    
if __name__ == "__main__":
    USERNAME=""
    PASSWORD=""
    DEVICE_ID=""
    hw=Honeywell(Id=USERNAME,Pass=PASSWORD,Device_id=DEVICE_ID)
    conn=hw.get_login()

    if sys.argv[1] == "-s":
        #hw.get_login("status")
        thstat=hw.do_set(r1=conn,action="status")
        print("Indoor Temperature:",thstat['in_temp'])
        print("Indoor Humidity:",thstat['in_hum'])
        print("Cool Setpoint:",thstat['cool'])
        print("Heat Setpoint:",thstat['heat'])
        print("Hold Until :",thstat['hold'])
        print("Status Cool:",thstat['stat_cool'])
        print("Status Heat:",thstat['stat_heat'])
        print("Status Fan:",thstat['stat_fan'])
        print("System switch:",thstat['system'])
        sys.exit()

    if sys.argv[1] == "-x":
#        hw.get_login("cancel")
        hw.do_set(conn,"cancel")
        sys.exit()

    if (len(sys.argv) < 3) or (sys.argv[1] == "-help"):
        hw.printUsage()
        sys.exit()

    if sys.argv[1] == "-c":
#        hw.get_login("cool", sys.argv[2])
        hw.do_set(conn,"cool",sys.argv[2])
        sys.exit()

    if sys.argv[1] == "-h":
#        hw.get_login("heat", sys.argv[2])
        hw.do_set(conn,"heat",sys.argv[2])
        sys.exit()

    if sys.argv[1] == "-f":
#        hw.get_login("fan", sys.argv[2])
        hw.do_set(conn,"fan",sys.argv[2])
        sys.exit()
#   main()
