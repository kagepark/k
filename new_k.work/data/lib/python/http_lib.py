import os
import sqlite3
import login
import time
#
# HTTPD
#
import BaseHTTPServer
import cgi
import ssl
import socket



############################
#Global parameters
############################
log_file='log/KHAS.log'
error_log_file='log/KHAS.err'
dbg_file='log/KHAS.dbg'

secret_key='mxl2Z8fhsksAejT9'
login_key=''
api={}

class WEB:
    def __init__(self,login_class=None,k_class=None,k_net=None,pem_file=None,hostname=None,port_num=None):
       self.db_file=db_file
       self.HOST_NAME=hostname
       self.PORT_NUMBER=port_num
       self.login_class=login_class

    def menu(s,login_key=None):
       if login_key is None:
          kha_menu="<table><tr>"
          kha_menu=kha_menu+"<td>[<a href='https://{0}:{1}/'>Login</a>]</td>".format(HOST_NAME,PORT_NUMBER)
          kha_menu=kha_menu+"</td></tr></table><hr>"
       else:
          kha_menu="<table><tr>"
          kha_menu=kha_menu+"<td>[<a href='https://{0}:{1}/{2}/account_reg'>Add KHA Account</a>]</td>".format(HOST_NAME,PORT_NUMBER,login_key)
          kha_menu=kha_menu+"<td>[<a href='https://{0}:{1}/{2}/setup/member_add'>Add Device Login</a>]</td>".format(HOST_NAME,PORT_NUMBER,login_key)
          kha_menu=kha_menu+"<td>[<a href='https://{0}:{1}/{2}/setup/device_add'>Add Device Information</a>]</td>".format(HOST_NAME,PORT_NUMBER,login_key)
          kha_menu=kha_menu+"<td>[<a href='https://{0}:{1}/{2}/setup/env_add'>Environments</a>]</td>".format(HOST_NAME,PORT_NUMBER,login_key)
          kha_menu=kha_menu+"<td>[<a href='https://{0}:{1}/{2}/setup/schedule'>schedule update</a>]</td>".format(HOST_NAME,PORT_NUMBER,login_key)
          kha_menu=kha_menu+"<td>[<a href='https://{0}:{1}/{2}/log/status'>status log</a>]</td>".format(HOST_NAME,PORT_NUMBER,login_key)
          kha_menu=kha_menu+"<td>[<a href='https://{0}:{1}/{2}/log/err'>error log</a>]</td>".format(HOST_NAME,PORT_NUMBER,login_key)
          kha_menu=kha_menu+"<td>[<a href='https://{0}:{1}/{2}/log/dbg'>debug log</a>]</td>".format(HOST_NAME,PORT_NUMBER,login_key)
          kha_menu=kha_menu+"<td>[<a href='https://{0}:{1}/'>logout</a>]</td>".format(HOST_NAME,PORT_NUMBER,login_key)
          kha_menu=kha_menu+"</td></tr></table><hr>"
       return kha_menu


    def get(self,rpath):
        # Parse out the arguments.
        # The arguments follow a '?' in the URL. Here is an example:
        #   http://example.com?arg1=val1
        global login_key,db_file
        conn=sqlite3.connect(db_file)
        lg=conn.cursor()
        args = {}
        idx = s.path.find('?')
        if idx >= 0:
            rpath = s.path[:idx]
            args = cgi.parse_qs(s.path[idx+1:])
        else:
            rpath = s.path

        if len(args):
            i = 0
            for key in sorted(args):
                k.dbg(3,'ARG[%d] %s=%s' % (i, key, args[key]))
                i += 1


        if (rpath == '/{0}/log/status/'.format(login_key) or rpath == '/{0}/log/status'.format(login_key) ) and login_key != '':
           self.send_response(200)
           self.send_header("Content-type", "text/html")
           self.end_headers()
           self.wfile.write(s.menu(login_key))

           tail_log=k.tail(home_path+'/'+log_file,100)
           self.wfile.write("""<pre>""")
           if tail_log is False:
              self.wfile.write("""No Log""")
           else:
              for ii in list(tail_log):
                 self.wfile.write("""{0}""".format(ii.replace('\n','<br>')))
           self.wfile.write("""</pre>""")
        elif (rpath == '/{0}/log/dbg/'.format(login_key) or rpath == '/{0}/log/dbg'.format(login_key)) and login_key != '':
           self.send_response(200)
           self.send_header("Content-type", "text/html")
           self.end_headers()
           self.wfile.write(s.menu(login_key))
           yy=time.strftime('%Y')
           mm=time.strftime('%m')
           dd=time.strftime('%d')
           dbg_file_name_now="{0}/{1}.{2}-{3}-{4}".format(home_path,dbg_file,yy,mm,dd)
           tail_log=k.tail(dbg_file_name_now,600)
   
           self.wfile.write("""<pre>""")
           if tail_log is False:
              self.wfile.write("""No Log""")
           else:
              for ii in list(tail_log):
                 self.wfile.write("""{0}""".format(ii.replace('\n','<br>')))
           self.wfile.write("""</pre>""")
        elif (rpath == '/{0}/log/err/'.format(login_key) or rpath == '/{0}/log/err'.format(login_key)) and  login_key != '':
           self.send_response(200)
           self.send_header("Content-type", "text/html")
           self.end_headers()
           self.wfile.write(s.menu(login_key))
           tail_log=k.tail(home_path+'/'+error_log_file,120)
           self.wfile.write("""<pre>""")
           if tail_log is False:
              self.wfile.write("""No Log""")
           else:
              for ii in list(tail_log):
                 self.wfile.write("""{0}""".format(ii.replace('\n','<br>')))
           self.wfile.write("""</pre>""")

        elif (rpath == '/{0}/setup/schedule/'.format(login_key) or rpath == '/{0}/setup/schedule'.format(login_key))  and login_key != '':
           yy=time.strftime('%Y')
           mm=time.strftime('%m')
           dd=time.strftime('%d')
           ww=time.strftime('%a')
           hh=time.strftime('%H')
           mmin=time.strftime('%M')
           sql_cmd="select idx,title,dest,mode,opt,msg,yy,mm,dd,week,hh,min from schedule"
           lg.execute(sql_cmd)

           self.send_response(200)
           self.send_header("Content-type", "text/html")
           self.end_headers()
           self.wfile.write(s.menu(login_key))
           self.wfile.write("<html><head><title>Title goes here.</title></head>")
           #self.wfile.write("""<table width=112 border=1 cellpadding=1 cellspacing=0 align="center">""")
           self.wfile.write("""<table>""")
           self.wfile.write("""<form name="sch_update_form" method="POST" action="/{0}/setup/sch_update">""".format(login_key))
           self.wfile.write("""<tr>""")
           self.wfile.write("""<td>E/N</td>""")
           self.wfile.write("""<td>Del</td>""")
           self.wfile.write("""<td>title</td>""")
           self.wfile.write("""<td>type</td>""")
           self.wfile.write("""<td>device</td>""")
           self.wfile.write("""<td>do</td>""")
           self.wfile.write("""<td>ext</td>""")
           self.wfile.write("""<td>YY</td>""")
           self.wfile.write("""<td>MM</td>""")
           self.wfile.write("""<td>DD</td>""")
           self.wfile.write("""<td>WEEK</td>""")
           self.wfile.write("""<td>HH</td>""")
           self.wfile.write("""<td>MIN</td>""")
           self.wfile.write("</tr>")
           i=0
           for ii in list(lg.fetchall()):
               self.wfile.write("""<tr>""")
               self.wfile.write("""<td> <input type=hidden name="idx" value="{0}">""".format(ii[0]))
               self.wfile.write("""<input type=checkbox id="new_up" name="check" value="{0}"></td>""".format(i))
               self.wfile.write("""<td> <input type=checkbox id="new_up" name="del" value="{0}"></td>""".format(i))
               self.wfile.write("""<td> <input type=text name="title" value="{0}" size=25></td>""".format(ii[1]))
               self.wfile.write("""<td> <input type=text name="dev_type" value="{0}" size=10></td>""".format(ii[2]))
               self.wfile.write("""<td> <input type=text name="dev_name" value="{0}" size=15></td>""".format(ii[3]))
               self.wfile.write("""<td> <input type=text name="dev_act" value="{0}" size=5></td>""".format(ii[4]))
               self.wfile.write("""<td> <input type=text name="dev_ext" value="{0}" size=25></td>""".format(ii[5]))
               self.wfile.write("""<td> <input type=text name="yy" value="{0}" size=5></td>""".format(ii[6]))
               self.wfile.write("""<td> <input type=text name="mm" value="{0}" size=5></td>""".format(ii[7]))
               self.wfile.write("""<td> <input type=text name="dd" value="{0}" size=5></td>""".format(ii[8]))
               self.wfile.write("""<td> <input type=text name="week" value="{0}" size=20></td>""".format(ii[9]))
               self.wfile.write("""<td> <input type=text name="hh" value="{0}" size=5></td>""".format(ii[10]))
               self.wfile.write("""<td> <input type=text name="min" value="{0}" size=5></td>""".format(ii[11]))
               self.wfile.write("</tr>")
               k.dbg(4,"{0}".format(ii))
               i=i+1
           new_idx=k.now(1)
           self.wfile.write("""<tr>""")
           self.wfile.write("""<td> <input type=hidden name="idx" value="{0}">""".format(new_idx))
           self.wfile.write("""<input type=checkbox id="new_up" name="check" value="{0}"></td>""".format(i))
           self.wfile.write("""<td> Add </td>""")
           self.wfile.write("""<td> <input type=text name="title" value="" size=25></td>""")
           self.wfile.write("""<td> <input type=text name="dev_type" value="" size=10></td>""")
           self.wfile.write("""<td> <input type=text name="dev_name" value="" size=15></td>""")
           self.wfile.write("""<td> <input type=text name="dev_act" value="" size=5></td>""")
           self.wfile.write("""<td> <input type=text name="dev_ext" value="" size=25></td>""")
           self.wfile.write("""<td> <input type=text name="yy" value="" size=5></td>""")
           self.wfile.write("""<td> <input type=text name="mm" value="" size=5></td>""")
           self.wfile.write("""<td> <input type=text name="dd" value="" size=5></td>""")
           self.wfile.write("""<td> <input type=text name="week" value="" size=20></td>""")
           self.wfile.write("""<td> <input type=text name="hh" value="" size=5></td>""")
           self.wfile.write("""<td> <input type=text name="min" value="" size=5></td>""")
           self.wfile.write("</tr>")

           self.wfile.write("""<tr><td><input type="submit" value="update"></td></tr>""")
           self.wfile.write("</form>")
           self.wfile.write("</table>")
           self.wfile.write("</body></html>")

        elif rpath == '/{0}/setup/env_add'.format(login_key) and login_key != '':
           sql_cmd="select idx,vargrp,varname,varval,opt1 from env"
           lg.execute(sql_cmd)

           self.send_response(200)
           self.send_header("Content-type", "text/html")
           self.end_headers()
           self.wfile.write(s.menu(login_key))
           self.wfile.write("<html><head><title>Global Environment</title></head>")
           self.wfile.write("""<table>""")
           self.wfile.write("""<form name="env_update_form" method="POST" action="/{0}/setup/env_update">""".format(login_key))
           self.wfile.write("""<tr>""")
           self.wfile.write("""<td>E/N</td>""")
           self.wfile.write("""<td>Del</td>""")
           self.wfile.write("""<td>Group Name</td>""")
           self.wfile.write("""<td>Variable Name</td>""")
           self.wfile.write("""<td>Value</td>""")
           self.wfile.write("""<td>Option</td>""")
           self.wfile.write("</tr>")
           i=0
           for ii in list(lg.fetchall()):
               self.wfile.write("""<tr>""")
               self.wfile.write("""<td> <input type=hidden name="idx" value="{0}">""".format(ii[0]))
               self.wfile.write("""<input type=checkbox id="new_up" name="check" value="{0}"></td>""".format(i))
               self.wfile.write("""<td> <input type=checkbox id="new_up" name="del" value="{0}"></td>""".format(i))
               self.wfile.write("""<td> <select name='vargrp'>""")
               sel_global=''
               sel_weather=''
               sel_computer=''
               sel_sms=''
               if ii[1] == 'global':
                  sel_global="selected=selected"
               elif ii[1] == 'weather':
                  sel_weather="selected=selected"
               elif ii[1] == 'computer':
                  sel_computer="selected=selected"
               elif ii[1] == 'sms':
                  sel_sms="selected=selected"
               self.wfile.write("""<option value='global' {0}>Global</option>""".format(sel_global))
               self.wfile.write("""<option value='weather' {0}>Weather</option>""".format(sel_weather))
               self.wfile.write("""<option value='computer' {0}>Computer</option>""".format(sel_computer))
               self.wfile.write("""<option value='sms' {0}>SMS</option>""".format(sel_sms))
               self.wfile.write("""</select>""")
               self.wfile.write("""</td>""")
               self.wfile.write("""<td> <input type=text name="varname" value="{0}" size=20></td>""".format(ii[2]))
               self.wfile.write("""<td> <input type=text name="varval" value="{0}" size=25></td>""".format(ii[3]))
               self.wfile.write("""<td> <input type=text name="opt1" value="{0}" size=25></td>""".format(ii[4]))
               self.wfile.write("</tr>")
               k.dbg(4,"{0}".format(ii))
               i=i+1
           new_idx=k.now(1)
           self.wfile.write("""<tr>""")
           self.wfile.write("""<td> <input type=hidden name="idx" value="{0}">""".format(new_idx))
           self.wfile.write("""<input type=checkbox id="new_up" name="check" value="{0}"></td>""".format(i))
           self.wfile.write("""<td> Add </td>""")
           self.wfile.write("""<td> <select name='vargrp'>""")
           self.wfile.write("""<option value='global'>Global</option>""")
           self.wfile.write("""<option value='weather'>Weather</option>""")
           self.wfile.write("""<option value='computer'>Computer</option>""")
           self.wfile.write("""<option value='sms'>SMS</option>""")
           self.wfile.write("""</select>""")
           self.wfile.write("""<td> <input type=text name="varname" value="" size=20></td>""")
           self.wfile.write("""<td> <input type=text name="varval" value="" size=25></td>""")
           self.wfile.write("""<td> <input type=text name="opt1" value="" size=25></td>""")
           self.wfile.write("""</td>""")
           self.wfile.write("</tr>")

           self.wfile.write("""<tr><td><input type="submit" value="update"></td></tr>""")
           self.wfile.write("</form>")
           self.wfile.write("</table>")
           self.wfile.write("</body></html>")

        elif rpath == '/{0}/setup/device_add'.format(login_key) and login_key != '':
           sql_cmd="select idx,devmodel,devname,ip,signal from device"
           lg.execute(sql_cmd)

           self.send_response(200)
           self.send_header("Content-type", "text/html")
           self.end_headers()
           self.wfile.write(s.menu(login_key))
           self.wfile.write("<html><head><title>Device</title></head>")
           self.wfile.write("""<table>""")
           self.wfile.write("""<form name="device_update_form" method="POST" action="/{0}/setup/device_update">""".format(login_key))
           self.wfile.write("""<tr>""")
           self.wfile.write("""<td>E/N</td>""")
           self.wfile.write("""<td>Del</td>""")
           self.wfile.write("""<td>Model Name</td>""")
           self.wfile.write("""<td>Device Name</td>""")
           self.wfile.write("""<td>IP Address</td>""")
           self.wfile.write("""<td>RM Signal file</td>""")
           self.wfile.write("""<td>Dest IP</td>""")
           self.wfile.write("</tr>")
           i=0
           for ii in list(lg.fetchall()):
               self.wfile.write("""<tr>""")
               self.wfile.write("""<td> <input type=hidden name="idx" value="{0}">""".format(ii[0]))
               self.wfile.write("""<input type=checkbox id="new_up" name="check" value="{0}"></td>""".format(i))
               self.wfile.write("""<td> <input type=checkbox id="new_up" name="del" value="{0}"></td>""".format(i))
               self.wfile.write("""<td> <select name='devmodel'>""")
               sel_wemo=''
               sel_rm=''
               sel_br=''
               if ii[1] == 'wemo':
                  sel_wemo="selected=selected"
               elif ii[1] == 'rm':
                  sel_rm="selected=selected"
               elif ii[1] == 'broadlink':
                  sel_br="selected=selected"
               self.wfile.write("""<option value='wemo' {0}>WeMO</option>""".format(sel_wemo))
               self.wfile.write("""<option value='broadlink' {0}>BroadLink</option>""".format(sel_rm))
               self.wfile.write("""<option value='rm' {0}>BroadLink Remocon</option>""".format(sel_br))
               self.wfile.write("""</select>""")
               self.wfile.write("""</td>""")
               self.wfile.write("""<td> <input type=text name="devname" value="{0}" size=20></td>""".format(ii[2]))
               self.wfile.write("""<td> <input type=text name="ip" value="{0}" size=15></td>""".format(ii[3]))
               self.wfile.write("""<td> <input type=text name="signal" value="{0}" size=25></td>""".format(ii[4]))
               self.wfile.write("""<td> <input type=text name="ip2" value="{0}" size=15></td>""".format(ii[4]))
               self.wfile.write("</tr>")
               k.dbg(4,"{0}".format(ii))
               i=i+1
           new_idx=k.now(1)
           self.wfile.write("""<tr>""")
           self.wfile.write("""<td> <input type=hidden name="idx" value="{0}">""".format(new_idx))
           self.wfile.write("""<input type=checkbox id="new_up" name="check" value="{0}"></td>""".format(i))
           self.wfile.write("""<td> Add </td>""")
           self.wfile.write("""<td> <select name='devmodel'>""")
           self.wfile.write("""<option value='wemo'>WeMO</option>""")
           self.wfile.write("""<option value='broadlink'>BroadLink</option>""")
           self.wfile.write("""<option value='rm'>BroadLink Remocon</option>""")
           self.wfile.write("""</select>""")
           self.wfile.write("""</td>""")
           self.wfile.write("""<td> <input type=text name="devname" value="" size=20></td>""")
           self.wfile.write("""<td> <input type=text name="ip" value="" size=15></td>""")
           self.wfile.write("""<td> <input type=text name="signal" value="" size=25></td>""")
           self.wfile.write("""<td> <input type=text name="ip2" value="" size=15></td>""")
           self.wfile.write("</tr>")

           self.wfile.write("""<tr><td><input type="submit" value="update"></td></tr>""")
           self.wfile.write("</form>")
           self.wfile.write("</table>")
           self.wfile.write("</body></html>")

        elif (rpath == '/{0}/setup/member_add/'.format(login_key) or rpath == '/{0}/setup/member_add'.format(login_key))  and login_key != '':
           self.send_response(200)
           self.send_header("Content-type", "text/html")
           self.end_headers()
           self.wfile.write(s.menu(login_key))
           self.wfile.write("<html><head><title>Device Member Add</title></head>")
           self.wfile.write("""<table>""")
           self.wfile.write("""<form name="member_add_form" method="POST" action="/{0}/setup/member_get_info">""".format(login_key))
           self.wfile.write("""<tr>""")
           self.wfile.write("""<td>Type</td>""")
           self.wfile.write("""<td>ID</td>""")
           self.wfile.write("""<td>Password</td>""")
           self.wfile.write("""<td>option1</td>""")
           self.wfile.write("""<td>option2</td>""")
           self.wfile.write("""<td>option3</td>""")
           self.wfile.write("</tr>")
           self.wfile.write("""<tr>""")
           self.wfile.write("""<td> <select name='type' value='icloud'>""")
           self.wfile.write("""<option value='icloud'>icloud</option>""")
           self.wfile.write("""<option value='myq'>MyQ</option>""")
           self.wfile.write("""<option value='honeywell'>honeywell</option>""")
           self.wfile.write("""<option value='login'>user</option>""")
           self.wfile.write("""</select>""")
           self.wfile.write("""</td>""")
           self.wfile.write("""<td> <input type=text name="username" value="" size=20></td>""")
           self.wfile.write("""<td> <input type=password name="password" value="" size=20></td>""")
           self.wfile.write("""<td> <input type=text name="opt1" value="" size=20></td>""")
           self.wfile.write("""<td> <input type=text name="opt2" value="" size=20></td>""")
           self.wfile.write("""<td> <input type=text name="opt3" value="" size=20></td>""")
           self.wfile.write("</tr>")
           self.wfile.write("""<tr><td><input type="submit" value="Submit"></td></tr>""")
           self.wfile.write("</form>")
           self.wfile.write("</table>")
           self.wfile.write("</body></html>")

#        elif os.path.isfile(os.curdir + rpath):
#           if os.access(os.curdir + rpath, os.R_OK):
#              try:
#                 sendReply = False
#                 if rpath.endswith(".html"):
#                    mimetype='text/html'
#                    sendReply = True
#                 elif rpath.endswith(".jpg"):
#                    mimetype='image/jpg'
#                    sendReply = True
#                 elif rpath.endswith(".gif"):
#                    mimetype='image/gif'
#                    sendReply = True
#                 elif rpath.endswith(".js"):
#                    mimetype='application/javascript'
#                    sendReply = True
#                 elif rpath.endswith(".css"):
#                    mimetype='text/css'
#                    sendReply = True
#                 else:
#                    self.send_error(404,'Not support file: %s' % rpath)
#
#                 if sendReply == True:
#                    #Open the static file requested and send it
#                    f = open(os.curdir + rpath)
#                    self.send_response(200)
#                    self.send_header('Content-type',mimetype)
#                    self.end_headers()
#
#                    #self.wfile.write("<p>File found at : {0},{1},{2}</p>".format(os.curdir,os.sep,rpath))
#                    self.wfile.write("<p>File found at : {0},{1}</p>".format(os.curdir,rpath))
#
#                    self.wfile.write(f.read())
#                    f.close()
#                 else:
#                    k.log('ERROR: Not support file: %s' % rpath)
#              except IOError:
#                 k.log('ERROR: File Not Found: %s' % rpath)
#           else:
#              k.log("ERROR: Can't read the file : %s" % rpath)
        elif rpath == '/{0}/account_reg'.format(login_key) and login_key != '':
           self.send_response(200)
           self.send_header("Content-type", "text/html")
           self.end_headers()
           self.wfile.write(s.menu(login_key))
           self.wfile.write("<html><head><title>Kage Home Automation System</title></head>")
           self.wfile.write("<body>")
           self.wfile.write("""
        <form action="/{0}/setup/account_reg" method="post">
        <table>
        <tr>
           <td> Username </td> <td> <input name="username" type="text" /></td>
        </tr>
        <tr>
           <td> Password </td> <td> <input name="password" type="password" />
           <input type=hidden name="type" value="login" /></td>
        </tr>
        <tr>
           <td> Email    </td> <td> <input type=text name="email" value="" /></td>
        </tr>
        <tr>
           <td> Tel(SMS) </td> <td> <input type=text name="sms" value="" /></td>
        </tr>
        <!--tr>
           <td> Option1  </td> <td> <input type=text name="opt1" value="" /></td>
        </tr>
        <tr>
           <td> Option2  </td> <td> <input type=text name="opt2" value="" /></td>
        </tr>
        <tr>
           <td> Option3  </td> <td> <input type=text name="opt3" value="" /></td>
        </tr-->

           <td> Level    </td> <td>
            <select name='level'>""
            <option value='2'>Admin</option>
            <option value='9' selected=selected>User</option>
            </select> </td>
        </tr>

        <tr>
            <td> <input value="Add New" type="submit" /> </td>
        </tr>
        </table>
        </form>
           """.format(login_key))
           self.wfile.write("</body></html>")
        else:
           rc=login.check(hwtype='check')
           if rc['ok'] == 2:
              login_key=''
              self.send_response(200)
              self.send_header("Content-type", "text/html")
              self.end_headers()
              self.wfile.write(s.menu())
              self.wfile.write("<html><head><title>Kage Home Automation System</title></head>")
              self.wfile.write("<body>")
              self.wfile.write("""
        <form action="/login" method="post">
            Username: <input name="username" type="text" />
            Password: <input name="password" type="password" />
            <input value="Login" type="submit" />
        </form>
              """)
              self.wfile.write("</body></html>")
           else:
              login_key='new_ini_reg'
              self.send_response(200)
              self.send_header("Content-type", "text/html")
              self.end_headers()
              self.wfile.write("<html><head><title>Kage Home Automation System</title></head>")
              self.wfile.write("<body>")
              self.wfile.write("""
        <form action="/{0}/setup/account_reg" method="post">
        <table>
        <tr>
           <td> Username </td> <td> <input name="username" type="text" /></td>
        </tr>
        <tr>
           <td> Password </td> <td> <input name="password" type="password" />
           <input type=hidden name="type" value="login" />
           <input type=hidden name="level" value="0" /></td>
        </tr>
        <tr>
           <td> Email    </td> <td> <input type=text name="email" value="" /></td>
        </tr>
        <tr>
           <td> Tel(SMS) </td> <td> <input type=text name="sms" value="" /></td>
        <tr>
            <td> <input value="Add initial admin account" type="submit" /> </td>
        </tr>
        </table>
        </form>
           """.format(login_key))
              self.wfile.write("</body></html>")

    #Handler for the POST requests
    def send(self,rpath):
       conn=sqlite3.connect(db_file)
       lg=conn.cursor()

       ctype, pdict = cgi.parse_header(s.headers['content-type'])
       if ctype == 'multipart/form-data':
           postvars = cgi.parse_multipart(s.rfile, pdict)
       elif ctype == 'application/x-www-form-urlencoded':
           length = int(s.headers['content-length'])
           postvars = cgi.parse_qs(s.rfile.read(length), keep_blank_values=1)
       else:
           postvars = {}

       if rpath=='/login':
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            rc=login.check(username=postvars['username'][0],password=postvars['password'][0])
            if rc['ok'] == 0:
               self.wfile.write("""missing username or password""")
            elif rc['ok'] == 1:
               self.wfile.write(rc['msg'])
            elif rc['ok'] == 2:
               login_key=rc['msg']
               k.dbg(3,rc['msg'])
               self.wfile.write(s.menu(login_key))
               self.wfile.write("""{0} accepted""".format(postvars['username'][0]))
            elif rc['ok'] == 3:
               k.dbg(3,rc['msg'])
               self.wfile.write(rc['msg'])
            return

       elif rpath=="/{0}/setup/account_reg".format(login_key) and login_key != '':
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            rc=login.check(username=postvars['username'][0],password=postvars['password'][0],values=postvars,hwtype='save')
            if rc['ok'] == 0:
               self.wfile.write(s.menu())
               self.wfile.write(rc['msg'])
               return
            elif rc['ok'] == 1:
               self.wfile.write(s.menu())
               self.wfile.write(rc['msg'])
               return
            elif rc['ok'] == 2:
               self.wfile.write(s.menu())
               k.dbg(3,rc['msg'])
               self.wfile.write(rc['msg'])
               return
       elif (rpath=="/{0}/setup/env_update".format(login_key)) and login_key != '':
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(s.menu(login_key))
            # Print out logging information about the path and args.
            k.dbg(4,'TYPE %s, PATH %s, ARGS %d' % (ctype,s.path,len(postvars)))
            chk=0
            #Delete schedule
            if 'del' in postvars:
                k.dbg(4,'del=%s' % (postvars['del']))
                mx_del=len(postvars['del'])
                if mx_del > 0:
                   chk=1
                   for ii in range(0,mx_del):
                      del_idx=int(postvars['del'][ii])
                      sch_ud="delete from env where idx={0}".format(postvars['idx'][del_idx])
                      lg.execute(sch_ud)
                      k.dbg(4,"ii:%s = %s" % (ii,sch_ud))
                      lg.execute(sch_ud)
                   conn.commit()
                   self.wfile.write("""{0} items deleted""".format(mx_del))

            #Edit/New device
            if 'check' in postvars:
                k.dbg(4,'check=%s' % (postvars['check']))
                mx_update=len(postvars['check'])
                if mx_update > 0:
                   chk=1
                   for ii in range(0,mx_update):
                      item_idx=int(postvars['check'][ii])
                      k.dbg(3,'idx#: {0} => {1}, item_idx:{2}'.format(len(postvars['idx']),postvars['idx'][(len(postvars['idx'])-1)],item_idx))
                      k.dbg(4,'postvars: {0}'.format(postvars))
                      #New device
                      if item_idx == len(postvars['idx']) -1:
                         sch_ud="insert into env (idx,vargrp,varname,varval,opt1) values (%s,'%s','%s','%s','%s')" %(
                            postvars['idx'][item_idx],
                            postvars['vargrp'][item_idx],
                            postvars['varname'][item_idx],
                            postvars['varval'][item_idx],
                            postvars['opt1'][item_idx])

                      #Edit device
                      else:
                         sch_ud="update env set vargrp='%s',varname='%s',varval='%s',opt1='%s' where idx='%s'" %(
                            postvars['vargrp'][item_idx],
                            postvars['varname'][item_idx],
                            postvars['varval'][item_idx],
                            postvars['opt1'][item_idx],
                            postvars['idx'][item_idx])
                      k.dbg(4,"ii:%s = %s" % (ii,sch_ud))
                      lg.execute(sch_ud)
                   conn.commit()
                   self.wfile.write("""{0} items updated/added""".format(mx_update))
            if chk == 0:
               self.wfile.write("""Nothing to do""")
            return

       elif (rpath=="/{0}/setup/device_update".format(login_key)) and login_key != '':
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(s.menu(login_key))
            # Print out logging information about the path and args.
            k.dbg(4,'TYPE %s, PATH %s, ARGS %d' % (ctype,s.path,len(postvars)))
            chk=0
            #Delete schedule
            if 'del' in postvars:
                k.dbg(4,'del=%s' % (postvars['del']))
                mx_del=len(postvars['del'])
                if mx_del > 0:
                   chk=1
                   for ii in range(0,mx_del):
                      del_idx=int(postvars['del'][ii])
                      sch_ud="delete from device where idx={0}".format(postvars['idx'][del_idx])
                      lg.execute(sch_ud)
                      k.dbg(4,"ii:%s = %s" % (ii,sch_ud))
                      lg.execute(sch_ud)
                   conn.commit()
                   self.wfile.write("""{0} items deleted""".format(mx_del))

            #Edit/New device
            if 'check' in postvars:
                k.dbg(4,'check=%s' % (postvars['check']))
                mx_update=len(postvars['check'])
                if mx_update > 0:
                   chk=1
                   for ii in range(0,mx_update):
                      item_idx=int(postvars['check'][ii])
                      k.dbg(3,'idx#: {0} => {1}, item_idx:{2}'.format(len(postvars['idx']),postvars['idx'][(len(postvars['idx'])-1)],item_idx))
                      k.dbg(4,'postvars: {0}'.format(postvars))
                      #New device
                      if item_idx == len(postvars['idx']) -1:
                         sch_ud="insert into device (idx,devmodel,devname,ip,signal,ip2) values (%s,'%s','%s','%s','%s','%s')" %(
                            postvars['idx'][item_idx],
                            postvars['devmodel'][item_idx],
                            postvars['devname'][item_idx],
                            postvars['ip'][item_idx],
                            postvars['signal'][item_idx],
                            postvars['ip2'][item_idx])

                      #Edit device
                      else:
                         sch_ud="update device set devmodel='%s',devname='%s',ip='%s',signal='%s',ip2='%s' where idx='%s'" %(
                            postvars['devmodel'][item_idx],
                            postvars['devname'][item_idx],
                            postvars['ip'][item_idx],
                            postvars['signal'][item_idx],
                            postvars['ip2'][item_idx],
                            postvars['idx'][item_idx])
                      k.dbg(4,"ii:%s = %s" % (ii,sch_ud))
                      lg.execute(sch_ud)
                   conn.commit()
                   self.wfile.write("""{0} items updated/added""".format(mx_update))
            if chk == 0:
               self.wfile.write("""Nothing to do""")
            return


       elif (rpath=="/{0}/setup/sch_update".format(login_key)) and login_key != '':
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(s.menu(login_key))
            # Print out logging information about the path and args.
            k.dbg(4,'TYPE %s, PATH %s, ARGS %d' % (ctype,s.path,len(postvars)))
            chk=0
            #Delete schedule
            if 'del' in postvars:
                k.dbg(4,'del=%s' % (postvars['del']))
                mx_del=len(postvars['del'])
                if mx_del > 0:
                   chk=1
                   for ii in range(0,mx_del):
                      del_idx=int(postvars['del'][ii])
                      sch_ud="delete from schedule where idx={0}".format(postvars['idx'][del_idx])
                      lg.execute(sch_ud)
                      k.dbg(4,"ii:%s = %s" % (ii,sch_ud))
                      lg.execute(sch_ud)
                   conn.commit()
                   self.wfile.write("""{0} items deleted""".format(mx_del))

            #Edit/New schedule
            if 'check' in postvars:
                k.dbg(4,'check=%s' % (postvars['check']))
                mx_update=len(postvars['check'])
                if mx_update > 0:
                   chk=1
                   for ii in range(0,mx_update):
                      item_idx=int(postvars['check'][ii])
                      k.dbg(3,'idx#: {0} => {1}, item_idx:{2}'.format(len(postvars['idx']),postvars['idx'][(len(postvars['idx'])-1)],item_idx))
                      #New schedule
                      if item_idx == len(postvars['idx']) -1:
                         sch_ud="insert into schedule (idx,title,dest,mode,opt,msg,yy,mm,dd,week,hh,min) values (%s,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')" %(
                            postvars['idx'][item_idx],
                            postvars['title'][item_idx],
                            postvars['dev_type'][item_idx],
                            postvars['dev_name'][item_idx],
                            postvars['dev_act'][item_idx],
                            postvars['dev_ext'][item_idx],
                            postvars['yy'][item_idx],
                            postvars['mm'][item_idx],
                            postvars['dd'][item_idx],
                            postvars['week'][item_idx],
                            postvars['hh'][item_idx],
                            postvars['min'][item_idx])
                      #Edit schedule
                      else:
                         sch_ud="update schedule set title='%s',dest='%s',mode='%s',opt='%s',msg='%s',yy='%s',mm='%s',dd='%s',week='%s',hh='%s',min='%s' where idx=%s" %(
                            postvars['title'][item_idx],
                            postvars['dev_type'][item_idx],
                            postvars['dev_name'][item_idx],
                            postvars['dev_act'][item_idx],
                            postvars['dev_ext'][item_idx],
                            postvars['yy'][item_idx],
                            postvars['mm'][item_idx],
                            postvars['dd'][item_idx],
                            postvars['week'][item_idx],
                            postvars['hh'][item_idx],
                            postvars['min'][item_idx],
                            postvars['idx'][item_idx])
                      k.dbg(4,"ii:%s = %s" % (ii,sch_ud))
                      lg.execute(sch_ud)
                   conn.commit()
                   self.wfile.write("""{0} items updated/added""".format(mx_update))
            if chk == 0:
               self.wfile.write("""Nothing to do""")
            return

       elif (rpath=="/{0}/setup/member_get_info".format(login_key)) and login_key != '':
            from pyicloud import PyiCloudService
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(s.menu(login_key))

            rc=login.check(username=postvars['username'][0],password=postvars['password'][0],values=postvars,hwtype='check_id')
            k.dbg(3,'check_id:{0}'.format(rc['ok']))
            if rc['ok'] == 0:
               self.wfile.write(s.menu())
               self.wfile.write(rc['msg'])
               return
            elif rc['ok'] == 2:
               self.wfile.write("""Aready registered {0}""".format(postvars['username'][0]))
               return
            
            if postvars['username'] != "" and postvars['password'] != "":
               if postvars['type'][0] == 'icloud':
                  try:
                     api=login.check(username=postvars['username'][0],password=postvars['password'][0],values=postvars,hwtype='icloud')
                  except:
                     self.wfile.write('Invalid email/password combination. Please try again')
                     return
    
                  k.dbg(4,api)
                  self.wfile.write("<html><head><title>Member Add</title></head>")
                  self.wfile.write("""<table>""")
                  self.wfile.write("""<form name="member_add_form" method="POST" action="/{0}/setup/icloud_member_reg">""".format(login_key))
                  self.wfile.write("""<tr>""")
                  self.wfile.write("""<td> <input type=text name="type" value="icloud" size=10></td>""")
                  self.wfile.write("""<td> <input type=text name="username" value="{0}" size=20></td>""".format(postvars['username'][0]))
                  self.wfile.write("""<td> <input type=hidden name="password" value="{0}" size=20></td>""".format(postvars['password'][0]))
                  self.wfile.write("</tr>")
                  self.wfile.write("""<tr>""")
                  self.wfile.write("""<td>Select</td>""")
                  self.wfile.write("""<td>Device/Member Name</td>""")
                  self.wfile.write("""<td>R1 interval</td>""")
                  self.wfile.write("""<td>R2 interval</td>""")
                  self.wfile.write("""<td>R3 interval</td>""")
                  self.wfile.write("""<td>Monitor</td>""")
                  self.wfile.write("</tr>")
                  
                  i=0
                  for api_key in list(api.devices.keys()):
                     self.wfile.write("""<tr>""")
                     device_name="{0}".format(api.devices[api_key])
                     device_name=device_name.split(':')[1].replace(' ','',1).replace(' ','-')
                     self.wfile.write("""<td> <input type=checkbox name="check" value="{0}"></td>""".format(i))
                     self.wfile.write("""<td> <input type=text name="api_name" value="{0}" size=30>""".format(device_name))
                     self.wfile.write("""<input type=hidden name="api_key" value="{0}"></td>""".format(api_key))
                     self.wfile.write("""<td> <input type=text name="readius1_interval" value="10" size=5></td>""")
                     self.wfile.write("""<td> <input type=text name="readius2_interval" value="60" size=5></td>""")
                     self.wfile.write("""<td> <input type=text name="readius3_interval" value="300" size=5></td>""")
                     self.wfile.write("""<td> <input type=text name="monitor" value="1" size=5></td>""")
                     self.wfile.write("</tr>")
                     i=i+1
                  self.wfile.write("</tr>")
                  self.wfile.write("""<tr><td><input type="submit" value="Add Member"></td></tr>""")
                  self.wfile.write("</form>")
                  self.wfile.write("</table>")
                  self.wfile.write("</body></html>")
                  return
               else:
                   rc=login.check(values=postvars,hwtype='save')
                   if rc['ok'] == 2:
                      self.wfile.write("{0} at {1}".format(rc['msg'],postvars['type'][0]))
                      return
                   else:
                      self.wfile.write(rc['msg'])
                      return
            self.wfile.write("""ID or Password not found""")

       elif rpath=="/{0}/setup/icloud_member_reg".format(login_key) and login_key != '':
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(s.menu(login_key))
            if 'check' in postvars:
                k.dbg(3,'check=%s' % (postvars['check']))
                mx_update=len(postvars['check'])
                if mx_update > 0:
                   item_idx=int(postvars['check'][0])
                   members="{0}".format(postvars['api_name'][item_idx])
                   for ii in range(1,mx_update):
                      item_idx=int(postvars['check'][ii])
                      members="{0} {1}".format(postvars['api_name'][item_idx],members)

                   postvars['opt1']=[members]
                   rc=login.check(username=postvars['username'][0],password=postvars['password'][0],values=postvars,hwtype='save')
                   if rc['ok'] == 2:
                      lg.execute("select idx from login where id='{0}' and hwtype='{1}'".format(postvars['username'][0],postvars['type'][0]))
                      icloud_idx=lg.fetchone()[0]
                   else:
                      self.wfile.write(rc['msg'])
                      return

                   for ii in range(0,mx_update):
                      item_idx=int(postvars['check'][ii])
                      sch_ud="insert into members (name,icloud_idx,dev_id,email,tel,radius1_interval,radius2_interval,radius3_interval,monitor) values ('%s',%s,'%s','%s','%s',%s,%s,%s,%s)"%(
                          postvars['api_name'][item_idx],
                          icloud_idx,
                          postvars['api_key'][item_idx],
                          postvars['email'][item_idx],
                          postvars['tel'][item_idx],
                          postvars['readius1_interval'][item_idx],
                          postvars['readius2_interval'][item_idx],
                          postvars['readius3_interval'][item_idx],
                          postvars['monitor'][item_idx])
                      k.dbg(4,"ii:%s = %s" % (ii,sch_ud))
                      lg.execute(sch_ud)
                   conn.commit()
                   self.wfile.write("""Added {0}""".format(postvars['username'][0]))
                   return
            self.wfile.write("""0 items Added""")


 
       elif (rpath=="/{0}/do/switch".format(login_key)) and login_key != '':
            # Print out logging information about the path and args.
            k.dbg(3,'TYPE %s' % (ctype))
            k.dbg(3,'PATH %s' % (s.path))
            k.dbg(3,'ARGS %d' % (len(postvars)))
            if len(postvars):
                i = 0
                for key in sorted(postvars):
                    k.dbg(3,'ARG[%d] %s=%s' % (i, key, postvars[key]))
                    i += 1

                    #imessage_get="insert into schedule (idx,yy,mm,dd,week,hh,min,title,dest,mode,opt,msg) values (18.0,'','','','','22','30','Go to Sleep','thermostat','','','')"
                    #lg.execute(imessage_get)
                #conn.commit()


       elif (rpath=="/{0}/do/thermostat".format(login_key)) and login_key != '':
            # Print out logging information about the path and args.
            k.dbg(3,'TYPE %s' % (ctype))
            k.dbg(3,'PATH %s' % (s.path))
            k.dbg(3,'ARGS %d' % (len(postvars)))
            if len(postvars):
                i = 0
                for key in sorted(postvars):
                    k.dbg(3,'ARG[%d] %s=%s' % (i, key, postvars[key]))
                    i += 1

                    #imessage_get="insert into schedule (idx,yy,mm,dd,week,hh,min,title,dest,mode,opt,msg) values (18.0,'','','','','22','30','Go to Sleep','thermostat','','','')"
                    #lg.execute(imessage_get)
                #conn.commit()


       elif (rpath=="/{0}/setup/thermostat_update".format(login_key)) and login_key != '':
            # Print out logging information about the path and args.
            k.dbg(3,'TYPE %s' % (ctype))
            k.dbg(3,'PATH %s' % (s.path))
            k.dbg(3,'ARGS %d' % (len(postvars)))
            if len(postvars):
                i = 0
                for key in sorted(postvars):
                    k.dbg(3,'ARG[%d] %s=%s' % (i, key, postvars[key]))
                    i += 1

                    #imessage_get="insert into schedule (idx,yy,mm,dd,week,hh,min,title,dest,mode,opt,msg) values (18.0,'','','','','22','30','Go to Sleep','thermostat','','','')"
                    #lg.execute(imessage_get)




       else:
          self.send_response(200)
          self.end_headers()
          self.wfile.write("Unknown command %s !" % rpath)

#################################
# Main Run
#################################
if __name__ == "__main__":
   import cep
   home_path=os.path.dirname(os.path.abspath(__file__))
   k=cep.CEP(test=1,_kv_dbg=4)   
   k_net=cep.NET()
   pem_file=os.path.dirname(os.path.abspath(__file__))+'/../server.pem'
   db_file=home_path+'/../.KHAS.sql'
   login=login.LOGIN(k_class=k,k_net=k_net,db_file=db_file)
   web=WEB(k_class=k,k_net=k_net,pem_file=pem_file,login_class=login)
   web.start()
