import copy
def append2list(src,*add):
   if type(src) == str:
      org=[]
      for jj in src.split(','):
         org.append(jj)
   elif type(src) == list:
      org=copy.deepcopy(src)
   elif type(src) == int or type(src) == float:
      org=[]
      org.append(src)
   else:
      return False

   if len(add) < 0:
       return False
   for ii in list(add):
      if type(ii) == type([]):
         for jj in list(ii):
            org.append(jj)
      else:
         for jj in ii.split(','):
            org.append(jj)
   return org

