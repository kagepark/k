def change_password(user=None,password=None):
    if user is None or len(user) == 0 or password is None or len(password) == 0:
        return
    user_found=False
    with open('/etc/shadow','r') as f:
        passwords=f.read()
    new_passwords=''
    for ii in passwords.split('\n'):
        ii_a=ii.split(':')
        if ii_a[0] == user :
            new_password=crypt.crypt(password,crypt.mksalt(crypt.METHOD_SHA512))
            if len(new_passwords) == 0:
                new_passwords='{7}:{0}:{1}:{2}:{3}:{4}:{5}:{6}:{7}'.format(new_password,ii_a[2],ii_a[3],ii_a[4],ii_a[5],ii_a[6],ii_a[7],ii_a[8],user)
            else:
                new_passwords='{0}\n{8}:{1}:{2}:{3}:{4}:{5}:{6}:{7}:{8}'.format(new_passwords,new_password,ii_a[2],ii_a[3],ii_a[4],ii_a[5],ii_a[6],ii_a[7],ii_a[8],user)
            user_found=True
        else:
            if len(new_passwords) == 0:
                new_passwords=ii
            else:
                new_passwords='{0}\n{1}'.format(new_passwords,ii)
    if user_found:
        with open('/etc/shadow','w') as f:
            f.write(new_passwords)