
def cut_string(string,len1=None,len2=None):
    if type(string) != str:
       string='{0}'.format(string)
    str_len=len(string)

    if len1 is None or len1 >= str_len:
       return [string]

    if len2 is None:
        rc=[string[i:i + len1] for i in range(0, str_len, len1)]
        return rc
    rc=[]
    rc.append(string[0:len1])
    string_tmp=string[len1:]
    string_tmp_len=len(string_tmp)
    for i in range(0, int(string_tmp_len/len2)+1):
        if (i+1)*len2 > string_tmp_len:
           rc.append(string_tmp[len2*i:])
        else:
           rc.append(string_tmp[len2*i:(i+1)*len2])
    return rc

