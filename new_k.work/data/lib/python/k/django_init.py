#Kage
#
import os
import kmisc as km

prj_name='KBurn'
app_name='cburn'
django_home_path='/django'
data={'time_zone':'America/Los_Angeles','template_dir':'''os.path.join(BASE_DIR,'templates')''','server_ip':'172.16.115.130','server_port':'8002','fileupload_dir':'media','static_dir':'static'}


def default_setting(django_home_path,app_name,prj_name,data={}):
    filename='{0}/{1}/{1}/settings.py'.format(django_home_path,prj_name)
    app_class_name=app_name.title()
    print(filename)
    with open(filename,'r') as f:
        setting_cfg=f.read()

    new_cfg=''
    chk_td=False
    chk_ia=False
    for line in setting_cfg.split('\n'):
        if chk_td and 'template_dir' in data:
            if line.strip().split(' ')[0] == "'DIRS':":
                new_cfg="{0}\n'DIRS': [{1}],".format(new_cfg,data['template_dir'])
                chk_td=False
                continue

        if chk_ia:
            new_cfg="{0}\n'{1}.apps.{2}Config',".format(new_cfg,app_name,app_class_name)
            chk_ia=False

        if line.split(' ')[0] == 'ALLOWED_HOSTS':
            if 'server_ip' in data:
                new_cfg="{0}\nALLOWED_HOSTS = ['localhost','127.0.0.1','{1}']".format(new_cfg,data['server_ip'])
                continue
        elif line.split(' ')[0] == 'TEMPLATES':
            chk_td=True
        elif line.split(' ')[0] == 'TIME_ZONE':
            if 'time_zone' in data:
                new_cfg="{0}\nTIME_ZONE = '{1}'".format(new_cfg,data['time_zone'])
                continue
        elif line.split(' ')[0] == 'INSTALLED_APPS':
             chk_ia=True

        if len(new_cfg) == 0:
            new_cfg="{0}".format(line)
        else:
            new_cfg="{0}\n{1}".format(new_cfg,line)

    if 'fileupload_dir' in data:
        new_cfg="{0}\nMEDIA_URL = '/{1}/'\nMEDIA_ROOT = os.path.join(BASE_DIR, '{1}')".format(new_cfg,data['fileupload_dir'])
    if 'static_dir' in data:
        new_cfg="{0}\nSTATIC_URL = '/{1}/'\nSTATICFILES_DIRS = [os.path.join(BASE_DIR, '{1}'),]".format(new_cfg,data['static_dir'])
    
    with open(filename,'w') as f:
        f.write(new_cfg)
    

def model(django_home_path,prj_name,app_name):
    filename='{0}/{1}/{2}/models.py'.format(django_home_path,prj_name,app_name)
    print(filename)
    app_class_name=app_name.title()
    str_info='''
from django.db import models
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles

LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted((item, item) for item in get_all_styles())

class {0}(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    writer = models.CharField(max_length=25, blank=True, default='')
    email = models.CharField(max_length=45, blank=True, default='')
#    text = models.TextField(default='',blank=True)
#    attached_file = models.CharField(blank=True, default='')
#    relay = models.IntegerField(blank=True, default='')

    class Meta:
        ordering = ('created',)
'''.format(app_class_name)
    with open(filename,'a') as f:
        f.write(str_info)
     

def url(django_home_path,prj_name,app_name):
    filename='{0}/{1}/{1}/urls.py'.format(django_home_path,prj_name)
    print(filename)
    app_class_name=app_name.title()
    str_info='''from django.conf.urls import url,include
from django.contrib import admin

urlpatterns = [
    url(r'^', include('{0}.urls')),
]
'''.format(app_name)
    with open(filename,'w') as f:
        f.write(str_info)

    filename='{0}/{1}/{2}/urls.py'.format(django_home_path,prj_name,app_name)
    str_info='''from django.conf.urls import url,include
from {0} import views
from rest_framework.urlpatterns import format_suffix_patterns

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', views.list,name='home'),
#    url(r'^list/$', views.list,name='home'),
    url(r'^list/(?P<pk>\d+)/$', views.detail,name='detail'),
    url(r'^edit/(?P<pk>\d+)/$', views.edit,name='edit'),
    url(r'^delete/(?P<pk>\d+)/$', views.delete,name='delete'),
    url(r'^download/(?P<pk>\d+)/$', views.download,name='download'),
    url(r'^write/$', views.writeView.as_view(),name='write'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns = format_suffix_patterns(urlpatterns)
'''.format(app_name)
    with open(filename,'w') as f:
        f.write(str_info)




def seirializer(django_home_path,prj_name,app_name):
    filename='{0}/{1}/{2}/serializers.py'.format(django_home_path,prj_name,app_name)
    print(filename)
    app_class_name=app_name.title()
    str_info="""from rest_framework import serializers
from %s.models import %s, LANGUAGE_CHOICES, STYLE_CHOICES


class %sSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    created = serializers.CharField(required=False) # for Read created date at reading function.
                                                    # automatically create date at models.py when Adding data 
    writer = serializers.CharField(max_length=25,allow_blank=True,required=False)
    email = serializers.CharField(max_length=45,allow_blank=True,required=False)
#    text = serializers.CharField(style={'base_template': 'textarea.html'},allow_blank=True,required=False)
#    attached_file = serializers.CharField(required=False, allow_blank=True)
#    relay = serializers.IntegerField(allow_blank=True,required=False)

    class Meta:
        model = %s
#        fields = ('id', 'created','writer','email','text','attached_file', 'relay', 'qa')
        fields = ('id', 'created','writer','email')


    def create(self, validated_data):
        return %s.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
#        instance.attached_file = validated_data.get('attached_file', instance.attached_file)
#        instance.relay = validated_data.get('relay', instance.relay)
#        instance.text = validated_data.get('text', instance.text)
        instance.save()
        return instance
""" % (app_name,app_class_name,app_class_name,app_class_name,app_class_name)

    with open(filename,'w') as f:
        f.write(str_info)

def views(django_home_path,prj_name,app_name):
    filename='{0}/{1}/{2}/views.py'.format(django_home_path,prj_name,app_name)
    print(filename)
    app_class_name=app_name.title()
    str_info='''from django.shortcuts import render
import os
from django.conf import settings

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

#Convert to JSON
from django.http import HttpResponse, JsonResponse
from rest_framework.renderers import JSONRenderer
from django.utils.six import BytesIO
from rest_framework.parsers import JSONParser

from %s.models import %s
from %s.serializers import %sSerializer

#File Upload
from django.core.files.storage import FileSystemStorage

# Page (pip3 install django-bootstrap-pagination)
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator

# Celery
#from celery.task.control import inspect

# Others
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from .forms import WriteForm
#from .tasks import <task func name>
from django.contrib import messages
from django.shortcuts import redirect

#timezone
import time
import socket
import datetime
import pytz
from django.utils.timezone import get_current_timezone
import re

@api_view(['GET','POST'])
def list(request, format=None):
    if request.method == 'GET':
        hostname=socket.gethostname()
        #i = inspect()
        #get_tasks=i.reserved()
        #if get_tasks is None:
        #    reserved_task='Celery not ready'
        #else:
        #    reserved_task=len(get_tasks['celery@{0}'.format(hostname)])

        # Search
        search_item=''
        if 'item' in request.GET and request.GET['find_str'] and request.GET['find_str'] != 'all' and request.GET['find_str'] != '' :
            search_item=request.GET['item']
            search_item_val=request.GET['find_str']
            if search_item == 'find':
#                item = %s.objects.filter(<item field name>__contains=search_item_val).reverse()
                 pass
            elif search_item == 'date':
                tz=get_current_timezone()
                timezone=pytz.timezone(str(tz))
                date_range_arr=search_item_val.split(',')
                date_format=re.compile(r'(\d{4})-(\d{2})-(\d{2})')
                item = []

                if len(date_range_arr) == 1:
                    date_arr=date_format.findall(date_range_arr[0])
                    if len(date_arr)==0:
                        messages.success(request, 'Wrong date format(format ex: YYYY-MM-DD)')
                    else:
                        start_date=timezone.localize(datetime.datetime(int(date_arr[0][0]),int(date_arr[0][1]),int(date_arr[0][2]),0,0,0))
                        end_date=start_date + datetime.timedelta(hours=24)
                        item = %s.objects.filter(created__range=(start_date,end_date)).reverse()
                else:
                    if len(date_range_arr[0]) == 0 and len(date_range_arr[1]) > 0:
                        date_arr=date_format.findall(date_range_arr[1])
                        if len(date_arr)==0:
                            messages.success(request, 'Wrong date format(format ex: ,YYYY-MM-DD)')
                        else:
                            end_date=timezone.localize(datetime.datetime(int(date_arr[0][0]),int(date_arr[0][1]),int(date_arr[0][2]),0,0,0)) + datetime.timedelta(hours=24)
                            item = %s.objects.filter(created__lt=end_date).reverse()
                    elif len(date_range_arr[1]) == 0 and len(date_range_arr[0]) > 0:
                        date_arr=date_format.findall(date_range_arr[0])
                        if len(date_arr)==0:
                            messages.success(request, 'Wrong date format(format ex: YYYY-MM-DD,)')
                        else:
                            start_date=timezone.localize(datetime.datetime(int(date_arr[0][0]),int(date_arr[0][1]),int(date_arr[0][2]),0,0,0))
                            item = %s.objects.filter(created__gt=start_date).reverse()
                    else:
                        start_date_arr=date_format.findall(date_range_arr[0])
                        end_date_arr=date_format.findall(date_range_arr[1])
                        if len(start_date_arr)==0 or len(end_date_arr)==0:
                            messages.success(request, 'Wrong date format(format ex: YYYY-MM-DD,YYYY-MM-DD)')
                        else:
                            start_date=timezone.localize(datetime.datetime(int(start_date_arr[0][0]),int(start_date_arr[0][1]),int(start_date_arr[0][2]),0,0,0))
                            end_date=timezone.localize(datetime.datetime(int(end_date_arr[0][0]),int(end_date_arr[0][1]),int(end_date_arr[0][2]),23,59,59))
                            item = %s.objects.filter(created__range=(start_date,end_date)).reverse()

        else:
            item = %s.objects.all().reverse()
        # with Page
        p = Paginator(item, 25) # 25 items per page
        if 'page' in request.GET:
            page = request.GET.get('page')
        else:
            page=1
        contacts = p.page(page)
        return render(request, 'list.html', {'data': contacts,'localtime':time.strftime('%s'),'item':search_item})

    elif request.method == 'POST': # wget or request function
#        # File handle(get data from request data)
#        if 'filename' in request.data:
#            myfile = request.FILES['filename']
#            fs = FileSystemStorage() # Save file to media directory in the BASE_DIR
#            filename = fs.save(myfile.name, myfile)
#
#            # Add new filename to field 'run_log_file' in request data 
#            request.data['filename']=filename

        # Save original data to DB from request data
        serializer = %s(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

@api_view(['GET', 'PUT', 'DELETE'])
def detail(request, pk, format=None):
    try:
        item = %s.objects.get(pk=pk)
    except %s.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        if 'page' in request.GET:
           page=request.GET['page']
        else:
           page=1
        #return render(request, 'detail.html', {'ii': item, 'id': item.id, 'downfile': item.filename, 'page': page})
        return render(request, 'detail.html', {'ii': item, 'id': item.id, 'page': page})

@api_view(['GET'])
def download(request, pk, format=None):
    try:
        item = %s.objects.get(pk=pk)
    except %s.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = %sSerializer(item)

        # Convert serializer data to Json
        jsonsum = JSONRenderer().render(serializer.data)
        stream = BytesIO(jsonsum)
        data = JSONParser().parse(stream)
        filename = data['filename']
        filename_path=os.path.join(settings.MEDIA_ROOT, filename)

        with open(filename_path, 'rb') as fh:
           response = HttpResponse(fh.read(), content_type="application/force-download")
           response['Content-Disposition'] = 'inline; filename=' + filename
           return response

class writeView(FormView):
    #permission_required = ('<app name>.add_<app name>')
    login_url = 'home'
    redirect_field_name = 'redirect_to'

    template_name = 'write.html'
    form_class = WriteForm

    def form_valid(self,form):
#        if not self.request.user.is_authenticated:
#            return redirect('home')

        writer=''
        email=''
#        title=''
#        text=''
#        shell=''
#        group=''
#        done=''
        writer = form.cleaned_data.get('writer')
        email=form.cleaned_data.get('email')
#        title=form.cleaned_data.get('title')
#        text=form.cleaned_data.get('text')
#        shell=form.cleaned_data.get('shell')
#        group=form.cleaned_data.get('group')
#        done=form.cleaned_data.get('done')

#        attached_file=''
#        attache_files = self.request.FILES.getlist('attached_file')
#        attache_file_arr=[]
#        if form.is_valid():
#            for f in attache_files:
#                fs = FileSystemStorage()
#                attached_file=fs.save(f.name, f)
#                attache_file_arr.append(attached_file)
#                saved_file = fs.path(attached_file)
#                os.chmod(saved_file,0o644)
#        relay=0
#        # if replay item then set to 'qna' group
#        if 'pk' in self.kwargs:
#            relay=self.kwargs['pk']
#            group='qna'

#        data={'writer':writer, 'email':email, 'attached_file':'{0}'.format(attache_file_arr), 'relay':relay, 'group':group, 'done':done,  'title':title, 'text':text, 'shell':shell}
        data={'writer':writer, 'email':email}
        serializer = %sSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
        else:
            return HttpResponse(status=404) # Wrong format then notice

#        # Add my child to mine
#        if int(relay) > 0:
#            item = %s.objects.get(pk=relay)
#            sitem = %sSerializer(item)
#            jsonitem = JSONRenderer().render(sitem.data)
#            stream = BytesIO(jsonitem)
#            data = JSONParser().parse(stream)
#            if len(item.frelay) > 0:
#                data['frelay']='{0},{1}'.format(item.frelay,serializer.data['id'])
#            else:
#                data['frelay']='{0}'.format(serializer.data['id'])
#            updateS = %sSerializer(item, data=data)
#            if updateS.is_valid():
#                updateS.save()

        return redirect('home')

@api_view(['GET', 'POST'])
def edit(request, pk, format=None):
    #if not request.user.is_authenticated:
    #    return redirect('home')
    try:
        item = %s.objects.get(pk=pk)
    except %s.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET': # Edit screen
        if 'page' in request.GET:
           page=request.GET['page']
        else:
           page=1
        return render(request, 'edit.html', {'ii': item, 'page': page})

    elif request.method == 'POST': # Update DB
        try:
            item = %s.objects.get(pk=pk)
        except %s.DoesNotExist:
            return HttpResponse(status=404)

        serializer = %sSerializer(item, data=request.data)

        if serializer.is_valid():
            serializer.save()

        return redirect('home')

@api_view(['GET','POST'])
def delete(request, pk, format=None):
    #if not request.user.is_authenticated:
    #    return redirect('home')

    if request.method == 'GET':
        if 'page' in request.GET:
           page=request.GET['page']
        else:
           page=1
        return render(request, 'del.html', {'idx': pk, 'page': page})

    elif request.method == 'POST' and 'csrfmiddlewaretoken' in request.data:

        if pk == request.data['id'] and len(request.data['csrfmiddlewaretoken']) > 40:
            try:
                item = %s.objects.get(pk=pk)
            except %s.DoesNotExist:
                return HttpResponse(status=404)

            # Delete my item
            item.delete()
        return redirect('home')
''' % (app_name,app_class_name,app_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,'%X %x %Z',app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name,app_class_name)

    with open(filename,'w') as f:
        f.write(str_info)

def forms(django_home_path,prj_name,app_name):
    filename='{0}/{1}/{2}/forms.py'.format(django_home_path,prj_name,app_name)
    print(filename)
    app_class_name=app_name.title()
    str_info='''from django import forms
from django.core.validators import MinValueValidator, MaxValueValidator

class WriteForm(forms.Form):
    text_input_size=55
    writer = forms.CharField(label='Your Name',max_length=15,required=True, widget=forms.TextInput(attrs={'size':text_input_size}))
    email = forms.EmailField(label='Your Email',max_length=30,required=False, widget=forms.TextInput(attrs={'size':text_input_size}))
    #attached_file = forms.FileField(label='Select your file',required=False,widget=forms.ClearableFileInput(attrs={'multiple': True}))
    #text = forms.CharField(label='Desc',required=False,initial='',widget=forms.Textarea(attrs={'width':"100%", 'cols' : "80", 'rows': "20", }))
    #group_list=(
#('linux','Linux'),
#('shell','Shell'),
#('python','Python'),
#('qna','Q&A'),
#('done','Done'),
#)
#   group = forms.ChoiceField(label='Group',widget=forms.Select,choices=group_list)

'''
    with open(filename,'w') as f:
        f.write(str_info)

def shells(django_home_path,prj_name,app_name,data,is_celery=False,start=False):
    with open('{0}/{1}/run_server.sh'.format(django_home_path,prj_name),'w') as f:
        f.write('cd $(dirname $(readlink -f $0)) && python36 manage.py runserver {0}:{1} & '.format(data['server_ip'],data['server_port']))
    with open('{0}/{1}/run_server_cli.sh'.format(django_home_path,prj_name),'w') as f:
        f.write('cd $(dirname $(readlink -f $0)) && python36 manage.py runserver {0}:{1}'.format(data['server_ip'],data['server_port']))
    with open('{0}/{1}/reset_db.sh'.format(django_home_path,prj_name),'w') as f:
        f.write('cd $(dirname $(readlink -f $0)) && python36 manage.py flush')
    with open('{0}/{1}/migrate_db.sh'.format(django_home_path,prj_name),'w') as f:
        f.write('cd $(dirname $(readlink -f $0)) && python36 manage.py makemigrations {0} && python36 manage.py migrate'.format(app_name))
    if os.path.exists('/lib/systemd/system'):
        with open('/lib/systemd/system/{0}.service'.format(prj_name),'w') as f:
            f.write('''[Unit]
Description={1}ViewUI service

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/bin/sh -c 'sh {0}/{1}/run_server.sh'

[Install]
WantedBy=multi-user.target
'''.format(django_home_path,prj_name))
        if start:
           rc=km.rshell('systemctl daemon-reload; systemctl enable {0}; systemctl start {0}'.format(prj_name))
           print(rc[1])

        if is_celery:
            celery()
    

def celery(django_home_path,prj_name,app_name,data,tasks=None,broker='redis',start=False):
    #update task

    #Add celery
    str_info='''import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', '{0}.settings')

app = Celery('{0}')
# If define namespace='CELERY' then Celery use default AMPQ server
# if not define namespace then it use setting configuration
app.config_from_object('django.conf:settings')
app.autodiscover_tasks()


#DEBUG
@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
'''.format(prj_name)
    with open('{0}/{1}/{1}/celery.py'.format(django_home_path,prj_name),'w') as f:
        f.write(str_info)
    with open('{0}/{1}/{1}/__init__.py'.format(django_home_path,prj_name),'w') as f:
        f.write('''from __future__ import absolute_import
# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .celery import app as celery_app
__all__ = ['celery_app']
''')

    # add django_celery_beat,django_celery_results to setting INSTALLED_APPS
    if broker == 'redis':
        filename='{0}/{1}/{1}/settings.py'.format(django_home_path,prj_name)
        with open(filename,'a') as f:
            f.write('''
BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
CELERY_ACCEPT_CONTENT = ['application/json']''')
    else:
        filename='{0}/{1}/{1}/settings.py'.format(django_home_path,prj_name)
        with open(filename,'a') as f:
            f.write('''
BROKER_URL = 'amqp://guest:guest@localhost:5672//'
CELERY_ACCEPT_CONTENT = ['json'] to setting
''')

    filename='{0}/{1}/{1}/settings.py'.format(django_home_path,prj_name)
    with open(filename,'a') as f:
        f.write('''
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = '{0}' to setting
'''.format(data['time_zone']))

    if broker == 'redis':
        km.rshell('systemctl start redis; systemctl enable redis')
    else:
        km.rshell('systemctl start rabbitmq-server; systemctl enable rabbitmq-server')

    if tasks is None:
        with open('{0}/{1}/run_celery.sh'.format(django_home_path,prj_name),'w') as f:
            f.write('''cd $(dirname $(readlink -f $0)) && celery -A {0} worker -l info -f /var/log/{0}.Celery.log &
'''.format(prj_name,tasks))
    else:
        with open('{0}/{1}/run_celery.sh'.format(django_home_path,prj_name),'w') as f:
            f.write('''cd $(dirname $(readlink -f $0)) && celery -A {0} worker -E -c {1} -l info -f /var/log/{0}.Celery.log &
'''.format(prj_name,tasks))
    if tasks is None:
        with open('{0}/{1}/run_celery_cli.sh'.format(django_home_path,prj_name),'w') as f:
            f.write('''cd $(dirname $(readlink -f $0)) && celery -A {0} worker -l info
'''.format(prj_name,tasks))
    else:
        with open('{0}/{1}/run_celery_cli.sh'.format(django_home_path,prj_name),'w') as f:
            f.write('''cd $(dirname $(readlink -f $0)) && celery -A {0} worker -E -c {1} -l info
'''.format(prj_name,tasks))

    if os.path.exists('/lib/systemd/system'):
        with open('/lib/systemd/system/{0}Celery.service'.format(prj_name),'w') as f:
            f.write('''[Unit]
Description=${prj_name}Celery service

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/bin/sh -c 'sh {0}/{1}/run_celery.sh'

[Install]
WantedBy=multi-user.target
'''.format(django_home_path,prj_name))
        if start:
            rc=km.rshell('systemctl daemon-reload; systemctl enable {0}Celery; systemctl start {0}Celery'.format(prj_name))
            print(rc[1])


def write_template(msg,filename):
    with open(filename,'w') as f:
        f.write('''{% extends 'base.html' %}

{% block content %}
{0}
{% endblock %}
'''.format(msg))


def template(django_home_path,prj_name,app_name):
    if os.path.exists('{0}/{1}/{2}/templatetags'.format(django_home_path,prj_name,app_name)) is False:
        os.mkdir('{0}/{1}/{2}/templatetags'.format(django_home_path,prj_name,app_name))

    with open('{0}/{1}/{2}/templatetags/tag_library.py'.format(django_home_path,prj_name,app_name),'w') as f:
        f.write('''from django import template
from datetime import datetime

register = template.Library()

@register.filter()
def to_int(value):
    return int(value)

@register.filter()
def to_date_time(value):
    print(value)
    return datetime.fromtimestamp(int(value)).strftime("%b %d, %Y %H:%M %p")
''')

    if os.path.exists('{0}/{1}/templates'.format(django_home_path,prj_name)) is False:
        os.mkdir('{0}/{1}/templates'.format(django_home_path,prj_name))

    with open('{0}/{1}/templates/base.html'.format(django_home_path,prj_name),'w') as f:
        f.write('''<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    {% load staticfiles %}
    <title>Django APP Title</title>
  </head>
  <body>
    <table border=0>
    <td>
    {% if user.is_authenticated %}
       Hi {{ user.username }}!
       <a href="{% url 'logout' %}">logout</a>
    {% endif %}
    </td>
    <td>
    <a href="{% url 'home' %}">List</a>
    <a href="{% url 'write' %}">Write</a>
    </td>
    </table>
    <main>
      {% block content %}
      {% endblock %}
    </main>
  </body>
</html>
''')
    with open('{0}/{1}/templates/detail.html'.format(django_home_path,prj_name),'w') as f:
        f.write('''{% extends 'base.html' %}

{% block content %}
<br>

<table align=center border=0 width=1200>
<tr>
   <td colspan=4> [<a href=/list/?group={{ii.group}}{% if page %}&page={{page}}{% endif %}>Home</a>] [<a href=/edit/{{ii.id}}>Edit</a>] <!--[<a href=/reply/{{ii.id}}>reply</a>]--> [<a href=/delete/{{ii.id}}>del</a>] </td>
</tr>
<tr>
   <td>Item #</td><td>{{ii.id}}</td>
   <!--td>Group</td><td>{{ii.group}}</td-->
</tr>
<tr>
   <td colspan=4>{{ii.title}}</td>
</tr>
<tr>
   <td colspan=4><pre>{{ii.text}}
   <br> <p align=right>by {{ii.writer}} {% if ii.email %} ( {{ii.email}} ) {% endif %}
   <br> at {{ii.created}}</p></pre>
   </td>
</tr>
{% if ii.text %}
<tr>
   <td colspan=4><pre>{{ii.text}}</pre></td>
</tr>
<!--tr>
   <td colspan=4><pre>{{ii.crelay}}</pre></td>
</tr-->
{% endif %}
<tr>
   <td colspan=4>[<a href=/list/?group={{ii.group}}{% if page %}&page={{page}}{% endif %}>Home</a>] [<a href=/edit/{{ii.id}}>Edit</a>] <!--[<a href=/reply/{{ii.id}}>reply</a>]--> [<a href=/delete/{{ii.id}}>del</a>] </td>
</tr>
<!--tr>
   <td colspan=4> {% if downfile %} {% for ff in downfile %} <li><a href="/download/{{ii.id}}/?filename={{ff}}">download '{{ff}}' file</a></li>{% endfor %}{% endif %} </td>
</tr>
<tr>
   <td colspan=4> {% for rr in rep %}<li><a href=/list/{{rr.id}}>{{rr.title}}</a></li>{% endfor %} </td>
</tr-->
</table>
{% endblock %}
''')

    with open('{0}/{1}/templates/write.html'.format(django_home_path,prj_name),'w') as f:
        f.write('''{% extends 'base.html' %}

{% block content %}
  <form method="post" enctype="multipart/form-data">
    {% csrf_token %}
    <div class="rat_main_container">
      <div class="rat_sub_container">
         <table>
         <tr>
           <td>Name <span class="required">*</span></td> 
           <td>{{ form.writer }}</td>
         </tr>
         <tr>
           <td>Email</td> 
           <td>{{ form.email }}</td>
         </tr>
         </table>
      </div>
    </div>

    <!--div class="rat_main_container">
      <div class="rat_sub_container">
        <table>
          <tr>
            <td>Attache file</td> 
            <td>{{ form.attached_file }}</td>
          </tr>
        </table>
      </div>
    </div>

    <div class="rat_main_container">
      <div class="rat_sub_container">
         <table>
           <tr>
             <td>Board Group</td>
             <td>{{ form.group }}</td>
           </tr>
           <tr>
             <td>title *</td> 
             <td>{{ form.title }}</td>
           </tr>
           <tr>
             <td>Desc. *</td> 
             <td>{{ form.text }}</td>
           </tr>
           <tr>
             <td>Code</td> 
             <td>{{ form.shell }}</td>
           </tr>
           <tr>
             <td>Code reference #</td> 
             <td>{{ form.crelay }}</td>
           </tr>
         </table>
      </div>
    </div-->

    <button class="rat_submit_button" type="submit">Submit</button>
  </form>
{% endblock %}
''')

    with open('{0}/{1}/templates/rep.html'.format(django_home_path,prj_name),'w') as f:
        f.write('''{% extends 'base.html' %}

{% block content %}
  <form method="post" enctype="multipart/form-data">
    {% csrf_token %}
    <div class="rat_main_container">
      <div class="rat_sub_container">
         <table>
         <tr>
           <td>Name <span class="required">*</span></td> 
           <td>{{ form.writer }}</td>
         </tr>
         <tr>
           <td>Email</td> 
           <td>{{ form.email }}</td>
         </tr>
         </table>
      </div>
    </div>

    <!--div class="rat_main_container">
      <div class="rat_sub_container">
        <table>
          <tr>
            <td>Attache file</td> 
            <td>{{ form.attached_file }}</td>
          </tr>
        </table>
      </div>
    </div>

    <div class="rat_main_container">
      <div class="rat_sub_container">
         <table>
           <tr>
             <td>Board Group</td>
             <td>{{ form.group }}</td>
           </tr>
           <tr>
             <td>title *</td> 
             <td>{{ form.title }}</td>
           </tr>
           <tr>
             <td>Desc. *</td> 
             <td>{{ form.text }}</td>
           </tr>
           <tr>
             <td>Code</td> 
             <td>{{ form.shell }}</td>
           </tr>
           <tr>
             <td>Code reference #</td> 
             <td>{{ form.crelay }}</td>
           </tr>
         </table>
      </div>
    </div-->

    <button class="rat_submit_button" type="submit">Submit</button>
  </form>
{% endblock %}
''')

    with open('{0}/{1}/templates/home.html'.format(django_home_path,prj_name),'w') as f:
        f.write('''{% extends 'base.html' %}

{% block title %}Home{% endblock %}

{% block content %}
<!-- { % if user.is_authenticated == False % } -->
{% if user.is_authenticated %}
  Hi {{ user.username }}!
  <p><a href="{% url 'logout' %}">logout</a></p>
{% else %}
  <p>You are not logged in</p>
  <a href="{% url 'login' %}">login</a>
{% endif %}
{% endblock %}
''')

    with open('{0}/{1}/templates/edit.html'.format(django_home_path,prj_name),'w') as f:
        f.write('''{% extends 'base.html' %}

{% block content %}
  <form method="post">
    {% csrf_token %}
    <input name='id' type=hidden value={{ii.id}}>
         <table>
         <tr>
           <td>Name <span class="required">*</span></td> 
           <td>{{ ii.writer }}</td>
         </tr>
         <tr>
           <td>Email</td> 
           <td><input type=text name=email value="{{ ii.email }}"></td>
         </tr>
         </table>

        <!--table>
          <tr>
            <td>Attache file</td> 
            <td>{{ form.attached_file }}</td>
          </tr>
        </table-->

         <!--table>
           <tr>
             <td>Board Group</td>
             <td><select name="group" id="id_group">
  {% if ii.group == 'linux' %}
      <option value="linux" selected>Linux</option>
  {% else %}
      <option value="linux">Linux</option>
  {% endif %}
  {% if ii.group == 'shell' %}
      <option value="shell" selected>Shell</option>
  {% else %}
      <option value="shell">Shell</option>
  {% endif %}
  {% if ii.group == 'python' %}
      <option value="python" selected>Python</option>
  {% else %}
      <option value="python">Python</option>
  {% endif %}
  {% if ii.group == 'qna' %}
      <option value="qna" selected>Q&A</option>
  {% else %}
      <option value="qna">Q&A</option>
  {% endif %}
  {% if ii.group == 'done' %}
      <option value="done" selected>Done</option>
  {% else %}
      <option value="done">Done</option>
  {% endif %}


</select></td>
           </tr>
           <tr>
             <td>title</td> 
             <td><input type=text size=81 name=title value="{{ ii.title }}"></td>
           </tr>
           <tr>
             <td>Desc.</td> 
             <td><textarea name=text rows=20 cols=80>{{ ii.text }}</textarea></td>
           </tr>
           <tr>
             <td>Code</td> 
             <td><textarea name=shell rows=20 cols=80>{{ ii.shell }}</textarea></td>
           </tr>
           <tr>
             <td>Code reference#</td> 
             <td><input type=text size=81 name=crelay value="{{ ii.crelay }}"></td>
           </tr>
         </table-->

    <input type="submit">
  </form>
{% endblock %}
''')

    with open('{0}/{1}/templates/done.html'.format(django_home_path,prj_name),'w') as f:
        f.write('''{% extends 'base.html' %}

{% block content %}
  <form method="post">
    {% csrf_token %}
    <input name='id' type=hidden value={{idx}}>
    Fixed {{idx}}, Are you sure?
    <button type="submit">Yes</button>
    <button type="cancel">No</button>
  </form>
{% endblock %}
''')

    with open('{0}/{1}/templates/error.html'.format(django_home_path,prj_name),'w') as f:
        f.write('''{% extends 'base.html' %}

{% block content %}
    <pre>{{msg}}</pre>
{% endblock %}
''')

    with open('{0}/{1}/templates/del.html'.format(django_home_path,prj_name),'w') as f:
        f.write('''{% extends 'base.html' %}

{% block content %}
  <form method="post">
    {% csrf_token %}
    <input name='id' type=hidden value={{idx}}>
    Delete {{idx}}, Are you sure?
    <button type="submit">Yes</button>
    <button type="cancel">No</button>
  </form>
{% endblock %}
''')


    with open('{0}/{1}/templates/list.html'.format(django_home_path,prj_name),'w') as f:
        f.write('''{% extends 'base.html' %}

{% block content %}
<!-- 
Example : make a user tag library and use data convert
{% load tag_library %}
{{<value>|to_int}}
{{<value>|to_date_time}}
-->
<style type="text/css">
    tr:nth-child(even){background-color: #f1f1f1}
</style>

<div class="st_status_container">
  <i>Local Time:</i> <b>{{localtime }}</b>
</div>

<div class="st_search_container">
  <table align=center cellspacing="0">
    <tr>
    <td colspan=10 align=center>
      <form action='/list/' method='get'>
         <select name='group'>
        {% if group == 'all' %}
         <option value='all' selected>All</option>
        {% else %}
         <option value='all'>All</option>
        {% endif %}
        {% if group == 'shell' %}
         <option value='shell' selected>Shell</option>
        {% else %}
         <option value='shell'>Shell</option>
        {% endif %}
        {% if group == 'linux' %}
         <option value='linux' selected>Linux</option>
        {% else %}
         <option value='linux'>Linux</option>
        {% endif %}
        {% if group == 'python' %}
         <option value='python' selected>Python</option>
        {% else %}
         <option value='python'>Python</option>
        {% endif %}
        {% if group == 'qna' %}
         <option value='qna' selected>Q&A</option>
        {% else %}
         <option value='qna'>Q&A</option>
        {% endif %}
        {% if group == 'done' %}
         <option value='done' selected>Done</option>
        {% else %}
         <option value='done'>Done</option>
        {% endif %}
         </select>

         <select name='item'>
        {% if item == 'title' %}
         <option value='title' selected>Title</option>
        {% else %}
         <option value='title'>Title</option>
        {% endif %}
        {% if item == 'group' %}
         <option value='group' selected>Group</option>
        {% else %}
         <option value='group'>Group</option>
        {% endif %}
        {% if item == 'text' %}
         <option value='text' selected>TEXT</option>
        {% else %}
         <option value='text'>TEXT</option>
        {% endif %}
        {% if item == 'shell' %}
         <option value='shell' selected>SHELL Code</option>
        {% else %}
         <option value='shell'>SHELL Code</option>
        {% endif %}
        {% if item == 'writer' %}
         <option value='writer' selected>Writer</option>
        {% else %}
         <option value='writer'>Writer</option>
        {% endif %}
        {% if item == 'email' %}
         <option value='email' selected>Email</option>
        {% else %}
         <option value='email'>Email</option>
        {% endif %}
        {% if item == 'attached_file' %}
         <option value='attached_file' selected>Attache File</option>
        {% else %}
         <option value='attached_file'>Attache File</option>
        {% endif %}
        {% if item == 'date' %}
         <option value='date' selected>Date</option>
        {% else %}
         <option value='date'>Date</option>
        {% endif %}
         </select>
        <input type='text' name='find_str'>
        <input type='submit' value='Search'>
      </form>
    </td>
    </tr>
  </table>
</div>

<table align=center cellspacing="0" class="st_data_table">
<tr>
   <td width=185 align=center>Date       </td>
   <td width=100 align=right>Writer     </td>
   <td width=50 align=right>Group      </td>
   <td width=520 align=right>Title       </td>
</tr>
    {% for ss in data %}
        <tr>
           <td width=185 align=center nowrap>{{ss.created}}</td>
           <td width=100 align=right  nowrap>{{ss.writer}}</td>
           <td width=50 align=right  nowrap>{{ss.group}}</td>
           {% if ss.title %}
               <td width=520 align=right  nowrap><a href=/list/{{ss.id}}>{{ss.title}}</a></td>
           {% else %}
               <td width=520 align=right  nowrap><a href=/list/{{ss.id}}>null</a></td>
           {% endif %}
        </tr>
    {% endfor %}
<tr>
</table>

        {% if data.has_previous %}
            <a href="?page=1">&laquo; first</a>
            <a href="?page={{ data.previous_page_number }}">previous</a>
        {% endif %}

        {% if data.has_next %}
        <span class="current">
            {{ data.number }} of {{ data.paginator.num_pages }}
        </span>
        {% endif %}

        {% if data.has_next %}
            <a href="?page={{ data.next_page_number }}">next</a>
            <a href="?page={{ data.paginator.num_pages }}">last &raquo;</a>
        {% endif %}

{% endblock %}
''')


# Make project and app
rc=km.rshell('cd {0} && django-admin.py startproject {1}'.format(django_home_path,prj_name))
print(rc[1])
rc=km.rshell('cd {0}/{1} && django-admin.py startapp {2}'.format(django_home_path,prj_name,app_name))
print(rc[1])

# Default settting
default_setting(django_home_path,app_name,prj_name,data=data)

# configure files
model(django_home_path,prj_name,app_name)
url(django_home_path,prj_name,app_name)
seirializer(django_home_path,prj_name,app_name)
views(django_home_path,prj_name,app_name)
forms(django_home_path,prj_name,app_name)
template(django_home_path,prj_name,app_name)

# make conf
shells(django_home_path,prj_name,app_name,data,is_celery=False,start=False)

print("make db")
rc=km.rshell('cd {0}/{1} && python36 manage.py makemigrations {2}'.format(django_home_path,prj_name,app_name))
print(rc[1])

print("Data migration")
rc=km.rshell('cd {0}/{1} && python36 manage.py migrate'.format(django_home_path,prj_name))
print(rc[1])