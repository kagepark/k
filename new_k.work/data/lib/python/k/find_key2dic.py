def find_val(dic,search,correct=False,symbol=' '):
    if type(dic) is dict:
        for k in dic.keys():
            k_type=type(dic[k])
            if k_type is dict: # dict then try sub dict
                frc=find_val(dic[k],search,correct,symbol)
                if frc is not None:
                    return '{0}/{1}'.format(k,frc)
            elif k_type is str: # data then find
                if correct:
                    if search in dic[k].split(symbol):
                        return k
                else:
                    if type(search) is not bool and type(dic[k]) is not bool and search in dic[k]:
                        return k
            elif k_type is list or k_type is tuple: # data(list,tuple) then find 
                if correct:
                    if search in dic[k]:
                        return k
                else:
                    for dk in dic[k]:
                        if type(search) is bool:
                            if dk == search:
                                return k
                        else:
                            if type(dk) is not bool and search in dk:
                                return k
            elif k_type is bool:
                if search == dic[k]:
                    return k

def find_key(dic,search,correct=False):
    search_type=type(search)
    if type(dic) is dict and (search_type is str or search_type is int):
        for k in dic.keys():
            if correct:
                if search == k:
                    return k
            else:
                if type(k) is not bool and search in k:
                    return k
            if type(dic[k]) is dict: # Not found then try sub dict
                frc=find_key(dic[k],search,correct)
                if frc is not None:
                    return '{0}/{1}'.format(k,frc)

def find_key2dic(dic,search,opt='all',correct=False,split=' '):
    found_key=None
    if opt == 'key' or opt == 'all':
        found_key=find_key(dic,search,correct)
    if found_key is None:
        found_key=find_val(dic,search,correct,split)
    if found_key is not None:
        return '/{0}'.format(found_key)