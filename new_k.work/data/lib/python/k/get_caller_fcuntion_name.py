import sys

def get_caller_fcuntion_name(detail=False):
    try:
        if detail:
            return sys._getframe(2).f_code.co_name,sys._getframe(2).f_lineno,sys._getframe(2).f_code.co_filename
        else:
            return sys._getframe(2).f_code.co_name
    except:
        return False