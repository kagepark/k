import inspect
def get_function_args(func):
    args, varargs, keywords, defaults = inspect.getargspec(func)
    if defaults is not None:
        return dict(zip(args[-len(defaults):], defaults))

