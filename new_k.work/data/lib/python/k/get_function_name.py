import traceback
def get_function_name():
    return traceback.extract_stack(None, 2)[0][2]
