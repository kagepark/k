def get_method_in_class(class_name):
    ret = dir(class_name)
    if hasattr(class_name,'__bases__'):
        for base in class_name.__bases__:
            ret = ret + get_method_in_class(base)