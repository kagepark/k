import os
#include rshell
def git_ver(git_dir=None):
    if git_dir is not None and os.path.isdir('{0}/.git'.format(git_dir)):
        gver=rshell('''cd {0} && git describe'''.format(git_dir))
        branch=rshell('''cd {0} && git branch| head -1'''.format(git_dir))
        tag=rshell('''cd {0} && git tag|tail -1'''.format(git_dir))
        if branch[0] == 0:
            if branch[1].split(' ')[1] == 'master':
                return gver[1]
            else:
                return gver[1].replace(tag[1],'v{0}'.format(branch[1].split(' ')[1]))
    return

