def human_byte(num, unit="K", wunit=None):
    units = ["B","K","M","G","T","P"]
    if wunit is None:
        unit=unit.upper()
        if num >= 1000:
            while num >= 1000:
               sui=units.index(unit)
               if len(units) <= sui+1:
                   break
               num /= 1024.0
               unit = units[sui+1]
        elif num < 1:
            while num < 1: #0.XXX digits
               sui=units.index(unit)
               if sui-1 < 0:
                   break
               num *= 1024.0
               unit = units[sui-1]
    else:
        wunit=wunit.upper()
        eui=units.index(wunit)
        if sui < eui:
            for i in range(sui,eui):
                num /= 1024.0
        elif sui > eui:
            for i in range(sui,eui,-1):
                num *= 1024.0
        unit=wunit
    if unit == 'B':
        return "%.1f %s" % (num,unit)
    else:
        return "%.1f %sB" % (num,unit)