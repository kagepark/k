#include is_ipv4
import socket
import struct
def ip2num(ip):
    if is_ipv4(ip):
        return struct.unpack("!L", socket.inet_aton(ip))[0]
    return False
