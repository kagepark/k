#include ip2num
def ip_in_range(ip,start,end):
    if type(ip) is str and type(start) is str and type(end) is str:
        ip=ip2num(ip)
        start=ip2num(start)
        end=ip2num(end)
        if start <= ip and ip <= end:
            return True
    return False
