#include rshell
def ipmi_cmd(cmd,ipmi_ip=None,ipmi_user='ADMIN',ipmi_pass='ADMIN'):
    if ipmi_ip is None:
        #ipmi_str=""" ipmitool {0} 2>/dev/null""".format(cmd)
        ipmi_str=""" ipmitool {0} """.format(cmd)
    else:
        #ipmi_str=""" ipmitool -I lanplus -H {0} -U {1} -P {2} {3} /dev/null""".format(ipmi_ip,ipmi_user,ipmi_pass,cmd)
        ipmi_str=""" ipmitool -I lanplus -H {0} -U {1} -P {2} {3} """.format(ipmi_ip,ipmi_user,ipmi_pass,cmd)
    return rshell(ipmi_str)

