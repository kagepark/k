def ipv4(ipaddr=None):
    if type(ipaddr) is str:
        for i in range(0,3):
            if ipaddr.find('.0') < 0:
                break
            ipaddr=ipaddr.replace('.0','.')
    return ipaddr
