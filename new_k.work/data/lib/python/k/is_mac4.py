
def is_mac4(mac=None,symbol=':'):
    if mac is None or type(mac) is not str:
        return False
    octets = mac.split(symbol)
    if len(octets) != 6:
        return False
    for i in octets:
        try:
           if len(i) != 2 or int(i, 16) > 255:
               return False
        except:
           return False
    return True

