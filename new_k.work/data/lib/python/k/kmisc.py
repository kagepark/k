#!/bin/python
# -*- coding: utf-8 -*-
#Kage stuff
from __future__ import print_function
import sys,os,re,subprocess,traceback,copy
import requests
import tarfile
import tempfile
import inspect
from datetime import datetime
from os import close, remove

ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')

def error_exit(msg=None):
    if msg is not None:
       print(msg)
    sys.exit(-1)


def dget(dict=None,keys=None):
    if dict is None or keys is None:
        return False
    tmp=dict.copy()
    for ii in keys.split('/'):
        if ii in tmp:
           dtmp=tmp[ii]
        else:
           return False
        tmp=dtmp
    return tmp

def dput(dic=None,keys=None,val=None,force=False,safe=True):
    if dic is None or keys is None:
        return False
    tmp=dic
    keys_arr=keys.split('/')
    keys_num=len(keys_arr)
    for ii in keys_arr[:(keys_num-1)]:
        if ii in tmp:
           if type(tmp[ii]) == type({}):
               dtmp=tmp[ii]
           else:
               if tmp[ii] == None:
                   tmp[ii]={}
                   dtmp=tmp[ii]
               else:
                   if force:
                      vtmp=tmp[ii]
                      tmp[ii]={vtmp:None}
                      dtmp=tmp[ii]
                   else:
                      return False
        else:
           if force:
               tmp[ii]={}
               dtmp=tmp[ii]
           else:
               return False
        tmp=dtmp
    if val == '_blank_':
       val={}
    if keys_arr[keys_num-1] in tmp.keys():
        if safe:
            ttype=type(tmp[keys_arr[keys_num-1]])
            if tmp[keys_arr[keys_num-1]] is None or ((ttype is str or ttype is list or ttype is dict) and len(tmp[keys_arr[keys_num-1]]) == 0):
               tmp.update({keys_arr[keys_num-1]:val})
            else:
               return False
        tmp.update({keys_arr[keys_num-1]:val})
    else:
        if force is False:
            return False
        tmp.update({keys_arr[keys_num-1]:val})

def is_py3():
    if sys.version_info[0] >= 3:
        return True
    return False


def rshell(cmd,timeout=None,ansi=True):
    Popen=subprocess.Popen
    PIPE=subprocess.PIPE
    start_time=datetime.now().strftime('%s')

    p = Popen(cmd , shell=True, stdout=PIPE, stderr=PIPE)
    out=None
    err=None
    try:
        if timeout is None:
            out, err = p.communicate()
        else:
            out, err = p.communicate(timeout=timeout)
        if is_py3():
            if ansi:
                #return p.returncode, out.decode("utf-8").rstrip(), err.decode("utf-8"),start_time,datetime.now().strftime('%s')
                return p.returncode, out.decode("ISO-8859-1").rstrip(), err.decode("ISO-8859-1"),start_time,datetime.now().strftime('%s')
            else:
                return p.returncode, ansi_escape.sub('',out).decode("ISO-8859-1").rstrip(), ansi_escape.sub('',err).decode("ISO-8859-1"),start_time,datetime.now().strftime('%s')
        else:
            if ansi:
                return p.returncode, out.rstrip(), err,start_time,datetime.now().strftime('%s')
            else:
                return p.returncode, ansi_escape.sub('',out).rstrip(), ansi_escape.sub('',err),start_time,datetime.now().strftime('%s')
    except subprocess.TimeoutExpired:
        p.kill()
        return p.returncode, 'Kill process after timeout ({0} sec)'.format(timeout), 'Error: Kill process after Timeout {0}'.format(timeout),start_time,datetime.now().strftime('%s')


def sendanmail(to,subj,msg,html=True):
    if html:
        email_msg='''To: {0}
Subject: {1}
Content-Type: text/html
<html>
<body>
<pre>
{2}
</pre>
</body>
</html>'''.format(to,subj,msg)
    else:
        email_msg=''
    cmd='''echo "{0}" | sendmail -t'''.format(email_msg)
    return rshell(cmd)

def is_ipv4(ipadd=None):
    ipa = ipadd.split(".")
    if len(ipa) != 4:
        return False
    for ip in ipa:
        if not ip.isdigit():
            return False
        if not 0 <= int(ip) <= 255:
            return False
    return True


def get_function_name():
    return traceback.extract_stack(None, 2)[0][2]

def ipmi_cmd(ipmi_ip,ipmi_user,ipmi_pass,cmd):
    ipmi_str=""" ipmitool -I lanplus -H {0} -U {1} -P {2} {3} 2>/dev/null""".format(ipmi_ip,ipmi_user,ipmi_pass,cmd)
    return rshell(ipmi_str)

def get_ipmi_mac(ipmi_ip,ipmi_user,ipmi_pass):
    ipmi_mac_str=""" ipmitool -I lanplus -H {0} -U {1} -P {2} lan print 2>/dev/null | grep "MAC Address" | awk """.format(ipmi_ip,ipmi_user,ipmi_pass)
    ipmi_mac_str=ipmi_mac_str + """ '{print $4}' """
    return rshell(ipmi_mac_str)


def rm_file(filelist):
    if type(filelist) == type([]):
       filelist_tmp=filelist
    else:
       filelist_tmp=filelist.split(',')
    for ii in list(filelist_tmp):
        if os.path.isfile(ii):
            os.unlink(ii)
        else:
            print('not found {0}'.format(ii))

def make_tar(filename,filelist,ctype='gz'):
    if ctype == 'bz2':
        tar = tarfile.open(filename,"w:bz2")
    elif ctype == 'stream':
        tar = tarfile.open(filename,"w:")
    else:
        tar = tarfile.open(filename,"w:gz")
    if type(filelist) == type([]):
       filelist_tmp=filelist
    else:
       filelist_tmp=filelist.split(',')
    for ii in list(filelist_tmp):
        if os.path.isfile(ii):
            tar.add(ii)
        else:
            print('not found {0}'.format(ii))
    tar.close()


def cut_string(string,len1=None,len2=None):
    if type(string) != str:
       string='{0}'.format(string)
    str_len=len(string)
    
    if len1 is None or len1 >= str_len:
       return [string]
       
    if len2 is None:
        rc=[string[i:i + len1] for i in range(0, str_len, len1)]
        return rc
    rc=[]
    rc.append(string[0:len1])
    string_tmp=string[len1:]
    string_tmp_len=len(string_tmp)
    for i in range(0, int(string_tmp_len/len2)+1):
        if (i+1)*len2 > string_tmp_len:
           rc.append(string_tmp[len2*i:])
        else:
           rc.append(string_tmp[len2*i:(i+1)*len2])
    return rc

def is_tempfile(filepath,tmp_dir='/tmp'):
   filepath_arr=filepath.split('/')
   if len(filepath_arr) == 1:
      return False
   tmp_dir_arr=tmp_dir.split('/')
   
   for ii in range(0,len(tmp_dir_arr)):
      if filepath_arr[ii] != tmp_dir_arr[ii]:
          return False
   return True


def mktemp(prefix='tmp-',suffix=None,opt='dry'):
   dir=os.path.dirname(prefix)
   if dir == '.':
       dir=os.path.realpath(__file__)
   elif dir == '':
       dir='/tmp'

   pfilename, file_ext = os.path.splitext(prefix)
   filename=os.path.basename(pfilename)
   if suffix is not None:
       if len(file_ext) == 0:
          file_ext='.{0}'.format(suffix)

   dest_file='{0}/{1}{2}'.format(dir,filename,file_ext)
   if opt == 'file':
      if os.path.isfile(dest_file):
          return tempfile.TemporaryFile(prefix='{0}-'.format(filename),suffix=file_ext,dir=dir)
      else:
          os.mknod(dest_file)
          return dest_file
   elif opt == 'dir':
      if os.path.isfile(dest_file) or os.path.isdir(dest_file):
          return tempfile.TemporaryDirectory(suffix='{0}-'.format(filename),prefix=prefix,dir=dir)
      else:
          os.mkdir(dest_file)
          return dest_file
   else:
      if os.path.isfile(dest_file):
         return tempfile.mktemp(prefix='{0}-'.format(filename),suffix=file_ext,dir=dir)
      else:
         return dest_file

def append2list(src,*add):
   if type(src) == str:
      org=[]
      for jj in src.split(','):
         org.append(jj)
   elif type(src) == list:
      org=copy.deepcopy(src)
   elif type(src) == int or type(src) == float:
      org=[]
      org.append(src)
   else:   
      return False

   if len(add) < 0:
       return False
   for ii in list(add):
      if type(ii) == type([]):
         for jj in list(ii):
            org.append(jj)
      else:
         for jj in ii.split(','):
            org.append(jj)
   return org


def isfile(filename=None):
   if filename is None:
      return False
   if len(filename) == 0:
      return False
   if os.path.isfile(filename):
      return True
   return False


def ping(host,test_num=3):
    rc=rshell("ping -c {0} {1}".format(test_num,host))
    if rc[0] == 0:
       return True
    else:
       return False


def get_function_args(func):
    args, varargs, keywords, defaults = inspect.getargspec(func)
    if defaults is not None:
        return dict(zip(args[-len(defaults):], defaults))

def get_function_list(obj=None):
    aa={}
    if obj is None:
        obj=__name__
    for name,obj in inspect.getmembers(sys.modules[obj]):
        if inspect.isfunction(obj): # inspect.ismodule(obj) check the obj is module or not
            aa.update({name:obj})
    return aa

def space(space_num=0,_space_='   '):
    space_str=''
    for ii in range(space_num):
        space_str='{0}{1}'.format(space_str,_space_)
    return space_str

def tap_print(string,bspace='',rc=False,NFLT=False):
    rc_str=None
    if type(string) is str:
        for ii in string.split('\n'):
            if NFLT:
               line='%s'%(ii)
               NFLT=False
            else:
               line='%s%s'%(bspace,ii)
            if rc_str is None:
               rc_str='%s'%(line)
            else:
               rc_str='%s\n%s'%(rc_str,line)
    else:
        rc_str='%s%s'%(bspace,string)

    if rc:
        return rc_str
    else:
        print(rc_str)

def str_format_print(string,rc=False):
    if type(string) is str:
        if len(string.split("'")) > 1:
            rc_str='"%s"'%(string)
        else:
            rc_str="'%s'"%(string)
    else:
        rc_str=string
    if rc:
        return rc_str
    else:
        print(rc_str)

def format_print(string,rc=False,num=0,bstr=None,NFLT=False):
    string_type=type(string)
    rc_str=''
    chk=None
    bspace=space(num)

    # Start Symbol
    if string_type is tuple:
        if bstr is None:
            if NFLT:
                rc_str='%s('%(rc_str)
            else:
                rc_str='%s%s('%(bspace,rc_str)
        else:
            rc_str='%s,\n%s%s('%(bstr,bspace,rc_str)
    elif string_type is list:
        if bstr is None:
            if NFLT:
                rc_str='%s['%(rc_str)
            else:
                rc_str='%s%s['%(bspace,rc_str)
        else:
            rc_str='%s,\n%s%s['%(bstr,bspace,rc_str)
    elif string_type is dict:
        if bstr is None:
            rc_str='%s{'%(rc_str)
        else:
            rc_str='%s,\n%s %s{'%(bstr,bspace,rc_str)
    rc_str='%s\n%s '%(rc_str,bspace)

    # Print string
    if string_type is list or string_type is tuple:
       for ii in list(string):
           ii_type=type(ii)
           if ii_type is tuple or ii_type is list or ii_type is dict:
               if not ii_type is dict:
                  num=num+1
               rc_str=format_print(ii,num=num,bstr=rc_str,rc=True)
           else:
               if chk == None:
                  rc_str='%s%s'%(rc_str,tap_print(str_format_print(ii,rc=True),rc=True))
                  chk='a'
               else:
                  rc_str='%s,\n%s'%(rc_str,tap_print(str_format_print(ii,rc=True),bspace=bspace+' ',rc=True))
    elif string_type is dict:
       for ii in string.keys():
           ii_type=type(string[ii])
           if ii_type is dict or ii_type is tuple or ii_type is list:
               num=num+1
               if ii_type is dict:
                   tmp=format_print(string[ii],num=num,rc=True)
               else:
                   tmp=format_print(string[ii],num=num,rc=True,NFLT=True)
               rc_str="%s,\n%s %s:%s"%(rc_str,bspace,str_format_print(ii,rc=True),tmp)
           else:
               if chk == None:
                  rc_str='%s%s'%(rc_str,tap_print("{0}:{1}".format(str_format_print(ii,rc=True),str_format_print(string[ii],rc=True)),rc=True))
                  chk='a'
               else:
                  rc_str='%s,\n%s'%(rc_str,tap_print("{0}:{1}".format(str_format_print(ii,rc=True),str_format_print(string[ii],rc=True)),bspace=bspace+' ',rc=True))

    # End symbol
    if string_type is tuple:
        rc_str='%s\n%s)'%(rc_str,bspace)
    elif string_type is list:
        rc_str='%s\n%s]'%(rc_str,bspace)
    elif string_type is dict:
        if bstr is None:
            rc_str='%s\n%s}'%(rc_str,bspace)
        else:
            rc_str='%s\n%s }'%(rc_str,bspace)

    else:
       rc_str=string

    # Output
    if rc:
       return rc_str    
    else:
       print(rc_str)


def str2url(string):
    return string.replace('/','%2F').replace(':','%3A').replace('=','%3D').replace(' ','+')