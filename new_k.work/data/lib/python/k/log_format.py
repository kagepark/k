#include timechk
#include get_caller_fcuntion_name

def log_format(*msg):
    if len(msg) > 0:
        m_str=None
        intro=''
        if log_intro > 0:
            intro='[{0}]'.format(timechk(tformat='%m/%d/%Y %H:%M:%S'))
        if log_intro > 1:
            intro=intro+' {0}()'.format(get_caller_fcuntion_name())
        if intro == '':
           for m in list(msg):
               if m_str is None:
                   m_str=m
               else:
                   m_str='{0} {1}'.format(m_str,m)
        else:
           intro_space=''
           for i in range(0,len(intro)):
               intro_space=intro_space+' '
           for m in list(msg):
               if m_str is None:
                   m_str='{0} {1}'.format(intro,m)
               else:
                   m_str='{0}\n{1} {2}'.format(m_str,intro_space,m)
        return m_str
