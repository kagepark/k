#include log_format
import os
def logging(*msg):
    if log_new_line == '':
        log_intro=0
    else:
        log_intro=1
    m_str=log_format(*msg)
    def log2file(msg_str,log_file):
        dirname=os.path.dirname(log_file)
        if os.path.isdir(dirname):
            with open(log_file,'a+') as f:
                #f.write(msg_str+'\n')
                f.write(msg_str+log_new_line)
    if m_str is not None:
        if type(log_file) is str:
           log2file(m_str,log_file)
        elif type(log_file) is list:
           for logf in list(log_file):
               log2file(m_str,logf)
        else:
           print(m_str)
