import tarfile
import os
def make_tar(filename,filelist,ctype='gz',ignore_file=None):
    if ctype == 'bz2':
        tar = tarfile.open(filename,"w:bz2")
    elif ctype == 'stream':
        tar = tarfile.open(filename,"w:")
    else:
        tar = tarfile.open(filename,"w:gz")
    if type(filelist) == type([]):
       filelist_tmp=filelist
    else:
       filelist_tmp=filelist.split(',')
    for ii in list(filelist_tmp):
        if os.path.isfile(ii):
            if ignore_file is not None:
                if ignore_file == os.path.basename(ii):
                    continue
            tar.add(ii)
        elif os.path.isdir(ii):
            for r,d,f in os.walk(ii):
                for ff in f:
                    if ignore_file is not None:
                        if ignore_file == ff:
                            continue
                    tar.add(os.path.join(r,ff))
        else:
            print('not found {0}'.format(ii))
    tar.close()

