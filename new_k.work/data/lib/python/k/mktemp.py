import os
import tempfile

def mktemp(prefix='tmp-',suffix=None,opt='dry'):
   dir=os.path.dirname(prefix)
   if dir == '.':
       dir=os.path.realpath(__file__)
   elif dir == '':
       dir='/tmp'

   pfilename, file_ext = os.path.splitext(prefix)
   filename=os.path.basename(pfilename)
   if suffix is not None:
       if len(file_ext) == 0:
          file_ext='.{0}'.format(suffix)

   dest_file='{0}/{1}{2}'.format(dir,filename,file_ext)
   if opt == 'file':
      if os.path.exists(dest_file):
          return tempfile.TemporaryFile(prefix='{0}-'.format(filename),suffix=file_ext,dir=dir)
      else:
          os.mknod(dest_file)
          return dest_file
   elif opt == 'dir':
      if os.path.exists(dest_file):
          return tempfile.TemporaryDirectory(suffix='{0}-'.format(filename),prefix=prefix,dir=dir)
      else:
          os.mkdir(dest_file)
          return dest_file
   else:
      if os.path.exists(dest_file):
         return tempfile.mktemp(prefix='{0}-'.format(filename),suffix=file_ext,dir=dir)
      else:
         return dest_file

