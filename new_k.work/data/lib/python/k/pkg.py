import yum

def pkg(pkgs):
    yb=yum.YumBase()
    inst=yb.rpmdb.returnPackages()
    installed=[x.name for x in inst]
    for pkg in pkgs:
        if pkg in installed:
             print('{0} is already installed, skipping...'.format(pkg))
        else:
            print('Installing {0}'.format(pkg))
            yb.install(pkg)
            yb.resolveDeps()
            yb.buildTransaction()
    yb.processTransaction()