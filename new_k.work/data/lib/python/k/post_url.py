import requests, requests.utils, pickle

def post_url(url,user=None,password=None,options=None,token='csrftoken',session_file='/tmp/abc'):
    if user is not None and password is not None and os.path.isfile(session_file):
        os.unlink(session_file)
    if os.path.isfile(session_file):
        with open(session_file) as f:
            ss = pickle.load(f)
        csrf_token=ss.cookies[token]
        data={'csrfmiddlewaretoken':csrf_token}
        if options is not None and len(options) > 0:
            data.update(options)
        r = ss.post(url, data=data, verify=False)
    else:
        # Login
        ss = requests.Session()
        ss.stream = False
        ss.get(url,verify=False)

        csrf_token=ss.cookies[token]
        data={'csrfmiddlewaretoken':csrf_token,'username': user, 'password':password}
        if options is not None and len(options) > 0:
            data.update(options)
        headers={"X-CSRFToken": csrf_token}
        r = ss.post(url, data=data, headers=headers, verify=False)
        with open(session_file,'wb') as f:
            session = pickle.dump(ss,f)
    return r

#Login
#print(post_url('http://172.16.115.130:8007/accounts/login/',user='test',password='zaq12wsx').text)
# get data
print(post_url('http://172.16.115.130:8007/list/').text)