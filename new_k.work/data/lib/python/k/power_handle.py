#include logging
#include rshell
#include ipmi_cmd
def power_handle(ipmi_ip,mode='reset',num=None,interval=None,ipmi_user='ADMIN',ipmi_pass='ADMIN',force=False,eth_ip=None,boot_mode=None,order=False,wait_ready=False):
    rc=True
    if num is None:
        num=2
    if interval is None:
        interval=10

    power_mode={'on':['chassis power on'],'off':['chassis power off'],'reset':['chassis power reset'],'off_on':['chassis power off','chassis power on']}
    if not mode in power_mode:
        return False
    verify_status=''
    sys_status='-'
    for ii in range(0,int(num)):
        rc=True
        test=1
        for rr in list(power_mode[mode]):
            verify_status=rr.split(' ')[2]
            if sys_status == verify_status and len(power_mode[mode]) == test:
                if force:
                    logging("wait {0} seconds after force {1} at {2}".format(interval,rr,ipmi_ip))
                    time.sleep(int(interval))
                else:
                    logging("Command and system status are same. So do not run the command to {0}".format(ipmi_ip))
                    return rc
            else:
                if boot_mode is not False and boot_mode is not None and boot_mode != 'none' and (verify_status == 'on' or verify_status == 'reset'):
                    time.sleep(2)
                    if order:
                        ipmi_cmd(cmd='chassis bootdev {0} options=persistent'.format(boot_mode),ipmi_ip=ipmi_ip,ipmi_user=ipmi_user,ipmi_pass=ipmi_pass)
                        logging("Persistent Boot mode set to {0} at {1}".format(boot_mode,ipmi_ip))
                    else:
                        ipmi_cmd(cmd='chassis bootdev {0}'.format(boot_mode),ipmi_ip=ipmi_ip,ipmi_user=ipmi_user,ipmi_pass=ipmi_pass)
                        logging("Temporary Boot mode set to {0} at {1}".format(boot_mode,ipmi_ip))
                    time.sleep(5)
                sys_power=ipmi_cmd(cmd=rr,ipmi_ip=ipmi_ip,ipmi_user=ipmi_user,ipmi_pass=ipmi_pass)
                logging("{0} to {1} (rc:{2})".format(rr,ipmi_ip,sys_power))
                if sys_power[0] == 0:
                    test=test+1
                    if eth_ip is not None:
                        if verify_status == 'off':
                            for pp in range(0,120):
                                if ping(eth_ip,2) is False:
                                    break
                                logging("ping for off {0} at {1}".format(pp,eth_ip))
                                time.sleep(2)
                        else: # Power on
                            for pp in range(0,600):
                                if ping(eth_ip,2):
                                    break
                                logging("ping for on {0} at {1}".format(pp,eth_ip))
                                time.sleep(2)
                    else:
                        logging("wait 20 seconds for next command at {0}".format(ipmi_ip))
                        time.sleep(20)
                else:
                    rc=False
                    logging("wait {0} seconds for failed power controll at {1}".format(interval,ipmi_ip))
                    time.sleep(int(interval))
                    continue
        last_mode=mode
        sys_status='unknown'
        for ll in mode.split('_'):
            last_mode=ll
        for pp in range(0,30):
            power_status=ipmi_cmd(cmd='chassis power status',ipmi_ip=ipmi_ip,ipmi_user=ipmi_user,ipmi_pass=ipmi_pass)
            sys_status='unknown'
            if power_status[0] == 0:
                if power_status[1] == 'Chassis Power is on':
                    sys_status='on'
                elif power_status[1] == 'Chassis Power is off':
                    sys_status='off'
            if sys_status == last_mode:
               break
            time.sleep(5)

        if sys_status == last_mode:
            rc=True
            break
        else:
            time.sleep(20)

    if wait_ready:
        wait_ready_system(ipmi_ip,ipmi_user,ipmi_pass)
    if rc:
        return {'rc':rc,'try':(ii+1),'state':'{0}'.format(sys_status)}
    else:
        return False

