# Kage
_k_version='1.1.35'
import subprocess
import re
from datetime import datetime
import time
import sys

#def rshell(cmd,code=None,dbg=False,timeout=None,ansi=True):
#    '''
#    Interactive shell
#    '''
#    Popen=subprocess.Popen
#    PIPE=subprocess.PIPE
#    STDOUT=subprocess.STDOUT
#    if dbg and code:
#       p = Popen('set -x\n'+ code + '\n', shell=True, stdout=PIPE, stderr=STDOUT, executable='/bin/bash',bufsize=1)
#    else:
#       if code:
#           p = Popen(code, shell=True, stdout=PIPE, stderr=STDOUT, executable='/bin/bash',bufsize=1)
#       else:
#           p = Popen(cmd, shell=True, stdout=PIPE, stderr=STDOUT, bufsize=1)
#    for line in iter(p.stdout.readline, b''):
#       print(line.decode('utf-8').rstrip())
#    p.stdout.close()
#    p.wait()
#    return p.returncode,'',''

def rshell(cmd,code=None,dbg=False,timeout=None,ansi=True,progress=False):
    '''
    Interactive shell
    '''
    ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
    Popen=subprocess.Popen
    PIPE=subprocess.PIPE
    STDOUT=subprocess.PIPE
    executable=None
    start_time=datetime.now().strftime('%s')
    ctimeout=-1
    out=''
    err=''
    if timeout:
       ctimeout=int(timeout)
    if not cmd and code:
       executable='/bin/bash'
       if dbg:
           code='set -x\n' + code
       cmd=code

    if progress: 
       bufsize=1
       STDOUT=subprocess.STDOUT
       p = Popen(cmd , shell=True, stdout=PIPE, stderr=STDOUT,executable=executable,bufsize=1)
       for line in iter(p.stdout.readline, b''):
          print(line.decode('utf-8').rstrip())
       p.stdout.close()
       p.wait()
    else:
       p = Popen(cmd , shell=True, stdout=PIPE, stderr=STDOUT,executable=executable)
       if sys.version_info[0] >= 3:
          try:
              out, err = p.communicate(timeout=int(timeout))
              if ansi:
                  out=ansi_escape.sub('',out).decode("ISO-8859-1").rstrip()
                  err=ansi_escape.sub('',err).decode("ISO-8859-1").rstrip()
          except subprocess.TimeoutExpired:
              p.kill()
              out='Error: Kill process after Timeout {0}'.format(timeout)
              err='Error: Kill process after Timeout {0}'.format(timeout)
       else:
          while p.poll() is None and ctimeout > 0:
              time.sleep(1)
              ctimeout -= 1
          if p.poll() is None and ctimeout == 0:
              p.kill()
              out='Error: Kill process after Timeout {0}'.format(timeout)
              err='Error: Kill process after Timeout {0}'.format(timeout)
          else:
              out, err = p.communicate()
              if ansi:
                  out=ansi_escape.sub('',out).rstrip()
                  err=ansi_escape.sub('',err).rstrip()
    return p.returncode, out, err,start_time,datetime.now().strftime('%s')
