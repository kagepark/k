from pyparsing import *
def str2list(string):
   stype=type(string)
   if stype == bool or stype == type(None):
       return []
   elif stype == list:
      return string
   else:
      if len(re.findall(r'^\[|\]$',string,re.IGNORECASE)) == 2:
         LBR,RBR = map(Suppress,"[]")
         qs = quotedString.setParseAction(removeQuotes, lambda t: t[0].strip())
         qsList = LBR + delimitedList(qs) + RBR
         return qsList.parseString(string).asList()
      else:
         if stype == str and len(string) == 0:
            return []
         else:
            return [string]