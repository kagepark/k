import datetime
def timechk(time='0',wait='0',tformat=None):
    if tformat is None:
        now=int(datetime.now().strftime('%s'))
    else:
        if time == '0':
            now=datetime.now().strftime(tformat)
        else:
            now=int(datetime.strptime(str(time),tformat).strftime('%s'))

    if tformat is None and str(time).isdigit() and int(time) > 0 and int(wait) > 0:
        if now - int(time) > int(wait):
            return False
        else:
            return True
    else:
        return now
