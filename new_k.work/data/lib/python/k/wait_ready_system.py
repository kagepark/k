import datetime
import time
def wait_ready_system(ipmi_ip,ipmi_user='ADMIN',ipmi_pass='ADMIN',wait_sec=1200):
    aa="""ipmitool -I lanplus -H {0} -U {1} -P {2} sdr type Temperature 2>/dev/null | grep -e "CPU" -e "System" | grep "Temp" | head -n1 | awk """.format(ipmi_ip,ipmi_user,ipmi_pass)
    aa=aa + """ '{print $10}' | grep "[0-9]" """
    time.sleep(15)
    chk=0
    init_sec=datetime.datetime.now().strftime('%s')
    while True:
        if do_sec - init_sec > wait_sec:
            return False
        if ping(ipmi_ip,2):
            wrc=km.rshell('''{0} '''.format(aa))
            if wrc[0] == 0:
                 if len(wrc[1]) > 1:
                     break
            if chk % 5 == 0:
                logging("wait to read system for {0} ({1})".format(ipmi_ip, wrc))
        else:
            if chk % 5 == 0:
                logging("can't ping to {0}".format(ipmi_ip))
        chk=chk+1
        time.sleep(15)
        do_sec=datetime.datetime.now().strftime('%s')
    time.sleep(20)
    return True

