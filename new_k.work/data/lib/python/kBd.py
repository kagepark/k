#!/usr/bin/env python
# coding: utf-8
# encoding=utf8
#
# Copyright CEP(Kage Park)
# license : GPL
# Created at 2019-06-27 10:49 by K
#
# python 2.6~ for no newline on print()
# from __future__ import print_function
# reference : PyPXE
_k_version="1.0.3"
import io
import os
import fcntl
import struct

def bytes_to_sector(byte,blk_size=512):
    sectors,rem=divmod(byte, blk_size)
    assert rem == 0
    return sectors

def bd_open(filename,ftype,mode={}):
    f=None
    # mode : {'write':<False/True>,'cow':<True/False>,'in_ram':<False/True>}
    in_ram=False # default:False => FILE I/O
    if 'in_ram' in mode:
        in_ram=mode['in_ram']
    write=False
    if 'write' in mode:
        write=mode['write']
    if ftype == 'nbd':
        cow=True
        if 'cow' in mode:
            cow=mode['cow']
        open_mode='r+b' if write and not cow else 'rb'
    elif ftype == 'cdrom':
        open_mode='ro'
    else:
        open_mode='r+b' if write else 'rb'
    f=open(filename,open_mode)

    if ftype == 'cdrom':
        bd_size=fcntl.ioctl(f,0x00001260) #????
    else:
        f.seek(0,2) # go to EOF
        bd_size=f.tell() # Need block device size(filesize) when client mount it
        f.seek(0)   # go to start

    if in_ram: # copy data to RAM
       f = io.ByteIO(f.read())
    return f,bd_size

def offset(blk_num=1,blk_size=512):
    return blk_size * (blk_num - 1)

def read(f,_offset):
    '''
    Block start from 1,...
    Default block size : 512'''
    f.seek(_offset))
    try:
        return f.read(blk_size)
    except:
        return False

def write(f,data,_offset):
    '''
    Block start from 1,...
    Default block size : 512'''
    f.seek(_offset)
    try:
        f.write(data)
        f.flush()
    except:
        return False

def nbd_init_tag(filename):
    f,bd_size=bd_open(filename,'nbd')
    return 'NBDMAGIC\x00\x00\x42\x02\x81\x86\x12\x53' + struct.pack('>Q', afile.tell()) + '\0'*128

def nbd_tag(handle):
    # magic, request, handle, offset, dlen = struct.unpack('>LL8sQL', header)
    return 'gDf\x98\0\0\0\0'+handle


