import os, time
#import broadlink
import kBroadlink as broadlink
#import kLog
# /usr/local/lib/python2.7/site-packages/broadlink_kg 
# update code to controll switch with device IP
# This broadlink_kg having bug for light on.
# When light on(SP2 switch's light) then the switch status will return True value (on). 
# This case, one time run power on or off after turn off light then will be ok again
def _log(log,msg):
   if log:
       log(msg)

def _dbg(dbg,level,msg):
   if dbg:
       dbg(level,msg)

class BroadLink:
   def __init__(self,log=None,dbg=None,home_path=None):
      if home_path is None:
          self.home_path=os.path.dirname(os.path.abspath(__file__))
      else:
          self.home_path=home_path
      self.log=log
      self.dbg=dbg
      _log(log,'Module load : BroadLink (rm/sw)')

   def signal(self,filename,cmd=None,ir_packet=None):
      if filename is None:
         return False
      sg_file=self.home_path+'/signal/'+filename+'.rm'
      if os.path.isfile(sg_file) is True:
         with open(sg_file,'rb') as bin_file:
            signal_val=bin_file.read()
         return signal_val
      elif cmd == 'save':
         if ir_packet is None:
            return False
         f=open(sg_file,'w')
         f.write(ir_packet)
         f.close()
         _dbg(self.dbg,4,"Saved at {0}".format(sg_file))
         return True
      else:
         return False

   def status(self,dest_ipaddr=None):
      _dbg(self.dbg,3,'dest_ipaddr={0}'.format(dest_ipaddr))
      if dest_ipaddr is None or dest_ipaddr == '':
         return
      for ii in range(0,2):
         aa=os.system("ping -c 3 " + dest_ipaddr + ' >& /dev/null')
         if aa == 0:
            return True
      return False

   def pbutton(self,button=None,dev_ipaddr=None,cmd=None):
      _dbg(self.dbg,4,'dev_ipaddr={0}'.format(dev_ipaddr))
      if dev_ipaddr is None:
         devices = broadlink.discover(timeout=5)
      else:
         devices = broadlink.discover(timeout=5,dest_host=dev_ipaddr)
 
      br_dev_auth=None
       
      for zz in range(0,6):
         if br_dev_auth is not None:
            break
         jj=0
         for dd in list(devices):
            ddaa="{0}".format(dd)
            _dbg(self.dbg,4,"DEVICE: {0}".format(ddaa))
            if 'rm' == ddaa.split(' ')[0].split('.')[1]:
               try:
                  br_dev_auth=devices[jj].auth()
                  break
               except:
                  br_dev_auth=None
                  time.sleep(1)
            jj=jj+1

      if cmd == 'temp' or cmd == 'learn':
         signal_rc=True
      else:
         signal_rc=self.signal(button)
      _dbg(self.dbg,4,"cmd: {0}, button: {1}".format(cmd,button))
      if br_dev_auth is None:
         _dbg(self.dbg,3,"BroadLink auth/finding device fail")
         return False
      elif signal_rc is False:
         _dbg(self.dbg,3,"signal file({0}) read error".format(button))
         return False
      else:
         try:
             if cmd == 'temp':
                 return devices[jj].check_temperature()
             elif cmd == 'learn':
                 devices[jj].enter_learning()
                 time.sleep(2)
                 _log(self.log,"Ready to learn IR {0} at {1}".format(button,dev_ipaddr))
                 for kkk in range(0,10):
                    ir_packet=devices[jj].check_data()
                    _dbg(self.dbg,4,"Try({0}) to learn ({1})".format(kkk,ir_packet))
                    rc=self.signal(button,cmd='save',ir_packet=ir_packet)
                    if rc is True:
                       _log(self.log,"learned IR and save at {0}".format(button))
                       return True
                    time.sleep(3)
                 return False
             else:
                 return devices[jj].send_data(signal_rc)
         except IndexError:
             return False

   def remocon_multi_dev(self,button,rm_dev_ipaddr,cmd):
        _dbg(self.dbg,3,"remocon ip : {}".format(cmd,rm_dev_ipaddr))
        dev_ipaddr_type=type(rm_dev_ipaddr).__name__
        if dev_ipaddr_type == 'str':
            dev_ipaddr=dev_ipaddr.split(',')
        elif dev_ipaddr_type != 'list':
            return False
        for rm_ip in rm_dev_ipaddr:
            _dbg(self.dbg,3,"remocon do {} at {}".format(cmd,rm_ip))
            rc=self.pbutton(button,dev_ipaddr=rm_ip,cmd=cmd)
        return rc


   # dev_ipaddr : Remote control (RM) Device IP
   # dest_ipaddr : example: samsung tv, apple tv, ... 's IP address for check pinging
   def remocon(self,button=None,cmd=None,dev_ipaddr=None,dest_ipaddr=None):
      toggle=0
      if button is None and cmd != 'status' and cmd != 'temp':
         return -1

      #if dest_ipaddr is not None and dest_ipaddr != '':
      if type(dest_ipaddr) is str and dest_ipaddr:
        _dbg(self.dbg,3,"device IP {0}".format(dest_ipaddr))
        #if self.k_net_class.valid_IPv4_format(dest_ipaddr) is True:
        for zz in range(0,4):
            dev_state=self.status(dest_ipaddr)
            _dbg(self.dbg,3,"push button status({0}) for {1}: {2}".format(dest_ipaddr,cmd,dev_state))
            if cmd == 'status':
               if dev_state is True:
                  return 'on'
               elif dev_state is False:
                  return 'off'
               else:
                  return 'unknown: not found IP address'
            elif cmd == 'on':
               if dev_state is True:
                  if toggle == 0:
                     return
                  return True
               elif dev_state is False and toggle == 0:
                  #rc=self.pbutton(button,dev_ipaddr=dev_ipaddr)
                  rc=self.remocon_multi_dev(button,dev_ipaddr,cmd)
                  _dbg(self.dbg,3,"push button for {0} result ({1}) : {2}".format(cmd,dev_ipaddr,rc))
                  toggle=1
                  time.sleep(3)
            elif cmd == 'off':
               if dev_state is False:
                  if toggle == 0:
                     return
                  return True
               elif dev_state is True and toggle == 0:
                  #rc=self.pbutton(button,dev_ipaddr=dev_ipaddr)
                  rc=self.remocon_multi_dev(button,dev_ipaddr,cmd)
                  _dbg(self.dbg,3,"push button for {0} result ({1}) : {2}".format(cmd,dev_ipaddr,rc))
                  toggle=1
                  time.sleep(3)
            elif cmd == 'temp':
                  #return self.pbutton('temp',dev_ipaddr=dev_ipaddr)
                  return self.remocon_multi_dev('temp',dev_ipaddr,cmd)
            elif cmd == 'learn':
                  #return self.pbutton(button,dev_ipaddr=dev_ipaddr,cmd=cmd)
                  return self.remocon_multi_dev(button,dev_ipaddr,cmd)
        return False
      else:
        return self.remocon_multi_dev(button,dev_ipaddr,cmd)
#        dev_ipaddr_type=type(dev_ipaddr).__name__
#        if dev_ipaddr_type == 'str':
#            dev_ipaddr=dev_ipaddr.split(',')
#        elif dev_ipaddr_type != 'list':
#            return False
#        for rm_ip in dev_ipaddr:
#            rc=self.pbutton(button,dev_ipaddr=dev_ipaddr,cmd=cmd)
#        return rc

   def switch(self,dev_ipaddr=None,cmd=None):
      if dev_ipaddr is None:
         return False

      for qq in range(1,10):
         devices = broadlink.discover(timeout=5,dest_host=dev_ipaddr)
         _dbg(self.dbg,4,"dev_ipaddr: {0}, devices : {1}, try count: {2}".format(dev_ipaddr,len(devices),qq))
         if len(devices) > 0:
            break
         time.sleep(7)

      _dbg(self.dbg,3,"bl switch's devices: {0} ({1})".format(devices,len(devices)))
      if len(devices) == 0:
         return False

      try:
         devices[0].auth()
      except:
         return False
    
      dev_state=devices[0].check_power()
      if cmd == 'status' or cmd is None:
         if dev_state is False:
            return 'off'
         elif dev_state is True:
            return 'on'
         else:
            return 'unknown'
      if cmd == 'on':
         if dev_state is False:
            rc=devices[0].set_power(True)
            if rc is None:
               return True
            else:
               return False
         elif dev_state is True:
            return 
         else:
            return False
      elif cmd == 'off':
         if dev_state is True:
            rc=devices[0].set_power(False)
            if rc is None:
               return True
            else:
               return False
         elif dev_state is False:
            return
         else:
            return False

def help():
       print('For Remote control')
       print(sys.argv[0] + ' <rm ip> <cmd>')
       print(' <cmd>')
       print(' <signal> <none> : Send <signal> to IR-DEV')
       print(' <signal> learn  : learn IR <signal> and save to file')
       print(' none temp       : Read Temperature')
       print(' <signal> on [<dest IP>]  : [Checkup <dest IP> and] send <signal> to IR-DEV')
       print(' <signal> off [<dest IP>] : [Checkup <dest IP> and] send <signal> to IR-DEV')
       sys.exit(0)

if __name__ == "__main__":
   import sys
   import kLog
   kLog._kv_dbg=4
   br=BroadLink(kLog.log,kLog.dbg)
   home_path=os.path.dirname(os.path.abspath(__file__))
   args=len(sys.argv)
   if args == 1 or sys.argv[1] == "--help" :
       help()
   else:
       if args >=3:
          rm_ip=sys.argv[1]
          button=sys.argv[2]
          if button == 'sw':
              if args == 4:
                  cmd=sys.argv[3]
                  print(br.switch(dev_ipaddr=rm_ip,cmd=cmd))
              else:
                  help()
              sys.exit(0)

          # RM
          signal_file='signal/'+button+'.rm'
          if args == 3:
              if os.path.isfile(signal_file) is True:
                  aa=br.remocon(button=button,dev_ipaddr=rm_ip)
              else:
                  k.log(signal_file + ' not found')
              sys.exit(0)
          elif args == 4:
              cmd=sys.argv[3]
              if cmd == 'learn' or cmd == 'save':
                  if os.path.isfile(signal_file) is True:
                      k.log(signal_file + ' found')
                      sys.exit(0)
                  br.remocon(button=button,cmd='learn',dev_ipaddr=rm_ip)
              elif cmd == "on" or cmd == "off" :
                  if args == 5:
                      aa=br.remocon(button=button,cmd=cmd,dest_ipaddr=sys.argv[4],dev_ipaddr=rm_ip)
                  else:
                      aa=br.remocon(button=button,cmd=cmd,dev_ipaddr=rm_ip)
              elif cmd == "temp":
                  aa=br.remocon(cmd=cmd,dev_ipaddr=rm_ip)
                  if aa is False:
                      print(aa)
                  else:
                      print("{0}'C".format(aa))
              sys.exit(0)
   help()
