#Kage
from __future__ import print_function
import sys
from cmd import Cmd
import readline
import os,termios,fcntl,select
import inspect
import kLine as kl
import kFile as kf
import shlex

class TEST_CMD:
    def test(self,line):
        print(line)

def str2shell_list(args):
    return shlex.split(args)

class kPrompt(Cmd):
#require: cmd.Cmd, sys, os, termios, fcntl, select, inspect, kLine, kFile
#example) test.py
#from kCmdLine import kPrompt
#import os
#
#class AAAAA: # Custom function list
#    # made function name do_<command>(self,<arg name>):
#    def do_aaa(self,abc):
#        print('aaa {}'.format(abc))
#
#if __name__ == '__main__':
#    import getpass
#    import socket
#    username=getpass.getuser()
#    hostname=socket.gethostname()
#    prompt = kPrompt()
#    prompt._class_=AAAAA # put the custom command line to kPrompt()
#    #prompt.history=[]   # if you want use custom history the put here
#    #prompt.CMDLISTS=[]  # if you want use custom command list the put here
#    prompt_str='{0}@{1}'.format(username,hostname)
#    prompt.prompt = '[{0} {1}]$ '.format(prompt_str,os.path.basename(os.getcwd()))
#    prompt.cmdloop_with_mykey(intro='-K-')
    _class_=None
    _history=[]
    CMDLISTS=['help']

    def do_history(self,arg):
        for ii in self._history:
             print(ii)

    def KReadLine2(self): #very good
        cursor=0
        cursor_h=1
        _history_idx=0
        keep_new_line=False
        line=''

        fd = sys.stdin.fileno()
        oldterm = termios.tcgetattr(fd)
        oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)

        newattr = termios.tcgetattr(fd)
        newattr[3] = newattr[3] & ~termios.ICANON
        newattr[3] = newattr[3] & ~termios.ECHO
        termios.tcsetattr(fd, termios.TCSANOW, newattr)

        sys.stdout.flush()
        while True:
            inp, outp, err = select.select([sys.stdin], [], [])
            c = sys.stdin.read()
            # analysis line
            before_first,before_end,line_first,line_end,after_first,after_end=kl.line_dbg(line,cursor_h)
            if c == '\x1b[3~': # delete key
                if cursor < len(line):
                    front=line[:cursor]
                    backend=line[cursor+1:]
                    sys.stdout.write(backend+' ')
                    sys.stdout.write('\033[{0}D'.format(len(backend)+1))# 1 move left, if 3 move then '\033[3D'
                    line=front+backend
            elif c == '\x1b[D': # LEFT
                if cursor >0:
                    sys.stdout.write('\b')
                    cursor-=1
                    #sys.stdout.write('\033[1D')# 1 move left, if 3 move then '\033[3D'
            elif c == '\x1b[C': # Right
                if cursor < len(line):
                    sys.stdout.write('\033[1C') # 1 move right, if 3 move then '\033[3C'
                    cursor+=1
            elif c == '\x1b[A': # UP
                if self._history:
                    if _history_idx < len(self._history):
                        _history_idx+=1
                        get__history=self._history[-_history_idx]
                        sys.stdout.write('\x1b[2K') # erase current line
                        sys.stdout.write('\r'+self.prompt+self._history[-_history_idx]) # \r: cursor start from 0
                        sys.stdout.flush()
                        line=self._history[-_history_idx]
                        cursor=len(line)
            elif c == '\x1b[B': # Down
#                if self.history:
                    if _history_idx == 1 or not self._history:
                        sys.stdout.write('\x1b[2K') # erase current line
                        sys.stdout.write('\r'+self.prompt)
                        line='' # end of history
                        cursor=0
                        _history_idx=0
                    elif self._history and _history_idx > 0:
                        _history_idx-=1
                        get_history=self._history[-_history_idx]
                        sys.stdout.write('\x1b[2K') # erase current line
                        sys.stdout.write('\r'+self.prompt+self._history[-_history_idx])
                        line=self._history[-_history_idx]
                        cursor=len(line)
            elif c == '\x1bOH': # HOME Key
                cursor=0
                sys.stdout.write('\r'+'\033[{0}C'.format(len(self.prompt))) # 1 move right, if 3 move then '\033[3C'
            elif c == '\x1bOF': # END Key
                cursor=len(line)
                sys.stdout.write('\r'+'\033[{0}C'.format(len(self.prompt)+len(line))) # 1 move right, if 3 move then '\033[3C'
            elif c == ' ' and cursor < len(line): # SPACE KEY
                front=line[:cursor]
                backend=line[cursor:]
                sys.stdout.write(' '+backend)
                line=front+' '+backend
                sys.stdout.write('\033[{0}D'.format(len(backend)))# 1 move
                cursor+=1

            elif len(c) == 1 and ord(c) == 4: # Control+d
                # Recover the terminal
#                termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
#                fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
                termios.tcsetattr(fd, termios.TCSANOW, oldterm)
                raise SystemExit

            elif len(c) == 1 and ord(c) == 127: # backspace key
                if cursor > 0 and cursor == len(line): # delete from backend
                    sys.stdout.write('\b \b')
                    line=line[:len(line)-1]
                    cursor-=1
                elif cursor < len(line): # delete from middle
                    front=line[:cursor-1]
                    backend=line[cursor:]
                    sys.stdout.write('\b \b')
                    sys.stdout.write(backend+' ')
                    sys.stdout.write('\033[{0}D'.format(len(backend)+1))# move left to cursor
                    line=front+backend
                    cursor-=1
            elif c == '\t' and keep_new_line is False: # Tab key
                found=None
                if line_end is None:
                    find_word=''
                else:
                    find_word=line_end
                if line_end and line_end != '\n':
                    line_arr=line_end.split('/')
                    if len(line_arr)>1: # search defined directory
                        if line_end[-1] == '/':
                            find_dir=line_end
                            find_word=''
                        else:
                            find_dir='/'.join(line_arr[:-1])
                            find_word=line_arr[-1]
                            if not find_dir:
                                find_dir='/'
#                            found=LIBS().completion(LIBS().list_in_dir(find_dir),line_arr[-1])
                        found=kl.completion(kf.list_in_dir(find_dir),find_word)
                    else: # Search current directory
                        found=kl.completion(kf.list_in_dir('.'),line_end)
                elif line_first and not line_end: # search command and complete command
                    find_word=line_first
                    found=kl.completion(self.CMDLISTS,line_first)
                else: # Nothing then print command list
                    find_word=line_end
                    #sys.stdout.write('\x1b[2K') # erase current line
                    sys.stdout.write('\n') # erase current line
                    sys.stdout.write('\r'+'\t'.join(self.CMDLISTS)+'\n')
                    sys.stdout.write('\r'+self.prompt+line)
#                    print(CMDLISTS)
                if found:
                    if len(found) == 1:
                        adding_str=found[0][len(find_word):]
                        if len(adding_str): # If found something then complete cursor string
                            print(adding_str,end='')
                            line+=adding_str
                            cursor+=len(adding_str)
                        else: # print currnt file or dir list when already completed command
#                            print(LIBS().list_in_dir('.'))
                            #sys.stdout.write('\x1b[2K') # erase current line
                            sys.stdout.write('\n') # erase current line
                            sys.stdout.write('\r'+'\n '.join(kf.list_in_dir('.'))+'\n')
                            sys.stdout.write('\r'+self.prompt+line)
                    else:
                        #print(found)
                        #sys.stdout.write('\x1b[2K') # erase current line
                        sys.stdout.write('\n') # erase current line
                        sys.stdout.write('\r'+'\n'.join(found)+'\n')
                        sys.stdout.write('\r'+self.prompt+line)
                sys.stdout.flush()
            elif c in ['\r','\n','\r\n']: # Enter ; return value
                cursor=0
                before_first,before_end,line_first,line_end,after_first,after_end=kl.line_dbg(line,cursor_h)
                if keep_new_line is False and line_end in ['{',':','\\']:
                    keep_new_line=True
                    print(c)
                if keep_new_line is False or (keep_new_line and ( line_first == '}' or (line_end == '\n' and line_first=='\n'))): # Return all data
                    keep_new_line=False
                    self._history.append(line)
                    # Recover the terminal
                    termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
                    fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
                    print('')
                    return line
                else:
                    line+=c # Get a line mark
                if keep_new_line:
                    print('> ',end='') # prompt after new line
                else:
                    print(self.prompt,end='') # prompt after new line
            else: # get typed charactor on current cursor
                if cursor < len(line):
                    front=line[:cursor]
                    backend=line[cursor:]
                    sys.stdout.write(c+backend)
                    line=front+c+backend
                    sys.stdout.write('\033[{0}D'.format(len(backend)))# 1 move
                    cursor+=1
                else:
                    print(c,end='') # type prompt
                    line+=c         # get type prompt
                    cursor+=len(c) # Fix copy and paste issue (paste got single c for the whole pasted line)
            sys.stdout.flush()

    def mycmdloop(self, intro=None):
        """Repeatedly issue a prompt, accept input, parse an initial prefix
        off the received input, and dispatch to action methods, passing them
        the remainder of the line as argument.

        """
        self.preloop()
        if self.use_rawinput and self.completekey:
            try:
                self.old_completer = readline.get_completer()
                readline.set_completer(self.complete)
                readline.parse_and_bind(self.completekey+": complete")
            except ImportError:
                pass
        try:
            if intro is not None:
                self.intro = intro
            if self.intro:
                self.stdout.write(str(self.intro)+"\n")
            stop = None
            line=''
            while not stop:
                if self.cmdqueue:
                    line = self.cmdqueue.pop(0)
                else:
                    if self.use_rawinput:
                        try:
                            print(self.prompt,end='')
                            line = self.KReadLine2()#input(self.prompt)
                        except EOFError:
                            line = 'EOF'
                    else:
                        self.stdout.write(self.prompt)
                        self.stdout.flush()
                        line = self.KReadLine2()#self.stdin.readline()
                if line:
                    line = line.rstrip('\r\n')

                    line = self.precmd(line)
                    stop = self.onecmd(line)
                    stop = self.postcmd(stop, line)
            self.postloop()
        finally:
            if self.use_rawinput and self.completekey:
                try:
                    readline.set_completer(self.old_completer)
                except ImportError:
                    pass

    def cmdloop_with_mykey(self, intro):
        doQuit = False
        while doQuit != True:
            try:
                if intro!='':
                    cintro=intro
                    intro=''
                    self.mycmdloop(cintro)
                else:
                    self.intro=''
                    self.mycmdloop()
                doQuit = True
            except KeyboardInterrupt:
                sys.stdout.write('\n')

    def default(self,args):
        args_a=args.split(' ')
        args_o=' '.join(args_a[1:])
        if self._class_:
            class_f=[i for i in dir(self._class_) if not inspect.ismethod(i)]
            if 'do_{}'.format(args_a[0]) in class_f:
                return getattr(self._class_(),'do_{}'.format(args_a[0]))(args_o)
            elif 'default' in class_f:
                return getattr(self._class_(),'default'.format(args_a[0]))(args_o)

if __name__ == '__main__':
    import getpass
    import socket
    username=getpass.getuser()
    hostname=socket.gethostname()
    prompt = kPrompt()
    prompt._class_=TEST_CMD # put the custom command line to kPrompt()
    prompt_str='{0}@{1}'.format(username,hostname)
    prompt.prompt = '[{0} {1}]$ '.format(prompt_str,os.path.basename(os.getcwd()))
    prompt.cmdloop_with_mykey(intro='-K-')

