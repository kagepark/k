# Kage
import os
import re
from itertools import chain

import ast

_k_version='1.0.37'

lib_ext={'shell':['','.k','.so','.sh'],'python':['','.py']}
headers_find={'shell':[re.compile('#include (\w.*)')],'python':[re.compile('import (\w.*)'),re.compile('from (\w.*) import')]}

def read_file(filename,base_dir):
    filename='{}/{}'.format(base_dir,filename)
    if os.path.isfile(filename):
        with open(filename,'r') as f:
            data=f.read()
        return data

def read_lib(lib_name,lib_type,base_dir):
    lib_ata=None
    lib_dir='{}/lib/{}'.format(base_dir,lib_type)
    lib_file='{}/{}'.format(lib_dir,lib_name)
    for i in lib_ext[lib_type]:
        lib_fn='{}{}'.format(lib_file,i)
        if os.path.isfile(lib_fn):
            with open(lib_fn,'r') as f:
                lib_data=f.read()
            return lib_data

# Include module base file
def headers(data,lib_type):
    header_files=[]
    if data:
        for header in headers_find[lib_type]:
            for hh in header.findall(data):
                if lib_type == 'python':
                    if '#' in hh:
                        continue
                    if 'as' in hh:
                        header_files+=hh.split()[0]
                    else:
                        header_files+=hh.replace(' ','').split(',')
                else:
                    header_files+=hh.replace(' ','').split(',')
    return header_files

def code_body(filename,base_dir,lib_type):
    data=read_file(filename,base_dir)
    if data:
        for hh in headers(data,lib_type):
            tmp=read_lib(hh,lib_type,base_dir)
            if tmp:
                data='{}\n{}'.format(tmp,data)
        return data

def code_lib(name,base_dir,lib_type):
    data=read_lib(name,lib_type,base_dir)
    if data:
        for hh in headers(data,lib_type):
            tmp=read_lib(hh,lib_type,base_dir)
            if tmp:
                data='{}\n{}'.format(tmp,data)
        return data

def code_k(name,base_dir,lib_type):
    name=re.sub('^%s' % 'k.', '', name)
    data=read_lib('k/{}.py'.format(name),lib_type,base_dir)
    if data:
        for hh in headers(data,lib_type):
            tmp=read_lib(hh,lib_type,base_dir)
            if tmp:
                data='{}\n{}'.format(tmp,data)
        return data

# Include function code (function base file)
class ParseCall(ast.NodeVisitor):
    def __init__(self):
        self.ls = []
    def visit_Attribute(self, node):
        ast.NodeVisitor.generic_visit(self, node)
        self.ls.append(node.attr)
    def visit_Name(self, node):
        self.ls.append(node.id)


class FindFuncs(ast.NodeVisitor):
    def visit_Call(self, node):
        rc=[]
        p = ParseCall()
        p.visit(node.func)
        print ".".join(p.ls)
        ast.NodeVisitor.generic_visit(self, node)

def func_list_bak(s, r, mod=''): # mod='k.' then read k.XXX() function, not then all XXX() functions
    t = re.sub(r'\([^()]+\)', '()', s) # diplay () for function name
    m = re.findall(r'{}[\w.]+\(\)'.format(mod), t) # All function
    t = re.sub(r'[\w.]+\(\)', '', t) # All function
    if m==r:
        return
    for i in chain(m, func_list(t, m)):
        yield i

def function_list(s, r, mod=''): # mod='k.' then read k.XXX() function, not then all XXX() functions
    new_s=''
    for line in s.splitlines():
        if not line.startswith('def '):
             new_s='{}\n{}'.format(new_s,line)
    t = re.sub(r'\([^()]+\)', '()', new_s) # diplay () for function name
    m = re.findall(r'{}[\w.]+\(\)'.format(mod), t) # All function
    t = re.sub(r'[\w.]+\(\)', '', t) # All function
    if m==r:
        return
    for i in chain(m, function_list(t, m)):
        yield i

def get_func_list(code,mod=''):
   if code:
       funcs=function_list(code,[],mod=mod)
       rc=[]
       for i in sorted(set(list(funcs))): # Need more filter out for reduce function list (exclude base func name)
           if i[0] != '.' and not i in ['print()','LIBS()','tuple()']:
               rc.append(i.replace('()',''))
       return rc


def read_all_func_code_from_code(code,base_dir,lib_type,func_list=[]):
    mod=''
    if not func_list:
        mod='k.'
    func_list=get_func_list(code,mod=mod)
    if func_list:
        rc_code='' 
        chk_list=[]
        for rfn in func_list:
            if rfn in chk_list:
                continue
            # Read func code
            if mod:
                new_name=re.sub('^%s' % 'k.', 'k/', rfn)
            else:
                new_name='k/{}'.format(rfn)
            new_func_code=code_lib(new_name,base_dir,lib_type)
            sub_func_code,sub_list=read_all_func_code_from_code(new_func_code,base_dir,lib_type,func_list=func_list)
            if sub_func_code:
                rc_code='''{}\n{}'''.format(rc_code,sub_func_code)
                if func_list:
                    chk_list=chk_list+sub_list
            if new_func_code is not None:
                rc_code='''{}\n{}'''.format(rc_code,new_func_code)
                chk_list.append(rfn)
        return rc_code,func_list
    return None,None


def read_all_func_code_from_func_list(func_list,base_dir,lib_type):
    if func_list:
        rc_code=''
        chk_list=[]
        for rfn in func_list:
            if rfn in chk_list:
                continue
            # Read func code
            #new_name=re.findall('^k\.(\w.*)',rfn)
            new_name=re.findall('^(k\.\w.*)',rfn)
            if new_name:
                new_name=re.sub('^k.','k/',new_name[0])
            else:
                new_name='k/{}'.format(rfn)
            new_func_code=code_lib(new_name,base_dir,lib_type)
            sub_func_code,sub_list=read_all_func_code_from_code(new_func_code,base_dir,lib_type,func_list=func_list)
            if sub_func_code:
                rc_code='''{}\n{}'''.format(rc_code,sub_func_code)
                if sub_list:
                    chk_list=chk_list+sub_list
            if new_func_code is not None:
                rc_code='''{}\n{}'''.format(rc_code,new_func_code)
                chk_list.append(rfn)
        return rc_code

if __name__ == '__main__':
#    print(code_body('lib/shell/svn.k','/home/kage/bin/k_py/new_k/data','shell'))
#    print(code_body('kServer.py','/home/kage/bin/k_py/new_k/data','python'))
#    print(code_lib('kmisc','/home/kage/bin/k_py/new_k/data','python'))
#    os.system(code_lib('mount','/home/kage/bin/k_py/new_k.work/data','shell')+'\n_k_mount --help')

#####################################################################################################
#    Auto find function name list from main file and read each function file and add the code to main file's code
#    So, it made single file python code 
#    This is new feature for coding
#    This is not a module base code. This can make function base code
#####################################################################################################
    with open('test2.py','r') as f:
        ddd=f.read()
#    tree=ast.parse(ddd) # this is just print to screen only
#    or
#    code,lib_list=read_all_func_code_from_code(ddd,'/home/kage/bin/k_py/new_k.work/data','python')
#    if code:
#        mmm='{}\n{}'.format(code,ddd)
#        print(mmm)
#        exec(mmm)
    func_list=get_func_list(ddd)
    code=read_all_func_code_from_func_list(func_list,'/home/kage/bin/k_py/new_k.work/data','python')

    print(code)
#    code_body_str=''
#    # Get func list from main code
#    require_func_list=get_func_list(ddd) 
#    for sfunc in require_func_list:
#        # Read func code
#        new_func_code=code_lib(re.sub('^%s' % 'k.', 'k/', sfunc),'/home/kage/bin/k_py/new_k.work/data','python')
#        # Check the func code use another func code
#        for sub_func in get_func_list(new_func_code):
#             if not sub_func in require_func_list:
#                 sub_func_code=code_lib('k/{}'.format(sub_func),'/home/kage/bin/k_py/new_k.work/data','python')
#
#        code_body_str='''{}\n{}'''.format(code_body_str,code_k(sfunc,'/home/kage/bin/k_py/new_k.work/data','python'))
#    print(code_body_str)
