import time
import rshell as k

def _k_partition_usage(mnt):
    """ Check partition usage in OSX """
    if mnt is None:
       return -1
    cmd="df -l | awk -v mount="
    cmd=cmd+mnt
    cmd=cmd+""" '{if($9==mount) print $5}' | sed "s/%//g" """
    usage=k.rshell(cmd)
    if usage[0] == 0:
        return usage[1]

   ###############################
   # system Load monitoring
def loadavg(over_load=25):
    load = k.rshell(""" sysctl -n vm.loadavg| awk '{print $2}' | awk -F. '{print $1}' """)
    if load[0] == 0 and int(load[1]) >= over_load:
        return load[1]

def is_running(app):
    """ Check running Apps in OSX """
    cmd = """osascript<<END
tell application "System Events"
count (every process whose name is "{}")
end tell"""
    count=k.rshell(cmd)
    return count > 0

def is_sleep():
    cmd = """ osascript<<END
tell application "System Events"
  get running of screen saver preferences
end tell
END"""
    sleep=k.rshell(cmd)

    cmd = """ ioreg -n IODisplayWrangler |grep -i IOPowerManagement | sed -e "s/\\"//g" -e "s/\\}//g" | tr ',' '\\n' |grep CurrentPowerState | awk -F= '{print $2}' """
    screenpower=k.rshell(cmd)

    ssave=is_running('ScreenSaverEngine')
    if str(sleep) == 'false' and str(screenpower) == '4' and str(ssave) == 'False':
       return False
    else:
       return True

if __name__ == "__main__":
    print('sleep:',is_sleep())
    print('is_running:',is_running('ScreenSaverEngine'))
    print(loadavg(over_load=25))
    print(_k_partition_usage('/Volumes/KAGE4T'))
