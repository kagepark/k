#
# Kage Park
# for Kage Crypto application
#
_k_version="1.2.42"

import base64
#from Crypto.Cipher import AES
import lz4.frame
import uuid
import string
import random
import re
import cPickle as pickle

counter="cEPk"*4
#kkey="KeSE"*8
key="Ke1SE"
#key="KS2yc25FSgsbeC&2!P" * 10
#key_n=len(kkey)
compress=False

byten=256
def encrypt(data):
    #ikey=str(uuid.uuid4()).replace('-','')
    #ikey=''.join(random.choice(string.ascii_letters) for i in range(len(string.ascii_letters)//2-len(key)))
    ikey=''.join(random.choice(string.ascii_letters+string.digits+key) for i in range(len(string.ascii_letters+key)//2))
    key_n=len(ikey)
    data=lz4.frame.compress(pickle.dumps(data))
    encl = []
    for i in range(len(data)):
        key_c = ikey[i % key_n]
        enc_c = (ord(data[i]) + ord(key_c)) % byten
        encl.append(enc_c)
    hlen=len(encl)//2
    rc=[ikey+key]
    #return rc+enc[hlen:]+enc[:hlen]
    return pickle.dumps(rc+encl[hlen:]+encl[:hlen])

def decrypt(data):
    dec = []
    data=pickle.loads(data)
    iikey=data.pop(0)
    ikey=re.sub('{}$'.format(key),'',iikey)
    key_n=len(ikey)
    hlen=len(data)//2
    if len(data)%2 == 1:
        hlen=hlen+1
    enc=data[hlen:]+data[:hlen]
    for i in range(len(enc)):
        key_c = ikey[i % key_n]
        dec_c = chr((byten + enc[i] - ord(key_c)) % byten)
        dec.append(dec_c)
    return pickle.loads(lz4.frame.decompress("".join(dec)))

#def encrypt(data):
#    encrypto = AES.new(key, AES.MODE_CTR, counter=lambda: counter)
#    if compress:
#       import lz4.frame
#       return base64.b64encode(lz4.frame.compress(encrypto.encrypt(data)))
#    else:
#       return encrypto.encrypt(data)
#
#def decrypt(data):
#    decrypto = AES.new(key, AES.MODE_CTR, counter=lambda: counter)
#    if compress:
#        import lz4.frame
#        return decrypto.decrypt(lz4.frame.decompress(base64.b64decode(data)))
#    else:
#        return decrypto.decrypt(data)

if __name__ == '__main__':
    mm='Hyi18 dj hady1 07f71 j1-3r7632- {}  72ytfhb $4y1s'
    print(len(mm))
    print(mm)
    ee=encrypt(mm)
    print(ee)
    print(len(ee))
    dd=decrypt(ee)
    print(len(dd))
    print(dd)
