#!/usr/bin/env python
# coding: utf-8
# encoding=utf8
#
# Copyright CEP(Kage Park)
# License : GPL
#   Since : March 1994
# Kage

_k_version="1.0.12"

import os, time
import sys
import signal

class Daemon:
    """
    A generic daemon class.
    Usage: subclass the Daemon class and override the run() method

    example)
    from kDaemon import Daemon
    class MyDaemon(Daemon):
        def run(self):
            while True:
                <my code>
    MyD=MyDaemon('<pid file>')
    if sys.argv[1] == 'start':
        MyD.start()
    elif sys.argv[1] == 'stop':
        MyD.stop()
    elif sys.argv[1] == 'status':
        MyD.status()
    elif sys.argv[1] == 'restart':
        MyD.stop()
        MyD.start()
    else:
        print("unkown command")
    """
    def __init__(self, pidfile):
        self.pid_file=pidfile

    def get_pid(self):
        if os.path.exists(self.pid_file):
            fp=open(self.pid_file,'r')
            pid=fp.read().strip()
            fp.close()
            if pid:
                return int(pid)

    def set_pid(self,pid):
        if pid:
            fp=open(self.pid_file,'w')
            fp.write('{}'.format(pid))
            fp.flush()
            fp.close()
            return True
        return False

    def start(self):
       pid=self.get_pid()
       if pid and os.path.isdir('/proc/{}'.format(pid)):
           print('Already running')
       else:
           if os.path.exists(self.pid_file):
               os.remove(self.pid_file)
           try:
               pid = os.fork()
               if pid > 0:
                   self.set_pid(pid)
                   os._exit(0)
           except OSError, error:
               print('Unable to fork. Error: %d (%s)' % (error.errno, error.strerror))
               os._exit(1)

           self.run()

    def stop(self):
        pid=self.get_pid()
        if pid and os.path.isdir('/proc/{}'.format(pid)):
            try:
                os.kill(pid, signal.SIGTERM)
            except OSError as err:
                if str(err).find("No such process") > 0:
                    if os.path.exists(self.pid_file):
                        os.remove(self.pid_file)
                else:
                    sys.stdout.write(str(err))
                    sys.exit(1)
        else:
           if os.path.exists(self.pid_file):
               os.remove(self.pid_file)
           print('Already stopped')

    def status(self):
        pid=self.get_pid()
        if pid and os.path.isdir('/proc/{}'.format(pid)):
            print('running')
            return True
        else:
            print('stopped')
            return False

    def run(self):
        pass
