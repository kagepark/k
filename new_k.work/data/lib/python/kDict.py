#Kage
_k_version='1.0.9'

import cPickle as pickle
import kCrypt as ked
import os

def save_db(filename,data,enc=True):
    if os.path.isdir(os.path.dirname(filename)):
        dump_data=pickle.dumps(data)
        if enc:
            dump_data=ked.encrypt(dump_data)
        with open(filename,'w') as f:
            f.write(dump_data)

def open_db(filename,enc=True):
    if os.path.isfile(filename):
        with open(filename,'r') as f:
            data=f.read()
        if enc:
            return pickle.loads(ked.decrypt(data))
        else:
            return pickle.loads(data)

# function name format
# def <type>_<function>():

def dict_get_key_found_values(dic,find,like=False):
    def get_id_correct_value():
        return [ii for ii,vv in enumerate(dic.values()) if find == vv]
    def get_id_like_value(): # dict value is must be string
        return [ii for ii,vv in enumerate(dic.values()) if find in str(vv)]
    rc=[]
    if like:
        for i in get_id_like_value():
            rc.append(dic.keys()[i])
    else:
        for i in get_id_correct_value():
            rc.append(dic.keys()[i])
    return rc

def dict_get_key_found_all_values(dic,find,like=False,sub=False,rc=[]):
    if sub:
        rc=dict_get_key_found_values(dic,find,like=like)
        for ii in [iii for iii,vv in enumerate(dic.values()) if type(vv) is dict]:
            idkey=dic.keys()[ii]
            isrc=dict_get_key_found_all_values(dic.values()[ii],find,like=like,sub=sub)
            if isrc:
                for isr in isrc:
                    rc.append('{}/{}'.format(idkey,isr))
        return rc
    else:
        return dict_get_key_found_values(dic,find,like=like)

def dict_get_value_from_path(dic,path):
    for ii in path2list(path):
        if ii in dic:
            dic=dic[ii]
        else:
            return False
    return dic
    
def dict_get_first_key(dic):
    return next(iter(dic))

def string2dict(string):
    if type(string) is str:
        try:
            return ast.literal_eval(string)
        except:
            pass
    return string

def dict_check_key(dic,path):
    for key in path2list(path):
        if key in dic:
            dic=dic[key]
        else:
            return False
    return True

def path2list(path):
    path_arr=path.split('/')
    if not path_arr[0]:
        path_arr = path_arr[1:]
    if not path_arr[-1]:
        path_arr = path_arr[:-1]
    return path_arr

def dict_put_val_bak(dic,path,value,force=False):
    path_arr=path2list(path)
    mx=len(path_arr)-1
    new_dic=dic
    for ii in range(0,mx):
       if not path_arr[ii] in new_dic:
          if force:
              new_dic[path_arr[ii]]={}
          else:
              return False
       dic=new_dic[path_arr[ii]]
       new_dic=dic
    if len(path_arr[mx]) > 0:
       if not path_arr[mx] in new_dic:
          new_dic[path_arr[mx]]={}
       new_dic[path_arr[mx]]=value
       dic=new_dic
       return True

def dict_put_val(dic,path,value,force=False):
    path_arr=path2list(path)
    for ii in path_arr[:-1]:
       if not ii in dic:
          if force:
              dic[ii]={}
          else:
              return False
       dic=dic[ii]
    if path_arr[-1]:
       if not path_arr[-1] in dic:
          dic[path_arr[-1]]={}
       dic[path_arr[-1]]=value
       return True
    return False

if __name__ == '__main__':
    # Example)
    a={'a1':'123','a2':'4627','a3':{'z3':'196','uu':{'mm':'196'}},'b2':'196','c':198}
    print('1',dict_get_val_bak(a,'/a3/z2/new/new2'))
    print('2',dict_put_val(a,'/a3/uu3/hdi','abc'))
    print(a)
    print('3',dict_put_val_bak(a,'/a3/uu','abc'))
    print('3',dict_put_val(a,'/a3/uu','abc'))
    print(a)
    #
    #print(dict_get_key_found_all_values(a,'19',sub=True,like=True))
    #print(dict_get_value_from_path(a,'a3/uu/mm'))
    #print(dict_get_value_from_path(a,'a3/uu'))
    #print(dict_get_first_key(a))

