#Kage
_k_version='1.3.60'
import time
import kSms
import kMyQ as myq
from datetime import datetime

def _log(msg,log=None):
   if log:
       log(msg)

def _dbg(level,msg,dbg=None):
   if dbg:
       dbg(level,msg)

def now():
    return int(datetime.now().strftime('%s'))

def garage_status(garage_db,log=None,dbg=None,notice_to=None,monitor=True,garage_interval=240):
    if notice_to is None and 'notice_to' in garage_db:
        notice_to=garage_db['notice_to']

    # Support device check
    if garage_db['dev'] != 'myq':
        _log("Not support device",log)
        return False
    if 'interval' in garage_db:
        garage_interval=int(garage_db['interval'])
#    print('garage_status monitor:',monitor)
    if monitor and garage_db['status_at'] > 0 and (now() - garage_db['status_at']) < garage_interval:
        _dbg(5,"Garage door not monitoring time, {} < {} ?".format((now() - garage_db['status_at']),garage_interval),dbg)
#        print("Garage door not monitoring time, {} < {} ?".format((now() - garage_db['status_at']),garage_interval))
        return

    if not 'api' in garage_db or garage_db['api'] is None:
        garage_db['api']=myq.MyQ(garage_db['id'],garage_db['pass'])
        garage_db['token'] = None
        garage_db['dev_id'] = None
        garage_db['open_at']=0
        garage_db['status_at']=0
        _log("reset garage api",log)
    if garage_db['token'] is None:
        try:
            garage_db['token']=garage_db['api'].login()
            _log("garage login 1",log)
        except:
            garage_db['api'] = None
            _log("set garage api to None",log)
            return False
        garage_db['dev_id']=garage_db['api'].get_device_id(garage_db['token'])
    try:
        status_str = garage_db['api'].door_status(garage_db['token'],garage_db['dev_id'])
        garage_db['status_at'] = now()
        garage_db['status']=status_str
    except:
        garage_db['token'] = None
        return False

    # check exception status
    garage_msg='Garage door Unknown'
    if status_str == 'closed' and garage_db['open_at'] > 0:
        garage_db['open_at']=0
        garage_db['closed_at']=now()
        garage_msg='Garage door changed status to closed'
    elif status_str == 'open' and garage_db['open_at'] == 0:
        garage_db['open_at']=now()
        garage_db['closed_at']=0
        garage_msg='Garage door changed status to open'
    elif status_str == 'moving' or status_str == 'closing' or status_str == 'opening':
        garage_msg='Garage door {}'.format(status_str)
    _dbg(4,garage_msg,dbg)
    if notice_to:
        kSms.sms_send(notice_to,garage_msg)
    return status_str

def garage(garage_db,cmd=None,delay_close=0,log=None,dbg=None,notice_to=None,monitor=True):
    # check API in DB
    if notice_to is None and 'notice_to' in garage_db:
        notice_to=garage_db['notice_to']
    if cmd == 'status':
        return garage_status(garage_db,log=log,dbg=dbg,notice_to=notice_to,monitor=monitor)
    else:
        status=garage_status(garage_db,log=log,dbg=dbg,notice_to=notice_to,monitor=False)
    #print(cmd,status,garage_db)
    _dbg(4,"Current garage door status : {0}, with delay time {1} sec".format(status,delay_close),dbg)
    garage_api=garage_db['api']
    token=garage_db['token']
    dev_id=garage_db['dev_id']
    if not 'open_at' in garage_db:
        garage_db['open_at']=now()
    # Auto close function
    opened_time=(now() - int(garage_db['open_at']))
    if delay_close > 0 and garage_db['autoclose']:
         if opened_time < int(delay_close):
            if opened_time > int(float(delay_close) * 0.8): # send notice
                if garage_db['status'] == 'open' and garage_db['open_at'] > 0:
                    estimate_time=(garage_db['open_at'] + delay_close - now())//60
                    _log("Garage door will close in {0} min by KHA".format(estimate_time),log)
                    kSms.sms_send(notice_to,"Garage door will close in {0} min by KHA".format(estimate_time))
            return 
         #Over time for delay_close then do something according to below command define
    # do command to garage
    if cmd == 'open' and status == 'open'  and garage_db['open_at'] > 0 and garage_db['status'] != status:
        garage_db['closed_at']=0
        garage_db['status']='open'
        _log("Garage door already opened",log)
        return
    elif cmd == 'close' and status == 'closed' and garage_db['closed_at'] > 0 and garage_db['status'] != status:
        garage_db['open_at']=0
        garage_db['status']='closed'
        _log("Garage door already closed",log)
        return
    elif cmd == 'open' and status == 'closed':
        kSms.sms_send(notice_to,"KHA try open Garage door, why?")
        _log("KHA try open Garage door, why?",log)
    elif cmd == 'close' and status == 'open':
        kSms.sms_send(notice_to,"do close garage door by KHA")
        _log("do close garage door by KHA",log)
        rc=garage_api.door_close(token,dev_id)
        if not rc:
             time.sleep(30)
             rc=garage_api.door_close(token,dev_id)
        if rc:
             kSms.sms_send(notice_to,"Garage door close by KHA")
             _log("Garage door closed",log)
        else:
             kSms.sms_send(notice_to,"Garage door close fail by KHA")
             k.log("Garage door close fail")
        return rc
    return

if __name__ == "__main__":
   import os
   import kha_dict_q
   import kLog
   import time
   kha=kha_dict_q.kha
   kLog._kv_dbg=4
   status=garage(kha['devices']['locker']['garage'],cmd='status',log=kLog.log,dbg=kLog.dbg)
   print(status)
   kha['devices']['locker']['garage']['interval']=1
   #print(status)
   time.sleep(10)
   if status == 'open':
       print(garage(kha['devices']['locker']['garage'],cmd='close',log=kLog.log,dbg=kLog.dbg))
