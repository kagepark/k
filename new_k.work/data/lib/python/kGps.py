# Kage Park
# New design for KHA(Kage Home Automation)
# New design at 2018/05/19
_k_version='1.3.12'

def icloud_login(username=None,password=None):
    try:
          api = PyiCloudService(username, password)
    except:
          k.log('wrong password or username({})'.format(username))
          return

    aa = c.find_method_in_class(api,'requires_2sa')
    #if cep.find_method_in_class(api,'requires_2sa'):
    if aa:
        two_fa=api.requires_2sa
    else:
        two_fa=api.requires_2fa

    #if api.requires_2sa:
    if two_fa:
        import click
        print "Two-step authentication required. Your trusted devices are:"
        
        devices = api.trusted_devices
        for i, device in enumerate(devices):
            print("  %s: %s" % (i, device.get('deviceName',"SMS to %s" % device.get('phoneNumber'))))
        
        device = click.prompt('Which device would you like to use?', default=0)
        device = devices[device]
        if not api.send_verification_code(device):
            k.log('Failed to send verification code')
            sys.exit(1)
        
        code = click.prompt('Please enter validation code')
        if not api.validate_verification_code(device, code):
            k.log("Failed to verify verification code")
            sys.exit(1)

    return api

#########################################
# Get login devices
#########################################
def gps_login(user,passwd,gps_type='icloud'):
    if gps_type == 'icloud':
        return icloud_login(username=user,password=passwd)
    #elif log_dev == 'android':
    #    gps_db[log_dev][name]=icloud_login(username=aaa['id'],password=aaa['pass'])
   return gps_db


