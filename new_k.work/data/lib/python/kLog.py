from datetime import datetime
import os
import traceback
import inspect
_k_version='1.1.11'

_kv_dbg=0
def log(msg=None,level=0,detail=False,filename=None):
    '''Logging or debugging'''
    log_msg='{0} : {1}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"),msg)
    if filename is not None:
       new_path=os.path.dirname(filename)
       if os.path.isdir(new_path) is False:
           os.mkdir(new_path)
       f = open(filename,'a')
       f.write(log_msg + '\n')
       f.close()
       dbg(level=_kv_dbg,msg=msg,detail=False,filename=filename)
    else:
       print(log_msg)

def dbg(level=0,msg=None,detail=False,filename=None):
    """ debugging logging process """
    if _kv_dbg < level:
      return True
    stack = traceback.extract_stack()
    frame = inspect.currentframe().f_back
    argvalues = inspect.getargvalues(frame)
    arg_str = inspect.formatargvalues(*argvalues)
    total=len(stack) - 1
    now = datetime.now()
    if detail:
      dmsg = '''{0}: + {1} at {2} in {3}, call {4} at {5} line'''.format(now.strftime("%H:%M:%S"),stack[0][3],stack[0][1],stack[0][0],stack[1][3],stack[1][1])
    else:
      dmsg = '''{0}: + {1}'''.format(now.strftime("%H:%M:%S"),stack[0][3])
    tap="          "
    for ii in range(2,total):
        for jj in range(1,ii):
            tap=tap+" "
        if detail:
            if ii == total-1:
                dmsg = '''\n{0}{1}\_. {2} {3} in {4}, call {5} at {6} line'''.format(dmsg,tap,stack[ii][2],arg_str,stack[ii][0],stack[ii][3],stack[ii][1])
            else:
                dmsg = '''\n{0}{1}\_. {2} in {3}, call {4} at {5} line'''.format(dmsg,tap,stack[ii][2],stack[ii][0],stack[ii][3],stack[ii][1])
        else:
            dmsg = '''\n{0}{1}\_. {2}'''.format(dmsg,tap,stack[ii][2])
    dmsg = '{0}\n{1} => {2}'.format(dmsg,tap,msg)
    if filename is not None:
        f = open('{0}.{1}'.format(filename,now.strftime("%Y-%m-%d")),'a')
        f.write(dmsg + '\n')
        f.close()
    else:
        print(dmsg)

