# Kage Park
# New design for KHA(Kage Home Automation)
# New design at 2018/05/19

_k_version='1.7.21'

import kRouter
import kGps

def clean_otherplace(kha,TMPDB,obj_name):
    if not 'place' in TMPDB.keys():
        return
    # clean up remained place log when come in to any place
    for clpls in TMPDB['place'].keys():
          if obj_name in TMPDB['place'][clpls].keys():
              notice(kha,obj_name,clpls,'out')
              k.log("%s come out %s" % (obj_name,clpls))
              del TMPDB['place'][clpls][obj_name] # remove name from in_member


def check_in_place(dic_member,dic_location):
   '''
   Check object in monitoring place
   '''
   being_member={'home':[]}
   for member in dic_member.keys():
       if 'monitor_at' in dic_member[member] and (now() - dic_member[member]['monitor_at']) < dic_member[member]['wait_time']:
           continue
       if 'router' in dic_member[member]: # Router
           if not 'api' in dic_member[member]['router']: #initial login and get api
               dic_member[member]['router']['api'] = kRouter.get_api(dic_member[member]['router']['id'],dic_member[member]['router']['pass'],dic_member[member]['router']['ip'])
           find_mac_rc=find_mac(dic_member[member]['router'],dic_member[member]['router']['mac']):
           if find_mac_rc:
               dic_member[member]['monitor_at']=now()
               dic_member[member]['last_gps']=dic_location['home']['point'] # gps location update at last monitored gps
               if not member in being_member['home']:
                    being_member['home'].append(member)
               continue # if found member in router then do not check the member at GPS
           elif find_mac_rc is False: # Can't login
                dic_member[member]['router']['api'] = kRouter.get_api(dic_member[member]['router']['id'],dic_member[member]['router']['pass'],dic_member[member]['router']['ip'])

       if 'gps' in dic_member[member]: # GPS (icloud/google)
           if not 'api' in dic_member[member]['gps']: #initial login and get api:
               dic_member[member]['gps']['api']=gps_login(dic_member[member]['gps']['id'],dic_member[member]['gps']['pass'])
           gps_point=get_gps(dic_member[member]['gps']['api'],dic_member[member]['gps']['key'])
           if gps_point:
               dic_member[member]['monitor_at']=now()
               dic_member[member]['last_gps']=gps_point # gps location update at last monitored gps
               for place in dic_location.keys():
                   if kObj.radius(dic_location[place]['point'],gps_point) < dic_location[place]['boundary']:
                       if not place in being_member:
                           being_member[place]=[]
                       being_member[place].append(member)
           else: # Can't get then relogin and get api
               dic_member[member]['gps']['api']=gps_login(dic_member[member]['gps']['id'],dic_member[member]['gps']['pass'])
   return being_member
   # how many place occupyed by member : len(being_member)
   # how many person in home : len(being_member['home'])


def place_notice(being_member,dic,state='in'):
    for place in being_member.keys():    
        if place in dic['location'].keys():
            if 'notice' in dic['location'][place].keys():
                for name in dic['location'][place]['notice'].keys(): 
                    if 'text' in dic['notice'][name] and state in dic['location'][place]['notice'][name]:
                        # It need update to (In and Out boundary)
                        kSms.sms_send(dic['notice'][name]['text'],dic['location'][place]['notice'][name][state])
                    if 'email' in dic['notice'][name] and state in dic['location'][place]['notice'][name]:
                        # It need update to (In and Out boundary)
                        kSms.email_send(dic['notice'][name]['email'],dic['location'][place]['notice'][name][state])
