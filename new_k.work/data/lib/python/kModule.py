import imp,importlib
import sys
import os
import inspect

def get_method_in_class(_class_, func_name):
    '''
    Get method(function) object from class (object or instance) for function name(string text)
    _class_ : class object (<module>.<classname>) or class instance (<module>.<classname>())
    '''
    if hasattr(_class_,'__class__'):
        try:
            return getattr(_class_,func_name)
        except:
            return
    #elif inspect.isclass(_class_): # Same function as below. but sometimes 2.7.12 has issue
    elif hasattr(_class_,'__dict__'):
        class_dic=_class_.__dict__
        if func_name in class_dic:
            return class_dic[func_name]

def get_module_obj(module_name=None,global_var=globals()):
    '''
    get module object from module name(String Text)
    '''
    if module_name:
        #if module_name in globals():
        if module_name in global_var:
            #return globals()[module_name]
            return global_var[module_name]
        elif module_name in sys.modules:
            return sys.modules[module_name]
    return inspect.getmodule(inspect.stack()[1][0]) #self module ???

def get_class_in_module(module,class_name):
    '''
    get class object from module object for class name (string text)
    '''
    try:
        return getattr(module,class_name)
    except:
        return

def get_class_instance_in_module(module,class_name):
    '''
    get class instance from module object for class name (string text)
    '''
    try:
        return getattr(module,class_name)()
    except:
        return

def load_mod(filename,unload=False): # .pyc or .py or <module name>(string)
    if os.path.isfile(filename):
       file_path=os.path.dirname(os.path.abspath(filename))
       file_name=os.path.basename(filename)
       if not file_path in sys.path:
           sys.path.append(file_path)
       filename=os.path.splitext(file_name)[0]
       if not os.path.splitext(file_name)[1] in ['.py','.pyc']:
           print('wrong format file type')
           return False
    if unload:
       try:
           imp.reload(get_module_obj(filename))
           #del module correctly work
       except:
           pass
    try:
       return importlib.import_module(filename)
    except:
       print("\nCan't load {0}\r\n".format(filename))
    return False

def load_code_module(code,name,unload=False):
    if unload:
       try:
           imp.reload(get_module_obj(name))
       except:
           pass
    module=imp.new_module(name)
    try:
        exec code in module.__dict__
    except:
        return False
    return module

def imports(code=None,name=None,filename=None,global_var=None,unload=False):
#    def load_mod(filename): # .pyc or .py or <module name>(string)
#        if os.path.isfile(filename):
#           file_path=os.path.dirname(os.path.abspath(filename))
#           file_name=os.path.basename(filename)
#           if not file_path in sys.path:
#               sys.path.append(file_path)
#           filename=os.path.splitext(file_name)[0]
#           if not os.path.splitext(file_name)[1] in ['.py','.pyc']:
#               print('wrong format file type')
#               return False
#        if unload:
#           try:
#               imp.reload(self.get_module_obj(filename))
#               #del module correctly work
#           except:
#               pass
#        try:
#           return importlib.import_module(filename)
#        except:
#           print("\nCan't load {0}\r\n".format(filename))
#        return False
#    def load_code_module(code,name):
#        if unload:
#           try:
#               imp.reload(self.get_module_obj(name))
#           except:
#               pass
#        module=imp.new_module(name)
#        try:
#            exec code in module.__dict__
#        except:
#            return False
#        return module

    if (code and name) or filename:
        module=False
        if filename:
            if os.path.isfile(filename): # .py .pyc  .txt ....
                module=load_mod(filename,unload)# load .py and .pyc
                if module is False:# Read Python Code from .py or .txt ...
                    with open(filename,'r') as f:
                        code=f.read()
                    if name is None:
                        file_name=os.path.basename(filename)
                        name=os.path.splitext(file_name)[0]
                    module=load_code_module(code,name,unload)
            else:
                module=load_mod(filename,unload)# load module name
                if module is False:
                    print("Can't load {0}".format(filename))
                    return False
        else:
            module=load_code_module(code,name,unload) # load code
        if module:
            if global_var:
                global_var[module.__name__]=module
            return module
    else:
        print("Can't load {0}".format(name))
        return False



