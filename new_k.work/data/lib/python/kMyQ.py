import requests
import os
import json
import time

class MyQ:

    APP_ID = "Vj8pQggXLhLy0WHahglCD4N1nAkkXQtGYpq2HrHD7H1nvmbT55KqtN6RSF4ILB/i"
    LOCALE = "en"
    HOST_URI = "myqexternal.myqdevice.com"
    LOGIN_ENDPOINT = "api/v4/User/Validate"
    DEVICE_LIST_ENDPOINT = "api/v4/UserDeviceDetails/Get"
    DEVICE_SET_ENDPOINT = "api/v4/DeviceAttribute/PutDeviceAttribute"

    myq_cached_login_response   = ""
    myq_device_id               = False
    myq_lamp_device_id          = False

    def __init__(self, username, password,k_class=None):
        self.username = username
        self.password = password
        if k_class is not None:
           k_class.log('Module load : Garage Door(MyQ)')
    
    def door_open(self,security_token,device_id):
        return self.change_door_state("open",device_id,security_token)
    
    def door_close(self,security_token,device_id):
        return self.change_door_state("close",device_id,security_token)
        
    def door_status(self,security_token,device_id):
        state = self.check_device_state(security_token,device_id, "doorstate")
        if state is None:
            return
        if state == "1":
            return "open"
        elif state == "2":
            return "closed"
        elif state == "3":
            return "stopped"
        elif state == "4":
            return "opening"
        elif state == "5":
            return "closing"
        elif state == "8":
            return "moving"
        elif state == "9":
            return "open"
        else:
            return 
    
    def lamp_on(self,security_token,device_id):
        self.change_lamp_state("on",device_id,security_token)
    
    def lamp_off(self,security_token,device_id):
        self.change_lamp_state("off",device_id,security_token)
    
    def lamp_status(self,security_token,device_id):
        state = self.check_device_state(security_token,device_id, "lightstate")
        if state == "0":
            return "off"
        elif state == "1":
            return "on"
        else:
            return "unknown"
    
    
    def login(self):
        params = {
            'username': self.username,
            'password': self.password
        }
        try:
           login = requests.post(
                'https://{host_uri}/{login_endpoint}'.format(
                    host_uri=self.HOST_URI,
                    login_endpoint=self.LOGIN_ENDPOINT),
                    json=params,
                    headers={
                        'MyQApplicationId': self.APP_ID
                    }
           )
    
        except:
           return False
        auth = login.json()
        return auth['SecurityToken']
        #self.myq_security_token = auth['SecurityToken']
        #return True
    
    def change_device_state(self, payload,security_token):
        device_action = requests.put(
            'https://{host_uri}/{device_set_endpoint}'.format(
                host_uri=self.HOST_URI,
                device_set_endpoint=self.DEVICE_SET_ENDPOINT),
                data=payload,
                headers={
                    'MyQApplicationId': self.APP_ID,
                    'SecurityToken': security_token
                }
        )
        return device_action.status_code == 200
    
    def change_lamp_state(self, command,device_id,security_token):
        newstate = 1 if command.lower() == "on" else 0
        payload = {
            "attributeName": "desiredlightstate",
            "myQDeviceId": device_id,
            "AttributeValue": newstate
        }
        return self.change_device_state(payload,security_token)
    
    def change_door_state(self, command,device_id,security_token):
        open_close_state = 1 if command.lower() == "open" else 0
    
        payload = {
            'attributeName': 'desireddoorstate',
            'myQDeviceId': device_id,
            'AttributeValue': open_close_state,
        }
        return self.change_device_state(payload,security_token)
    
    def get_devices(self,security_token):
        try:
           devices = requests.get(
             'https://{host_uri}/{device_list_endpoint}'.format(
                host_uri=self.HOST_URI,
                device_list_endpoint=self.DEVICE_LIST_ENDPOINT),
                headers={
                    'MyQApplicationId': self.APP_ID,
                    'SecurityToken': security_token
                }
           )
           return devices.json()['Devices']
        except:
           return
    
    def get_device_id(self,security_token):
        devices = self.get_devices(security_token)
        if devices is None:
           return
        for dev in devices:
            #print("Device => " + dev["MyQDeviceTypeName"])
            if (not self.myq_device_id) and dev["MyQDeviceTypeName"] in ["VGDO", "GarageDoorOpener", "Garage Door Opener WGDO"]:
                return str(dev["MyQDeviceId"])
            elif (not self.myq_lamp_device_id) and dev["MyQDeviceTypeName"] == "LampModule":
                return str(dev["MyQDeviceId"])
                
    def check_door_state(self,security_token,device_id):
        return self.check_device_state(security_token,device_id, "doorstate")
    
    def check_lamp_state(self,security_token,device_id):
        return self.check_device_state(security_token,device_id, "lightstate")
        
    def check_device_state(self, security_token,device_id, state_name):
        devices = self.get_devices(security_token)
        if devices is None:
           return
        for dev in devices:
            if str(dev["MyQDeviceId"]) == str(device_id):
                for attribute in dev["Attributes"]:
                    if attribute["AttributeDisplayName"] == state_name:
                        door_state = attribute['Value']
                        return door_state
        return



if __name__ == "__main__":

   USERNAME = ""
   PASSWORD = ""
   mq = MyQ(USERNAME,PASSWORD)
   token=mq.login()
   dev_id=mq.get_device_id(token)

   doorstate=mq.door_close(token,dev_id)
   #doorstate=mq.door_open(token,dev_id)
   #doorstate = mq.door_status(token,dev_id)
   print(doorstate)
