#
# Kage Park
# for Kage Network application
#
_k_version='1.0.26'

import socket,os,sys
import kCrypt as ked
import struct # for packet type
import cPickle as pickle
from threading import Thread # for each client connection working
import re

MAX_BUFFER_SIZE=4096
SESSION_END='kSe'
ENC_DATA=True

def sreplace(pattern,sub,string):
    return re.sub('^%s' % pattern, sub, string)

def ereplace(pattern,sub,string):
    return re.sub('%s$' % pattern, sub, string)

def is_mac4(mac=None,symbol=':'):
    if mac is None or type(mac) is not str:
        return False
    octets = mac.split(symbol)
    if len(octets) != 6:
        return False
    for i in octets:
        try:
           if len(i) != 2 or int(i, 16) > 255:
               return False
        except:
           return False
    return True

def is_ipv4(ipadd=None):
    if ipadd is None or type(ipadd) is not str or len(ipadd) == 0:
        return False
    ipa = ipadd.split(".")
    if len(ipa) != 4:
        return False
    for ip in ipa:
        if not ip.isdigit():
            return False
        if not 0 <= int(ip) <= 255:
            return False
    return True

def ip2num(ip):
    if is_ipv4(ip):
        return struct.unpack("!L", socket.inet_aton(ip))[0]
    return False

def ip_in_range(ip,start,end):
    if type(ip) is str and type(start) is str and type(end) is str:
        ip=ip2num(ip)
        start=ip2num(start)
        end=ip2num(end)
        if start <= ip and ip <= end:
            return True
    return False

def ipv4(ipaddr=None):
    if type(ipaddr) is str:
        ipaddr_a=ipaddr.strip().split('.')
        new_ip=''
        for i in ipaddr_a:
            for j in range(0,10):
                if len(i) <= 1:
                    break
                i=sreplace('^0','',i)
            if new_ip:
                new_ip+='.{}'.format(i)
            else:
                new_ip=i
        if len(new_ip.split('.')) == 4:
            return new_ip
    return False
#ipv4('172.16.0.3')
#ipv4('172.016.0.3')
#ipv4('172.16.0.003')
#ipv4('002.16.0.3')
#ipv4('072.16.0.3')
#ipv4('072.016.000.03')
#ipv4('072.00016.000000.03')
#ipv4('')
#ipv4(12342)
#ipv4(123.42)
#ipv4(None)
#ipv4(True)

#################################################################################
def host_ip(hostname=None,cmd_dict={}):
    if hostname:
        if 'hosts' in cmd_dict and hostname in cmd_dict['hosts']:
            return cmd_dict['hosts'][hostname]['ip']
        else:
            try:
                return socket.gethostbyname(hostname)
            except socket.gaierror:
                return
    else:
        return socket.gethostbyname(socket.gethostname())

# Generate socket in Client side
def gen_client_socket_ip(ip,port): # ip: IP only
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    soc.connect((ip,port))
    return soc

def gen_client_socket(host,port): # host : Host name or IP
    for res in socket.getaddrinfo(host, port, socket.AF_UNSPEC, socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res
        try:
            soc = socket.socket(af, socktype, proto)
        except socket.error as msg:
            soc = None
            continue
        try:
            soc.connect(sa)
        except socket.error as msg:
            soc.close()
            soc = None
            continue
        break
    if soc is None:
        print('could not open socket for {0}:{1}'.format(host,port))
        os._exit(1)
    return soc

#################################################################################
# Send data each MAX_BUFFER_SIZE, fixed packet size
def put_data(soc,data):
    if ENC_DATA:
        soc.sendall(ked.encrypt(data))
    else:
        soc.sendall(data)

def get_data(conn, MAX_BUFFER_SIZE=MAX_BUFFER_SIZE):  # Receive data and decrypt the data, return readable data
    input_from_client_bytes = conn.recv(MAX_BUFFER_SIZE)
    siz = sys.getsizeof(input_from_client_bytes)
    if  siz >= MAX_BUFFER_SIZE:
        print("The length of input is probably too long: {}".format(siz))
    if ENC_DATA:
        return ked.decrypt(input_from_client_bytes)
    else:
        return input_from_client_bytes

#################################################################################
# send data Packet type with packet size, flexable packet size
def put_packet(soc,data):
    '''
    It need below code for socket (soc)
    soc=kNet.gen_client_socket(ip,port)   # Gen socket
    '''
    dict_data=pickle.dumps(data)
    if ENC_DATA:
        pdata=ked.encrypt(dict_data)
    length=len(pdata)
    soc.sendall(struct.pack('!I',length)) # Header for data size
    soc.sendall(pdata)                    # Send body data

def get_packet(conn,progress=None):  # Receive data and decrypt the data, return readable data
    lengthbuff=recvall(conn,4)
    if lengthbuff:
        length, = struct.unpack('!I',lengthbuff) # Header for data size
        if type(progress) is str:
            gdata = recvall(conn,length,progress=progress) 
        else:
            gdata = recvall(conn,length) 
        if ENC_DATA:
            dict_data=pickle.loads(ked.decrypt(gdata))
        else:
            dict_data=pickle.loads(gdata)
        return dict_data

def recvall(conn,count,progress=None): # Packet 
    buf = b''
    file_size_d=int('{0}'.format(count))
    status=''
    while count:
        newbuf = conn.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
        if progress is not None:
            perc=(file_size_d - count) * 100. /file_size_d
            status = "%s: [%3.1f%%]\r" %(progress,perc)
            sys.stdout.write(status)
            sys.stdout.flush()
    if status:
        print('')
    return buf

def packet(cmd=None,scmd=None,opt=None,src=None,dest=None,rc=True,data_type=None,data=None,info={},timeout=600):
    if data is not None and data_type is None:
        data_type=type(data).__name__
    size=0
    if data_type == 'str':
        size=len(data)
    kpk={
       'cmd':cmd, # Packet command
       'scmd':scmd, # Server command
       'opt':opt,  # Server command option
       'src':src, # source
       'dest':dest, # dest
       'rc':rc,
       'type':data_type,
       'size':size,
       'timeout':timeout,
       'info':info,
       'data':data
    }
    return kpk
#################################################################################
# Server daemon process
def start_server(server_ip,server_port,thread_func_name,max_connection=10): # Start daemon
    '''Server Function
       start_server() give parater to thread_func_name()
         - conn : client connection socket
         - ip   : client IP
         - port : client port
         - max_connection: default 10, maximum connection support

       server_ip: Server lesson IP
       server_port : Server lesson port
       thread_func_name: Server Function for thread
       -------------------------------------
       def <func name>(conn,ip,port,....):
          # code here
       -------------------------------------
    '''
    ssoc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ssoc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    try:
        ssoc.bind((server_ip, server_port))
    except socket.error as msg:
        print('Bind failed. Error : {0}'.format(msg))
        os._exit(1)
    ssoc.listen(max_connection)
    print('Start server for {0}:{1}'.format(server_ip,server_port))

    # for handling task in separate jobs we need threading
    while True:
        conn, addr = ssoc.accept()
        ip, port = str(addr[0]), str(addr[1])
        try:
            Thread(target=thread_func_name, args=(conn, ip, port)).start()
        except:
            print('No more generate thread for client from {0}:{1}'.format(ip,port))
    ssoc.close()

