_k_version='1.3.30'

def notice(kha_db,obj_name,place,mode='in'):
    if 'notice' in kha_db['location'][place].keys():
        if obj_name in kha_db['location'][place]['notice'].keys():
             if mode in kha_db['location'][place]['notice'][obj_name].keys():
                 sms_notice(kha_db[obj_name]['phone']['num'],kha_db['location'][place]['notice'][obj_name][mode])

def sms_notice(tel=None,msg=None):
    if tel is None or msg is None:
        return
    k.dbg(3,"[%s] send to [%s]" % (msg,tel))
#    k.osx_sms_send(tel,msg)
    sms_t=threading.Thread(name='sms_notice',target=k.osx_sms_send,args=(tel,msg,))
    sms_t.start()
#    main_thread = threading.currentThread()
#    for sms_t in threading.enumerate():
#        if sms_t is not main_thread:
#            sms_t.join()
