# Kage Park
# New design for KHA(Kage Home Automation)
# New design at 2018/05/19
_k_version='1.2.30'
import os
import time
import sys
import random
#import geopy
import geocoder
from geopy import distance
#great_circle=geopy.distance.great_circle
great_circle=distance.great_circle

def get_address(location_point=None):
    try:
        g = geocoder.google(location_point,method='reverse') # Change to google address
    except:
        return
    return "%s %s,%s,%s %s,%s" % (g.housenumber,g.street,g.city,g.state,g.postal,g.country)

#Not used this function yet
def radius(src_point=None,dest_point=None):
    if type(src_point) is list and type(dest_point) is list:
         return int(great_circle(src_point, dest_point).meters)

def is_on_boundary(radius, bound):
    if radius <= bound :
         return True
    return False

def mile2m(mile):
    return int(mile*1600)

def mile2km(mile):
    return float(mile*1.6)

def adjust_obj(db,name,place,place_bound,count=6):
    if not place in db['place'] or not name in db['place'][place].keys() or not 'adj' in db['place'][place][name]: # <name> Outside <place_bound> of the <place>
        k.dbg(4,"No adjust [%s] : %s" % (name,db))
        return False
    if db['place'][place][name]['distance'] <= place_bound or db['place'][place][name]['adj'] > count:
        k.dbg(4,"No adjust : %s(%s) < %s  or count out (adj:%s)"%(name,db['place'][place][name]['distance'],place_bound,db['place'][place][name]['adj']))
        return False
    db['place'][place][name]['adj'] = db['place'][place][name]['adj'] + 1
    k.dbg(3,"adjust [%s] count %s in %s"%(name,db['place'][place][name]['adj'],place))
    return True

def moving_obj(old_point,now_point,sec,adj=0,moving_boundary=200,adjust=10,moving=False):
    distance=radius(old_point,now_point)
    if distance:
        speed=distance//sec # speed
        if moving:
            return moving,speed
        else:
            if distance > moving_boundary: # suddenly out to moving boundary then adjust for make sure signal issue
                if adj > adjust:
                    return True,0
                else:
                    adj+=1
                    return False,adj
            return False,0
    return False,0

def distance_wait_time(distance):
    if distance > 4800000: # over 3000mil
        return 43200 # 12hours
    elif distance > 3200000: # over 2000mil
        return 14400 # 4hours
    elif distance > 800000: # over 500mil
        return 7200 # 2hours
    elif distance > 160000: # over 100mil
        return 3000 # 50min
    elif distance > 80000: # over 50mil
        return 1200 # 20min
    elif distance > 48000: # over 30mil
        return 600 # 10min
    elif distance > 8000: # over 5mil
        return 300 # 5min
    elif distance > 4800: # over 3mil
        return 120 #2min
    else: # under 3mil
        return 60  #1min

def monitor_obj(home_point,old_obj_point,now_obj_point,sec,obj_name,place_bound,home_boundary,monitor_boundary=8000,adj=0,moving=False,adjust=0,moving_boundary=0,monitor_wait_time=240):
    old_distance=radius(home_point,old_obj_point)
    now_distance=radius(home_point,now_obj_point)

    k.dbg(5,"%s : obj(%s) > monitor(%s)? (%d sec)" % (obj_name,now_distance,monitor_boundary,sec))
    if now_distance > monitor_boundary or old_distance - now_distance < 0 or now_distance < home_boundary: # being home area or moving out or out of monitor_boundary
        return distance_wait_time(now_distance),now_distance # give current distance for check boundary
    # monitor (in boundary area)
    moving,speed=moving_obj(old_obj_point,now_obj_point,sec,adj=adj,moving_boundary=moving_boundary,adjust=adjust,moving=moving)
    if moving:
        k.dbg(5,"%s : moving %s m/s" % (obj_name,speed))
        if speed < 7: # low speed 7m/sec (about 22.5mph), So, wait 
            moving=False
            adjust=6
            moving_boundary=200
            moving_wait_time=240
        else:
            moving=True
            adjust=0
            moving_boundary=1
            moving_wait_time=distance_wait_time(now_distance)
    else:
        adjust=speed
        k.dbg(3,"adjust [%s] count %s in %s"%(obj_name,adjust,get_address(old_obj_point)))
    return moving,adjust,moving_boundary,moving_wait_time

