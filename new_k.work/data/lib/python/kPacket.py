# coding: utf-8
# encoding=utf8
#
# Copyright CEP(Kage Park)
# License : GPL
#   It can be sell by CEP Only.
#   Others according to GPL license(Only Freeware software).
#   Since : March 1994
from __future__ import print_function

###################################################
# Packet Data handle
apc=['get','save','del','ls','find','grep'] # Available Packet Command define
    
def mk_pkd(data,type=None,name=None):
    if data:
        if type is None:
            type=type(data)
        try:
            size=len(data)
        except:
            size=None
        return {'type':type,'data':data,'size':size,'name':name}

def get_pkd(data,key):
    if key in data:
        return data[key]

def pkh():

def put_kpk(cmd=None,fpk={},data=None):
    if key and cmd and cmd in apc:
        return {'cmd':cmd,'file':fpk,'data':data}
    else:
        return {}

def mk_fpk(name=None,data=None):
    if name and data:
        return {'file': {'name':name,'data':data,'size':len(data),}}

def get_kpk(key=None):
    if key:
        if key in kpk:
            return kpk[key]

def info_fpk(kpk,key=None):
    if kpk:
        if 'file' in kpk:
            kpk=kpk['file']
        if key:
            if key in kpk:
                return kpk[key]
        else:
            return {'name':kpk['name'],'size':kpk['size'],'type':kpk['type']}
