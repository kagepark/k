#!/usr/bin/env python
# coding: utf-8
# encoding=utf8
#
# Copyright CEP(Kage Park)
# license : GPL
# Created at 2019-06-24 14:09 by K
#
# python 2.6~ for no newline on print()
# from __future__ import print_function
_k_version="1.0.2"

from pip._internal import main as _main
def pip(*args):
    if len(args):
        _main(list(args))
    else:
        _main(['list'])
