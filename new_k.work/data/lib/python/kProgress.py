from __future__ import print_function
import sys

def progress(pp=0,msg=None):
    pps=['|','/','-','\\','|']
    if msg:
        sys.stdout.write(pps[pp] + '\r')
    else:
        sys.stdout.write(msg + ' : ' + pps[pp] + '\r')
    sys.stdout.flush()
    if pp >= 4:
       pp=0
    else:
       pp=pp+1
    return pp

def progress_percents(msg,percent):
    '''
    example)
    data=<download file data>
    file_size_d=xx
    file_size_s=0
    f = open(save_file, 'wb')
    while True:
       bsize = data.read(8192)
       if not bsize:
          break
       file_size_s += len(bsize)
       f.write(bsize)
       msg="Downloading: %s (%s %s)"%(file_name,file_size_d,file_size_u)
       perc=file_size_s * 100. /file_size
       self.progress_percents(msg,perc)
    f.close()
    '''
    status = "%s: [%3.2f%%]\r" %(msg,percent)
    sys.stdout.write(status)
    sys.stdout.flush()
