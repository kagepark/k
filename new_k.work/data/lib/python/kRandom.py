import random

alpha_strs='''0aA1b2BcC3dD4eE5fF6g7Gh8Hi9IjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ'''
file_symbol='''_-.'''
symobol_str='''?,>:</!#'%&(+=)*^$@`~";\|]{[}'''
def random_string(length=8,strs=alpha_strs):
    new=''
    strn=len(strs)-1
    for i in range(0,length):
        new='{0}{1}'.format(new,strs[random.randint(0,strn)])
    return new

def random_num(length=100):
    return random.randint(0,length)
