#Kage
_k_version='1.2.25'
import pynetgear
# pip install pynetgear

def get_api(user_id,user_pass,router_ip):
    try:
        return pynetgear.Netgear(user_pass,router_ip,user_id)
    except:
        return -1

def find_mac(api,mac):
    attached_devices=api.get_attached_devices() 
    if attached_devices:
        for i in attached_devices:
#            print(i.mac,' <=> ', mac)
            if i.mac.upper() == mac.upper():
                return {'ip':i.ip,'mac':i.mac,'name':i.name}
    else:
        return False
    
def show_all(api):
    attached_devices=api.get_attached_devices() 
    if attached_devices:
        for i in attached_devices:
            print('%30s %15s %17s'%(i.name,i.ip,i.mac))
    

if __name__ == "__main__":
    userid=''
    userpass=''
    router_ip=''
    mac=''
    api=get_api(userid,userpass,router_ip)
    if api:
        info=find_mac(api,mac)
        if info is False:
            print('Fail')
            api=get_api(userid,userpass,router_ip)
        else:
            if info:
                print('found {}'.format(mac))
            else:
                print('Not found {}'.format(mac))

