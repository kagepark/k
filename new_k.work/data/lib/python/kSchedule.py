# Kage Park
# New design for KHA(Kage Home Automation)
# New design at 2018/05/19
_k_version='1.12.34'

#import os
#import time
#import sys
import random
import astral
import kTime 
import kLog as k
import kSms
import kSwitch
from geopy import distance
sw=kSwitch.Switch(k.log,k.dbg)
great_circle=distance.great_circle
k._kv_dbg=4
#k._kv_dbg=1

def is_on_boundary(location, bound, cur_loc):
     if int(great_circle(location, cur_loc).meters) <= bound :
          return True
     return False

def sun_time(loc,cmd):
    l = astral.Location()
    l.latitude = float(loc['latitude'])
    l.longitude = float(loc['longitude'])
    l.timezone = loc['timezone']
    if cmd == 'sunrise' :
        suntime = l.sun()['dawn']
    else:
        suntime = l.sun()['sunset']
    return "{0}:{1}".format(suntime.strftime('%H'),suntime.strftime('%M'))

def check_year(db):
    if not 'year' in db:
        db['year']=[]

    if len(db['year']) == 0:
        return True
    else:
        #year = datetime.now().year
        year = kTime.now().year
        for sch_year in list(db['year']):
            if sch_year == type([]):
                if int(sch_year[0]) <= int(year) and int(year) <= int(sch_year[1]):
                    return True
            else:
                if int(sch_year) == int(year):
                    return True
        return False

def check_month(db):
    if not 'month' in db:
        db['month']=[]

    if len(db['month']) == 0:
        return True
    else:
        #month = datetime.now().month
        month = kTime.now().month
        for sch_month in list(db['month']):
            if sch_month == type([]):
                if int(sch_month[0]) <= int(month) and int(month) <= int(sch_month[1]):
                    return True
            else:
                if int(sch_month) == int(month):
                    return True
        return False

def check_day(db):
    if not 'day' in db:
        db['day']=[]

    if len(db['day']) == 0:
        return True
    else:
        #day = datetime.now().day
        day = kTime.now().day
        for sch_day in list(db['day']):
            if type(sch_day) == type([]):
                if int(sch_day[0]) <= int(day) and int(day) <= int(sch_day[1]):
                    return True
            else:
                if int(day) == int(sch_day):
                    return True
        return False

def check_weeknum(db):
    if not 'weeknum' in db:
        db['weeknum']=[]

    if len(db['weeknum']) == 0:
        return True
    else:
        #day_of_month = datetime.now().day
        day_of_month = kTime.now().day
        week_number = (day_of_month - 1) // 7 + 1
        for sch_weeknum in list(db['weeknum']):
            if week_number and sch_weeknum and int(week_number) == int(sch_weeknum):
                return True
        return False

def check_weekday(db):
    if not 'weekday' in db:
        db['weekday']=[]

    if len(db['weekday']) == 0:
        return True
    else:
        #now_weekday=datetime.now().weekday()
        now_weekday=kTime.now().weekday()
        for sch_weekday in list(db['weekday']):
            if int(now_weekday) == int(sch_weekday):
                return True
        return False

def check_time(db,during=None):
    if not 'time' in db:
        db['time'] = []
    if len(db['time']) == 0:
        return True
    else:
        now=kTime.ptime()
        for nowtime in list(db['time']):
            nowtime_type=type(nowtime)
            if nowtime_type == type([]):
                if len(nowtime) == 2:
                    starttime=kTime.ptime(t=nowtime[0],add='today')
                    endtime=kTime.ptime(t=nowtime[1],add='today')
                    if starttime > endtime:
                        endtime=kTime.ptime(t=endtime.strftime('%Y-%m-%d %H:%M'),add='1d')
                    if starttime <= now and now < endtime:
                        return True
                elif len(nowtime) == 1:
                    nowtime=nowtime[0]
                    nowtime_type=type(nowtime)

            if nowtime_type is str:
                if during is None or during == 0 or during == 1:
                    start_time=kTime.ptime(t=nowtime,p='%H:%M')
                    now_time=kTime.ptime(p='%H:%M')
                    if  start_time == now_time:
                        return True
                else:
                    starttime=kTime.ptime(t=nowtime,add='today')
                    endtime=kTime.ptime(t=starttime.strftime('%Y-%m-%d %H:%M'),add='{0}m'.format(during))
                    if starttime <= now and now < endtime:
                        return True
    return False

def done(db,get_num=False,clear=False):
    if not 'start' in db:
        db['start']=[]
    if clear:
        db['start']=[]
        return
    if get_num:
        return len(db['start'])
    db['start'].append(kTime.ptime(p='%H:%M'))

def is_night(env_db):
    aa=sun_time(env_db['location']['home'],'sunset')
    bb=sun_time(env_db['location']['home'],'sunrise')
    sundb={}
    sundb['time']=[[bb,aa]]
    if check_time(sundb): # daytime
        k.dbg(4,'now day time(ss:'+str(bb)+'~now~'+str(aa)+')')
        return False
    else:
        k.dbg(4,'now night time(ss:'+str(aa)+'~now~'+str(bb)+')')
        return True

def get_list(pt,db):
    if pt == '*' or pt == 'all':
       return db.keys()
    else:
       rc=[]
       if pt in db.keys():
          rc.append(pt)
       if '*' in db.keys():
          rc.append('*')
       return rc

def get_schedule(db=None,pt=None,wd=None):
    # * is all
    # pt=<yyyy>/<mm>/<dd>/<HH>/<MM> for date schedule
    # wd=<weekday>/<HH>/<MM> for week schedule
    if db is None:
       return
    if pt is None:
       snow=kTime.now()
#       snow=datetime.now()
       pd=snow.strftime("%Y/%m/%d")
       ph=snow.strftime("%H/%M")
       wd=snow.weekday()
       pt="{0}/{1}".format(pd,ph)
       wt="{0}/{1}".format(wd,ph)
    else:
       ptat=pt.split('/')
       if wd is None:
          if ptat[0] != '*' and ptat[1] != '*' and ptat[2] != '*':
            ptwd="{0}/{1}/{2}".format(ptat[0],ptat[1],ptat[2])
            #wd=datetime.strptime(pt,"%Y/%m/%d").weekday()
            wd=kTime.strptime(pt,"%Y/%m/%d").weekday()
          else:
            wd=None
       if wd is not None:
          wt="{0}/{1}/{2}".format(wd,ptat[3],ptat[4])
    rt={}
    rc=0
    #Date schedule
    if 'date' in db:
       scdb=db['date']
       pta=pt.split('/')
       yys=get_list(pta[0],scdb)
       if yys is not None:
         for yy in yys:
            mms=get_list(pta[1],scdb[yy])
            if mms is not None:
               for mm in mms:
                  dds=get_list(pta[2],scdb[yy][mm])
                  if dds is not None:
                     for dd in dds:
                        hhs=get_list(pta[3],scdb[yy][mm][dd])
                        if hhs is not None:
                           for hh in hhs:
                              mins=get_list(pta[4],scdb[yy][mm][dd][hh])
                              if mins is not None:
                                 for min in mins:
                                    rt[rc]=scdb[yy][mm][dd][hh][min]
                                    rc=rc+1
                                    #print(yy,mm,dd,hh,min,get_list('*',aaa[sd][yy][mm][dd][hh][min]))
    #Week schedule
    if 'week' in db:
       if wd is not None:
          wsdb=db['week']
          wta=wt.split('/')
          wds=get_list(wta[0],wsdb)
          if wds is not None:
            for swd in wds:
               whhs=get_list(wta[1],wsdb[swd])
               if whhs is not None:
                  for whh in whhs:
                     wmms=get_list(wta[2],wsdb[swd][whh])
                     if wmms is not None:
                        for wmm in wmms:
                           rt[rc]=wsdb[swd][whh][wmm]
                           rc=rc+1

    return rt

def schedule(env_db=None,tmp_db={}):
     if env_db is None:
        return
     if not 'schedule' in env_db:
        return
     if not 'schedule' in tmp_db:
        tmp_db['schedule'] = 0
     db=env_db['schedule']
     #snow=datetime.now()
     snow=kTime.now()
     cur_date=snow.strftime("%Y/%m/%d")
     cur_week=snow.weekday()
     for item in list(db['on']):
         cur_location=None
         if not 'location' in db['on'][item]:
             db['on'][item]['location']={}
         during=None
         tap_block_time=False
         if 'during' in db['on'][item]:
             during=db['on'][item]['during']
             if type(during) is str:
                 if len(during) == 0:
                     during=0
                 else:
                     during=int(during)
             if during == 0 or during == 1:
                 during=None
             else:
                 if 'start' in db['on'][item] and len(db['on'][item]['start']) > 0:
                     if during > 3:
                         new_tap_during=int((during-1)/int(db['on'][item]['count']))
                         if new_tap_during < 1:
                             new_tap_during=1
                     else:
                         new_tap_during=1
                     a={}
                     a['time']=[db['on'][item]['start'][-1]]
                     tap_block_time=check_time(a,during=new_tap_during)
         ctime=check_time(db['on'][item],during=during)
         do_num=done(db['on'][item],get_num=True)
         cweekday=check_weekday(db['on'][item])
         cyear=check_year(db['on'][item])
         cmonth=check_month(db['on'][item])
         cday=check_day(db['on'][item])
         cweeknum=check_weeknum(db['on'][item])
         k.dbg(4,'do switch: item:{0}: times: {1}, ctime:{2}'.format(item,db['on'][item]['time'],ctime))
         if cyear and cmonth and cday and cweeknum and cweekday and ctime:
             if 'random' in db['on'][item] and db['on'][item]['random']:
                 if not 'random_time' in db['on'][item]:
                     db['on'][item]['random_time']=[]
                 if len(db['on'][item]['random_time']) == 0:
                     if during is None:
                         during=10 # Default 10min
                     for rin in range(0,int(db['on'][item]['count'])):
                         new_add_time=random.randint(0,int(during))
                         db['on'][item]['random_time'].append('{0}'.format(kTime.ptime(t=db['on'][item]['time'][0],add='{0}m'.format(new_add_time),p='%H:%M')))

                 k.dbg(4,'do switch: item:{0}: random times: {1}'.format(item,db['on'][item]['random_time']))
                 ignore=True
                 for rtc in list(db['on'][item]['random_time']):
                     k.dbg(4,'do switch: {0} != {1}?'.format(kTime.ptime(t=rtc,p='%H:%M'),kTime.ptime(p='%H:%M')))
                     iiiw = kTime.ptime(p='%H:%M')
                     if kTime.ptime(t=rtc,p='%H:%M') == iiiw and not iiiw in db['on'][item]['start']:
                         ignore=False
                 k.dbg(4,'do switch: ignore by random?{0}'.format(ignore))
                 if ignore:
                     return
             else:
                 if do_num >= db['on'][item]['count'] or tap_block_time:
                     return
             if not 'location' in db['on'][item] or len(db['on'][item]['location']) == 0:
                if 'cmd' in db['on'][item] and 'devices' in db['on'][item]['cmd']:
                   for ss_group in db['on'][item]['cmd']['devices'].keys():
                       for ss_dest in db['on'][item]['cmd']['devices'][ss_group].keys():
                           print(item,ss_group,ss_dest,db['on'][item]['cmd']['devices'])
                           ss_cmd=db['on'][item]['cmd']['devices'][ss_group][ss_dest]['cmd']
                           k.dbg(4,'do switch: group:{0}, dest:{1}, cmd:{2}'.format(ss_group,ss_dest,ss_cmd))
                           sw.do_switch(kha['devices'],cmd=ss_cmd, dest=ss_dest, group=ss_group)
                           done(db['on'][item])

                if 'cmd' in db['on'][item] and 'text' in db['on'][item]['cmd'] and 'to' in db['on'][item]['cmd']['text']:
                   for dest in list(db['on'][item]['cmd']['text']['to']):
                       if 'msg' in db['on'][item]['cmd']['text']:
                           msg=db['on'][item]['cmd']['text']['msg']
                           k.dbg(4,'do text:{0}'.format(msg))
                           kSms.sms_send(dest,msg)
                       else:
                           k.dbg(4,'do text:{0}'.format(db['on'][item]['cmd']['text']))
                           kSms.sms_send(dest,item)
                       done(db['on'][item])


             else:# Location base
                  if  is_on_boundary(db['on'][item]['location']['loc'],db['on'][item]['location']['bound'],  cur_location): # in location boundary
                       if 'cmd' in db['on'][item] and 'text' in db['on'][item]['cmd'] and 'to' in db['on'][item]['cmd']['text']:
                           for dest in list(db['on'][item]['cmd']['text']['to']):
                               if 'msg' in db['on'][item]['cmd']['text']:
                                   msg=db['on'][item]['cmd']['text']['msg']
                                   k.dbg(4,'do text:{0}'.format(msg))
                                   kSms.sms_send(dest,msg)
                               else:
                                   k.dbg(4,'do text:{0}'.format(db['on'][item]['cmd']['text']))
                                   kSms.sms_send(dest,item)
                           done(db['on'][item])
         else:
            done(db['on'][item],clear=True)
            k.dbg(4,'do switch: clean passed time : item:{0}: times: {1}'.format(item,db['on'][item]['start']))
            if 'random_time' in db['on'][item] and len(db['on'][item]['random_time']) > 0:
                k.dbg(4,'do switch: clean random_time : item:{0}: times: {1}'.format(item,db['on'][item]['random_time']))
                db['on'][item]['random_time']=[]

def schedule_on2off(name,db={}):
     if type(db) is dict:
         idb=db
         if 'schedule' in db and 'on' in db['schedule'] and name in db['schedule']['on']:
             if not 'off' in db['schedule']:
                 db['schedule']['off']={}
             idb=db['schedule']['on'].pop(name)
             db['schedule']['off'].update(idb)
             return db
         elif 'on' in db and name in db['on']:
             if not 'off' in db:
                 db['off']={}
             idb=db['on'].pop(name)
             db['off'].update(idb)
             return db

def schedule_off2on(name,db={}):
     if type(db) is dict:
         idb=db
         if 'schedule' in db and 'off' in db['schedule'] and name in db['schedule']['off']:
             if not 'on' in db['schedule']:
                 db['schedule']['on']={}
             idb=db['schedule']['off'].pop(name)
             db['schedule']['on'].update(idb)
             return db
         elif 'off' in db and name in db['off']:
             if not 'on' in db:
                 db['on']={}
             idb=db['off'].pop(name)
             db['on'].update(idb)
             return db

def schedule_db(name,db={},weekday=[],date=[],time=[],start=[],during=1,count=1,instance=False,desc=None,cmd={}):
     if db:
         idb=db
         if 'schedule' in db and 'on' in db['schedule']:
             idb=db['schedule']['on']
         elif 'on' in db:
             idb=db['on']
         if not name in idb:
             idb[name]={}
         if weekday:
             idb[name]['weekday']=weekday
         if date:
             idb[name]['date']=date
         if time:
             idb[name]['time']=time
         if start:
             idb[name]['start']=start
         if during:
             idb[name]['during']=during
         if count:
             idb[name]['count']=count
         if instance:
             idb[name]['instance']=instance
         if desc:
             idb[name]['desc']=desc
         if cmd:
             idb[name]['cmd']=cmd
         return db
     else:
         return {'schedule':{'on':{name:{'weekday':weekday,'date':date,'time':time,'start':[],'during':during,'count':count,'instance':instance,'desc':desc,'cmd':cmd}}}}


if __name__ == "__main__":
    import kha_dict_q
    import time
    kha=kha_dict_q.kha
    while True:
        schedule(env_db=kha)
        time.sleep(25)
