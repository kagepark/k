# Kage
############################
# downloaded/system modules
############################
import sys, os, time
import sqlite3
from datetime import datetime
import kSms 
_k_version="1.3.42"

############################
#Global parameters
############################
class SMS_TALK:
   def __init__(self):
      self.stop = 0
   # receive_account: KHA Server's iMessage accounts (<account1>,<account2>,...) : ignore reply to reply back again
   # sender_account: iPhone user's iMessage accounts (<account1>,<account2>,...) : security for ignore other users
   # sender_account: Self sending message ignore ( None: accept, 1: ignore )

# Example)
#   # talking between sender and server. need more code
#   def listen(self,receive_account=None,sender_account=None,self_sender_ignore=True,imessage_db="{0}/Library/Messages/chat.db".format(os.path.expanduser('~')), find_text=None):
#      monitored_time=0
#      while True:
#          imessage_fetchall=self.read(receive_account=receive_account,sender_account=sender_account,self_sender_ignore=self_sender_ignore,imessage_db=imessage_db,read_time=monitored_time, find_text=find_text)
#          if imessage_fetchall is None:
#             time.sleep(5)
#             continue
#
#          # Message debugging
#          for receive_msg in list(imessage_fetchall):
#             if receive_account is not None:
#                if receive_msg[0] == receive_account:
#                   continue
#             qq=receive_msg[1].rsplit()
#             print("received '{0}' from '{1}'".format(qq,receive_msg[0]))
#             kSms.sms_send(receive_msg[0],'reply: {}'.format(qq[1])) # reply back
#
#          imessage_last=len(imessage_fetchall)
#          if imessage_last > 0:
#              monitored_time=imessage_fetchall[imessage_last-1][2]
#          else:
#              monitored_time=0
#          time.sleep(5)

   def read(self,receive_account=None,sender_account=None,self_sender_ignore=True,imessage_db="{0}/Library/Messages/chat.db".format(os.path.expanduser('~')),read_time=0, find_text=None):
      try:
          imconn=sqlite3.connect(imessage_db)
      except:
          time.sleep(1)
          imconn=sqlite3.connect(imessage_db)
      imlg=imconn.cursor()
      imessage_get="SELECT h.id, m.text, m.date FROM message m JOIN handle h ON m.handle_id=h.ROWID"

      receive_account_str=''
      sender_account_list=''
      self_sender_ignore_str=''
      block_recive_account=''
      set_mode={}
      wait_time=10 # 10min


      if receive_account != "all" and receive_account is not None:
         #imessage_get=imessage_get+" and m.account='e:{0}' and h.id!='{1}'".format(receive_account,receive_account)
         imessage_get=imessage_get+" and m.account='e:{0}' ".format(receive_account)

      if sender_account is not None:
         for ii in list(sender_account.split(',')):
             if sender_account_list == '':
                sender_account_list = "h.id='{0}'".format(ii)
             else:
                sender_account_list = "{0} or h.id='{1}'".format(sender_account_list,ii)
         imessage_get = imessage_get + " and ( {0} ) ".format(sender_account_list)

      if self_sender_ignore:
         imessage_get = imessage_get + ' and m.is_from_me=0'

      if find_text is not None:
          imessage_get = imessage_get + " and m.text LIKE '%{0}%'".format(find_text)

      if read_time > 0:
          imessage_get = imessage_get + " and m.date > %s" % (read_time)

#      if read_time is not None:
#          initial_time=int(time.mktime(datetime(2001,1,1,0,0).timetuple())) - 3600 # 978307200
#          # currently mac os time=Linux UTC time - Mac initial Start time(2001-01-01) + 3600(1hr)
#          monitored_time=(int(time.mktime(datetime.utcnow().timetuple())) - initial_time)
#          print(monitored_time)
#          imessage_get = imessage_get + " and m.date > {0}".format(int(monitored_time))
#          #imessage_get = imessage_get + " and m.date > 547955351064152704"
#
      imlg.execute(imessage_get)
      imessage_fetchall=imlg.fetchall()
      imessage_last=len(imessage_fetchall)
      if imessage_last == 0:
            return
      return imessage_fetchall

#################################
# Main Run
#################################
if __name__ == "__main__":
   import kSwitch
   import kha_dict_q
   import kLog
   import kGarage
   import kRouter
   kLog._kv_dbg=2
   kha=kha_dict_q.kha
   sw=kSwitch.Switch(kLog.log,kLog.dbg)
   sms=SMS_TALK()
   #sms.read(receive_account='kagepark1@gmail.com')
   #sms.read(sender_account='+18324193990',receive_account='kagepark1@gmail.com')
   #sms.read(sender_account='+18324193990')
   #sms.read(receive_account='kagepark1@gmail.com',find_text="could come")
   #sms.listen(receive_account='kagepark1@gmail.com',find_text="[k] ")

   # Monitoring Text
   receive_account='kagepark1@gmail.com'
   self_sender_ignore=True
   sender_account=None
   imessage_db="{0}/Library/Messages/chat.db".format(os.path.expanduser('~'))
   find_text='[k] '

   # read from running time
   initial_time=int(time.mktime(datetime(2001,1,1,0,0).timetuple())) - 3600
   monitored_time=(int(time.mktime(datetime.utcnow().timetuple())) - initial_time)*1000000000

   # Router
   router_api=kRouter.get_api(kha['kage park']['router']['id'],kha['kage park']['router']['pass'],kha['kage park']['router']['ip'])
   mac=kha['kage park']['phone']['mac']
   chk_count=0
   _help="""help)
[k] <name> <on|off|status>
  lamp1
  lamp2
  air_filter2
  air_filter3
  samsung_tv
[k] irobot clean
[k] garage <close|status>
[k] <tv_on|tv_off>
[k] <all_off|all_status>
"""


   old_src=''
   do_close=False
   while True:
       # Router
       if router_api:
          info=kRouter.find_mac(router_api,mac)
          if info is False:
              router_api=kRouter.get_api(kha['kage park']['router']['id'],kha['kage park']['router']['pass'],kha['kage park']['router']['ip'])
              kLog.dbg(3,"router api: {}".format(router_api))
          else:
              if chk_count > 5:
                  member='Out'
                  kha['devices']['locker']['garage']['interval']=240 # monitor 4min
                  delay_close=0
                  rc=None
                  if info:
                      kha['devices']['locker']['garage']['interval']=600 # monitor 10min
                      delay_close=3600
                      member='In'
                  src=kGarage.garage(kha['devices']['locker']['garage'],cmd='status',log=kLog.log,dbg=kLog.dbg)
                  if src == 'open':
                      do_close=True
                      if old_src != src: # send message
                          kSms.sms_send('2818252766','garage door opend') # reply back
                          kLog.dbg(3,"Garage changed status: {}".format(src))
                  elif src == 'closed':
                      do_close=False
                      if old_src!=src: # Send message
                          kSms.sms_send('2818252766','garage door closed') # reply back
                          kLog.dbg(3,"Garage changed status: {}".format(src))

                  if do_close:
                      #kha['devices']['locker']['garage']['interval']=1
                      rc=kGarage.garage(kha['devices']['locker']['garage'],cmd='close',log=kLog.log,dbg=kLog.dbg,delay_close=delay_close)
                      kLog.dbg(3,"Try close Garage monitor time:{}, status:{}, Try close:{}".format(kha['devices']['locker']['garage']['interval'],src,rc))
                      if rc:
                          kSms.sms_send('2818252766','Garage door closing') # reply back
                          do_close=False
                          chk_count=0
                          src='closed'
                  else:
                      chk_count=0
                  if src in ['closed','open']:
                      old_src='{}'.format(src)
                  kLog.dbg(3,"count:{}, monitor:{}, status_rc:{}, rc:{}, member:{}".format(chk_count,kha['devices']['locker']['garage']['interval'],src,rc,info))
              chk_count+=1

       imessage_fetchall=sms.read(receive_account=receive_account,sender_account=sender_account,self_sender_ignore=self_sender_ignore,imessage_db=imessage_db,read_time=monitored_time, find_text=find_text)
       if imessage_fetchall is None:
          time.sleep(5)
          continue
       # Message debugging
       for receive_msg in list(imessage_fetchall):
          if receive_account is not None:
             if receive_msg[0] == receive_account:
                continue
          qq=receive_msg[1].rsplit()
          qqn=len(qq)
          print("received '{0}'({2}) from '{1}'".format(qq,receive_msg[0],qqn))
          if qqn == 3 and qq[1] in ['garage'] and qq[2] in ['close','status']:
              status=kGarage.garage(kha['devices']['locker']['garage'],cmd='status',log=kLog.log,dbg=kLog.dbg,monitor=False)
              if qq[2] == 'close' and status == 'open':
                  kha['devices']['locker']['garage']['interval']=1
                  time.sleep(3)
                  rc=kGarage.garage(kha['devices']['locker']['garage'],cmd='close',log=kLog.log,dbg=kLog.dbg)
                  kSms.sms_send(receive_msg[0],'Garage Door : {}'.format(rc)) # reply back
              elif qq[2] == 'close' and status == 'closed':
                  kSms.sms_send(receive_msg[0],'Garage Door : alread closed')
              else:
                  kSms.sms_send(receive_msg[0],'Garage Door : {}'.format(status)) # reply back
          elif qqn == 3 and qq[1] in ['lamp1','lamp2','air_filter2','air_filter3'] and qq[2] in ['on','off','status']:
              group='switch'
              rc=sw.do_switch(kha['devices'],cmd=qq[2], dest=qq[1], group=group)
              kSms.sms_send(receive_msg[0],'{}'.format(rc)) # reply back
          elif qqn == 2 and qq[1] in ['tv_on','tv_off','all_off','all_status']:
              group='chain'
              rc=sw.do_switch(kha['devices'],dest=qq[1], group=group)
              if qq[1] == 'all_status':
                  rc=rc+'Garage Door : {}'.format(kGarage.garage(kha['devices']['locker']['garage'],cmd='status',log=kLog.log,dbg=kLog.dbg,monitor=False))
              kSms.sms_send(receive_msg[0],'{}'.format(rc)) # reply back
          elif qqn == 3 and qq[1] in ['samsung_tv','irobot'] and qq[2] in ['on','off','status','clean']:
              group='remocon'
              rc=sw.do_switch(kha['devices'],dest=qq[1],cmd=qq[2], group=group)
              kSms.sms_send(receive_msg[0],'{}'.format(rc)) # reply back
          else:
              kSms.sms_send(receive_msg[0],_help) # reply back

       imessage_last=len(imessage_fetchall)
       if imessage_last > 0:
           monitored_time=imessage_fetchall[imessage_last-1][2]
       else:
           monitored_time=0
       time.sleep(5)
