#!/usr/bin/env python
# This compflict with "pyaes" module
import sys, os, time
from datetime import datetime
import threading
import kBl as bl
import kWemo as wemo

def _log(log,msg):
   if log:
       log(msg)

def _dbg(dbg,level,msg):
   if dbg:
       dbg(level,msg)

class Switch:
   def __init__(self, log=None,dbg=None):
      self.cmd=['on','off','status','close','clean','learn','temp'] # Unknown command
      self.wemo=wemo.Wemo(log,dbg)
      self.log=log
      self.dbg=dbg
      self.br=bl.BroadLink(log,dbg)
      _log(self.log,'Module load : switch')

   def dev_name(self,name):
      if name.upper() in ['REMOCON','RM']:
          return 'rm'
      elif name.upper() == 'WEMO':
          return 'wemo'
      elif name.upper() in ['BROADLINK','BL']:
          return 'broadlink'
      elif name.upper() in ['GARAGE','MYQ']:
          return 'myq'

   def remocon_db(self,name,db={},dev='rm',ip=None,devid=None,dest=None,time=None,use=None,desc=None,signal=None):
      dev=self.dev_name(dev)
      if dev is None or name is None:
          return
      idb=db
      if 'remocon' in db:
          idb=db['remocon']
      if idb:
          if ip:
              idb['ip']=ip
          if devid:
              idb['devid']=devid
          if dest:
              idb['dest']=dest
          if time:
              idb['time']=time
          if use:
              idb['use']=use
          if desc:
              idb['desc']=desc
          if signal:
              idb['signal']=signal
          return db
      else:
          return {'remocon':{name:{'dev':dev,'ip':ip,'devid':devid,'dest':dest,'time':time,'use':use,'desc':desc,'signal':{}}}}

   def switch_db(self,name,db={},dev='bl',ip=None,devid=None,dest=None,time=None,use=None,desc=None):
      dev=self.dev_name(dev)
      if dev is None or name is None:
          return
      idb=db
      if 'switch' in db:
          idb=db['switch']
      if idb:
          if ip:
              idb['ip']=ip
          if devid:
              idb['devid']=devid
          if dest:
              idb['dest']=dest
          if time:
              idb['time']=time
          if use:
              idb['use']=use
          if desc:
              idb['desc']=desc
          return db
      else:
          return {'switch':{name:{'dev':dev,'ip':ip,'devid':devid,'dest':dest,'time':time,'use':use,'desc':desc}}}

   def chain_db(self,name,db={},order=[],switch_dest=[],switch_cmd=None,remocon_dest=[],remocon_cmd=None,chain_dest=[]):
      db={'chain':{name:{}}}
      iorder=[]
      if switch_dest:
          iorder=iorder+['switch']
          db['chain'][name].update({'switch':{'dest':switch_dest,'cmd':switch_cmd}})
      if remocon_dest:
          iorder=iorder+['remocon']
          db['chain'][name].update({'remocon':{'dest':remocon_dest,'cmd':remocon_cmd}})
      if chain_dest:
          iorder=iorder+['chain']
          db['chain'][name].update({'chain':{'dest':chain_dest}})
      if order:
          db['chain'][name]['order']=order
      else:
          db['chain'][name]['order']=iorder
      return db

   def locker_db(self,name,db={},dev='myq',user=None,passwd=None,ip=None,devid=None,dest=None,time=None,desc=None,autoclose=True,autoopen=False,nightstart=None,nightend=None,daywait=None,nightwait=None,interval=None,open_at=None,status_at=None,closed_at=None):
      dev=self.dev_name(dev)
      if dev is None or name is None:
          return
      if 'locker' in db:
          idb=db['locker']
      if idb:
          if user:
              idb['user']=user
          if passwd:
              idb['pass']=passwd
          if ip:
              idb['ip']=ip
          if devid:
              idb['devid']=devid
          if dest:
              idb['dest']=dest
          if time:
              idb['time']=time
          if desc:
              idb['desc']=desc
          if autoclose:
              idb['autoclose']=autoclose
          if autoopen:
              idb['autoopen']=autoopen
          if nightstart:
              idb['nightstart']=nightstart
          if nightend:
              idb['nightend']=nightend
          if daywait:
              idb['daywait']=daywait
          if nightwait:
              idb['nightwait']=nightwait
          if interval:
              idb['interval']=interval
          if open_at:
              idb['open_at']=open_at
          if status_at:
              idb['status_at']=status_at
          if closed_at:
              idb['closed_at']=closed_at
          return db
      else:
          return {'locker':{name:{'dev':dev,'id':user,'pass':passwd,'ip':ip,'devid':devid,'dest':dest,'time':time,'desc':desc,autoclose:True,autoopen:False,nightstart:'11:30pm',nightend:'06:30am',daywait:3600,nightwait:1200,interval:240,open_at:0,status_at:0,closed_at:0}}}

   def trigger(self,db=None,cmd=None,dest=None,rc_str=None):
      if db is None or dest is None or cmd is None or not 'dev' in db[dest]:
         return
      if db[dest]['dev'] == 'wemo':
         ww=self.wemo.do(db[dest]['ip'],cmd)
      elif db[dest]['dev'] == 'broadlink':
         ww=self.br.switch(dev_ipaddr=db[dest]['ip'],cmd=cmd)
      elif db[dest]['dev'] == 'rm':
         btn=None
         if cmd in db[dest]['signal']:
             btn=db[dest]['signal'][cmd]
         dev_ip=None
         if 'ip' in db[dest]:
             dev_ip=db[dest]['ip']
         dest_ip=None
         if 'dest' in db[dest]:
             dest_ip=db[dest]['dest']
         ww=self.br.remocon(button=btn,cmd=cmd,dest_ipaddr=dest_ip,dev_ipaddr=dev_ip)
         if ww is None:
             ww=True
      else:
         # other model
         _log(self.log,"unknown switch model or not found {0}".format(dest))
         return rc_str

      if cmd == 'status':
         _log(self.log,"switch {0} state : {1}".format(dest,ww))
         if rc_str is None:
            rc_str='switch {0} : {1}\n'.format(dest,ww)
         else:
            rc_str='switch {0} : {1}\n{2}'.format(dest,ww,rc_str)
      else:
         _dbg(self.dbg,3,"switch {0}({1}) result (ww) : {2}".format(dest,db[dest],ww))
         if ww is True:
            _log(self.log,'switch {0} {1}'.format(dest,cmd))
            if rc_str is None:
               rc_str='switch {0} : {1}\n'.format(dest,cmd)
            else:
               rc_str='switch {0} : {1}\n{2}'.format(dest,cmd,rc_str)
         elif ww is None:
            _log(self.log,'switch {0} already {1}'.format(dest,cmd))
            if rc_str is None:
               rc_str='switch {0} : already {1}\n'.format(dest,cmd)
            else:
               rc_str='switch {0} : already {1}\n{2}'.format(dest,cmd,rc_str)
         else:
            _log(self.log,'switch {0} {1} fail'.format(dest,cmd))
            if rc_str is None:
               rc_str='switch {0} : {1} fail\n'.format(dest,cmd)
            else:
               rc_str='switch {0} : {1} fail\n{2}'.format(dest,cmd,rc_str)
      return rc_str


   def do_switch(self,db,cmd=None, dest=None, group=None, ignores=None):
      if cmd and not cmd in self.cmd:
          return 'Unknown command'
      elif not group in db.keys() or not dest in db[group].keys():
          return 'Wrong group or dest'
      rc_str=''
      if group == 'chain':
          if 'order' in db[group][dest].keys():
              for sgrp in list(db[group][dest]['order']):
#                  print('sgrp:',sgrp, db[group][dest].keys())
                  find_delay=sgrp.split(':')
                  if find_delay[0] == 'delay':
                      _log(self.log,'do_switch() in switch.py: delay {0} min in chain rule for {1}'.format(find_delay[1],dest))
                      time.sleep(int(find_delay[1])*60)
                  elif sgrp in db[group][dest].keys():
                      if 'dest' in db[group][dest][sgrp].keys():
                          for adest in list(db[group][dest][sgrp]['dest']):
                              #print('adest:',adest)
                              if 'use' in db[sgrp][adest] and db[sgrp][adest]['use'] == 'off':
                                  continue
                              if 'cmd' in db[group][dest][sgrp].keys():
                                  sgrp_cmd=db[group][dest][sgrp]['cmd']
                                  if type(sgrp_cmd) == type([]):
                                      rc=self.trigger(db=db[sgrp],cmd=db[group][dest][sgrp]['cmd'][0],dest=adest)
#                                      rc_str=''.join([rc_str,rr])
                                  elif type(sgrp_cmd) is str:
                                      rc=self.trigger(db=db[sgrp],cmd=sgrp_cmd,dest=adest)
#                                      rc_str=''.join([rc_str,rr])
                              else:
                                  rc=self.trigger(db=db[sgrp],cmd=cmd,dest=adest)
#                                  rc_str=''.join([rc_str,rr])
                              if rc:
                                  rc_str=rc_str+rc
          else:
              for sgrp in db[group][dest].keys():
                  if 'dest' in db[group][dest][sgrp].keys():
                      for adest in list(db[group][dest][sgrp]['dest']):
                          if not sgrp in db:
                              _log(self.log,'switch: sgrp({0}) not found in db({0})'.format(sgrp,db.keys()))
                          elif not adest in db[sgrp]:
                              _log(self.log,'switch: ades({0}) not found in db[{1}]({2})'.format(adest,sgrp,db[sgrp].keys()))
                          else:
                              if 'use' in db[sgrp][adest] and db[sgrp][adest]['use'] == 'off':
                                  continue
                              rr=self.trigger(db=db[sgrp],cmd=cmd,dest=adest)
                          if rc:
                              rc_str=rc_str+rc
      else:
          if 'use' in db[group][dest] and db[group][dest]['use'] == 'off':
              return rc_str
          else:
              rc_str=self.trigger(db=db[group],cmd=cmd,dest=dest,rc_str=rc_str)
      return rc_str

#################################
# Main Run
#################################
if __name__ == "__main__":
   import kLog
   import kDict
   import os
   import kha_dict_q
   kha=kha_dict_q.kha
   kLog._kv_dbg=4
   sw=Switch(kLog.log,kLog.dbg)
   sw_db={'devices':{'switch':{}}}
   sw_db['devices']['switch'].update(sw.switch_db('lamp1',dev='WeMO',ip='192.168.7.83',desc='test lamp')['switch'])
   sw_db['devices'].update(sw.chain_db('all_off',switch_dest=['lamp1','lamp2','air_filter2','air_filter3'],switch_cmd='off',remocon_dest=['samsung_tv'],remocon_cmd='off',order=['remocon','switch']))
   print(sw.switch_db('lamp2',dev='bl',ip='192.168.7.87',desc='test lamp2'))
   sw_db['devices']['switch'].update(sw.switch_db('lamp2',dev='bl',ip='192.168.7.87',desc='test lamp2')['switch'])
   sw_db['devices'].update(sw.remocon_db('tv',ip=['192.168.7.89','192.168.7.90'],dest='192.168.7.91',desc='test tv'))
   import pprint
   pprint.pprint(sw_db)
   #print(sw.chain_db('all_off',switch_dest=['lamp1','lamp2','air_filter2','air_filter3'],switch_cmd='off',remocon_dest=['samsung_tv'],remocon_cmd='off',order=['remocon','switch']))
#   print(sw.switch_db('lamp1',dev='WeMO',ip='192.168.7.83',desc='test lamp'))
   
#   print sw.do_switch(kha['devices'],cmd='status', dest='lamp1', group='switch')
   #print sw.do_switch(kha['devices'],cmd='on', dest='lamp1', group='switch')
   #print sw.do_switch(kha['devices'],dest='tv_off', group='chain')
#   print sw.do_switch(kha['devices'],dest='tv_on', group='chain')
#   print sw.do_switch(kha['devices'],dest='samsung_tv',cmd='off', group='remocon')
#   print sw.do_switch(kha['devices'], dest='all_status', group='chain')
#   print sw.do_switch(kha['devices'],cmd='clean', dest='irobot',group='remocon')
#   print sw.do_switch(kha['devices'],cmd='off', dest='lamp', group='switch')
#   print sw.do_switch(kha['devices'],cmd='status', dest='lamp', group='switch')
 #  print sw.trigger(kha['devices']['switch'],cmd='status', dest='lamp')
