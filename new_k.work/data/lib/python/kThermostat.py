#!/usr/bin/env python
############################
# downloaded/system modules
############################
import sys, os, time
import sqlite3
from datetime import datetime


class Thermostat:
    def __init__(self,member_class=None, db_file=None,login_class=None,k_class=None,k_net=None,kw_class=None):
       self.stop=0
       self.login_class = login_class
       self.db_file=db_file
       self.k_class = k_class
       self.k_net = k_net
       self.hw=None
       self.thermostat_conn=None
       self.member_class=member_class
       self.auto_fan=0
       self.thstat=None
       self.kw_class=kw_class
       # New feature
       self.cmd_cmd={} # Input from other class
       self.cmd_state={} # output for other class
       self.cmd_notice={} # if need notice then give to notie class
       
    def put_ai(self,system=None,temp=None,house_member_num=None):
       #### Thermostat to logging
       if house_member_num is not None:
          conn2=sqlite3.connect(self.db_file,timeout=1)
          lg2=conn2.cursor()
          wwcs=self.kw_class.weather()
#          set_system text,set_temp text, cur_system text, cur_in_temp text, cur_out_temp text, cur_out_humidity text, cur_out_cloud text, member int
          if system is None or temp is None:
             lg2.execute("insert or replace into ai_ts (idx,cur_system,cur_in_temp,cur_out_temp,cur_out_humidity,cur_out_cloud,member) values ({0},'{1}','{2}','{3}','{4}','{5}',{6})".format(self.k_class.now(1,2),self.thstat['system'],self.thstat['in_temp'],wwcs['temp'],wwcs['humidity'],wwcs['cloud'],house_member_num))
          else:
             lg2.execute("insert or replace into ai_ts (idx,set_system,set_temp,cur_system,cur_in_temp,cur_out_temp,cur_out_humidity,cur_out_cloud,member) values ({0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}',{8})".format(self.k_class.now(1,2),system,temp,self.thstat['system'],self.thstat['in_temp'],wwcs['temp'],wwcs['humidity'],wwcs['cloud'],house_member_num))
          conn2.commit()


    def get_val(self,gname='thermostat',name=None,new_val=None):
       if name is None:
           return
       if new_val is not None:
           return new_val
       conn=sqlite3.connect(self.db_file,timeout=1)
       lg=conn.cursor()
       # Make a temperature values
       lg.execute("select varval from env where vargrp='{0}' and varname='{1}'".format(gname,name))
       val=lg.fetchone()
       if val is None:
          return 
       return val[0]
        
    def thermostat_out_autoset(self,run=0, inst=0, fan=None, status=None,temp=None):
       rc=True
       if self.hw is None or run == 0:
          return False
     
       if self.thermostat_conn is None:
           self.thermostat_conn=self.hw.get_login()
     
       while True:
          self.thstat=self.hw.do_set(r1=self.thermostat_conn,action="status")
          if self.thstat is None:
             time.sleep(120)
             self.thermostat_conn=self.hw.get_login()
          else:
             break
     
       if status is not None:
          return self.thstat

       # Make a temperature values
       sleep_time=self.get_val(gname='time',name='sleep')
       if sleep_time is not None:
          sleep_start=sleep_time.split(',')[0]
          sleep_end=sleep_time.split(',')[1]
       else:
          sleep_start="10:00PM"
          sleep_end="07:30AM"

#       if self.k_class.time_check("10:00PM","07:30AM") == 1: # Sleep time
       set_value={}
       if self.k_class.time_check(sleep_start,sleep_end) == 1: # Sleep time
          #set_value={'heat_out':65,'cool_out':82,'heat_in':78,'cool_in':79}
          heat=self.get_val(gname='thermostat',name='heat_out_sleep',new_val=temp)
          if heat is not None:
             set_value['heat_out']=int(heat)
          else:
             set_value['heat_out']=65
          heat=self.get_val(gname='thermostat',name='heat_in_sleep',new_val=temp)
          if heat is not None:
             set_value['heat_in']=int(heat)
          else:
             set_value['heat_in']=78
          heat=self.get_val(gname='thermostat',name='cool_out_sleep',new_val=temp)
          if heat is not None:
             set_value['cool_out']=int(heat)
          else:
             set_value['cool_out']=82
          heat=self.get_val(gname='thermostat',name='cool_in_sleep',new_val=temp)
          if heat is not None:
             set_value['cool_in']=int(heat)
          else:
             set_value['cool_in']=79
       else: # day time
          #set_value={'heat_out':65,'cool_out':82,'heat_in':77,'cool_in':78}
          heat=self.get_val(gname='thermostat',name='heat_out_day',new_val=temp)
          if heat is not None:
             set_value['heat_out']=int(heat)
          else:
             set_value['heat_out']=65
          heat=self.get_val(gname='thermostat',name='heat_in_day',new_val=temp)
          if heat is not None:
             set_value['heat_in']=int(heat)
          else:
             set_value['heat_in']=78
          heat=self.get_val(gname='thermostat',name='cool_out_day',new_val=temp)
          if heat is not None:
             set_value['cool_out']=int(heat)
          else:
             set_value['cool_out']=82
          heat=self.get_val(gname='thermostat',name='cool_in_day',new_val=temp)
          if heat is not None:
             set_value['cool_in']=int(heat)
          else:
             set_value['cool_in']=79

     
       self.k_class.dbg(3,"current thermostat(in) : {0}:{1}F (SP[H{2}F,C{3}F] SV[H{4}F,C{5}F]), inst={6}, fan_state={7}, fan={8}, status={9}, temp={10}".format(self.thstat['system'],self.thstat['in_temp'],self.thstat['heat'],self.thstat['cool'],set_value['heat_in'],set_value['cool_in'],inst,self.thstat['stat_fan'],fan,status,temp))
       self.k_class.dbg(3,"set_value : {0}".format(set_value))

     
       if fan is None:
          if self.thstat['stat_fan'] == 1: # if on then make to auto
             rc=self.hw.do_set(r1=self.thermostat_conn,action='fan',value=0)
       elif fan == 1:
          if self.thstat['stat_fan'] == 0:
             rc=self.hw.do_set(r1=self.thermostat_conn,action='fan',value=1)
             return rc
       elif fan == 0:
          if self.thstat['stat_fan'] == 1:
             rc=self.hw.do_set(r1=self.thermostat_conn,action='fan',value=0)
             return rc
     
       if self.thstat['system'] == 'heat': # heat
          if inst == 1: # set to heat_out
             if int(self.thstat[self.thstat['system']]) == int(set_value['heat_out']) : # set heat_out
                self.k_class.dbg(3,"ignore set to {0}:{1}F(same temperature)".format(self.thstat['system'],set_value['heat_out']))
                self.put_ai(house_member_num=0,system=self.thstat['system'],temp=set_value['heat_out'])
             else:
                rc=self.hw.do_set(r1=self.thermostat_conn,action=self.thstat['system'],value=set_value['heat_out'])
                self.k_class.dbg(3,"Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],self.thstat['system'],set_value['heat_out']))
                self.put_ai(house_member_num=0,system=self.thstat['system'],temp=set_value['heat_out'])
          else:
             if self.thstat['in_temp'] > set_value['cool_out'] and self.thstat['in_temp'] > set_value['heat_out']: # too hot. so set to cool_out
                if int(self.thstat['running']) == 0:
                    self.k_class.dbg(3,"Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],"cool",set_value['cool_out']))
                    rc=self.hw.do_set(r1=self.thermostat_conn,action="cool",value=set_value['cool_out'])
                    self.put_ai(house_member_num=0,system="cool",temp=set_value['cool_out'])
                else:
                    self.k_class.dbg(3,"Wait for set to {2}:{3}F, currently running device".format("cool",set_value['cool_out']))
                    rc=5
             elif self.thstat['in_temp'] < set_value['heat_out']: # set to heat_out
                self.k_class.dbg(3,"Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],self.thstat['system'],set_value['heat_out']))
                rc=self.hw.do_set(r1=self.thermostat_conn,action=self.thstat['system'],value=set_value['heat_out'])
                self.put_ai(house_member_num=0,system=self.thstat['system'],temp=set_value['heat_out'])
       else:  # Cool
          if inst == 1:
             if int(self.thstat[self.thstat['system']]) == int(set_value['cool_out']) : # set cool_out
                self.k_class.dbg(3,"ignore set to {0}:{1}F(same temperature)".format(self.thstat['system'],set_value['cool_out']))
                self.put_ai(house_member_num=0,system=self.thstat['system'],temp=set_value['cool_out'])
             else:
                rc=self.hw.do_set(r1=self.thermostat_conn,action=self.thstat['system'],value=set_value['cool_out'])
                self.k_class.dbg(3,"Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],self.thstat['system'],set_value['cool_out']))
                self.put_ai(house_member_num=0,system=self.thstat['system'],temp=set_value['cool_out'])
          else:
             if self.thstat['in_temp'] > set_value['cool_out']: #set cool_out
                self.k_class.dbg(3,"Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],self.thstat['system'],set_value['cool_out']))
                rc=self.hw.do_set(r1=self.thermostat_conn,action=self.thstat['system'],value=set_value['cool_out'])
                self.put_ai(house_member_num=0,system=self.thstat['system'],temp=set_value['cool_out'])
             elif self.thstat['in_temp'] < set_value['heat_out'] and self.thstat['in_temp'] < set_value['cool_out']: # Too cold. so set to heat_out
                if int(self.thstat['running']) == 0:
                   self.k_class.dbg(3,"Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],"heat",set_value['heat_out']))
                   rc=self.hw.do_set(r1=self.thermostat_conn,action="heat",value=set_value['heat_out'])
                   self.put_ai(house_member_num=0,system="heat",temp=set_value['heat_out'])
                else:
                    self.k_class.dbg(3,"Wait for set to {2}:{3}F, currently running device".format("heat",set_value['heat_out']))
                    rc=5
     
       return rc
     
    def thermostat_in_autoset(self, run=0, inst=0, fan=None, status=None, temp=None):
       rc=True
     
       if self.hw is None or run == 0:
           return False
     
       if self.thermostat_conn is None:
           self.thermostat_conn=self.hw.get_login()
     
       while True:
          self.thstat=self.hw.do_set(r1=self.thermostat_conn,action="status")
          if self.thstat is None:
             time.sleep(120)
             self.thermostat_conn=self.hw.get_login()
          else:
             break
     
       if status is not None:
          return self.thstat
     
       # Make a temperature values
       sleep_time=self.get_val(gname='time',name='sleep')
       if sleep_time is not None:
          sleep_start=sleep_time.split(',')[0]
          sleep_end=sleep_time.split(',')[1]
       else:
          sleep_start="10:00PM"
          sleep_end="07:30AM"

#       if self.k_class.time_check("10:00PM","07:30AM") == 1: # Sleep time
       set_value={}
       if self.k_class.time_check(sleep_start,sleep_end) == 1: # Sleep time
          #set_value={'heat_out':65,'cool_out':82,'heat_in':78,'cool_in':79}
          heat=self.get_val(gname='thermostat',name='heat_out_sleep',new_val=temp)
          if heat is not None:
             set_value['heat_out']=int(heat)
          else:
             set_value['heat_out']=65
          heat=self.get_val(gname='thermostat',name='heat_in_sleep',new_val=temp)
          if heat is not None:
             set_value['heat_in']=int(heat)
          else:
             set_value['heat_in']=78
          heat=self.get_val(gname='thermostat',name='cool_out_sleep',new_val=temp)
          if heat is not None:
             set_value['cool_out']=int(heat)
          else:
             set_value['cool_out']=82
          heat=self.get_val(gname='thermostat',name='cool_in_sleep',new_val=temp)
          if heat is not None:
             set_value['cool_in']=int(heat)
          else:
             set_value['cool_in']=79

       else: # day time
          #set_value={'heat_out':65,'cool_out':82,'heat_in':77,'cool_in':78}
          heat=self.get_val(gname='thermostat',name='heat_out_day',new_val=temp)
          if heat is not None:
             set_value['heat_out']=int(heat)
          else:
             set_value['heat_out']=65
          heat=self.get_val(gname='thermostat',name='heat_in_day',new_val=temp)
          if heat is not None:
             set_value['heat_in']=int(heat)
          else:
             set_value['heat_in']=78
          heat=self.get_val(gname='thermostat',name='cool_out_day',new_val=temp)
          if heat is not None:
             set_value['cool_out']=int(heat)
          else:
             set_value['cool_out']=82
          heat=self.get_val(gname='thermostat',name='cool_in_day',new_val=temp)
          if heat is not None:
             set_value['cool_in']=int(heat)
          else:
             set_value['cool_in']=78


       self.k_class.dbg(3,"current thermostat(in) : {0}:{1}F (SP[H{2}F,C{3}F] SV[H{4}F,C{5}F]), inst={6}, fan_state={7}, fan={8}, status={9}, temp={10}".format(self.thstat['system'],self.thstat['in_temp'],self.thstat['heat'],self.thstat['cool'],set_value['heat_in'],set_value['cool_in'],inst,self.thstat['stat_fan'],fan,status,temp))
       self.k_class.dbg(3,"set_value : {0}".format(set_value))

       if fan is None:
          if self.thstat['stat_fan'] == 1: # if on then make to auto
             rc=self.hw.do_set(r1=self.thermostat_conn,action='fan',value=0)
       elif fan == 1:
          if self.thstat['stat_fan'] == 0:
             rc=self.hw.do_set(r1=self.thermostat_conn,action='fan',value=1)
             return rc
       elif fan == 0:
          if self.thstat['stat_fan'] == 1:
             rc=self.hw.do_set(r1=self.thermostat_conn,action='fan',value=0)
             return rc
     
       if self.thstat['system'] == 'heat': #heat
          if inst == 1:
             if self.thstat['in_temp'] > (set_value['heat_in']+1):
                 self.k_class.dbg(3,"Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],"cool",set_value['heat_in']))
                 rc=self.hw.do_set(r1=self.thermostat_conn,action="cool",value=set_value['heat_in'])
                 self.put_ai(house_member_num=1,system="cool",temp=set_value['heat_in'])
             elif self.thstat['in_temp'] < set_value['cool_in']:
                 self.k_class.dbg(3,"Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],"heat",set_value['heat_in']))
                 rc=self.hw.do_set(r1=self.thermostat_conn,action=self.thstat['system'],value=set_value['heat_in'])
                 self.put_ai(house_member_num=1,system=self.thstat['system'],temp=set_value['heat_in'])
             elif int(self.thstat[self.thstat['system']]) == int(set_value['heat_in']) :
                 self.k_class.dbg(3,"ignore set to {0}:{1}F (same temperature)".format(self.thstat['system'],set_value['heat_in']))
                 self.put_ai(house_member_num=0,system=self.thstat['system'],temp=set_value['heat_in'])
             else:
                 self.k_class.dbg(3,"Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],self.thstat['system'],set_value['heat_in']))
                 rc=self.hw.do_set(r1=self.thermostat_conn,action=self.thstat['system'],value=set_value['heat_in'])
                 self.put_ai(house_member_num=1,system=self.thstat['system'],temp=set_value['heat_in'])
          else:
             if self.thstat['in_temp'] > set_value['cool_in'] and self.thstat['in_temp'] > set_value['heat_in']:
                if int(self.thstat['running']) == 0:
                   self.k_class.dbg(3,"(A)Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],"cool",set_value['cool_in']))
                   rc=self.hw.do_set(r1=self.thermostat_conn,action="cool",value=set_value['cool_in'])
                   self.put_ai(house_member_num=1,system="cool",temp=set_value['cool_in'])
                else:
                   self.k_class.dbg(3,"Wait for set to {2}:{3}F, currently running device".format("cool",set_value['cool_in']))
                   rc=5
             elif self.thstat['in_temp'] < set_value['heat_in']:
                if self.thstat['heat'] < self.thstat['in_temp'] and (set_value['heat_in'] + 3) > self.thstat['heat'] and (set_value['heat_in'] -1 ) < self.thstat['heat']:
                    self.k_class.dbg(3,"ignore set to {0}:{1}F (available temperature range)".format(self.thstat['system'],set_value['heat_in']))
                    return
                if set_value['cool_in'] == self.thstat[self.thstat['system']] : # thermostat value and setting point is same then ignore
                    self.k_class.dbg(3,"ignore set to {0}:{1}F (same temperature)".format(self.thstat['system'],set_value['heat_in']))
                    self.put_ai(house_member_num=0,system=self.thstat['system'],temp=set_value['heat_in'])
                    return
                rc=self.hw.do_set(r1=self.thermostat_conn,action=self.thstat['system'],value=set_value['heat_in'])
                self.k_class.dbg(3,"(B)Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],self.thstat['system'],set_value['heat_in']))
                self.put_ai(house_member_num=1,system=self.thstat['system'],temp=set_value['heat_in'])
       else:  # Cool
          if inst == 1:
             if self.thstat['in_temp'] > set_value['cool_in']:
                 self.k_class.dbg(3,"Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],"cool",set_value['cool_in']))
                 rc=self.hw.do_set(r1=self.thermostat_conn,action="cool",value=set_value['cool_in'])
                 self.put_ai(house_member_num=1,system="cool",temp=set_value['cool_in'])
             elif self.thstat['in_temp'] < (set_value['heat_in'] -1):
                 self.k_class.dbg(3,"Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],"heat",set_value['cool_in']))
                 rc=self.hw.do_set(r1=self.thermostat_conn,action="heat",value=set_value['cool_in'])
                 self.put_ai(house_member_num=1,system="heat",temp=set_value['cool_in'])
             elif int(self.thstat[self.thstat['system']]) == int(set_value['cool_in']):
                 self.k_class.dbg(3,"ignore set to {0}:{1}F (same temperature)".format(self.thstat['system'],set_value['cool_in']))
                 self.put_ai(house_member_num=0,system=self.thstat['system'],temp=set_value['cool_in'])
             else:
                 rc=self.hw.do_set(r1=self.thermostat_conn,action=self.thstat['system'],value=set_value['cool_in'])
                 self.k_class.dbg(3,"Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],self.thstat['system'],set_value['cool_in']))
                 self.put_ai(house_member_num=1,system=self.thstat['system'],temp=set_value['cool_in'])
          else:
             if self.thstat['in_temp'] < set_value['cool_in'] and self.thstat['in_temp'] < set_value['heat_in']:
                if int(self.thstat['running']) == 0:
                   self.k_class.dbg(3,"(C)Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],"heat",set_value['heat_in']))
                   rc=self.hw.do_set(r1=self.thermostat_conn,action="heat",value=set_value['heat_in'])
                   self.put_ai(house_member_num=1,system="heat",temp=set_value['heat_in'])
                else:
                   self.k_class.dbg(3,"Wait for set to {2}:{3}F, currently running device".format("heat",set_value['heat_in']))
                   rc=5
             elif self.thstat['in_temp'] > set_value['cool_in']:
                if self.thstat['cool'] >= self.thstat['in_temp'] and (set_value['cool_in'] + 1) >= self.thstat['cool'] and (set_value['cool_in'] -3 ) <= self.thstat['cool']:
                    self.k_class.dbg(3,"ignore set to {0}:{1}F (available temperature range)".format(self.thstat['system'],set_value['cool_in']))
                    return
                if set_value['cool_in'] == self.thstat[self.thstat['system']] : # thermostat value and setting point is same then ignore
                    self.k_class.dbg(3,"ignore set to {0}:{1}F (same temperature)".format(self.thstat['system'],set_value['cool_in']))
                    self.put_ai(house_member_num=0,system=self.thstat['system'],temp=set_value['cool_in'])
                    return
                rc=self.hw.do_set(r1=self.thermostat_conn,action="cool",value=set_value['cool_in'])
                self.k_class.dbg(3,"(D)Set from {0}:{1}F to {2}:{3}F".format(self.thstat['system'],self.thstat[self.thstat['system']],"cool",set_value['cool_in']))
                self.put_ai(house_member_num=1,system="cool",temp=set_value['cool_in'])
     
       return rc
     
    def monitor(self,run=1):
        if run == 0:
            return
        old_stat=None
        th_mon_time=0
        moving=0
        fan_cnt=0
        th_err=1
        old_th_sleep_triger=0
     
        self.k_class.log("Module load : thermostat")
        #self.thstat['in_temp'] # indoor temperature
        #self.thstat['cool']  # cool set point
        #self.thstat['heat']  # heat set point
        #self.thstat['system']# system switch
     
        while True:
           if self.k_net.check_network() is False:
               time.sleep(60)
               continue
      
           if self.stop == 1:
               time.sleep(10)
               continue

           if self.member_class == 'test':
              house_member_num=1
           else:
              house_member_num=self.member_class.house_member(3200) # checkup before 2mil distance
              if house_member_num is None:
                 time.sleep(20)
                 continue
     
           if th_err == 1:
              self.hw=self.login_class.check(hwtype='honeywell')
              if self.hw is None:
                  time.sleep(60)
                  continue
              self.thermostat_conn=self.hw.get_login()
              self.thstat=self.hw.do_set(r1=self.thermostat_conn,action="status")
              if self.thstat is None:
                  self.k_class.log("Get error for initial status of thermostat. Please checkup user id and password")
                  th_err=1
                  time.sleep(60)
                  continue
              else:
                  self.k_class.log("current thermostat : {0} {1}F (Set point: C {2}F, H {3}F)".format(self.thstat['system'],self.thstat['in_temp'],self.thstat['cool'],self.thstat['heat']))
                  th_err=0
                  self.put_ai(house_member_num=house_member_num)
     
           th_now=self.k_class.now(1,2)
           if old_stat > 0 and house_member_num > 0:
              # in auto set ( monitoring 30 min period ) for inside members
              if int(th_now - th_mon_time) > 1800:
                 if fan_cnt > 3 and self.auto_fan == 1:
                     new_conn=self.thermostat_in_autoset(run=1, inst=0, fan=1)
                     self.k_class.log("Thermostat : Set Fan to on")
                     time.sleep(600) # Turn on fan to 10min
                     new_conn=self.thermostat_in_autoset(run=1, inst=0, fan=0)
                     self.k_class.log("Thermostat : Set Fan to auto")
                     fan_cnt=0
                 else:
                    sleep_time=self.get_val(gname='time',name='sleep')
                    if sleep_time is not None:
                       sleep_start=sleep_time.split(',')[0]
                       sleep_end=sleep_time.split(',')[1]
                    else:
                       sleep_start="10:00PM"
                       sleep_end="07:30AM"
                    th_sleep_triger=self.k_class.time_check(sleep_start,sleep_end)
                    if ( th_sleep_triger==1 or th_sleep_triger==0 ) and old_th_sleep_triger != th_sleep_triger:
                       new_conn=self.thermostat_in_autoset(run=1, inst=1)
                    else:
                       new_conn=self.thermostat_in_autoset(run=1, inst=0)

                    if self.auto_fan == 1:
                         fan_cnt=fan_cnt+1
     
                 if new_conn is True:
                    th_mon_time=self.k_class.now(1,2)
                    old_th_sleep_triger=th_sleep_triger
                    moving=0
           elif house_member_num == 0 and old_stat == 0:
              # out auto set ( monitoring 60 min period ) for out side members
              #if int(self.k_class.now(1) - th_mon_time) > 3600:
              if int(th_now - th_mon_time) > 3600:
                 if fan_cnt > 3 and self.auto_fan == 1:
                    new_conn=self.thermostat_out_autoset(run=1, inst=0, fan=1)
                    self.k_class.log("Thermostat : Set Fan to on")
                    time.sleep(600) # Turn on fan to 10min
                    new_conn=self.thermostat_out_autoset(run=1, inst=0, fan=0)
                    self.k_class.log("Thermostat : Set Fan to auto")
                    fan_cnt=0
                 else:
                    new_conn=self.thermostat_out_autoset(run=1, inst=0)
                    if self.auto_fan == 1:
                       fan_cnt=fan_cnt+1
                 if new_conn is True:
                    moving=0
                    th_mon_time=self.k_class.now(1,2)
                 elif new_conn == 5:#running device then wait 5min
                    time.sleep(300)
           else: #Changed state then set onetime
              fan_cnt=0
              if house_member_num == 0 and old_stat != 0: # out side
                 if moving == 1 and int(self.k_class.now(1,2) - th_mon_time) < 1200: 
                     # Looks moving. So, just wait until 20min. no more moving 20min then turn off
                     time.sleep(60)
                     continue
                 self.k_class.dbg(3,"detected all members out : {0} members (old_stat={0},moving={1})".format(house_member_num,old_stat,moving))
                 new_conn=self.thermostat_out_autoset(run=1, inst=1)
                 if new_conn is True:
                    old_stat=0
                    moving=0
                    th_mon_time=self.k_class.now(1,2)
     
              elif house_member_num == 200 and old_stat == 0:  # Somebody moving to home
                 self.k_class.dbg(3,"detected somebody moving to home (old_stat={0},moving={1})".format(old_stat,moving))
                 new_conn=self.thermostat_in_autoset(run=1, inst=1)
                 if new_conn is True:
                    old_stat=1
                    moving=1
                    th_mon_time=self.k_class.now(1,2)
     
              elif house_member_num > 0 and ( old_stat == 0 or old_stat is None ) :  # Somebody in
                 self.k_class.dbg(3,"detected somebody in :{0} members  (old_stat={1},moving={2})".format(house_member_num,old_stat,moving))
                 new_conn=self.thermostat_in_autoset(run=1, inst=1)
                 if new_conn is True:
                    old_stat=1
                    moving=0
                    th_mon_time=self.k_class.now(1,2)
     
           time.sleep(26)
          
#################################
# Main Run
#################################
if __name__ == "__main__":
   import cep
   import member
   import login
   db_file=os.path.dirname(os.path.abspath(__file__))+'/../.KHAS.sql'
   k=cep.CEP(test=1,_kv_dbg=4)
   k_net=cep.NET()
   login=login.LOGIN(k_class=k,k_net=k_net,db_file=db_file)
   mem=member.Member(db_file=db_file,login_class=login,k_class=k,k_net=k_net)
   th=Thermostat(member_class=mem,k_class=k,k_net=k_net,db_file=db_file,login_class=login)
   th.monitor()
