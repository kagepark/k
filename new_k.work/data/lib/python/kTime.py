#Kage

_k_version='1.2.15'

from datetime import datetime
from datetime import timedelta

def now():
    return datetime.now()

def strptime(data,form):
    return datetime.strptime(data,form)

def ptime(**data):
    '''
    ** Print format time
    rf=xxxx : read format (default:auto detect) (ex: %Y-%m-%d %H:%M:%S, %Y-%m-%d %I:%M:%S%p)
    <add|del>=[<##>N1,<##>N2,...] or <##>N  # Add or del <##>y(year),w(week),d(day),h(hour),m(min),s(sec)
    p=<format> : print format(yy(year),mm(mon),dd(day),hh(hour),m(min),ss(sec),ts(total sec),ww(week number),wd(weekday),wn(weekday name),apm(am/pm), %Y-%m-%d %H:%M:%S(default format))
    default return is TIME
    '''
    _print_format='%Y-%m-%d %H:%M:%S'
    _read_format=None
    _time=datetime.now()
    if len(data) > 0:
        keys=data.keys()
        if 'pf' in keys: # print form
            _print_format=data['pf']
        elif 'format' in keys: # print form
            _print_format=data['format']

        if 'rf' in keys: # Read form
            _read_format=data['rf']

        if 't' in keys: # input time
            if _read_format is None:
               time_date_format=None
               time_time=None
               time_apm=None
               if type(data['t']) is list:
                   input_date=data['t'][0]
               else:
                   input_date=data['t']
               if type(input_date) is not str:
                   return False
               #time_str_arr=data['t'].split(' ')
               time_str_arr=input_date.split(' ')

               # 1 or 2 value
               #date
               s_time_str_arr_1=time_str_arr[0].split('/')
               d_time_str_arr_1=time_str_arr[0].split('-')
               t_time_str_arr_1=time_str_arr[0].split(':')
               if len(s_time_str_arr_1) > 1:
                   if len(s_time_str_arr_1) == 3:
                       if len(s_time_str_arr[0]) == 4:
                           time_date_format='%Y/%m/%d'
                       else:
                           time_date_format='%m/%d/%y'
                   elif len(s_time_str_arr_1) == 2:
                           time_date_format='%m/%d'
               elif len(d_time_str_arr_1) > 1:
                   if len(d_time_str_arr_1) == 3:
                       if len(d_time_str_arr_1[0]) == 4:
                           time_date_format='%Y-%m-%d'
                       else:
                           time_date_format='%m-%d-%y'
                   elif len(d_time_str_arr_1) == 2:
                       time_date_format='%m-%d'
               elif len(t_time_str_arr_1) > 1:
               #time
                   if len(t_time_str_arr_1) == 3:
                       if len(time_str_arr) == 2:
                           _read_format='%I:%M:%S %p'
                       elif len(time_str_arr) == 1:
                           if time_str_arr[0][-1] == 'm' or time_str_arr[0][-1] == 'M':
                               _read_format='%I:%M:%S%p'
                           else:
                               _read_format='%H:%M:%S'
                   elif len(t_time_str_arr_1) == 2:
                       if len(time_str_arr) == 2:
                           _read_format='%I:%M %p'
                       elif len(time_str_arr) == 1:
                           if time_str_arr[0][-1] == 'm' or time_str_arr[0][-1] == 'M':
                               _read_format='%I:%M%p'
                           else:
                               _read_format='%H:%M'

               # 2 or 3 value
               #time
               if time_date_format is not None and _read_format is None and len(time_str_arr) > 1:
                   t_time_str_arr_2=time_str_arr[1].split(':')
                   if len(t_time_str_arr_2) > 1:
                       if len(t_time_str_arr_2) == 3:
                           if len(time_str_arr) == 3:
                               if time_str_arr[2][-1] == 'm' or time_str_arr[2][-1] == 'M':
                                   _read_format='{0} %I:%M:%S %p'.format(time_date_format)
                           else:
                               if time_str_arr[1][-1] == 'm' or time_str_arr[1][-1] == 'M':
                                   _read_format='{0} %I:%M:%S%p'.format(time_date_format)
                               else:
                                   _read_format='{0} %H:%M:%S'.format(time_date_format)
                       elif len(t_time_str_arr_2) == 2:
                           if len(time_str_arr) == 3:
                               if time_str_arr[2][-1] == 'm' or time_str_arr[2][-1] == 'M':
                                   _read_format='{0} %I:%M %p'.format(time_date_format)
                           else:
                               if time_str_arr[1][-1] == 'm' or time_str_arr[1][-1] == 'M':
                                   _read_format='{0} %I:%M%p'.format(time_date_format)
                               else:
                                   _read_format='{0} %H:%M'.format(time_date_format)

               if _read_format is None:
                   print("can't find format for {0}. please give time format".format(data['t']))
                   return False
               _time=datetime.strptime(data['t'],_read_format)

        def get_timedelta(key='add'):
            _year=0
            _week=0
            _day=0
            _hour=0
            _min=0
            _sec=0
            if not key in data:
                return (_day,_hour,_min,_sec)

            if type(data[key]) is list:
                for _add in list(data[key]):
                    _add_symbol=_add[-1]
                    _new_add=_add[:-1]
                    if _add_symbol == 'y':
                        _year=int(_new_add)
                    elif _add_symbol == 'w':
                        _week=int(_new_add)
                    elif _add_symbol == 'd':
                        _day=int(_new_add)
                    elif _add_symbol == 'h':
                        _hour=int(_new_add)
                    elif _add_symbol == 'm':
                        _min=int(_new_add)
                    elif _add_symbol == 's':
                        _sec=int(_new_add)
            else:
                keytype = type(data[key])
                if keytype is int:
                    _min=data[key]
                elif keytype is str:
                    _add_symbol=data[key][-1]
                    _new_add=data[key][:-1]
                    if _add_symbol == 'y':
                        _year=int(_new_add)
                    elif _add_symbol == 'w':
                        _week=int(_new_add)
                    elif _add_symbol == 'd':
                        _day=int(_new_add)
                    elif _add_symbol == 'h':
                        _hour=int(_new_add)
                    elif _add_symbol == 'm':
                        _min=int(_new_add)
                    elif _add_symbol == 's':
                        _sec=int(_new_add)

            if _year > 0:
                _day=_day+(_year * 365)
            if _week > 0:
                _day=_day+(_week * 7)
            return (_day,_hour,_min,_sec)

        if 'add' in keys: # Add time
            if data['add'] == 'today':
                dn=datetime.now()
                _time=_time.replace(year=dn.year,month=dn.month,day=dn.day)
            else:
                _day,_hour,_min,_sec=get_timedelta(key='add')
                _time = _time + timedelta(days=_day,hours=_hour,minutes=_min,seconds=_sec)
        elif 'del' in keys: # delete time
            _day,_hour,_min,_sec=get_timedelta(key='del')
            _time = _time - timedelta(days=_day,hours=_hour,minutes=_min,seconds=_sec)

        if 'p' in keys: # print short value
            _print=data['p']
            if _print == 'day' or _print == 'dd':
                return _time.day
            elif _print == 'year' or _print == 'yy':
                return _time.year
            elif _print == 'month' or _print == 'mon' and _print == 'mm':
                return _time.month
            elif _print == 'hour' or _print == 'hh':
                return _time.hour
            elif _print == 'minute' or _print == 'min' or _print == 'm':
                return _time.minute
            elif _print == 'second' or _print == 'sec' or _print == 'ss':
                return _time.minutes
            elif _print == 'week_number' or _print == 'ww':
                return _time.isocalendar()[1]
            elif _print == 'weekday' or _print == 'wd':
                return _time.weekday()
            elif _print == 'weekday_name' or _print == 'wn':
                return _time.strftime('%a')
            elif _print == 'apm':
                return _time.strftime('%p')
            elif _print == 'ts' or _print == 'total_sec':
                return _time.strftime('%s')
            elif _print == 'now':
                return datetime.now().strftime('%s')
            elif _print == 'today':
                return datetime.now().date()
            elif _print == 'today2':
                return datetime.now().strftime('%Y/%m/%d')
            else:
                if len(data['p'].split('%')) > 1:
                    return _time.strftime(data['p'])# print format
                else:
                    return _time.strftime(_print_format)# print format

    return _time # return to time

def check_date(**data):
    year=None
    month=None
    day=None

    def compare(a,b,c):
        if type(a) is str:
            a=int(a)
        if type(b) is str:
            a=int(b)
        if c == 'lt':
            if a < b:
                return True
        elif c == 'le':
            if a <= b:
                return True
        elif c == 'gt':
            if a > b:
                return True
        elif c == 'ge':
            if a >= b:
                return True
        else:
            if a == b:
                return True
        return False

    if 'd' in data:
        try:
            idate=ast.literal_eval(data['d'])
        except:
            idate=data['d']
        d_type=type(idate)
        if d_type is str:
            if 'df' in data:
                date_date=datetime.strptime(data['d'],data['df'])
                if date_date.year == 1900:
                    return {'year':None,'month':date_date.month,'day':date_date.day}
                else:
                    return {'year':date_date.year,'month':date_date.month,'day':date_date.day}
            else:
                ssplit=data['d'].split('/')
                if len(ssplit) == 1:
                    ssplit=data['d'].split('-')
                if len(ssplit) == 3:
                    try:
                        year=ast.literal_eval(ssplit[0])
                    except:
                        year=ssplit[0]
                    try:
                        month=ast.literal_eval(ssplit[1])
                    except:
                        month=ssplit[1]
                    try:
                        day=ast.literal_eval(ssplit[2])
                    except:
                        day=ssplit[2]
                elif len(ssplit) == 2:
                    try:
                        month=ast.literal_eval(ssplit[0])
                    except:
                        month=ssplit[1]
                    try:
                        day=ast.literal_eval(ssplit[1])
                    except:
                        day=ssplit[1]
                elif len(ssplit) == 1:
                    try:
                        day=ast.literal_eval(ssplit[0])
                    except:
                        day=ssplit[0]
                return {'year':year,'month':month,'day':day}
        elif d_type == type([]):
            tmp=[]
            #for ii in list(data['d']):
            for ii in list(idate):
                tmp.append(check_date(d=ii))
            return tmp

    if ('dd' in data or 'day' in data) and ('mm' in data or 'month' in data) and ('yy' in data or 'year' in data):
        if 'dd' in data:
            day=data['dd']
        else:
            day=data['day']
        if 'mm' in data:
            month=data['mm']
        else:
            month=data['month']
        if 'yy' in data:
            year=data['yy']
        else:
            year=data['year']
        return compare(datetime.now().date(),datetime.strptime('{0}-{1}-{2}'.format(year,month,day),'%Y-%m-%d').date(),data['compare'])
    if ('dd' in data or 'day' in data) and ('mm' in data or 'month' in data):
        if 'dd' in data:
            day=data['dd']
        else:
            day=data['day']
        if 'mm' in data:
            month=data['mm']
        else:
            month=data['month']
        year=datetime.now().year
        return compare(datetime.now().date(),datetime.strptime('{0}-{1}-{2}'.format(year,month,day),'%Y-%m-%d').date(),data['compare'])

    if 'yy' in data or 'year' in data:
        now_year=datetime.now().year
        if 'yy' in data:
            year=data['yy']
        else:
            year=data['year']

        if year is None:
            return True
        if type(year) is str:
                try:
                    year=ast.literal_eval(year)
                except:
                    pass
        if type(year) is str:
            year_arr=year.split('-')
            if len(year_arr) == 1:
                if 'compare' in data:
                    return compare(now_year,year,data['compare'])
                else:
                    if now_year == int(year):
                        return True
            elif len(year_arr) == 2:
                if int(year_arr[0]) <= now_year and now_year <= int(year_arr[1]):
                    return True
        elif type(year) is int:
            if 'compare' in data:
                return compare(now_year,year,data['compare'])
            else:
                if now_year == int(year):
                    return True
        elif type(year) is list:
            for sch_year in list(year):
                if type(sch_year) == type([]):
                    if int(sch_year[0]) <= now_year and now_year <= int(sch_year[1]):
                        return True
                else:
                    year_arr=sch_year.split('-')
                    if len(year_arr) == 1:
                        if now_year == int(sch_year):
                            return True
                    elif len(year_arr) == 2:
                        if int(year_arr[0]) <= now_year and now_year <= int(year_arr[1]):
                            return True
        return False

    if 'mm' in data or 'month' in data:
        now_month=datetime.now().month
        if 'mm' in data:
            month=data['mm']
        else:
            month=data['month']
        if month is None:
            return True
        if type(month) is str:
                try:
                    month=ast.literal_eval(month)
                except:
                    pass
        if type(month) is str:
            month_arr=month.split('-')
            if len(month_arr) == 1:
                if 'compare' in data:
                    return compare(now_month,month,data['compare'])
                else:
                    if now_month == int(month):
                        return True
            elif len(month_arr) == 2:
                if int(month_arr[0]) <= now_month and now_month <= int(month_arr[1]):
                    return True
        elif type(month) is int:
            if 'compare' in data:
                return compare(now_month,month,data['compare'])
            else:
                if now_month == int(month):
                    return True
        elif type(month) is list:
            for sch_month in list(month):
                if type(sch_month) == type([]):
                    if int(sch_month[0]) <= now_month and now_month <= int(sch_month[1]):
                        return True
                else:
                    month_arr=sch_month.split('-')
                    if len(month_arr) == 1:
                        if now_month == int(sch_month):
                            return True
                    elif len(month_arr) == 2:
                        if int(month_arr[0]) <= now_month and now_month <= int(month_arr[1]):
                            return True
        return False

    if 'dd' in data or 'day' in data:
        now_day=datetime.now().day
        if 'dd' in data:
            day=data['dd']
        else:
            day=data['day']

        if day is None:
            return True
        if type(day) is str:
                try:
                    day=ast.literal_eval(day)
                except:
                    pass
        if type(day) is str:
            day_arr=day.split('-')
            if len(day_arr) == 1:
                if 'compare' in data:
                    return compare(now_day,day,data['compare'])
                else:
                    if now_day == int(day):
                        return True
            elif len(day_arr) == 2:
                if int(day_arr[0]) <= now_day and now_day <= int(day_arr[1]):
                    return True
        elif type(day) is int:
            if 'compare' in data:
                return compare(now_day,day,data['compare'])
            else:
                if now_day == int(day):
                    return True
        elif type(day) is list:
            for sch_day in list(day):
                if type(sch_day) == type([]):
                    if int(sch_day[0]) <= now_day and now_day <= int(sch_day[1]):
                        return True
                else:
                    day_arr=sch_day.split('-')
                    if len(day_arr) == 1:
                        if now_day == int(sch_day):
                            return True
                    elif len(day_arr) == 2:
                        if int(day_arr[0]) <= now_day and now_day <= int(day_arr[1]):
                            return True
        return False

def check_weeknum(db):
    if not 'weeknum' in db:
        db['weeknum']=[]

    if len(db['weeknum']) == 0:
        return True
    else:
        day_of_month = datetime.now().day
        week_number = (day_of_month - 1) // 7 + 1
        for sch_weeknum in list(db['weeknum']):
            if int(week_number) == int(sch_weeknum):
                return True
        return False

def check_weekday(db):
    if not 'weekday' in db:
        db['weekday']=[]

    if len(db['weekday']) == 0:
        return True
    else:
        now_weekday=datetime.now().weekday()
        for sch_weekday in list(db['weekday']):
            if int(now_weekday) == int(sch_weekday):
                return True
        return False


def check_time(itime,during=None):
    now=ptime()
    if type(itime) == type([]):
        for nowtime in list(itime):
            nowtime_type=type(nowtime)
            if nowtime_type == type([]):
                if len(nowtime) == 2:
                    starttime=ptime(t=nowtime[0],add='today')
                    endtime=ptime(t=nowtime[1],add='today')
                    if starttime > endtime:
                        endtime=ptime(t=endtime.strftime('%Y-%m-%d %H:%M'),add='1d')
                    if starttime <= now and now < endtime:
                        return True
                elif len(nowtime) == 1:
                    nowtime=nowtime[0]
                    nowtime_type=type(nowtime)

            if nowtime_type is str:
                if during is None or during == 0 or during == 1:
                    start_time=ptime(t=nowtime,p='%H:%M')
                    now_time=ptime(p='%H:%M')
                    if  start_time == now_time:
                        return True
                else:
                    starttime=ptime(t=nowtime,add='today')
                    endtime=ptime(t=starttime.strftime('%Y-%m-%d %H:%M'),add='{0}m'.format(during))
                    if starttime <= now and now < endtime:
                        return True
    elif type(itime) is str:
         if during is None or during == 0 or during == 1:
             start_time=ptime(t=itime,p='%H:%M')
             now_time=ptime(p='%H:%M')
             if  start_time == now_time:
                 return True
         else:
             starttime=ptime(t=itime,add='today')
             endtime=ptime(t=starttime.strftime('%Y-%m-%d %H:%M'),add='{0}m'.format(during))
             if starttime <= now and now < endtime:
                 return True
    return False
