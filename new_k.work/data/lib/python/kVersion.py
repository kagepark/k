import re
def version_number_up(s):
    """ look for the last sequence of number(s) in a string and increment """
    lastNum = re.compile(r'(?:[^\d]*(\d+)[^\d]*)+')
    m = lastNum.search(s)
    if m:
        next = str(int(m.group(1))+1)
        start, end = m.span(1)
        s = s[:max(end-len(next), start)] + next + s[end:]
    return s

def version_up(src,version_tag='_k_version='):
    version_str=re.findall("{}.*[0-9]".format(version_tag),src)
    if version_str:
        new_version_str=version_number_up(version_str[0])
        src=src.replace("{}".format(version_str[0]),"{}".format(new_version_str))
        return src
    return src
