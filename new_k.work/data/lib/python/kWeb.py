#!/usr/bin/env python
# coding: utf-8
# encoding=utf8
#
# Copyright CEP(Kage Park)
# license : GPL
# Created at 2019-06-21 16:46 by K
#
# python 2.6~ for no newline on print()
# from __future__ import print_function
import request
import os
import zlib
import base64

_k_version="1.1.5"

import requests, requests.utils, pickle

def post_url(url,user=None,password=None,options=None,token='csrftoken',session_file='/tmp/k.tmp',ssl=False):
    ssl_file=False
    if ssl:
        #ssl_file=requests.certs.where()
        #ssl_file=certifi.old_where()
        ssl_file=False
    if user is not None and password is not None and os.path.isfile(session_file):
        os.unlink(session_file)
    if os.path.isfile(session_file):
        with open(session_file) as f:
            ss = pickle.load(f)
        csrf_token=ss.cookies[token]
        data={'csrfmiddlewaretoken':csrf_token}
        if options is not None and len(options) > 0:
            data.update(options)
        try:
            r = ss.post(url, data=data, verify=ssl_file) # ssl
        except:
            r = False
    else:
        # Login
        ss = requests.Session()
        ss.stream = False
        try:
            ss.get(url,verify=ssl_file) # ssl
        except requests.exceptions.RequestException as e:
            print('{0}\n{1}\n{2}'.format(url,ssl_file, e))
            return False

        csrf_token=ss.cookies[token]
        data={'csrfmiddlewaretoken':csrf_token,'username': user, 'password':password}
        if options is not None and len(options) > 0:
            data.update(options)
        headers={"X-CSRFToken": csrf_token}
        try:
            r = ss.post(url, data=data, headers=headers, verify=ssl_file) # ssl
            with open(session_file,'wb') as f:
                session = pickle.dump(ss,f)
        except:
            r = False
    if r.status_code == requests.codes.ok:
        return r
    else:
        return False
#def post_url_old(url,user=None,password=None,options=None,token='csrftoken',session_file='/tmp/abc'):
#    if user is not None and password is not None and os.path.isfile(session_file):
#        os.unlink(session_file)
#    if user is None and password is None and os.path.isfile(session_file):
#        with open(session_file) as f:
#            ss = pickle.load(f)
#        csrf_token=ss.cookies[token]
#        data={'csrfmiddlewaretoken':csrf_token}
#        if options is not None and len(options) > 0:
#            data.update(options)
#        r = ss.post(url, data=data, verify=False)
#    else:
#        # Login
#        ss = requests.Session()
#        ss.stream = False
#        print('11111')
#        ss.get(url,verify=False)
#        print('22222',url)
#
#        csrf_token=ss.cookies[token]
#        data={'csrfmiddlewaretoken':csrf_token,'username': user, 'password':password}
#        if options is not None and len(options) > 0:
#            data.update(options)
#        headers={"X-CSRFToken": csrf_token}
#        try:
#           r = ss.post(url, data=data, headers=headers, verify=False)
#           print(r.status_code)
#           with open(session_file,'wb') as f:
#               session = pickle.dump(ss,f)
#        except:
#           print('bbbb')
#           return
#    return r
#Login
#a=post_url('http://kage.cep.kr:7990/accounts/login/',user='xxxxx',password='xxxx')
#print(a)
#print('{0}'.format(a.text))
# get data
#print(sys.argv)
#print(post_url('http://kage.cep.kr:7990/list/').text)

def get_django(django_url=None,url_port=80,ids=None,list_tag=False,data=None,ssl=False):
    rc={}
    if django_url and url_port:
        if data is not None:
            rc=data
        if list_tag or ids == 'all':
            host_url='{0}'.format(django_url)
            r = post_url(host_url,ssl=ssl)
            try:
                json_data=json.loads(r.text)
            except:
                print("Please re-login")
                sys.exit(1)
     
            if ids is None:
                return json_data
     
        if ids is None:
            return False
     
        if ids == 'all':
            ids=''
            for ii in json_data:
               if ids == '' or ids == 'all':
                   ids='{0}'.format(ii)
               else:
                   ids='{0},{1}'.format(ids,ii)
     
        for ii in ids.split(','):
            if not ii in rc.keys():
                host_url='{0}{1}/'.format(django_url,ii)
                try:
                    #r = ss.post(host_url, verify=False)
                    r = post_url(host_url,ssl=ssl)
                except requests.exceptions.RequestException as e:
                    return False
                try:
                    json_data=json.loads(r.text)
                except:
                    return False
                rc[json_data['id']]={}
                rc[json_data['id']]={'code':json_data['code'],'crelay':json_data['crelay']}
     
    return rc


def str2url(string):
    return string.replace('/','%2F').replace(':','%3A').replace('=','%3D').replace(' ','+')

def get_session_key(request):
    return request.session._get_or_create_session_key()

def s2be(string):
    enc='{0}'.format(string)
    tmp=zlib.compress(enc.encode("utf-8"))
    return '{0}'.format(base64.b64encode(tmp).decode('utf-8'))

def be2s(string):
    if type(string) is str:
        dd=zlib.decompress(base64.b64decode(string))
        return '{0}'.format(dd.decode("utf-8"))
    return string

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def DownloadFile(HttpResponse,filename=None,json=None,field_name=None,store_dir=None):
    if json is not None and field_name is not None:
        if field_name in json:
            filename = json[field_name]
    if store_dir is None:
        path_to_file=filename
    else:
        path_to_file=os.path.join(store_dir, os.path.basename(filename))
    if os.path.isfile(path_to_file):
        with open(path_to_file, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/force-download")
            response['Content-Disposition'] = 'inline; filename=' + filename
            return response

def ConvertSerializer2Json(obj,item):
    from django.utils.six import BytesIO
    from rest_framework.parsers import JSONParser
    from rest_framework.renderers import JSONRenderer
    serializer = obj(item)
    jsonsum = JSONRenderer().render(serializer.data)
    stream = BytesIO(jsonsum)
    data = JSONParser().parse(stream)
    return data

def get_method_in_class(class_name):
    ret = dir(class_name)
    if hasattr(class_name,'__bases__'):
        for base in class_name.__bases__:
            ret = ret + get_method_in_class(base)
    return ret

def get_val(find,request,rc=None):
    rq_arr=request.__class__.__module__.split('.')
    if len(rq_arr) == 2:
       rq_type=rq_arr[1]
    else:
       return rc
    if rq_type == 'models':
        if find in get_method_in_class(request):
           tmp=getattr(request,find)
           if tmp != '':
               return tmp
    elif rq_type == 'forms':
        mm=request.cleaned_data.get(find)
        if mm is not None and mm != '':
           return mm
    elif rq_type == 'request':
        if request.method == 'GET':
            if find in request.GET and type(request.GET[find]) is str and len(request.GET[find]) > 0:
                return request.GET[find]
        elif request.method == 'POST':
            if find in request.data and type(request.data[find]) is str and len(request.data[find]) > 0:
                return request.data[find]
    return rc

def split_page(list_item,page,num_per_page):
    page=int(page)
    num=int(num_per_page)
    last_page=int(len(list_item)/num)
    first_page=1
    if page <= 1:
        previous_page=''
        next_page=page+1
        now_page=1
        page_data=list_item[:num]
    elif page == last_page:
        previous_page=page-1
        next_page=''
        now_page=page
        page_data=list_item[(last_page-1)*num:]
    else:
        previous_page=page-1
        next_page=page+1
        now_page=page
        page_data=list_item[(page-1)*num:(page*num)]

    return {'data':page_data,'firs_page':first_page,'last_page':last_page,'previous_page':previous_page,'next_page':next_page,'now_page':now_page}


# Django
def item_sort(dic,sort_field_name=None,rev=False):
    if rev and ( rev is True or rev == '1'):
        rev=True
    else:
        rev=False
    if sort_field_name is None:
        return sorted(dic.items(),reverse=rev)
    sort_dict={}
    for skey in dic.keys():
        if sort_field_name in dic[skey]:
            sort_dict[skey]=dic[skey][sort_field_name] # Fix date time sort
        else:
            sort_dict[skey]=''
    contacts=[]
    for i in sorted(sort_dict.items(),key=lambda kv: kv[1], reverse=rev):
        contacts.append((i[0],dic[i[0]]))
    return contacts

def multi_find(find_str,target_str,symbol=',',filter_in_like=False,filter_in_name=None):
    for ii in find_str.split(symbol):
        if filter_in_like: # Search something
            if ii.lower() in target_str.lower():
                return True
        else:
            if ii.lower() == target_str.lower():
                return True
    return False

def get_Dict_from_Obj(app_name,DJ_MODEL_OBJ,group_fieldname,item_name,name_item_value,model_list,select_fields=[],filter_out_name=None,filter_out_value=None,filter_in_name=None,filter_in_value=None,filter_in_like=False):
#    example model for server's item)
#    class Inventory(models.Model):
#       created = models.DateTimeField(auto_now_add=True)
#       bmc_mac = models.CharField(max_length=17, blank=True, default='')
#       item_name = models.CharField(max_length=100, blank=True, default='') # KEY NAME
#       item_info = models.TextField(default='',blank=True) # KEY NAME's DATA
#       item_desc = models.TextField(default='',blank=True)
#       item_del_date = models.CharField(max_length=10, blank=True, default='')
#       item_date = models.CharField(max_length=10, blank=True, default='')
#
#    get_Dict_from_Obj(<app_name>,Inventory.objects,'bmc_mac','item_name','item_info',('id','created','bmc_mac','item_name','item_info','item_desc','item_del_date','item_date'))
#    or
#    get_Dict_from_Obj(<app_name>,Inventory.objects.filter(item_del_date=''),'bmc_mac','item_name','item_info',('id','created','bmc_mac','item_name','item_info','item_desc','item_del_date','item_date'))
#
# the object is QuerySet or not check code 
#    if type(DJ_MODEL_OBJ).__name__ != 'QuerySet':
#
# Get all model list??
#    if model_list is None or model_list == 'all': # Get all model list
#        from django.db.models import get_app, get_models
#        app = get_app(app_name)
#        for model in get_models(app):
#            model_list=model._meta.verbose_name    
#    else:

    init_obj_query=DJ_MODEL_OBJ
    if not 'id' in model_list:
        model_list=tuple(['id']+list(model_list))
    obj_list=init_obj_query.values(*model_list).order_by(group_fieldname)
    # convert DB to single line item in a dict
    contacts={}
    filter_out_contacts={}
    filter_in_contacts={}
    for i in obj_list:
         if not i[group_fieldname] in contacts:
             contacts[i[group_fieldname]]={}
         for mm in model_list:
             contacts[i[group_fieldname]][mm]=i[mm] # initial contacts information and add link to detail view (id)
         if select_fields and select_fields != 'all': # filter collect field
             if i[item_name] in select_fields:
                 contacts[i[group_fieldname]][i[item_name]]=i[name_item_value]
         else: # collect all
             contacts[i[group_fieldname]][i[item_name]]=i[name_item_value]
    total_contacts=len(contacts)
    if filter_out_name and filter_out_value is not None: # Filter out unwants
        for i in contacts.keys():
             if filter_out_name in contacts[i] and contacts[i][filter_out_name] != filter_out_value:
                 filter_out_contacts[i]=contacts[i]
        total_contacts=len(filter_out_contacts)
    if filter_in_name and filter_in_value is not None: # Search something
        base_contacts={}
        if filter_out_contacts:
            base_contacts=filter_out_contacts
        else:
            base_contacts=contacts
        for i in base_contacts.keys():
            if filter_in_name in base_contacts[i]:
                if multi_find(filter_in_value,base_contacts[i][filter_in_name],symbol=',',filter_in_like=filter_in_like,filter_in_name=filter_in_name):
                    filter_in_contacts[i]=base_contacts[i]

        return filter_in_contacts # How to return with total_contacts and found contacts????

    if filter_out_contacts:
        return filter_out_contacts # How to return with total_contacts and found contacts????
    else:
        return contacts # How to return with total_contacts and found contacts????

