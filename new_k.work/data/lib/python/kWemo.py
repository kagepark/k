#!/usr/bin/python

import sys
import os
import re
import urllib2
import time

# Configuration:
# Enter the local IP address of your WeMo in the parentheses of the ip variable below. 
# You may have to check your router to see what local IP is assigned to the WeMo.
# It is recommended that you assign a static local IP to the WeMo to ensure the WeMo is always at that address.
# Uncomment one of the triggers at the end of this script.
#import kLog

def _log(log,msg):
   if log:
       log(msg)

def _dbg(dbg,level,msg):
   if dbg:
       dbg(level,msg)

class Wemo:
   OFF_STATE = '0'
   ON_STATES = ['1', '8']
   ports = [49151, 49152, 49153, 49154, 49155]

   def __init__(self,log=None,dbg=None):
      self.log=dbg
      _log(self.log,'Module load : WeMO switch')
#      if k_class is not None:
#          k_class.log('Module load : WeMO switch')
#      self.k_class=k_class
   
   def _get_header_xml(self, method, obj):
      method = method + obj
      return '"urn:Belkin:service:basicevent:1#%s"' % method
   
   def _get_body_xml(self, method, obj, value=0):
      method = method + obj
      return '<u:%s xmlns:u="urn:Belkin:service:basicevent:1"><%s>%s</%s></u:%s>' % (method, obj, value, obj, method)
	
   def _send(self, method, obj,ip, value=None,port=None):
      body_xml = self._get_body_xml(method, obj, value)
      header_xml = self._get_header_xml(method, obj)
      if port is not None:
         result = self._try_send(ip, port, body_xml, header_xml, obj) 
      else:
         for port in self.ports:
            result = self._try_send(ip, port, body_xml, header_xml, obj) 
            if result is not False:
               break
      return {'status':result,'port':port}

   def _try_send(self, ip, port, body, header, data):
      try:
         request = urllib2.Request('http://%s:%s/upnp/control/basicevent1' % (ip, port))
         request.add_header('Content-type', 'text/xml; charset="utf-8"')
         request.add_header('SOAPACTION', header)
         request_body = '<?xml version="1.0" encoding="utf-8"?>'
         request_body += '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'
         request_body += '<s:Body>%s</s:Body></s:Envelope>' % body
         request.add_data(request_body)
         result = urllib2.urlopen(request, timeout=3)
         rc = self._extract(result.read(), data)
         if rc == '1':
            return 'on'
         elif rc == '0':
            return 'off'
         else:
            return rc
      except Exception as e:
         #print(str(e))
         return False

   def _extract(self, response, name):
      exp = '<%s>(.*?)<\/%s>' % (name, name)
      g = re.search(exp, response)
      if g:
         return g.group(1)
      return response


   def do(self, ip=None, cmd=None):
      port=None
      if ip is None:
         print('Please input IP Address')

         return False
      if cmd is None:
         cmd = 'status'
      act=0

      for ii in range(1,5):
         ok=os.system("ping -c 2 " + ip + '>& /dev/null')
         _dbg(self.log,4,'Wemo ok={0}'.format(ok))
         if ok == 0:
             for jj in range(1,5):
                status = self._send('Get', 'BinaryState',ip,port=port)
                _dbg(self.log,4,'Wemo cmd={0} , status={1}'.format(cmd,status))
                if status['status'] == 'False':
                    sleep(2)
                    continue
                port=status['port']
                if cmd == 'status':
                   return status['status']
                elif cmd == status['status']:
                   if act == 0:
                       return
                   else:
                       return True
                elif cmd == 'on':
                   self._send('Set', 'BinaryState',ip, 1,port=port)
                   act=act+1
                elif cmd == 'name':
                   rc = self._send('Get', 'FriendlyName',ip)
                   return rc['status']
                elif cmd == 'signal':
                   rc = self._send('Get', 'SignalStrength',ip)
                   return rc['status']
                elif cmd == 'toggle':
                   if status['status'] == 'on':
                       self._send('Set', 'BinaryState',ip, 0,port=port)
                       cmd = 'off'
                       act=act+1
                   else:
                       self._send('Set', 'BinaryState',ip, 1,port=port)
                       cmd = 'on'
                       act=act+1
                elif cmd == 'off':
                   self._send('Set', 'BinaryState',ip, 0,port=port)
                   act=act+1
         else:
             time.sleep(7)
      return False
    
if __name__ == "__main__":
    switch = Wemo()
    ip = str(sys.argv[1])
    cmd = str(sys.argv[2])
    aa=switch.do(ip,cmd)
    if aa is True:
        print(cmd + ' done')
    elif aa is None:
        print(cmd + ' already done')
    elif aa is False:
        print(cmd + ' Fail')
    else:
        print(aa)

