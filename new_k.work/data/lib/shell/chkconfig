########################################################################
# Copyright (c) CEP Research Institude, All rights reserved. Since 2008
#    Kage Park <kagepark@cep.kr>
#$version$:0.1.0
# Created at Fri Jul 24 09:51:04 CDT 2015
# Description : 
########################################################################
KTAG=".k"
# _k_  : Kage function
# _kv_  : Kage variable

#include k.k
#include kcmd.so
#include version.k

_k_chkconfig_help() {
  #echo "chkconfig - v$(grep "^#\$version\$:" $0 | awk -F: ["]{print $2}["])
  echo "
$(basename $0) [<opt> [<input>] ]
  $(_k_version_info $0)

  --help : help
  "
  _k_exit 1
}

# base starting sample
# _k_opt <find> <num> <ro> <input options>

# <num>
#   all      : get whole value between option (start -)
#   #        : get values of # after finded option (default 1)

# <ro>
#  ro=0      : return <value> (default)
#  ro=1      : return <remained opt>
#  ro=2      : return <value>|<remained opt>

# example for cmd is sample
_k_chkconfig() {
   local cmd rcmd opt get_val
   echo $1 | grep "^-" >& /dev/null || cmd=$1
   declare -F ${FUNCNAME}_${cmd} >& /dev/null && shift 1

   opt=("$@")
   if _k_opt --help 0 0 "${opt[@]}" >/dev/null; then
       _k_chkconfig_help
#   elif get_val=$(_k_opt -t 1 0 "${opt[@]}"); then
#      _k_chkconfig_sample $get_val || return 1
#or
#   elif _k_opt -t 2 0 "${opt[@]}" >& /dev/null; then
#      _k_chkconfig_sample "${opt_out[@]}" || return 1
   else
      # <command> "${opt[@]}"
      # or
      #echo "Unknown option"
      #_k_exit 1
      _k_chkconfig_daemonctl "${opt[@]}"
   fi
}

#_k_chkconfig_sample () {
#  local opts
#  opts=("$@")
#  example  for "k chkconfig sample"
#}


_k_chkconfig_daemonctl() {
    local name level func
    func=$1
    name=$2
    level=$3

#declare -F
#echo ">> $func/$name/$level"

#    _k_cmd_check_os

    if (( $(_k_find_word $_kv_OS_NAME rhel centos suse sles && echo 1 || echo 0 ) == 1 )); then
      if (( $(_k_version_version $_kv_OS_RELEASE 7.0; echo $?) <= 1 )); then
         if [ "$name" == "nfslock" ]; then
             name="nfs-lock"
         elif [ "$name" == "rpcidmapd" ]; then
             name="nfs-idmapd"
         elif [ "$name" == "cman" ]; then
             return 0
         elif [ "$name" == "cpuspeed" ]; then
             name="cpupower"
         elif [ "$name" == "iptables" ]; then
             name="firewalld"
         fi

         if [ "$func" == "on" ]; then
             if [ "$name" == "tftp" ]; then
                 systemctl enable tftp.socket
                 systemctl enable $name
             else
                 [ -f /etc/init.d/$name ] && chkconfig --level 35 $name on || systemctl enable $name
             fi
         elif [ "$func" == "off" ]; then
             [ -f /etc/init.d/$name ] && chkconfig --level 12345 $name off || systemctl disable $name
         elif [ "$func" == "add" ]; then
             [ -f /etc/init.d/$name ] && chkconfig --add $name || systemctl enable $name
         elif [ "$func" == "del" ]; then
             [ -f /etc/init.d/$name ] && chkconfig --del $name || systemctl disable $name
         elif [ "$func" == "status" ]; then
             [ -f /etc/init.d/$name ] && /etc/init.d/$name status || systemctl status $name
         elif [ "$func" == "start" ]; then
             if [ "$name" == "tftp" ]; then
                 systemctl start tftp.socket
                 systemctl start $name
             elif [ "$name" == "nfs" ]; then
                 modprobe nfsv3 >/dev/null
                 systemctl start nfs
             else
                 [ -f /etc/init.d/$name ] && /etc/init.d/$name start || systemctl start $name
             fi
         elif [ "$func" == "stop" ]; then
             if [ "$name" == "tftp" ]; then
                 systemctl stop tftp.socket
                 systemctl stop $name
             else
                 [ -f /etc/init.d/$name ] && /etc/init.d/$name stop || systemctl stop $name
             fi
         elif [ "$func" == "restart" ]; then
             [ -f /etc/init.d/$name ] && /etc/init.d/$name stop || systemctl stop $name
             [ -f /etc/init.d/$name ] && /etc/init.d/$name start || systemctl start $name
         elif [ "$func" == "list" ]; then
             if [ -n "$name" ]; then
                 [ -f /etc/init.d/$name ] && chkconfig --list $name || systemctl list-unit-files | grep "\.service" | grep "${name}\."
             else
                 systemctl list-unit-files | grep "\.service"
                 chkconfig --list 2>/dev/null
             fi
         fi
      else
         if [ "$func" == "on" ]; then
             chkconfig --level $level $name on
         elif [ "$func" == "off" ]; then
             chkconfig --level $level $name off
         elif [ "$func" == "add" ]; then
             [ -f /etc/init.d/$name ] && chmod 755 /etc/init.d/$name
             chkconfig --add $name
         elif [ "$func" == "del" ]; then
             chkconfig --del $name
         elif [ "$func" == "status" ]; then
             [ -f /etc/init.d/$name ] && /etc/init.d/$name status || service $name status
         elif [ "$func" == "start" ]; then
             [ -f /etc/init.d/$name ] && /etc/init.d/$name start || service $name start
         elif [ "$func" == "stop" ]; then
             [ -f /etc/init.d/$name ] && /etc/init.d/$name stop || service $name stop
         elif [ "$func" == "restart" ]; then
             [ -f /etc/init.d/$name ] && /etc/init.d/$name stop || service $name stop
             [ -f /etc/init.d/$name ] && /etc/init.d/$name start || service $name start
         elif [ "$func" == "list" ]; then
             chkconfig --list $name
         fi
      fi
    else
      echo "Not support this OS"
      return 1
    fi
}


# IF run a shell then run a shell. if load the shell then not running shell. just load
if [ "$(basename $0 2>/dev/null)" == "chkconfig.k" ]; then
    declare -F _k_k >& /dev/null || source $_K_BIN/k.k
    _k_load_include $0

    # Run this script to main function
    _k_$(basename $0 | sed "s/${KTAG}$//g") "$@" || _k_exit $?
fi
