KTAG=".k"
# _k_  : Kage function
# _kv_  : Kage variable

#include k.k

_k_dev_help() {
  echo "
  device file manage

  $(basename $0) [<opt> [<input>] ]

  --help : help
  -u <dev>       : check use <dev> or not 
  -t <dev>       : show <dev> type
  -console       : show device of activated console
  -net           : show network device information
  -ib <dev name> : show network device name (<dev name> ex: mlx4_0, hfi1_0)
  -find_ib [<dev name>] : show active IB device
  "
  _k_exit
}

_k_dev() {
   local cmd rcmd opt get_val
   echo $1 | grep "^-" >& /dev/null || cmd=$1
   declare -F ${FUNCNAME}_${cmd} >& /dev/null && shift 1

   opt=("$@")

   if _k_opt --help 0 0 "${opt[@]}" >/dev/null; then
       _k_dev_help
       _k_exit 0
   elif get_val=$(_k_opt -u 1 0 "${opt[@]}"); then
      _k_dev_used $get_val || return 1
   elif get_val=$(_k_opt -t 1 0 "${opt[@]}"); then
      _k_dev_block_type $get_val || return 1
   elif get_val=$(_k_opt -ib 1 0 "${opt[@]}"); then
      _k_dev_ib_dev_name $get_val || return 1
   elif get_val=$(_k_opt -find_ib 1 0 "${opt[@]}"); then
      _k_dev_active_ib $get_val || return 1
   elif get_val=$(_k_opt -console 0 0 "${opt[@]}"); then
      _k_dev_console || return 1
   elif get_val=$(_k_opt -net 0 0 "${opt[@]}"); then
      _k_dev_net_info || return 1
   else
      echo "Unknown option"
      _k_exit 1
   fi
}

# cmd sample
_k_dev_used() {
    local dev
    dev=$1
    [ -b $dev ] || return 1
    fuser -m $dev >& /dev/null && return 0
    [ -d /sys/block/$(basename $dev) ] || return 1
    for (( ii=1; ii<$(cat /sys/block/$(basename $dev)/range); ii++)); do
        [ -b $dev$ii ] || continue
        if fuser -m $dev$ii >& /dev/null; then
           echo $ii
           return 0
        fi
    done
    return 1
}

_k_dev_block_type() {
    local dev
    dev=$1
    [ -b $dev ] || return 1
    file -s $(readlink -f $dev)
}

_k_dev_dm() {
    local dev
    dev=$1
    [ -n "$dev" ] || return 1
    dev_name=$(basename $dev)
    if dm_arr=($(dmsetup table $dev_name 2>/dev/null)); then
        if [ "${dm_arr[2]}" == "snapshot" ]; then
            echo "/dev/loop$(echo ${dm_arr[4]} | awk -F: ["]{print $2}["])"
            echo "/dev/loop$(echo ${dm_arr[5]} | awk -F: ["]{print $2}["])"
        elif [ "${dm_arr[2]}" == "linear" ]; then
            _k_readlink /dev/mapper/$dev_name
        elif [ "${dm_arr[2]}" == "multipath" ]; then
            _k_readlink /dev/mapper/$dev_name
        else
            return 1
        fi
    fi
}

_k_dev_dm_info() {
    local dev
    dev=$1
    [ -n "$dev" ] || return 1
    dev_name=$(basename $dev)
    dmsetup info $dev_name 
}

_k_dev_source() {
    local devs
    devs=$1
    [ -n "$devs" ] || return 1
    for dev in $devs; do
       [ -b "$dev" ] || continue
       losetup $dev | awk ["]{print $3}["] | sed -e "s/(//g" -e "s/)//g"
    done
}

_k_dev_mounted_source() {
    local mnt
    mnt=$1
    [ -n "$mnt" ] || return 1
    dev=$(awk ["]{if($2=="$mnt") print $1}["] /proc/mounts)
    [ -n "$dev" ] || return 1
    _k_dev_source $(_k_dev_dm $dev)
}

_k_dev_console ()
{
    version=0.1.3;
    desc="Copyright (c) CEP Research Institude, All rights reserved. Since 2008
Created at Tue Apr 12 16:32:17 UTC 2016
Description : _k_dev_console
";
    func_help="${FUNCNAME} [<opt> [<input>] ]
  version: $version
  --help : help
  --desc : view description
";
    if [ "$1" == "--help" ]; then
        echo "$func_help";
        return 0;
    else
        if [ "$1" == "--desc" ]; then
            echo "$desc";
            return 0;
        fi;
    fi;
    local tty;
    tty=$1;
    [ -n "$tty" ] || tty=console;
    for ((ii=0; ii<30; ii++)); do
        if [ -f /sys/class/tty/$tty/active ]; then
            tty=$(< /sys/class/tty/$tty/active);
#            tty=${tty##* };
            break
        else
            echo "Wait $tty device..."
            sleep 2
        fi
    done;
    echo "$tty"
}

_k_dev_net_info() {
   for _net in $(ls /sys/class/net | grep "^[e|i]"); do
      speed=""
      duplex=""
      ETH_DEV=( $(ls -l /sys/class/net/$_net/device/driver) )
      m_name=$(basename ${ETH_DEV[$((${#ETH_DEV[*]}-1))]})
      state=$(cat /sys/class/net/$_net/operstate)
      if [ "$state" == "up" ]; then
          if echo "$_net" | grep "eth[0-9]" >/dev/null;  then
              speed=$(cat /sys/class/net/$_net/speed)
              duplex=$(cat /sys/class/net/$_net/duplex)
              mtu=$(cat /sys/class/net/$_net/mtu)
          else
              dev_name=$(ls /sys/class/net/$_net/device/infiniband)
              [ "$old_dev_name" == "$dev_name" ] &&  port=$(($port+1)) || port=1
              rate=( $(cat /sys/class/net/$_net/device/infiniband/$dev_name/ports/$port/rate) )
              speed="${rate[0]}Gb"
              act=( $(cat /sys/class/net/$_net/device/infiniband/$dev_name/ports/$port/state) )
              mtu=$(cat /sys/class/net/$_net/mtu)
              duplex="${act[1]}"
              old_dev_name=$dev_name
          fi
      fi

      if [ -n "$speed" ]; then
          if [ -n "$dev_name" ]; then
              printf "%-15s\n" "$_net ($m_name($dev_name)/$state/$speed/$duplex/$mtu)"
          else
              printf "%-15s\n" "$_net ($m_name/$state/$speed/$duplex/$mtu)"
          fi
      else
          printf "%-10s\n" "$_net ($m_name/$state/$mtu)"
      fi
   done
}

_k_dev_ib_dev_name() {
   local dev
   dev=$1
   [ -n "$dev" ] || return 1
   [ -d /sys/class/infiniband/$dev ] || return 1
   ls /sys/class/infiniband/$dev/device/net
}

_k_dev_active_ib() {
   local ib_net_device dev_num
   [ -n "$*" ] && ib_net_device=($*) || ib_net_device=($(cd /sys/class/infiniband 2>/dev/null && ls))

   for ((ii=0;ii<${#ib_net_device[*]};ii++)); do
       dev_path=$(grep -il InfiniBand ${ib_net_device[$ii]}/ports/*/link_layer 2> /dev/null) || continue
       for ((jj=0;jj<60; jj++)); do
           if grep ACTIVE $(echo $dev_path | sed "s/link_layer/state/g") 2>/dev/null; then
                echo ${ib_net_device[$ii]}
                dev_num=$(($dev_num+1))
                break
           fi
           sleep 2
       done
   done
   [ "$dev_num" == "0" ] && return 1 || return 0
}

# IF run a shell then run a shell. if load the shell then not running shell. just load
if [ "$(basename $0 2>/dev/null)" == "dev.k" ]; then
    declare -F _k_k >& /dev/null || source $_K_BIN/k.k
    _k_load_include $0

    # Run this script to main function
    _k_$(basename $0 | sed "s/${KTAG}$//g") $* || _k_exit $?
fi
