########################################################################
# Copyright (c) CEP Research Institude, All rights reserved. Since 2008
#    Kage Park <kagepark@cep.kr>
#$version$:0.5.0
# Created at Tue Nov  3 09:20:46 CST 2015
# Description : newer fp.k (re-write)
########################################################################
KTAG=".k"
# _k_  : Kage function
# _kv_  : Kage variable

#include k.k
#include md5.k
#include time.k

# for Old format
_kv_date=0
_kv_file=2

_k_wl() {
  local file
  file=$1
  [ -f "$file" ] || return 1
  if [ "$(uname)" == "Darwin" ]; then
      wc -l $file  | awk '{print $1}'
  else
      wc -l $file
  fi
}

_k_fp2_print() {
  local date file md5
  date=$1
  md5=$2
  file=$3
  echo "$(_k_time_convert $date) : $md5 : $file"
}

_k_fp2_help() {
  #echo "fp2 - v$(grep "^#\$version\$:" $0 | awk -F: '{print $2}')
  echo "

$(basename $0) [<opt> [<input>] ]
  $(_k_version_info $0)

  --help : help
  -fp <finger_print_path> : default ( ~/.k_md5 )
  -cmd <command>          : del, add, check, cleanup, find, view, duplicate
       del <md5> [<file>]     : remove finger print [ of <file>] in db
       add <md5> <file>       : add file's finger print to db
       cleanup [<md5>][<file>]: cleanup wrong db information in db
       grep <filename>        : find <filename> in whole db
       find <file>            : find <file> in db
       view <md5>             : view db information
       duplicate              : find a db which had duplicated file lists in the db
       duplicate <file>       : check <file> which had duplicated or not
       check [<md5>][<file>]  : check [<md5>] [<file>]
  -dest <file|path>           : check file|path 
  -md5 <file's md5>           : file's md5
  -vv                         : Verbose mode

  * follow <command>'s input data from -dest or -md5 options
  "
  exit 1
}

# base starting sample
# _k_opt <find> <num> <ro> <input options>

# <num>
#   all      : get whole value between option (start -)
#   #        : get values of # after finded option (default 1)

# <ro>
#  ro=0      : return <value> (default)
#  ro=1      : return <remained opt>
#  ro=2      : return <value>|<remained opt>

# example for cmd is sample
_k_fp2() {
   local cmd rcmd opt get_val
   echo $1 | grep "^-" >& /dev/null || cmd=$1
   declare -F ${FUNCNAME}_${cmd} >& /dev/null && shift 1

   opt=("$@")
   _k_opt --help 0 0 "${opt[@]}" >/dev/null && _k_fp2_help
   _kv_fp_path=$(_k_opt -fp 1 0 "${opt[@]}")
   _kv_cmd=$(_k_opt -cmd 1 0 "${opt[@]}")
   _kv_file=$(_k_opt -dest 1 0 "${opt[@]}")
   _kv_md5=$(_k_opt -md5 1 0 "${opt[@]}")
   _k_opt -vv 0 0 "${opt[@]}" >&/dev/null && _kv_vv=1

   [ -n "$_kv_fp_path" ] && _K_FINGER_MD5=$_kv_fp_path || _K_FINGER_MD5=~/.k_md5
   export _K_FINGER_MD5

   [ -n "$_kv_cmd" ] || _kv_cmd=check
   if [ -n "$_kv_file" -a -d "$_kv_file" ]; then
      find $_kv_file -type f | while read line; do
          _k_fp2_ctl "$_kv_cmd" "" "$line"
      done
   else 
      _k_fp2_ctl "$_kv_cmd" "$_kv_md5" "$_kv_file"
   fi
}

#_k_fp2_sample () {
#  local opts
#  opts=("$@")
#  example  for "k fp2 sample"
#}

_k_fp2_ctl() {
   local opt md5 file
   opt=$1
   [ -n "$opt" ] || opt=check
   md5=$2 
   file=$3
   [ ! -n "$md5" -a ! -n "$file" ] && return 1
   if [ ! -n "$md5" -a -n "$file" ]; then
       md5=$(_k_md5_sum "$file") || return 1
   fi

   if [ "$opt" == "del" ]; then
      if [ -n "$file" ]; then
          cat "$_K_FINGER_MD5/$md5/info.txt" | while read line; do
              info_file=$(echo $line | awk -F\| '{print $4}')
              if [ "$file" == "$info_file" ]; then
                  [ "$_kv_vv" == "1" ] && echo "Del:$ii:$file"
                  sed -i "/$file/d" "$_K_FINGER_MD5/$ii/info.txt"
              fi
          done
          (( $(_k_wl "$_K_FINGER_MD5/$md5/info.txt") == 0 )) && rm -fr "$_K_FINGER_MD5/$md5"
      else
          if [ -d "$_K_FINGER_MD5/$md5" ]; then
               [ "$_kv_vv" == "1" ] && echo "Del:$ii"
               rm -fr "$_K_FINGER_MD5/$md5" || return 0
          fi
      fi
   elif [ "$opt" == "cleanup" ]; then
      [ -d "$_K_FINGER_MD5/$md5" ] || return 0
      if [ -n "$md5" ]; then
              cat "$_K_FINGER_MD5/$md5/info.txt" | while read line; do
                  info_file=$(echo $line | awk -F\| '{print $4}')
                  if [ -f "$info_file" ]; then
                      return 0
                  else
                      [ "$_kv_vv" == "1" ] && echo "Cleanup:$md5:$info_file"
                      sed -i "/$info_file/d" "$_K_FINGER_MD5/$md5/info.txt"
                  fi
              done
      else
         for ii in $(ls "$_K_FINGER_MD5"); do
            if [ -f "$_K_FINGER_MD5/$ii/info.txt" ]; then
              cat "$_K_FINGER_MD5/$ii/info.txt" | while read line; do 
                  info_file=$(echo $line | awk -F\| '{print $4}')
                  if [ -f "$info_file" ]; then
                      return 0
                  else
                      [ "$_kv_vv" == "1" ] && echo "Cleanup:$ii:$info_file"
                      sed -i "/$info_file/d" "$_K_FINGER_MD5/$ii/info.txt"
                  fi
              done
            else
              [ "$_kv_vv" == "1" ] && echo "Cleanup:$ii:not found info.txt => Del"
              rm -fr "$_K_FINGER_MD5/$ii"
            fi
         done
      fi
   elif [ "$opt" == "add" ]; then
      [ -n "$file" ] || return 1
      if [ -d "$_K_FINGER_MD5/$md5" ]; then
          if [ "$(awk -F\| -v add_file="$file" '{if($4==add_file) printf "found"}' "$_K_FINGER_MD5/$md5/info.txt")" == "found" ]; then
              [ "$_kv_vv" == "1" ] && echo "Add:$md5:$file:Found => Ignore"
              return 0
          fi
          [ "$_kv_vv" == "1" ] && echo "Add:$md5:$file"
          echo "$(date +"%s")|init|$file|$file" >> "$_K_FINGER_MD5/$md5/info.txt"
      else
          mkdir -p "$_K_FINGER_MD5/$md5"
          [ "$_kv_vv" == "1" ] && echo "Add:$md5:$file"
          echo "$(date +"%s")|init|$file|$file" >> "$_K_FINGER_MD5/$md5/info.txt"
      fi
   elif [ "$opt" == "view" ]; then
      [ -d "$_K_FINGER_MD5/$md5" ] || return 1
      [ "$_kv_vv" == "1" ] && echo "View:$md5"
      cat "$_K_FINGER_MD5/$md5/info.txt" | while read line; do
          _kv_info_date=$(echo $line | awk -F\| '{print $1}')
          _kv_info_file=$(echo $line | awk -F\| '{print $4}')
          _k_fp2_print "$_kv_info_date" "$md5" "$_kv_info_file"
      done
   elif [ "$opt" == "find" ]; then
      [ -n "$file" ] || return 1
      cat "$_K_FINGER_MD5/$md5/info.txt" | while read line; do
          _kv_info_date=$(echo $line | awk -F\| '{print $1}')
          _kv_info_file=$(echo $line | awk -F\| '{print $4}')
          _k_fp2_print "$_kv_info_date" "$md5" "$_kv_info_file"
      done
   elif [ "$opt" == "grep" ]; then
      [ -n "$file" ] || return 1
      find "$_K_FINGER_MD5" -name "info.txt" | while read line; do
          [ "$_kv_vv" == "1" ] && echo "find:check $(dirname $line) ..."
          grep "$file" "$line" >& /dev/null && echo "$line"
      done
   elif [ "$opt" == "duplicate" ]; then
      if [ -n "$file" ]; then
        [ -n "$_K_FINGER_MD5/$md5" ] || return 1
        dup_cnt=$(_k_wl "$_K_FINGER_MD5/$md5/info.txt")
        if (( $dup_cnt > 1 )); then
            if [ "$_kv_vv" == "1" ]; then
              cat "$_K_FINGER_MD5/$md5/info.txt" | while read line2; do
                  _kv_info_date=$(echo $line2 | awk -F\| '{print $1}')
                  _kv_info_file=$(echo $line2 | awk -F\| '{print $4}')
                  _k_fp2_print "$_kv_info_date" "$md5" "$_kv_info_file"
              done
            fi
            echo "duplicate:$md5:$dup_cnt"
        fi
      else
        find "$_K_FINGER_MD5" -name "info.txt" | while read line; do
          [ "$_kv_vv" == "1" ] && echo "duplicate:check $(dirname $line) ..."
          dup_cnt=$(_k_wl "$line")
          if (( $dup_cnt > 1 )); then
              if [ "$_kv_vv" == "1" ]; then
                cat "$line" | while read line2; do
                  _kv_info_date=$(echo $line2 | awk -F\| '{print $1}')
                  _kv_info_file=$(echo $line2 | awk -F\| '{print $4}')
                  _k_fp2_print "$_kv_info_date" "$(basename $(dirname $line))" "$_kv_info_file"
                done
              fi
              echo "duplicate:$(basename $(dirname $line)):$dup_cnt"
          fi
        done
      fi
   elif [ "$opt" == "check" ]; then
      [ -d "$_K_FINGER_MD5/$md5" ] || return 1
      if [ -n "$file" ]; then
          cat $_K_FINGER_MD5/$md5/info.txt | while read line; do
              info_file=$(echo $line | awk -F\| '{print $4}')
              if [ "$file" == "$info_file" ]; then
                  [ "$_kv_vv" == "1" ] && echo "check:$md5:$info_file"
              else
                  [ "$_kv_vv" == "1" ] && echo "check:$md5:$info_file <= $file"
                  [ -f "$info_file" ] && echo "Found other file at '$info_file'"
              fi
          done
          return 0
      else
          [ "$_kv_vv" == "1" ] && echo "check:$md5"
          return 0
      fi
   else
      echo "Unknown options"
      return 1
   fi
}


# IF run a shell then run a shell. if load the shell then not running shell. just load
if [ "$(basename $0)" == "fp2.k" ]; then
    declare -F _k_k >& /dev/null || source $_K_BIN/k.k
    _k_load_include $0

    # Run this script to main function
    _k_$(basename $0 | sed "s/${KTAG}$//g") "$@" || exit $?
fi
