#!/bin/bash
# Kage
huge_dir="/sys/devices/system/node/node0/hugepages"
huge_mount="/mnt/huge"
error_exit() {
   echo "$*"
   exit 1
}

show() {
   echo "Show Hugepage setting value:"
   for ii in $(ls $huge_dir); do
       base_huge=$(echo $ii | sed "s/hugepages-//g")
       free_huge=$(cat $huge_dir/$ii/free_hugepages)
       total_huge=$(cat $huge_dir/$ii/nr_hugepages)
       echo " - Huge($base_huge) => Total : $total_huge,  Free : $free_huge"
   done
   echo
   grep -i huge /proc/meminfo
   echo
   egrep 'trans|thp' /proc/vmstat
   echo
   ipcs -m 
   echo
   support_huge_size
}

set_huge() {
   size=$1
   set_base=$2
   [ -n "$set_base" ] || set_base="2048kB"
   [ -d $huge_dir/hugepages-$set_base ] || error_exit "wrong huge base size"
   [ -d "$huge_mount/$set_base" ] || mkdir -p $huge_mount/$set_base
   if [ "$size" == "1048576kB" ]; then
       mountpoint "$huge_mount/$set_base" >& /dev/null || mount -t hugetlbfs -o pagesize=1G none "$huge_mount/$set_base"
   else
       mountpoint "$huge_mount/$set_base" >& /dev/null || mount -t hugetlbfs none "$huge_mount/$set_base"
   fi
   echo $size > $huge_dir/hugepages-$set_base/nr_hugepages
   show
}

chk_huge_thp_enable() {
   cat /sys/kernel/mm/transparent_hugepage/enabled
   cat /sys/kernel/mm/transparent_hugepage/defrag
}

disable_huge_thp() {
   echo never > /sys/kernel/mm/transparent_hugepage/enabled
   echo never > /sys/kernel/mm/transparent_hugepage/defrag
}

find_huge_usage() {
   grep -e AnonHugePages  /proc/*/smaps | awk  '{ if($2>4) print $0} ' |  awk -F "/"  '{print $0; system("ps -fp " $3)} '
}


flush_huge() {
   set_base=$1
   [ -n "$set_base" ] || set_base="2048kB"
   [ -d "$huge_mount/$set_base" ] || error_exit "I can't find huge page directory"
   rm -f $huge_mount/$set_base/*
}

huge_usage() {
   echo
   echo "Hugepage usage:"
   for ii in $(grep HugetlbPages /proc/*/status | awk '{if($2 > 0) print $1}' | awk -F\: '{print $1}'); do
      echo " - $(cat $ii | grep Name)"
      echo " - $(cat $ii | grep State)"
      echo " - Hugepages : $(grep 'hugepage' $(dirname $(echo $ii))/numa_maps | wc -l)"
   done
}

support_huge_size() {
   echo
   echo "Support Hugepage size:"
   echo " - 2mb"
   for ii in $(grep pdpe /proc/cpuinfo | sort | uniq); do
       echo $ii | grep pdpe >& /dev/null && (echo " - $ii" | sed "s/pdpe//g")
   done
}


show
huge_usage
#set_huge "16" "1048576kB" #1G Huge Page
#set_huge "16" "2048kB"    #2M Huge Page (default)
# or
#set_huge "16"             #2M Huge Page (default)