########################################################################
# Copyright (c) CEP Research Institude, All rights reserved. Since 2008
#    Kage Park <kagepark@cep.kr>
#$version$:0.1.0
# Created at Mon Sep 28 10:14:18 CDT 2013
# Description : 
########################################################################
KTAG=".k"
# _k_  : Kage function
# _kv_  : Kage variable

#include k.k

_k_yum_help() {
  echo "

$(basename $0) [<opt> [<input>] ]
  $(_k_version_info $0)

  --help : help
  -r <yum conf file base dir or yum conf file>
  -p <package name1> [...]
  -m <media mount point>
  -iso <iso file>

  for CEP Cluster Engine (CCE/KCE)
  "
  _k_exit 1
}

# base starting sample
# _k_opt <find> <num> <ro> <input options>

# <num>
#   all      : get whole value between option (start -)
#   #        : get values of # after finded option (default 1)

# <ro>
#  ro=0      : return <value> (default)
#  ro=1      : return <remained opt>
#  ro=2      : return <value>|<remained opt>

# example for cmd is sample

_k_yum() {
  local media_dir dvd_iso yum_repo_base cmd rcmd opt get_val
  echo $1 | grep "^-" >& /dev/null || cmd=$1
  declare -F ${FUNCNAME}_${cmd} >& /dev/null && shift 1

  opt=("$@")
  if _k_opt --help 0 0 "${opt[@]}" >/dev/null; then
       _k_yum_help  
  fi
  media_dir=$(_k_opt -m 1 0 "${opt[@]}")
  dvd_iso=$(_k_opt -iso 1 0 "${opt[@]}")
  yum_repo_base=$(_k_opt -r 1 0 "${opt[@]}")
  yum_packages=$(_k_opt -p 1 0 "${opt[@]}")
  [ -n "$media_dir" ] && media_dir=$media_dir || media_dir=$(mktemp -d /tmp/iso-XXXX)
  [ -n "$dvd_iso" -a "$dvd_iso" == "auto" ] && dvd_iso=$(ls /images/$OS_NAME$OS_RELEASE/*${OS_RELEASE}*x86_64*.iso 2>/dev/null | tail -n 1) || dvd_iso=$dvd_iso
  [ -n "$yum_repo_base" ] || yum_repo_base=req/$OS_NAME$OS_RELEASE

  if [ -n "$dvd_iso" -a -f "$dvd_iso" ]; then
    if mountpoint $media_dir >& /dev/null; then
         echo "Alread mounted something at $media_dir"
         _k_exit 1
    fi
    mount -o loop $dvd_iso $media_dir
  fi

  if mountpoint $media_dir >& /dev/null ; then
    repo_file=/etc/yum.repos.d/cce-$(mktemp -u XXXXXX).repo

    echo "[CCE_REQ]
name=CCE_REQ
baseurl=file://$media_dir
enable=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release

[CCE_REQ-HA]
name=CCE_REQ-HA
baseurl=file://${media_dir}/$([ -d ${media_dir}/addons ] && echo "addons/")HighAvailability
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release
" > $repo_file

    if [ -n "$yum_packages" ]; then
      echo "Install packages"
      yum --disablerepo="*" --enablerepo="CCE_REQ*" --disableexcludes=all -y install $yum_packages
    fi
    if [ -f $yum_repo_base ]; then
      echo "Install $ii file"
      yum --disablerepo="*" --enablerepo="CCE_REQ*" --disableexcludes=all -y install $(cat $yum_repo_base)
    elif [ -d $yum_repo_base ]; then
      for ii in $(ls $yum_repo_base/*.yum 2>/dev/null); do
        echo "Install $ii file"
        yum --disablerepo="*" --enablerepo="CCE_REQ*" --disableexcludes=all -y install $(cat $ii)
      done
    fi

    umount $media_dir
    rm -f $repo_file
  fi
}


# IF run a shell then run a shell. if load the shell then not running shell. just load
if [ "$(basename $0 2>/dev/null)" == "yum.k" ]; then
    declare -F _k_k >& /dev/null || source $_K_BIN/k.k
    _k_load_include $0

    # Run this script to main function
    _k_$(basename $0 | sed "s/${KTAG}$//g") "$@" || _k_exit $?
fi
