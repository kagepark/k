*** Setup VM
smc@ubuntu1604:~$ sudo apt-get install qemu-kvm virtinst libvirt-bin bridge-utils ubuntu-vm-builder

smc@ubuntu1604:~$ sudo adduser $(id -un) libvirtd
The user `smc' is already a member of `libvirtd'.

smc@ubuntu1604:~$ sudo adduser $(id -un) vms
The user `smc' is already a member of `vms'.

smc@ubuntu1604:~$ virsh --version
1.3.1

smc@ubuntu1604:~$ libvirtd --version
libvirtd (libvirt) 1.3.1

smc@ubuntu1604:~$ /usr/bin/qemu-system-x86_64 --version
QEMU emulator version 2.5.0 (Debian 1:2.5+dfsg-5ubuntu10.30), Copyright (c) 2003-2008 Fabrice Bellard

smc@ubuntu1604:/etc/network$ sudo vi interfaces
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto eno1
iface eno1 inet dhcp

auto br0
iface br0 inet static
         address 192.168.102.1
         netmask 255.255.255.0
         broadcast 192.168.102.255
         # set static route for LAN
#        post-up route add -net 10.0.0.0 netmask 255.0.0.0 gw 10.18.44.1
#        post-up route add -net 161.26.0.0 netmask 255.255.0.0 gw 10.18.44.1
         bridge_ports eno2
         bridge_stp off
         bridge_fd 0
         bridge_maxwait 0


smc@ubuntu1604:/etc/network$ sudo systemctl restart networking

smc@ubuntu1604:~$ lsmod |grep br
bridge                126976  0
stp                    16384  2 garp,bridge
llc                    16384  3 stp,garp,bridge


smc@ubuntu1604:/etc/network$ sudo brctl show
bridge name	bridge id		STP enabled	interfaces
br0		8000.000000000000	no		


smc@ubuntu1604:~$ kvm-ok
INFO: /dev/kvm exists
KVM acceleration can be used


smc@ubuntu1604:~$ virsh -c qemu:///system list
 Id    Name                           State
----------------------------------------------------

smc@ubuntu1604:~$

Install first VM
using iso)
udo virt-install \
--virt-type=kvm \
--name centos7 \
--ram 2048 \
--vcpus=2 \
--os-variant=centos7.0 \
--virt-type=kvm \
--hvm \
--cdrom=/var/lib/libvirt/boot/CentOS-7-x86_64-DVD-1708.iso \
--network=bridge=br0,model=virtio \
--network=bridge=br1,model=virtio \
--graphics vnc \
--disk path=/var/lib/libvirt/images/centos7.qcow2,size=40,bus=virtio,format=qcow2


install from internet)
smc@ubuntu1604:~$ sudo virt-install -n demo01 -r 256 --vcpus 1 \
    --location "http://us.archive.ubuntu.com/ubuntu/dists/xenial/main/installer-i386/" \
    --os-type linux \
    --os-variant ubuntu16.04 \
    --network bridge=br0,virtualport_type='ovs' \
    --graphics vnc \
    --hvm --virt-type kvm \
    --disk size=8,path=/var/lib/libvirt/images/test01.img \
    --noautoconsole \
    --extra-args 'console=ttyS0,115200n8 serial '

this process will not prompot. because, it will waiting for install OS in the KVM.


smc@ubuntu1604:~$ sudo virsh list --all
 Id    Name                           State
----------------------------------------------------
 10    ubuntu16                       running


if you need ssh turnnel port then
[kage@kagep ~]$ ssh smc@172.16.103.226 -L 5904:127.0.0.1:5901


Connect VNC from local to KVM
VNC Viewer : "localhost:5904"

Install OS on KVM.


# restart vm example ) virsh --connect qemu:///system start demo01

# show list
#$ sudo virsh list --all

# open console
#sudo virsh console 1
