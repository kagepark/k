Install require packages

[root@cent74 ~]# yum -y install wget openssl-devel gcc make python-devel openssl-devel kernel-devel kernel-debug-devel autoconf automake rpm-build redhat-rpm-config libtool

[root@cent74 ~]# wget http://openvswitch.org/releases/openvswitch-2.5.5.tar.gz

[root@cent74 ~]# tar zxvf openvswitch-2.5.5.tar.gz

[root@cent74 ~]# mkdir -p ~/rpmbuild/SOURCES

[root@cent74 ~]# cp openvswitch-2.5.5.tar.gz ~/rpmbuild/SOURCES

[root@cent74 ~]# cd openvswitch-2.5.5

[root@cent74 openvswitch-2.5.5]# sed 's/openvswitch-kmod, //g' rhel/openvswitch.spec > rhel/openvswitch_no_kmod.spec

[root@cent74 openvswitch-2.5.5]# rpmbuild -bb rhel/openvswitch_no_kmod.spec
~~~~
Wrote: /root/rpmbuild/RPMS/x86_64/openvswitch-2.5.5-1.x86_64.rpm
Wrote: /root/rpmbuild/RPMS/x86_64/openvswitch-debuginfo-2.5.5-1.x86_64.rpm
Executing(%clean): /bin/sh -e /var/tmp/rpm-tmp.mf7Ar5
+ umask 022
+ cd /root/rpmbuild/BUILD
+ cd openvswitch-2.5.5
+ rm -rf /root/rpmbuild/BUILDROOT/openvswitch-2.5.5-1.x86_64
+ exit 0

[root@cent74 openvswitch-2.5.5]# rpm -ihv /root/rpmbuild/RPMS/x86_64/openvswitch-2.5.5-1.x86_64.rpm
Preparing...                          ################################# [100%]
Updating / installing...
   1:openvswitch-2.5.5-1              ################################# [100%]

[root@cent74 openvswitch-2.5.5]# ovs-vsctl -V
ovs-vsctl (Open vSwitch) 2.5.5
Compiled Jun 26 2018 13:17:05
DB Schema 7.12.1

[root@cent74 openvswitch-2.5.5]# systemctl status openvswitch
\u25cf openvswitch.service - LSB: Open vSwitch switch
   Loaded: loaded (/etc/rc.d/init.d/openvswitch; bad; vendor preset: disabled)
   Active: inactive (dead)
     Docs: man:systemd-sysv-generator(8)

[root@cent74 openvswitch-2.5.5]# systemctl start openvswitch
[root@cent74 openvswitch-2.5.5]# systemctl status openvswitch
\u25cf openvswitch.service - LSB: Open vSwitch switch
   Loaded: loaded (/etc/rc.d/init.d/openvswitch; bad; vendor preset: disabled)
   Active: active (running) since Tue 2018-06-26 13:19:44 EDT; 2s ago
     Docs: man:systemd-sysv-generator(8)
  Process: 3139 ExecStart=/etc/rc.d/init.d/openvswitch start (code=exited, status=0/SUCCESS)
    Tasks: 4
   CGroup: /system.slice/openvswitch.service
           \u251c\u25003164 ovsdb-server: monitoring pid 3165 (healthy)
           \u251c\u25003165 ovsdb-server /etc/openvswitch/conf.db -vconsole:emer -vsyslog:err -vfile:info --remote=punix:/va...
           \u251c\u25003177 ovs-vswitchd: monitoring pid 3178 (healthy)
           \u2514\u25003178 ovs-vswitchd unix:/var/run/openvswitch/db.sock -vconsole:emer -vsyslog:err -vfile:info --mlockal...

Jun 26 13:19:44 cent74 openvswitch[3139]: /etc/openvswitch/conf.db does not exist ... (warning).
Jun 26 13:19:44 cent74 openvswitch[3139]: Creating empty database /etc/openvswitch/conf.db [  OK  ]
Jun 26 13:19:44 cent74 openvswitch[3139]: Starting ovsdb-server [  OK  ]
Jun 26 13:19:44 cent74 ovs-vsctl[3166]: ovs|00001|vsctl|INFO|Called as ovs-vsctl --no-wait -- init -- set Ope....12.1
Jun 26 13:19:44 cent74 ovs-vsctl[3171]: ovs|00001|vsctl|INFO|Called as ovs-vsctl --no-wait set Open_vSwitch ....wn\""
Jun 26 13:19:44 cent74 openvswitch[3139]: Configuring Open vSwitch system IDs [  OK  ]
Jun 26 13:19:44 cent74 openvswitch[3139]: Inserting openvswitch module [  OK  ]
Jun 26 13:19:44 cent74 openvswitch[3139]: Starting ovs-vswitchd [  OK  ]
Jun 26 13:19:44 cent74 openvswitch[3139]: Enabling remote OVSDB managers [  OK  ]
Jun 26 13:19:44 cent74 systemd[1]: Started LSB: Open vSwitch switch.
Hint: Some lines were ellipsized, use -l to show in full.
[root@cent74 openvswitch-2.5.5]# systemctl enable openvswitch
openvswitch.service is not a native service, redirecting to /sbin/chkconfig.
Executing /sbin/chkconfig openvswitch on


[root@cent74 openvswitch-2.5.5]# ovs-vsctl show
a1869730-f871-436a-b2d8-b437af708e97
    ovs_version: "2.5.5"

[root@cent74 openvswitch-2.5.5]# ovs-ofctl show br0
ovs-ofctl: br0 is not a bridge or a socket

*** CLI Add port
create bridge device
[root@cent74 openvswitch-2.5.5]# ovs-vsctl add-br vmbr0
[root@cent74 openvswitch-2.5.5]# ovs-vsctl show
a1869730-f871-436a-b2d8-b437af708e97
    Bridge "vmbr0"
        Port "vmbr0"
            Interface "vmbr0"
                type: internal
    ovs_version: "2.5.5"

Add a device to bridge device
[root@cent74 openvswitch-2.5.5]# ovs-vsctl add-port vmbr0 eno2
[root@cent74 openvswitch-2.5.5]# ovs-vsctl show
a1869730-f871-436a-b2d8-b437af708e97
    Bridge "vmbr0"
        Port "eno2"
            Interface "eno2"
        Port "vmbr0"
            Interface "vmbr0"
                type: internal
    ovs_version: "2.5.5"


[root@cent74 openvswitch-2.5.5]# ovs-vsctl list interface eno2
_uuid               : 15255afc-717f-4dc1-a309-fa09a021f517
admin_state         : up
bfd                 : {}
bfd_status          : {}
cfm_fault           : []
cfm_fault_status    : []
cfm_flap_count      : []
cfm_health          : []
cfm_mpid            : []
cfm_remote_mpids    : []
cfm_remote_opstate  : []
duplex              : full
error               : []
external_ids        : {}
ifindex             : 3
ingress_policing_burst: 0
ingress_policing_rate: 0
lacp_current        : []
link_resets         : 0
link_speed          : 1000000000
link_state          : up
lldp                : {}
mac                 : []
mac_in_use          : "0c:c4:7a:92:9c:03"
mtu                 : 1500
name                : "eno2"
ofport              : 1
ofport_request      : []
options             : {}
other_config        : {}
statistics          : {collisions=0, rx_bytes=70464, rx_crc_err=0, rx_dropped=0, rx_errors=0, rx_frame_err=0, rx_over_err=0, rx_packets=225, tx_bytes=0, tx_dropped=0, tx_errors=0, tx_packets=0}
status              : {driver_name=igb, driver_version="5.4.0-k", firmware_version="1.63, 0x80000a05"}
type                : ""

[root@cent74 openvswitch-2.5.5]# ovs-vsctl list port eno2
_uuid               : 8c97f17d-288d-4a28-bb03-ce058dc81cc0
bond_active_slave   : []
bond_downdelay      : 0
bond_fake_iface     : false
bond_mode           : []
bond_updelay        : 0
external_ids        : {}
fake_bridge         : false
interfaces          : [15255afc-717f-4dc1-a309-fa09a021f517]
lacp                : []
mac                 : []
name                : "eno2"
other_config        : {}
qos                 : []
rstp_statistics     : {}
rstp_status         : {}
statistics          : {}
status              : {}
tag                 : []
trunks              : []
vlan_mode           : []

*Update configuration to existing eno2 port
[root@cent74 openvswitch-2.5.5]# ovs-vsctl set port eno2 tag=101 trunks=101,102 vlan_mode=native-untagged
[root@cent74 openvswitch-2.5.5]# ovs-vsctl list port eno2
_uuid               : 8c97f17d-288d-4a28-bb03-ce058dc81cc0
bond_active_slave   : []
bond_downdelay      : 0
bond_fake_iface     : false
bond_mode           : []
bond_updelay        : 0
external_ids        : {}
fake_bridge         : false
interfaces          : [15255afc-717f-4dc1-a309-fa09a021f517]
lacp                : []
mac                 : []
name                : "eno2"
other_config        : {}
qos                 : []
rstp_statistics     : {}
rstp_status         : {}
statistics          : {}
status              : {}
tag                 : 101
trunks              : [101, 102]
vlan_mode           : native-untagged

* Add port with configuration
Setting port vlan101 and create vlan101 device on the system.
[root@cent74 openvswitch-2.5.5]# ovs-vsctl add-port vmbr0 vlan101 tag=101 -- set interface vlan101 type=internal
[root@cent74 openvswitch-2.5.5]# ovs-vsctl show
a1869730-f871-436a-b2d8-b437af708e97
    Bridge "vmbr0"
        Port "eno2"
            tag: 101
            trunks: [101, 102]
            Interface "eno2"
        Port "vlan101"
            tag: 101
            Interface "vlan101"
                type: internal
        Port "vmbr0"
            Interface "vmbr0"
                type: internal
    ovs_version: "2.5.5"

[root@cent74 openvswitch-2.5.5]# ovs-vsctl add-port vmbr0 vlan102 tag=102 -- set interface vlan102 type=internal
[root@cent74 openvswitch-2.5.5]# ovs-vsctl show
a1869730-f871-436a-b2d8-b437af708e97
    Bridge "vmbr0"
        Port "vlan102"
            tag: 102
            Interface "vlan102"
                type: internal
        Port "eno2"
            tag: 101
            trunks: [101, 102]
            Interface "eno2"
        Port "vlan101"
            tag: 101
            Interface "vlan101"
                type: internal
        Port "vmbr0"
            Interface "vmbr0"
                type: internal
    ovs_version: "2.5.5"

[root@cent74 openvswitch-2.5.5]# ifconfig -a
eno1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.16.103.225  netmask 255.255.0.0  broadcast 172.16.255.255
        inet6 fe80::6f4f:b1e5:bff4:8b66  prefixlen 64  scopeid 0x20<link>
        ether 0c:c4:7a:92:9c:02  txqueuelen 1000  (Ethernet)
        RX packets 2351224  bytes 493466102 (470.6 MiB)
        RX errors 0  dropped 33662  overruns 0  frame 0
        TX packets 83735  bytes 8197481 (7.8 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device memory 0xc7320000-c733ffff  

eno2: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        ether 0c:c4:7a:92:9c:03  txqueuelen 1000  (Ethernet)
        RX packets 225  bytes 70464 (68.8 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
        device memory 0xc7300000-c731ffff  

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1  (Local Loopback)
        RX packets 5701  bytes 1297205 (1.2 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 5701  bytes 1297205 (1.2 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

ovs-system: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether ea:10:a1:96:d2:0d  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.122.1  netmask 255.255.255.0  broadcast 192.168.122.255
        ether 52:54:00:94:b7:d7  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0-nic: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether 52:54:00:94:b7:d7  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

vlan101: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether 46:f0:a6:70:a3:eb  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

vlan102: flags=4098<BROADCAST,MULTICAST>  mtu 1500
        ether 46:96:00:83:38:ff  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

vmbr0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet6 fe80::ec4:7aff:fe92:9c03  prefixlen 64  scopeid 0x20<link>
        ether 0c:c4:7a:92:9c:03  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 8  bytes 648 (648.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0



[root@cent74 openvswitch-2.5.5]# ip addr add 192.168.101.1/24 dev vlan101

[root@cent74 openvswitch-2.5.5]# ip link set vlan101 up

[root@cent74 openvswitch-2.5.5]# ifconfig vlan101
vlan101: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.101.1  netmask 255.255.255.0  broadcast 0.0.0.0
        inet6 fe80::44f0:a6ff:fe70:a3eb  prefixlen 64  scopeid 0x20<link>
        ether 46:f0:a6:70:a3:eb  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 8  bytes 648 (648.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0


*** Delete port)
[root@cent74 openvswitch-2.5.5]# ovs-vsctl del-port vmbr0 eno2

*** Delete bridge)
[root@cent74 openvswitch-2.5.5]# ovs-vsctl del-br vmbr0


*** Config port)
Physical port)
[root@cent74 network-scripts]# cat ifcfg-eno2 
NAME=eno2
DEVICE=eno2
TYPE=OVSPort
DEVICETYPE=ovs
BOOTPROTO=none
OVS_BRIDGE=vmbr0
OVS_OPTIONS="tag=101 trunks=101,102 vlan_mode=native-untagged"
ONBOOT=yes
HOTPLUG=no


Openvswitch bridge port)
[root@cent74 network-scripts]# cat ifcfg-vmbr0 
DEVICE=vmbr0
NAME=vmbr0
TYPE=OVSBridge
DEVICETYPE=ovs
ONBOOT=yes
BOOTPROTO=static
HOTPLUG=no


vlan port with IP
[root@cent74 network-scripts]# cat ifcfg-vlan101 
NAME=vlan101
DEVICE=vlan101
DEVICETYPE=ovs
TYPE=OVSIntPort
OVS_BRIDGE=vmbr0
OVS_OPTIONS="tag=101"
OVS_EXTRA="set Interface $DEVICE external-ids:iface-id=$(hostname -s)-$DEVICE-vif"
BOOTPROTO=static
ONBOOT=yes
IPADDR=192.168.101.1
NETMASK=255.255.255.0
HOTPLUG=no


vlan port without IP
[root@cent74 network-scripts]# cat ifcfg-vlan102
NAME=vlan102
DEVICE=vlan102
DEVICETYPE=ovs
TYPE=OVSIntPort
OVS_BRIDGE=vmbr0
OVS_OPTIONS="tag=102"
OVS_EXTRA="set Interface $DEVICE external-ids:iface-id=$(hostname -s)-${DEVICE}-vif"
BOOTPROTO=none
ONBOOT=yes
HOTPLUG=no



*** Debug)
Disappear vlan101 device

[root@cent74 ~]# ovs-vsctl list interface vlan101
_uuid               : efbc10bc-d931-41b0-9b4e-d25ea372d176
admin_state         : []
bfd                 : {}
bfd_status          : {}
cfm_fault           : []
cfm_fault_status    : []
cfm_flap_count      : []
cfm_health          : []
cfm_mpid            : []
cfm_remote_mpids    : []
cfm_remote_opstate  : []
duplex              : []
error               : "could not open network device vlan101 (No such device)"
external_ids        : {}
ifindex             : []
ingress_policing_burst: 0
ingress_policing_rate: 0
lacp_current        : []
link_resets         : []
link_speed          : []
link_state          : []
lldp                : {}
mac                 : []
mac_in_use          : []
mtu                 : []
name                : "vlan101"
ofport              : -1
ofport_request      : []
options             : {}
other_config        : {}
statistics          : {}
status              : {}
type                : ""


[root@cent74 ~]# ifconfig -a | grep vlan101

create vlan101 device name again
[root@cent74 ~]# ovs-vsctl set interface vlan101 type=internal
[root@cent74 ~]# ovs-vsctl list interface vlan101
_uuid               : efbc10bc-d931-41b0-9b4e-d25ea372d176
admin_state         : down
bfd                 : {}
bfd_status          : {}
cfm_fault           : []
cfm_fault_status    : []
cfm_flap_count      : []
cfm_health          : []
cfm_mpid            : []
cfm_remote_mpids    : []
cfm_remote_opstate  : []
duplex              : []
error               : []
external_ids        : {}
ifindex             : 8
ingress_policing_burst: 0
ingress_policing_rate: 0
lacp_current        : []
link_resets         : 0
link_speed          : []
link_state          : down
lldp                : {}
mac                 : []
mac_in_use          : "62:09:4e:7c:7c:54"
mtu                 : 1500
name                : "vlan101"
ofport              : 2
ofport_request      : []
options             : {}
other_config        : {}
statistics          : {collisions=0, rx_bytes=0, rx_crc_err=0, rx_dropped=0, rx_errors=0, rx_frame_err=0, rx_over_err=0, rx_packets=0, tx_bytes=0, tx_dropped=0, tx_errors=0, tx_packets=0}
status              : {driver_name=openvswitch}
type                : internal
[root@cent74 ~]# ifconfig -a | grep vlan101
vlan101: flags=4098<BROADCAST,MULTICAST>  mtu 1500


*** configure port reference)
To use the integration for a Open vSwitch bridge or interface named
<name>, create or edit /etc/sysconfig/network-scripts/ifcfg-<name>.
This is a shell script that consists of a series of VARIABLE=VALUE
assignments. The following OVS-specific variable names are supported:

- DEVICETYPE: Always set to "ovs".

    - TYPE: If this is "OVSBridge", then this file represents an OVS
      bridge named <name>.  Otherwise, it represents a port on an OVS
      bridge and TYPE must have one of the following values:

        * "OVSPort", if <name> is a physical port (e.g. eth0) or
          virtual port (e.g. vif1.0).

        * "OVSIntPort", if <name> is an internal port (e.g. a tagged
          VLAN).

        * "OVSBond", if <name> is an OVS bond.

        * "OVSTunnel", if <name> is an OVS tunnel.

        * "OVSPatchPort", if <name> is a patch port

    - OVS_BRIDGE: If TYPE is anything other than "OVSBridge", set to
      the name of the OVS bridge to which the port should be attached.

    - OVS_OPTIONS: Optionally, extra options to set in the "Port"
      table when adding the port to the bridge, as a sequence of
      column[:key]=value options.  For example, "tag=100" to make the
      port an access port for VLAN 100.  See the documentation of
      "add-port" in ovs-vsctl(8) for syntax and the section on the
      Port table in ovs-vswitchd.conf.db(5) for available options.

    - OVS_EXTRA: Optionally, additional ovs-vsctl commands, separated
      by "--" (double dash).

    - BOND_IFACES: For "OVSBond" interfaces, a list of physical
      interfaces to bond together.

    - OVS_TUNNEL_TYPE: For "OVSTunnel" interfaces, the type of the tunnel.
      For example, "gre", "vxlan", etc.

    - OVS_TUNNEL_OPTIONS: For "OVSTunnel" interfaces, this field should be
      used to specify the tunnel options like remote_ip, key, etc.

    - OVS_PATCH_PEER: For "OVSPatchPort" devices, this field specifies
      the patch's peer on the other bridge.

Note
----

* "ifdown" on a bridge will not bring individual ports on the bridge
down.  "ifup" on a bridge will not add ports to the bridge.  This
behavior should be compatible with standard bridges (with
TYPE=Bridge).

* If 'ifup' on an interface is called multiple times, one can see
"RTNETLINK answers: File exists" printed on the console. This comes from
ifup-eth trying to add zeroconf route multiple times and is harmless.

Examples
--------

Standalone bridge:

==> ifcfg-ovsbridge0 <==
DEVICE=ovsbridge0
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSBridge
BOOTPROTO=static
IPADDR=A.B.C.D
NETMASK=X.Y.Z.0
HOTPLUG=no

Enable DHCP on the bridge:
* Needs OVSBOOTPROTO instead of BOOTPROTO.
* All the interfaces that can reach the DHCP server
as a space separated list in OVSDHCPINTERFACES.

DEVICE=ovsbridge0
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSBridge
OVSBOOTPROTO="dhcp"
OVSDHCPINTERFACES="eth0"
HOTPLUG=no


Adding Internal Port to ovsbridge0:

==> ifcfg-intbr0 <==
DEVICE=intbr0
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSIntPort
OVS_BRIDGE=ovsbridge0
HOTPLUG=no


Internal Port with fixed IP address:

DEVICE=intbr0
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSIntPort
OVS_BRIDGE=ovsbridge0
BOOTPROTO=static
IPADDR=A.B.C.D
NETMASK=X.Y.Z.0
HOTPLUG=no

Internal Port with DHCP:
* Needs OVSBOOTPROTO or BOOTPROTO.
* All the interfaces that can reach the DHCP server
as a space separated list in OVSDHCPINTERFACES.

DEVICE=intbr0
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSIntPort
OVS_BRIDGE=ovsbridge0
OVSBOOTPROTO="dhcp"
OVSDHCPINTERFACES="eth0"
HOTPLUG=no

Adding physical eth0 to ovsbridge0 described above:

==> ifcfg-eth0 <==
DEVICE=eth0
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSPort
OVS_BRIDGE=ovsbridge0
BOOTPROTO=none
HOTPLUG=no


Tagged VLAN interface on top of ovsbridge0:

==> ifcfg-vlan100 <==
DEVICE=vlan100
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSIntPort
BOOTPROTO=static
IPADDR=A.B.C.D
NETMASK=X.Y.Z.0
OVS_BRIDGE=ovsbridge0
OVS_OPTIONS="tag=100"
OVS_EXTRA="set Interface $DEVICE external-ids:iface-id=$(hostname -s)-$DEVICE-vif"
HOTPLUG=no


Bonding:

==> ifcfg-bond0 <==
DEVICE=bond0
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSBond
OVS_BRIDGE=ovsbridge0
BOOTPROTO=none
BOND_IFACES="gige-1b-0 gige-1b-1 gige-21-0 gige-21-1"
OVS_OPTIONS="bond_mode=balance-tcp lacp=active"
HOTPLUG=no

==> ifcfg-gige-* <==
DEVICE=gige-*
ONBOOT=yes
HOTPLUG=no

An Open vSwitch Tunnel:

==> ifcfg-gre0 <==
DEVICE=ovs-gre0
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSTunnel
OVS_BRIDGE=ovsbridge0
OVS_TUNNEL_TYPE=gre
OVS_TUNNEL_OPTIONS="options:remote_ip=A.B.C.D"


Patch Ports:

==> ifcfg-patch-ovs-0 <==
DEVICE=patch-ovs-0
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSPatchPort
OVS_BRIDGE=ovsbridge0
OVS_PATCH_PEER=patch-ovs-1

==> ifcfg-patch-ovs-1 <==
DEVICE=patch-ovs-1
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSPatchPort
OVS_BRIDGE=ovsbridge1
OVS_PATCH_PEER=patch-ovs-0

------fake bridge configuration--------------
DEVICE=vlan65
DEVICETYPE=ovs
TYPE=OVSBridge
ONBOOT=yes
BOOTPROTO=static
STP=off
NM_CONTROLLED=no
HOTPLUG=no
OVS_EXTRA="br-set-external-id $DEVICE bridge-id $DEVICE"
OVS_OPTIONS="br0 65"


Variation of fake bridge 
==> ifcfg-vlan100 <==
DEVICE=vlan12
ONBOOT=yes
DEVICETYPE=ovs
TYPE=OVSIntPort
BOOTPROTO=static
IPADDR=A.B.C.D
NETMASK=X.Y.Z.0
OVS_BRIDGE=ovsbr
OVS_OPTIONS="tag=12"
OVS_EXTRA="set Interface $DEVICE external-ids:iface-id=$(hostname -s)-$DEVICE-vif"
HOTPLUG=no
