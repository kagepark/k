# Kage
# Kage command server
#for DAemon
import os, time
import sys
import signal

import threading
import cPickle as pickle
import os,sys
_k_home_dir=os.path.dirname(os.path.realpath(__file__))
_k_base_dir='{0}/client'.format(_k_home_dir)
base_dir='{0}/data'.format(_k_home_dir)
# for random_file_str()
#import random
from datetime import datetime
sys.path.append('{}/lib/python'.format(base_dir))
#Read Shell
import kCodeRead as kcr
import kNet
import kRandom as krnd
import kFile as kf
import kmisc as km
from kDaemon import Daemon

_k_version='4.2.124'
_k_log_file='{}/kServer.log'.format(os.path.dirname(os.path.realpath(__file__)))
def log(msg):
    print('{} : {}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S'),msg))
    with open(_k_log_file,'a+') as f:
        f.write('{} : {}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S'),msg))

# Server Function
# kNet.start_server will give conn,ip,port variable to client_thread2()
# conn : client connection
# ip: client IP
# port : client port

###################################################
# DB Data handle
#def code_lib(name,lib_type):
#    return kcr.code_lib(name,base_dir,lib_type)
#
#def code_body(filename,lib_type):
#    return kcr.code_body(filename,base_dir,lib_type)
def get_hosts(base_dir):
    hosts={}
    host_file='{}/hosts/hosts'.format(base_dir)
    if os.path.isfile(host_file):
         with open(host_file,'r') as f:
             host_line=f.read()
         host_a=host_line.split('\n')
         for ii in host_a:
             if ii and ii[0] != '#':
                 ii_a=' '.join(ii.split()).split(' ')
                 if not ii_a[-1] in hosts:
                     hosts[ii_a[-1]]={'ip':ii_a[0]}
                 key_file='{}/hosts/{}'.format(base_dir,ii_a[-1])
                 if os.path.isfile(key_file):
                     with open(key_file,'r') as h:
                         hosts[ii_a[-1]]['key']=h.read()
    return hosts

def build(filename,base_dir):
    if os.path.isfile('{}/{}'.format(base_dir,filename)):
        dir_name=os.path.dirname(filename)
        bin_name=os.path.basename(filename).replace('.py','')
        os.system('''cd {0}; [ -f {1}/dist/k ] && rm -f {1}/dist/k; [ -d {1}/build ] && rm -fr {1}/build; [ -f {1}/{2}.spec ] && rm -f {1}/{2}.spec; [ -d /tmp/build/{2} ] && rm -fr /tmp/build/{2}'''.format(base_dir,dir_name,bin_name))
        log('build : {}/{}'.format(base_dir,filename))
        rc=km.rshell('cd {0}/{1}; PYTHONOPTIMIZE=3 pyinstaller -p {3}/data/lib/python --workpath /tmp/build --onefile {2}'.format(base_dir,dir_name,os.path.basename(filename),_k_home_dir))
        if rc[0] == 0:
            if os.path.isfile('{0}/{1}/dist/{2}'.format(base_dir,dir_name,bin_name)):
                log('got : {}/dist/{}'.format(dir_name,bin_name))
                return {'rc':True,'data':os.path.basename(bin_name)}
            else:
                return {'rc':False,'data':'{} file not found'.format(os.path.basename(bin_name))}
        else:
            return {'rc':False,'data':rc[1]+rc[2]}
    else:
        return {'rc':False,'data':'{} not found'.format(filename)}

def cmd_dict():
    cmd_d={}
    for ii in ['shell','python']:
        if not ii in cmd_d:
            cmd_d[ii] = {}
        for r, d, f in os.walk('{}/cmd/{}'.format(base_dir,ii)):
            for ff in f:
                if not ff in cmd_d[ii]:
                    cmd_d[ii][ff]={}
    return cmd_d

def read(filename,base_dir=base_dir):
    dest_file='{}/{}'.format(base_dir,filename)
    if os.path.isfile(dest_file):
        with open(dest_file,'r') as f:
            data=f.read()
        return data
    return False

def save(filename,data,force=False,base_dir=base_dir):
    dest_file='{}/{}'.format(base_dir,filename)
    if force is False and os.path.exists(dest_file):
        log('save fail: exist {}'.format(filename))
        return 2
    elif os.path.isdir(os.path.dirname(dest_file)):
        with open(dest_file,'w') as f:
            f.write(data)
        log('save : {}'.format(filename))
        return 0
    else:
        log('save : Error {}'.format(filename))
        return 1

def mkdir(dest,base_dir=base_dir,block=[]):
    dest_a=dest.split('/')
    if len(dest_a) > 1 and dest_a[0] in ['memo','save','temp','lib','code','bin'] and not dest_a[0] in block:
        if os.path.isdir(os.path.dirname('{}/{}'.format(base_dir,dest))):
             if not os.path.exists('{}/{}'.format(base_dir,dest)):
                 os.mkdir('{}/{}'.format(base_dir,dest))
                 return True
             else:
                 return 'aready exist {}'.format(dest)
        else:
             return 'parent directory not found'
    return False
   
def trash(filename,move=True,base_dir=base_dir):
    trash_dir='{}/data/trash'.format(_k_home_dir)
    dest_file='{0}/{1}'.format(base_dir,filename)
    if os.path.isfile(dest_file):
        trash_file_tmp=kf.mktemp(trash_dir,filename=os.path.basename(filename),ext=datetime.now().strftime('%s'))
        if move:
            cmd_str='''mv {} {}'''.format(dest_file,trash_file_tmp)
            #cmd_str='''cd {} && mv {} {}'''.format(base_dir,filename,trash_file_tmp)
        else:
            cmd_str='''cp -a {} {}'''.format(dest_file,trash_file_tmp)
        rc=km.rshell(cmd_str)
        if rc[0] == 0:
            log('trash: {} => tash/{}'.format(filename,os.path.basename(trash_file_tmp)))
            return True
    log('trash: {} not found'.format(dest_file))
    return False

def _cmd(cmd):
    cmd_a=cmd.split(':')
    if len(cmd_a) == 2:
        return tuple(cmd_a)
    elif len(cmd_a) == 1:
        return cmd,None
    else:
        return False,None

def _dest(dest):
    if dest and dest[0] == '!':
        return dest[1:]
    return dest
 
def _path(dest=None,category=None):
    if dest:
        dest=_dest(dest)
        if category:
            return '{}/{}/{}'.format(base_dir,category,dest)
        else:
            return '{}/{}'.format(base_dir,dest)
    if category:
        return '{}/{}'.format(base_dir,category)
    else:
        return base_dir

def _find(dest,block=[],category=None):
    if not block and not category:
        category='.'
    elif not category:
        category='code memo task temp'
    category_chk=category.split()
    for ii in block:
        if ii in category_chk:
            category_chk.remove(ii)
    if len(category_chk):
        category=' '.join(category_chk)
    else:
        return False
    found=km.rshell('cd {}; find {} | grep -i "{}"'.format(base_dir,category,dest))[1].split('\n')
    #return {'rc':True,'data':found}
    return found

def _grep(dest,block=[],category=None):
    if not block and not category:
        category='.'
    elif not category:
        category='code memo task temp'
    category_chk=category.split()
    for ii in block:
        if ii in category_chk:
            category_chk.remove(ii)
    if len(category_chk):
        category=' '.join(category_chk)
    else:
        return False
    found_str='''cd %s; find %s -type f -exec grep -H -i "%s" {} \; 2>/dev/null | awk -F: ' '''%(base_dir,category,dest)
    found_str=found_str+''' {print $1}' | grep -v "Binary file" | sort | uniq'''
    found=km.rshell(found_str)[1].split('\n')
    #return {'rc':True,'data':found}
    return found

def _ls(dest,block=[],category=None):
    if not dest:
        dest=[None]
    if type(dest) is str:
        dest=[dest]
    list_tmp=[]
    for ii in dest:
        if block is None or (type(block) is list and type(ii) is str and ii.split('/')[0] in block):
            return False
        ii_path=_path(ii,category=category)
        if os.path.isdir(ii_path):
            list_tmp=list_tmp+os.listdir(ii_path)
            for zz in block:
                if zz in list_tmp:
                    list_tmp.remove(zz)
        elif os.path.exists(ii_path):
            list_tmp.append(ii)
        else:
            list_tmp.append('{} not found'.format(ii))
    return list_tmp

####################################
# Network Packet hanle on Server
def th_packet(conn, ip, port): # thread per each connection
    # read lines periodically without ending connection
    still_listen = True
    while still_listen:
        rc = kNet.get_packet(conn) # receive data
        if type(rc) is dict and ('cmd' in rc or 'data' in rc): # received something 
            # Server Code here
            # echo test
            #input_from_client = pickle.loads(input_from_client) # receive data
            #log('cmd:{0},sub_cmd:{1}'.format(cmd,sub_cmd))
            cmd,sub_cmd=_cmd(rc['cmd'])
            log('cmd:{}, sub_cmd:{}, scmd:{}, dest:{}'.format(cmd, sub_cmd,rc['scmd'],rc['dest']))
            if cmd == 'login': # build file
                if sub_cmd == 'kage' and rc['dest'] == 'K@ge9ark':
                    kNet.put_packet(conn,kNet.packet(rc=True,data='ok'))
                else:
                    kNet.put_packet(conn,kNet.packet(rc=False,data='ok'))

            elif cmd == 'build': # build file
                if sub_cmd == 'k':
                    brc=build('k/k.py',_k_base_dir)
                    if brc['rc']:
                        kNet.put_packet(conn,kNet.packet(rc=True,data=brc['data'],info={'name':'k'}))
                    else:
                        kNet.put_packet(conn,kNet.packet(rc=False,data=brc['data']))
                elif sub_cmd == 'cmd':
                    brc=build('cmd/{}'.format(rc['dest']),base_dir) # Python is work, but shell????
                    if brc['rc']:
                        kNet.put_packet(conn,kNet.packet(rc=True,data=brc['data'],info={'name':'k'}))
                    else:
                        kNet.put_packet(conn,kNet.packet(rc=False,data=brc['data']))
                else:
                    # add code here for build any file in temp directory
                    kNet.put_packet(conn,kNet.packet(rc=False,data='Unknown command'))
            elif cmd == 'save':
                dest=_dest(rc['dest'])
                save_file=1
                if sub_cmd in ['lib','cmd']:
                    trash('{}/{}'.format(sub_cmd,dest),move=True)
                    save_file=save('{}/{}'.format(sub_cmd,dest),rc['data'],force=True)
                elif sub_cmd == 'khosts':
                    trash('{}'.format(dest),move=True)
                    save_file=save('{}'.format(dest),rc['data'],force=True)
                elif sub_cmd == 'k7':
                    trash('k/k.py',move=True,base_dir=_k_base_dir)
                    save_file=save('k/k.py',rc['data'],force=False,base_dir=_k_base_dir)
                else:
                    save_file=save(dest,rc['data'],force=False)
                if save_file == 2:
                    kNet.put_packet(conn,kNet.packet(rc=False,data='Exist:{0}'.format(dest)))
                elif save_file == 0:
                    kNet.put_packet(conn,kNet.packet(rc=True,data='saved:{0}'.format(dest)))
                else:
                    kNet.put_packet(conn,kNet.packet(rc=False,data='Error:{0}'.format(dest)))
            elif cmd in ['find','grep','ls']:
                block=[]
                if sub_cmd in ['lib','cmd','[k]','cmd_dict']: # Special command
                    if sub_cmd == '[k]': # Special command
                         sub_cmd=None
                else:
                    block=['k','build','dist','lib','trash','cmd']
                if cmd == 'ls':
                    if sub_cmd == 'cmd_dict':
                        found=cmd_dict()
                    else:
                        found=_ls(rc['dest'],category=sub_cmd,block=block)
                elif cmd == 'find':
                    found=_find(rc['scmd'],category=rc['dest'],block=block)
                else:
                    found=_grep(rc['scmd'],category=rc['dest'],block=block)
                if found:
                    if found is True:
                        kNet.put_packet(conn,kNet.packet(data='',rc=True))
                    else:
                        kNet.put_packet(conn,kNet.packet(data=found,rc=True))
                else:
                    kNet.put_packet(conn,kNet.packet(data='Not support',rc=False))
            elif cmd == 'mkdir':
                if sub_cmd:
                    found=mkdir(rc['dest'],base_dir=base_dir)
                    if found is True:
                        kNet.put_packet(conn,kNet.packet(data='',rc=True))
                    else:
                        kNet.put_packet(conn,kNet.packet(data=found,rc=True))
            elif cmd == 'get':
                dest=_dest(rc['dest'])
                data=None
                lib_type=None
                if dest:
                    lib_type=dest.split('/')[0]
                if sub_cmd == 'run_cmd': # get command code
#                    dest=_dest(rc['dest'])
#                    lib_type=dest.split('/')[0]
                    #filename='{}/{}'.format(sub_cmd,dest)
                    filename='cmd/{}'.format(dest)
                    data=kcr.code_body(filename,base_dir,lib_type)
                elif sub_cmd in ['lib','cmd']: # get command code
#                    dest=_dest(rc['dest'])
                    filename='{}/{}'.format(sub_cmd,dest)
                    data=read(filename)
                elif sub_cmd =='hosts': # get command code
                    data=get_hosts(base_dir)
                elif sub_cmd == 'khost':
                    data=read('hosts/hosts')
                elif sub_cmd == 'k7':
                    data=read('k/k.py',base_dir=_k_base_dir)
                elif sub_cmd == 'update':
                    data=read('k/dist/k',base_dir=_k_base_dir)
                elif sub_cmd == 'func':
                    #data=kcr.read_all_func_code_from_func_list(rc['dest'],base_dir,'python')
                    data=kcr.read_all_func_code_from_func_list(dest,base_dir,'python')
                elif not lib_type in ['lib','cmd']:
                    data=read(rc['dest']) # cmd, memo, ... directory read file.
                if data:
                    kNet.put_packet(conn,kNet.packet(rc=True,data=data))
                else:
                    #kNet.put_packet(conn,kNet.packet(rc=False,data='not found: {0}'.format(rc['dest'])))
                    kNet.put_packet(conn,kNet.packet(rc=False,data='not found: {0}'.format(dest)))
            elif cmd == 'del':
                dest=_dest(rc['dest'])
                category=dest.split('/')[0]
                if not sub_cmd and category in ['trash','lib','k','cmd']:
                    kNet.put_packet(conn,kNet.packet(rc=False,data='Not support directory'))
                elif sub_cmd in ['lib','cmd']:
                    dest_file=dest.split('/')[-1]
                    if dest_file in ['kCmdLine.py','kCodeRead.py','kCrypt.py','kFile.py','kLine.py','kMemory.py','kmisc.py','kNet.py','kPacket.py','kRandom.py','kProgress.py','kDict.py']:
                        if trash('{}/{}'.format(sub_cmd,dest),move=False):
                            kNet.put_packet(conn,kNet.packet(rc=True,data='trashed: {0}'.format(dest)))
                        else:
                            kNet.put_packet(conn,kNet.packet(rc=False,data='not support: {0}'.format(dest)))
                    else:
                        if trash('{}/{}'.format(sub_cmd,dest),move=True):
                            kNet.put_packet(conn,kNet.packet(rc=True,data='trashed: {0}'.format(dest)))
                        else:
                            kNet.put_packet(conn,kNet.packet(rc=False,data='not found: {0}'.format(dest)))
                else:
                    if trash(dest,move=True):
                        kNet.put_packet(conn,kNet.packet(rc=True,data='trashed: {0}'.format(dest)))
                    else:
                        kNet.put_packet(conn,kNet.packet(rc=False,data='not found: {0}'.format(dest)))
            else:
                kNet.put_packet(conn,kNet.packet(rc=False,data='{}: Unknown command'.format(cmd)))
        else: # End data
            conn.close()
            log('Connection ' + ip + ':' + port + " ended")
            still_listen = False


####################################
# Network Data hanle on Server
def th_data(conn, ip, port, MAX_BUFFER_SIZE = kNet.MAX_BUFFER_SIZE): # thread per each connection , se
    # read lines periodically without ending connection
    still_listen = True
    while still_listen:
        input_from_client = kNet.get_data(conn, MAX_BUFFER_SIZE) # receive data
        if input_from_client: # received something 
            # Server Code here
            # echo test
            log(input_from_client)
            if input_from_client == 'ls':
                kNet.put_data(conn,os.listdir())
            else:
                kNet.put_data(conn,input_from_client)
#            kNet.put_data(conn,kNet.SESSION_END)
        else: # End data
            conn.close()
            log('Connection ' + ip + ':' + port + " ended")
            still_listen = False

class MyDaemon(Daemon):
    def run(self):
        # Start Daemon process
        kNet.start_server("0.0.0.0",7992,th_packet)

if __name__ == '__main__':
##############################################
# Server IP : 0.0.0.0 : All network
# Server Port
# client_thread2 is server function
#    kNet.start_server("0.0.0.0",7992,th_data)
    #kNet.start_server("0.0.0.0",7992,th_packet)

    def exit():
        os._exit(0)

    if os.path.isdir('/var/tmp'):
        pid_path='/var/tmp/k.pid'
    else:
        pid_path='/tmp/k.pid'

    import signal
    signal.signal(signal.SIGINT,exit)

    MyD=MyDaemon(pid_path)
    if len(sys.argv) > 1:
        if sys.argv[1] == 'start':
            MyD.start()
        elif sys.argv[1] == 'stop':
            MyD.stop()
        elif sys.argv[1] == 'status':
            MyD.status()
        elif sys.argv[1] == 'restart':
            MyD.stop()
            MyD.start()
        elif sys.argv[1] == 'cli':
            kNet.start_server("0.0.0.0",7992,th_packet)
        else:
            log('{0} <start|stop|status>'.format(sys.argv[0]))
    else:
        log('{0} <start|stop|status>'.format(sys.argv[0]))
